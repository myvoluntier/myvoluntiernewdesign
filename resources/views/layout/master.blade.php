@extends('layout.frame')
@section('css')
<style>
    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }

    .error-has {
        border: 1px solid #ff0000 !important;
    }
    .select2-container--below{background: yellow};
</style>
@yield('css')
@endsection
@section('body')
<body>
<!--
    <script>
        window.fcWidget.init({
        token: "8b64b62c-6e0d-4c8c-bc9a-1ac397065ad8",
                host: "https://wchat.freshchat.com"
        });    </script>
   -->
    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5426731.js"></script>
    <!-- End of HubSpot Embed Code -->  

    <script>
                // Make sure fcWidget.init is included before setting these values

                // To set unique user id in your system when it is available
                /*   window.fcWidget.setExternalId("john.doe1987");
                 
                 // To set user name
                 window.fcWidget.user.setFirstName("John");
                 
                 // To set user email
                 window.fcWidget.user.setEmail("john.doe@gmail.com");
                 */
                // To set user properties
                //window.fcWidget.user.setProperties({
                 //   plan: "Estate",  // meta property 1
                 //   status: "Active" // meta property 2
                //});
    </script>
<!--
    <script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script>

    {{--<script type="text/javascript">--}}
        {{--FreshWidget.init("", {"queryString": "&widgetType=popup&formTitle=Help+%26+Support&submitTitle=Send+feedback&submitThanks=Thank+you+for+your+feedback.&captcha=yes", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "Support", "buttonColor": "white", "buttonBg": "#00C515", "alignment": "4", "offset": "235px", "submitThanks": "Thank you for your feedback.", "formHeight": "500px", "captcha": "yes", "url": "https://myvoluntierhelp.freshdesk.com"}); --}}
        {{--</script>--}}
-->

    <div class="offcanvas-menu">
        <div class="container">
            <div class="header-center-menu">
                <ul class="nav">
                    @include('components.non-auth.buttons_registration_and_login')
                </ul>
            </div>
        </div>
    </div>

    <div class="offcanvas-menu-backdrop"></div>

    <div class="offcanvas-contentarea">

        <div class="wrapper_bottom_footer">
            @include('components.non-auth.header')

            <div class="row-content">
                <input class="type-user" type="hidden" value="Volunteer">

                @if(request()->confirmed)
                    <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                        <strong>Hi, </strong> Your account has been confirmed. Please login to continue.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @yield('content')
            </div>

            @include('components.footer')

        </div>

    </div>

    {{--show only if user is not logged in--}}
    @if(!Auth::check())
        @include('components.non-auth.modal_login')
    @endif

    @include('components.non-auth.modal_user_tipe')
    @include('components.non-auth.modal_pop_up_sign_org')
    @include('components.non-auth.modal_pop_up_sign_vol')
    @include('components.non-auth.modal_pop_up_sign_forg_pass')
    @include('components.non-auth.modal_org_tipe')
    @include('components.non-auth.modal_terms_and_conditions')
    @include('components.non-auth.modal_privacy_policy')
    @include('components.non-auth.modal_successful_reg_pop_up')

    <script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('front-end/js/popper.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/check_validate.js')}}"></script>
    <script src="{{asset('js/home-action.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('front-end/js/select2.full.js')}}"></script>
    <script src="{{asset('js/inputmask/jQuery.SimpleMask.min.js')}}"></script>

    @include('admin.include.toaster-js')

    <script>

    $(document).ready(function () {

        $('.phoneUSMask').simpleMask({
            'mask': ['###-###-####']
        });

        $('.userName').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        });

        $('.Founded_year_format').datepicker({
            autoclose: true,
            format: 'yyyy',
            minViewMode: 2
        });
        var $outerwidth = $('.row-header header .outer-width-box');
        var $innerwidth = $('.row-header header .inner-width-box');

        function checkWidth() {

            var outersize = $outerwidth.width();
            var innersize = $innerwidth.width();
            if (innersize > outersize) {
                $('body').addClass("navmobile");
            } else {
                $('body').removeClass("navmobile");
                $('body').removeClass("offcanvas-menu-show");
            }
        }

        checkWidth();
        $(window).resize(checkWidth);
        $('.offcanvas-menu-backdrop').on('click', function (e) {
            $('body').toggleClass("offcanvas-menu-show");
            e.preventDefault();
        });
        $('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
            $('body').toggleClass("offcanvas-menu-show");
            e.preventDefault();
        });

        function doResize($wrapper, $el) {

            var scale = Math.min(
                $wrapper.outerWidth() / $el.outerWidth(),
                $wrapper.outerHeight() / $el.outerHeight()
            );
            if (scale < 1) {

                $el.css({
                    '-webkit-transform': 'scale(' + scale + ')',
                    '-moz-transform': 'scale(' + scale + ')',
                    '-ms-transform': 'scale(' + scale + ')',
                    '-o-transform': 'scale(' + scale + ')',
                    'transform': 'scale(' + scale + ')'
                });
            } else {
                $el.removeAttr("style");
            }
        }

        doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
        $(window).resize(function () {
            doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
        });
        $('body').on('click', '.close_open', function () {
            $('#myModalSuccessfulReg').modal('hide');
        });
        $('body').on('change', '.type-user-select', function () {
            $('.type-user').val($('.type-user-select').val())
        });
        $('body').on('click', '.login_button_class', function () {
            $('.close').click();
            setTimeout(function () {
                $('#myModalLogin').modal('show');
                $('#login_user').val('');
                setTimeout(function () {
                    $('body').addClass('modal-open');
                    $('body').removeClass('offcanvas-menu-show')

                }, 500);
            }, 200);
        });
        $('body').on('click', '.termsAndConditions', function (e) {
            e.preventDefault();
            $('#type_tab').val($(this).data('type'));
            $('#myModalSingOrgRegistration').modal('hide');
            $('#myModalSingVolRegistration').modal('hide');
            setTimeout(function () {
                $('#myModalTermsAndConditions').modal('show');
                $('body').removeClass('offcanvas-menu-show');
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200);
        });

        $('body').on('click', '.privacyPolicy', function (e) {
            e.preventDefault();
            $('#type_tab').val($(this).data('type'));
            $('#myModalSingOrgRegistration').modal('hide');
            $('#myModalSingVolRegistration').modal('hide');
            $('#myModalTermsAndConditions').modal('hide');
            setTimeout(function () {
                $('#myModalprivacyPolicy').modal('show');
                $('body').removeClass('offcanvas-menu-show');
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200);
        });

        $('#btn_agree').on('click', function () {
            $('#o_accept_terms').prop('checked', true);
            $('#v_accept_terms').prop('checked', true);
            $('#v_terms_alert').hide();
            $('#ov_terms_alert').hide();
            $('#myModalTermsAndConditions').modal('hide');
            setTimeout(function () {
                if ($('#type_tab').val() == 'org') {
                    $('#myModalSingOrgRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show');
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
                else {
                    $('#myModalSingVolRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show');
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
            }, 200);
        });
        $('#btn_decline').on('click', function () {
            $('#v_terms_alert').hide();
            $('#ov_terms_alert').hide();
            $('#myModalTermsAndConditions').modal('hide');
            setTimeout(function () {
                if ($('#type_tab').val() == 'org') {
                    $('#myModalSingOrgRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show');
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
                else {
                    $('#myModalSingVolRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show');
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
            }, 200);
        });
        $('#btn_agree1').on('click', function () {
            $('#op_accept_terms').prop('checked', true);
            $('#p_accept_policy').prop('checked', true);
            $('#ov_policy_alert').hide();
            $('#v_policy_alert').hide();
            $('#myModalprivacyPolicy').modal('hide');
            setTimeout(function () {
                if ($('#type_tab').val() == 'org') {
                    $('#myModalSingOrgRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show');
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
                else {
                    $('#myModalSingVolRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show');
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
            }, 200);
        });
        $('#btn_decline1').on('click', function () {
            $('#ov_policy_alert').hide();
            $('#v_policy_alert').hide();
            $('#myModalprivacyPolicy').modal('hide');
            setTimeout(function () {
                if ($('#type_tab').val() == 'org') {
                    $('#myModalSingOrgRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show');
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
                else {
                    $('#myModalSingVolRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show');
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
            }, 200);
        });
        @if(!Auth::check())
        $(document).on('click', '.vol_registr', function (e) {
            e.preventDefault();
            scroll(0, 0);
            setTimeout(function () {
                var text = 'Volunteer';
                $('.type-user').val(text);
                $('#myModalSingVolRegistration').modal('show');
                $('body').removeClass('offcanvas-menu-show');
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200)
        });
        $(document).on('click', '.org_registr', function (e) {
            scroll(0, 0);
            setTimeout(function () {
                e.preventDefault();
                var text = 'Organization';
                $('.type-user').val(text);
                $('#myModalOrgType').modal('show');
                $('body').removeClass('offcanvas-menu-show');
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200)
        });
        $(document).on('click', '.inst_registr', function (e) {
            e.preventDefault();
            scroll(0, 0);
            var text = 'Organization';
            $('.type-user').val(text);
            $('#org_type').val(1);
            $('#chooseTypeOrg').click();
            setTimeout(function () {

            }, 200);
        });
        @endif


        $('body').on('click', '#chooseTypeOrg', function (e) {
            e.preventDefault();
            var typeOrg = $('#org_type').val();
            if (typeOrg == 1) {

                $('.ein_div').hide();
                $('.org_name_label').text('School Name:')
                $('.org_type_div').hide()
                $('.non-org-type').hide();
                $('.school_type').show();
            }

            else if (typeOrg == 2) {
                $('.org_type_div').hide();
                $('.ein_div').show();
                if($('#type-user-select-id').val() == 'SubOrganization'){
                    $('.org_name_label').text('Sub Organization Name:');
                }else{
                    $('.org_name_label').text('Organization Name:');
                }
                $('.school_type').hide();
                $('.non-org-type').show();
            }

            else if (typeOrg == 3) {
                $('.org_type_div').hide();
                $('.ein_div').hide();
                $('.school_type').hide();
                $('.non-org-type').hide();
            }

            else {
                $('.org_type_div').show();
                $('.ein_div').hide();
                $('.school_type').hide();
                $('.non-org-type').hide();
            }

            $('#myModalOrgType').modal('hide');
            setTimeout(function () {
                $('#myModalSingOrgRegistration').modal('show');
                $('body').removeClass('offcanvas-menu-show')
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200);
        });
        $('body').on('click', '#login_button', function () {
            $('.close').click();
            $('#myModalLogin').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        });


        $('body').on('click', '#forPass', function (e) {
            e.preventDefault();
            $('#myModalLogin').modal('hide');
            setTimeout(function () {
                $('#myModalForgotPassword').modal('show');
                $('body').removeClass('offcanvas-menu-show');
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200);
        });
        $('body').on('click', '.registration_button', function () {
            $('#myModalUserTipe').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        });

        $('body').on('click', '.registration_button_on_modal_form', function (e) {
            e.preventDefault();
            $('#myModalLogin').modal('hide');
            setTimeout(function () {
                $('#myModalUserTipe').modal('show');
                $('body').removeClass('offcanvas-menu-show');
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200);
        });
        $("select").select2({
            theme: "bootstrap",
            minimumResultsForSearch: -1
        });
        $('body').on('click', '#chooseType', function (e) {
            e.preventDefault();
           
            $('#orgSelectError').css('display','none');
            var typeUser = $('.type-user').val();
            if (typeUser === 'Volunteer') {
                $('#myModalUserTipe').modal('hide');
                setTimeout(function () {
                    $('#myModalSingVolRegistration').modal('show');
                    $('body').removeClass('offcanvas-menu-show')
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }, 200);
            }
            else if (typeUser === 'Organization') {

                $('#myModalUserTipe').modal('hide');
                setTimeout(function () {
                    $('#myModalOrgType').modal('show');
                    $('body').removeClass('offcanvas-menu-show')
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                    // $('#myModalSingOrgRegistration').modal('show');
                }, 200);
            }else if (typeUser === 'SubOrganization') {
                $('#myModalUserTipe').modal('hide');
                    setTimeout(function () {
                        $('#myModalOrgType').modal('show');
                        $('body').removeClass('offcanvas-menu-show')
                        setTimeout(function () {
                            $('body').addClass('modal-open');
                        }, 500);
                        // $('#myModalSingOrgRegistration').modal('show');
                    }, 200);
                
            }
        });
        $('body').on('click', '.previous', function () {
            $('#myModalSingVolRegistration').modal('hide');
            $('#myModalSingOrgRegistration').modal('hide');
            setTimeout(function () {
                if ($('.type-user').val() == 'Volunteer') {
                    $('#myModalUserTipe').modal('show');
                    $('body').removeClass('offcanvas-menu-show')
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
                else {
                    $('#myModalOrgType').modal('show');
                    $('body').removeClass('offcanvas-menu-show')
                    setTimeout(function () {
                        $('body').addClass('modal-open');
                    }, 500);
                }
            }, 200);
        });
        $('.wrapper_input.fa-icons i').click(function () {
            $('.wrapper_input.fa-icons input').datepicker('show');
        });

        $('.wrapper_input.fa-icons input').datepicker({
            'format': 'mm-dd-yyyy',
            'autoclose': true,
            'orientation': 'right',
            'todayHighlight': true
        });
        $('body').on('click', '.gender', function () {
            if ($('.gender-radio').val() == 'male') {
                $('.gender-radio').val('female')
            }
            else {
                $('.gender-radio').val('male')
            }
        });
        $('.terms-conditions-vol').click(function () {

            if ($('.terms-conditions-vol-value').val() == 0) {
                $('.terms-conditions-vol-value').val(1)
            } else {
                $('.terms-conditions-vol-value').val(0)
            }
        });
        $('.years-old-check').click(function () {
            if ($('.years-old-check-value').val() == 0) {
                $('.years-old-check-value').val(1)
            } else {
                $('.years-old-check-value').val(0)

            }
        });
        $('.terms-conditions-org').click(function () {
            if ($('.terms-conditions-org-value').val() == 0) {
                $('.terms-conditions-org-value').val(1);
            } else {
                $('.terms-conditions-org-value').val(0);
            }
        });
        $("#subOrgSel").hide();

        $("#organizationSelect").select2({
            theme: "bootstrap",
            minimumInputLength: 2
        });

        var parentOrgSel = $('#parentOrgSel');
        var subOrgSel = $("#subOrgSel");

        parentOrgSel.select2({theme: "bootstrap"}).on('change', function () {
            if (parentOrgSel.val()) {
                $('#parent_user_id').val(parentOrgSel.val());
                $("#subOrgSel").show();
                $.ajax({
                    url: API_URL + 'get-sub-organization/' + parentOrgSel.val(),
                    type: 'GET',
                    success: function (data) {
                        subOrgSel.empty();
                        subOrgSel.append($("<option></option>").attr("value", "").text("Select"));
                        $.each(data, function (value, key) {
                            subOrgSel.append($("<option></option>").attr("value", value).text(key)); // name refers to the objects value when you do you ->lists('name', 'id') in laravel
                        });
                        subOrgSel.select2({
                            theme: "bootstrap",
                            dropdownAutoWidth: true,
                            width: 'auto'
                        }).on('change', function () {
                            $('#login_user').val(subOrgSel.val());
                            $('.loginEmail').hide();
                            $('.loginPass').show();
                        }); //reload the list and select the first option
                    }
                });
            }
        }).trigger('change');

        $('.loginSub, .parentOrganizationDiv, .organizationDivS').hide();

        $('#loginAS').on('change', function () {
            $('#login_user,#login_password,#parent_user_id').val('');

            $('#login_user,#login_password,#parent_user_id').val('');

            $('.organizationDivS').hide();

            if ($(this).val() == "") {
                $('.loginSub, .parentOrganizationDiv').hide();
            } else if ($(this).val() == "suborganization") {
                $('.parentOrganizationDiv').show();
                $('.loginSub').hide();
                $('.loginPass').show();

            } else {
                $('.parentOrganizationDiv').hide();
                $('.loginSub').show();
                $(".select2-container--bootstrap .select2-selection--single", "#myModalLogin").css("border", "");

                if ($(this).val() == "organization")
                    $('.organizationDivS').show();
            }
        });
        
        $('.parentOrgRego').hide();
        $('#type-user-select-id').on('change', function () {
            $('#parent_user_id').val('');
            $('.parentOrgRego').hide();
            if ($(this).val() == "SubOrganization") {
                $('.parentOrgRego').show();
            } else {
                $('.parentOrgRego').hide();
                $(".select2-container--bootstrap .select2-selection--single", "#myModalUserTipe").css("border", "");
            }
        });
    });

</script>

    @yield('script')

</body>
@endsection
