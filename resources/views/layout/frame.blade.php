<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MyVoluntier</title>
    <meta http-quiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @if(trim($__env->yieldContent('social_meta_tags')))
        @yield('social_meta_tags')
    @else
    <!-- start meta tags for social sharing-->
        <meta property="og:title" content="MyVoluntier.com">
        <meta property="og:description"
              content="We match people and organizations to create positive community impacts.">
        <meta property="og:image" content="{{asset('front-end/img/myvoluntier-social.png')}}">
        <meta property="og:url" content="http://myvoluntier.com">
        <meta property="og:site_name" content="MyVoluntier">
        <meta name="twitter:card" content="summary_large_image">
        <!-- end meta tags for social sharing-->
    @endif
    <!-- <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"> to add pagination added this link -->
    <link href="<?=asset('front-end/css/bootstrap.css')?>" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('front-end/img/myvoluntier_social_iT3_icon.ico')}}" type="image/x-icon"/>


    <link href="{{asset('front-end/css/jquery.bxslider.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/select2-bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/jquery.bxslider.css') }}" rel="stylesheet">
    <link href="{{asset('front-end/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/bootstrap-datepicker.css')}}" rel="stylesheet" -->
    <link href="{{asset('front-end/css/bootstrap-sortable.css')}}" rel="stylesheet">
    <link href="<?=asset('front-end/css/main.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('admin/plugins/toastr/toastr.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/css/jquery-ui.css')}}">
	<link rel="stylesheet" href="{{asset('front-end/css/float-label.min.css')}}"/>
    <!--
    <script src="https://wchat.freshchat.com/js/widget.js"></script>
    -->
    <script src="https://cdn.logrocket.io/LogRocket.min.js" crossorigin="anonymous"></script>
    <script>
        if(window.attachEvent) {
            window.attachEvent('onload', loadLogRocket);
        } else {
            if(window.onload) {
                var curronload = window.onload;
                var newonload = function(evt) {
                    curronload(evt);
                    loadLogRocket(evt);
                };
                window.onload = newonload;
            } else {
                window.onload = loadLogRocket;
            }
        }
        function loadLogRocket(e){
            window.LogRocket && window.LogRocket.init('<?php echo env('LOGROCKET_KEY');?>');
            <?php if(!empty(Auth::user()->id)) { ?>
            LogRocket.identify('<?php echo Auth::user()->id;?>', {
                name: "<?php echo (!empty(Auth::user()->first_name)) ? Auth::user()->first_name . " " . Auth::user()->last_name : Auth::user()->org_name; ?>",
                email: '<?php echo Auth::user()->email; ?>',

                // Add your own custom user variables here, ie:
                subscriptionType: 'pro'
            });
            <?php } ?>
        }

    </script>

    <script type="application/javascript">

        function resizeIFrameToFitContent(iFrame) {
            if(iFrame){
                iFrame.width = iFrame.contentWindow.document.body.scrollWidth;
                iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
            }

        }

        window.addEventListener('DOMContentLoaded', function (e) {

            var iFrame = document.getElementById('iFrame1');
            resizeIFrameToFitContent(iFrame);

            // or, to resize all iframes:
            var iframes = document.querySelectorAll("iframe");
            for (var i = 0; i < iframes.length; i++) {
                resizeIFrameToFitContent(iframes[i]);
            }
        });

    </script>
    @yield('css')
</head>

@yield('body')

</html>
