@extends('layout.frame')

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">

    <style>
        .org_logo{
            height: 120px;
            width: 120px;
            float: left;
        }

        .org_info{
            padding-left: 15px;
            padding-top: 15px;
            display: inline-block;
            clear: both;
        }

        .org_info image{
            display: inline-block;
        }

        .org_info label{
            display: block;
        }

        .org_info_addr{
            margin-left: 15px;
            padding: 15px;
            float: right;
            clear: right;
        }
        .org_info_addr label{
            display: block;
        }
        .show{
            display:block;
        }
        .hide{
            display:none;
        }
    </style>
@endsection

@section('body')
    <div class="row-content">

        <div class="wrapper-profile-box">

            <div class="top-profile-info">
                <form id="upload_logo" role="form" method="post" action="{{url('api/organization/profile/upload_logo')}}"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="file" accept="image/*" name="file_logo" id="upload_logo_input" hidden="true">
                </form>


                <form id="upload_logo1" role="form" method="post"
                      action="{{url('api/organization/profile/upload_back_img')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="file" accept="image/*" name="file_logo" id="upload_logo1_input" hidden="true">
                </form>

                <div class="img-box"
                     style="background-image:url('{{$user->back_img == NULL ? asset('front-end/img/bg/0002.png') :  $user->back_img}} ')">
                </div>

                <div class="container mobile-container">
                    <div class="avatar"
                         style="background-image:url('{{$user->logo_img == NULL ? asset('front-end/img/org/001.png') : $user->logo_img}}')">
                        <span></span>
                    </div>

                    <div class="link-img"></div>
                </div>

                <div class="container">
                    <div class="row txt-box">

                        <div class="col-12 col-md-6">
                            <div class="main-text"><p class="h2">{{$user->user_role == 'organization' ? $user->org_name : $user->first_name ." ". $user->last_name}}</p></div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="main-text">

                                <ul class="list-column clearfix">
                                    <li>
                                        @if($user_role == 'organization')
                                            <p class="h2">{{count($members)}}</p>
                                            <p class="light">Members</p>
                                        @else
                                            <p class="h2">{{$friend->count()}}</p>
                                            <p class="light">Friends</p>
                                        @endif
                                    </li>

                                    <li>
                                        <p class="h2">{{$group->count()}}</p>
                                        <p class="light">Groups</p>
                                    </li>

                                    <li>
                                        @if($user_role == 'organization')
                                            <p class="h2">{{$active_oppr->count()}}</p>
                                            <p class="light">Opportunites</p>
                                        @else
                                            <p class="h2">{{$opportunity->count()}}</p>
                                            <p class="light">Opportunites</p>
                                        @endif

                                    </li>
                                </ul>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="text-profile-info">

                <div class="container">

                    @if($user->user_role === 'volunteer')
                     <div id="transcriptSection" class="{{$checkval==1? 'show':'hide'}}">
                        <div  id="filter_controller" style="padding: 10px 15px 10px 15px; background: #efefef; border: solid 1px #333;">
                            <label style="margin-bottom: 0;">Filter Results:</label>
                            <img src="{{asset('img/arrow_down.png')}}" style="float: right; width: 20px; padding-top: 7px;">
                        </div>

                        <div class="filter_div" style="padding: 15px; background: #efefef; display: none">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <label class="label-filter">Opportunity Types:</label>
                                    <div class="wrapper_select">
                                        <select class="opp-search-checkbox form-control" id="opportunity_type_select">
                                            <option value="" class="oppr_li" id="opprtypeAll">All</option>
                                            @foreach($oppr_types as $key=>$ot)
                                                <option value="{{$key}}" class="oppr_li" id="opprtype{{$key}}">{{$ot['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <label class="label-filter">Organization Types:</label>
                                    <div class="wrapper_select">
                                        <select class="org-search-checkbox form-control" id="organization_type_select">
                                            <option value="" class="oppr_li" id="orgtypeAll">All</option>
                                            @foreach($org_types as $key=>$or)
                                                <option value="{{$key}}" class="oppr_li" id="orgtype{{$key}}">{{$or['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="label-filter" style="margin-left: 15px">Date Range:</label>
                                <div class="col-12 col-md-5">
                                    <input type="text" id="transcript_start_date" name="transcript_start_date" class="form-control chose-date" placeholder="Start Date">
                                </div>
                                <div class="col-12 col-md-5">
                                    <input type="text" id="transcript_end_date" class="form-control chose-date" placeholder="End Date">
                                </div>
                                <div class="col-12 col-md-2">
                                    <a href="#" id="filter_apply" name="filter" class="btn btn-success" style="float:right";>Apply Filter</a>
                                </div>
                            </div>
                        </div>

                        <div class="report_typescript" style="padding: 15px">
                            <a href="#" name="filter_print" onclick="printData()" class="btn btn-success" style="float:right; margin-right: 10px;">Export (PDF)</a>
                        </div>

                        <div class="voluntier_info" style="display: none">
                            <Img src="{{asset('front-end/img/logo.jpg')}}" style="margin-bottom: 10px">
                            <h4><strong>Volunteer</strong></h4>
                            <div style="padding-left: 45px">
                                <label>Name: <strong>{{$user->first_name.' '.$user->last_name}}</strong></label><br>
                                <label>Contact: {{$user->contact_number}}</label><br>
                                <label>Email: {{$user->email}}</label><br>
                            </div>
                            <h4><strong>Transcript</strong></h4>
                        </div>

                        <div id="filter_content">
                            @foreach($tracks as $key=>$t)
                                <div class="typescript" style="padding: 15px;">
                                    <div id="">
                                        <img src="{{$t['org_logo'] == NULL ? asset('front-end/img/org/001.png') : $t['org_logo']}}" class="org_logo">
                                        <div class="org_info">
                                            <Label><strong>{{$t['org_name']}}</strong></Label>
                                            <Label>Phone Number: {{$t['contact_number']}}</Label>
                                            <Label>Email: {{$t['email']}}</Label>
                                            <Label>Expiration Date: {{date('Y-m-d', strtotime($shared_info->expired_at))}}</Label>
                                        </div>
                                        <div class="org_info_addr">
                                            <Label>{{$t['address']}}</Label>
                                        </div>
                                    </div>
                                    <div style="overflow: auto; clear: both;">
                                        <table id="example21" class="table sortable">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <div class="main-text"><p>Date/Time</p></div>
                                                </th>
                                                <th>
                                                    <div class="main-text"><p>Duration(hrs)</p></div>
                                                </th>
                                                <th>
                                                    <div class="main-text"><p>Opportunity Title</p></div>
                                                </th>
                                                <th>
                                                    <div class="main-text"><p>Category/Type</p></div>
                                                </th>
                                                <!--th>
                                                    <div class="main-text"><p>Expiration Date</p></div>
                                                </th-->
                                                <th>
                                                    <div class="main-text"><p>Date Validated</p></div>
                                                </th>
                                                <th>
                                                    <div class="main-text"><p>Validator Name</p></div>
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($t['track_info'] as $opp)
                                                <tr>
                                                    <td>{{$opp['logged_date']}}</td>
                                                    <td>{{$opp['logged_mins']/60}}</td>
                                                    <td>{{$opp['oppor_name']}}</td>
                                                    <!--td>{{$oppr_types[$opp['category_type']]['name']}}</td-->
													<td>{{$opp['category_names']}}</td>
                                                    <!--td>{{\App\Opportunity::find($opp['oppor_id'])->end_date}}</td-->
                                                    <td>{{$opp['confirmed_at']}}</td>
                                                    <td>{{\App\Opportunity::find($opp['oppor_id'])->contact_name}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="<?=asset('front-end/js/bootstrap-datepicker.js')?>"></script>
    <script src="<?=asset('js/plugins/datetimepicker/moment.js')?>"></script>
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>

    <script>

        var is_open = 0;
        $('#filter_controller').on('click', function (e) {
            if(is_open == 0){
                $('.filter_div').show();
                is_open = 1;
            }else {
                $('.filter_div').hide();
                is_open = 0;
            }
        });

        $('#share_transcript').on('click', function (e) {
            $('#myModal').modal('show');
        });

        $('#filter_apply').on('click', function (e) {
            e.preventDefault();
            var url = API_URL + 'profile/get_transcript';

            var id = '{{$user->id}}';

            var opp_types = $('.opp-search-checkbox').val();

            var org_types = $('.org-search-checkbox').val();

            var start_date = $('#transcript_start_date').val();

            var end_date = $('#transcript_end_date').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "GET";

            var formData = {
                id: id,
                opp_types: opp_types,
                org_types: org_types,
                start_date: start_date,
                end_date: end_date
            };

            $.ajax({

                type: type,

                url: url,

                data: formData,

                success: function (data) {
                    var contentTypeScriptString = '<div id="filter_content">';
                    $.each(data.oppr, function (index,value) {
                        contentTypeScriptString +='<div class="typescript" style="padding: 15px;"><div id="">';
                        if(value.org_logo == null){
                            contentTypeScriptString +='<img src="{{ asset('front-end/img/org/001.png')}}" class="org_logo">';
                        }else {
                            contentTypeScriptString +='<img src="' +value.org_logo+ '" class="org_logo">';
                        }
                        contentTypeScriptString +='<div class="org_info"><Label><strong>' +value.org_name+ '</strong></Label><Label>' +value.contact_number+ '</Label><Label>' +value.email+ '</Label></div>';
                        contentTypeScriptString +='<div class="org_info_addr"><Label>' +value.address+ '</Label></div></div><div style="overflow: auto; clear: both;">';
                        contentTypeScriptString +='<table id="example21" class="table sortable member-table">';
                        contentTypeScriptString +='<thead><tr><th><div class="main-text"><p>Date/Time</p></div></th><th><div class="main-text"><p>Duration(hrs)</p></div></th><th><div class="main-text"><p>Opportunity Title</p></div></th><th><div class="main-text"><p>Category/Type</p></div></th><th><div class="main-text"><p>Date Validated</p></div></th><th><div class="main-text"><p>Validator Name</p></div></th></tr></thead>';
                        contentTypeScriptString +='<tbody>';
                        $.each(value.track_info, function (index,track_value) {
                            contentTypeScriptString +='<tr><td>'+track_value.logged_date+'</td><td>'+track_value.logged_mins/60+'</td><td>'+track_value.oppor_name+'</td>';
                            var category_name = '';
                            var confirmed_at = '';
                            var confirmer_name = '';
                            if(track_value.confirmed_at != null){
                                confirmed_at = track_value.confirmed_at;
                            }
                            if(track_value.confirmer_name != null){
                                confirmer_name = track_value.confirmer_name;
                            }
                            category_name = track_value.name;
                            contentTypeScriptString +='<td>'+category_name+'</td><td>'+confirmed_at+'</td><td>'+confirmer_name+'</td></tr>';
                        });
                        contentTypeScriptString +='</tbody></table></div></div>';
                    });

                    $('#filter_content').replaceWith(contentTypeScriptString);
                },

                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        $("#transcript_end_date").prop('disabled', true);
        $('#transcript_start_date').datepicker({
            'format': 'mm-dd-yyyy',
            'autoclose': true,
            'orientation': 'right',
            'todayHighlight': true
        }).on('changeDate', function (selected) {
            var endDate = new Date(selected.date.valueOf());
            $("#transcript_end_date").prop('disabled', false);
            $('#transcript_end_date').datepicker('setStartDate', endDate);
            $('#transcript_end_date').datepicker('setDate', endDate);
        }).on('clearDate', function (selected) {
            $('#transcript_end_date').datepicker('setStartDate', '+0d');
        });

        $('#transcript_end_date').datepicker({
            'format': 'mm-dd-yyyy'
        }).on('changeDate', function (selected) {
            daterangeChange();
        });

        function printData() {
            window.print();
        }
    </script>

@endsection


