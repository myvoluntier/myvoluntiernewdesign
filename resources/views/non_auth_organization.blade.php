@extends('layout.master')
@section('css')
    <style>
        img {
            width: 70px;
        }
        a {
            text-decoration: none !important;
        }
        .pagination .flex-wrap .justify-content-center{
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .select2-container {
            width: 100% !important;
        }


    </style>
@endsection

@section('content')
    <div class="wrapper-groups_org">
        <div class="container">
            <div class="row row-padding">
                <div class="col-12">
                    <div class="box-body table-responsive">
                        <table id="example4" class="table">
                            <thead>
                                <th>No.</th>
                                <th></th>
                                <th>Name</th>
                                <th>Zipcode</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @if($list->count()>0)
                                @foreach($list as $each)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>
                                        <a href="{{url('/vieworganization/profile')}}/{{$each->id}}" data-id="{{$each->id}}" class="actnbtn">
                                        <img src="{{$each->logo_img == NULL ? asset('front-end/img/org/001.png') : $each->logo_img}}" class="org_logo">
                                        </a>
                                    </td>

                                    <td>
                                       
                                        @if($each->user_role=='organization') {{$each->org_name}}@endif
                                    </td>
                                    <td>{{$each->zipcode}}</td>
                                    <td>
                                        <a href="{{url('/vieworganization/profile')}}/{{$each->id}}" data-id="{{$each->id}}" class="actnbtn"><i class="fa fa-edit"></i></a>
                                    </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>

    

@endsection

@section('script')

    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
    $('#example4').DataTable({
             "paging": true,
             "lengthChange": false,
             "searching": true,
             "ordering": false,
             "info": true,
             "autoWidth": false
    });
    </script>
@endsection

