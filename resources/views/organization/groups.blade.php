@extends('layout.masterForAuthUser')

@section('css')

    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/jquery.bxslider.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap-sortable.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/select2-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">

    <style>
        .active_group_page {
            color: #3bb44a !important;
            background: #fff;
            text-decoration: none;
        }
        .tab_item {
            display: none;
        }

        .hide {
            display: none;
        }

        a {
            text-decoration: none !important;
        }

        .pagination .flex-wrap .justify-content-center {
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .footable-page-arrow, .footable-page {
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .footable-page-arrow a, .footable-page a {
            position: relative;
            display: block;
            padding: 10px;
            margin: 10px 5px;
            font-size: 16px;
            line-height: 20px;
            font-family: 'Open Sans', sans-serif;
            color: #5f5f5f;
            background: none;
            border: 0 none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-decoration: none;
            cursor: auto;
            outline: none;
            z-index: auto;
        }

        .footable-page-arrow a:hover, .footable-page a:hover {
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }


        .footable-page:nth-child(4), .footable-page:nth-last-child(4), .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled {
            display: none;
        }

        .footable-page.active a, .footable-page-arrow.disabled:first-child, .footable-page-arrow.disabled:last-child {
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
            display:block;
        }
        input[type=text] {
            width: 130px;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;
            /*background-image: url('public/img/searchicon.png');*/
            background-position: 10px 10px;
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }

        .contents {
            display: none;
        }

        .preload {
            margin-top: 10%;
        }

        .print-header {
            display: none !important;
        }

    </style>

    @if(Auth::user()->user_role !== 'organization')

        <style>
            #sendFrm ul{
                position: relative;
                text-align: left;
            }
            ul {
                list-style-type: none;
            }

        </style>
    @endif

@endsection

@section('content')
    <div class="preload">
        <img class="rounded mx-auto d-block" src="{{asset('front-end/css/images/spinner.gif')}}">
    </div>

    <div class="wrapper-groups_org contents" style="padding-top: 0px;">
        <div class="container">
            <div class="row">
                <div class="col print-header">
                    <img src="{{asset('front-end/img/logo.png')}}">
                    <span>Groups Impact</span>
                </div>
            </div>
            <div class="container">    
                <div class="row row-padding new-group-page">
                    <div class="col-12 col-md-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6 main-text add-group-btn">
                                <div class="row flex-container">
                                    <div class="noPrint">
                                        <h2 class="h2">Add Group</h2>
                                    </div>

                                    <div class="plus-group-btn">
                                        <a href="{{Auth::user()->user_role == 'organization' ? route('organization-group-group_add') : route('volunteer-group-group_add')}}"
                                        class="add_opportunity_plus"><span class="fa fa-plus"></span></a>
                                    </div>
                                </div>
                                @if(session('admin_role')=='admin' && env('APP_ENV') == 'local' )
                                    <a href="{{route('addTestGroupsData')}}"><i class="fa fa-refresh" aria-hidden="true"> Load Test Data</i></a>
                                @endif
                            </div>
                            <div class="offset-lg-2 offset-md-2 col-lg-6 col-md-6 col-sm-6 col-6 add-group-search">
                                <input type="text" name="search" placeholder="Search Group..." style="width:100%;" onkeyup="search()" id="myInput"/>
                                <i class="fa fa-search"></i>
                            </div> 
                        </div>       

                        <div class="link-box">
                            <ul id="tabs" class="tabs_groups nav nav-tabs" role="tablist">
                                @if(count($groupList)>0)
                                <div class="inner-tablist-wrap">    
                                    <ul id="myUL">
                                        @foreach($groupList as $key => $lists)
                                        <li>
                                        <div class="row group-row"> 
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-12 groupername">
                                                <div data-groupTab='{{$key}}' class="groupTab tab_groups nav-item" id="pane-li-{{$key}}">
                                                <a id="tab-{{$key}}" href="#pane-{{$key}}" data-toggle="tab" role="tab">{{ $lists->name }}</a>
                                                    <div class="impact-text">
                                                        <p><b>IMPACTS:</b></p>
                                                        <p>{{number_format($lists->tracked_hours/60,1)}}</p>
                                                        <p>Hour(s) </p>
                                                    </div>
                                                </div>
                                                <!-- <div class="wrapper-friends  shift-15">
                                                    <div class="search-friends">
                                                        <div class="container">
                                                            <div class="search send_invitation clearfix" data-id="{{$lists->id}}">
                                                                <input type="text" name="invite_name" class="invite_name" placeholder="Type name to invite">
                                                                <a href="#" class="send_invitation send_invitation_user send"><i class="fa fa-envelope-square"></i>
                                                            
                                                                </a>
                                                            </div>
                                                            <span class="input-group-btn" style="display: none">
                                                                <input type="button" value="Send Invitation" class="btn btn-primary send">
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div> -->
                                            
                                            <div class="modal fade" id="send_invitation_modal_{{$lists->id}}"  role="dialog" aria-hidden="true">
                                              <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
                                                <div class="modal-dialog-fix">
                                                    <div>
                                                        <div class="modal-content">                   
                                                            <div class="modal-header">
                                                                <div class="main-text">
                                                                    <h2 class="h2">Send Invitation</h2>
                                                                    <!-- @if(Session::has('success'))
                                                                        <div class="alert alert-success">{{Session::get('success')}}</div>
                                                                    @endif
                                                                    @if(Session::has('error'))
                                                                        <div class="alert alert-danger">{{Session::get('error')}}</div>
                                                                    @endif      -->
                                                                    <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span>&times;</span>
                                                                    </a>
                                                                    <div id="inviteError_{{$lists->id}}" class="alert alert-danger" role="alert" style="display:none"></div>
                                                                    <div id="inviteSuccess_{{$lists->id}}" class="alert alert-success" role="alert" style="display:none"></div>
                                                                </div>
                                                            </div>
                                                                <div class="modal-body">
                                                                        <div class="wrapper-friends  shift-15">
                                                                            <div class="search-friends">
                                                                                <div class="container">
                                                                                    <div class="search send_invitation clearfix" data-id="{{$lists->id}}">
                                                                                        <input type="text" name="invite_name" class="invite_name" placeholder="Type name to invite">
                                                                                        <a href="#" class="send_invitation send_invitation_user send"><i class="fa fa-envelope-o"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                    <span class="input-group-btn" style="display: none">
                                                                                        <input type="button" value="Send Invitation" class="btn btn-primary send">
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                 </div>
                                              </div>
                                            </div>
                                        </div>  
                                            
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-12 group-btn">
                                                <div class="nav-item">
                                                    <!-- <a class="nav-link active show" href="#members{{$key}}" role="tab" data-toggle="tab" aria-selected="false"><span>Old Members</span></a> -->
                                                    <a title="Send Invitation" data-toggle="modal" data-target="#send_invitation_modal_{{$lists->id}}" class="nav-link" href="#"><i class="fa fa-envelope-o"></i>
                                                    <!-- <span>Members</span> -->
                                                </a>
                                                </div>    
                                                <div class="nav-item">
                                                    <!-- <a class="nav-link active show" href="#members{{$key}}" role="tab" data-toggle="tab" aria-selected="false"><span>Old Members</span></a> -->
                                                    <a class="nav-link" title="Members" href="{{route('organization-group-member',['groupId'=>$lists->id])}}" target="_blank"><i class="fa fa-users"></i>
                                                    <!-- <span>Members</span> -->
                                                </a>
                                                </div>
                                                <div class="nav-item">
                                                    <!-- <a class="nav-link" href="#impact{{$key}}" role="tab" data-toggle="tab" aria-selected="false"><span>old Impact</span></a> -->
                                                    <a class="nav-link" title="Impact" href="{{route('organization-group-impact',['groupId'=>$lists->id])}}" target="_blank"><i class="fa fa-signal"></i>
                                                    <!-- <span>Impact</span> -->
                                                </a>

                                                </div>
                                                @if($lists->role_id==1)
                                                <div class="nav-item">
                                                    <a title="Edit" href="{{Auth::user()->user_role == 'organization' ? route('organization-group-group_add', [$lists->id]) : route('volunteer-group-group_add', [$lists->id])}}"
                                                        class="nav-link main-link">
                                                        <i class="fa fa-edit"></i>
                                                        <!-- <span>Edit</span> -->
                                                    </a>
                                                </div>   
                                                <div class="nav-item">     
                                                    @if(count($lists->members)>0 && Auth::user()->user_role == 'organization')
                                                        <a title="Export Members" class="nav-link main-link" id="export_member" href="{{url('/organization/export-group-members/'.$lists->id) }}"><i class="fa fa-download"></i>
                                                        <!-- <span>Export Members</span> -->
                                                    </a>
                                                    @endif
                                                </div>
                                            @else
                                                <div class="col col-auto nav-item">
                                                    <a onclick="return confirm('Are you sure to leave ?')"
                                                        class="nav-link main-link"
                                                    @if(Auth::user()->user_role == 'organization')
                                                        href="{{url('organization/leave_group/'.$lists->id)}}"> <i class="fa fa-sign-out"></i></a>
                                                    @else
                                                        href="{{url('volunteer/leave_group/'.$lists->id)}}
                                                        "><i class="fa fa-sign-out"></i></a>
                                                    @endif
                                                </div>
                                                @endif
                                                @if($lists->role_id==1 && $lists->is_public!=2)
                                                        
                                                        @endif

                                                        <!-- <div class="banner-text-box  shift-15">
                                                            <div class="row no-gutters">
                                                                <div class="col-12 col-md-8"
                                                                    @if($lists->banner_image!='')style="background-image: url('{{$lists->banner_image}}')"@endif>
                                                                </div>
                                                                <div class="col-12 col-md-4">
                                                                    <div class="main-text">
                                                                        <p>IMPACTS</p>
                                                                        <p class="bold">{{number_format($lists->tracked_hours/60,1)}}</p>
                                                                        <p class="light">HOUR(S) </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                            </div>            
                                        </li>            
                                        @endforeach
                                    </ul> 
                                </div>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script src="{{asset('front-end/js/highcharts.js')}}"></script>
    {{--<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>--}}
    <script src="{{asset('front-end/js/bootstrap-sortable.js')}}"></script>
    <script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>
    <script src="//cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.19/pagination/ellipses.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>

    <script>

        $(document).ready(function () {

            var clipboard = new ClipboardJS('.btn');

            clipboard.on('success', function (e) {
                toastr.success("Share URL Copied.", "Message");
                e.clearSelection();
            });

            var $outerwidth = $('.row-header header .outer-width-box');
            var $innerwidth = $('.row-header header .inner-width-box');

            function checkWidth() {

                var outersize = $outerwidth.width();
                var innersize = $innerwidth.width();

                if (innersize > outersize) {
                    $('body').addClass("navmobile");
                } else {
                    $('body').removeClass("navmobile");
                    $('body').removeClass("offcanvas-menu-show");
                }
            }

            checkWidth();
            $(window).resize(checkWidth);

            $('.offcanvas-menu-backdrop').on('click', function (e) {
                $('body').toggleClass("offcanvas-menu-show");
                e.preventDefault();
            });

            $('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
                $('body').toggleClass("offcanvas-menu-show");
                e.preventDefault();
            });

            var $outerwidth = $('.row-header header .outer-width-box');
            var $innerwidth = $('.row-header header .inner-width-box');


            function checkWidth() {

                var outersize = $outerwidth.width();
                var innersize = $innerwidth.width();

                if (innersize > outersize) {
                    $('body').addClass("navmobile");

                } else {
                    $('body').removeClass("navmobile");
                    $('body').removeClass("offcanvas-menu-show");
                }

            }

            checkWidth();
            $(window).resize(checkWidth);

            $('.offcanvas-menu-backdrop').on('click', function (e) {
                $('body').toggleClass("offcanvas-menu-show");
                e.preventDefault();
            });

            $('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
                $('body').toggleClass("offcanvas-menu-show");
                e.preventDefault();
            });


            $('#tab-0').click();
        });

        $('.tab_item:eq(' + 0 + ')').show();

        // console.log($('.tab_groups:eq(' + 0 + ')').children('a').children('span').addClass('active_group_page'))
        $('.tab_groups:eq(' + 0 + ')').children('a').children('span').css('color', '#3bb44a');

        $(document).ready(function () {

            $('.example3').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "pageLength": 10,
                "pagingType": "full_numbers",
                "language": {
                    "paginate": {
                        "first": '<<',
                        "next": '>',
                        "previous": '<',
                        "last": '>>'
                    }
                }
            });

            $("#DataTables_Table_0_wrapper").addClass('wrapper-pagination')

            $('body').on('click', '.send', function (e) {
                e.preventDefault();
                if($("input[name='list_user_id']:checked").val()){
                 $("#volError").hide();
                // if ($("input[type='checkbox']:checked").length > 0) {
                //     var id = $(this).attr('value');
                $.ajax({
                  type:'post',
                  url: "{{ Auth::user()->user_role == 'organization' ? route('org_add_user_invitation') : route('vol_add_user_invitation') }}",
                  data: {
                     "_token" : $('meta[name=_token]').attr('content'),
                     "data": $("#sendFrm").serializeArray(),
                       },
                  success: function (response) {
               
                 if(response.error){
                    $("#inviteSuccess_"+response.group_id).hide();
                    $("#inviteError_"+response.group_id).html(response.error); 
                    $("#inviteError_"+response.group_id).show();
                 }else if(response.success){
                    $("#inviteError_"+response.group_id).hide();
                    $("#inviteSuccess_"+response.group_id).html(response.success); 
                    $("#inviteSuccess_"+response.group_id).show();
                 }else if(response.error && response.redirect){
                        $("#inviteSuccess_"+response.group_id).hide();
                        $("#inviteError_"+response.group_id).html(response.error); 
                        $("#inviteError_"+response.group_id).show();  
                        window.location.replace(BASE_URL+response.redirect);
                 }else{
                        window.location.replace(BASE_URL+response.redirect);
                 }    
                  },
                error: function(xhr, type, exception) { 
                    // if ajax fails display error alert
                    console.log("ajax error response type "+type);
                }
                      });                 
                   
                }else{
                $("#volError").addClass(".error");
                $("#volError").show();
                }
            });

            $(document).on("submit", ".addGroupInvitation", function(e) {
                    e.preventDefault();
                  
                    var group_id = $('.searchResult').data("id");
                    var id= $(this).find('input:hidden[name=id]').val();
                    var name = $(this).find('input:hidden[name=name]').val();
                    $.ajax({
                  type:'post',
                  url:BASE_URL+'api/organization/group/add_group_invitation',
                  data: {
                     "_token" : $('meta[name=_token]').attr('content'),
                      "id":id,
                      "name":name
                  },
                  success: function (response) {
                    if(response.error){                       
                    $("#inviteSuccess_"+group_id).hide();
                    $("#inviteError_"+group_id).html(response.error); 
                    $("#inviteError_"+group_id).show();
                 }else if(response.success){                   
                    $("#inviteError_"+group_id).hide();
                    $("#inviteSuccess_"+group_id).html(response.success); 
                    $("#inviteSuccess_"+group_id).show();
                 }else if(response.error && response.redirect){
                        $("#inviteSuccess_"+group_id).hide();
                        $("#inviteError_"+group_id).html(response.error); 
                        $("#inviteError_"+group_id).show();  
                        window.location.replace(BASE_URL+response.redirect);
                 }else{
                       window.location.replace(BASE_URL+response.redirect);            
                     } 
                  },
                error: function(xhr, type, exception) { 
                    // if ajax fails display error alert
                    console.log("ajax error response type "+type);
                }
                      });
                });

            $('.tab_groups').on('click', function (e) {
                e.preventDefault();
                var form = $('#sendFrm');
                if (form != undefined & form != null) {
                    $('#sendFrm').remove();
                    $('.invite_name').val('');
                }
            })

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            var previous_index = 0;

            $(".tab_groups").click(function (e) {

                e.preventDefault()
                $(".tab_groups").eq(previous_index).removeClass("active_group_page");
                $(".tab_groups").eq(previous_index).children('a').removeClass("active");
                $('.group_name_class').css('color', '#28292e');
                $(".tab_groups").eq($(this).index()).addClass("active_group_page");
                $(".tab_item").hide().eq($(this).index()).hide();

                $('.tab_item:eq(' + $(this).index() + ')').show();
                $(this).children('a').children('span').css('color', '#3bb44a');
                previous_index = $(this).index();

            }).eq(0).addClass("active_group_page");

            $('.invite_name').keyup(function () {
                if ($(this).val().length > 2) {//alert($(this).val().length);
                    var this1 = $(this);
                    $.ajax({
                        url: "{{ Auth::user()->user_role == 'organization' ? route('api-organization-group-get-user') : route('api-volunteer-group-get-user') }}",
                        data: 'keyword=' + $(this).val() + '&groupId=' + $(this).parent().data('id'),
                        type: 'POST',
                        success: function (res) {
                            this1.parent().next().html(res);
                            this1.parent().parent().next().show();
                            var container = this1.parent().parent()[0];
                            $(container).children('span').show()
                        }
                    })
                }
                else {
                    $(this).parent().next().html('');
                }
            });
               
              

            $(".search-member-group").click(function (e) {
                e.preventDefault();
                var key = $(this).data('key-id');
                var input = $(this).siblings()[0];
                $('.search-members-tr').remove()
                if ($(input).val() !== "") {

                    var this1 = $(this);
                    $.ajax({
                        url: "{{Auth::user()->user_role == 'organization' ?  route('get_user_by_group') : route('volunteer-get-user-by-group') }}",
                        data: 'keyword=' + $('#search-member-group-input' + key).val() + '&groupId=' + $('#search-member-group-input' + key).data('id'),
                        type: 'POST',
                        success: function (res) {
                            $('#members-list-main' + key).hide();
                            $('#members-list-search' + key).show();

                            $('#members-list-search' + key).append(res)
                        }
                    })
                }

                else {
                    $('#members-list-search' + key).hide();
                    $('#members-list-main' + key).show();
                }

            });

        });

        $('.invi_link').click(function () {
            var dis = $(this).data('id');
            if ($('#invi_div' + dis).hasClass('hide')) {
                $('#invi_div' + dis).removeClass('hide');
            } else {
                $('#invi_div' + dis).addClass('hide')
            }
            console.log($(this).data('id'))
        });

        function search() {

            var input, filter, ul, li, a, i, span;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            ul = document.getElementById("myUL");
            li = ul.getElementsByTagName("li");

            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                //span = li[i].getElementByTagName("span");
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }

        $(function() {
            $(".preload").fadeOut(2000, function() {
                $(".contents").fadeIn(1000);
            });
        });

    </script>

@endsection
