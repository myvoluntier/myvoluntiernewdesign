@extends('layout.masterForAuthUser')

@section('css')

    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/jquery.bxslider.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap-sortable.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/select2-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">

    <style>
        .active_group_page {
            color: #3bb44a !important;
            background: #fff;
            text-decoration: none;
        }
        .tab_item {
            display: none;
        }

        .hide {
            display: none;
        }

        a {
            text-decoration: none !important;
        }

        .pagination .flex-wrap .justify-content-center {
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .footable-page-arrow, .footable-page {
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .footable-page-arrow a, .footable-page a {
            position: relative;
            display: block;
            padding: 10px;
            margin: 10px 5px;
            font-size: 16px;
            line-height: 20px;
            font-family: 'Open Sans', sans-serif;
            color: #5f5f5f;
            background: none;
            border: 0 none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-decoration: none;
            cursor: auto;
            outline: none;
            z-index: auto;
        }

        .footable-page-arrow a:hover, .footable-page a:hover {
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }


        .footable-page:nth-child(4), .footable-page:nth-last-child(4), .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled {
            display: none;
        }

        .footable-page.active a, .footable-page-arrow.disabled:first-child, .footable-page-arrow.disabled:last-child {
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
            display:block;
        }
        input[type=text] {
            width: 130px;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;
            /*background-image: url('public/img/searchicon.png');*/
            background-position: 10px 10px;
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }

        .contents {
            display: none;
        }

        .preload {
            margin-top: 10%;
        }

        .print-header {
            display: none !important;
        }

    </style>

    @if(Auth::user()->user_role !== 'organization')

        <style>
            #sendFrm ul{
                position: relative;
                text-align: left;
            }
            ul {
                list-style-type: none;
            }

        </style>
    @endif

@endsection

@section('content')
    <div class="preload">
        <img class="rounded mx-auto d-block" src="{{asset('front-end/css/images/spinner.gif')}}">
    </div>

    <div class="wrapper-groups_org contents new-grp-member">
        <div class="container">
            <div class="row">
                <div class="col print-header">
                    <img src="{{asset('front-end/img/logo.png')}}">
                    <span>Groups Impact</span>
                </div>
            </div>
            @if(Session::has('success'))
                        <div class="alert alert-success">{{Session::get('success')}}</div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">{{Session::get('error')}}</div>
                    @endif
                    <div class="main-text">
                        @if(session('admin_role')=='admin' && env('APP_ENV') == 'local' )
                            <a href="{{route('addTestGroupsData')}}"><i class="fa fa-refresh" aria-hidden="true"> Load Test Data</i></a>
                        @endif
                    </div>
                <div>
                        @if(count($groupList)>0)
                            @foreach($groupList as $lists)
                                 @if(count($lists->members)>0)

                                         <div id="members{{$groupId}}">
                                                 <div class="main-text">
                                                            <div class="wrapper-sort-table tesposive-table">
                                                                <div>
                                                                <table class="table sortable example3">
                                                                    <div class="heading-mobile-view">
                                                                        <p>Member Details</p>
                                                                    </div>
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col">
                                                                            <div class="main-text"><p>Name</p></div>
                                                                        </th>
                                                                        <th scope="col">
                                                                            <div class="main-text"><p>Location</p>
                                                                            </div>
                                                                        </th>
                                                                        <th scope="col">
                                                                            <div class="main-text"><p>Impact (Hours)</p>
                                                                            </div>
                                                                        </th>
                                                                        <th scope="col">
                                                                            <div class="main-text"><p>Rating</p>
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="members-list-main{{$groupId}}">
                                                                    @foreach($lists->members as $members)
                                                                        <tr class="for-desktop-view">
                                                                            <td data-label="Name">{{($members->user_role == 'organization') ? $members->org_name : $members->first_name.' '.$members->last_name}}</td>
                                                                            <td data-label="Location">{{implode(', ', array_filter([$members->city, $members->state]))}}</td>
                                                                            <td data-label="Impact" class="impact">
                                                                                <p>{{$members->impact/60}}</p>
                                                                            </td>
                                                                            <td data-label="Rating">
                                                                            <!-- {{-- {{empty($members->mark) ? 0 : $members->mark}} --}} -->
                                                                            @php $rating = $members->mark; @endphp  
                                                                              @foreach(range(1,5) as $i)
                                                                                @if($rating > 0 && $rating < 1)
                                                                                 <span class="fa fa-star-half-o"></span>
                                                                                @elseif($rating >= 1)
                                                                                 <span class="fa fa-star checked"></span>
                                                                                @else
                                                                                 <span class="fa fa-star-o"></span>
                                                                                @endif
                                                                                @php $rating--; @endphp
                                                                             @endforeach

                                                                             
                                                                            </td>
                                                                        </tr>

                                                                        <tr class="for-mobile-view">
                                                                            <td colspan="2" data-label="Name">{{($members->user_role == 'organization') ? $members->org_name : $members->first_name.' '.$members->last_name}}</td>
                                                                            <td data-label="Rating">
                                                                                <!-- {{--  {{empty($members->mark) ? 0 : $members->mark}} --}} -->
                                                                                                                                                                
                                                                                @php $rating = $members->mark; @endphp  
                                                                                @foreach(range(1,5) as $i)
                                                                                  @if($rating > 0 && $rating < 1)
                                                                                   <span class="fa fa-star-half-o"></span>
                                                                                  @elseif($rating >= 1)
                                                                                   <span class="fa fa-star checked"></span>
                                                                                  @else
                                                                                   <span class="fa fa-star-o"></span>
                                                                                  @endif
                                                                                  @php $rating--; @endphp
                                                                               @endforeach
                                                                                                                                                               
                                                                            </td>
                                                                            <td data-label="Location">{{implode(', ', array_filter([$members->city, $members->state]))}}</td>
                                                                            <td data-label="Impact"><span class="fa fa-clock-o"></span>
                                                                                {{$members->impact/60}}
                                                                                    h
                                                                            </td> 
                                                                        </tr>

                                                                        
                                                                    @endforeach

                                                                    </tbody>
                                                                    <tbody style="display: none"
                                                                            id="members-list-search{{$groupId}}">
                                                                    </tbody>
                                                                </table>

                                                                </div>

                                                            </div>
                                                            <!-- <div class="wrapper-pagination">
                                                                <ul class="pagination flex-wrap justify-content-center">
                                                                </ul>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                @endif      
                                        </div>
                                    @endforeach
                                @endif
        </div>
    </div>

@endsection

@section('script')

    <script src="{{asset('front-end/js/highcharts.js')}}"></script>
    {{--<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>--}}
    <script src="{{asset('front-end/js/bootstrap-sortable.js')}}"></script>
    <script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>
    <script src="//cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.19/pagination/ellipses.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>

    @include('_groups.groups-charts-custom-js')

<script>
$(document).ready(function () {
    $('.example3').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "pageLength": 40,
                "pagingType": "full_numbers",
                "language": {
                    "paginate": {
                        "first": '<<',
                        "next": '>',
                        "previous": '<',
                        "last": '>>'
                    }
                }
            });
});     
$(function() {
    $(".preload").fadeOut(2000, function() {
        $(".contents").fadeIn(1000);
    });
});
</script>

@endsection
