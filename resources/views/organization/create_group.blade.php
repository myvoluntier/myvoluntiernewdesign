@extends('layout.masterForAuthUser')

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/main.css')}}">
    <link rel="stylesheet" href="https://foliotek.github.io/Croppie/croppie.css">

    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">--}}
    <link href="{{asset('css/jQuery-QueryBuilder/query-builder.default.min.css')}}" rel="stylesheet" />

    <style> 
        label.cabinet{
            display: block;
            cursor: pointer;
        }

        label.cabinet input.file{
            position: relative;
            height: 100%;
            width: auto;
            opacity: 0;
            -moz-opacity: 0;
            filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            margin-top:-30px;
        }

        #upload-logo{
            width: 400px;
            height: 400px;
            padding-bottom:25px;
        }

        #upload-banner{
            width: 800px;
            height: 400px;
            padding-bottom:25px;
        }

        figure figcaption {
            position: absolute;
            bottom: 0;
            color: #fff;
            width: 100%;
            padding-left: 9px;
            padding-bottom: 5px;
            text-shadow: 0 0 10px #000;
        }

        #cropBannerImagePop .modal-dialog{
            max-width: 900px !important;
        }
    </style>

@endsection

@section('content')
    <?php header("Access-Control-Allow-Origin: *"); ?>
    <div class="wrapper-opportunities">
    <div class="add-new-box">
        <div class="container">
        <div class="row align-items-center">
        <div class="col-12 col-md-6 col-lg-4 text-left"><h2 class="h2">{{((if_route_pattern('volunteer-group-group_add') or if_route_pattern('organization-group-group_add') ) and if_route_param('id', null)) ? 'Creating New Group' : 'Edit Group'}}</h2>
        </div>
                <div class="col-12 col-md-6 col-lg-8">
                @if(!empty($group_id) && !empty($group_info->affiliated_org_id))
                    <h4 style="float:right;">{{($group_info->affiliated_status == '1') ? 'Active' : 'Inactive'}}</h4>
                @endif
                <p class="add-new-para text-right">{{((if_route_pattern('volunteer-group-group_add') or if_route_pattern('organization-group-group_add') )  and if_route_param('id', null)) ? 'You can create a new Group right now. Fill out the information below and click "Create Group".' : 'You can edit your Group here. Only an Administrator can edit this page.'}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper-tablist">
        <div class="container">
            <div class="form-group">
                <div class="main-text">
                    <h3 class="h3">Group Info</h3>
                </div>
            </div>

            <div class="tab-content-1" id="myTabContent">
                <form  class="float-label" id="post_group_form" role="form" method="post"
                      @if(Auth::user()->user_role == 'organization' )
                      @if($group_id == null)
                      action="{{url('api/organization/group/create_org_group')}}"
                      @else
                      action="{{url('api/organization/group/change_org_group')}}"
                      @endif
                      @else
                      @if($group_id == null)
                      action="{{url('api/volunteer/group/create_vol_group')}}"
                      @else
                      action="{{url('api/volunteer/group/change_org_group')}}"
                      @endif
                      @endif
                      enctype="multipart/form-data">
                    <input type="hidden" id="banner_size" value="0">

                    {{csrf_field()}}

                    <div class="row">
                    <div class="col-12 col-md-12">

<div class="form-group text-right addgrp-banner">
    @if($group_id == null)
        <img class="img-responsive img-thumbnail upload-img-banner"
             src="{{asset('front-end/img/upload/default-banner-img.jpg')}}"
             id="item-img-output-banner"/>
    @else
        @if($group_info->banner_image != null)
            <img class="img-responsive img-thumbnail upload-img-banner" src="{{$group_info->banner_image}}"
                 id="item-img-output-banner"/>
        @else
            <img class="img-responsive img-thumbnail upload-img-banner"
                 src="{{asset('front-end/img/upload/default-banner-img.jpg')}}"
                 id="item-img-output-banner"/>
        @endif
    @endif
        <div class="wrapper-file-upload upload-banner-camera">
            <input type="file" id="inputBanner" accept="image/*" name="file_banner">
            <input type="hidden" name="image_banner" id="image_banner">
            <a href="#"><i class="fa fa-camera"></i></a>
        </div>
        <div class="add-grp-profileimg">
        <div class="pos-upload-logo">
                                @if($group_id == null)
                                    <img class="img-responsive img-thumbnail upload-img-banner"
                                         src="{{asset('front-end/img/upload/default-logo-img.jpg')}}" id="item-img-output"/>
                                @else
                                    @if($group_info->logo_img == null)
                                        <img class="img-responsive img-thumbnail upload-img-banner"
                                             src="{{asset('front-end/img/upload/default-logo-img.jpg')}}" id="item-img-output"/>
                                    @else
                                        <img class="img-responsive img-thumbnail upload-img-banner" src="{{$group_info->logo_img}}"
                                             id="item-img-output"/>
                                    @endif

                                @endif

                                <div class="addgrp-upload-logo">
                                    <div class="wrapper-file-upload">
                                        <input name="file_logo" type="file" accept="image/*" id="inputImage">
                                        <input type="hidden" name="image_logo" id="image_logo">
                                        <a href="#"><i class="fa fa-camera"></i></a>
                                    </div>
                                </div>
        </div>
        </div>
</div>

</div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group has-float-label">
                                    <input type="text" id="group_name" name="group_name" value="{{ $group_id !== null ?  $group_info->name  : ""}}" placeholder="Group name">
                                           <label class="label-text" for="group_name">Group Name:<span class="required-field"></span></label>
                                    <div style="color:red" id="nameError"></div>      
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="control form-group select-text select-without-text mb-30">
                                    <select id="group_type" name="group_type">
                                            <option value="1" {{$group_id && $group_info->is_public==1 ? 'selected' : ''}} >Public </option>
                                            <option value="0" {{$group_id && $group_info->is_public==0 ? 'selected' : ''}}> Private </option>
                                            @if(Auth::user()->user_role == 'organization')
                                                <option value="2" {{$group_id && $group_info->is_public==2 ? 'selected' : ''}}> Dynamic </option>
                                            @endif
                                    </select>
                                    <label class="label-text">Group Type:</label>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group checkbox-radio-form auto_accept_join_cls">
                                <div class="form-check form-group">
                                    <label class="form-check-label">
                                        Auto Accept Group Joins
                                        <input class="form-check-input" type="checkbox" name="auto_accept_join"
                                               {{@$group_info && $group_info->auto_accept_join==1 ? 'checked' : ''}} id="auto_accept">
                                               <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group checkbox-radio-form private_share_able_cls {{ isset($group_info) && $group_info->is_public==0 ? '' : 'd-none'}}">
                                <div class="form-check form-group">
                                    <label class="form-check-label">
                                        Shareable?
                                        <input class="form-check-input" type="checkbox" name="is_share_able"
                                               {{@$group_info && $group_info->is_public==0 && $group_info->is_share_able==1 ? 'checked' : ''}} id="share_able">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group private_passcode_cls"
                                 style="display: {{@$group_info && $group_info->is_public==0 && $group_info->is_share_able==1 ? 'block' : 'none'}}">
                                <div class="wrapper_input">
                                    <input placeholder="Enter Pass Code To Join Private Group"
                                           value="{{@$group_info ? $group_info->private_passcode : ''}}" type="text"
                                           minlength="4"
                                           maxlength="15" name="private_passcode" class="form-control"
                                           id="private_passcode">
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-12" style="padding-bottom:20px">
                            <div id="dynamicGroup" class="{{@$group_info && $group_info->is_public==2 ? '' : 'd-none'}}">
                                @if(Auth::user()->user_role == 'organization')
                                    <div class="">
                                        <div class="form-group select-without-text">
                                            <div style="font-size:14px">Select applicable domain(s)</div>
                                            <div class="wrapper_select">
                                                <select name="domains[]" class="custom-dropdown form-control domain" multiple>
                                                    <option disabled>Select Domains</option>
                                                    @foreach($verifiedDomains as $userDomain)
                                                        @if($userDomain->domain_1_confirm_code == 1)
                                                            <option value="{{$userDomain->domain_1}}" {{in_array($userDomain->domain_1 ,$domains_array) ? 'selected' : ''}}>{{$userDomain->domain_1}}</option>
                                                        @endif

                                                        @if($userDomain->domain_2_confirm_code == 1)
                                                            <option value="{{$userDomain->domain_2}}" {{in_array($userDomain->domain_2 ,$domains_array) ? 'selected' : ''}}>{{$userDomain->domain_2}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" id="rules" name="rules" value=""/>
                                    <br/>

                                    <div class="{{@$group_info && $group_info->is_public==2 ? '' : 'disableControl'}}" id="uiGroup">
                                        <div id="builder"></div>
                                        <button class="btn btn-warning" id="btn-reset" style="margin-left: 3px">Reset</button>
                                    </div>

                                    @if($group_id)
                                        <a class="btn btn-success float-right" data-toggle="collapse" href="#collapseMembers" role="button" aria-expanded="false" aria-controls="collapseMembers">
                                            View Members
                                        </a>
                                        <br>
                                    @endif
                                    @if(isset($group_info))
                                        <div class="collapse" id="collapseMembers">
                                            <div class="card card-body">
                                                <div class="main-text">
                                                    <div class="wrapper-sort-table wrapper-table-org">
                                                        <div>
                                                            <table class="table sortable example3">
                                                                <thead>
                                                                <tr>
                                                                    <th>
                                                                        <div class="main-text"><p>Name</p></div>
                                                                    </th>
                                                                    <th>
                                                                        <div class="main-text"><p>Location</p>
                                                                        </div>
                                                                    </th>
                                                                    <th>
                                                                        <div class="main-text"><p>Impact</p>
                                                                        </div>
                                                                    </th>
                                                                    <th>
                                                                        <div class="main-text"><p>Rating</p>
                                                                        </div>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>

                                                                @foreach($group_info->getGroupMembersData() as $member)
                                                                    <tr>
                                                                        <td>{{$member->first_name.' '.$member->last_name}}</td>
                                                                        <td>{{implode(', ', array_filter([$member->city, $member->state]))}}</td>
                                                                        <td>
                                                                            <p class="green">{{$member->impact/60}}
                                                                                hour(s)</p></td>
                                                                        <td>{{empty($member->mark) ? 0 : $member->mark}}</td>
                                                                    </tr>
                                                                @endforeach

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <a target="_blank" href="{{ route('share.group' ,base64_encode($group_info->id))}}" class="btn btn-danger btn-sm float-right">View More</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-md-6" id="group_category_div">
                            <div class="control form-group select-text select-without-text">
                                    <select name="group_category_id"
                                            id="group_category_id">
                                        @foreach($groupCategory as $category)
                                            <option value="{{$category->id}}" {{ isset($group_info->group_category_id) && $group_info->group_category_id==$category->id ? 'selected' : ''}} >{{$category->name}} </option>
                                        @endforeach
                                    </select>
                                    <label class="label-text">Group Category:</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="control form-group select-text select-without-text">
                                    <select name="affiliated_org_id"
                                            id="affiliated_org_id" {{isset($group_id)? 'disabled' : ''}}>
                                        <option value=""></option>
                                        @foreach($affiliatedOrgs as $key=>$friend)
                                            <option value="{{$key}}" {{isset($group_info->affiliated_org_id) && $group_info->affiliated_org_id==$key ? 'selected' : ''}} >{{$friend}} </option>
                                        @endforeach
                                    </select>
                                    <label class="label-text" style='margin-bottom:5px'>Affiliated Organization:</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                    <div class="label-block"><label class="label-text">Group Description:<span class="required-field"></span></label></div>
                        <div class="wrapper_input">
                            <textarea class="form-control" name="description" id="description"
                                      placeholder="">{{$group_id !== null ? $group_info->description : "" }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="main-text">
                            <h3 class="h3">Contact Info</h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="form-group has-float-label">
                                <div class="wrapper_input">
                                    <input type="text" id="contact_name" name="contact_name" 
                                           value="{{ Auth::user()->user_role == 'organization' ? Auth::user()->org_name :  Auth::user()->first_name}}"
                                           placeholder="Contact name">
                                    <label class="label-text" for="contact_name">Contact Name:</label>
                                </div>    
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group has-float-label"> 
                                <div class="wrapper_input">
                                    <input type="text" id="contact_email" name="contact_email" 
                                           value="{{Auth::user()->email}}" placeholder="Contact email">
                                    <label class="label-text" for="contact_email">Contact Email:</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group has-float-label">
                                <div class="wrapper_input">
                                    <input type="text" value="{{Auth::user()->contact_number}}"
                                           placeholder="111-111-1111" name="contact_phone"
                                           class="phoneUSMask" id="contact_phone">
                                   <label class="label-text" for="contact_phone">Contact Phone:</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="wrapper_row_link">
                        <div class="wrapper-link two-link margin-top text-center">
                            <!--<a href="{{Auth::user()->user_role == 'organization' ? url('/organization/group') : url('/volunteer/group') }}"
                               class="red"><span>Back</span></a>-->
                            <a class="button-fill" id="btn_create_group_loading" style="display:none;color:#fff;"> <span><i
                                            class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                            <a class="button-fill" href="javascript:void(0);"
                               id="btn_create_group"><span>{{((if_route_pattern('volunteer-group-group_add') or if_route_pattern('organization-group-group_add'))  and if_route_param('id', null)) ? 'Create group' : 'Confirm Edit(s)'}}</span></a>
                        </div>
                    </div>

                    @if($group_id != null)
                        <input type="hidden" name="group_id" id="group_id" value="{{$group_id}}">
                    @endif

                </form>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="upload-logo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Crop</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cropBannerImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="upload-banner" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropBannerImageBtn" class="btn btn-primary">Crop</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

    <script src="{{ asset('front-end/js/jquery.bxslider-rahisified.js') }}"></script>
    <script src="{{ asset('front-end/js/select2.full.js') }}"></script>
    <script src="{{ asset('js/check_validate.js') }}"></script>

    <script src="https://cdn.ckeditor.com/4.11.3/standard-all/ckeditor.js"></script>
    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
    <script src="{{ asset('front-end/js/group.js') }}"></script>

    <script src="{{ asset('js/jQuery-QueryBuilder/query-builder.standalone.js') }}"></script>

    <script type="text/javascript">

        var user_role = {!! json_encode(auth()->user()->user_role) !!};

        $(document).ready(function () {

            $("#btn-reset").attr("type", 'button');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var ruleCount = 0;

            var isEdit = "{{$group_id ? $group_id : 0}}";

            $(".domain").select2({ width: 'resolve' });

            $('#group_type').change(function(){

                var dynamic = $('#dynamicGroup');

                if($(this).val() == 2) {
                    dynamic.removeClass('d-none');
                    $(".domain").prop('required',true);
                }
                else {
                    dynamic.addClass('d-none');
                    $(".domain").prop('required',false);
                }
            });


            if(user_role == 'organization') {

                $(document.body).on("change",".domain",function(){

                    var builder = $('#builder');

                    var domains = $(".domain").select2('val');

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({

                        type: 'POST',
                        url: "{{route('org-dGroup-domain-attr')}}",

                        data: { domains: domains },

                        success: function (data) {

                            builder.queryBuilder('reset');

                            if(data.success) {
                                $('#uiGroup').removeClass("disableControl");
                                builder.queryBuilder('setFilters', true, data.response);
                            }

                            else {
                                builder.queryBuilder('setFilters', true, [{
                                    id: '0',
                                    label: 'Organization',
                                    type: 'string',
                                    operators: []
                                }]);

                                $('#uiGroup').addClass("disableControl");
                            }
                        }
                    });

                });

                $('#builder').queryBuilder({
                    filters: [
                        {
                            id: '0',
                            label: 'Organization',
                            type: 'string'
                        }
                    ]
                });

                if (isEdit!=0) {

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({

                        type: 'POST',
                        url: "{{route('get-dynamic-group-rules')}}",

                        data: { groupId: isEdit, _token: '{{csrf_token()}}' },

                        success: function (data) {

                            $('#builder').queryBuilder('setFilters', true, JSON.parse(data.filtersForUi));

                            if(data.rulesForUi && data.rulesForUi.length>0){
                                $('#builder').queryBuilder('setRules', JSON.parse(data.rulesForUi));
                            }

                        }
                    });
                }

                /****************************************************************
                 Triggers and Changers QueryBuilder
                 *****************************************************************/

                $('#btn-reset').on('click', function () {
                    $('#builder').queryBuilder('reset');
                    $('#builder').removeClass("builder-with-3-rules");
                    ruleCount = 0;
                });

            }

        });

    </script>
@endsection