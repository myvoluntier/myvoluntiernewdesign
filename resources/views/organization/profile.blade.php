{{--{{THIS IS FOR THE VOLUNTEER PROFILE STRUCURE IS OPPOSITE}}--}}

@extends('layout.masterForAuthUser')

@section('css')

    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">
    <link rel="stylesheet" href="{{asset('front-end/css/accordian-profile.css')}}">
    <link rel="stylesheet" href="https://foliotek.github.io/Croppie/croppie.css">
    
    <!-- my css start -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <!-- my css end -->
     
    <style>

    .wrapper-profile-box .avatar_outer_blk .avatar{
        border-radius: 50%; 
        border: 2px solid #fff;
        box-shadow: 2px 2px 0px 0px rgba(140, 140, 140, 0.3);
        width: 250px;
        min-width: 250px;
        max-width: 250px;
        height: auto;
    }
    .wrapper-profile-box .avatar_outer_blk{
        position: relative;
        max-width: 250px;
        /* height: 250px; */
    }
    .wrapper-profile-box .avatar_outer_blk .link-img{
        position: absolute;
        z-index:99;
        top:392px;
        right: 0;
        margin: 0;
    }
    .wrapper-profile-box  .top-profile-info .avatar_outer_blk .link-img a{
        margin: 0;
    }
    .wrapper-profile-box .top-profile-info .avatar_outer_blk .link-img #upload_logo1_icon{
        top:45px;
    }

        .wrapper-sort-table .table tr td:first-child {
            white-space: normal !important;
            word-wrap: break-word !important;
            max-width: 550px;
        }

        @media screen and (max-width: 524px){
            .wrapper-sort-table .table tr td:first-child{
                width: 100%;
                white-space: nowrap !important;
                word-wrap: normal !important;
                max-width: 100%;
            }
        }

        .alpha{
            font-family:'Open Sans',sans-serif;
            color:green;
            text-transform:uppercase;
            background:#fff;
            border-radius:20px;
            font-size: 14px;
        }

        a {
            text-decoration: none !important;
        }

        .pagination .flex-wrap .justify-content-center {
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .footable-page-arrow, .footable-page{
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .footable-page-arrow a, .footable-page a {
            position: relative;
            display: block;
            padding: 10px;
            margin: 10px 5px;
            font-size: 16px;
            line-height: 20px;
            font-family: 'Open Sans', sans-serif;
            color: #5f5f5f;
            background: none;
            border: 0 none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-decoration: none;
            cursor: auto;
            outline: none;
            z-index: auto;
        }

        .footable-page-arrow a:hover, .footable-page a:hover {
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }

        .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled {
            display: none;
        }

        #first_name, #last_name{
            background-color:#D3D3D3;

        }


        .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled{
            display: none;
        }

        .footable-page.active a{
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }

        .org_logo{
            height: 120px;
            width: 120px;
            float: left;
        }

        .org_info{
            padding-left: 15px;
            padding-top: 15px;
            display: inline-block;
            clear: both;
        }

        .org_info image{
            display: inline-block;
        }

        .org_info label{
            display: block;
        }

        .org_info_addr{
            margin-left: 15px;
            padding: 15px;
            float: right;
            clear: right;
        }
        .org_info_addr label{
            display: block;
        }

        /* #profile-description {
            margin-top: 50px; position:relative;
        }
        #profile-description .text {
            margin-bottom: 5px; color: #777; position:relative; font-size: 14px; display: block;
        }
        #profile-description .show-more {
            color: blue; position:relative; font-size: 10px; height: 20px; cursor: pointer;
        }
        #profile-description .show-more:hover {
            color: #1779dd;
        }
        #profile-description .show-more-height { height: 62px; overflow:hidden; } */

        .text-overflow {
            width:auto;
            max-height:120px;
            display:block;
            overflow:hidden;
            word-break: break-word;
            word-wrap: break-word;
        }

        .btn-overflow {
            display: none;
            text-decoration: none;
        }


        label.cabinet{
            display: block;
            cursor: pointer;
        }

        label.cabinet input.file{
            position: relative;
            height: 100%;
            width: auto;
            opacity: 0;
            -moz-opacity: 0;
            filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            margin-top:-30px;
        }

        #upload-logo{
            width: 400px;
            height: 400px;
            padding-bottom:25px;
        }

        #upload-banner{
            width: 800px;
            height: 400px;
            padding-bottom:25px;
        }

        figure figcaption {
            position: absolute;
            bottom: 0;
            color: #fff;
            width: 100%;
            padding-left: 9px;
            padding-bottom: 5px;
            text-shadow: 0 0 10px #000;
        }

        #cropBannerImagePop .modal-dialog{
            max-width: 900px !important;
        }
    </style>

@endsection

@section('content')

    <div class="wrapper-profile-box">
        <div class="top-profile-info">

            <form id="upload_logo" role="form" method="post" action="{{$user->user_role === 'volunteer' ? url('api/volunteer/upload_logo') : url('api/organization/profile/upload_logo')}}"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" accept="image/*" name="file_logo" id="upload_logo_input" hidden="true">
                <input type="hidden" name="image_logo" id="image_logo">
            </form>

            <form id="upload_logo1" role="form" method="post"
                  action="{{$user->user_role === 'volunteer' ? url('api/volunteer/profile/upload_back_img') : url('api/organization/profile/upload_back_img')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" accept="image/*" name="file_logo" id="upload_logo1_input" hidden="true">
                <input type="hidden" name="image_banner" id="image_banner">
            </form>
            <div class="bg_profile_bel_txt">
            <div class="bg_profile_pic_blk">
                    
                 <div class="img-box" style="background-image:url('{{$user->back_img == NULL ? asset('front-end/img/bg/0002.png') :  $user->back_img}} ')">
                        <div class="container">
                            <div class="avatar_outer_blk">
                                <div class="avatar"
                                    @if($user->user_role === 'organization')
                                        style="background-image:url('{{$user->logo_img == NULL ? asset('front-end/img/org/001.png') : $user->logo_img}}')">
                                    @else
                                        style="background-image:url('{{$user->logo_img == NULL ? asset('img/logo/member-default-logo.png') : $user->logo_img}}')">
                                    @endif
                                    <span></span>
                                    
                                </div>
                                <div class="link-img">
                                    @if($profile_info['is_my_profile'] == 1)
                                        <a id="upload_logo_icon" title="Update Logo" href="#"><i class="fa fa-camera" aria-hidden="true"></i></a>
                                        <a id="upload_logo1_icon" title="Update Cover" href="#"><i class="fa fa-picture-o" aria-hidden="true"></i></a>
                                    @endif
                                </div>
                               
                            </div>
                            
                        </div>

                         
                
                </div>
                {{-- <div class="container mobile-container">
                    
                    </div> --}}
             </div>

            <div class="container">
                <div class="row txt-box pic_below_txt_blk">
                    <div class="col-12 col-md-4">
                        <div class="main-text">
                        @if($profile_info['is_friend'] > 1 || $profile_info['is_my_profile'] == 1)
                            <p class="h2">{{$user->user_role == 'organization' ? $user->org_name : $user->first_name ." ". $user->last_name}}</p>
                        @else
                            <p class="h2">{{$user->user_role == 'organization' ? $user->org_name : $user->first_name ." ". substr($user->last_name,0,1)}}</p>
                        @endif
                        @if(!empty($user->parent_id))
                            <p>Parent Organization : {{ $user->parentOrgName()}}</p>
                        @endif
                        </div>
                        <?php
                        $address='';
                        $country = '';

                        if($user->city) {
                            $address = $address.$user->city;
                        }
                        if($user->country == "US" || $user->country == "USA"){
                            $country = 'United States';
                        }
                        if($address!='' && $user->country) {
                            $address= $address.','.$country;
                        }
                        elseif($user->country) {
                            $address= $address.$country;
                        }
                        ?>
                        @if(isset($a))
                            <p class="bold">{{$a}}</p>
                        @endif

                        <div class="text-overflow">
                            @if($user->brif)
                                {!! $user->brif !!}
                            @endif
                        </div>
                        <a class="btn-overflow" href="#">Show more</a>
                    </div>
                    
                    
                    <div class="col-12 col-md-8">
                        <div class="main-text">
                            <ul class="list-column clearfix my_flex_blk">
                                <li>
                                    <p class="h2">{{$friend->count()}}</p>
                                    <p class="light">Friends</p>
                                </li>
                                <li>
                                    <p class="h2">
                                        <?php  echo $groupCount = ($user->user_role === 'organization') ? $group->count() : count((array)$group); ?>
                                    </p>
                                    <p class="light">Groups</p>
                                </li>
                                @if($user->user_role === 'organization')
                                    <li>
                                        <p class="h2">{{$affiliatedGroups->count()}}</p>
                                        <p class="light">Affiliated Groups</p>
                                    </li>
                                @endif
                                <li>
                                    <p class="h2">{{$active_oppr->count()}}</p>
                                    <p class="light">Opportunities</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                 @if(isset($inviteLink) && $inviteLink!="" )
                    <div class="copy-link">
                         <div class="container">
                             <div class="row">
                                 <div class="flex_list_full">
                                    <input type="text" READONLY value="{{ url('/sign-up-affiliate/'.$inviteLink)}}" size="70" id="myInput" style="cursor: not-allowed;">
                                    <button onclick="inviteFun()">Copy Link</button>
                                 </div>
                             </div>
                            </div>
                   </div> 
                  @endif 
                
              </div>
          </div>
        
         


        <div class="text-profile-info my_tab_blk_sec">
            <div class="container">
                @if($user->user_role === 'organization')
                   {{-- <!-- @include('components.profile.profile-tabs') --> --}}
                   @include('components.profile.profile-tabs-organization') 
                @else
                  @include('components.profile.profile-tabs-volunteer')
                @endif
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="upload-logo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Set Logo</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cropBannerImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div id="upload-banner" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="cropBannerImageBtn" class="btn btn-primary">Set Banner</button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/plugins/datetimepicker/moment.js')}}"></script>
    

    @if($profile_info['is_my_profile'] == 1)
        <script src="{{asset('front-end//js/ckeditor/ckeditor.js')}}"></script>
        <script src="{{asset('front-end/js/ckeditor/sample.js')}}"></script>

        <script src="{{asset('js/check_validate.js')}}"></script>
        <script src="{{asset('front-end/js/sub-organization.js')}}"></script>
        <script src="{{asset('front-end/js/delegates.js')}}"></script>
        <script src="{{asset('front-end/js/custom-attributes.js')}}"></script>
        <script src="{{asset('front-end/js/partner.js')}}"></script>
        <script src="{{asset('front-end/js/org-account.js')}}"></script>
    @endif

    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    
    <!-- owlCarousel js and css start -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <!-- owlCarousel js and css end -->
    
    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
    
    <script>
    $('body').on('click', '#btn_connect', function (e) {
        e.preventDefault();
        var typeUser = '{{Auth::user()->user_role}}';
        var url = API_URL + typeUser + '/connectOrganization';

        var current_button = $(this);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        Uid = "{{$user->id}}";
        var type = "POST";
        var formData = { id: Uid };

        $.ajax({

            type: type,
            url: url,
            data: formData,
            success: function (data) {
                current_button.hide();
                $('#btn_pending').show();

                if(data.autoAccept == true){
                    toastr.success("Your friend request has been approved.", "Message");
                    $('#btn_pending').text('Approved')
                }
                else{
                    toastr.success("Your friend request has been sent.", "Message");
                }
            },

            error: function (data) {
                console.log('Error:', data);
                toastr.error("Something went wrong..!", "Error");

            }
        });
    });


        function inviteFun() {
            var copyText = document.getElementById("myInput");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
        }
        
        $(document).ready(function () {

            if(window.location.href.indexOf("serviceProject") > -1) {
                $('#serviceProjectTab').trigger('click');
            }else if(window.location.href.indexOf("customAttributes") > -1) {
                $('#customAttributesTab').trigger('click');
            }else if(window.location.href.indexOf("orgPartner") > -1) {
                $('#orgPartnerTab').trigger('click');
            }

            $('.Founded_year_format').datepicker({
                autoclose: true,
                format: 'yyyy',
                minViewMode: 2
            });

            $('#birth_day').datepicker({
                'format': 'mm-dd-yyyy',
                'autoclose': true,
                'orientation': 'right',
                'todayHighlight': true
            });
        });

        var $uploadCrop,
            tempFilename,
            rawLogoImg,
            rawBannerImg,
            imageId,
            $selected_input;

        function readURL(input, name) {

            $selected_input = name;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                if(name == "profile"){
                    reader.onload = function (e) {
                        $('.upload-logo').addClass('ready');
                        $('#cropImagePop').modal('show');
                        rawLogoImg = e.target.result;
                    };
                }else{
                    reader.onload = function (e) {
                        $('.upload-banner').addClass('ready');
                        $('#cropBannerImagePop').modal('show');
                        rawBannerImg = e.target.result;
                    };
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $uploadLogoCrop = $('#upload-logo').croppie({
            viewport: {
                width: 300,
                height: 300
            },
            enforceBoundary: true,
            enableExif: true
        });


        $uploadBannerCrop = $('#upload-banner').croppie({
            viewport: {
                width: 1500,
                height: 300
            },
            enforceBoundary: true,
            enableExif: true
        });

        $('#cropImagePop').on('shown.bs.modal', function(){
            $uploadLogoCrop.croppie('bind', {
                url: rawLogoImg
            }).then(function(){
                console.log('jQuery bind complete');
            });
        });

        $('#cropBannerImagePop').on('shown.bs.modal', function(){
            // alert('Shown pop');
            $uploadBannerCrop.croppie('bind', {
                url: rawBannerImg
            }).then(function(){
                console.log('jQuery bind complete');
            });
        });

        $('.item-img').on('change', function () { imageId = $(this).data('id'); tempFilename = $(this).val();
            $('#cancelCropBtn').data('id', imageId); readFile(this); });

        $('#cropImageBtn').on('click', function (ev) {
            $uploadLogoCrop.croppie('result', {
                type: 'base64',
                format: 'jpeg',
                backgroundColor:'white',
                size: {width: 300, height: 300}
            }).then(function (resp) {
                $('#image_logo').val(resp);
                // alert(resp);
                $('#item-img-output').attr('src', resp);
                $('#cropImagePop').modal('hide');
                $('#upload_logo').submit()
            });
        });

        $('#cropBannerImageBtn').on('click', function (ev) {
            $uploadBannerCrop.croppie('result', {
                type: 'base64',
                format: 'jpeg',
                backgroundColor:'white',
                size: {width: 1500, height: 300}
            }).then(function (resp) {
                $('#image_banner').val(resp);
                $('#item-img-output-banner').attr('src', resp);
                $('#cropBannerImagePop').modal('hide');
                $('#upload_logo1').submit()
            });
        });

        var is_open = 0;
        $('#filter_controller').on('click', function (e) {
            if(is_open == 0){
                $('.filter_div').show();
                is_open = 1;
            }else {
                $('.filter_div').hide();
                is_open = 0;
            }
        });

        $('#share_transcript').on('click', function (e) {
            $('.success-first').hide();
            $("#share_emails").val('');
            $('.hide-email-comment').show();
            $('#comments').val('');
            $('#share_profile').show();
            $('#close_share_profile_hide').hide();
            $('#myModal').modal('show');
        });

        $('#filter_apply').on('click', function (e) {
            e.preventDefault();
            var url = API_URL + 'profile/get_transcript';
            var id = '{{$id}}';
            var opp_types = $('.opp-search-checkbox').val();
            var org_types = $('.org-search-checkbox').val();
            var start_date = $('#transcript_start_date').val();
            var end_date = $('#transcript_end_date').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "GET";

            var formData = {
                id: id,
                opp_types: opp_types,
                org_types: org_types,
                start_date: start_date,
                end_date: end_date
            };

            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                    var contentTypeScriptString = '<div id="filter_content">';
                    $.each(data.oppr, function (index,value) {
                        contentTypeScriptString +='<div class="typescript" style="padding: 15px;"><div id="">';

                        if(value.org_logo == null){
                            contentTypeScriptString +='<img src="{{ asset('front-end/img/org/001.png')}}" class="org_logo">';
                        }else {
                            contentTypeScriptString +='<img src="' +value.org_logo+ '" class="org_logo">';
                        }

                        contentTypeScriptString +='<div class="org_info"><Label><strong>' +value.org_name+ '</strong></Label><Label>' +value.contact_number+ '</Label><Label>' +value.email+ '</Label></div>';
                        contentTypeScriptString +='<div class="org_info_addr"><Label>' +value.address+ '</Label></div></div><div style="overflow: auto; clear: both;">';
                        contentTypeScriptString +='<table id="example21" class="table sortable member-table">';
                        contentTypeScriptString +='<thead><tr><th><div class="main-text"><p>Date/Time</p></div></th><th><div class="main-text"><p>Duration(hrs)</p></div></th><th><div class="main-text"><p>Opportunity Title</p></div></th><th><div class="main-text"><p>Category/Type</p></div></th><th><div class="main-text"><p>Date Validated</p></div></th><th><div class="main-text"><p>Validator Name</p></div></th></tr></thead>';
                        contentTypeScriptString +='<tbody>';

                        $.each(value.track_info, function (index,track_value) {
                            contentTypeScriptString +='<tr><td>'+track_value.logged_date+'</td><td>'+track_value.logged_mins/60+'</td><td>'+track_value.oppor_name+'</td>';
                            var category_name = '';
                            var confirmed_at = '';
                            var confirmer_name = '';
                            if(track_value.confirmed_at != null){
                                confirmed_at = track_value.confirmed_at;
                            }
                            if(track_value.confirmer_name != null){
                                confirmer_name = track_value.confirmer_name;
                            }
                            category_name = track_value.name;
                            contentTypeScriptString +='<td>'+category_name+'</td><td>'+confirmed_at+'</td><td>'+confirmer_name+'</td></tr>';
                        });
                        contentTypeScriptString +='</tbody></table></div></div>';
                    });
                    $('#filter_content').replaceWith(contentTypeScriptString);
                },

                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

    $("#transcript_end_date").prop('disabled', true);

    $('#transcript_start_date').datepicker({
        'format': 'mm-dd-yyyy',
        'autoclose': true,
        'orientation': 'right',
        'todayHighlight': true,
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $("#transcript_end_date").prop('disabled', false);
        $('#transcript_end_date').datepicker('setStartDate', endDate);
        $('#transcript_end_date').datepicker('setDate', endDate);
    }).on('clearDate', function (selected) {
        $('#transcript_end_date').datepicker('setStartDate', '+0d');
    });

    $('#transcript_end_date').datepicker({
        'format': 'mm-dd-yyyy',
    }).on('changeDate', function (selected) {
        daterangeChange();
    });

    function daterangeChange() {}

    $('#btn_accept').on('click', function (e) {
        e.preventDefault();

        var typeUser = '{{Auth::user()->user_role}}';

        var url = API_URL + typeUser + '/acceptFriend';

        var id = {{$user->id}};

        var current_button = $(this);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var type = "POST";
        var formData = { id: id };

        $.ajax({

            type: type,
            url: url,
            data: formData,
            success: function (data) {
                $('.wrapper_connect_button').hide();
                window.location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });

</script>

    <script type="text/javascript">
        var _URL = window.URL || window.webkitURL;
        $('#upload_logo_icon').on('click', function () {
            $('#upload_logo_input').click()
        });

        $('#upload_logo1_icon').on('click', function () {
            $('#upload_logo1_input').click()
        });

        $('#upload_logo_input').on('change', function () {
//            $('#upload_logo').submit()
            readURL(this, "profile");
        });

        $('#upload_logo1_input').on('change', function () {
            readURL(this, "banner");
//            var file, img;
//
//            if ((file = this.files[0])) {
//
//                img = new Image();
//
//                img.onload = function () {
//
//
//                    if (this.width < 1500 || this.height < 300) {
//
//                        $('#banner_size').val(1);
//
//                        alert('Recomended banner size 1500 X 300. Please try another');
//
//                    }
//
//                    else {
//
//                        $('#upload_logo1').submit()
//
//                        //alert(this.width+'=='+this.height);
//
//                    }
//
//                };
//
//                img.src = _URL.createObjectURL(file);
//
//            }

        });
        $('.footable').dataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "pageLength": 10,
            "pagingType": "full_numbers",
            "language": {
                "paginate": {
                    "first": "",
                    "next": '',
                    "previous": '',
                    "last": ''
                }
            }
        });
        /*$('.footable').footable({
            limitNavigation: 5,
            firstText: '1'
        });
        $('ul.pagination').each(function(){
            if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
            else $(this).find(' .footable-page-arrow:last').show();
        });

        $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
            var pagination = $(this).parents('.tab-pane.active .pagination');
            if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
            else pagination.find('.footable-page-arrow:first').show();
            if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
            else pagination.find('.footable-page-arrow:last').show();
        });*/
        @if($profile_info['is_my_profile'] == 1)
        initSample();
        @endif
        $(document).ready(function () {
            @if($user->user_role === 'volunteer' and   $profile_info['is_my_profile'] == 1)
                CKEDITOR.replace('editor');
            @endif

            /*$('.footable').footable({
                limitNavigation: 5,
                firstText: '1'
            });
            $('ul.pagination').each(function(){
                if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
                else $(this).find(' .footable-page-arrow:last').show();
            });

            $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
                var pagination = $(this).parents('.tab-pane.active .pagination');
                if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
                else pagination.find('.footable-page-arrow:first').show();
                if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
                else pagination.find('.footable-page-arrow:last').show();
            });*/

            /*.dataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });*/

            function doResize($wrapper, $el) {

                var scale = Math.min(
                    $wrapper.outerWidth() / $el.outerWidth(),
                    $wrapper.outerHeight() / $el.outerHeight()
                );

                if (scale < 1) {

                    $el.css({
                        '-webkit-transform': 'scale(' + scale + ')',
                        '-moz-transform': 'scale(' + scale + ')',
                        '-ms-transform': 'scale(' + scale + ')',
                        '-o-transform': 'scale(' + scale + ')',
                        'transform': 'scale(' + scale + ')'
                    });

                } else {
                    $el.removeAttr("style");
                }

            }

            $('.wrapper_input.fa-icons input').datepicker({
                'format': 'mm-dd-yyyy',
                'autoclose': true,
                'orientation': 'right',
                'todayHighlight': true
            });


            $('.dashboard_org .track-it-time_slider .slider-wrapper').each(function () {
                doResize($(this), $(this).find('.slider-item-scale > div'));
            });

            $(window).resize(function () {
                $('.dashboard_org .track-it-time_slider .slider-wrapper').each(function () {
                    doResize($(this), $(this).find('.slider-item-scale > div'));
                });
            });

            $('.member-table').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "pageLength": 10,
                "pagingType": "full_numbers",
                "language": {
                    "paginate": {
                        "first": "",
                        "next": '',
                        "previous": '',
                        "last": ''
                    }
                }
            });
            /* $('.member-table').footable({
                 limitNavigation: 5,
                 firstText: '1'
             });
             $('ul.pagination').each(function(){
                 if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
                 else $(this).find(' .footable-page-arrow:last').show();
             });

             $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
                 var pagination = $(this).parents('.tab-pane.active .pagination');
                 if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
                 else pagination.find('.footable-page-arrow:first').show();
                 if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
                 else pagination.find('.footable-page-arrow:last').show();
             });

             if ($('.footable-page-arrow:last').text() < 5) $('.footable-page-arrow:last').hide();
             else $('.footable-page-arrow:last').show();
             console.log($('.footable-page-arrow:last').text())

             $(document).on('click', 'a[data-page]', function () { //, .footable-page-arrow
                 if ($('.footable-page:first a').data('page') == 0) $('.footable-page-arrow:first').hide();
                 else $('.footable-page-arrow:first').show();
                 if (parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
                 else $('.footable-page-arrow:last').show();
                 console.log($('.footable-page-arrow:last').text())
             });

             $('.member-table').footable({
                 limitNavigation: 5,
                 firstText: '1'
             });
             $('ul.pagination').each(function(){
                 if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
                 else $(this).find(' .footable-page-arrow:last').show();
             });

             $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
                 var pagination = $(this).parents('.tab-pane.active .pagination');
                 if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
                 else pagination.find('.footable-page-arrow:first').show();
                 if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
                 else pagination.find('.footable-page-arrow:last').show();
             });*/


            $('.search-input').on('keyup', function (e) {
                if (e.keyCode == 13) {
                    $('#btn_search_page').trigger('click');
                }
            })


            $('.filter').on('click', function () {
                console.log('change')
                $('#btn_search_page').trigger('click');
            });


            var $image = $(".profile-back-image > img")


//            $("#inputImage").change(function (e) {
//
//                var file, img;
//
//                if ((file = this.files[0])) {
//
//                    img = new Image();
//
//                    img.onload = function () {
//
//
//                        if (this.width < 1500 || this.height < 300) {
//
//                            $('#banner_size').val(1);
//
//                            alert('Recomended banner size 1500 X 300. Please try another');
//
//                        }
//
//                        else {
//
//                            $('#banner_size').val(0);
//
//                            $('#upload_logo').submit();
//
//                            //alert(this.width+'=='+this.height);
//
//                        }
//
//                    };
//
//                    img.src = _URL.createObjectURL(file);
//
//                }
//
//            });

        });


        // Get all html elements for map
         @if($user->user_role !== 'volunteer')
        var latLng = new google.maps.LatLng(parseFloat($('#lat_val').val()), parseFloat($('#lng_val').val()));

        var myOptions = {
            zoom: 16,
            center: latLng,
            styles: [{
                "featureType": "water",
                "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]
            }, {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#808080"}, {"lightness": 54}]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ece2d9"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ccdca1"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#767676"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#ffffff"}]
            }, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]
            }, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.sports_complex",
                "stylers": [{"visibility": "on"}]
            }, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.business",
                "stylers": [{"visibility": "simplified"}]
            }],
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("pos_map"), myOptions);

        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon: '{{asset('front-end/img/pin.png')}}'
        });
        @endif

        $('#btn_follow').on('click', function () {

            var url = API_URL + 'organization/followOrganization';
            var id = $('#current_id').val();
            var current_button = $(this);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "POST";

            var formData = { id: id }

            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                    current_button.hide();
                    $('#btn_unfollow').show();
                },

                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


        $('#btn_unfollow').on('click', function () {

            var url = API_URL + 'organization/unfollowOrganization';
            var id = $('#current_id').val();
            var current_button = $(this);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "POST";

            var formData = { id: id}

            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                    current_button.hide();
                    $('#btn_follow').show();
                },

                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    </script>

    @if($profile_info['is_my_profile'] == 1)
    
       <script>
            var display = $('.footable-page-arrow:nth-last-child(3)').css('display')

            if (display == 'none') {
                $('.footable-page-arrow:last-child').css('display', 'none')
            }
            else {
                $('.footable-page-arrow:last-child').css('display', 'block')
            }


            $('#submit-update-account').on('click', function () {

                $('.org_name_detail').text($('#org_name').val());
                $('.user_name_detail').text($('#user_id').val());
                $('.user_email_detail').text($('#email').val());
                $('.user_phone_detail').text($('#contact_num').val());
                $('.user_education_detail').text(CKEDITOR.instances.editor.getData());
                $('#orgSum').val(CKEDITOR.instances.editor.getData());

                $('#textarea_box').html(CKEDITOR.instances.editor.getData());

                $('#update_account_oug').submit();
            });

            $("#update_account_oug").unbind('submit').submit(function (event) {
                event.preventDefault();
                var formData = new FormData($(this)[0]);
                var flags = 0;

                /*if ($('#org_name').val() == '') {
                    $('#org_name').css("border", "1px solid #ff0000");
                    flags++;
                }*/
                $('#domain_1, #domain_2').css("border", "");
                if ($('#domain_1').val() != "" && !ValidateDomain($('#domain_1').val())) {
                    flags++;
                    $('#domain_1').css("border", "1px solid #ff0000");
                }

                if ($('#domain_2').val() != "" && !ValidateDomain($('#domain_2').val())) {
                    flags++;
                    $('#domain_2').css("border", "1px solid #ff0000");
                }
                if ($('#new_password').val() != '') {
                    if (!ValidatePassword($('#new_password').val())) {
                        flags++;
                        $('#invalid_password').show();
                        $('#new_password').css("border", "1px solid #ff0000");
                    }

                    if ($('#new_password').val() != $('#confirm_password').val()) {
                        flags++;
                        $('#invalid_confirm').show();
                        $('#confirm_password').css("border", "1px solid #ff0000");
                    }
                }

                if(flags == 0){
                    $('#submit-update-account').hide();
                    $('.btn_update_account').show();
                    $.ajax({
                        url: API_URL + 'organization/update_account',
                        type: 'POST',
                        data: formData,
                        async: true,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (return_data) {
                            $('#submit-update-account').show();
                            $('.btn_update_account').hide();
                            var result = $.parseJSON(return_data);

                            if (result.status == 1) {
                                toastr.success(result.msg, "Message");
                            } else {
                                toastr.error(result.msg, "Error");
                            }
                        }
                    });
                }else{
                    toastr.error('Form field validation.', "Error");
                }
            });

            $('#btn_edit').click(function () {
                var modal = document.getElementById('myModal');
                var btn = document.getElementById('btn_edit');
                var span = document.getElementsByClassName("close")[0];
                btn.onclick = function () {
                    modal.style.display = "block";
                };
                span.onclick = function () {
                    modal.style.display = "none";
                };
                window.onclick = function (event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
            });

            $('#btn_save').on('click', function (e) {

                e.preventDefault();
                var flags = 0;

                $('#email2, #email3').css("border", "");
                if ($('#email2').val() != "" && !ValidateEmail($('#email2').val())) {
                    flags++;
                    $('#email2').css("border", "1px solid #ff0000");
                }

                if ($('#email3').val() != "" && !ValidateEmail($('#email3').val())) {
                    flags++;
                    $('#email3').css("border", "1px solid #ff0000");
                }

                if ($('#birth_day').val() == '') {
                    $('#birth_day').css("border", "1px solid #ff0000");
                    flags++;
                }

                if ($('#address').val() == '') {
                    $('#address').css("border", "1px solid #ff0000");
                    flags++;
                }

                if ($('#zipcode').val() == '') {
                    $('#zipcode').css("border", "1px solid #ff0000");
                    flags++;
                }

                if (!ValidateZipcode($('#zipcode').val())) {
                    flags++;
                    $('#zipcode').css("border", "1px solid #ff0000");
                    $('#invalid_zipcode_alert').show();
                }

                if ($('#new_password').val() != '') {
                    if (!ValidatePassword($('#new_password').val())) {
                        flags++;
                        $('#invalid_password').show();
                        $('#new_password').css("border", "1px solid #ff0000");
                    }

                    if ($('#new_password').val() != $('#confirm_password').val()) {
                        flags++;
                        $('#invalid_confirm').show();
                        $('#confirm_password').css("border", "1px solid #ff0000");
                    }
                }

                if (flags == 0) {

                    $('.user_name_profile').text($('user_id').val())
                    $('.user_email_profile').text($('#email').val())

                    $('.my_summary_acount').val(CKEDITOR.instances.editor.getData())
                    $('#textarea_box').html(CKEDITOR.instances.editor.getData())

                    $('#update_account').submit()
                }else{
                    toastr.error('Form field validation.', "Error");
                }

            });

            $("#update_account").unbind('submit').submit(function (event) {

                event.preventDefault();
                $('#btn_save').hide();
                $('.btn_update_account').show();
                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: API_URL + "volunteer/update_account",
                    type: 'POST',
                    data: formData,
                    async: true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (return_data) {
                        var result = $.parseJSON(return_data);
                        $('#btn_save').text('Save Change');
                        $('#btn_save').show();
                        $('.btn_update_account').hide();
                        if (result.status == 1) {
                            toastr.success(result.msg, "Message");
                        } else {
                            toastr.error(result.msg, "Error");
                        }
                    }
                });
            });

            var text = $('.text-overflow'), btn = $('.btn-overflow'), h = text[0].scrollHeight;

            if (h > 120) {
                btn.addClass('less');
                btn.css('display', 'block');
            }

            btn.click(function (e) {
                e.stopPropagation();
                if (btn.hasClass('less')) {
                    btn.removeClass('less');
                    btn.addClass('more');
                    btn.text('Show less');
                    text.animate({'height': h});
                } else {
                    btn.addClass('less');
                    btn.removeClass('more');
                    btn.text('Show more');
                    text.animate({'height': '120px'});
                }
            });

            $('.Founded_year_format').datepicker({
                autoclose: true,
                format: 'yyyy',
                minViewMode: 2
            });

        </script>
       
    @endif
    
    
    
  <script type="text/jscript">
    $('.owl_1').owlCarousel({  
        loop:true,
        margin:15,	
        responsiveClass:true,
        autoplayHoverPause:true,
        autoplay:false,
        autoWidth:true,
        slideSpeed: 400,
        paginationSpeed: 400,
        autoplayTimeout:3000,
        responsive:{
            0:{
                items:3,
                nav:false,
                    loop:false
            },
            600:{
                items:3,
                nav:false,
                    loop:false
            },
            1000:{
                items:4,
                margin:50,
                nav:false,
                loop:false
            }
        }
    })
    $(document) .ready(function(){
        var li =  $(".owl-item li ");
        $(".owl-item li").click(function(){
            li.removeClass('active');
        });
    });
    </script>
    
@endsection