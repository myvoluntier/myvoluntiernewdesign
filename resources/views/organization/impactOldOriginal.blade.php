@extends('layout.masterForAuthUser')

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">
@endsection

@section('content')

    <div class="wrapper-impact">
        <div class="container">

            <div class="row align-items-center border-bottom-0">
                <div class="col">
                    <div class="main-text">
                        <h2 class="h2">Impact</h2>
                    </div>
                </div>
                <div class="col col-auto">
                    <a href="javascript:void(0)" onclick="window.print()" class="print"><span><i class="fa fa-print" aria-hidden="true"></i></span></a>
                </div>
            </div>

            <div class="row align-items-center border-bottom-0">
                <div class="col-12 col-md-4">
                    <div class="main-text">
                        <h3 class="h3 text-center">Ranked</h3>
                        <p class="mb-0 text-center green middle"><strong class="big">{{$rank}}</strong>
                            @if($rank>0)

                                @if($rank==1)st

                                @elseif($rank==2)nd

                                @elseif($rank==3)rd

                                @else th

                                @endif

                            @endif
                        </p>

                        <p class="light text-center">in
                            @if($userDetails->country != null)
                                {{ $userDetails->country }}
                            @endif

                        </p>

                        <p class="mb-0 text-center green big">
                            <strong>
                                @if(count($trackedHourForOrg)>0)
                                    {{ round($trackedHourForOrg[0]->SUM/60) }}
                                @else
                                    0
                                @endif
                            </strong>
                        </p>
                        <p class="mb-0 text-center green middle">HOUR(S)</p>
                        <p class="light text-center">contributed since creation</p>
                    </div>
                </div>
                <div class="col-12 col-md-8 border-left">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#lastMonth" role="tab" data-id="lastMonthVol"
                               data-toggle="tab"><span>Last Month</span></a>
                        </li>
                        <li class="nav-item second">
                            <a class="nav-link" href="#last6Month" role="tab" data-id="last6MonthVol"
                               data-toggle="tab"><span>Last 6 month</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#lastYear" role="tab" data-id="lastYearVol"
                               data-toggle="tab"><span>Last year</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#yearToDate" role="tab" data-id="yearToDateVol"
                               data-toggle="tab"><span>Last 12 Months</span></a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active show" id="lastMonth">
                            <div id="div-lastMonth" class="wrapper-svg"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="last6Month">
                            <div id="div-last6Month" class="wrapper-svg"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="lastYear">
                            <div id="div-lastYear" class="wrapper-svg"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="yearToDate">
                            <div id="div-year-to-date" class="wrapper-svg"></div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row align-items-center">
                <div class="col-12">
                    <div class="main-text">
                        <h3 class="h3">List of volunteers</h3>
                    </div>
                </div>
                <div class="col-12 text-center">
                    {{--This code is not used just did a trick to show two tabs on 1 click--}}
                    <ul class="nav nav-tabs d-none" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#lastMonthVol" role="tab"
                               data-toggle="tab"><span>Last Month</span></a>
                        </li>
                        <li class="nav-item second">
                            <a class="nav-link" href="#last6MonthVol" role="tab"
                               data-toggle="tab"><span>Last 6 month</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#lastYearVol" role="tab"
                               data-toggle="tab"><span>Last year</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#yearToDateVol" role="tab"
                               data-toggle="tab"><span>Last 12 Months</span></a>
                        </li>
                    </ul>
                    {{--This code is not used just did a trick to show two tabs on 1 click--}}

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active show" id="lastMonthVol">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Age</th>
                                        <th>Total Hours</th>
                                        <th>Total Opportunities</th>
                                        <th>City</th>
                                        <th>Email</th>
                                        <th>Phone #</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($volList['lastMonth'] as $member)
                                        @if($member->user)
                                        <tr>
                                            <td>{{$member->user->getFullNameVolunteer()}}</td>
                                            <td>{{ $member->user->birth_date ? \Carbon\Carbon::parse($member->user->birth_date)->age : 'n/a'}}</td>
                                            <td class="totalHoursCls" data-id="{{$member->user_id}}">{{$member->user->getLoggedHoursSum()}} <span style="cursor: pointer;" class="badge badge-success">See More?</span></td>
                                            <td>{{$member->user->oppr_count}}</td>
                                            <td>{{$member->user->city}}</td>
                                            <td>{{$member->user->email}}</td>
                                            <td>{{$member->user->contact_number}}</td>
                                        </tr>
                                    @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="last6MonthVol">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Age</th>
                                        <th>Total hours</th>
                                        <th>Total Opportunities</th>
                                        <th>City</th>
                                        <th>Email</th>
                                        <th>Phone #</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($volList['last6Month'] as $member)
                                        @if($member->user)
                                            <tr>
                                                <td>{{$member->user->getFullNameVolunteer()}}</td>
                                                <td>{{ $member->user->birth_date ? \Carbon\Carbon::parse($member->user->birth_date)->age : 'n/a'}}</td>
                                                <td class="totalHoursCls" data-id="{{$member->user_id}}">{{$member->user->getLoggedHoursSum()}} <span style="cursor: pointer;" class="badge badge-success">See More?</span></td>
                                                <td>{{$member->user->oppr_count}}</td>
                                                <td>{{$member->user->city}}</td>
                                                <td>{{$member->user->email}}</td>
                                                <td>{{$member->user->contact_number}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="lastYearVol">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Age</th>
                                        <th>Total hours</th>
                                        <th>Total Opportunities</th>
                                        <th>City</th>
                                        <th>Email</th>
                                        <th>Phone #</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($volList['lastYear'] as $member)
                                        @if($member->user)
                                            <tr>
                                                <td>{{$member->user->getFullNameVolunteer()}}</td>
                                                <td>{{ $member->user->birth_date ? \Carbon\Carbon::parse($member->user->birth_date)->age : 'n/a'}}</td>
                                                <td class="totalHoursCls" data-id="{{$member->user_id}}">{{$member->user->getLoggedHoursSum()}} <span style="cursor: pointer;" class="badge badge-success">See More?</span></td>
                                                <td>{{$member->user->oppr_count}}</td>
                                                <td>{{$member->user->city}}</td>
                                                <td>{{$member->user->email}}</td>
                                                <td>{{$member->user->contact_number}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="yearToDateVol">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Age</th>
                                    <th>Total hours</th>
                                    <th>Total Opportunities</th>
                                    <th>City</th>
                                    <th>Email</th>
                                    <th>Phone #</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($volList['lastYearToDate'] as $member)
                                    @if($member->user)
                                        <tr>
                                            <td>{{$member->user->getFullNameVolunteer()}}</td>
                                            <td>{{ $member->user->birth_date ? \Carbon\Carbon::parse($member->user->birth_date)->age : 'n/a'}}</td>
                                            <td class="totalHoursCls" data-id="{{$member->user_id}}">{{$member->user->getLoggedHoursSum()}} <span style="cursor: pointer;" class="badge badge-success">See More?</span></td>
                                            <td>{{$member->user->oppr_count}}</td>
                                            <td>{{$member->user->city}}</td>
                                            <td>{{$member->user->email}}</td>
                                            <td>{{$member->user->contact_number}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row align-items-center border-bottom-0">
                <div class="col-12 col-md-7">
                    <!--Top 5 Opportunities
                        Helping Orphans
                        460 hrs -->
                    <div id="container-2" class="wrapper-svg"></div>

                </div>
                <?php
                    $a=0;
                    $t1 = array();
                    $t2 = array();
                    $t3 = array();
                    $t4 = array();
                
                foreach($name_and_loc as $tr)
                {
                    $t1[$a] = $tr->user_name;
                    $t2[$a] = $tr->country;
                    $a = $a + 1;
                }   
                $a=0;
                if(isset($t))
                {
                    foreach($t as $tr)
                    {
                        $t3[$a] = $tr;
                        $a = $a + 1;
                    }
                }
                $a=0;
                if(isset($rating))
                {
                    foreach($rating as $tr)
                    {
                        $t4[$a] = $tr;
                        $a = $a + 1;
                    }
                }
                ?>

                <div class="col-12 col-md-5 border-left">
                    <div class="table-responsive">
                        <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Impact</th>
                        <th>Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i < count($t1); $i++)
                        <tr>
                            <td>{{$t1[$i]}}</td>
                            <td>{{$t2[$i]}}</td>
                            <td>{{$t3[$i]/60}} hours</td>
                            @if(isset($t4[$i]))
                                <td>{{number_format((float)$t4[$i], 1, '.', '')}}</td>
                            @else
                            <td>n/a</td>
                            @endif
                        </tr>
                        @endfor
                    </tbody>
                </table>
                    </div>
                </div>
            </div>

        </div>

    </div>

    @include('components.auth.modal_pop_up_show_oppr')
@endsection


@section('script')
    <script src="{{asset('js/highcharts.js')}}"></script>

    <script>
        $(function(){
            $('.totalHoursCls').on('click', function (e) {

                id = $(this).attr('data-id');
                $('#records_table').empty();

                var action = '{{route('organization-impact-vol-opprs')}}';

                $.ajax({
                    type: 'post',
                    url: action,
                    data: { 'id': id },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function () {},
                    success: function (response) {

                        console.log(response.length);
                        if( response.length > 0 ) {
                            var trHTML = '';
                            $.each(response, function (i, item) {
                                trHTML += '<tr><td>' + item.title + '</td><td>' + item.city + '</td><td>' + item.state + '</td><td>' + item.contact_name + '</td><td>' + item.contact_number + '</td><td>' + item.hours + '</td></tr>';
                            });

                            $('#records_table').append(trHTML);
                        }

                        else
                            toastr.error("no record found in the system.", "Error!");
                    }
                });

                $('#showOpprVol').modal('show');
            });

            $('.nav-link').click(function(e){
                id = '#'+$(this).data('id'); //tab-content
                $('.nav-tabs a[href="' + id + '"]').tab('show');
            })
        })
    </script>

    <script>
        Highcharts.chart('div-lastMonth', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                   @foreach($lastMonth as $date)
                        ['{{date('d M',strtotime($date->logged_date))}}', {{round($date->SUM/60)}}],
                    @endforeach
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });


        Highcharts.chart('div-last6Month', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                    @foreach($sixMonth as $sixmnth)
                        ['{{date('M',strtotime($sixmnth->logged_date))}}', {{round($sixmnth->SUM/60)}}],
                    @endforeach
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });


        Highcharts.chart('div-lastYear', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                    @foreach($LastYear as $lastYear)
                     ['{{date('M',strtotime($lastYear->logged_date))}}', {{round($lastYear->SUM/60)}}],
                    @endforeach
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('div-year-to-date', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -90,
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                    @foreach($year as $y)
                        ['{{date('Y',strtotime($y->logged_date))}}', {{round($y->SUM/60)}}],
                    @endforeach
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });
    </script>


    <script>
        Highcharts.chart('container-2', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Top 5 Opportunities',
                style: {"color": "#27282f", "fontSize": "24px", "fontFamily": 'Open Sans, sans-serif'}
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                lineWidth: 0,
                tickLength: 0,
                categories: [

                @if(count($opportunityList)>0)

                    @for($i=0; $i < count($opportunityList); $i++)
                        @if($i === 5)
                           @break;
                        @endif
                        '{!! $opportunityList[$i]->title !!}',
                    @endfor

                @endif

                ],
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#28292e'
                    }
                }
            },
            yAxis: {
                visible: false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontSize: '14px',
                            fontFamily: 'Open Sans, sans-serif',
                            fontWeight: 'normal',
                            color: '#fff',
                            textOutline: ''
                        },
                        align: 'right',
                    },
                    showInLegend: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                    formatter: function() {
                    return '<b>' + this.x +':'+ this.y + '</b>';
                }

            },
            series: [{
                pointWidth: 40,
                color: '#42bd41',
                data: [
                    @if(count($opportunityList)>0)

                     @for($i=0; $i < count($opportunityList); $i++)
                        @if($i === 5)
                            @break;
                        @endif
                         {{$opportunityList[$i]->tracked_hours}},

                     @endfor

                @endif
                ],
                dataLabels: {
                    format: '{y} mins'
                }
            }]
        });
    </script>

@endsection

