@extends('layout.masterForAuthUser')
@section('content')
        <div class="container dashboard_org">

            <div class="news-followed-box pt22 wrapper-tablist">
                @if($action == "verified")
                    <p id="new_signin" class="alert-success" style="padding: 10px">Your account successfully verified!</p>
                @endif

                <div class="main-text"><h3 class="h3">News Feed</h3></div>

                <div class="row">
                    <div class="col-12 col-md-12">
                        <ul class="news-feeds-list">                 
                            @if (count($feedNewsArr) > 0)
                                @foreach($feedNewsArr as $nawfVal)
                                    <li> <div class="dateand-time">
                                            <span class="news-feed-icon">
                                                <i class="fa {{ !preg_match("/left/", $nawfVal['reason']) ? 'fa-check' : 'fa-exclamation-triangle'}}" aria-hidden="true">
                                                </i>
                                            </span>
                                            <span class="light feed-date">
                                            <?php
                                                 $str=strtotime($nawfVal['created_at']) ;  
                                               ?>
                                                  <div>{{date('M d, Y', $str)}}</div>
                                                  <div>{{date('H:i:s A',$str)}}</div>     
                                            </span>
                                        </div>
                                            <p>
                                                <span>{{ucfirst($nawfVal['reason']) }} by <a href="{{url( $nawfVal['userurl'])}}">{{ $nawfVal['who_joined'] }}</a> on <a href="{{url( $nawfVal['utl']) }}">{{ $nawfVal['name']}}</a></span>  
                                            </p> 
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                        <div class="col-12 col-md-12">
                        <div class="pull-right">
                            {{$newsFeedData->render()}}
                        </div>
                        </div>                       
                    </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/jquery.bxslider-rahisified.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.dashboard_org .track-it-time_slider > div.slider').bxSlider({
                pager: true,
                slideMargin: 0,
                speed: 1000,
                hideControlOnEnd: true,
                auto: true,
                infiniteLoop: true,
                autoReload: true,
                controls: false,
                breaks: [{screen: 0, slides: 1}]

            });

            setTimeout(function () {
                $('#new_signin').fadeOut('fast');
            }, 3000);

            function doResize($wrapper, $el) {

                var scale = Math.min(
                    $wrapper.outerWidth() / $el.outerWidth(),
                    $wrapper.outerHeight() / $el.outerHeight()
                );

                if (scale < 1) {

                    $el.css({
                        '-webkit-transform': 'scale(' + scale + ')',
                        '-moz-transform': 'scale(' + scale + ')',
                        '-ms-transform': 'scale(' + scale + ')',
                        '-o-transform': 'scale(' + scale + ')',
                        'transform': 'scale(' + scale + ')'
                    });

                } else {

                    $el.removeAttr("style");

                }

            }


            $('.dashboard_org .track-it-time_slider .slider-wrapper > div').each(function () {
                doResize($(this), $(this).find('.slider-item-scale > div'));
            });

            $(window).resize(function () {
                $('.dashboard_org .track-it-time_slider .slider-wrapper > div').each(function () {
                    doResize($(this), $(this).find('.slider-item-scale > div'));
                });
            });

        });

    </script>
@endsection