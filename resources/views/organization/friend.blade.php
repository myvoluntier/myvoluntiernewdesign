@extends('layout.masterForAuthUser')

@section('css')
    <link href="<?= asset('css/plugins/footable/footable.core.css') ?>" rel="stylesheet">
@endsection

@section('content')
    <div class="wrapper-friends org_fri_blk">
    <div class="search-friends page_head-box green_bg_blk">
            <div class="container search_inner_blk">
                <h2 class="h2">Friends</h2>
                <div class="search_blk search-container">                    
                    <input id="search-friens-input" class="search expandright" type="text" placeholder="Search Friends">
                    <label class="button_style searchbutton" for="search-friens-input"><span class="gl-icon-search"></span></label>
                </div>
            </div>
        </div>

        <div class="wrapper-friends-list">
            <div class="container">
                        <!-- Tab section start -->
                            <div class="new-track-calender-tab friend_tab_btn">
                                <div class="container">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active show" id="calenderTab" href="#friends_section" role="tab"
                                            data-toggle="tab"><span>Friends</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#suggested_volunteers" id="opporTab" role="tab"
                                            data-toggle="tab"><span class="short_txt">Suggested</span> <div class="mob_txt"> Volunteers</div></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#suggested_organizations" id="opporTab" role="tab"
                                            data-toggle="tab"><span class="short_txt">Suggested</span> <div class="mob_txt"> Organizations</div></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                                <div class="main-data-div"></div>

                                @include('organization._friendsUserList')
                        
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script src="<?= asset('js/plugins/dataTables/jquery.dataTables.js') ?>"></script>
    <script>
        $(document).ready(function () {

            $('.friend-table').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "pageLength": 10,
                "pagingType": "full_numbers",
                "language": {
                    "paginate": {
                        "first": "",
                        "next": '',
                        "previous": '',
                        "last": ''
                    }
                }
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            $('#search-friens-input').keyup(function () {

                if ($(this).val().length > 2) {//alert($(this).val().length);
                     $(this).addClass('search_text');
                    var this1 = $(this);
                    $.ajax({
                        url: "{{ Auth::user()->user_role === 'organization' ? route('organization-get-friend') : route('volunteer-get-friend')}}",
                        data: 'keyword=' + $(this).val(),
                        type: 'POST',
                        success: function (res) {
                            $('.friends-list-search').remove();
                            $('.friends-list-main').hide();
                            $('.main-data-div').after(res);
                        }
                    })
                }
                else {
                    $(this).removeClass('search_text');
                    $('.friends-list-search').remove();
                    $('.friends-list-main').show();
                }
            })
        });

        function request_update(id, friend_id, status, delete_type) {
            $('#error').html('');

            if(confirm('Are you sure?')){
                $.ajax({
                    url: "{{Auth::user()->user_role === 'organization' ? route('organization-friend-accept-reject') : route('volunteer-friend-accept-reject') }}",
                    data: {
                        'id': id,
                        'user_id':{{ $user_id }},
                        'sender': friend_id,
                        'status': status,
                        'type': delete_type
                    },
                    type: 'POST',
                    success: function (res) {
                        if (res.success == 0) {
                            $('#error').html(res.error);
                        } else {
                            window.location.reload();
                        }
                    }
                });
            }
        }
    </script>
@endsection
