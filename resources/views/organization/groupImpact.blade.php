@extends('layout.masterForAuthUser')
@section('css')

    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/jquery.bxslider.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap-sortable.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/select2-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">

    <style>
        .active_group_page {
            color: #3bb44a !important;
            background: #fff;
            text-decoration: none;
        }
        .tab_item {
            display: none;
        }

        .hide {
            display: none;
        }

        a {
            text-decoration: none !important;
        }

        .pagination .flex-wrap .justify-content-center {
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .footable-page-arrow, .footable-page {
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .footable-page-arrow a, .footable-page a {
            position: relative;
            display: block;
            padding: 10px;
            margin: 10px 5px;
            font-size: 16px;
            line-height: 20px;
            font-family: 'Open Sans', sans-serif;
            color: #5f5f5f;
            background: none;
            border: 0 none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-decoration: none;
            cursor: auto;
            outline: none;
            z-index: auto;
        }

        .footable-page-arrow a:hover, .footable-page a:hover {
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }


        .footable-page:nth-child(4), .footable-page:nth-last-child(4), .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled {
            display: none;
        }

        .footable-page.active a, .footable-page-arrow.disabled:first-child, .footable-page-arrow.disabled:last-child {
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
            display:block;
        }
        input[type=text] {
            width: 130px;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;
            /*background-image: url('public/img/searchicon.png');*/
            background-position: 10px 10px;
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }

        .contents {
            display: none;
        }

        .preload {
            margin-top: 10%;
        }

        .print-header {
            display: none !important;
        }

    </style>

    @if(Auth::user()->user_role !== 'organization')

        <style>
            #sendFrm ul{
                position: relative;
                text-align: left;
            }
            ul {
                list-style-type: none;
            }

        </style>
    @endif

@endsection

@section('content')
<div class="container">
 <div class="wrapper-impact pb-0 pt-0">
    <div class="row align-items-center border-top-0 border-left-0 border-right-0">
    <div class="col">
            <div class="main-text">
                        <h2 class="h2">Impact</h2>
            </div>
        </div>
        <div class="col col-auto">
            <a href="javascript:void(0)" onclick="window.print()" class="print">
                <span><i class="fa fa-print" aria-hidden="true"></i></span>
            </a>
        </div>
    </div>
        @if(count($groupList)>0)
                    @foreach($groupList as $lists)                
    <div class="row align-items-center border-top-0 border-left-0 border-right-0">
        <div class="col-12 col-md-4">
            <img style="max-width: 45%" src="{{$lists->logo_img ? $lists->logo_img : asset('no-image.png')}}" class="img-thumbnail mx-auto d-block" alt="Group logo">
            <div class="main-text">
                <h3 class="h3 text-center">Ranked</h3>     
                <div class="row">
                    <div class="col">
                        <p class="mb-0 text-center green middle">
                            <strong class="big">{{$lists->rank}}</strong>{{numberDescender($lists->rank)}}
                        </p>
                        <p class="light text-center">Of All <br>Groups</p></div>
                    <div class="col">
                        <p class="mb-0 text-center green middle">
                            <strong class="big">{{$lists->ranking_cat}}</strong>{{numberDescender($lists->ranking_cat)}}
                        </p>
                        <p class="light text-center">Of "{{$lists->ranking_cat_name}}" Groups</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <p class="mb-0 text-center green middle">
                            <strong class="middle">{{number_format($lists->tracked_hours/60,1)}}</strong>
                        </p>
                        <p class="mb-0 text-center green ">HOUR(S)</p>
                        <p class="light text-center">of service contributed</p>
                    </div>
                    <div class="col">
                        <p class="mb-0 text-center green middle">
                            <strong class="middle">{{number_format($lists->des_tracked_hours,1)}}</strong>
                        </p>
                        <p class="mb-0 text-center green">Designated</p>
                        <p class="mb-0 text-center green">HOUR(S)</p>
                        <p class="light text-center">of contributed</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-8 border-left">
            <div class="main-text">
                <h3 class="h3"  style="text-align:center">Hours by Month</h3>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link"
                       href="#thisMonth{{$groupId}}"
                       role="tab"
                       title="This Month"
                       data-toggle="tab"><span>This Month</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#lastMonth{{$groupId}}"
                       role="tab"
                       title="Last Month"
                       data-toggle="tab"><span>Last Month</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#last6Month{{$groupId}}"
                       role="tab"
                       title="Last 6 Month"
                       data-toggle="tab"><span>Last 6 Month</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active show" href="#y2date{{$groupId}}"
                       role="tab"
                       title="last 12 months"
                       data-toggle="tab"><span>last 12 months</span></a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="thisMonth{{$groupId}}">
                    <div id="this-month-chart{{$groupId}}" class="wrapper-svg"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="lastMonth{{$groupId}}">
                    <div id="last-month-chart{{$groupId}}" class="wrapper-svg"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="last6Month{{$groupId}}">
                    <div id="last6-month-chart{{$groupId}}" class="wrapper-svg"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade in active show" id="y2date{{$groupId}}">
                    <div id="year-date-chart{{$groupId}}" class="wrapper-svg"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row border-top-0 border-left-0 border-right-0 border-bottom-0">
        <div class="col-12 pb-0">
            <div class="main-text">

                @if(round($lists->tracked_hours/60) != 0)
                    <h3 class="h3">Past Records</h3>

                    @if(in_array($lists->group_id,$lists->arr5))
                        <p class="light mb-0">Top 5 Groups in the
                            State (number
                            of hours contributed during the last
                            year)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->arr))
                        <p class="light mb-0">Top 10 Groups in the
                            Country
                            (number of hours contributed during the
                            last
                            year)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->month5))
                        <p class="light mb-0">Top 5 Groups in the
                            State (number
                            of hours contributed during the last
                            month)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->month))
                        <p class="light mb-0">Top 10 Groups in the
                            Country
                            (number of hours contributed during the
                            last
                            ymonth)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->volun5))
                        <p class="light mb-0">Top 5 Groups in the
                            State (number
                            of volunteers)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->volun))
                        <p class="light mb-0">Top 10 Groups in the
                            Country
                            (number of volunteers)</p>
                    @endif
                @endif

            </div>
        </div>
    </div>

    <div class="row align-items-center border-bottom-0">
        <div class="col-12 col-md-8">
            <div id="hours-by-opp-type{{$groupId}}" class="wrapper-svg"></div>
        </div>

        <div class="col-12 col-md-4">
            <div class="main-text">
                <p class="mb-0 text-center green middle"><strong class="big">{{$lists->total_opportunities}}</strong></p>
                <p class="mb-0 text-center green middle">Total</p>
                <p class="light text-center">Opportunities</p>

                <p class="mt-1 mb-0 text-center green big"><strong>{{number_format($lists->avg_hrs_per_oppr ,1)}}</strong></p>
                <p class="mb-0 text-center green middle">Average</p>
                <p class="light text-center">hour per opportunity</p>
            </div>
        </div>
    </div>

    <div class="row align-items-center border-bottom-0">
        <div class="col-12 col-md-7">
            <div id="top-5-opportunities-chart{{$groupId}}" class="wrapper-svg"></div>
        </div>

        <div class="col-12 col-md-5 border-left">
            <div class="main-text">
                <h3 class="h3">Opportunity List</h3>
                <div class="wrapper-table-impact">
                    <div>
                        <table class="example4">
                            @if(count($lists->opprtunities_list))
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Hours</th>
                                    <th>Volunteers</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lists->opprtunities_list as $oppr)
                                    <tr>
                                        <td>{{$oppr['name']}}</td>
                                        <td>
                                            <p class="green">{{$oppr['hours']}}</p>
                                        </td>
                                        <td>{{$oppr['volonteers']}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            @else
                                <tr>
                                    Sorry..! No Opportunity found
                                </tr>
                            @endif

                        </table>
                    </div>
                </div>
                <div class="wrapper-pagination">
                    <ul class="pagination flex-wrap justify-content-center">
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row align-items-center border-bottom-0">
        <div class="col-12 col-md-12">
            <div class="row">
                <div class="col">
                    <div id="volnt-zipcode-chart{{$groupId}}" style="min-width: 110px; height: 250px; margin: 0 auto" class="wrapper-svg"></div>
                </div>

                <div class="col">
                    <div id="volnt-agegroup-chart{{$groupId}}" style="min-width: 110px; height: 250px; margin: 0 auto" class="wrapper-svg"></div>
                </div>

                <div class="col">
                    <div class="main-text">
                        <p class="mb-0 text-center green middle"><strong class="big">{{count($lists->volunteers)}}</strong></p>
                        <p class="mb-0 text-center green middle">Total</p>
                        <p class="light text-center">Volunteers</p>

                        <p class="mt-1 mb-0 text-center green big"><strong>{{number_format($lists->volunteers_avg ,1)}}</strong></p>
                        <p class="mb-0 text-center green middle">Average</p>
                        <p class="light text-center">hours per volunteer</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row align-items-center border-bottom-0">
        <div class="col-12 col-md-7">
            <div id="top-5-volonteers-chart{{$groupId}}" class="wrapper-svg"></div>
        </div>

        <div class="col-12 col-md-5 border-left">
            <div class="main-text">
                <h3 class="h3">Volunteer List</h3>
                <div class="wrapper-table-impact">
                    <div>
                        <table class="example5">
                            @if(count($lists->volunteers))
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Hours</th>
                                    <th>Opportunities</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lists->volunteers as $member)
                                    <tr>
                                        <td>{{$member['name']}}</td>
                                        <td>
                                            <p class="green">{{$member['hours']}}</p>
                                        </td>
                                        <td>{{$member['total_oppr']}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            @else
                                <tr>
                                    There are no top vountieers yet
                                </tr>
                            @endif

                        </table>
                    </div>
                </div>
                <div class="wrapper-pagination">
                    <ul class="pagination flex-wrap justify-content-center">
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
    @endforeach
 @endif
</div>
 @endsection

@section('script')

    <script src="{{asset('front-end/js/highcharts.js')}}"></script>
    {{--<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>--}}
    <script src="{{asset('front-end/js/bootstrap-sortable.js')}}"></script>
    <script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>
    <script src="//cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.19/pagination/ellipses.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>

    @include('_groups.groups-charts-custom-js')

<script>
$(document).ready(function () {
    $('.example4').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "pageLength": 10,
                "pagingType": "full_numbers",
                "language": {
                    "paginate": {
                        "first": "",
                        "next": '',
                        "previous": '',
                        "last": ''
                    }
                }
            });
            $('.example5').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "pageLength": 10,
                "pagingType": "full_numbers",
                "language": {
                    "paginate": {
                        "first": "",
                        "next": '',
                        "previous": '',
                        "last": ''
                    }
                }
            });
});     
$(function() {
    $(".preload").fadeOut(2000, function() {
        $(".contents").fadeIn(1000);
    });
});
</script>

@endsection