@extends('layout.masterForAuthUser')
<?php
if($oppr_info->logo_img == NULL){
	$organizLogo = $oppr_info->getOrgLogoAttribute();
	if($organizLogo == NULL){
		$logo = asset('front-end/img/org/001.png');
	}else{
		$logo = $organizLogo;
	}
}else{
	$logo = $oppr_info->logo_img;
}
?>

@section('social_meta_tags')
	<meta property="og:title" content="Volunteer Opportunity: {{$oppr_info->title}}">
	<meta property="og:url" content="{{ Request::url() }}">
	<meta property="og:image" content="{{$logo}}">
	<meta property="og:description" content="<?php echo $oppr_info->description;?>">
	<meta property="og:state" content="{{$oppr_info->state}}">
	<meta property="geo.region" content="{{$oppr_info->state}}">
	<meta property="og:city" content="{{$oppr_info->city}}">
	<meta property="geo.placename" content="{{$oppr_info->city}}">
	<meta property="og:date" content="{{$oppr_info->start_date}} to {{$oppr_info->end_date}}">
	<meta property="og:qualifications" content="<?php echo $oppr_info->qualification;?>">
@endsection

@section('css')

    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap-sortable.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/multi-select.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">
    <link media="all" type="text/css" rel="stylesheet" href="{{ url('js/plugins/jquery-slick/slick.css') }}">
    <link media="all" type="text/css" rel="stylesheet" href="{{ url('js/plugins/jquery-slick/slick-theme.css') }}">

    <style>
        .time-selector .select2 {
            display: none !important;
        }

        .wrapper_select .select2 {
            width: 100% !important;
        }

        .only-mobile {
            display: none;
        }

        #social-links a,
        #social-links button {
            box-shadow: 0 6px 16px 0 rgba(0, 0, 0, 0.2);
        }

        #social-links a:hover,
        #social-links button:hover {
            background-color: #fff;
            color: #3bb44a;
            border-color: #fff;
        }

        .green {
            color: #42bd41;
        }

        .modal-open .modal {
            overflow-x: hidden !important;
            overflow-y: auto !important;
        }

        @media screen and (max-width: 600px) {
            .only-mobile {
                display: inline-block;
            }
        }

        .tab-content > .tab-pane {
            display: block;
            height: 0px;
            overflow: hidden;
        }

        .tab-content > .active {
            height: auto;
        }

        #hourError {
            float: right;
        }

        .print-header {
            display: none;
        }
    </style>

@endsection

@section('content')

    <div class="wrapper-profile-box">
        <div class="dashboard_org">
            <div class="org-info-box view-oppor-grp">
                <div class="container noPrint">

                    @if(Session::has('message'))
                        <div class="alert alert-success">
                          <strong>Success!</strong> {{Session::get('message')}}
                        </div>
                    @endif

                    @if(Session::has('error'))
                        <div class="alert alert-danger">
                          <strong>Danger!</strong> {{ Session::get('error') }}
                        </div>
                    @endif

                    <div class="row align-items-center">
                        <div class="col-12">
                            <div class="main-text">
                                <h3 class="h3">
                                    <?php echo wordwrap($oppr_info->title,35,'<br>', true); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4 col-lg-3 order-2 viewoppo-profileimg">
                            <div class="big-org-logo">
                                <span style="background-image:url('{{$logo}}')"></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-8 col-lg-9 order-1">
                            <div class="main-text">
                            <ul>  
                                <li> 
                                <p class="mb-0"><b>Time Info:</b>
                                    <!--<i data-toggle="tooltip" data-placement="top"
                                       title="{{$oppr_info->is_expired ? 'Expired' : 'Active'}}"
                                       class="fa {{$oppr_info->is_expired ? 'fa-warning' : 'fa-check-circle'}}">
                                    </i>-->
                                </p>
                                <p>{{dateMDY($oppr_info->start_date)}} to {{dateMDY($oppr_info->end_date)}}</p>
                                </li>
                                <li>
                                <p class="mb-0"><b>Hosted By:</b></p>

                                @if($user->user_role == "organization")
                                    <p><a href="{{url('/organization/show_organization/'.$oppr_info->org_id)}}">{{$oppr_info->org_name}}</a></p>
                                @else
                                    <p><a href="{{url('/volunteer/show_organization/'.$oppr_info->org_id)}}">{{$oppr_info->org_name}}</a></p>
                                @endif
                                </li>
                                @if(!empty($oppr_info->parent_id))
                                <li><p class="mb-0"><b>Parent Organization : </b></p>
                                    <p>{{ $user->parentOrgName($oppr_info->parent_id)}}</p></li>
                                @endif
                                <li><p class="mb-0"><b>Status:</b></p>
                                <p>{{$oppr_info->is_expired  ? 'Expired' :  'Active'}}</p></li>

                                <li><p class="mb-0"><b>Allow Tracking/Scheduling:</b></p>
                                <p>{{$oppr_info->allow_tracking == 1 ? 'Yes' :  'No'}}</p></li>

                                <li><p class="mb-0"><b>Maximum Concurrent Volunteers:</b></p>
                                <p>{{($oppr_info->max_concurrent_vol > 0) ? $oppr_info->max_concurrent_vol :  'Not Set'}}</p></li>
                                <li><div id="socialShare">
                                <div class="socialBox pointer">
                                    <span class="fa fa-share-alt"></span>
                                    <div id="socialGallery">
                                    <div id="social-links" class="socialToolBox">
                                    <button type="button" class="btn btn-success btn-sm rounded-circle" onclick="share_to_frnd('{{$oppr_info->id}}')" data-toggle="tooltip" data-placement="top" title="Share With Friends"><span class="fa fa-users"></span></button>
                                    <a href="sms:?body={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }}" class="btn btn-success btn-sm rounded-circle only-mobile" data-toggle="tooltip" data-placement="top" title="Phone"><span class="fa fa-phone"></span></a>
                                    <a href="mailto:?subject=Share Opportunity&body={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }}" class="btn btn-success btn-sm rounded-circle" id="myEmail_id" data-toggle="tooltip" data-placement="top" title="E-mail"><span class="fa fa-envelope"></span></a>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }} " class="social-button btn btn-success btn-sm rounded-circle" id="" data-toggle="tooltip" data-placement="top" title="Facebook"><span class="fa fa-facebook-official"></span></a>
                                    <a href="https://twitter.com/intent/tweet?text=my share text&amp;url={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }}" class="social-button btn btn-success btn-sm rounded-circle" id="" data-toggle="tooltip" data-placement="top" title="Twitter"><span class="fa fa-twitter"></span></a>
                                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }}" class="social-button btn btn-success btn-sm rounded-circle" id="" data-toggle="tooltip" data-placement="top" title="Linkedin"><span class="fa fa-linkedin"></span></a>
                                    </div>
                                    </div>
                                </div>
                                </div></li>
                                <!--<li><p class="mb-0"><b><i class="fa fa-share-alt" aria-hidden="true"></i></b></p>

                                <div id="social-links" class="mt-1 mb-2">
                                    <button type="button" class="btn btn-success btn-sm rounded-circle" onclick="share_to_frnd('{{$oppr_info->id}}')" data-toggle="tooltip" data-placement="top" title="Share With Friends"><span class="fa fa-users"></span></button>
                                    <a href="sms:?body={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }}" class="btn btn-success btn-sm rounded-circle only-mobile" data-toggle="tooltip" data-placement="top" title="Phone"><span class="fa fa-phone"></span></a>
                                    <a href="mailto:?subject=Share Opportunity&body={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }}" class="btn btn-success btn-sm rounded-circle" id="myEmail_id" data-toggle="tooltip" data-placement="top" title="E-mail"><span class="fa fa-envelope"></span></a>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }} " class="social-button btn btn-success btn-sm rounded-circle" id="" data-toggle="tooltip" data-placement="top" title="Facebook"><span class="fa fa-facebook-official"></span></a>
                                    <a href="https://twitter.com/intent/tweet?text=my share text&amp;url={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }}" class="social-button btn btn-success btn-sm rounded-circle" id="" data-toggle="tooltip" data-placement="top" title="Twitter"><span class="fa fa-twitter"></span></a>
                                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{ route('share_opportunity_view', ['id'=>$oppr_info->id]) }}" class="social-button btn btn-success btn-sm rounded-circle" id="" data-toggle="tooltip" data-placement="top" title="Linkedin"><span class="fa fa-linkedin"></span></a>
                                </div>
                                </li>-->
                            </ul>
                                @if(isset($is_member))
                                    @if($is_member == 0)
                                        <form action="{{url('api/volunteer/opportunity/join')}}" method="post">
                                            <div class="row">
                                                @if($oppr_info->private_passcode)
                                                <div class="col-12 col-md-4">
                                                    <div class="form-group mt-1">
                                                        <div class="wrapper_input">
                                                            <input placeholder="Pass Code to Join" name="passcode" class="form-control" id="passcode" type="text" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-12 col-md-4">
                                                    <div class="form-group">
                                                        <div class="">
                                                            <input type="submit" value="Join Opportunity" class="btn btn-success btn-lg">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" value="{{$oppr_info->id}}" name="oppor_id">
                                            <input type="hidden" value="{{$oppr_info->title}}" name="oppor_title">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$user->id}}" name="user_id">
                                        </form>

                                    @elseif($is_member == 2)
                                        <input type="submit" value="Pending..." class="btn btn-secondary btn-lg"
                                               disabled>

                                    @elseif($is_member == 1 )
                                        <button type="button" class="btn btn-secondary btn-lg" disabled>
                                            Join Opportunity
                                        </button>
                                        @if($oppr_info->allow_tracking == 1)
                                            <input type="submit" value="Track" class="btn btn-success btn-lg" id="btn" >
                                        @endif
                                    @else
                                        <button type="button" class="btn btn-secondary btn-lg" disabled>
                                            Join Opportunity
                                        </button>
                                        <input type="submit" value="Track" class="btn btn-success btn-lg"
                                               id="btn" disabled>
                                    @endif
                                @endif
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="text-profile-info vol-news-feedlist view-oppor-grp view-oppor-tab">
            <div class="container">
            <nav>
                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active"  id="details-tab" href="#details" role="tab" aria-controls="details" data-toggle="tab" aria-selected="true">Details</a>
                        <a class="nav-item nav-link" id="members-tab" href="#members" role="tab" aria-controls="members" data-toggle="tab" aria-selected="false">Members</a>
                        <a class="nav-item nav-link impacts_tab" id="impacts-tab" href="#impacts" role="tab" aria-controls="impacts" data-toggle="tab" aria-selected="false">Impact</a>
                </div>
            </nav>
                <div class="tab-content py-3 px-3" id="nav-tabContent">
                    <div role="tabpanel" class="tab-pane fade show active" id="details" aria-labelledby="details-tab">
                        <div class="row">
                            <div class="col-12 col-md-12">

                                <div class="main-text">
                                    <!-- <p class="title">Opportunity Details</p> -->
                                    <ul clss="">  
                                    {{--<p>@if(array_key_exists('opportunity_type',$oppr_info->toArray())){{$oppr_info->opportunity_type}}@endif</p>--}}

                                    <li><p class="bold">Opportunity Type(s):</p>
									@foreach(@$opportunity_category as $category)
										@if(!empty($oppr_info->selected_cat) && in_array($category->id, $oppr_info->selected_cat))
                                            <p>{{$category->name}}</p>
										@endif
                                    @endforeach</li>
                                    <li><p class="bold">Contact Name:</p>
                                    <p>{{$oppr_info->contact_name}}</p></li>
                                    <li><p class="bold">Contact Email:</p>
                                    <p>{{$oppr_info->contact_email}}</p></li>
                                    <li><p class="bold">Phone number:</p>
                                    <p>{{$oppr_info->contact_number}}</p></li>
                                    <li><p class="bold">Qualifications:</p>
                                    <p><?php echo $oppr_info->qualification; ?></p></li>
                                    <li><p class="bold">Description:</p>
                                    <p><?php echo $oppr_info->description; ?></p></li>
                                    </ul>
                                </div>

                            </div>
                            <div class="col-12 col-md-12">

                                <div class="dashboard_org"></div>

                                <div class="details_content">
                                    <div class="map-box" id="pos_map">
                                        <input type="hidden" id="lat_val" value="{{$oppr_info->lat}}">
                                        <input type="hidden" id="lng_val" value="{{$oppr_info->lng}}">
                                    </div>
                                    <div class="main-text viewoppo-mapdtl">
                                        <ul>
                                        <li><p class="bold">Address:</p>
                                        <p>{{$oppr_info->street_addr1}}, {{$oppr_info->city}}, {{$oppr_info->state}}
                                            , {{$oppr_info->zipcode}}</p></li>

                                        <li><p class="bold">Date:</p>
                                        <p>{{dateMDY($oppr_info->start_date)}} - {{dateMDY($oppr_info->end_date)}}</p>
                                        </li>
                                        <li><p class="bold">Time:</p>
                                        <p>{{$oppr_info->start_at}} - {{$oppr_info->end_at}}</p>
                                        </li>
                                        <li><p class="bold">Days of Week:</p>
                                        <?php $temp = str_replace(",",", ",$oppr_info->weekdays); ?>
                                        <p>{{$temp}}</p></li>
                                        <li><p class="bold">Minimum Age:</p>
                                        <p>{{$oppr_info->min_age}}</p></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="members" aria-labelledby="members-tab">
                        <div class="wrapper-friends">
                            <div class="wrapper-friends-list pb-0 pt-0">
                                <div class="wrapper-sort-table tesposive-table">
                                    <div>
                                        <table id="example" class="table sortable">
                                            <!-- <div class="heading-mobile-view">
                                                <p>Member Details</p>
                                            </div> -->
                                            <thead>
                                            <tr>
                                                <th scope="col">
                                                    Member Name
                                                </th>
                                                @if(isset($user))
                                                    @if($user->user_role == "organization")
                                                    <th scope="col">
                                                        Email
                                                    </th>
                                                    <th scope="col">
                                                        Phone
                                                    </th>
                                                    @endif
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($oppr_info->opportunity_member as $om)
                                                <tr class="for-desktop-view">

                                                    <?php $member_info = \App\User::find($om->user_id); ?>

                                                    @if(isset($friend))

                                                        @if(in_array($member_info->id,$friend) || $member_info->id == $user->id)

                                                        <td data-label="Member Name">{{$member_info->first_name}} {{$member_info->last_name}}</td>

                                                        @else

                                                        <td data-label="Member Name"> {{$member_info->first_name}}  <?php echo(substr($member_info->last_name, 0, 1)); ?> </td>

                                                        @endif

                                                        @if($user->user_role == "organization")

                                                        <td data-label="Email">{{$member_info->email}}</td>

                                                        <td data-label="Phone">{{$member_info->contact_number}}</td>

                                                        @endif

                                                        @else
                                                            <td>{{$member_info->first_name}} {{$member_info->last_name}}</td>
                                                            <td>{{$member_info->email}}</td>
                                                            <td>{{$member_info->contact_number}}</td>
                                                        @endif

                                                </tr>

                                                <tr class="for-mobile-view">

                                                    <?php $member_info = \App\User::find($om->user_id); ?>

                                                    @if(isset($friend))

                                                        @if(in_array($member_info->id,$friend) || $member_info->id == $user->id)

                                                        <td data-label="Member Name">{{$member_info->first_name}} {{$member_info->last_name}}</td>

                                                        @else

                                                        <td data-label="Member Name"> {{$member_info->first_name}}  <?php echo(substr($member_info->last_name, 0, 1)); ?> </td>

                                                        @endif

                                                        @if($user->user_role == "organization")

                                                        <td data-label="Email"><i class="fa fa-envelope-o" aria-hidden="true"></i>{{$member_info->email}}</td>

                                                        <td data-label="Phone"><i class="fa fa-phone-square" aria-hidden="true"></i>{{$member_info->contact_number}}</td>

                                                        @endif

                                                        @else
                                                            <td>{{$member_info->first_name}} {{$member_info->last_name}}</td>
                                                            <td><i class="fa fa-envelope-o" aria-hidden="true"></i>{{$member_info->email}}</td>
                                                            <td><i class="fa fa-phone-square" aria-hidden="true"></i>{{$member_info->contact_number}}</td>
                                                        @endif

                                                </tr>
                                            @endforeach
                                            </tbody>
                               
                                        </table>



                                        <!-- <table id="example" class="table sortable mobile-table">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Member Details
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($oppr_info->opportunity_member as $om)
                                                <tr>

                                                    <?php $member_info = \App\User::find($om->user_id); ?>

                                                    @if(isset($friend))

                                                        @if(in_array($member_info->id,$friend) || $member_info->id == $user->id)

                                                        <td><strong>{{$member_info->first_name}} {{$member_info->last_name}}</strong><br>

                                                        @else

                                                        <strong>{{$member_info->first_name}}  <?php echo(substr($member_info->last_name, 0, 1)); ?></strong><br>

                                                        @endif

                                                        @if($user->user_role == "organization")

                                                        {{$member_info->email}}<br>

                                                        {{$member_info->contact_number}}</td>

                                                        @endif

                                                    @else
                                                        <td><strong>{{$member_info->first_name}} {{$member_info->last_name}}</strong><br>
                                                        {{$member_info->email}}<br>
                                                        {{$member_info->contact_number}}</td>
                                                    @endif

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="impacts" aria-labelledby="impacts-tab">
                        <div class="wrapper-impact pb-0 pt-0  ">
                            <div class="impact-tab-dtl">
                                <!--<div class="row">
                                    <div class="col print-header">
                                        <img src="{{asset('front-end/img/logo.png')}}">
                                        <h2 class="h3">Opportunity Impact</h2>
                                    </div>
                                </div>-->
                                <div class="row align-items-center border-bottom-0 view-oppoimpact-head">
                                    <div class="col noPrint">
                                        <div class="main-text">
                                            <h2 class="h3">Opportunity Impact
                                                @if(session('admin_role')=='admin' && env('APP_ENV') == 'local' )
                                                    <a class="" href="{{route('addTestOpprData' ,$oppr_info->id)}}"><i class="fa fa-refresh " aria-hidden="true"> Load Test Data</i></a>
                                                @endif
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="col col-auto">
                                        <a href="javascript:void(0)"
                                           onclick="window.print()"
                                           class="print">
                                            <i class="fa fa-print" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="row align-items-center border-bottom-0 view-oppo-imp-dtl">
                                    <div class="col-12 col-md-12 pb-0">
                                        <img style="max-width: 45%" src="{{url($logo)}}" class="img-thumbnail mx-auto d-block impact-view-space" alt="Opportunity Image">

                                        <div class="main-text">
                                            <h3 class="h3 text-center">Ranked</h3>

                                            <section class="slider orgRankingSlick">
                                                @foreach($impactsData['ranking_categories'] as $rankCat)
                                                    <div class="">
                                                        <div class="row">
                                                            <div class="col text-center">
                                                                <p class="mb-0 text-center green middle">
                                                                    <strong class="big">{{$impactsData['ranking_all']}}<sup>{{numberDescender($impactsData['ranking_all'])}}</sup></strong>
                                                                </p>
                                                                <div class="impact-hour-dtl"><p class="light text-center">Of All Opportunities</p></div></div>
                                                            <div class="col text-center">
                                                                <p class="mb-0 text-center green middle">
                                                                    <strong class="big">{{$rankCat['rank']}}<sup>{{numberDescender($rankCat['rank'])}}</sup></strong>
                                                                </p>
                                                                <div class="impact-hour-dtl"><p class="light text-center">Of "{{$rankCat['ranking_category']}}" Opportunities</p></div>
                                                            </div>
                                                            <div class="col text-center">
                                                                <p class="mt-1 mb-0 text-center green middle"><strong class="big">{{number_format($impactsData['tracked_hours'],1)}}</strong></p>
                                                                <div class="impact-hour-dtl"><p class="mb-0 text-center middle">HOUR(S)</p>
                                                                <p class="light text-center">of service contributed</p></div>
                                                            </div>
                                                    </div>
                                                @endforeach
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center border-bottom-0 view-oppo-imp-dtl">
                                    <div class="col-12 col-md-12">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link" href="#thisMonth" role="tab" data-toggle="tab"><span >This Month</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#lastMonth" role="tab" data-toggle="tab"><span >Last Month</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#last6Month" role="tab" data-toggle="tab"><span>Last 6 month</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link  active show" href="#y2date" role="tab" data-toggle="tab"><span>Last 12 Months</span></a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade" id="thisMonth">
                                                <div id="this-month-chart" class="wrapper-svg"></div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="lastMonth">
                                                <div id="last-month-chart" class="wrapper-svg"></div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="last6Month">
                                                <div id="last6-month-chart" class="wrapper-svg"></div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade  in active show" id="y2date">
                                                <div id="year-date-chart" class="wrapper-svg"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row align-items-center border-bottom-0 pie-charts">
                                    <div class="col-12 col-md-12">
                                        <div class="row">
                                            <div class="col">
                                                <div id="volnt-zipcode-chart" class="wrapper-svg"></div>
                                            </div>

                                            <div class="col">
                                                <div id="volnt-agegroup-chart" class="wrapper-svg"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col text-center">
                                                <div class="main-text">
                                                    <p class="mb-0 text-center green middle"><strong class="big">{{count($impactsData['volunteers'])}}</strong></p>
                                                    <p class="mb-0 text-center green middle">Total</p>
                                                    <p class="light text-center">volunteer</p>

                                                    <p class="mt-1 mb-0 text-center green big"><strong class="big">{{$impactsData['volunteers_avg']}}</strong></p>
                                                    <p class="mb-0 text-center green middle">Average</p>
                                                    <p class="light text-center">of service contributed</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row align-items-top border-bottom-0 list-vol-impc">
                                    <div class="col-12 col-md-6 pb-0">
                                        <div id="top-5-volonteers-chart" class="wrapper-svg"></div>
                                    </div>

                                    <div class="col-12 col-md-6 pb-0 border-left">
                                        <div class="main-text">
                                            <h3 class="h3 text-center">Volunteer List</h3>
                                            <div class="wrapper-table-impact">
                                                <div class="wrapper-sort-table tesposive-table">
                                                    <table class="desktop-table">
                                                        @if(count($impactsData['volunteers']))
                                                        <thead>

                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Hours</th>
                                                            <th>Location</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($impactsData['volunteers'] as $array)
                                                            <tr>
                                                                <td>{{$array['name']}}</td>
                                                                <td>{{$array['hours']}}</td>
                                                                <td>{{$array['location']}}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                        @else
                                                            <tr>
                                                                <td class="text-center">Sorry..! No volunteers found</td>
                                                            </tr>
                                                        @endif

                                                    </table>
                                                    <table class="mobile-table">
                                                        @if(count($impactsData['volunteers']))
                                                        <thead>
                                                        <tr>
                                                            <th>Volunteer Detail</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($impactsData['volunteers'] as $array)
                                                            <tr>
                                                                <td><strong>{{$array['name']}}</strong><br>
                                                                {{$array['hours']}} Hrs<br>
                                                                <i class="fa fa-map-marker" aria-hidden="true"></i> {{$array['location']}}
                                                            </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                        @else
                                                            <tr>
                                                                Sorry..! No volunteers found
                                                            </tr>
                                                        @endif

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal modal-popups fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Share with Friends</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form class="form-group" action="{{ route('opportunity-share-to-friends') }}" method="post">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="control form-group select-text">
                            <div class="wrapper_select">
                                <input type="hidden" name="opportunity_id" value="" class="opportunity_id">
                                <select name="friend_ids[]" id="friend_ids" class="form-control custom-dropdown select2"
                                        multiple required>
                                    @foreach(@$all_friends as $key => $frnd)
                                        <option value="{{ $frnd->friend_id }}">
                                            {{$frnd->user_role == 'volunteer' ? $frnd->first_name. " ".$frnd->last_name : $frnd->org_name}}
                                        </option>
                                    @endforeach
                                </select>
                                <label class="label-text">Select Friends</label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="share_to_frnd" class="button-fill pull-right">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="myModalEmail" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Share with Friends</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="label-text">Email</label>
                        <div class="wrapper_select">
                            <input type="email" name="friend_ids_email" id="friend_ids_email" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" value="Share" name="share_to_frnd_email" class="btn btn-success pull-right"
                           id="share_to_frnd_email">
                </div>
            </div>
        </div>
    </div>

    @if(isset($timeBlocks))
    {{--todo: show modal only if the volunteer is the MEMBER and can track hours--}}

    <div class="modal fade" id="add_hours1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
            <div class="modal-dialog-fix">
                <div class="track-add-hour">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="main-text">
                                <h2 class="h2">Add Hours</h2>
                            </div>

                            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                <span>&times;</span>
                            </a>
                        </div>
                        <div class="modal-body ">

                            <div class="form-group  opp_select_div">
                                <label class="label-text">Opportunity: <b> {{$oppr_info->title}}</b></label>
                            </div>

                            <div class="mb-2" style="color: darkgrey">
                                @include('volunteer.layout._opporInfoHtml' ,$oppr_info)
                            </div>

                            <div class="row mb-2">
                                <div class="col-sm-12 col-md-5">
                                    <div class="form-group">
                                        <div class="wrapper-checkbox">
                                            <label>
                                                <input type="checkbox" name="is_designated" id="is_designated">
                                                <i></i>
                                                <span class="label-checkbox-text">Designate hours for group? </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-7">
                                    <div id="designated_group_id" style="display: none">
                                        <select name="designated_group_id" id="designated_group_id_select" class="form-control">
                                            <option value="">Select any group</option>
                                            @foreach($groups_list as $group)
                                                <option value="{{$group->id}}">{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                        <p class="p_invalid" id="empty_designated_group_id" style="display: none">Please Select Group!</p>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-12 col-lg-8 time-selector">
                                        <label class="label-text">Select Timeblock:</label>
                                        <select id="time_block" multiple='multiple'>
                                            @foreach($timeBlocks as $key => $block)
                                                <option value="{{$key}}">{{$block}}</option>
                                            @endforeach
                                        </select>
                                        <span id="hourError"></span>
                                    </div>

                                    <div class="col-12 col-lg-4">
                                        <div class="form-group">
                                            <label class="label-text">Date:</label>
                                            <div class="wrapper_input fa-icons">
                                                <input onkeydown="return false" name="date" data-date-end-date="0d" id="selected_date" class="form-control" type="text" required >
                                                <span class="focus-border"></span>
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </div>
                                            <span id="dayError"></span>
                                            <label class="label-text" style="padding-top: 20px">Logged Mins:</label>
                                            <p id="hours_mins" class="mt-15">(0min)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="label-text">Comments:</label>
                                <div class="wrapper_input">
                                    <textarea name="adding_hours_comments" id="adding_hours_comments" placeholder=""></textarea>
                                </div>
                            </div>

                            <div class="row" id="img_spin" style="display: none">
                                <img class="mx-auto d-block" src="{{asset('img/loading.gif')}}" style="height: 60px" />
                            </div>

                        </div>

                        <div class="modal-footer">

                            <div class="wrapper_row_link">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="wrapper-link two-link">
                                            <a style="display: none" href="#" id="btn_remove_hours"><span>Remove</span></a>
                                            <a href="#" class="white close_button"><span>Close</span></a>
                                            <a id="btn_add_hours" href="#"><span>Save</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="opp_id" value="{{$oppr_info->id}}">
                    <input type="hidden" id="opp_name" value="{{$oppr_info->title}}">
                    <input type="hidden" id="is_edit" value="0">
                    <input type="hidden" id="track_id" value="0">
                    <input type="hidden" id="is_from_addhour" value="0">
                    <input type="hidden" id="is_no_org" value="0">
                    <input type="hidden" id="is_link_exist" value="0">
                    <input type="hidden" id="org_email" value="{{$oppr_info->contact_email}}">
                    <input type="hidden" id="start_date" value="{{$oppr_info->start_date}}">
                    <input type="hidden" id="end_date" value="{{$oppr_info->end_date}}">
                    <input type="hidden" id="weekdays" value="{{$oppr_info->weekdays}}">

                </div>
            </div>
        </div>
    </div>
    @endif
    
@endsection

@section('script')
    <script src="{{ asset('js/share.js') }}"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es"></script>
    <script src="<?=asset('js/plugins/dataTables/jquery.dataTables.js')?>"></script>
    <script src="{{asset('front-end/js/moment.min.js')}}"></script>
    <script src="<?=asset('js/plugins/fullcalendar/moment.min.js')?>"></script>
    <script src="<?=asset('js/plugins/fullcalendar/fullcalendar.min.js')?>"></script>
    <script src="<?=asset('js/jquery-ui.custom.min.js')?>"></script>
    <script src="<?=asset('js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?=asset('js/plugins/dataTables/jquery.dataTables.js')?>"></script>
    <script src="{{asset('front-end/js/bootstrap-sortable.js')}}"></script>
    <script src="{{asset('js/tracking-action.js')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>--}}
    <script src="{{asset('js/jquery.multi-select.js')}}"></script>
    <script src="{{asset('front-end/js/highcharts.js')}}"></script>
    <script src="{{ url('js/plugins/jquery-slick/slick.min.js') }}"></script>
    <script src="{{asset('front-end/js/jquery-ui.js')}}"></script>

    <script>
        Highcharts.chart('this-month-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '12px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Tracking',
                color: '#03a9f4',
                data: [
                   @if(count($impactsData['this_month_chart'])>0)
                      @foreach($impactsData['this_month_chart'] as $key => $single)
                         ['{{$key}}', {{$single}}],
                      @endforeach
                    @endif
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('last-month-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '12px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Tracking',
                color: '#03a9f4',
                data: [
                    @if(count($impactsData['last_month_chart'])>0)
                        @foreach($impactsData['last_month_chart'] as $key => $single)
                            ['{{$key}}', {{$single}}],
                        @endforeach
                    @endif
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('last6-month-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '12px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Tracking',
                color: '#03a9f4',
                data: [
                    @if(count($impactsData['last_6month_chart'])>0)
                        @foreach($impactsData['last_6month_chart'] as $key => $single)
                            ['{{$key}}', {{$single}}],
                        @endforeach
                    @endif
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('year-date-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '12px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Tracking',
                color: '#03a9f4',
                data: [
                    @if(count($impactsData['year_date_chart'])>0)
                        @foreach($impactsData['year_date_chart'] as $key => $single)
                            ['{{$key}}', {{$single}}],
                        @endforeach
                    @endif
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });
    </script>

    <script>

        var allTop5Headings = {!! json_encode($impactsData['top_5_vol_chart']['heading']) !!}
        var volPieChartZip = {!! json_encode($impactsData['volunteers_pie_zip']) !!}
        var volPieChartAge = {!! json_encode($impactsData['volunteers_pie_age']) !!}

        Highcharts.chart('top-5-volonteers-chart', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Top 5 Volunteers',
                style:{ "color": "#27282f", "fontSize": "24px",  "fontFamily": 'Open Sans, sans-serif' }
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                lineWidth:0,
                tickLength:0,
                categories: [
                    @foreach($impactsData['top_5_vol_chart']['heading'] as $ky)
                        '{{removeNumFromStart($ky)}}',
                    @endforeach
                ],

                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#28292e'
                    }
                }
            },
            yAxis: {
                visible:false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontSize: '14px',
                            fontFamily: 'Open Sans, sans-serif',
                            fontWeight: 'normal',
                            color: '#fff',
                            textOutline: ''
                        } ,
                        align: 'right'
                    },
                    showInLegend: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            series: [{
                color: '#42bd41',
                data: [
                        @if(count($impactsData['top5Chart'])>0)
                        @foreach($impactsData['top5Chart'] as $key => $val)
                           {{$val}},
                        @endforeach
                        @endif
                ],

                dataLabels:{
                    format:'{y} hrs'
                }
            }]
        });

        Highcharts.chart('volnt-agegroup-chart', {
            chart: {

                type: 'pie'
            },
            title: {
                text: 'Volunteers by Age Group'
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + ' volunteers</b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                itemStyle:{
                    color:'#9ca0a1',
                    fontSize:'14px',
                    fontWeight:'normal',
                    fontFamily: 'Open Sans, sans-serif',
                    textOverflow:'ellipsis'
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: volPieChartAge
            }]
        });

        Highcharts.chart('volnt-zipcode-chart', {
            chart: {

                type: 'pie'
            },
            title: {
                text: 'Volunteers by Zip Code'
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + ' volunteer</b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                itemStyle:{
                    color:'#9ca0a1',
                    fontSize:'14px',
                    fontWeight:'normal',
                    fontFamily: 'Open Sans, sans-serif',
                    textOverflow:'ellipsis'
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: volPieChartZip
            }]
        });
    </script>

    <script>

        $('#is_designated').on('click', function () {

            if ($(this).is(':checked')) {
                $('#is_designated').val(1);
                $("#designated_group_id").show();
            }
            else {
                $('#is_designated').val(0);
                $("#designated_group_id").hide();
            }
        });

        function inProgress() {
            $('.track-add-hour').addClass("inProgress");
            $('#btn_add_hours').text("Please Wait...!");
            $('#img_spin').show();
        }

        function releaseProgress() {
            $('.track-add-hour').removeClass("inProgress");
            $('#img_spin').hide();
        }

        function share_to_frnd(id) {
            $(".opportunity_id").val(id);
            $("#myModal").modal('show');
        }

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();

            // Slick in multiple tabs
            $('.impacts_tab').on('click', function (e) {
                $('.orgRankingSlick').slick('refresh');
                $('.orgRankingSlick').slick('setPosition', 0);

            });

            $(".orgRankingSlick").slick({
                dots: true,
                infinite: true,
                fade: true,
                cssEase: 'linear',
                autoplay: true,
                autoplaySpeed: 2000
            });
        })

        $('.select2').select2();

        $('#time_block').multiSelect({
            afterSelect: function (values) {
                var mins = $('#time_block').val().length * 30;
                $('#hours_mins').text("(" + mins + "mins)");
            },
            afterDeselect: function (values) {
                var mins = $('#time_block').val().length * 30;
                $('#hours_mins').text("(" + mins + "mins)");
            }
        });

        function DisableWeekDays(date) {

            var weekenddate = $.datepicker.noWeekends(date);

            // In order to disable weekdays, we will invert the value returned by noWeekends functions.
            // noWeekends return an array with the first element being true/false.. So we will invert the first element

            var disableweek = [!weekenddate[0]];
            return disableweek;
        }

        $(function () {
            $(".wrapper_input.fa-icons input").datepicker({
                beforeShowDay: DisableWeekDays
            });
        });

        $('.wrapper_input.fa-icons input').datepicker({
            'format': 'mm-dd-yyyy',
            'autoclose': true,
            'orientation': 'right',
            'todayHighlight': true,
            'startDate': format($('#start_date').val()),
            'endDate': format($('#end_date').val())
        });

        $(".wrapper_input.fa-icons input").datepicker({
            beforeShowDay: function (date) {
                return [date.getDay() == 3 || date.getDay() == 4, ""]
            }
        });

        $('.wrapper_input.fa-icons i').click(function () {
            $('.wrapper_input.fa-icons input').datepicker('show');
        });

        function format(inputDate) {
            var date = new Date(inputDate);
            return date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
        }

        var latLng = new google.maps.LatLng(parseFloat($('#lat_val').val()), parseFloat($('#lng_val').val()));

        var myOptions = {
            zoom: 16,
            center: latLng,
            styles: [{
                "featureType": "water",
                "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]
            }, {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#808080"}, {"lightness": 54}]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ece2d9"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ccdca1"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#767676"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#ffffff"}]
            }, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]
            }, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.sports_complex",
                "stylers": [{"visibility": "on"}]
            }, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.business",
                "stylers": [{"visibility": "simplified"}]
            }],
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("pos_map"), myOptions);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon: '{{asset('front-end/img/pin.png')}}'
        });

        $('#btn').click(function () {
            $('#add_hours1').modal('show');
        });

        function activityChange() {
            var range = $('#action_range').val();
            var url = API_URL + 'volunteer/track/activityView';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var type = "get";
            var formData = {range: range};

            $.ajax({
                url: url,
                type: type,
                data: formData,
                success: function (data) {
                    $('#activity_table_del').remove()
                    $('.pag').remove()
                    $('.conteiner-activity-table').append(data)
                }
            });
        }

        $(document).ready(function () {
            var display = $('.footable-page-arrow:nth-last-child(3)').css('display')

            if (display == 'none') {
                $('.footable-page-arrow:last-child').css('display', 'none')
            }
            else {
                $('.footable-page-arrow:last-child').css('display', 'block')
            }

            if (parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
            else $('.footable-page-arrow:last').show();
            $(document).on('click', '.footable-page, .footable-page-arrow', function () {

                var display = $('.footable-page-arrow:nth-child(3)').css('display')

                if (display == 'none') {
                    $('.footable-page-arrow:first-child').css('display', 'none')
                }
                else {
                    $('.footable-page-arrow:first-child').css('display', 'block')
                }

                if (parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
                else $('.footable-page-arrow:last').show();
            });

            $('.modal').css({'position': 'fixed', 'overflow': 'hidden'})
            activityChange();

            function activityChange() {
                var range = $('#action_range').val();
                var url = API_URL + 'volunteer/track/activityView';

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var type = "get";
                var formData = {range: range};

                $.ajax({
                    url: url,
                    type: type,
                    data: formData,
                    success: function (data) {
                        $('#activity_table_del').remove();
                        $('.pag').remove();
                        $('.conteiner-activity-table').append(data)
                    }
                });

                var $outerwidth = $('.row-header header .outer-width-box');
                var $innerwidth = $('.row-header header .inner-width-box');

                function checkWidth() {

                    var outersize = $outerwidth.width();
                    var innersize = $innerwidth.width();

                    if (innersize > outersize) {

                        $('body').addClass("navmobile");

                    } else {
                        $('body').removeClass("navmobile");
                        $('body').removeClass("offcanvas-menu-show");
                    }
                }

                checkWidth();
                $(window).resize(checkWidth);

                $('.offcanvas-menu-backdrop').on('click', function (e) {
                    $('body').toggleClass("offcanvas-menu-show");
                    e.preventDefault();
                });

                $('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
                    $('body').toggleClass("offcanvas-menu-show");
                    e.preventDefault();
                });
            }


            $('body').on('click', '.pag > .pagination > .page-item > .page-link', function (e) {
                e.preventDefault()
                if (!($(this).parent().hasClass('active'))) {
                    console.log()
                    var range = $('#action_range').val();
                    var url = $(this).attr('href');

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var type = "get";
                    var formData = {range: range};

                    $.ajax({
                        url: url,
                        type: type,
                        data: formData,
                        success: function (data) {
                            $('#activity_table_del').remove();
                            $('.pag').remove();
                            $('.conteiner-activity-table').append(data)
                        }
                    });
                }
            })

            $('#action_range').change(function () {
                activityChange()
            })

            $('#add_on_addtime').on('click', function () {
                $('#is_from_addhour').val(1);
                $(".select_organization").select2('val', '');
                $('#org_emails').val('');
                $('.org_email_div').hide();
                $('.opp_div').hide();
                $('.opp_private_div').hide();
                $('#opp_date_div').hide();
                $('#org_not_exist').attr('checked', false);
                $('#opp_not_exist').attr('checked', false);
                $('#org_name').attr('disabled', false);
                $('#opp_name').attr('disabled', false);
                $('#private_opp_name').val('');
                $('#end_date').val('');
            });

            $('.add_opportunity_dlg').on('click', function () {

                $('#is_from_addhour').val(0);
                $('#is_no_org').val(0);
                $(".select_organization").select2('val', '');
                $('#org_emails').val('');
                $('.private_opp_div').hide();
                $('.org_email_div').hide();
                $('.opp_div').hide();
                $('.opp_private_div').hide();
                $('#opp_date_div').hide();
                $('#org_not_exist').attr('checked', false);
                $('#opp_not_exist').attr('checked', false);
                $('#org_name').attr('disabled', false);
                $('#opp_name').attr('disabled', false);
            });

            $(".add_opportunity_plus").click(function () {
                $('select').select2("enable");
                $('#org_name').prop('selectedIndex', 0);
                $('#select2-org_name-container').text('Select an Organization')
                $('#opportunity_exist_alert').hide()
                $('#private_org_name').val('')
                $('#opp_date_div').hide()
                $('.org_does_exist').show()
                $('#private_opp_name').val('')
                $('#org_not_exist').click()
                $('#wrapper_div').hide()
                $('#is_from_addhour').val(0);
                $('#is_no_org').val(0);
                $('.org_not_exist').show()
                $('.org_private_div').hide()
                $('.opp_private_div').hide()
                $('.opp_div').hide()
                $('#org_emails').val('')
                $('#end_date').val('')
                $('#checkbox_div').prop('checked', false);
                $('#opp_not_exist').prop('checked', false);

                $('#add_opportunity').modal('show');
            });


            $('#btn_add_hours').on('click', function (e) {
                $(".ms-selection").css("border", "");
                $('#dayError,#hourError').hide();
                e.preventDefault();

                var selected_date = $('#selected_date').val();
                selected_date = selected_date.slice(0, 10);

                var opp_id = $('#opp_id').val();
                var is_designated = $('#is_designated').val();
                var designated_group_id = $('#designated_group_id_select').val();

                var opp_name = $('#opp_name').val();
                var logged_mins = $('#hours_mins').text();
                logged_mins = (logged_mins.slice(1)).slice(0, -5);

                var adding_hours_comments = $('#adding_hours_comments').val();
                var tracking_id = $('#track_id').val();
                var org_email = $('#org_email').val();
                var is_edited = $('#is_edit').val();
                var is_no_org = $('#is_no_org').val();
                var timeval = $('#time_block').val().toString().split(',');
                var start_time = timeval[0];
                var end_time = timeval[timeval.length - 1];

                end_time = addMinutesToTime(end_time, 30);
                var time_block = $('#time_block').val().toString();

                if (is_designated == "1" && designated_group_id == "") {
                    toastr.error("Please select any group first.", "Error");
                    return;
                }

                if ($('#time_block').val() == "") {
                    toastr.error("Please select time blocks.", "Error");
                    return;
                }
                if ($('#selected_date').val() == "") {
                    toastr.error("Please select date you want add hours on.", "Error");
                    return;
                }

                if (is_no_org == 1) {
                    opp_id = 'private';
                }

                var restDay = getValidatedTrackDay('0');

                if (restDay == 0) {
                    inProgress();
                    $('#btn_add_hours').prop('disabled', true);

                    var url = API_URL + 'volunteer/track/addHours';

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var type = "post";
                    var formData = {
                        opp_id: opp_id,
                        is_designated: is_designated,
                        designated_group_id: designated_group_id,
                        opp_name: opp_name,
                        start_time: start_time,
                        end_time: end_time,
                        logged_mins: logged_mins,
                        selected_date: selected_date,
                        comments: adding_hours_comments,
                        is_edit: is_edited,
                        tracking_id: tracking_id,
                        time_block: time_block,
                        is_no_org: is_no_org,
                        private_opp_name: null,
                        org_email: org_email
                    };

                    $.ajax({
                        type: type,
                        url: url,
                        data: formData,
                        success: function (data) {
                            toastr.success("Hours added Successfully!", "Message");
                            $("#add_hours").modal('hide');
                            location.reload();
                        },
                        error: function (data) {
                            releaseProgress();
                            toastr.error("Something Went Wrong...!", "error");
                            console.log('Error:', data);
                        }
                    });
                }
            });

            function addMinutesToTime(time, minsAdd) {
                function z(n) {
                    return (n < 10 ? '0' : '') + n;
                }
                var bits = time.split(':');
                var mins = bits[0] * 60 + +bits[1] + +minsAdd;
                return z(mins % (24 * 60) / 60 | 0) + ':' + z(mins % 60);
            }

            $(".ms-selection").css("border", "");
            $('#dayError,#hourError').hide();


            function getValidatedTrackDay(flags) {

                var formData = {
                    addDate: $('#selected_date').val(),
                    oppId: $('#opp_id').val()
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: API_URL + 'volunteer/track/validatedTrackDate',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#dayError').hide();
                        $("#selected_date").css("border", "");
                        if (data != "success") {

                            $("#selected_date").css("border", "1px solid #ff0000");
                            $('#dayError').css('color', '#ff0000');
                            $('#dayError').show();
                            $('#dayError').html(data);
                            flags++;
                        } else {
                            flags = 0;
                        }
                    }
                });
                if ($('#time_block').val().length > 0 && flags == 0) {
                    var resultFlag = getValidatedMaxVolunteer('0');
                    if (resultFlag == '0') {
                        return getValidatedTrackHours('0');
                    } else {
                        return 1;
                    }
                }
                return flags;
            }

            function getValidatedTrackHours(flags) {

                var formData = {
                    addDate: $('#selected_date').val(),
                    oppId: $('#opp_id').val(),
                    time_block: $('#time_block').val().toString(),
                    track_id: '0',
                    is_edit: '0'
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: API_URL + 'volunteer/track/validatedTrackHours',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#hourError').hide();
                        $(".ms-selection").css("border", "");
                        if (data != "success") {

                            $(".ms-selection").css("border", "1px solid #ff0000");
                            $('#hourError').css('color', '#ff0000');
                            $('#hourError').show();
                            $('#hourError').html(data);
                            toastr.error("Selected time is overlapped.", "Error");
                            flags++;
                        } else {
                            flags = 0;
                        }

                    }
                });
                return flags;
            }

            function getValidatedMaxVolunteer(flags) {

                var formData = {
                    addDate: $('#selected_date').val(),
                    oppId: $('#opp_id').val(),
                    time_block: $('#time_block').val().toString(),
                    track_id: '0',
                    is_edit: '0'
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: API_URL + 'volunteer/track/validatedMaxVolunteer',
                    data: formData,
                    async: false,
                    success: function (data) {

                        $('#hourError').hide();
                        $(".ms-selection").css("border", "");

                        if (data != "success") {

                            $(".ms-selection").css("border", "1px solid #ff0000");
                            $('#hourError').css('color', '#ff0000');
                            $('#hourError').show();
                            $('#hourError').html(data);
                            toastr.error(data, "Error");
                            flags++;
                        } else {
                            flags = 0;
                        }

                    }
                });
                return flags;
            }

            var $outerwidth = $('.row-header header .outer-width-box');
            var $innerwidth = $('.row-header header .inner-width-box');

            function checkWidth() {

                var outersize = $outerwidth.width();
                var innersize = $innerwidth.width();
                if (innersize > outersize) {
                    $('body').addClass("navmobile");
                } else {
                    $('body').removeClass("navmobile");
                    $('body').removeClass("offcanvas-menu-show");
                }
            }

            checkWidth();
            $(window).resize(checkWidth);

            $('.offcanvas-menu-backdrop').on('click', function (e) {
                $('body').toggleClass("offcanvas-menu-show");
                e.preventDefault();
            });

            $('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
                $('body').toggleClass("offcanvas-menu-show");
                e.preventDefault();
            });

            $('.wrapper_input.fa-icons input').datepicker({
                'format': 'mm-dd-yy',
                'autoclose': true,
                'orientation': 'right',
                'todayHighlight': true
            });
            $('.wrapper_input.fa-icons input').datepicker( "option", "dateFormat", "mm-dd-yy");

            $("#checkbox_div").click(function () {
                console.log($(this).is(":checked"))
                if ($(this).is(":checked")) {
                    $('.org_email_div').show()
                    $('#wrapper_div').show()
                    $('.org_private_div').show()
                    $('select').select2("enable", false);
                    $('#opp_date_div').show()
                } else {
                    $('#opp_date_div').hide()
                    $('#wrapper_div').css("display", "none");
                    $('select').select2("enable");
                }
            });

            $('body').on('click', '#add_on_addtime', function () {
                $('#add_hours').modal('hide');
                setTimeout(function () {
                    $('.add_opportunity_plus').click()
                }, 200);
            });

            $('body').on('click', '.close_button', function () {
                $('.close').click();
            });
        });

        $(document).on('click', '.fc-more', function () {
            var top = $('.fc-more-popover').css("top");
            var number = top.substring(0, 3)
            var px = number + 'px';
            $('.fc-more-popover').css("top", px);
        });

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        document.addEventListener('DOMContentLoaded', function () {

            var calendarEl = document.getElementById('calendar');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{route('volunteer-track-view-tracks')}}',
                success: function (data) {

                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },

                        defaultView: 'agendaWeek',
                        height: 700,
                        defaultDate: date,
                        navLinks: true, // can click day/week names to navigate views
                        editable: true,
                        eventLimit: true, // allow "more" link when too many events
                        events: data,

                        eventClick: function (calEvent, jsEvent, view) {
                            $('#btn_remove_hours').show()
                            $('#opp_id').prop('disabled', false);
                            $('.opp_select_div').show()
                            $('.private_opp_div_add_hours').hide()
                            $('#is_edit').val(1);
                            $('#track_id').val(calEvent.id);
                            $('#btn_remove_hours').show();
                            $('#is_no_org').val(0);
                            $('#adding_hours_comments').val(calEvent.comments);
                            $('.private_opp_div').hide();
                            $('.opp_select_div').show();
                            $('.confirmed_track').hide();
                            $('.add_hours_modal_title').text("Do you want make Change?");
                            if (calEvent.opp_id == 0) {
                                $('.opp_select_div').hide();
                                $('.private_opp_div').show();
                                $('#private_opp_name_hours').val(calEvent.title);
                                $('#is_no_org').val(1);
                                $('.private_opp_div_add_hours').show();
                            }
                            if (calEvent.link == 1) {
                                $('#is_link_exist').val(1);
                            } else {
                                $('#is_link_exist').val(0);
                            }
                            var startDate = calEvent.start.format("YYYY-MM-DD HH:mm");
                            $('#selected_date').val(startDate.slice(0, 11));
                            if (calEvent.end == null) {
                                var new_date = startDate.slice(0, 11);
                                var new_hour = startDate.slice(11, 13);
                                var new_mins = startDate.slice(13);
                                new_hour = parseInt(new_hour) + 2;
                                if (new_hour < 10)
                                    new_hour = '0' + new_hour;

                                var endDate = new_date + new_hour + new_mins;
                            } else {
                                var endDate = calEvent.end.format("YYYY-MM-DD HH:mm");
                            }
                            var cur_start_val = startDate.slice(11, 16);
                            $('#start_time option').filter(function () {
                                return ($(this).val() == cur_start_val);
                            }).prop('selected', true);
                            var cur_end_val = endDate.slice(11, 16);
                            $('#end_time option').filter(function () {
                                return ($(this).val() == cur_end_val);
                            }).prop('selected', true);
                            $('#opp_id option').filter(function () {
                                return ($(this).val() == calEvent.opp_id);
                            }).prop('selected', true);

                            /*      var t = cur_start_val.slice(0, 2);
                                  t = parseInt(t);
                                  t = t + 1;
                                  if (t < 10) {
                                      var forward_time = ("0" + t + cur_start_val.slice(2));
                                  } else {
                                      var forward_time = (t + cur_start_val.slice(2));
                                  }
                                  if (cur_end_val < cur_start_val) {
                                      $('#end_time option').filter(function () {
                                          return ($(this).val() == forward_time); //To select Blue
                                      }).prop('selected', true);
                                  } else {
                                      forward_time = cur_end_val;
                                  }
                                  var h = parseInt(forward_time.slice(0, 2)) - parseInt(cur_start_val.slice(0, 2));
                                  var m = parseInt(forward_time.slice(3)) - parseInt(cur_start_val.slice(3));
                                  if (m < 0) {
                                      h = h - 1;
                                      m = 30;
                                  }
                                  var mins = h * 60 + m;
                                  */
                            var mins = size * 30;
                            $('#hours_mins').text("(" + mins + "mins)");
                            $('#comments').text('');
                            $("select").select2({
                                theme: "bootstrap",
                                minimumResultsForSearch: 1
                            });
                            $('#btn_add_hours').prop('disabled', false);
                            $('#add_hours').modal('show');
                        },

                        dayClick: function (date, allDay, jsEvent, view) {
                            console.log('dwaadw')
                            var currentdate = new Date();

                            function formatDate(date) {
                                var d = new Date(date),
                                    month = '' + (d.getMonth() + 1),
                                    day = '' + d.getDate(),
                                    year = d.getFullYear();
                                if (month.length < 2) month = '0' + month;
                                if (day.length < 2) day = '0' + day;
                                return [year, month, day].join('-');
                            }

                            var newDate = date;
                            var j = new Date(newDate)
                            var t = j.getTime();

                            var f = new Date(currentdate)
                            var r = f.getTime();

                            var checkDate = t > r;
                            console.log(t, r)

                            if (checkDate) {
                                $('#unavailable').modal('show')
                            }
                            else {

                                $('#btn_remove_hours').hide()
                                $('#opp_id').prop('disabled', false);
                                $('.opp_select_div').show()
                                $('.private_opp_div_add_hours').hide()
                                $('#is_edit').val(0);
                                $('#btn_remove_hours').hide();
                                $('.org_not_exist').show();
                                $('.private_opp_div').hide();
                                $('#is_no_org').val(0);
                                $('.opp_select_div').show();
                                $('.confirmed_track').hide();
                                $('.declined_track').hide();
                                $('#selected_date').val(date.format('YYYY-MM-DD'))
                                $('#start_time > option[value="' + date.format('HH:mm') + '"]').attr('selected', 'true').text(date.format('(hh:mm)a'));
                                $('#end_time > option[value="' + $('#start_time > option[value="' + date.format('HH:mm') + '"]').next().val() + '"]').attr('selected', 'true').text($('#start_time > option[value="' + date.format('HH:mm') + '"]').next().text());
                                $('#hours_mins').text('(30mins)')
                                $('select option[value="Select an opportunity"]').attr('selected', 'true').text('Select an opportunity');
                                $("select").select2({
                                    theme: "bootstrap",
                                    minimumResultsForSearch: 1
                                });
                                $('#adding_hours_comments').val("")
                                $('#add_hours').modal('show');
                            }
                        },
                        viewDisplay: function (view) {
                            var now = new Date();
                            var end = new Date();
                            end.setMonth(now.getMonth() + 11); //Adjust as needed

                            var cal_date_string = view.start.getMonth() + '/' + view.start.getFullYear();
                            var cur_date_string = now.getMonth() + '/' + now.getFullYear();
                            var end_date_string = end.getMonth() + '/' + end.getFullYear();

                            if (cal_date_string == cur_date_string) {
                                jQuery('.fc-button-prev').addClass("fc-state-disabled");
                            }
                            else {
                                jQuery('.fc-button-prev').removeClass("fc-state-disabled");
                            }

                            if (end_date_string == cal_date_string) {
                                jQuery('.fc-button-next').addClass("fc-state-disabled");
                            }
                            else {
                                jQuery('.fc-button-next').removeClass("fc-state-disabled");
                            }
                        }
                    });

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        $('#org_name').on('change', function () {
            $('#opp_date_div').hide()
            $('#invalid_org_name_alert').hide();
            var org_id = $(this).val();
            if (org_id != '') {
                var url = API_URL + 'volunteer/track/getOpportunities';
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var type = "post";
                var formData = {org_id: org_id}

                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    success: function (data) {

                        $('#opp_name').find('option').remove().end();
                        // if(data.oppors != 'not exist'){

                        $('#opp_name').append($("<option></option>"));
                        $.each(data.oppor, function (index, value) {
                            $('#opp_name')
                                .append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.title));
                        });
                        $("select").select2({
                            theme: "bootstrap",
                            minimumResultsForSearch: 1
                        });

                        $('.org_does_exist').hide();
                        $('.opp_div').show();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
        $('.fa-calendar').css('cursor', 'pointer');
        $('.fa-calendar').on('click', function () {
            $('#selected_date').datepicker('show');
        })
    </script>
<!--Social Sharing-->
<script>
    // add this rail gallery effect
jQuery(document).on('click', '#socialShare .socialBox', function() {
var self = $(this);
var element = $('#socialGallery a');
var c = 0;
if (!self.hasClass('open')) {
  self.addClass('open');
} else {
    self.removeClass('open');
}
});
</script>
@endsection