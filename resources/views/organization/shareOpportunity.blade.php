@extends('layout.master')

@section('social_meta_tags')
	<meta property="og:title" content="Volunteer Opportunity: {{$oppr_info->title}}">
	<meta property="og:url" content="{{ Request::url() }}">
	<meta property="og:image" content="{{ asset('front-end/img/myvoluntier-social.png') }}">
	<meta property="og:description" content="{{$oppr_info->description}}">
	<meta property="og:state" content="{{$oppr_info->state}}">
	<meta property="geo.region" content="{{$oppr_info->state}}">
	<meta property="og:city" content="{{$oppr_info->city}}">
	<meta property="geo.placename" content="{{$oppr_info->city}}">
	<meta property="og:date" content="{{$oppr_info->start_date}} to {{$oppr_info->end_date}}">
	<meta property="og:qualifications" content="{{$oppr_info->qualification}}">
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap-sortable.css')}}">
    {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">--}}
    <link rel="stylesheet" type="text/css" href="{{asset('/css/multi-select.css')}}">
    <style>
        .time-selector .select2 {
            display: none !important;
        }
        .wrapper_select .select2 {
            width: 100% !important;
        }
        .only-mobile{
            display: none;
        }
        #social-links a,
        #social-links button{
            box-shadow: 0 6px 16px 0 rgba(0, 0, 0, 0.2);
        }
        #social-links a:hover,
        #social-links button:hover{
            background-color: #fff;
            color: #3bb44a;
            border-color: #fff;
        }

        @media screen and (max-width: 600px){
            .only-mobile{
                display: inline-block;
            }
        }
    </style>
@endsection

@section('content')
    <div class="wrapper-profile-box">
        <div class="dashboard_org">
            <div class="org-info-box">

                <div class="container">

                    @if(Session::has('message'))
                    <div class="alert alert-success">
                      <strong>Success!</strong> {{Session::get('message')}}
                    </div>
                    @endif

                    @if(Session::has('error'))
                    <div class="alert alert-danger">
                      <strong>Danger!</strong> {{ Session::get('error') }}
                    </div>
                    @endif

                    <div class="row align-items-center">
                        <div class="col-12 col-md-5">

                            <div class="big-org-logo">
                                <span style="background-image:url('{{$oppr_info->logo_img == NULL ?  asset('front-end/img/org/001.png') : $oppr_info->logo_img}}')"></span>
                            </div>

                        </div>

                        <div class="col-12 col-md-7">

                            <div class="main-text">

                                <h2 class="h2">
                                    @if($oppr_info->private_passcode)
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                    @endif

                                    {{$oppr_info->title}}
                                </h2>

                                <p class="mb-0"><b>Time Info:</b>
                                    <i data-toggle="tooltip" data-placement="top"
                                       title="{{$oppr_info->is_expired ? 'Expired' : 'Active'}}"
                                       class="fa {{$oppr_info->is_expired ? 'fa-warning' : 'fa-check-circle'}}">
                                    </i>
                                </p>
                                <p>{{$oppr_info->start_date}} to {{$oppr_info->end_date}}</p>

                                <p class="mb-0"><b>Hosted By:</b></p>
                                <p>{{$oppr_info->org_name}}</p>
                                <p class="mb-0"><b>Status:</b></p>
                                <p>{{$oppr_info->is_expired ? 'Expired' :  'Active'}}</p>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="modal fade" id="add_hours1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
                <div class="modal-dialog-fix">
                    <div class="track-add-hour">
                        <div class="modal-content">
                            <div class="modal-header">

                                <div class="main-text">
                                    <h2 class="h2">Add Hours</h2>
                                </div>

                                <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                    <span>&times;</span>
                                </a>

                            </div>
                            <div class="modal-body">

                                <div class="form-group  opp_select_div">
                                    <label class="label-text">Opportunity:<b>{{$oppr_info->title}}</b></label>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-12 col-lg-8 time-selector">
                                            <label class="label-text">Select Timeblock:</label>
                                            <select id="time_block" multiple='multiple'>
                                                <?php
//                                                $start = "00:00";
//                                                $end = "23:00";
                                                $tStart = strtotime($oppr_info->start_at);
                                                $tEnd = strtotime($oppr_info->end_at);
                                                $tNow = $tStart;

                                                while($tNow <= strtotime('-60 minutes',$tEnd)){
                                                $time = date("gA",$tNow).'-'.date("gA",strtotime('+60 minutes',$tNow));
                                                $val = date("H:i",$tNow)
                                                ?>
                                                <option value="{{$val}}">{{$time}}</option>
                                                <?php    $tNow = strtotime('+60 minutes',$tNow);
                                                }?>
                                            </select>
                                        </div>
                                        
                                        <div class="col-12 col-lg-4">
                                            <div class="form-group">
                                                <label class="label-text">Date:</label>
                                                <div class="wrapper_input fa-icons">
                                                    <input name="date" id="selected_date" class="form-control" type="text" value="" placeholder="" required>
                                                    <span class="focus-border"></span>
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                </div>
                                                <label class="label-text" style="padding-top: 20px">Logged Mins:</label>
                                                <p id="hours_mins" class="mt-15">(0min)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="label-text">Comments:</label>
                                    <div class="wrapper_input">
                                        <textarea name="adding_hours_comments" id="adding_hours_comments" placeholder=""></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <div class="wrapper_row_link">
                                    <div class="row">
                                        <div class="col-12">

                                            <div class="wrapper-link two-link">
                                                <a style="display: none" href="#" id="btn_remove_hours"><span>Remove</span></a>
                                                <a href="#" class="white close_button"><span>Close</span></a>
                                                <a id="btn_add_hours" href="#"><span>Save</span></a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <input type="hidden" id="selected_date" value="{{date('Y-m-d')}}"> -->
                        <input type="hidden" id="opp_id" value="{{$oppr_info->id}}">
                        <input type="hidden" id="opp_name" value="{{$oppr_info->title}}">
                        <input type="hidden" id="is_edit" value="0">
                        <input type="hidden" id="track_id" value="0">
                        <input type="hidden" id="is_from_addhour" value="0">
                        <input type="hidden" id="is_no_org" value="0">
                        <input type="hidden" id="is_link_exist" value="0">
                        <input type="hidden" id="org_email" value="{{$oppr_info->contact_email}}">
                        <input type="hidden" id="start_date" value="{{$oppr_info->start_date}}">
                        <input type="hidden" id="end_date" value="{{$oppr_info->end_date}}">
                        <input type="hidden" id="weekdays" value="{{$oppr_info->weekdays}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="text-profile-info">

            <div class="container">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" href="#details" role="tab"
                           data-toggle="tab"><span>Details</span></a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="details">

                        <div class="row">
                            <div class="col-12 col-md-8">

                                <div class="main-text">

                                    <p class="bold">Opportunity Type(s):</p>
									@foreach(@$opportunity_category as $category)
										@if(!empty($oppr_info->selected_cat) && in_array($category->id, $oppr_info->selected_cat))
                                            <p>{{$category->name}}</p>
										@endif
                                    @endforeach
									
                                    <p class="bold">Qualifications:</p>
                                    <p><?php echo $oppr_info->qualification; ?></p>

                                    <p class="bold">Description:</p>
                                    <p><?php echo $oppr_info->description; ?></p>

                                </div>

                            </div>
                            <div class="col-12 col-md-4">

                                <div class="details_content">
                                    <div class="map-box" id="pos_map">
                                        <input type="hidden" id="lat_val" value="{{$oppr_info->lat}}">
                                        <input type="hidden" id="lng_val" value="{{$oppr_info->lng}}">
                                    </div>
                                    <div class="main-text">
                                        <p class="bold">Address:</p>
                                        <p>{{$oppr_info->city}}, {{$oppr_info->state}}
                                            , {{$oppr_info->zipcode}}</p>

                                        <p class="bold">Date:</p>
                                        <p>{{$oppr_info->start_date}} - {{$oppr_info->end_date}}</p>

                                        <p class="bold">Time:</p>
                                        <p>{{$oppr_info->start_at}} - {{$oppr_info->end_at}}</p>

                                        <p class="bold">Days of Week:</p>
                                        
                                        <?php $temp = str_replace(",",", ",$oppr_info->weekdays); ?>
                                        <!-- <p>{{$oppr_info->weekdays}}</p> -->
                                        <p>{{$temp}}</p>

                                        <p class="bold">Minimum Age:</p>
                                        <p>{{$oppr_info->min_age}}</p>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es"></script>
<script type="text/javascript">
    var latLng = new google.maps.LatLng(parseFloat($('#lat_val').val()), parseFloat($('#lng_val').val()));
    var myOptions = {
            zoom: 16,
            center: latLng,
            styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}],
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("pos_map"), myOptions);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon: '{{asset('front-end/img/pin.png')}}'
        });
</script>
@endsection