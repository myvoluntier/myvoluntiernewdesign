@extends('layout.masterForAuthUser')

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/main.css')}}">
    <link rel="stylesheet" href="https://foliotek.github.io/Croppie/croppie.css">

    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">--}}
    <link href="{{asset('css/jQuery-QueryBuilder/query-builder.default.min.css')}}" rel="stylesheet" />

    <style>
        label.cabinet{
            display: block;
            cursor: pointer;
        }

        label.cabinet input.file{
            position: relative;
            height: 100%;
            width: auto;
            opacity: 0;
            -moz-opacity: 0;
            filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            margin-top:-30px;
        }

        #upload-logo{
            width: 400px;
            height: 400px;
            padding-bottom:25px;
        }

        #upload-banner{
            width: 800px;
            height: 400px;
            padding-bottom:25px;
        }

        figure figcaption {
            position: absolute;
            bottom: 0;
            color: #fff;
            width: 100%;
            padding-left: 9px;
            padding-bottom: 5px;
            text-shadow: 0 0 10px #000;
        }

        #cropBannerImagePop .modal-dialog{
            max-width: 900px !important;
        }
    </style>

@endsection

@section('content')
    <?php header("Access-Control-Allow-Origin: *"); ?>
    <div class="row-content">
        <div class="wrapper-opportunities">
            <div class="wrapper-tablist">
                <div class="container">
                    {{-- <div class="row align-items-center">
                        <div class="col-12 col-md-12">
                            <div class="form-group text-right addgrp-banner">
                                <img class="img-responsive img-thumbnail upload-img-banner" src="{{$curr_user->back_img == NULL ? asset('front-end/img/bg/0002.png') :  $curr_user->back_img}}" id="item-img-output-banner">                                
                                <div class="wrapper-file-upload upload-banner-camera">
                                    <input type="file" id="inputBanner" accept="image/*" name="file_banner">
                                    <input type="hidden" name="image_banner" id="image_banner">
                                    <a href="#"><i class="fa fa-camera"></i></a>
                                </div>
                                <div class="add-grp-profileimg">
                                    <div class="pos-upload-logo">
                                        <img class="img-responsive img-thumbnail upload-img-banner" src="{{$curr_user->logo_img == NULL ? asset('front-end/img/org/001.png') : $curr_user->logo_img}}" id="item-img-output">
                                        <div class="addgrp-upload-logo">
                                            <div class="wrapper-file-upload">
                                                <input name="file_logo" type="file" accept="image/*" id="inputImage">
                                                <input type="hidden" name="image_logo" id="image_logo">
                                                <a href="#"><i class="fa fa-camera"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   --}}
                    
                    <div class="row friend-text">
                        <div class="col-md-6 col-lg-6 col-sm-4 col-12 text-roow">
                            <div class="main-text">
                                <p class="h2">
                                    {{$curr_user->user_role == 'organization' ? $curr_user->org_name : $curr_user->first_name ." ". $curr_user->last_name}}
                                </p>
                            </div>                  
                            <div class="text-overflow"></div>
                        </div>
                        {{-- <div class="col-md-6 col-lg-6 col-sm-8 col-12 text-col-row">
                            <div class="main-text">
                                <ul class="list-column clearfix">
                                    <li>
                                        <p class="h2">0</p>
                                        <p class="light">Friends </p>
                                    </li>
                                    <li>
                                        <p class="h2">0</p>
                                        <p class="light">Groups</p>
                                    </li>
                                    <li>
                                        <p class="h2">0</p>
                                        <p class="light">Affiliated Groups</p>
                                    </li>
                                </ul>
                            </div>
                        </div> --}}
                    </div>
                    
                    <article class="panel-group bs-accordion new-accordian" id="accordion" role="tablist" aria-multiselectable="true">
                        <section class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a id="detailsLbl" class="nav-link" role="button" data-toggle="collapse" data-parent="#accordion" href="#details" aria-expanded="false" aria-controls="details">
                                        Impact Reports
                                        <span class="fa fa-minus pull-right" aria-hidden="true"></span>
                                        <span class="fa fa-plus pull-right" aria-hidden="true"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="details" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="details" style="">
                                <div class="panel-body">
                                    {{-- Graph Start --}}                     
                                        {{-- First Row --}}
                                        
                                        <div class="row align-items-center border-top-0 border-left-0 border-right-0">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
                                                    <div class="csv"> <h3>Volunteer Hours By Month</h3><a href="{{url('/organization/export-members-hours')}}">
                                                    <img class="" src="{{$curr_user->back_img == NULL ? asset('front-end/img/csv-new.png') :  $curr_user->back_img}}">
                                                    </a></div>
                                                    <div id="voluntier-hours-by-month{{$groupId}}" class="wrapper-svg">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 border-left">
                                                    <div class="csv"> <h3>Volunteer Hours By Type</h3><a href="{{url('/organization/export-members-type')}}" >
                                                    <img class="" src="{{$curr_user->back_img == NULL ? asset('front-end/img/csv-new.png') :  $curr_user->back_img}}">
                                                    </a></div>
                                                    <div id="voluntier-hours-by-type{{$groupId}}" class="wrapper-svg"></div>
                                                </div>
                                        </div>                  
                                        {{-- Second Row --}}
                                        <div class="row align-items-center border-bottom-0">
                                            <div class="col-12 col-md-12">
                                                <div class="row">
                                                    {{-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div id="volnt-zipcode-chart{{$groupId}}" style="min-width: 110px; height: 250px; margin: 0 auto" class="wrapper-svg"></div>
                                                    </div> --}}
                                                    <div class="csv"> <h3>Volunteer by Age Group</h3><a href="{{url('/organization/export-members-age')}}">
                                                    <img class="" src="{{$curr_user->back_img == NULL ? asset('front-end/img/csv-new.png') :  $curr_user->back_img}}">
                                                    </a></div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div id="volnt-agegroup-chart{{$groupId}}" style="min-width: 110px; height: 250px; margin: 0 auto" class="wrapper-svg"></div>
                                                 
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    {{-- Graph End --}}                                    
                                    <div class="row text-profile-info vol-news-feedlist view-oppor-grp view-oppor-tab view-oppo-imp-dtl">
                                        <div class="col-12 col-md-12">
                                        <nav>
                                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active show"  id="summary-tab" href="#summary_ref" role="tab" aria-controls="summary" data-toggle="tab" aria-selected="true">Summary</a>
                                                <a class="nav-item nav-link" id="detailed-tab" href="#detailed" role="tab" aria-controls="detailed" data-toggle="tab" aria-selected="false">Detailed</a>
                                            </div>
                                        </nav>

                                            <div class="tab-content py-3 px-3" id="nav-tabContent">
                                                <div role="tabpanel" class="tab-pane fade active show" id="summary_ref">
                                                <div class="wrapper-friends">
                                                        <div class="wrapper-friends-list pb-0 pt-0">
                                                            <div class="wrapper-sort-table tesposive-table">
                                                                <div>
                                                                <h3>Top Volunteers</h3>
                                                                    <table class="example5 table sortable">
                                                                        @if(count($volunteers))
                                                                            <thead>
                                                                                <tr>
                                                                                    <th scope="col" class="nosort" data-sortcolumn="0" data-sortkey="0-0">
                                                                                        Name
                                                                                    </th>
                                                                                    <th scope="col" class="nosort" data-sortcolumn="1" data-sortkey="1-0">
                                                                                        Hours
                                                                                    </th>
                                                                                    <th scope="col" class="nosort" data-sortcolumn="2" data-sortkey="2-0">
                                                                                        Opportunities
                                                                                    </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach($volunteers as $member)                                                                                    
                                                                                    <tr>
                                                                                        <td>{{$member['name']}}</td>
                                                                                        <td><span class="fa fa-clock-o desktop-hide-icon"></span>{{$member['hours']}}</td>
                                                                                        <td><p class="desktop-hide-icon">Opportunities :</p>{{$member['total_oppr']}}</td>
                                                                                    </tr>
                                                                                @endforeach
                                            
                                                                            </tbody>
                                                                        @else
                                                                            <tr>
                                                                                There are no top vountieers yet
                                                                            </tr>
                                                                        @endif
                                            
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="detailed">
                                                    
                                                </div>
                                                    
                                            </div>
                                        </div>
                                    </div> 

                                </div>
                            </div>
                        </section>
                        <section class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a id="groupsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#groups" aria-expanded="false" aria-controls="groups">
                                        Groups
                                        <span class="fa fa-minus pull-right" aria-hidden="true"></span>
                                        <span class="fa fa-plus pull-right" aria-hidden="true"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="groups" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                               <div class="panel-body">
                                    <div class="row">
                                        @if($groups->count()>0)
                                         @foreach($groups as $groupSingle)
                                            <div class="col-sm-4 mb-2">
                                                @if($groupSingle->is_share_able || $groupSingle->is_public ==1)
                                                <a href="{{route('share.group' ,base64_encode($groupSingle->id))}}" target="_black">
                                                    <div class="card" style="background-color: #edf2f3">
                                                        <div class="card-body">
                                                            <div class="main-text">
                                                                <p class="name">
                                                                    {{str_limit($groupSingle->name ,30)}}
                                                                    @if($groupSingle->is_public ==1)
                                                                    <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                                    @else
                                                                    <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                                    @endif
                                                                </p>
                                                                @if(isset($groupSingle->org_name))
                                                                <p><span style="font-size:13px">{{$groupSingle->org_name}}</span></p>
                                                                @endif
                                                                @if(isset($groupSingle->categoryName))
                                                                <p>{{"Category : ".$groupSingle->categoryName}}</p>                  
                                                                @endif
                                                                <p>{{"Vounteer Registered : ".$groupCount[$groupSingle->id]}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                                @else
                                                <div class="card" style="background-color: #edf2f3">
                                                    <div class="card-body">
                                                        <div class="main-text">
                                                            <p class="name">
                                                                {{str_limit($groupSingle->name ,30)}}
                                                                @if($groupSingle->is_public ==1)
                                                                <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                                @else
                                                                <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                                @endif
                                                            </p>
                                                            @if(isset($groupSingle->org_name))
                                                            <p><span style="font-size:13px">{{$groupSingle->org_name}}</span></p>
                                                            @endif
                                                            @if(isset($groupSingle->categoryName))
                                                            <p>{{"Category : ".$groupSingle->categoryName}}</p>                  
                                                            @endif
                                                            <p>{{"Vounteer Registered : ".$groupCount[$groupSingle->id]}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif 
                                            </div>
                                            @endforeach  
                                            @else
                                            <div class="col-sm-12 mb-12">
                                                You do not have active groups yet
                                            </div>
                                            @endif 
                                    </div>                                                     
                                </div>
                            </div>
                        </section>
                        <section class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a id="serviceProjectTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#serviceProject" aria-expanded="false" aria-controls="serviceProject">
                                        Service Projects
                                        <span class="fa fa-minus pull-right" aria-hidden="true"></span>
                                        <span class="fa fa-plus pull-right" aria-hidden="true"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="serviceProject" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    
                                </div>
                            </div>
                        </section>
                        <section class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a id="serviceProjectTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#attribute" aria-expanded="false" aria-controls="serviceProject">
                                        Custom Attributes
                                        <span class="fa fa-minus pull-right" aria-hidden="true"></span>
                                        <span class="fa fa-plus pull-right" aria-hidden="true"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="attribute" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    
                                </div>
                            </div>
                        </section>
                    </article>
                </div>
            </div>    
        </div>    
    </div>
@endsection  

@section('script')
<script src="{{asset('front-end/js/highcharts.js')}}"></script>
@include('_groups.groups-charts-dash-js')

<script src="{{asset('front-end/js/bootstrap-sortable.js')}}"></script>
<script src="{{asset('js/plugins/footable/footable.all.min.js')}}"></script>
<script src="//cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/1.10.19/pagination/ellipses.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>

<script>
$(document).ready(function () {
        $('.example5').dataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "sorting":false,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "pageLength": 10,
            "pagingType": "full_numbers",
            "language": {
                "paginate": {
                    "first": '<<',
                    "next": '>',
                    "previous": '<',
                    "last": '>>'
                }
            }
        });
});     
$(function() {
$(".preload").fadeOut(2000, function() {
    $(".contents").fadeIn(1000);
});
});
</script>

@endsection
