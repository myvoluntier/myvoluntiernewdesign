@extends('layout.masterForAuthUser')

@section('css')
    <link href="<?=asset('css/plugins/fullcalendar/fullcalendar.css')?>" rel="stylesheet">
    <link href="<?=asset('css/plugins/fullcalendar/fullcalendar.print.css')?>" rel='stylesheet' media='print'>
    <link rel="stylesheet" type="text/css" href="{{asset('/css/multi-select.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">
    <style>
        .opp-public{
            text-decoration: none;
            cursor: pointer;
            height: 40px;
            font-size: 14px;
            line-height: 24px;
            text-transform: uppercase;
            font-weight: 700;
            font-family: 'Open Sans',sans-serif;
            color: #fff;
            margin: 0;
            padding: 8px 30px;
            background: #3bb44a;
            -webkit-border-radius: 20px;
            -moz-border-radius: 20px;
            border-radius: 20px;
            max-width: 100%;
            -webkit-box-shadow: 0 6px 16px 0 rgba(29,125,41,0.3);
            -moz-box-shadow: 0 6px 16px 0 rgba(29,125,41,0.3);
            box-shadow: 0 6px 16px 0 rgba(29,125,41,0.3);
        }

        .opp-public:hover{
            background: #fff;
            color: #3bb44a;
        }

        a {
            text-decoration: none !important;
        }
        .pagination .flex-wrap .justify-content-center{
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .footable-page-arrow, .footable-page{
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .footable-page-arrow a, .footable-page a{
            position: relative;
            display: block;
            padding: 10px;
            margin: 10px 5px;
            font-size: 16px;
            line-height: 20px;
            font-family: 'Open Sans',sans-serif;
            color: #5f5f5f;
            background: none;
            border: 0 none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-decoration: none;
            cursor: auto;
            outline: none;
            z-index: auto;
        }

        .footable-page-arrow a:hover, .footable-page a:hover{
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }

        .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled{
            display: none;
        }

        .footable-page.active a{
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }
        .news-followed-box .main-text{margin: 15px 0px 15px 0px;}
        .actionBtn{color:#fff !important;}
        #ics_download {display : none;}
    </style>
    <link rel="stylesheet" href="{{asset('front-end/css/star-rating.css')}}">
@endsection

@section('content')
<div class="text-profile-info organization_track_blk">
    <!-- page heading start -->
    <div class="page_head-box green_bg_blk">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <h2 class="h2 text-center">Organization Track</h2>                            
                        </div>
                        
                    </div>
                </div>
            </div>
       
        <!-- page heading end -->
    <div class="container">
        <!-- <div class="news-followed-box mt25">
            <div class="main-text">
                <h3 class="h3">Organization Track</h3>
            </div>
        </div> -->
        <div class="new-track-calender-tab">
            <ul class="profile_tab nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" id="pendingHoursTab" href="#pendingHours" role="tab" data-toggle="tab"><span>Pending Action</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#trackedHours" id="trackedHoursTab" role="tab" data-toggle="tab"><span>Tracked Hours</span></a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active show" id="pendingHours">
                    @include('components.orgPendingHours')
                </div>

                <div role="tabpanel" class="tab-pane fade" id="trackedHours">
                    @include('components.orgTrackedHours')
                </div>
           
        </div>
    </div>
</div>
<input type="hidden" id="trackedURL" value="{{url('/organization/track#trackedHours') }}" />
@include('components.auth.modal_pop_up_action_track_org')

@endsection

@section('script')

    <script src="<?=asset('js/plugins/dataTables/jquery.dataTables.js')?>"></script>
    <script src="{{asset('front-end/js/moment.min.js')}}"></script>
    <script src="<?=asset('js/plugins/fullcalendar/moment.min.js')?>"></script>
    <script src="<?=asset('js/plugins/fullcalendar/fullcalendar.min.js')?>"></script>
    
    <script src="<?=asset('js/plugins/select2/select2.full.min.js')?>"></script>
    <script src="<?=asset('js/plugins/dataTables/jquery.dataTables.js')?>"></script>
    <script src="{{asset('front-end/js/star-rating.js')}}"></script>
    
    <script src="<?=asset('js/jquery-ui.custom.min.js')?>"></script>
    <script src="{{asset('js/jquery.multi-select.js')}}"></script>
    <script src="{{asset('front-end/js/trackhours-valid.js')}}"></script>
    <script src="{{asset('front-end/js/org-trackhours.js')}}"></script>
    <script src="{{asset('front-end/js/jquery-ui.js')}}"></script>
    
    <script>
$("#pendingHoursTab").click(function(){
    location.replace(location.origin+location.pathname+"#pendingHours");   
   $("#trackHour_paginate").hide();
   $("#pendingTrack_paginate").show();  
});
$("#trackedHoursTab").click(function(){
   location.replace(location.origin+location.pathname+"#tracked");
    $("#pendingTrack_paginate").hide();
    $("#trackHour_paginate").show();
});

        function searchName(filterTxt, tableId) {
            // Declare variables
            var input, filter, table, tr, td, i, td2, td3, td4, td5;
            var flag = 0;
            input = document.getElementById(filterTxt);
            filter = input.value.toUpperCase();
            table = document.getElementById(tableId);
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                td2 = tr[i].getElementsByTagName("td")[1];
                td3 = tr[i].getElementsByTagName("td")[2];
                td4 = tr[i].getElementsByTagName("td")[3];
                td5 = tr[i].getElementsByTagName("td")[4];
                if (td) {
                    
                    flag = 1;
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    }else if (td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    }else if (td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    }else if (td4.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    }else if (td5.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                        console.log(333);
                    }
                }
            }
            
        }
$(document).ready(function () {

if(window.location.href.indexOf("tracked")>1){
    $( "#trackedHoursTab" ).addClass('active');
    $( "#pendingHoursTab" ).removeClass('active');
    $("#trackedHours").addClass('active show');
    $("#pendingHours").removeClass('active show');
    $("#trackHour_paginate").show();
     }
if($(".nav-tabs li a.active").attr('id')=="pendingHoursTab"){
    $("#trackHour_paginate").hide();
   $("#pendingTrack_paginate").show(); 
}

// $('#tbl_org_tracking').dataTable({
//                 "paging": true,
//                 "lengthChange": false,
//                 "searching": false,
//                 "ordering": true,
//                 "info": false,
//                 "autoWidth": true,
//                 "pageLength": 20,
//                 "pagingType": "full_numbers",
//                 "language": {
//                     "paginate": {
//                         "first": "<<",
//                         "next": '>',
//                         "previous": '<',
//                         "last": '>>'
//                     }
//                 }
//             });

    //  $('.example5').dataTable({
    //             "paging": true,
    //             "lengthChange": false,
    //             "searching": false,
    //             "ordering": true,
    //             "info": false,
    //             "autoWidth": true,
    //             "pageLength": 20,
    //             "pagingType": "full_numbers",
    //             "language": {
    //                 "paginate": {
    //                     "first": "<<",
    //                     "next": '>',
    //                     "previous": '<',
    //                     "last": '>>'
    //                 }
    //             }
    //         });

            /*$('.confirm-table').footable({
                limitNavigation: 5,
                firstText: '1'
            });
            $('ul.pagination').each(function(){
                if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
                else $(this).find(' .footable-page-arrow:last').show();
            });

            $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
                var pagination = $(this).parents('.tab-pane.active .pagination');
                if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
                else pagination.find('.footable-page-arrow:first').show();
                if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
                else pagination.find('.footable-page-arrow:last').show();
            });*/

            /*if($('.footable-page-arrow:last').text() < 5) $('.footable-page-arrow:last').hide();
            else $('.footable-page-arrow:last').show();

                $(document).on('click', 'a[data-page]', function () { //, .footable-page-arrow
                    if($('.footable-page:first a').data('page') == 0) $('.footable-page-arrow:first').hide();
                    else $('.footable-page-arrow:first').show();
                    if(parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
                    else $('.footable-page-arrow:last').show();
                });*/


            /*$('.confirm-table').footable({
                     limitNavigation: 5,
                     firstText: '1'
                 });
             $('ul.pagination').each(function(){
                 if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
                 else $(this).find(' .footable-page-arrow:last').show();
             });

             $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
                 var pagination = $(this).parents('.tab-pane.active .pagination');
                 if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
                 else pagination.find('.footable-page-arrow:first').show();
                 if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
                 else pagination.find('.footable-page-arrow:last').show();
             });*/


            $('.search-input').on('keyup', function (e) {
                if (e.keyCode == 13) {
                    $('#btn_search_page').trigger('click');
                }
            });


            $('.filter').on('click', function () {
                console.log('change')
                $('#btn_search_page').trigger('click');

            });

            $('.volunteer-rate').rating();

            $('.btn_action').on('click', function (e) {
                e.preventDefault();

                setTimeout(function () {
                    $('body').removeClass('modal-open')
                }, 200);

                $('#wrapper_div').hide();
                $('#myModalTrackedHours').css({overflow:'auto'});
                $('#myModalTrackedHours').modal('show');

                $('#checkbox_div').attr("checked", false);
                $('.review-comment').val('');
                $('.review').hide();
                $('.private-opp').hide();


                var track = $(this).parent().find('.track_id').val();
                var v_name = $(this).parent().find('.volunteer_name').val();
                var v_logo = $(this).parent().find('.volunteer_logo').val();
                var o_id = $(this).parent().find('.opportunity_id').val();
                var o_name = $(this).parent().find('.opportunity_name').val();
                var o_logo = $(this).parent().find('.opportunity_logo').val();
                var w_date = $(this).parent().find('.worked_date').val();
                var s_time = $(this).parent().find('.started_time').val();
                var e_time = $(this).parent().find('.ended_time').val();
                var w_mins = $(this).parent().find('.worked_mins').val();
                var submit_t = $(this).parent().find('.submitted_time').val();
                var v_comment = $(this).parent().find('.volunteer_comment').val();
                var o_type = $(this).parent().find('.opportunity_type').val();
                $('#track_id').val(track);

                if (v_logo != '') {
                    var string = 'url(' + SITE_URL + "uploads/" + v_logo + ')';
                    string = string.replace(/ /g, '%20')
                    $('.v-logo').css('background-image', string);
                } else {
                    var string = 'url(' + SITE_URL + "img/logo/member-default-logo.png" + ')'
                    $('.v-logo').css('background-image', string);
                }

                if (o_logo != '') {
                    var stringOpp = 'url(' +'"' + SITE_URL + "uploads/" + o_logo + '"' + ');';
                    stringOpp = stringOpp.replace(/ /g, '%20');
                    console.log(stringOpp)
                    // $('#o-logo-modal').css('background-image', stringOpp);
                    // console.log($('#o-logo-modal').css('background-image', stringOpp))
                    // $('#o-logo-modal').hide()
                    $('#o-logo-modal').attr("style", 'background-image: ' + stringOpp)

                } else {
                    var stringOpp = 'url(' + SITE_URL + "front-end/img/org/001.png" + ')';
                    $('#o-logo-modal').css('background-image', stringOpp);
                }
                $('.opp-public').attr('href', SITE_URL + "organization/edit_opportunity/" + o_id);
                $('.v-name').text(v_name);
                $('.o-name').text(o_name);
                $('.w-mins').text(w_mins + " mins");
                $('.track-comment').text(v_comment);
                $('.submitted-time').text(submit_t);
                $('.worked-time').text(w_date + ' ' + s_time + ' to ' + e_time);
                if (o_type == 0) {
                    $('.private-opp').show();
                }
            });

            $('#btn_approve').on('click', function () {

                var track_id = $('#track_id').val();
                var is_review = 0;
                if ($('#checkbox_div').is(":checked")) {
                    is_review = 1;
                }
                var review_score = $('#input-rating').val();
                var review_comment = $('.review-comment').val();
                var url = API_URL + 'organization/track/approve';
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var type = "post";
                var formData = {
                    track_id: track_id,
                    is_review: is_review,
                    review_score: review_score,
                    review_comment: review_comment,
                };
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    success: function (data) {
                        $('#' + 'track' + track_id).remove();
                        $('#track_process').modal('toggle');
                        $('.close').click()
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });

            $('#btn_decline').on('click', function () {
                var track_id = $('#track_id').val();
                var url = API_URL + 'organization/track/decline';
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                console.log();
                var type = "post";
                var formData = {
                    track_id: track_id
                };
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    success: function (data) {
                        $('#' + 'track' + track_id).remove();
                        $('#track_process').modal('toggle');
                        $('.close').click()
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });

        });

        $("#checkbox_div").click(function () {
            if ($(this).is(":checked")) {
                $('#wrapper_div').css("display","block");
            } else {
                $('#wrapper_div').css("display","none");
            }
        });

        $('.kv-fa').rating({
            clearButton: '<i class="fa fa-minus-circle"></i>',
            filledStar: '<i class="fa fa-star"></i>',
            emptyStar: '<i class="fa fa-star-o"></i>'
        });

    </script>
@endsection
