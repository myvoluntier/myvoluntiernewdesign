@extends('layout.masterForAuthUser')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

        .massMail {
            padding: 13px !important;
            line-height: 5px !important;
            margin-left: 7px !important;
        }

        .qrCodeScan {
            padding: 13px !important;
            line-height: 5px !important;
        }

        .row-centered {
            text-align:center;
        }

        .modal { overflow-y: auto !important; }

    </style>
@endsection

@section('content')
    <div class="wrapper-opportunities">
        <div class="add-new-box">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-8 col-lg-8">
                        <p>Opportunities</p>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 text-right">
                        <a href="{{route('organization-opportunity-post')}}" class="add-new"><span>Add new opportunity</span></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="wrapper-tablist">
            <div class="container">
                <div class="inner-tablist-wrap">
                <ul class="nav nav-tabs" role="tablist" >
                    <li class="nav-item">
                        <a class="nav-link active show" id="active_data" href="#active" role="tab" data-toggle="tab"><span>Active</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#expired" id="expire_data" role="tab" data-toggle="tab"><span>Expired</span></a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="active">
                        <p class="title">Active Opportunities</p>
                        <ul>
                            @foreach($active_oppors as $op)
								<?php 
									if($op->logo_img == NULL){
										$organizLogo = $op->getOrgLogoAttribute();
										if($organizLogo == NULL){
											$logo = asset('front-end/img/org/001.png');
										}else{
											$logo = $organizLogo;
										}
									}else{
										$logo = $op->logo_img;
									}
									?>
                                <li>
                                    <div class="row">
                                        <div class="col-12 col-md-12 oppotunity-listing">
                                            <div class="oppo-avtar-img"><div class="avatar" style="background-image:url('{{ $logo}}')"></div></div>
                                            <div class="row align-items-end opp-flex-ul">

                                                <div class="oppo-list-desc">
                                                    <div class="main-text">
                                                        <a href="{{url('/organization/view_opportunity')}}/{{$op->id}}"> <p class="name"> <?php echo wordwrap($op->title,45,'<br>', true); ?></p></a>
                                                        <?php echo $op->description; ?>
                                                        <p class="light" style="visibility:hidden">Opportunity</p>   
                                                    </div>
                                                </div>

                                                <div class="oppo-list-btns">
                                                    <div class="buttons">
                                                    <a title="Survey" href="{{url('/organization/survey_opportunity')}}/{{$op->id}}" class="survey"><span><img src="{{asset('img/oppo-survey.png')}}" alt=""></span></a>
                                                        <a title="Clone" href="{{url('/organization/clone_opportunity')}}/{{$op->id}}" class="edit"><span><img src="{{asset('img/oppo-clone.png')}}" alt=""></span></a>
                                                        <a title="Edit" href="{{url('/organization/edit_opportunity')}}/{{$op->id}}" class="edit"><span><img src="{{asset('img/oppo-edit.png')}}" alt=""></span></a>

                                                        <a data-opportuniti-id="{{$op->id}}" id="{{$op->id}}" title="Delete" href="#" class="remove"><span><img src="{{asset('img/oppo-del.png')}}" alt=""></span></a>
                                                        <a title="Email Blast To Members" data-opportuniti-id-mail="{{$op->id}}" id="{{'massMail_'.$op->id}}" href="#" class="edit massMail"><span><img src="{{asset('img/oppo-email.png')}}" alt=""></span></a>
                                                        <a title="Approve hours by scanning QR CODE" data-opportunity-id="{{$op->id}}"
                                                           href="#" class="edit qrCodeScan" data-toggle="modal" data-target="#qrCodeScanModal">
                                                            <span><img src="{{asset('img/oppo-qrcode.png')}}" alt=""></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>    
                    <div role="tabpanel" class="tab-pane fade" id="expired">
                        <p class="title">Expired Opportunities</p>
                        <ul>
                            @foreach($expired_oppors as $ex_op)
                                <li>
                                    <div class="row">
                                        <div class="col-12 col-md-12 oppotunity-listing">
                                        <div class="oppo-avtar-img"><div class="avatar" style="background-image:url('{{ $ex_op->logo_img === NULL ? asset('front-end/img/org/001.png') : $ex_op->logo_img }}')"></div>
                                        </div>   
                                        <div class="row align-items-end opp-flex-ul">
                                             <div class="oppo-list-desc">
                                                 <div class="main-text">
                                                        <a href="{{url('/organization/view_opportunity')}}/{{$ex_op->id}}"><p class="name"><?php echo wordwrap($ex_op->title,45,'<br>', true); ?></p></a>
                                                        <?php echo $ex_op->description; ?>
                                                        <p class="light">Opportunity</p>
                                                    </div>
                                                </div>
                                                <div class="oppo-list-btns">
                                                    <div class="buttons">
                                                        <a title="Clone" href="{{url('/organization/clone_opportunity')}}/{{$ex_op->id}}" class="edit"><span><img src="{{asset('img/oppo-clone.png')}}" alt=""></span></a>
                                                        <a title="Edit" href="{{url('/organization/edit_opportunity')}}/{{$ex_op->id}}" class="edit"><span><img src="{{asset('img/oppo-edit.png')}}" alt=""></span></a>
                                                        <a title="Delete" data-opportuniti-id="{{$ex_op->id}}" id="{{$ex_op->id}}" href="#" class="remove">
                                                            <span><img src="{{asset('img/oppo-del.png')}}" alt=""></span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>  
                    </div>
                </div>
                <div class="pull-right" style="display:none; padding-top:20px" id="active_paginate">
                {{$active_oppors->render()}}
                </div> 
                  <div class="pull-right" style="display:none; padding-top:20px" id="expired_paginate"> 
                 {{$expired_oppors->fragment('expired')}}
                 </div> 
            </div>
         </div>
        </div>

    </div>

    <div class="modal fade modal-popups" id="delete_modal">
        <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header" style="text-align:center;">
                <h4 class="modal-name">
                    Are You Sure You Want To Delete ?
                </h4>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" data-opportuniti-id="" class="button-fill button-grey removePop" id="delete_link">Delete</a>
                <button type="button" class="button-fill" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade modal-popups" id="qrCodeScanModal" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="">QR Scanner</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <p>Scan the Volunteer's QR code to confirm volunteer hours associated with this opportunity.</p>

                    <input type="hidden" name="oppor_id" id="oppor_id">
                    <div class="row row-centered">
                        <div style="margin: auto; width: 50%; padding: 5px;">
                            <div id="qrCodeOrg"></div>
                        </div>
                    </div>

                    <div class="d-none" id="detailAfterScan">
                        <div class="row row-centered">
                            <div style="margin: auto; width: 50%; padding: 5px; color: green; font-weight:bold ">
                                <i class="fa fa-check" aria-hidden="true"></i> APPROVED
                            </div>
                        </div>
                        <br>
                        <div class="row row-centered">

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <img id="qr_image" src="https://via.placeholder.com/100" alt="Image Logo" class="img-thumbnail">
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3 my-auto">
                            <h6 id="qr_name">No Name</h6>
                            <span class="text-success" id="qr_hours">3 Hours</span>
                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-6"
                             style="font-size: 12px;background: #a9a9a973; border-radius: 6px">
                            <br/>
                            <b>Organization:</b> <span id="qr_org_name"></span> <br>
                            <b>Opportunity:</b> <span id="qr_opp_name"></span> <br>
                            <b>Data:</b> <span id="qr_date"></span><br>
                            <b>Time Tracked:</b> <span id="qr_time_range"></span> <br>
                            <br/>
                        </div>
                        <br>
                    </div>
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="button-fill button-grey float-right ml-2" data-dismiss="modal">CLOSE</button>
                    <button type="button" class="button-fill float-right qrCodeScanning" >SCAN CODE</button>
                    {{--@if(env('APP_ENV') == 'local')--}}
                        {{--<button type="button" class="btn btn-success float-right Dev2Test" >Dev2 Test</button>--}}
                    {{--@endif--}}
                </div>
            </div>
        </div>
    </div>

    @include('components.opportunityEmailBlasts')

    <div class="modal fade" id="scanningQrCodeVidModal" tabindex="-1" role="dialog" aria-labelledby="scanningQrCodeVidModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Scanning Code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <video id="previewScan"></video>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

    <script>
$("#active_data").click(function(){
    location.replace(location.origin+location.pathname+"#active");   
   $("#expired_paginate").hide();
   $("#active_paginate").show();  
});
$("#expire_data").click(function(){
    location.replace(location.origin+location.pathname+"#expired");
    $("#active_paginate").hide();
    $("#expired_paginate").show();
});
$( document ).ready(function() {
if(window.location.href.indexOf("expired")>1){
    $( "#expire_data" ).addClass('active');
    $( "#active_data" ).removeClass('active');
    $("#expired").addClass('active show');
    $("#active").removeClass('active show');
    $("#expired_paginate").show();
     $("#active_paginate").hide();
     }
});
if($(".nav-tabs li a.active").attr('id')=="active_data"){
   $("#expired_paginate").hide();
   $("#active_paginate").show();
}
        $('.qrCodeScan').click(function(){
            let opprID =  $(this).data('opportunityId');
            $('#oppor_id').val(opprID);

            $("#qrCodeOrg").html("");
            $('#qrCodeOrg').qrcode({width: 120,height: 120,text: JSON.stringify(opprID)});

        });

        {{--$('.Dev2Test').click(function(){--}}
            {{--let oppr_id =  $('#oppor_id').val();--}}

            {{--let url = "{{route('approve-org-track-detail')}}";--}}

            {{--$.ajaxSetup({--}}
                {{--headers: {--}}
                    {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                {{--}--}}
            {{--});--}}

            {{--let type = "get";--}}
            {{--let formData = {oppr_id: oppr_id};--}}

            {{--$.ajax({--}}
                {{--url: url,--}}
                {{--type: type,--}}
                {{--data: formData,--}}
                {{--success: function (response) {--}}

                    {{--if(response){--}}
                        {{--console.log(response);--}}
                        {{--$('#qr_image').attr('src',response['image']);--}}
                        {{--$('#qr_name').text(response['name']);--}}
                        {{--$('#qr_hours').text(response['hours']);--}}
                        {{--$('#qr_org_name').text(response['org_name']);--}}
                        {{--$('#qr_opp_name').text(response['opportunity_name']);--}}
                        {{--$('#qr_date').text(response['date']);--}}
                        {{--$('#qr_time_range').text(response['time_range']);--}}
                    {{--}--}}
                    {{--else{--}}
                        {{--$('#detailAfterScan').html(--}}
                            {{--'<div class="row row-centered">\n' +--}}
                            {{--'<div style="margin: auto; width: 50%; padding: 5px; color: red; font-weight:bold ">\n' +--}}
                            {{--'<i class="fa fa-cross" aria-hidden="true"></i> No pending tracks found.\n' +--}}
                            {{--'</div>\n' +--}}
                            {{--'</div>');--}}
                    {{--}--}}

                    {{--$('#detailAfterScan').removeClass('d-none');--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}


        $('.qrCodeScanning').click(function(){
            let opprID =  $('#oppor_id').val();

            let scanner = new Instascan.Scanner({ video: document.getElementById('previewScan') });

            scanner.addListener('scan', function (content) {

                $('#detailAfterScan').addClass('d-none');

                let oppr_id =  $('#oppor_id').val();

                let url = "{{route('approve-org-track-detail')}}";

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let type = "get";
                let formData = {oppr_id: oppr_id};

                $.ajax({
                    url: url,
                    type: type,
                    data: formData,
                    success: function (response) {

                        if(response){
                            console.log(response);
                            $('#qr_image').attr('src',response['image']);
                            $('#qr_name').text(response['name']);
                            $('#qr_hours').text(response['hours']);
                            $('#qr_org_name').text(response['org_name']);
                            $('#qr_opp_name').text(response['opportunity_name']);
                            $('#qr_date').text(response['date']);
                            $('#qr_time_range').text(response['time_range']);
                        }
                        else{
                            $('#detailAfterScan').html(
                                '<div class="row row-centered">\n' +
                                '<div style="margin: auto; width: 50%; padding: 5px; color: red; font-weight:bold ">\n' +
                                '<i class="fa fa-cross" aria-hidden="true"></i> No pending tracks found.\n' +
                                '</div>\n' +
                                '</div>');
                        }

                        $('#detailAfterScan').removeClass('d-none');
                    }
                });

                $('#scanningQrCodeVidModal').modal('hide');

                //Do all other stuff here to approve first one oppr hour and then show next same as above...!
            });

            Instascan.Camera.getCameras().then(function (cameras) {
                $('#previewScan').attr('style','transform: scaleX(1); width: 100%');

                if (cameras.length > 0) {
                    var selectedCam = cameras[0];
                    $.each(cameras, (i, c) => {
                        if (c.name.indexOf('back') != -1) {
                            selectedCam = c;
                            return false;
                        }
                    });

                    scanner.start(selectedCam);
                    $('#scanningQrCodeVidModal').modal('show');
                } else {
                    toastr.error('No cameras found.', "Error");
                }

            }).catch(function (e) {
                console.log(e);
                toastr.error(e, "Error");
            });
        });

        $('.remove').click(function(){
             $('#delete_modal').modal('show', {backdrop: 'static'});
             $('#delete_link').attr('data-opportuniti-id', $(this).data('opportunitiId'));
        });

        $('.removePop').click(function (e) {
            e.preventDefault();

            $('#delete_link').html('Loading....');
            var $div = $('#'+$(this).attr('data-opportuniti-id'));
            var oppr_id = $(this).attr('data-opportuniti-id');
          
          
            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                    $div.parent().parent().parent().parent().parent().parent().hide()
                    $('#delete_link').html('Delete');
                    jQuery('#delete_modal').modal('hide');
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#delete_link').html('Delete');
                }
            });
            
        });

        $('.massMail').on('click', function (e) {
            $("#cke_message_members").css("border", "");
            CKEDITOR.instances['message_members'].setData('');
            
            $("#oppEmailBlast_loading").hide();
            $('.success-first').hide();
            $('.hide-email-comment').show();
            $('#oppEmailBlastBtn').show();
            $('#oppEmailBlast_hide').hide();
            
            $('#opportunitiIdMail').val($(this).data('opportunitiIdMail'));
            $('.top-50').css('margin-top', '0px');
            $('#oppEmailBlast').modal('show');
        });

        $('#oppEmailBlastBtn').on('click',function(){
            var flags = 0;
            if (CKEDITOR.instances['message_members'].getData() == '') {
                $("#cke_message_members").css("border", "1px solid #ff0000");
                flags++;
            }
            if(flags == 0){
                $("#oppEmailBlastBtn").hide();
                $("#oppEmailBlast_loading").show();
                var formData = {
                        opprId: $('#opportunitiIdMail').val(),
                        message: CKEDITOR.instances['message_members'].getData(),
                };

                var url = API_URL + 'opportunity_email_blast'; 
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    success: function (data) {
                        $("#oppEmailBlast_loading").hide();
                        $('.success-first').show();
                        $('.hide-email-comment').hide();
                        CKEDITOR.instances['message_members'].setData('');
                        $('#oppEmailBlastBtn').hide();
                        $('#oppEmailBlast_hide').show();
                        $('.top-50').css('margin-top', '50px');
                    },
                    error: function (data) {
                        $("#oppEmailBlastBtn").show();
                        $("#oppEmailBlast_loading").hide();
                        console.log('Error:', data);
                        toastr.error("Something went wrong.", "Error");
                    }
                });
            }
        });

        $('#oppEmailBlast_hide').on('click', function () {
            $('#oppEmailBlast').modal('hide')
        });
</script>
@endsection
