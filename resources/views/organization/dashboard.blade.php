@extends('layout.masterForAuthUser')

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/main.css')}}">
    <link rel="stylesheet" href="https://foliotek.github.io/Croppie/croppie.css">

    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">--}}
    <link href="{{asset('css/jQuery-QueryBuilder/query-builder.default.min.css')}}" rel="stylesheet" />

    <style>
        label.cabinet{
            display: block;
            cursor: pointer;
        }

        label.cabinet input.file{
            position: relative;
            height: 100%;
            width: auto;
            opacity: 0;
            -moz-opacity: 0;
            filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            margin-top:-30px;
        }

        #upload-logo{
            width: 400px;
            height: 400px;
            padding-bottom:25px;
        }

        #upload-banner{
            width: 800px;
            height: 400px;
            padding-bottom:25px;
        }

        figure figcaption {
            position: absolute;
            bottom: 0;
            color: #fff;
            width: 100%;
            padding-left: 9px;
            padding-bottom: 5px;
            text-shadow: 0 0 10px #000;
        }

        #cropBannerImagePop .modal-dialog{
            max-width: 900px !important;
        }
    </style>

@endsection

@section('content')
    <?php header("Access-Control-Allow-Origin: *"); ?>
    <div class="row-content">
        <div class="wrapper-opportunities">
            <div class="wrapper-tablist">
                <div class="container">

                    <div class="row align-items-center">
                        <div class="col-12 col-md-12">
                            <div class="form-group text-right addgrp-banner">
                                <img class="img-responsive img-thumbnail upload-img-banner" src="https://localhost/myvoluntier/public/front-end/img/upload/default-banner-img.jpg" id="item-img-output-banner">
                                <div class="wrapper-file-upload upload-banner-camera">
                                    <input type="file" id="inputBanner" accept="image/*" name="file_banner">
                                    <input type="hidden" name="image_banner" id="image_banner">
                                    <a href="#"><i class="fa fa-camera"></i></a>
                                </div>
                                <div class="add-grp-profileimg">
                                    <div class="pos-upload-logo">
                                        <img class="img-responsive img-thumbnail upload-img-banner" src="https://localhost/myvoluntier/public/front-end/img/upload/default-logo-img.jpg" id="item-img-output">
                                        <div class="addgrp-upload-logo">
                                            <div class="wrapper-file-upload">
                                                <input name="file_logo" type="file" accept="image/*" id="inputImage">
                                                <input type="hidden" name="image_logo" id="image_logo">
                                                <a href="#"><i class="fa fa-camera"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  
                    
                    <div class="row friend-text">
                        <div class="col-md-6 col-lg-6 col-sm-4 col-12 text-roow">
                            <div class="main-text">
                                <p class="h2">100 Black Men</p>
                            </div>                  
                            <div class="text-overflow"></div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-8 col-12 text-col-row">
                            <div class="main-text">
                                <ul class="list-column clearfix">
                                    <li>
                                        <p class="h2">0</p>
                                        <p class="light">Friends </p>
                                    </li>
                                    <li>
                                        <p class="h2">0</p>
                                        <p class="light">Groups</p>
                                    </li>
                                    <li>
                                        <p class="h2">0</p>
                                        <p class="light">Affiliated Groups</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <article class="panel-group bs-accordion new-accordian" id="accordion" role="tablist" aria-multiselectable="true">
                        <section class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a id="detailsLbl" class="nav-link" role="button" data-toggle="collapse" data-parent="#accordion" href="#details" aria-expanded="false" aria-controls="details">
                                        Impact Reports
                                        <span class="fa fa-minus pull-right" aria-hidden="true"></span>
                                        <span class="fa fa-plus pull-right" aria-hidden="true"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="details" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="details" style="">
                                <div class="panel-body">

                                    <div class="row hours-graph">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bar-graph">
                                            <h3>Volunteer Hours By Month</h3>
                                            <img src="/myvoluntier/public/img/bar-graph.png" alt="">
                                        </div>    
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 blue-graph">
                                        <h3>Volunteer Hours By type</h3>
                                            <img src="/myvoluntier/public/img/blue-graph_new.jpg" alt="">
                                        </div>    
                                    </div> 

                                    <div class="row hours-graph">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bar-graph">
                                            <h3>Volunteers By Zip Code</h3>
                                            <img src="/myvoluntier/public/img/zip_graph.jpg" alt="">
                                        </div>    
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 blue-graph">
                                        <h3>Volunteers By Age Group</h3>
                                            <img src="/myvoluntier/public/img/age-group.jpg" alt="">
                                        </div>    
                                    </div>
                                    
                                    <div class="row text-profile-info vol-news-feedlist view-oppor-grp view-oppor-tab view-oppo-imp-dtl">
                                        <div class="col-12 col-md-12">
                                        <nav>
                                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active show"  id="summary-tab" href="#summary_ref" role="tab" aria-controls="summary" data-toggle="tab" aria-selected="true">Summary</a>
                                                <a class="nav-item nav-link" id="detailed-tab" href="#detailed" role="tab" aria-controls="detailed" data-toggle="tab" aria-selected="false">Detailed</a>
                                            </div>
                                        </nav>

                                            <div class="tab-content py-3 px-3" id="nav-tabContent">
                                                <div role="tabpanel" class="tab-pane fade active show" id="summary_ref">
                                                <div class="wrapper-friends">
                                                        <div class="wrapper-friends-list pb-0 pt-0">
                                                            <div class="wrapper-sort-table tesposive-table">
                                                                <div>
                                                                <h3>Top Volunteers</h3>
                                                                    <table id="example" class="table sortable">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col" class="nosort" data-sortcolumn="0" data-sortkey="0-0">
                                                                                    Name
                                                                                </th>
                                                                                <th scope="col" class="nosort" data-sortcolumn="1" data-sortkey="1-0">
                                                                                    Hours
                                                                                </th>
                                                                                <th scope="col" class="nosort" data-sortcolumn="2" data-sortkey="2-0">
                                                                                    Opportunities
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>Kelsey henry</td>
                                                                                <td><span class="fa fa-clock-o desktop-hide-icon"></span>2.5</td>
                                                                                <td><p class="desktop-hide-icon">Opportunities :</p>1</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Kennedi Wooldridge</td>
                                                                                <td><span class="fa fa-clock-o desktop-hide-icon"></span>2.5</td>
                                                                                <td><p class="desktop-hide-icon">Opportunities :</p>1</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Kiara Teague</td>
                                                                                <td><span class="fa fa-clock-o desktop-hide-icon"></span>2.5</td>
                                                                                <td><p class="desktop-hide-icon">Opportunities :</p>1</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Indigo Jones</td>
                                                                                <td><span class="fa fa-clock-o desktop-hide-icon"></span>2</td>
                                                                                <td><p class="desktop-hide-icon">Opportunities :</p>4</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Naila Smith-Woods</td>
                                                                                <td><span class="fa fa-clock-o desktop-hide-icon"></span>2</td>
                                                                                <td><p class="desktop-hide-icon">Opportunities :</p>2</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="detailed">
                                                    
                                                </div>
                                                    
                                            </div>
                                        </div>
                                    </div> 

                                </div>
                            </div>
                        </section>
                        <section class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a id="groupsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#groups" aria-expanded="false" aria-controls="groups">
                                        Groups
                                        <span class="fa fa-minus pull-right" aria-hidden="true"></span>
                                        <span class="fa fa-plus pull-right" aria-hidden="true"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="groups" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="">
                                <div class="panel-body">
                                                                                              
                                </div>
                            </div>
                        </section>
                        <section class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a id="serviceProjectTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#serviceProject" aria-expanded="false" aria-controls="serviceProject">
                                        Service Projects
                                        <span class="fa fa-minus pull-right" aria-hidden="true"></span>
                                        <span class="fa fa-plus pull-right" aria-hidden="true"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="serviceProject" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    
                                </div>
                            </div>
                        </section>
                        <section class="panel panel-default">
                            <div class="panel-heading" role="tab">
                                <h4 class="panel-title">
                                    <a id="serviceProjectTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#attribute" aria-expanded="false" aria-controls="serviceProject">
                                        Custom Attributes
                                        <span class="fa fa-minus pull-right" aria-hidden="true"></span>
                                        <span class="fa fa-plus pull-right" aria-hidden="true"></span>
                                    </a>
                                </h4>
                            </div>
                            <div id="attribute" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    
                                </div>
                            </div>
                        </section>
                    </article>



                </div>
            </div>    
        </div>    
    </div>
@endsection   
            

