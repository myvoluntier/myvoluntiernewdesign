<style>
    .chatImg{
        position: absolute !important;
        left: 93%;
        top: 70%;
    }
</style>
 
<div class="{{$nameClassUl}}">
    <div class="tab-content">
    <!-- Tab content start --> 
        <!-- tab container first div open at below  -->
        <div role="tabpanel" class="tab-pane new-opportunity-tab-data fade in active show" id="friends_section">
                        
                       <!-- ----friends_requests part start---- -->
                       <div class="friends_requests_blk"> 
                            <div class="main-text"><h2 class="h2 text-center">Friends Requests</h2></div>
                            <ul class="friends-list clearfix {{$nameClassUl}}">
                                @if(count($friend_requests))
                                    @foreach($friend_requests as $friend)
                                        <li>
                                        <div class="friend_desc_area">
                                            @if($friend->user_role == 'organization')
                                                <a href="{{url('/') . "/organization/profile/" . $friend->user_id}}">
                                            @else
                                                <a href="{{url('/') . "/volunteer/profile/" . $friend->user_id}}">
                                            @endif

                                                
                                                    <div class="friend_desc_inn"> 
                                                        <div class="avatar"
                                                            @if($friend->user_role === 'organization')
                                                            style="background-image:url({{$friend->logo_img === null ? asset('front-end/img/org/001.png') : str_replace (' ','%20',$friend->logo_img) }})">
                                                            @else
                                                                style="background-image:url({{$friend->logo_img === null ? asset('img/noprofilepic.png') : str_replace (' ','%20',$friend->logo_img) }}
                                                                )">
                                                            @endif
                                                            <span></span>
                                                        </div>
                                                        <div class="main-text"> 
                                                            <p class="name">{{ $friend->org_name ? ucfirst($friend->org_name) :  ucfirst($friend->first_name) . ' ' . ucfirst($friend->last_name)}}</p>
                                                            <p class="light">{{ implode(', ', array_filter([ucfirst($friend->city), ucfirst($friend->state)]))}}</p>
                                                        </div>
                                                    </div>
                                                
                                            </a>

                                            <a href="#" class="times" title="Remove Friend">
                                                <span onclick="request_update({{ $friend->id }}, {{ $friend->friend_id }}, 0, 0)">&times;</span></a>

                                            <div class="dashboard_org  ul.notifications-list">
                                                <div class="news-followed-box">
                                                    <ul class="notifications-list">
                                                        <a class="process" href="#" title="Accept Friend"
                                                        onclick="request_update({{ $friend->id }}, {{ $friend->user_id }}, 2)"><span>Accept</span></a>

                                                        <a class="process" href="#" title="Reject Friend"
                                                        onclick="request_update({{ $friend->id }}, {{ $friend->user_id }}, 0, 1)">Dismiss</a>
                                                    </ul>
                                                </div>
                                            </div>
                                        </a>
                                        </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>

                       <!-- ----friends_requests part end---- -->
        
                        <div class="friends_wrapper_blk">
                            <div class="main-text"><h2 class="h2 text-center">Friends</h2></div>
                            <ul class="friends-list clearfix all_fri_out_blk">
                                @if(count($friends))
                                    @foreach($friends as $friend)
                                        <?php 
                                            $chatData = App\Chat::getChatInfo($friend->friend_id);
                                            $args = ['chatId' => $chatData['chat_id']];
                                            if($chatData['chat_deleted']){
                                                $args['create'] = true;
                                                $args['opponent'] = $friend->friend_id;
                                            }
                                        ?>
                                        <li>
                                        <div class="friend_desc_area all_fri_blk">
                                            @if($friend->user_role == 'organization')
                                                <a href="{{url('/') . "/organization/profile/" . $friend->friend_id}}">
                                            @else
                                            <a href="{{url('/') . "/volunteer/profile/" . $friend->friend_id}}">
                                            @endif

                                                <div class="friend_desc_inn">                        
                                                        <div class="avatar"
                                                            @if($friend->user_role === 'organization')
                                                            style="background-image:url({{$friend->logo_img === null ? asset('front-end/img/org/001.png') : str_replace (' ','%20',$friend->logo_img) }})">
                                                            @else
                                                                style="background-image:url({{$friend->logo_img === null ? asset('img/noprofilepic.png') : str_replace (' ','%20',$friend->logo_img) }}
                                                                )">
                                                            @endif
                                                        </div>                        
                                                        <div class="main-text">
                                                            <p class="name">{{ $friend->org_name ? ucfirst($friend->org_name) :  ucfirst($friend->first_name) . ' ' . ucfirst($friend->last_name)}}</p>
                                                            <p class="light">{{ implode(', ', array_filter([ucfirst($friend->city), ucfirst($friend->state)]))}}</p>
                                                        </div>                       
                                                </div>
                                            </a>
                                            <a class="desktop_view" href="<?php echo route( Auth::user()->user_role . '-chat', $args )?>" class="chatImg" title="Chat" target="blank">
                                                <img src="{{ asset('front-end/img/chat.png') }}" alt="Chat" width="30px">
                                            </a>
                                            <a href="#" class="times" title="Remove Friend">
                                                <span onclick="request_update({{ $friend->id }}, {{ $friend->friend_id }}, 0, 0)">&times;</span>
                                            </a>
                                        </div>
                                        </li>
                                    @endforeach
                                @else
                                    You do not have friends yet
                                @endif
                            </ul>
                            <div class="wrapper-pagination">
                                <div class="wrapper-pagination">
                                    {{ $friends->links('components.pagination') }}
                                </div>
                            </div>

                        </div>
        </div>
        <!-- tab container first div close above  -->
        
        <!-- tab container second div open at below  -->
        <div role="tabpanel" class="tab-pane fade new-opportunity-tab-data" id="suggested_volunteers">
            <div class="suggested_volunteers_blk">
                    <div class="main-text"><h2 class="h2 text-center">Suggested Volunteers</h2></div>
                    <ul class="friends-list clearfix">
                        @if(isset($OtherVolunteers) && count($OtherVolunteers))
                            @foreach($OtherVolunteers as $volonteer)
                                <li>
                                <div class="friend_desc_area">
                                    <a href="{{url('/') . "/volunteer/profile/" . $volonteer->id}}">

                                        <div class="friend_desc_inn">                        
                                                <div class="avatar" style="background-image:url({{$volonteer->logo_img === null ? asset('img/noprofilepic.png') : str_replace (' ','%20',$volonteer->logo_img) }})">
                                                    <span></span>
                                                </div>
                                            
                                                <div class="main-text">
                                                    <p class="name">{{ucfirst($volonteer->first_name) . ' ' . ucfirst($volonteer->last_name)}}</p>
                                                    <p class="light">{{ implode(', ', array_filter([ucfirst($volonteer->city), ucfirst($volonteer->state)]))}}</p>
                                                </div>                       
                                        </div>
                                    </a>
                                </div>
                                </li>
                            @endforeach
                        @else
                        There are no top vountieers yet        
                        @endif
                    </ul>
            </div>

        </div>
        <!-- tab container second div close above  -->    
        
        <!-- tab container third div open at below  -->
        <div role="tabpanel" class="tab-pane fade new-opportunity-tab-data" id="suggested_organizations">
            <div class="suggested_organizations_blk">
                    <div class="main-text"><h2 class="h2 text-center">Suggested Organizations</h2></div>
                    <ul class="friends-list clearfix">
                        @if(isset($OtherOrgs) && count($OtherOrgs))
                            @foreach($OtherOrgs as $orgnization)
                                <li>
                                <div class="friend_desc_area">
                                    <a href="{{url('/') . "/organization/profile/" . $orgnization->id}}">
                                    <div class="friend_desc_inn">                        
                                                <div class="avatar"
                                                    style="background-image:url({{$orgnization->logo_img === null ? asset('front-end/img/org/001.png') : str_replace (' ','%20',$orgnization->logo_img) }})">
                                                    <span></span>
                                                </div>                        
                                            
                                                <div class="main-text">
                                                    <p class="name">{{ ucfirst($orgnization->org_name)}}</p>
                                                    <p class="light">{{ implode(', ', array_filter([ucfirst($orgnization->city), ucfirst($orgnization->state)]))}}</p>
                                                </div>                       
                                        </div>
                                    </a>
                                </div>
                                </li>
                            @endforeach
                        @else
                            Sorry, Organizations not found...!
                        @endif
                    </ul>
                </div>
            </div>
            <!-- tab container third div close above  -->  
            
    </div> 
    <!-- Tab content end -->          
</div>