@extends('layout.masterForAuthUser')

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">
@endsection

@section('content')

        <div class="wrapper-impact pb-0 pt-0">
        <!-- page heading start -->
        <div class="page_head-box green_bg_blk">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-8 col-lg-8">
                            <h2 class="h2">Impact</h2>
                            @if(session('admin_role')=='admin' && env('APP_ENV') == 'local' )
                                <a href="{{route('addTestVolImpactData' ,Auth::user()->id)}}"><i class="fa fa-refresh" aria-hidden="true"> Load Test Data</i></a>
                            @endif
                        </div>
                        <div class="col-12 col-md-4 col-lg-4 text-right print_btn_blk">
                             @if($isVerifiedDomain && env('ENABLEDASHBOARDS') == true)
                                <a class="customGreenBtn" target="_blank"  href="{{route('quick_sight_dashboard')}}" style="margin:0px 10px 0px 0px">View Dashboard</a>
                            @endif

                            <a href="javascript:void(0)" onclick="window.print()" class="print">
                                <span><i class="fa fa-print" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>       
        <!-- page heading end -->

        <div class="container">
            <div class="row align-items-center margin_b30 margin_mob0">
                <div class="col-12 col-md-4">
                    <div class="main-text">                                    
                    <div class="main-text rank_hour_blk rank_org_blk">
                        <div class="rank_pick" style="background-image:url('{{$user->logo_img == NULL ? asset('no-image.png') : $user->logo_img}}')">
                            <span></span>
                        </div>
                        <h3 class="h3 text-center hed_second org">Ranked</h3>
                        <div class="org_outer_blk">
                                <div class="rank_box_blk left_blk_flex">                            
                                    <p class="mb-0 text-center green middle"><strong class="big">{{$impactsData['ranking_all']}}</strong>{{numberDescender($impactsData['ranking_all'])}}</p>
                                    <p class="light text-center">Of All <br>Organizations</p>
                                </div>

                                <div class="rank_box_blk right_blk_flex">
                                    <div class="hour_txt_blk">
                                        <p class="mb-0 text-center green big"><strong class="big">{{$impactsData['ranking_org_type']}}</strong>{{numberDescender($impactsData['ranking_org_type'])}}</p>
                                        <p class="light text-center">Of "{{$impactsData['ranking_type_name']}}" Organizations</p>
                                    </div>                            
                                </div>
                            </div>
                            <div class="hour_box_blk">
                            <div class="hour_txt_blk">
                                <p class="mb-0 text-center green big"><strong class="middle big">{{$impactsData['tracked_hours']}}</strong><span class="green small">HOUR(S)</span></p>
                            </div>
                            <p class="light text-center">of service contributed</p>
                        </div>
                        </div>

                       
                    </div>

                </div>
                <div class="col-12 col-md-8 border-left">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" href="#thisMonth"
                               role="tab"
                               data-toggle="tab"><span>This Month</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               href="#lastMonth"
                               role="tab"
                               data-toggle="tab"><span>Last Month</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#last6Month"
                               role="tab"
                               data-toggle="tab"><span>Last 6 month</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link active show" href="#y2date"
                               role="tab"
                               data-toggle="tab"><span>Last 12 Months</span></a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade" id="thisMonth">
                            <div id="this-month-chart" class="wrapper-svg"></div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade " id="lastMonth">
                            <div id="last-month-chart" class="wrapper-svg"></div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="last6Month">
                            <div id="last6-month-chart" class="wrapper-svg"></div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade in active show" id="y2date">
                            <div id="year-date-chart" class="wrapper-svg"></div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row text-center margin_mob0 border-top-0 border-left-0 border-right-0 border-bottom-0">
                <div class="col-12 pb-0">
                    <div class="main-text">
                        @if(round($impactsData['tracked_hours']) != 0)
                            <h3 class="h3">Past Records</h3>

                            @if($top5_list['is_top5_state'])
                                <p class="light mb-0">Top 5 in the
                                    State (number
                                    of hours contributed)</p>
                            @endif

                            @if($top5_list['is_top10_country'])
                                <p class="light mb-0">Top 10 in the
                                    Country
                                    (number of hours contributed)</p>
                            @endif

                        @endif

                    </div>
                </div>
            </div>

            <div class="row align-items-center margin_b30 margin_mob0">
                <div class="col-12 col-md-8">
                    <div id="hours-by-opp-type" class="wrapper-svg"></div>
                </div>

                <div class="col-12 col-md-4 margin_b30">
                    <div class="main-text">
                        <p class="mb-0 text-center green middle"><strong class="big">{{$impactsData['total_opportunities']}}</strong></p>
                        <p class="mb-0 text-center green middle">Total</p>
                        <p class="light text-center">Opportunities</p>

                        <p class="mt-1 mb-0 text-center green big"><strong>{{number_format($impactsData['avg_hrs_per_oppr'] ,1)}}</strong></p>
                        <p class="mb-0 text-center green middle">Average</p>
                        <p class="light text-center">hour per opportunity</p>
                    </div>
                </div>
            </div>

            <div class="row margin_b30 margin_mob0">

                @if(!isset($user->parent_id))
                <div class="col-12 col-md-6">
                    <div id="top-5-suborg-chart" class="wrapper-svg"></div>
                </div>
                @endif

                <div class="col-12 col-md-6">
                    <div id="top-5-opportunities-chart" class="wrapper-svg"></div>
                </div>

                
            </div>

            <div class="row align-items-center margin_b30 margin_mob0">
                        <div class="col-12 col-md-6">
                            <div id="volnt-zipcode-chart" style="min-width: 110px; height: 250px; margin: 0 auto" class="wrapper-svg"></div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div id="volnt-agegroup-chart" style="min-width: 110px; height: 250px; margin: 0 auto" class="wrapper-svg"></div>
                        </div>   
            </div>


            <div class="row align-items-center margin_b30 margin_mob0">
                <div class="col-12 col-md-8">
                    <div id="top-5-volonteers-chart" class="wrapper-svg"></div>
                </div>

                <div class="col-12 col-md-4 margin_b30">
                            <div class="main-text">
                                <p class="mb-0 text-center green middle"><strong class="big">{{count($impactsData['volunteers'])}}</strong></p>
                                <p class="mb-0 text-center green middle">Total</p>
                                <p class="light text-center">volunteer</p>

                                <p class="mt-1 mb-0 text-center green big"><strong>{{number_format($impactsData['volunteers_avg'] ,1)}}</strong></p>
                                <p class="mb-0 text-center green middle">Average</p>
                                <p class="light text-center">hours per volunteer</p>
                            </div>
                        </div>

            </div>    

            <div class="row margin_b30 margin_mob0">
                <div class="col-12 col-md-6 border-left">
                    <div class="main-text">
                        <h3 class="h3 sub_title">Opportunity List</h3>
                        <div class="wrapper-table-impact">
                            <div>
                                <table class="oppListPagination">
                                    @if(count($impactsData['opprtunities_list']))
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Hours</th>
                                            <th>Volunteers</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($impactsData['opprtunities_list'] as $oppr)
                                            <tr>
                                                <td data-toggle="tooltip" data-placement="bottom" title="{{$oppr['name']}}" style="cursor: hand">

                                                    {{$oppr['name']}}</td>
                                                <td>
                                                    <p class="green">{{$oppr['hours']}}</p>
                                                </td>
                                                <td>{{$oppr['volonteers']}}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    @else
                                        <tr>
                                            Sorry..! No Opportunity found
                                        </tr>
                                    @endif

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 border-left">
                    <div class="main-text">
                        <h3 class="h3 sub_title">Volunteers List</h3>
                        <div class="wrapper-table-impact">
                            <div>
                                <table class="volListTable">
                                    @if(count($impactsData['volunteers']))
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Hours</th>
                                            <th>Opportunities</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($impactsData['volunteers'] as $member)
                                            <tr>
                                                <td>{{$member['name']}}</td>
                                                <td>
                                                    <p class="green">{{$member['hours']}}</p>
                                                </td>
                                                <td>{{$member['total_oppr']}}</td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    @else
                                        <tr>
                                            Sorry..! No volunteers found
                                        </tr>
                                    @endif

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@include('components.auth.modal_pop_up_show_oppr')
@endsection


@section('script')
    <script src="{{asset('js/highcharts.js')}}"></script>
    <script src="//cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>


    <script>

        $(document).ready(function() {
            $('.oppListPagination').dataTable({
                                "paging": true,
                                "lengthChange": false,
                                "searching": false,
                                "ordering": true,
                                "info": false,
                                "autoWidth": true,
                                "pageLength": 10,
                                "pagingType": "full_numbers",
                                "language": {
                                    "paginate": {
                                        "first": '<<',
                                        "next": '>',
                                        "previous": '<',
                                        "last": '>>'
                                    }
                                }
                            });
                            
                            $('.volListTable').dataTable({
                                "paging": true,
                                "lengthChange": false,
                                "searching": false,
                                "ordering": true,
                                "info": false,
                                "autoWidth": true,
                                "pageLength": 10,
                                "pagingType": "full_numbers",
                                "language": {
                                    "paginate": {
                                        "first": '<<',
                                        "next": '>',
                                        "previous": '<',
                                        "last": '>>'
                                    }
                                }
                            });

          });
        $(function(){

            $('[data-toggle="tooltip"]').tooltip();

            $('.totalHoursCls').on('click', function (e) {

                id = $(this).attr('data-id');
                $('#records_table').empty();

                var action = '{{route('organization-impact-vol-opprs')}}';

                $.ajax({
                    type: 'post',
                    url: action,
                    data: { 'id': id },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function () {},
                    success: function (response) {

                        console.log(response.length);
                        if( response.length > 0 ) {
                            var trHTML = '';
                            $.each(response, function (i, item) {
                                trHTML += '<tr><td>' + item.title + '</td><td>' + item.city + '</td><td>' + item.state + '</td><td>' + item.contact_name + '</td><td>' + item.contact_number + '</td><td>' + item.hours + '</td></tr>';
                            });

                            $('#records_table').append(trHTML);
                        }

                        else
                            toastr.error("no record found in the system.", "Error!");
                    }
                });

                $('#showOpprVol').modal('show');
            });

            $('.nav-link').click(function(e){
                id = '#'+$(this).data('id'); //tab-content
                $('.nav-tabs a[href="' + id + '"]').tab('show');
            })
        })
    </script>

    <script>
        Highcharts.chart('this-month-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -30,
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + ' hours</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                        @if(count($impactsData['this_month_chart'])>0)
                        @foreach($impactsData['this_month_chart'] as $object)
                            ['{{date('d-M-Y',strtotime($object->logged_date))}}', {{($object->SUM/60)}}],
                        @endforeach
                        @endif
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('last-month-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -30,
                    style: {
                        fontSize: '12px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + ' hours</b>';
                }
            },
            series: [{
                name: 'Tracking',
                color: '#03a9f4',
                data: [
                        @if(count($impactsData['last_month_chart'])>0)
                        @foreach($impactsData['last_month_chart'] as $object)
                            ['{{date('d-M-Y',strtotime($object->logged_date))}}', {{($object->SUM/60)}}],
                        @endforeach
                        @endif
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('last6-month-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '12px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + ' hours</b>';
                }
            },
            series: [{
                name: 'Tracking',
                color: '#03a9f4',
                data: [
                        @if(count($impactsData['last_6month_chart'])>0)
                            @foreach($impactsData['last_6month_chart'] as $object)
                                ['{{date('M-Y',strtotime($object->logged_date))}}', {{($object->SUM/60)}}],
                            @endforeach
                        @endif
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('year-date-chart', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '12px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + ' hours</b>';
                }
            },
            series: [{
                name: 'Tracking',
                color: '#03a9f4',
                data: [
                   @if(count($impactsData['year_date_chart'])>0)
                        @foreach($impactsData['year_date_chart'] as $object)
                            ['{{date('M-Y',strtotime($object->logged_date))}}', {{($object->SUM/60)}}],
                        @endforeach
                    @endif
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });


        Highcharts.chart('hours-by-opp-type', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Hours by Opportunity type'
            },

            xAxis: {
                categories: {!! json_encode(getLast12MonthsY()) !!},
            },
            yAxis: {
                title: {
                    text: 'Hour'
                }
            },
            tooltip: {
                split: false
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },

            series: [
                    @forelse($impactsData['hours_oppr_type_chart'] as $ind => $oppr_type)
                    {
                        name: '{{$ind}}',
                        data: [{{implode( ',',$oppr_type)}}]
                    },
                        @empty
                    {},
                @endforelse
            ]
        });

        Highcharts.chart('top-5-opportunities-chart', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Top 5 Opportunities',
                style:{ "color": "#27282f", "fontSize": "24px",  "fontFamily": 'Open Sans, sans-serif' }
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                lineWidth:0,
                tickLength:0,
                categories: {!! json_encode(array_keys($impactsData['top_5_opprtunities'])) !!},
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#28292e'
                    }
                }
            },
            yAxis: {
                visible:false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontSize: '14px',
                            fontFamily: 'Open Sans, sans-serif',
                            fontWeight: 'normal',
                            color: '#fff',
                            textOutline: ''
                        } ,
                        align: 'right'
                    },
                    showInLegend: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            series: [{
                color: '#42bd41',
                data: [
                    @if(count($impactsData['top_5_opprtunities'])>0)
                    @foreach($impactsData['top_5_opprtunities'] as $key => $val)
                        {{$val}},
                    @endforeach
                    @endif
                ],

                dataLabels:{
                    format:'{y} hrs'
                }
            }]
        });
        Highcharts.chart('top-5-suborg-chart', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Top 5 Sub Organization',
                style:{ "color": "#27282f", "fontSize": "24px",  "fontFamily": 'Open Sans, sans-serif' }
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                lineWidth:0,
                tickLength:0,
                categories: {!! json_encode(array_keys($impactsData['top_5_sub_org'])) !!},
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#28292e'
                    }
                }
            },
            yAxis: {
                visible:false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontSize: '14px',
                            fontFamily: 'Open Sans, sans-serif',
                            fontWeight: 'normal',
                            color: '#fff',
                            textOutline: ''
                        } ,
                        align: 'right'
                    },
                    showInLegend: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            series: [{
                color: '#42bd41',
                data: [
                    @if(count($impactsData['top_5_sub_org'])>0)
                    @foreach($impactsData['top_5_sub_org'] as $key => $val)
                        {{$val}},
                    @endforeach
                    @endif
                ],

                dataLabels:{
                    format:'{y} hrs'
                }
            }]
        });

    </script>


    <script>

        var volPieChartZip = {!! json_encode($impactsData['volunteers_pie_zip']) !!}
        var volPieChartAge = {!! json_encode($impactsData['volunteers_pie_age']) !!}

            Highcharts.chart('top-5-volonteers-chart', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Top 5 volunteers',
                    style:{ "color": "#27282f", "fontSize": "24px",  "fontFamily": 'Open Sans, sans-serif' }
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    lineWidth:0,
                    tickLength:0,

                    categories: [
                        @if(count($impactsData['volunteersTop5Chart'])>0)
                                @foreach(array_keys($impactsData['volunteersTop5Chart']) as $ky)
                            '{{removeNumFromStart($ky)}}',
                        @endforeach
                        @endif
                    ],
                    title: {
                        text: ''
                    },
                    labels: {
                        style: {
                            fontSize: '14px',
                            fontWeight: 'normal',
                            fontFamily: 'Open Sans, sans-serif',
                            color: '#28292e'
                        }
                    }
                },
                yAxis: {
                    visible:false
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontSize: '14px',
                                fontFamily: 'Open Sans, sans-serif',
                                fontWeight: 'normal',
                                color: '#fff',
                                textOutline: ''
                            } ,
                            align: 'right'
                        },
                        showInLegend: false
                    }
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    enabled: false
                },
                series: [{
                    color: '#42bd41',
                    data: [
                        @if(count($impactsData['volunteersTop5Chart'])>0)
                        @foreach($impactsData['volunteersTop5Chart'] as $key => $val)
                            {{$val}},
                        @endforeach
                        @endif
                    ],
                    dataLabels:{
                        format:'{y} hrs'
                    }
                }]
            });
    </script>

    <script>
        Highcharts.chart('volnt-zipcode-chart', {
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Volunteers by Zip Code'
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + ' volunteers</b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                itemStyle:{
                    color:'#9ca0a1',
                    fontSize:'14px',
                    fontWeight:'normal',
                    fontFamily: 'Open Sans, sans-serif',
                    textOverflow:'ellipsis'
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: volPieChartZip
            }]
        });
    </script>

    <script>
        Highcharts.chart('volnt-agegroup-chart', {
            chart: {

                type: 'pie'
            },
            title: {
                text: 'Volunteers by Age group'
            },
            tooltip: {
                enabled: true,
                formatter: function() {
                    return '<b>' + this.y + ' volunteers</b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                itemStyle:{
                    color:'#9ca0a1',
                    fontSize:'14px',
                    fontWeight:'normal',
                    fontFamily: 'Open Sans, sans-serif',
                    textOverflow:'ellipsis'
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: volPieChartAge
            }]
        });
    </script>

@endsection

