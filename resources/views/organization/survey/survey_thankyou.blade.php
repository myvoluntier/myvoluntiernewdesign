@extends('layout.frame')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

    </style>
@endsection

@section('body')

<body>   
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5426731.js"></script>

    <div class="offcanvas-menu">
        <div class="container">
            <div class="header-center-menu">
                <ul class="nav">
                    @include('components.non-auth.buttons_registration_and_login')
                </ul>
            </div>
        </div>
    </div>

    <div class="offcanvas-contentarea">

        <div class="wrapper_bottom_footer">
            @include('components.non-auth.header')

            <div class="row-content">
                <input class="type-user" type="hidden" value="Volunteer">

                @if(request()->confirmed)
                <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                    <strong>Hi, </strong> Your account has been confirmed. Please login to continue.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

    <div class="container text-center">
        <br><br><br><br>
        <p><h3>Hi {{$name[0]['first_name']}} {{$name[0]['last_name']}}</h3></p>
        <p><h4>Thanks for participating in the survey and we will improve the things based on your feedback. Your response will keep confidential and will not be used for any other purpose.<h4></p>
        <p><h4>Thank You!</h4></p>
    </div>
            
    {{-- @include('components.opportunityEmailBlasts') --}}
</body>
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
@endsection
