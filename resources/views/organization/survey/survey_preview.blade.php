@extends('layout.masterForAuthUser')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

    </style>
@endsection

@section('content')
    <div class="wrapper-opportunities survey_result_blk">
        <div class="add-new-box">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-12 col-lg-12"><h2 class="h2 text-center">{{$name[0]->survey_name}}-Preview</h2></div>
                </div>
            </div>
        </div>

            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                       <!--==== Preview design start =========-->
                                <div class="preview_list_outer">
                                    <div class="btn_exit">
            
                                        Survey type: <a href="javascript:void(0);">{{$name[0]->survey_type}}</a>
                                    </div>
                                 <div>
                                   </div>
                                   <!-- Result start -->
                                    <div class="result_list">
                                        <div class="question_no"></div>
                                        <div class="question_answer_blk">
                                       
                                         @foreach($options as $key=>$list)                                                                                    
                                            <div class="question_name">
                                            <span> {{$key+1}}. {{$list->questions}}</span>
                                            </div>
                                          <div class="form-check form-check-inline own-form-field">
                                                @foreach($list->ques_option as $op)
                                                    <label class="form-check-label" for="option">
                                                    <input class="" disabled type="{{$list->question_type=="single" ? 'radio' :'checkbox'}}" name="option" id="option" value="{{$op->options}}">{{$op->options}}
                                                    <span class={{$list->question_type=="single" ? 'checkmark' : "checkbox2"}}></span>   
                                                    </label>
                                                @endforeach 
                                         </div>                                                                                      
                                         @endforeach
                                        </div>
                                    </div>
                                    <!-- Result end -->
                                    <div class="form_btn_blk preview_btn_blk">
                                        <div class="bottom_btn">
                                            <a id="" class="button-fill" href="{{route('organization-opportunity-survey-list',$name[0]->opportunity_id)}}">Close Preview</a>
                                        </div>
                                    </div>
                               </div>  
                            <!--======== Preview design end ========-->
                      
                        
                        



                    </div>
                 </div>
                </div>


        
                
    </div>
           
    @include('components.opportunityEmailBlasts')
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    
@endsection
