@extends('layout.masterForAuthUser')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

        .massMail {
            padding: 13px !important;
            line-height: 5px !important;
            margin-left: 7px !important;
        }

        .qrCodeScan {
            padding: 13px !important;
            line-height: 5px !important;
        }

        .row-centered {
            text-align:center;
        }

        .modal { overflow-y: auto !important; }

    </style>
@endsection

@section('content')
    <div class="wrapper-opportunities">

        <div class="add-new-box">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-12 col-lg-12"><h2 class="h2 text-center">Add Survey</h2></div>
                   
                </div>
            </div>
        </div>
        
                <form id="addSurveyQues" class="float-label">
                    <div class="type_survey_outer">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-12">
                                    <div class="alert alert-danger login_error_div" role="alert" style="display: none">
                                        <span id="login_errors_div"></span>
                                    </div>

                                    <div class="checkbox-form survey_btn_blk">
                                        <div class="checkbox-form" id="chck_user">
                                            <div class="form-check form-check-inline title">
                                                <h5>Type of Survey:</h5>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label" for="exit_survey">
                                                    Exit Survey
                                                <input class="form-check-input" type="radio" name="survey_form" id="exit_survey" value="exit_survey" checked="">
                                                <span class="checkmark"></span>         
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label" for="intermediate_survey">
                                                    Intermediate Survey
                                                <input class="form-check-input" type="radio" name="survey_form" id="intermediate_survey" value="intermediate_survey">
                                                <span class="checkmark"></span>    
                                                </label>
                                            </div>
                                        </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                        <div class="question_outer_blk">        
                            <div class="question_blk"> 
                                <label>Q</label> 
                                 
                                <div class="question_select_blk">
                                    <input id="question"  class="option" type="text">    
                                    <select name="surveyOptions" id="surveyOptions">
                                        <option  value="1">MCQ</option>
                                        <option value="2">Checkbox</option>
                                    <select> 
                                </div>                      
                            </div>

                            <div class="question_option_blk">

                                <div class="option_field_blk option_field_1">
                                    <input id="option1" class="option" type="text" >
                                    <div class="add_minus">
                                        <a href="#" onclick="addRow(this)">Add</a>
                                        <a href="#" onclick="minusRow(this)">Minus</a>
                                    </div> 
                                </div>
                                <div class="option_field_blk option_field_2">
                                    <input id="option2" class="option"  type="text"> 
                                    <div class="add_minus">
                                        <a href="#" onclick="addRow(this)">Add</a>
                                        <a href="#" onclick="minusRow(this)">Minus</a>
                                    </div>
                                </div>
                                <div class="option_field_blk option_field_3">
                                    <input id="option3" class="option"  type="text"> 
                                    <div class="add_minus">
                                        <a href="#" onclick="addRow(this)">Add</a>
                                        <a href="#" onclick="minusRow(this)">Minus</a>
                                    </div>
                                </div>
                                <div class="option_field_blk option_field_4">
                                    <input id="option4" class="option"  type="text">
                                    <div class="add_minus">
                                        <a href="#" onclick="addRow(this)">Add</a>
                                        <a href="#" onclick="minusRow(this)">Minus</a>
                                    </div> 
                                </div>
                            </div>
                        </div>
                            

                            <input id="oppor_id" type="hidden" value="{{$id}}">
                            <div class="form-group">
                                <div class="wrapper-link two-link login-wrap-link">
                                    <a id="btn_survey_add" class="button-fill" href="#">
                                        Save
                                    </a>
                                </div>
                            </div>
                            <div class="form-group nmb">
                                <div class="wrapper-link two-link text-center login-wrap-link">
                                    <a href="{{route('signUp')}}" class="button-border">
                                        Sign UP
                                    </a>
                                    <p>Need to register?</p>
                                </div>
                        </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    @include('components.opportunityEmailBlasts')
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <script>
   var myArray= [];
 $("#btn_survey_add").click(function(){
    var obj = {};
    $('.option').each( function(key,value) { 
        obj[value.id]=$( this ).val();
        });
        myArray.push(obj);
        console.log(myArray);
        $('.question_option_blk').find(':input').val('');
        $('.question_select_blk').find(':input').val('');
    });

    
var count = $('form').find('.option_field_blk').length;
function addRow(addField){
    if($("#surveyOptions").val()=="1"){
        $(".square").remove();
     var labelField="<label class='cir'></label>";   
   }
   else{
    $(".cir").remove();
    var labelField="<label class='square'></label>";
   }
     count++;
     var fieldHTML = '<div class="option_field_blk option_field_'+count+'">'+labelField+'<input id="option'+count+'" class="option" type="text" >  <div class="add_minus"><a href="#" onclick="addRow(this)">Add</a><a href="#" onclick="minusRow(this)">Minus</a></div></div>';
     $(addField).parent().parent().after(fieldHTML);
}

function minusRow(minusField){
    $(minusField).parent().parent().remove();
}

$( document ).ready(function() {
//  if($("#surveyOptions").val()=="1"){
//      var labelField="<label class='cir'></label>";
//      $(".square").remove();
//     $(".option_field_blk").prepend(labelField);
//    }
//    else{
//     var labelField="<label class='square'></label>";
//     $(".cir").remove();
//     $(".option_field_blk").prepend(labelField);
//    }
  })    ;   
</script>
@endsection
