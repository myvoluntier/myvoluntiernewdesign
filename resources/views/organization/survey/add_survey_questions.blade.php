@extends('layout.masterForAuthUser')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

        .massMail {
            padding: 13px !important;
            line-height: 5px !important;
            margin-left: 7px !important;
        }

        .qrCodeScan {
            padding: 13px !important;
            line-height: 5px !important;
        }

        .row-centered {
            text-align:center;
        }

        .modal { overflow-y: auto !important; }

    </style>
@endsection

@section('content')
    <div class="wrapper-opportunities">

        <div class="add-new-box">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-12 col-lg-12"><h2 class="h2 text-center">Add Survey</h2></div>
                   
                </div>
            </div>
        </div>
        
                <form id="addSurveyQues" class="float-label">
                    <div class="type_survey_outer">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-12">
                                    <div class="alert alert-danger login_error_div" role="alert" style="display: none">
                                        <span id="login_errors_div"></span>
                                    </div>

                                    <div class="checkbox-form survey_btn_blk">
                                        <div class="checkbox-form" id="chck_user">
                                        <div class="survey_name_blk">
                                            <div class="form-check form-check-inline title">
                                                <h5>Type of Survey:</h5>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label" for="exit_survey">
                                                    Final Survey
                                                <input class="form-check-input" type="radio" name="survey_form" id="exit_survey" value="Final" checked="">
                                                <span class="checkmark"></span>         
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label" for="intermediate_survey">
                                                    Intermediate Survey
                                                <input class="form-check-input" type="radio" name="survey_form" id="intermediate_survey" value="Intermediate">
                                                <span class="checkmark"></span>    
                                                </label>
                                            </div>
                                        </div>

                                        <div class="survey_date_blk">
                                            <div class="form-check form-check-inline" id="intermediateSurvey" >
                                                <label class="form-check-label">Start date:</label>
                                                <input type="text" class="border_bot" name="start_date" id="start_date" readonly='true'>                                                    
                                                
                                                <label class="form-check-label">End date:</label>
                                                 <input type="text" class="border_bot" name="end_date" id="end_date" readonly='true'>
                                                
                                                <input type="hidden" id="oppor_startDate" value="{{$dates->start_date}}">
                                                <input type="hidden" id="oppor_endDate" value="{{$dates->end_date}}">
                                            </div>
                                            <div class="form-check form-check-inline" id="endSurvey" >                                                
                                                <input type="hidden" name="exitStart_date" id="exitStart_date" value="{{$dates->end_date}}">
                                                  
                                                <label class="form-check-label">End date:</label>
                                                <input type="text" class="border_bot" name="exitEnd_date" id="exitEnd_date" readonly='true'>
                                                
                                            </div>
                                        </div>
                                        </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="container">
                    <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <div class="survey_name_blk">
                                        <h5>Survey Name :</h5>
                                        <div class="form-check form-check-inline">
                                            <input style="padding: 0;" id="survey_name" maxLength="30" class="form-control"  type="text" placeholder="enter name">
                                        </div>
                                </div>
                            </div>
                    </div>       
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <!-- First question start -->
                                <div class="question_outer_blk">        
                                    <div class="question_blk"> 
                                        <label>Q<span id="ques_no">1</span></label> 
                                         {{-- question --}}
                                        <div class="question_select_blk">
                                            <input id="question" maxLength="120" class="form-control option"  type="text">    
                                            <select name="surveyOptions" id="ques_type" class="surveyOptions option" onchange="surveyType()">                                             
                                                <option value="single">Single Choice</option>
                                                <option value="multiple">Multiple Choice</option>
                                            <select> 
                                        </div>                      
                                    </div>
                                     {{-- options --}}
                                    <div class="question_option_blk">
                                        <div class="option_field_blk option_field_1">
                                            <label></label> 
                                            <input id="option1" maxLength="100" class="form-control option"  type="text" >
                                            <div class="add_minus">
                                                <a href="#" onclick="addRow(this)"><img src="{{asset('img/plus_icon_survey.png')}}" alt=""></a>
                                                <a href="#"onclick="minusRow(this)"><img src="{{asset('img/minus_icon_survey.png')}}" alt=""></a>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                            <input id="oppor_id" type="hidden" value="{{$id}}">
                            <input id="org_id" type="hidden" value="{{$org_id}}">                           
                            
                            <div class="form_btn_blk">
                                <div class="top_btn">                                   
                                <div class="left_blk">
                                    <a id="btn_survey_reset" class="button-fill" href="javaScript:void(0);">Cancel</a>
                                    <a id="btn_survey_custom" class="button-fill" href="javaScript:void(0);">Save</a>
                                    <div id="savedSurvey" style="display:none" class="text-danger" >Question successfully saved</div>
                                </div>
								<div class="right_blk">
                                        <a href="JavaScript:Void(0);" id="btn_survey_add" class="button-fill">Add Question</a>
                                        <div id="nextQues" style="display:none" class="text-danger" >Question successfully saved</div>
                                </div>
                            </div>
                            <div class="bottom_btn">
                                <a id="btn_survey_done" class="button-fill" href="javaScript:void(0);">Complete survey</a>
                                <div class="saveques"></div>
                                <div class="surveyCompleted text-danger" style="display:none;">Survey successfully created</div>
                                <div class="text-danger">*When questions get completed then click on 'complete survey' button</div>
                            </div>
                        </div>
                </form>
                </div>
            </div>
        </div>
    </div>
    @include('components.opportunityEmailBlasts')
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script  src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <link href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css" rel="stylesheet">
   <script>
  $( document ).ready(function() {

    surveyType();

    if($('#exit_survey').is(':checked')){  
        $("#intermediateSurvey").hide();
        $("#endSurvey").show();
       }
    else{
        $("#intermediateSurvey").show();
        $("#endSurvey").hide();
    }
   
    $('input[type=radio]').click(function(){
    var survey_type= $('input[type=radio][name=survey_form]:checked').attr('id');
    if(survey_type=="intermediate_survey"){
        $("#intermediateSurvey").show();
        $("#endSurvey").hide();
    }else{
        $("#intermediateSurvey").hide(); 
        $("#endSurvey").show();
    }
    });

     //date picker //
     var minDate = moment($("#oppor_startDate").val()).format('YYYY-MM-DD');
     var maxDate = moment($("#oppor_endDate").val()).format('YYYY-MM-DD');

    $("#start_date").datepicker({
      showAnim: 'drop',
      numberOfMonth:1,
      minDate:new Date(),
      maxDate:maxDate,
      dateFormat:'yy-mm-dd',
    });

    $("#end_date").datepicker({
      showAnim: 'drop',
      numberOfMonth:1,
      minDate:minDate,
      maxDate:maxDate,
      dateFormat:'yy-mm-dd',
    });

    $("#exitEnd_date").datepicker({
      showAnim: 'drop',
      numberOfMonth:1,
      minDate:maxDate,
      dateFormat:'yy-mm-dd',
    });
    //end date picker //
   });

  
 var myArray= [];
 var num = 1;
 $("#btn_survey_add").click(function(){
    $(".saveques").empty(); 
    var obj = {};
    var flag=0;
    if($("#question").val()==""){
        $("#question").addClass('error');
        flag++;
    }else{
        $("#question").removeClass('error');
    }
    if($("#option1").val()==""){
        $("#option1").addClass('error');
        flag++;
    }else{
        $("#option1").removeClass('error');
    }
    if(flag==0){
        num++;
    if((myArray.length-1) < 0){ 
      $('.option').each( function(key,value) { 
            if($( this ).val()==""){
                return;
            }
            obj[value.id]=$( this ).val();
        });
         myArray.push(obj);
         $("#ques_no").html(num);
        $('.question_option_blk').find(':input').val('');
        $('.question_select_blk').find('input').val('');
    }else{
     var edit=myArray[myArray.length-1];
      if(edit['question']!=$('.option').val()){
        $('.option').each( function(key,value) { 
            if($( this ).val()==""){
                return;
            }
            obj[value.id]=$( this ).val();
        });
        myArray.push(obj);
        $("#ques_no").html(num);
        $('.question_option_blk').find(':input').val('');
        $('.question_select_blk').find('input').val('');
      }else{
        $("#ques_no").html(num);
        $('.question_option_blk').find(':input').val('');
        $('.question_select_blk').find('input').val(''); 
      }
     }
     $("#nextQues").show().delay(2000).fadeOut();
    }
      
      });



  $("#btn_survey_custom").click(function(){
    var obj={};
    var flag=0;
    if($("#question").val()==""){
        $("#question").addClass('error');
        flag++;
    }else{
        $("#question").removeClass('error');
    }
    if($("#option1").val()==""){
        $("#option1").addClass('error');
        flag++;
    }else{
        $("#option1").removeClass('error');
    }
  if(flag==0){
    if((myArray.length-1) < 0){   
    $('.option').each( function(key,value) { 
            if($( this ).val()==""){
                return;
            }
            obj[value.id]=$( this ).val();
        });
        myArray.push(obj); 
        $("#savedSurvey").show().delay(2000).fadeOut();
        $(".saveques").empty();     
     }else{
      var edit=myArray[myArray.length-1];
      if(edit['question']===$('.option').val()){
        myArray.pop();
      $('.option').each( function(key,value) { 
            if($( this ).val()==""){
                return;
            }
            obj[value.id]=$( this ).val();
        });
        myArray.push(obj);
        $("#savedSurvey").show().delay(2000).fadeOut();
        $(".saveques").empty();
      }else{
        $('.option').each( function(key,value) { 
            if($( this ).val()==""){
                return;
            }
            obj[value.id]=$( this ).val();
        });
        myArray.push(obj);
        $("#savedSurvey").show().delay(2000).fadeOut();
        $(".saveques").empty();
       }
     }  

    }
    });

    //cancel button 
    $("#btn_survey_reset").click(function(){
            $('.question_option_blk').find(':input').val('');
            $('.question_select_blk').find('input').val(''); 
        });
    //end cancel button




var count = $('form').find('.option_field_blk').length;
function addRow(addField){
     count++;
     if($(".surveyOptions").val()=="single"){
        var type= "cir";
   }
   else{
       var type= "square";
   }
     var fieldHTML ='<div class="option_field_blk option_field_'+count+'"><label class="'+type+'"></label><input id="option'+count+'" class="option" type="text" ><div class="add_minus"> <a href="#" onclick="addRow(this)"><img src="{{asset('img/plus_icon_survey.png')}}" alt=""></a> <a href="#" onclick="minusRow(this)"><img src="{{asset('img/minus_icon_survey.png')}}" alt=""></a> </div> </div>';
     $(addField).parent().parent().after(fieldHTML);
 }


function minusRow(minusField){
    $(minusField).parent().parent().remove();
}


   

function surveyType(){
    if($(".surveyOptions").val()=="single"){
        $(".option_field_blk").find('label').removeClass('square');
        $(".option_field_blk").find('label').addClass('cir');
   }
   else{
    $(".option_field_blk").find('label').removeClass('cir');
        $(".option_field_blk").find('label').addClass('square');
   }
} 


$("#btn_survey_done").click(function(){
    var obj={};
    var flag=0;
    if($("#survey_name").val()==""){
        $("#survey_name").addClass('error');
        flag++;
    }else{
        $("#survey_name").removeClass('error');
    }
   if(myArray.length==0){
    if($("#question").val()==""||$("#option1").val()==""){
        $("#question").addClass('error');
        $("#option1").addClass('error');
        flag++;
    }else{
        $("#question").removeClass('error');
        $("#option1").removeClass('error');
     }
   }

   


   if($('input[name="survey_form"]:checked').val()=="Final"){ 
        if($("#exitEnd_date").val()==""){
            $("#exitEnd_date").addClass('error');
            flag++;
        }else{
            $("#exitEnd_date").removeClass('error');
           var startDate=$("#exitStart_date").val();
           var endDate=$("#exitEnd_date").val();
        }  
   }else{
     if($("#start_date").val()=="" && $("#end_date").val()==""){
        $("#start_date").addClass('error');
        $("#end_date").addClass('error');
            flag++;
        }else{
        $("#start_date").removeClass('error');
        $("#end_date").removeClass('error');
        var startDate=$("#start_date").val();
        var endDate=$("#end_date").val();
        }
   }

  
    if(flag==0){    
         if(myArray.length>0){
             $signal=0;
           $(".saveques").empty(); 
            var edit=myArray[myArray.length-1];
                if($('.option').val()==''){
                    $(".saveques").empty();
                }
                else if($('.option').val()!=''){
                    if(edit['question']!=$('.option').val()){
                    $(".saveques").html('click on save button, then complete survey').css("color","red");
                        $signal++;
                        }
                }
     if($signal==0){  
         var formData={
            survey_type:$('input[name="survey_form"]:checked').val(),
            oppor_id: $("#oppor_id").val(),
            org_id:$("#org_id").val(),
            name: $("#survey_name").val(),
            ques_option : myArray ,
            surveyStartDate:startDate,
            surveyEndDate:endDate,
         }
          var url = API_URL + 'opportunity/save_survey';
            $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
         var type = "post";
            $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function (data) {
            if(data.result=="success"){
                $(".surveyCompleted").show();
                setTimeout(function(){ 
                    window.location.replace(SITE_URL + "organization/survey_opportunity/"+data.oppor_id);
                     }, 2000);
            }
            },
            error: function (data) {
            }
            });
         }
        }else{
       $(".saveques").html('click on save button, then complete survey').css("color","red");
        }

    }
  


});
</script>
@endsection
