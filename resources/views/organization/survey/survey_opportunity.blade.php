@extends('layout.masterForAuthUser')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

        .massMail {
            padding: 13px !important;
            line-height: 5px !important;
            margin-left: 7px !important;
        }

        .qrCodeScan {
            padding: 13px !important;
            line-height: 5px !important;
        }

        .row-centered {
            text-align:center;
        }

        .modal { overflow-y: auto !important; }

    </style>
@endsection

@section('content')
    <div class="wrapper-opportunities servey_list_blk">

        <div class="add-new-box">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-8 col-lg-8">
                        <p>Survey</p>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4 text-right">
                        <a href="{{route('organization-opportunity-survey-add',['id'=>$id])}}" class="add-new"><span>Add new survey</span></a>
                    </div>
                </div>
            </div>
        </div>
   

        <div class="wrapper-tablist">
            <div class="container">
                <div class="inner-tablist-wrap">
                    <ul>
                    @if(count($data)>0)
                      @foreach($data as $list)
                    <li>
                            <div class="left_blk">
                                <h5 class="">{{$list['survey_name']}}<div class="status_txt">{{$list['status']}}</div></h5>
                                <div class="status_blk">
                                    <span>Answered: {{$list['answered']}}</span>
                                    <span>Skipped: {{$list['skipped']}}</span>
                                    <span>Created <?php $timestamp=strtotime($list['created_at']) ;
                                            echo date('m/d/Y', $timestamp)     ?>
                                    </span>
                                </div>
                            </div>
                            <?php $code = encrypt($list['id']."/".$list['organization_id']."/".$list['opportunity_id']); ?>
                            <div class="buttons">
                                <a title="Edit" href="{{$list['status']=="pending" ? route('organization-opportunity-survey-edit',['id'=>$list['id'],'opporId'=>$list['opportunity_id']]):'#' }}" class="{{$list['status']=="pending" ? 'edit' :'icon-disabled' }}"><span><img src="{{asset('img/oppo-edit.png')}}" alt=""></span></a>
                                <a title="Delete" href="#" class="{{$list['status']=='pending' ? 'remove' :'icon-disabled'}}" data-id="{{$list['id']}}"><span><img src="{{asset('img/oppo-del.png')}}" alt=""></span></a>
                                <a title="Preview" href="{{route('organization-opportunity-survey-preview',$list['id'])}}" class=""><span><img src="{{asset('img/preview_icon_survey.png')}}" alt=""></span></a>
                                <a title="{{$list['status']=="pending" ? 'Publish':'Closed' }}" href="javascript:void(0);" class="{{$list['status']=="pending" ? 'surveyPublish':'unpublish' }}" data-id="{{$list['id']}}"><span><img src="{{$list['status']=="pending" ? asset('img/publish_icon_survey.png') : asset('img/publish_icon_survey.png') }}" alt=""></span></a>
                                <a title="Email Blast To Members" href="javascript:void(0)" data-value="{{$code}}" class="{{$list['status']=="pending" ? 'icon-disabled' : 'emailVol'}}"><span><img src="{{asset('img/oppo-email.png')}}" alt=""></span></a>
                                <a title="Result" href="{{route('organization-opportunity-survey-result',$code)}}" class=""><span><img src="{{asset('img/result_icon_survey.png')}}" alt=""></span></a>  
                            </div>
                            <input type="hidden" class="encryptedCode" data-id="{{$code}}">                                
                        </li>
                          @endforeach
                          @else
                              <div style="text-align:center">No survey found yet</div>
                          @endif
                    </ul>
                </div>
            </div>
        </div>
       </div>

    <div class="modal fade modal-popups" id="delete_modal">
        <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header" style="text-align:center;">
                <h4 class="modal-name">
                    Are You Sure You Want To Delete ?
                </h4>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" data-survey-id="" class="button-fill button-grey removePop" id="delete_link">Delete</a>
                <button type="button" class="button-fill" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    </div>

    <div class="modal fade modal-popups" id="publish_modal">
        <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header" style="text-align:center;">
                <h4 class="modal-name">
                    Are You Sure You Want To Publish ?
                </h4>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" data-survey-id="" class="button-fill button-grey publishPop" id="publish_link">Publish</a>
                <button type="button" class="button-fill" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
    </div>

  <!-- The mail blast Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <div id="showMsg" class="alert alert-success" style="display:none"></div>
          <div id="errorMsg" class="alert alert-danger" style="display:none">Please select email</div>
          
          <h4 class="modal-title">Email send to:</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary float-left submitSendMail">Submit</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
       </div>
     </div>
   </div>
  {{-- end email blast --}}

    {{-- <div class="modal fade modal-popups" id="qrCodeScanModal" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="">QR Scanner</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <p>Scan the Volunteer's QR code to confirm volunteer hours associated with this opportunity.</p>

                    <input type="hidden" name="oppor_id" id="oppor_id">
                    <div class="row row-centered">
                        <div style="margin: auto; width: 50%; padding: 5px;">
                            <div id="qrCodeOrg"></div>
                        </div>
                    </div>

                    <div class="d-none" id="detailAfterScan">
                        <div class="row row-centered">
                            <div style="margin: auto; width: 50%; padding: 5px; color: green; font-weight:bold ">
                                <i class="fa fa-check" aria-hidden="true"></i> APPROVED
                            </div>
                        </div>
                        <br>
                        <div class="row row-centered">

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <img id="qr_image" src="https://via.placeholder.com/100" alt="Image Logo" class="img-thumbnail">
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3 my-auto">
                            <h6 id="qr_name">No Name</h6>
                            <span class="text-success" id="qr_hours">3 Hours</span>
                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-6"
                             style="font-size: 12px;background: #a9a9a973; border-radius: 6px">
                            <br/>
                            <b>Organization:</b> <span id="qr_org_name"></span> <br>
                            <b>Opportunity:</b> <span id="qr_opp_name"></span> <br>
                            <b>Data:</b> <span id="qr_date"></span><br>
                            <b>Time Tracked:</b> <span id="qr_time_range"></span> <br>
                            <br/>
                        </div>
                        <br>
                    </div>
                    </div>
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="button-fill button-grey float-right ml-2" data-dismiss="modal">CLOSE</button>
                    <button type="button" class="button-fill float-right qrCodeScanning" >SCAN CODE</button>
                    {{--@if(env('APP_ENV') == 'local')--}}
                        {{--<button type="button" class="btn btn-success float-right Dev2Test" >Dev2 Test</button>--}}
                    {{--@endif--}}
                </div>
            </div>
        </div>
    </div> 

    {{-- @include('components.opportunityEmailBlasts') --}}

    <div class="modal fade" id="scanningQrCodeVidModal" tabindex="-1" role="dialog" aria-labelledby="scanningQrCodeVidModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Scanning Code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <video id="previewScan"></video>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary float-left" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

    <script>  
    
$(".emailVol").click(function(){
    var id=$(this).attr("data-value");
    $.ajax({
        url: API_URL + 'get-volunteers/' + id,
        type: 'GET',
        success: function(data) {
                 $(".modal-body").empty();
                 $(".submitSendMail").attr('data-id',id);
               for(var i =0 ; i<data.details.length;i++){
                 $(".modal-body").append("<label class='volunteerMailId'><input id="+data.details[i]['id']+" type='checkbox'>"+data.details[i]['email']+"</label><br>");
               } 
            $("#myModal").modal("show");
         }
     });
});

$(".submitSendMail").click(function(){
var arr=[];
var code=$(this).attr("data-id");
    console.log(code);
$(".volunteerMailId").children().each(function(){ 
    var $this = $(this);
    if($this.is(":checked")){
        arr.push($this.attr("id"));
    }
});
   if(arr.length>0){
    $("#errorMsg").hide();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var type = "POST";
            var formData = {
                code:code,
                vol_id:arr,     
            }
            $.ajax({
                type: type,
                url: '{!!route('organization-opportunity-survey-sendEmail')!!}',
                data: formData,
                success: function (data) {
                    $("#showMsg").html(data.success);
                    $("#showMsg").show();
                    setTimeout(function(){
                        $('#myModal').modal('hide');
                    }, 2000);
                },
                error: function (data) {
                    $('.login-first').show();
                    console.log('Error:', data);
                }
            });
       }
       else{
           $("#errorMsg").show();
       }
});

//publish pop up 
$('.surveyPublish').click(function(){
             $('#publish_modal').modal('show', {backdrop: 'static'});
             $('#publish_link').attr('data-survey-id', $(this).data('id'));
        });


        $('.publishPop').click(function (e) {           
            e.preventDefault(); 
            var id  = $(this).attr('data-survey-id');          
            $('#publish_link').html('Loading....');
            var url = "{{url('organization/publish-survey-opportunity')}}/"+id ;         
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            
            var type = "GET";
            $.ajax({
                type: type,
                url: url,
                success: function (data) {
                    location.reload();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#publish_link').html('Publish');
                }
            });
            
        });

//end publish popup 

$('.remove').click(function(){
             $('#delete_modal').modal('show', {backdrop: 'static'});
             $('#delete_link').attr('data-survey-id', $(this).data('id'));
        });

        $('.removePop').click(function (e) {
            e.preventDefault();
            $('#delete_link').html('Loading....');
           
            var url = "{{route('organization-opportunity-survey-delete')}}";           
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            
            var type = "POST";
            var formData = {
                survey_id: $(this).attr('data-survey-id'),
            };
            $.ajax({
                type: type,
                url: url,
                 data: formData,
                success: function (data) {
                    if(data.message=="success"){
                        location.reload();
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#delete_link').html('Delete');
                }
            });
            
        });




$( document ).ready(function() {

      $('.qrCodeScan').click(function(){
            let opprID =  $(this).data('opportunityId');
            $('#oppor_id').val(opprID);

            $("#qrCodeOrg").html("");
            $('#qrCodeOrg').qrcode({width: 120,height: 120,text: JSON.stringify(opprID)});

        });
});


</script>
@endsection
