@extends('layout.masterForAuthUser')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

    </style>
@endsection

@section('content')
    <div class="wrapper-opportunities survey_result_blk">
        <div class="add-new-box">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-12 col-lg-12"><h2 class="h2 text-center">{{$surveyName->survey_name}}</h2></div>
                </div>
            </div>
        </div>       
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="result_list_outer">
                            <!-- First result start -->
                            <div class="result_list">
                                <div class="question_no"></div>

                                    <div class="question_name_blk">                                        
                                        @foreach($result as $key=>$list)                           
                                        <div class="question_name">

                                            {{$key+1}}. {{$list['question']}}

                                        </div>
                                        <div class="status_blk">
                                            <span>Answered:{{$list['answered']}} </span>
                                            <span>Skipped:{{$list['skipped']}}</span>
                                        </div>
                                        <div class="table_blk">
                                            <table class="table align-middle table_border">
                                                    <thead>
                                                        <tr>                                                    
                                                        <th scope="col">Answer Choices</th>
                                                        <th scope="col">Responses</th>
                                                        <th scope="col">-</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       @foreach($list['Options'] as $option)
                                                       <tr>
                                                               <td>{{$option['answerchoice']}}</td>
                                                               <td>{{$option['response']}}</td>
                                                               <td>{{$option['responsepercent']}}%</td>
                                                       </tr>
                                                        @endforeach                                                       
                                                    </tbody>  
                                                    <tfoot>
                                                        <th colspan="3">Total Respondents:{{$list['totalresponses']}}</th>
                                                    </tfoot>
                                                </table>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            
                            <!-- First result end -->

                                                      
                        </div>   
                        
                    </div>
                 </div>
                </div>


        
                
    </div>
           
    @include('components.opportunityEmailBlasts')
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    
@endsection
