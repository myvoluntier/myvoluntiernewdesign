@extends('layout.masterForAuthUser')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

    </style>
@endsection

@section('content')
    <div class="wrapper-opportunities survey_result_blk">
        <div class="add-new-box">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-12 col-lg-12"><h2>{{$name->survey_name}}</h2></div>
                </div>
            </div>
        </div>

            <div class="container">
                <div class="row">

                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="preview_list_outer">
                                <div class="survey_name_blk">   
                                <h5>Survey Name:</h5>
                                <div class="form-check form-check-inline"><input type="text" name="survey_name" id="survey_name" value="{{$name->survey_name}}"></div>
                                </div>
                            </div>
                        </div>

                    {{-- {{dd($dates)}} --}}
                  <div class="col-12 col-md-12 col-lg-12">
                        <div class="preview_list_outer">
                            <div class="checkbox-form survey_btn_blk">
                                <div class="checkbox-form" id="chck_user">  
                                    <div class="survey_name_blk">                         
                                        <div class="form-check form-check-inline title">
                                            <h5>Type of Survey:</h5>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label" for="exit_survey">
                                                Final Survey
                                            <input class="form-check-input" type="radio" name="survey_form" id="exit_survey" value="Final" {{$name->survey_type=="Final" ? 'checked' : ''}}>
                                            <span class="checkmark"></span>         
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label" for="intermediate_survey">
                                                Intermediate Survey
                                            <input class="form-check-input" type="radio" name="survey_form" id="intermediate_survey" value="Intermediate" {{$name->survey_type=="Intermediate" ? 'checked' : ''}}>
                                            <span class="checkmark"></span>    
                                            </label>
                                        </div>
                                    </div>


                                 <div class="survey_date_blk">
                                        <div class="form-check form-check-inline" id="intermediateSurvey" >
                                            <label class="form-check-label">Start date:</label>
                                            <input type="text" class="border_bot" name="start_date" id="start_date" readonly='true' value="{{$name->survey_type=="Intermediate" ? $name->survey_start : ''}}">                                                    
                                            
                                            <label class="form-check-label">End date:</label>
                                            <input type="text" class="border_bot" name="end_date" id="end_date" readonly='true' value="{{$name->survey_type=="Intermediate" ? $name->survey_end : ''}}">
                                            
                                            <input type="hidden" id="oppor_startDate" value="{{$dates->start_date}}">
                                            <input type="hidden" id="oppor_endDate" value="{{$dates->end_date}}">
                                        </div>
                                        <div class="form-check form-check-inline" id="endSurvey" >                                                
                                            <input type="hidden" name="exitStart_date" id="exitStart_date" value="{{$dates->end_date}}">
                                            
                                            <label class="form-check-label">End date:</label>
                                            <input type="text" class="border_bot" name="exitEnd_date" id="exitEnd_date" readonly='true' value="{{$name->survey_type=="Final" ? $name->survey_end : ''}}">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>



                    <div class="col-12 col-md-12 col-lg-12">
                       <!--==== Preview design start =========-->
                                <div class="preview_list_outer">
                                    <div class="btn_exit">
                                        {{-- Survey type: <a href="javascript:void(0);">{{$name->survey_type}}</a> --}}
                                    </div>
                                 <div>
                                   </div>
                                   <!-- Result start -->
                                    <div class="result_list">
                                        <div class="question_no"></div>
                                        <div class="question_answer_blk">
                                         @foreach($options as $key=>$list)                                                                                    
                                            <div id="question_name_{{$list->id}}">
                                               <?php $quesNum=$key+1 ; ?>
                                                Q{{$quesNum}}.<span id="ques"> {{$list->questions}}</span><span class="test-survey-edit btn_exit"> <a onclick="openEditForm({{$list->id}},{{$list->survey_id}},{{$quesNum}})" href="javascript:void(0)">Edit</a></span>
                                            </div>
                                          <div class="form-check form-check-inline own-form-field">
                                                @foreach($list->ques_option as $op)
                                                    <label class="form-check-label" for="option">
                                                    <input class="" type="{{$list->question_type=="single" ? 'radio' :'checkbox'}}" disabled name="option" id="option_{{$list->id}}" value="{{$op->options}}">{{$op->options}}
                                                    <span class="{{$list->question_type=="single" ? 'checkmark' : "checkbox2"}}"  ></span> 
												   </label>
                                                @endforeach
                   
                                         </div>                                                                                      
                                          @endforeach
                                        </div>
                                    </div>
									
									
									<!-- edit form start -->
								<div class="question_outer_blk edit-form" style="display:none;">                                        
                                    <div id="editQues" style="display:none;">
                                         <div class="question_blk"> 
                                                <label>Q<span id="ques_no"></span></label> 
                                                {{-- question --}}                                        
                                                <div class="question_select_blk float-label">
                                                    <input id="question" class="form-control option"  type="text">    
                                                    <select name="surveyOptions" id="ques_type" class="surveyOptions option" >                                             
                                                        <option value="single">Single Choice</option>
                                                        <option value="multiple">Multiple Choice</option>
                                                    <select> 
                                                </div>                      
                                          </div>
                                          {{-- options --}}
                                          <div class="question_option_blk">

                                          </div>
                                          <div id="hiddenField"></div>
                                            <div class="form_btn_blk preview_btn_blk">
                                                <div class="bottom_btn">
                                                    <input type="button" id="updateData" class="button-fill" value="Update">
                                                </div>
                                            </div>
                                    </div>
                                    <div id="updateQues" style="display:none;">
                                        <div class="question_blk"> 
                                            <label>Q<span id="ques_no"></span></label> 
                                            {{-- question --}}                                        
                                            <div class="question_select_blk float-label">
                                                <input id="questionAdd" class="form-control option"  type="text">    
                                                <select name="surveyOptions" id="ques_type" class="surveyOptions option" >                                             
                                                    <option value="single">Single Choice</option>
                                                    <option value="multiple">Multiple Choice</option>
                                                <select> 
                                            </div>                      
                                      </div>
                                      {{-- options --}}
                                      <div class="question_option_blk">
                                        <div class="option_field_blk option_field_1">
                                            <label class="cir"></label> 
                                            <input id="option"  class="form-control option"  type="text" >
                                            <div class="add_minus">
                                                <a href="javascript:void(0)" onclick="addRow(this)"><img src="{{asset('img/plus_icon_survey.png')}}" alt=""></a>
                                                <a href="javascript:void(0)" onclick="minusRow(this)"><img src="{{asset('img/minus_icon_survey.png')}}" alt=""></a>
                                            </div> 
                                        </div>
                                      </div>
                                      <input id="surveyID" type="hidden" value= {{$name->id}} >
                                        <div class="form_btn_blk preview_btn_blk">
                                            <div class="bottom_btn">
                                                <input type="button" id="addData" class="button-fill" value="Add">
                                            </div>
                                        </div> 
                                    </div>
                                        </div>
									<!--  edit form end -->							  
                                    <div>
                                        <button class="btn btn-success" onclick="openAddQues()">Create question</button>
                                    </div>
                                    <!-- Result end -->
                                    <div class="form_btn_blk preview_btn_blk">
                                        <div class="bottom_btn">
                                            <a id="" class="button-fill" href="{{route('organization-opportunity-survey-list',$name->opportunity_id)}}">Back to list</a>
                                        </div>
                                    </div>
                               </div>  
                            <!--======== Preview design end ========-->
                    </div>
                 </div>
                </div>           
    </div>
    {{-- @include('components.opportunityEmailBlasts') --}}
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <script  src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script  src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <script>
$( document ).ready(function() {
    $(".surveyOptions").change();
        if($('#exit_survey').is(':checked')){  
            $("#intermediateSurvey").hide();
            $("#endSurvey").show();
        }
        else{
            $("#intermediateSurvey").show();
            $("#endSurvey").hide();
        }
   
    $('input[type=radio]').click(function(){
    var survey_type= $('input[type=radio][name=survey_form]:checked').attr('id');
    if(survey_type=="intermediate_survey"){
        $("#intermediateSurvey").show();
        $("#endSurvey").hide();
    }else{
        $("#intermediateSurvey").hide(); 
        $("#endSurvey").show();
    }
    });

     //date picker //
     var minDate = moment($("#oppor_startDate").val()).format('YYYY-MM-DD');
     var maxDate = moment($("#oppor_endDate").val()).format('YYYY-MM-DD');
    $("#start_date").datepicker({
      showAnim: 'drop',
      numberOfMonth:1,
      minDate:minDate,
      maxDate:maxDate,
      dateFormat:'yy-mm-dd',
    });

    $("#end_date").datepicker({
      showAnim: 'drop',
      numberOfMonth:1,
      minDate:minDate,
      maxDate:maxDate,
      dateFormat:'yy-mm-dd',
    });

    $("#exitEnd_date").datepicker({
      showAnim: 'drop',
      numberOfMonth:1,
      minDate:maxDate,
      dateFormat:'yy-mm-dd',
    });
    //end date picker //
  }); 

    function openAddQues(){
        $(".edit-form").show();
        $("#editQues").hide();
        $("#updateQues").show();
    }

//*** add questions ***//
    $("#addData").click(function(){
        console.log('hey');
        var myArray= [];
        var obj = {};
        var flag=0;
    if($("#questionAdd").val()==""){
        $("#questionAdd").addClass('error');
    flag++;
    }else{
        $("#questionAdd").removeClass('error');
    }
    if($("#updateQues").find("#option").val()==""){
        $("#updateQues").find("#option").addClass('error');
        flag++;
    }else{
        $("#updateQues").find("#option").removeClass('error');
    }

    if($("#survey_name").val()==""){
        $("#survey_name").addClass('error');
        flag++;
    }else{
        $("#survey_name").removeClass('error');
    }
    
    //start date and end date 
    if($('input[name="survey_form"]:checked').val()=="Final"){ 
        if($("#exitEnd_date").val()==""){
            $("#exitEnd_date").addClass('error');
            flag++;
        }else{
            $("#exitEnd_date").removeClass('error');
           var survey_type="Final";
           var startDate=$("#exitStart_date").val();
           var endDate=$("#exitEnd_date").val();
        }  
   }else{
     if($("#start_date").val()=="" && $("#end_date").val()==""){
        $("#start_date").addClass('error');
        $("#end_date").addClass('error');
            flag++;
        }else{
        $("#start_date").removeClass('error');
        $("#end_date").removeClass('error');
        var survey_type="Intermediate";
        var startDate=$("#start_date").val();
        var endDate=$("#end_date").val();
        }
   }
   //start date and end date 

        var surveyId= $("#surveyID").val();
        var question= $("#questionAdd").val();
        var quesType= $("#updateQues").find(".surveyOptions").val();
        var survey_name= $("#survey_name").val();

        $.each($("#updateQues .option_field_blk #option"), function(index, value) {  
        var num = index+1;
         obj[value.id+"_"+num]=$( this ).val();
         });
         myArray.push(obj);

    if(flag==0){   
         var formData={
            survey_id:surveyId,
            question:question,
            ques_type:quesType,
            option :myArray,
            surName:survey_name,
            surveyType:survey_type,
            surveyStartDate:startDate,
            surveyEndDate:endDate,
          }
         var url = API_URL + 'opportunity/add_survey_question';
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
         var type = "post";
            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                if(data.result=="success"){
                window.location.reload();
                }
                },
                error: function (data) {
                }
            });
        }
    });
//***end add questions ***//


    function openEditForm(id ,surveyId,quesNum){
        $("#ques_no").text(quesNum+".");
        $(".surveyOptions option:selected").removeAttr('selected');
        var ques_type=$(".form-check-label #option_"+id).next().attr('class');
        if(ques_type=="checkmark"){
            var type= "cir";
            $(".surveyOptions option[value=single]").attr("selected","selected");
            }
            else{
                var type= "square";
                $(".surveyOptions option[value=multiple]").attr("selected","selected");
            }
        $("#question").val('');
        $(".question_option_blk").empty();
        $("#hiddenField").empty();
        var opt = $("#question_name_"+id).next();
        var question= $("#question_name_"+id).children("#ques").text();

        $("#question").val(question); 
        $.each(opt.children().children('input[name="option"]'), function(index, value) {   
           var num=index+1;
         $(".question_option_blk").append("<div class='option_field_blk option_field_"+num+"'><label class='"+type+"'></label><input id='option' value='"+$(this).val()+"'><div class='add_minus'><a href='javascript:void(0)' onclick='addRow(this)'><img src='{{asset('img/plus_icon_survey.png')}}' alt=''></a><a href='javascript:void(0)' onclick='minusRow(this)'><img src='{{asset('img/minus_icon_survey.png')}}' alt=''></a></div></div>");
         });
         $("#hiddenField").append("<input id='surveyId' type='hidden' value='"+surveyId+"'><input id='questionId' type='hidden' value='"+id+"'>");
        $(".edit-form").show();
        $("#editQues").show();
        $("#updateQues").hide();

     } 

//start update question //
     $("#updateData").click(function(){
        var myArray= [];
        var obj = {};
        var surveyId= $("#hiddenField #surveyId").val();
        var quesId= $("#hiddenField #questionId").val();
        var question=$("#question").val();
        var quesType= $("#ques_type").val();
        var survey_name= $("#survey_name").val();

            //start date and end date 
            if($('input[name="survey_form"]:checked').val()=="Final"){ 
                var survey_type="Final";
                var startDate=$("#exitStart_date").val();
                var endDate=$("#exitEnd_date").val();     
           }else{
                var survey_type="Intermediate";
                var startDate=$("#start_date").val();
               var endDate=$("#end_date").val();                
          }
        //start date and end date 



        $.each($("#editQues .option_field_blk #option"), function(index, value) {  
        var num = index+1;
         obj[value.id+"_"+num]=$( this ).val();
         });
         myArray.push(obj);
         
        var formData={
            survey_id:surveyId,
            ques_id: quesId,
            question:question,
            ques_type:quesType,
            quesAndoption : myArray ,
            surveyName:survey_name,
            surveyType:survey_type,
            surveyStartDate:startDate,
            surveyEndDate:endDate,
         }
         var url = API_URL + 'opportunity/update_survey_question';
         $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
         var type = "post";
            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                if(data.result=="success"){
                window.location.reload();
                }
                },
                error: function (data) {
                }
            });
     });
//end update question //

    function addRow(addField){
        var count = $(document).find(".option_field_blk").length;
            count++;
          
        if($("#updateQues").find(".surveyOptions").val()=="single"){
            var type= "cir";
        }
        if($("#editQues").find(".surveyOptions").val()=="single"){
            var type= "cir";
        }
        if($("#editQues").find(".surveyOptions").val()=="multiple"){
            var type= "square";
        }
        if($("#updateQues").find(".surveyOptions").val()=="multiple"){
            var type= "square";
        }
            var fieldHTML ='<div class="option_field_blk option_field_'+count+'"><label class="'+type+'"></label><input id="option" class="option" type="text" ><div class="add_minus"> <a href="javascript:void(0)" onclick="addRow(this)"><img src="{{asset('img/plus_icon_survey.png')}}" alt=""></a> <a href="javascript:void(0)" onclick="minusRow(this)"><img src="{{asset('img/minus_icon_survey.png')}}" alt=""></a> </div> </div>';
            $(addField).parent().parent().after(fieldHTML);
    }


    function minusRow(minusField){
        $(minusField).parent().parent().remove();
    }

    $(function(){
    $(".surveyOptions").on('change', function(){
        if($(this).val()=="single"){
                $(".option_field_blk").find('label').removeClass('square');
                $(".option_field_blk").find('label').addClass('cir');
            }
            else{
                $(".option_field_blk").find('label').removeClass('cir');
                $(".option_field_blk").find('label').addClass('square');
            }
    })
    
    });
   
     </script>
@endsection
