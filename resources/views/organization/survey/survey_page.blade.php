@extends('layout.frame')

@section('css')
    <style>
        .main-text a {
            text-decoration: none;
        }

    </style>
@endsection

@section('body')

<body>   
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5426731.js"></script>

    <div class="offcanvas-menu">
        <div class="container">
            <div class="header-center-menu">
                <ul class="nav">
                    @include('components.non-auth.buttons_registration_and_login')
                </ul>
            </div>
        </div>
    </div>

    <div class="offcanvas-contentarea">

        <div class="wrapper_bottom_footer">
            @include('components.non-auth.header')

            <div class="row-content">
                <input class="type-user" type="hidden" value="Volunteer">

                @if(request()->confirmed)
                <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                    <strong>Hi, </strong> Your account has been confirmed. Please login to continue.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

  <div class="wrapper-opportunities survey_result_blk">
        <div class="add-new-box">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-12 col-lg-12"><h2 class="h2 text-center">{{$name[0]->survey_name}}</h2></div>
                </div>
            </div>
        </div>
        @if($exist=="not exist")
            <div class="container">
                    <form method="post" action={{route('saveAnswerSurvey')}}>
                <div class="row">                
                    <div class="col-12 col-md-12 col-lg-12">
                       <!--==== Preview design start =========-->
                                <div class="preview_list_outer">
                                    <div class="btn_exit">
                                        Survey type:<a href="javascript:void(0);">{{$name[0]->survey_type}}</a>
                                    </div>
                                 <div>
                                   </div>
                                   <?php
                                    $vol_id= Request::segment(2);
                                    $ids=decrypt(Request::segment(3));
                                     $id=  explode("/",$ids);
                                    $survey_id=$id[0];
                                    // $org_id=$id[1];
                                    // $oppor_id=$id[2];
                                   ?>
                                   <!-- Result start -->
                                    <div class="result_list">
                                        <div class="question_no"></div>
                                        <div class="question_answer_blk">
                                         @foreach($options as $key=>$list)                                                                                    
                                            <div class="question_name" id="{{$list->id}}">
                                             {{$key+1}}. {{$list->questions}}
                                            </div>
                                            <input type="hidden" name="questions[]" value="{{$list->id}}">
                                          <div class="form-check form-check-inline">
                                                @foreach($list->ques_option as $op)
                                                    <label class="form-check-label" for="option">
                                                    <input type="hidden" id="vol_id"name="vol_id" value="{{$vol_id}}" >
                                                    <input type="hidden" id="survey_id" name="survey_id" value="{{$survey_id}}">
                                                  @if($list->question_type=="single")
                                                    <input class="" type="radio" name="ques_{{$list->id}}"  value="{{$op->id}}">{{$op->options}}
                                                  @else
                                                  <input class="" type="checkbox" name="ques_{{$list->id}}[]"  value="{{$op->id}}">{{$op->options}}
                                                  @endif
                                                    </label>
                                                @endforeach 
                                         </div>                                                                                      
                                         @endforeach
                                        </div>
                                    </div>
                                    <!-- Result end -->
                                    <div class="form_btn_blk preview_btn_blk">
                                        <div class="bottom_btn">
                                            <button  class="button-fill"  target="_blank">Submit</button>
                                        </div>
                                    </div>
                               </div>  
                            <!--======== Preview design end ========-->
                    </div>
                  </div>               
                </div>
            </form>           
    </div>
      @else
      <div class="container text-center">
                <br><br><br>
                <p><h4>You have already participated in survey  and we will improve the things based on your feedback.</h4></p>            
                <p><h4>Thank You!</h4><p>
                <button class="btn btn-success" id="closeBtn">Close</button>
      </div>  
      @endif   
    @include('components.opportunityEmailBlasts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$("#closeBtn").click(function(){
    window.close();
});
</script>
</body>
@endsection

@section('script')
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>
    {{----}}
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    
@endsection
