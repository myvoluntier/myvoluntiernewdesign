@extends('layout.masterForAuthUser')

@section('css')
<link rel="stylesheet" href="{{asset('front-end/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('front-end/css/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('front-end/css/bootstrap-datepicker.css')}}">
<link rel="stylesheet" href="{{asset('front-end/css/bootstrap-slider.css')}}">
<link rel="stylesheet" href="{{asset('front-end/css/main.css')}}">
<link rel="stylesheet" href="https://foliotek.github.io/Croppie/croppie.css">

<style>
    .p_invalid {
        display: none;
        font-size: 15px;
        font-weight: bold;
        margin-top: 10px;
        margin-bottom: 3px;
        color: red !important;
    }

    label.cabinet{
        display: block;
        cursor: pointer;
    }

    label.cabinet input.file{
        position: relative;
        height: 100%;
        width: auto;
        opacity: 0;
        -moz-opacity: 0;
        filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
        margin-top:-30px;
    }

    #upload-demo{
        width: 400px;
        height: 400px;
        padding-bottom:25px;
    }
    figure figcaption {
        position: absolute;
        bottom: 0;
        color: #fff;
        width: 100%;
        padding-left: 9px;
        padding-bottom: 5px;
        text-shadow: 0 0 10px #000;
    }
    .astriskCol{color:red;}
</style>

@endsection

@section('content')

<div class="wrapper-create-edit-group wrapper-opportunities">
        <form id="post_opportunity" role="form" method="post" action="{{(isset($cloneOpportunity))? url('api/organization/post_opportunity')  : url('api/organization/edit_opportunity').'/'.$oppr_info->id}}" enctype="multipart/form-data">

            {{csrf_field()}}
        <div class="add-new-box">
        <div class="container">
            <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-6 text-left"><h2 class="h2"><?php echo (isset($cloneOpportunity)) ? 'Create Opportunity' : 'Edit Opportunity'; ?></h2></div>
            <div class="col-12 col-md-6 col-lg-6"><p class="add-new-para text-right light"><?php echo (isset($cloneOpportunity)) ? 'You can create your Opportunity here' : 'You can edit your Opportunity here'; ?></p></div>
            </div>
        </div>
        </div>
    <div class="wrapper-tablist">
        <div class="container" id="opportunity_box">
            <div class="row float-label">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <div class="main-text">
                            <h3 class="h3">Opportunity Info</h3>
                        </div>
                    </div>
                    <div class="form-group floating text-center">
                    <div class="wrapper_input wrapper-file-upload">
                        <label class="label-text" id="opportunityImg">
                            @if(!empty($oppr_info->logo_img) && !isset($cloneOpportunity))
                                <img @if($oppr_info->logo_img == '') src = "{{asset('img/upload/logo.png')}}" @else src = "{{$oppr_info->logo_img}}" @endif class="img-responsive img-thumbnail" id="item-img-output" />
                            @else
                                &nbsp;
                            @endif
                            <div class="upload-img-icon"><img src="{{asset('img/upload-icon-img.png')}}" alt=""></div>
                        </label>
                            <input accept="image/*" name="file_logo" id="inputImage" class="hide" type="file">
                            <input type="hidden" name="image_content" id="image_content">
                            <a href="#"><span class="button-fill">Upload logo</span></a>
                    </div>
                    </div>
                    <div class="form-group has-float-label">
                            <input type="text" name="title" id="title" value="{{(isset($cloneOpportunity))? '' : $oppr_info->title}}" placeholder="Opportunity title">
                            <label class="label-text" for="title">Opportunity Title:<span class="astriskCol">*</span></label>
                            <span style="color:red" id="titleError"></span>
                    </div>

                    <div class="control form-group select-text no-border mul-select">
                         <div style="font-size: 14px;">Opportunity Type(s):</div> 
                            <select name="opportunity_type[]" id="opportunity_type" class="form-control custom-dropdown select2" multiple>
                                @foreach($opportunity_category as $oc)
                                <option <?php if (!empty($oppr_info->selected_cat) && in_array($oc->id, $oppr_info->selected_cat)) { ?> selected <?php } ?> value="{{$oc->id}}">{{$oc->name}}</option>
                                @endforeach
                            </select>
                    </div>

                    <div class="form-group floating">
                    <div class="label-block"><label class="label-text ">Opportunity Description:<span class="astriskCol">*</span></label></div>
                        <div class="wrapper_input">
                            <textarea name="description" class="form-control"
                                      id="description" placeholder="">{{$oppr_info->description}}</textarea>
                        </div>
                    </div>

                    <div class="control form-group select-without-text mb-30">
                            <select class="custom-dropdown" name="min_age" id="min_age">
                                <?php for($i=1;$i<=30;$i++) {?>
                                    <option {{$oppr_info->min_age == $i ? 'selected' : ''}} value="{{$i}}">{{$i}}</option>
                                <?php } ?>
                            </select>
                            <label class="label-text">Minimum Age:<span class="astriskCol">*</span></label>
                    </div>


                    <div class="form-group">
                    <div class="label-block"><label class="label-text ">Activities:</label></div>
                        <div class="wrapper_input">
                            <textarea name="activity" class="form-control" id="activity" placeholder="">{{$oppr_info->activity}}</textarea>
                        </div>
                    </div>


                    <div class="form-group">
                    <div class="label-block"><label class="label-text">Qualification:<span class="astriskCol">*</span></label></div>
                        <div class="wrapper_input">
                            <textarea name="qualification" class="form-control" id="qualification"
                                      placeholder="">{{$oppr_info->qualification}}</textarea>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">

                    <div class="form-group">
                        <div class="main-text">
                            <h3 class="h3">Address</h3>
                        </div>
                    </div>

                    <div class="form-group has-float-label">
                            <input name="street1" id="street1" type="text" value="{{$oppr_info->street_addr1}}" placeholder="street address">
                            <label class="label-text" for="street1">Street 1:<span class="astriskCol">*</span></label>
                    </div>
                    <div class="form-group has-float-label">
                            <input name="street2" id="street2" type="text" value="{{$oppr_info->street_addr2}}" placeholder="street address">
                            <label class="label-text" for="street2">Street 2:</label>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-4">
                            <div class="form-group has-float-label">
                                    <input name="city" id="city" type="text" value="{{$oppr_info->city}}" placeholder="City">
                                    <label class="label-text" for="city">City:<span class="astriskCol">*</span></label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-5">
                            <div class="control form-group select-without-text mb-30">
                                    <select name="state" id="state">
                                        @foreach($states as $key => $stat)
                                            <option value="{{$key}}" {{$oppr_info->state == $key ? 'selected' : '' }}>{{$stat}}</option>
                                        @endforeach
                                    </select>
                                    <label class="label-text">State:</label>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-3">
                            <div class="form-group has-float-label">
                                    <input name="zipcode" id="zipcode" type="text" value="{{$oppr_info->zipcode}}"
                                    placeholder="Zip code">
                                    <label class="label-text" for="zipcode">Zip Code:<span class="astriskCol">*</span></label>
                                    <p class="p_invalid error-pos" id="invalid_zipcode_alert">Invalid ID</p>
                            </div>
                        </div>

                    </div>


                    <div class="form-group floating">
                        <div class="label-block"><label class="label-text">Additional Info:</label></div>
                        <div class="wrapper_input">
                            <textarea name="add_info" id="add_info" class="form-control" placeholder="">{{$oppr_info->additional_info}}</textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="main-text">
                            <h3 class="h3">Time Info</h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group has-float-label">
                                <div class="wrapper_input fa-icons">
                                    <input name="start_date" readonly id="start_date"
                                           type="text" value="{{dateMDY($oppr_info->start_date)}}" placeholder="Start date">
                                    <label class="label-text" for="start_date">Start Date:<span class="astriskCol">*</span></label>
                                    <span class="focus-border"></span>
                                    <i id="start_date_icon" class="fa fa-calendar" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group has-float-label" id="endDateInput">   
                                <div class="wrapper_input fa-icons">
                                    <input name="end_date" readonly id="end_date" type="text"
                                           value="{{dateMDY($oppr_info->end_date)}}" placeholder="End date">
                                           <label class="label-text" for="end_date">End Date:<span class="astriskCol">*</span></label>
                                           <span class="focus-border"></span>
                                    <i id="end_date_icon" class="fa fa-calendar" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12">
                            <div class="form-group checkbox-radio-form">
                                <div class="form-check form-group round-form-chek">
                                    <label class="switch">
                                        Is Non-Expiring Opportunity ?
                                        <input type="checkbox" value="1" name="non_expiring" class="form-check-input" {{$oppr_info->non_expiring ? 'checked' : ''}} id="non_expiring">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12">
                            <div class="form-group checkbox-radio-form mb-30">
                                <div class="form-check form-group round-form-chek">
                                    <label class="switch">
                                        Allow Tracking/Scheduling
                                        <input type="checkbox" value="1" name="allow_tracking" {{$oppr_info->allow_tracking ? 'checked' : ''}} id="allow_tracking">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12">
                            <div class="form-group has-float-label">
                            <div class="wrapper_input">
                                    <input name="max_concurrent_vol" min="1" id="max_concurrent_vol" type="text" value="{{$oppr_info->max_concurrent_vol}}" placeholder="Concurrent volunteers">
                                    <label class="label-text" for="max_concurrent_vol">Maximum Concurrent Volunteers:</label>
                                    <p class="p_invalid" id="invalid_max_volunteer_alert">Invalid number</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="form-group">
                    <div class="label-block"><label id="time-range" class="label-text">
                            Time Range:<span class="astriskCol">*</span>
                        </label>
                    </div>
                        <div class="slider-range">
                            <input name="start_at" id="slider-range" type="text" value="" data-slider-min="0"
                                   data-slider-max="1440" data-slider-step="30" data-slider-value="[540,1020]">
                            <input type="hidden" name="start_at" id="start_at" value="9:00 AM" autocomplete="off">
                            <input type="hidden" name="end_at" id="end_at" value="5:00 PM" autocomplete="off">
                        </div>
                    </div>

                    
                    <div class="form-group">
                        <div class="checkbox-list" id="weekdays_chk_border">

                        <div class="form-group label-block"><label class="label-text">Select Week Days:<span class="astriskCol">*</span></label></div>
                        <div class="row">
                        <div class="col-4">
                            <div class="form-group checkbox-radio-form">
                            <div class="form-check form-group round-form-chek">
                                <label class="switch">
                                    Monday
                                    <input type="checkbox" value="Monday" class="weekdays_chk" id="mon" <?php if (strpos($oppr_info->weekdays, 'Monday') !== false) echo 'checked' ?>>
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                            </div>
                            <div class="col-4">
                            <div class="form-group checkbox-radio-form">
                            <div class="form-check form-group round-form-chek">
                            <label class="switch">
                                Tuesday
                                    <input type="checkbox" value="Tuesday" class="weekdays_chk" id="tue" <?php if (strpos($oppr_info->weekdays, 'Tuesday') !== false) echo 'checked' ?>>
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                            </div>

                            <div class="col-4">
                            <div class="form-group checkbox-radio-form">
                            <div class="form-check form-group round-form-chek">
                            <label class="switch">
                            Wednesday
                                    <input type="checkbox" value="Wednesday" class="weekdays_chk" id="wed" <?php if (strpos($oppr_info->weekdays, 'Wednesday') !== false) echo 'checked' ?>>
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                            </div>

                            <div class="col-4">
                            <div class="form-group checkbox-radio-form">
                            <div class="form-check form-group round-form-chek">
                            <label class="switch">
                            Thursday
                                    <input type="checkbox" value="Thursday" class="weekdays_chk" id="thu" <?php if (strpos($oppr_info->weekdays, 'Thursday') !== false) echo 'checked' ?>>
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                            </div>

                            <div class="col-4">
                            <div class="form-group checkbox-radio-form">
                            <div class="form-check form-group round-form-chek">
                            <label class="switch">
                            Friday
                                    <input type="checkbox" value="Friday" class="weekdays_chk" id="fri" <?php if (strpos($oppr_info->weekdays, 'Friday') !== false) echo 'checked' ?>>
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                            </div>

                            <div class="col-4">
                            <div class="form-group checkbox-radio-form">
                            <div class="form-check form-group round-form-chek">
                            <label class="switch">
                            Saturday
                                    <input type="checkbox" value="Saturday" class="weekdays_chk" id="sat" <?php if (strpos($oppr_info->weekdays, 'Saturday') !== false) echo 'checked' ?>>
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                            </div>

                            <div class="col-4">
                            <div class="form-group checkbox-radio-form">
                            <div class="form-check form-group round-form-chek">
                            <label class="switch">
                            Sunday
                                    <input type="checkbox" value="Sunday" class="weekdays_chk" id="sun" <?php if (strpos($oppr_info->weekdays, 'Sunday') !== false) echo 'checked' ?>>
                                    <span class="slider round"></span>
                                </label>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                        <input type="hidden" id="weekday_vals" name="weekday_vals" value="{{$oppr_info->weekdays}}">
                    </div>
                   
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group checkbox-radio-form auto-vol-formgrp">
                        <div class="form-check form-group round-form-chek">
                            <label class="switch">
                            Auto Accept Volunteer Request
                                <input class="form-check-input" type="checkbox" value="1" name ="auto_accept" {{$oppr_info->auto_accept == 1 ? 'checked' : ''}}   id="auto_accept">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group has-float-label private_passcode_cls"  style="display: {{is_null($oppr_info->private_passcode) ? 'none' : 'block'}}">
                        <div class="wrapper_input">
                            <input placeholder="Minimum 4 chars" type="text" minlength="4" maxlength="15" value="{{$oppr_info->private_passcode}}" name="private_passcode" id="private_passcode">
                            <label class="label-text" for="private_passcode">(Optional) Enter a secret passcode to make this a private opportunity:</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="main-text">
                    <h3 class="h3">Contact Info</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="form-group has-float-label">
                        <div class="wrapper_input">
                            <input type="text" value="{{$oppr_info->contact_name}}" placeholder="Contact name" name="contact_name" class="form-control"
                                   id="contact_name">
                            <label class="label-text" for="contact_name">Contact Name:<span class="astriskCol">*</span></label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group has-float-label">
                        <div class="wrapper_input">
                            <input type="text" value="{{$oppr_info->contact_email}}" placeholder="Contact email" name="contact_email" class="form-control"
                                   id="contact_email">
                            <label class="label-text" for="contact_email">Contact Email:<span class="astriskCol">*</span></label>
                            <p class="p_invalid" id="invalid_email_alert">Invalid email address.</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group has-float-label">
                        <div class="wrapper_input">
                            <input type="text" value="{{$oppr_info->contact_number}}" placeholder="111-111-1111" name="contact_phone" class="form-control phoneUSMask"
                                   id="contact_phone">
                            <label class="label-text" for="contact_phone">Contact Phone:<span class="astriskCol">*</span></label>
                            <p class="p_invalid" id="invalid_phone_alert">Invalid Phone number.</p>
                        </div>
                    </div>
                    <input type="hidden" id="opp_id" value="{{ (isset($cloneOpportunity)) ? '' : $oppr_info->id}}">
                </div>
            </div>


            <div class="wrapper_row_link  post-oppo-btngrp">
                <div class="wrapper-link two-link margin-top text-center">
                    <a href="{{url('/organization/opportunity')}}"
                       class="button-fill button-grey"><span>Close</span></a>
                    <a id="btn_post_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                    <a class="button-fill" id="btn_post" href="javascript:void(0)"><span><?php echo (isset($cloneOpportunity)) ? 'POST OPPORTUNITY' : 'SAVE' ?></span></a>
                    @if(!empty($members))
                        <a class="button-fill" id="export_member" href="{{url('/organization/export-members/'.$oppr_info->id) }}"><span>Export Members</span></a>
                    @endif
                </div>
            </div>
           </div>
        </form>
    </div>
</div>

<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div id="upload-demo" class="center-block"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button-fill button-grey" data-dismiss="modal">Close</button>
                <button type="button" id="cropImageBtn" class="button-fill">Crop</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="<?= asset('js/plugins/nouslider/nouislider.js') ?>"></script>
<script src="https://foliotek.github.io/Croppie/croppie.js"></script>
<script src="<?= asset('front-end/js/select2.full.js') ?>"></script>
<script src="https://cdn.ckeditor.com/4.11.3/standard-all/ckeditor.js"></script>
<script src="<?= asset('front-end/js/opportunity.js') ?>"></script>
<script>
    $('#auto_accept').on('change', function() {

        if ($(this).prop('checked')) {
            $('.private_passcode_cls').show();
        } else {
            $('.private_passcode_cls').hide();
        }
    });

    $('#non_expiring').on('click', function() {
        if ($(this).is(':checked')) {
            $('#non_expiring').val(1);
            $("#endDateInput").hide();
        }
        else {
            $('#non_expiring').val(0);
            $("#endDateInput").show();
        }

    });

    

    $('.form-control').on('click', function() {
        $(this).css("");
        $(this).parent().find('.p_invalid').hide();
    });

    $(document).ready(function() {
        $('body').on('change', '#inputImage', function() {
            readURL(this, $('.logo-box'));
        })

        $('.weekdays_chk').on('click', function() {
            var weekdays = '';
            $('.weekdays_chk').each(function() {
                if ($(this).is(':checked')) {
                    weekdays += $(this).val() + ',';
                }
            });
            if (weekdays != '') {
                weekdays = weekdays.slice(0, -1);
            }
            $('#weekday_vals').val(weekdays);

        });

//            $("select").select2({
//                theme: "bootstrap",
//                minimumResultsForSearch: -1
//            });

        $('#auto_accept').on('click', function() {
            if ($(this).is(':checked')) {
                $('#auto_accept').val(1);
            } else {
                $('#auto_accept').val(0);
            }
        });

        $('.wrapper_input.fa-icons input').datepicker({
            startDate: '{{date("m-d-Y",strtotime($oppr_info->start_date))}}',
            'format': 'mm-dd-yyyy',
            'autoclose': true,
            'orientation': 'right',
            'todayHighlight': true
        });

        var starAt = $('#start_at').val()
        var endAt = $('#end_at').val()

        var starAtNumber = toMin(starAt);
        var endAtNumber = toMin(endAt);

        var start_at = toMin($('#start_at').val());
        var end_at = toMin($('#end_at').val());
        $('#slider-range').attr('data-slider-value', '[' + starAtNumber + ',' + endAtNumber + ']')

        var slid = $("#slider-range").slider({});

        $(".slider-range .tooltip.tooltip-min").append('<span class="slider-time-1">' + $('#start_at').val() + '</span>');
        $(".slider-range .tooltip.tooltip-max").append('<span class="slider-time-2">' + $('#end_at').val() + '</span>');

        $("#slider-range").on("slide", function(slideEvt) {

            var hours1 = Math.floor(slideEvt.value[0] / 60);
            var minutes1 = slideEvt.value[0] - (hours1 * 60);

            if (hours1.length == 1)
                hours1 = '0' + hours1;
            if (minutes1.length == 1)
                minutes1 = '0' + minutes1;
            if (minutes1 == 0)
                minutes1 = '00';
            if (hours1 >= 12) {
                if (hours1 == 12) {
                    hours1 = hours1;
                    minutes1 = minutes1 + " PM";
                } else {
                    hours1 = hours1 - 12;
                    minutes1 = minutes1 + " PM";
                }
            } else {
                hours1 = hours1;
                minutes1 = minutes1 + " AM";
            }
            if (hours1 == 0) {
                hours1 = 12;
                minutes1 = minutes1;
            }

            $('.slider-range .slider-time-1').html(hours1 + ':' + minutes1);

            var hours2 = Math.floor(slideEvt.value[1] / 60);
            var minutes2 = slideEvt.value[1] - (hours2 * 60);

            if (hours2.length == 1)
                hours2 = '0' + hours2;
            if (minutes2.length == 1)
                minutes2 = '0' + minutes2;
            if (minutes2 == 0)
                minutes2 = '00';
            if (hours2 >= 12) {
                if (hours2 == 12) {
                    hours2 = hours2;
                    minutes2 = minutes2 + " PM";
                } else if (hours2 == 24) {
                    hours2 = 11;
                    minutes2 = "59 PM";
                } else {
                    hours2 = hours2 - 12;
                    minutes2 = minutes2 + " PM";
                }
            } else {
                hours2 = hours2;
                minutes2 = minutes2 + " AM";
            }

            $('.slider-range .slider-time-2').html(hours2 + ':' + minutes2);

        });
    });

    console.log(start_at, end_at);

    function toTime(current_time) {

        var mins = parseInt(current_time);

        var min = mins % 60;

        var hours = (mins - min) / 60;

        var day = 'AM';

        if (min < 10) {
            min = '0' + min.toString();
        } else {
            min = min.toString();
        }

        if (hours >= 12 && hours < 24) {
            day = 'PM';
        }

        if (hours > 12) {
            hours = hours - 12;
        }

        return hours.toString() + ':' + min.toString() +' '+ day;

    }


    function toMin(current_time) {

        var current_min;

        if (current_time.slice(-2) == 'AM') {

            if (current_time.slice(0, -5) == '12') {
                current_min = 1440;
                return current_min;

            } else {
                current_min = parseInt(current_time.slice(0, -5)) * 60;
                current_min = current_min + parseInt(current_time.slice(0, -2).slice(-2));
                return current_min;
            }

        } else {

            if (current_time.slice(0, -5) == '12') {
                current_min = 720 + parseInt(current_time.slice(0, -2).slice(-2));
                return current_min;

            } else {
                current_min = 720 + parseInt(current_time.slice(0, -5)) * 60;
                current_min = current_min + parseInt(current_time.slice(0, -2).slice(-2));
                return current_min;
            }
        }
    }


    $("#slider-range").on('change', function(values, handle) {
        var value = values['value']['newValue'];
        $('#start_at').val(toTime(value[0]));
        $('#end_at').val(toTime(value[1]));

    });

    var $uploadCrop,
            tempFilename,
            rawImg,
            imageId;

    function readURL(input, name) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
//                    name.css('background-image', 'url(' + e.target.result +  ')');
//                    console.log(e.target.result);
                $('.upload-demo').addClass('ready');
                $('#cropImagePop').modal('show');
                rawImg = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);
            // console.log()e.target.result;
        }
    }

//        function readFile(input) {
//            if (input.files && input.files[0]) {
//                var reader = new FileReader();
//                reader.onload = function (e) {
//                    $('.upload-demo').addClass('ready');
//                    $('#cropImagePop').modal('show');
//                    rawImg = e.target.result;
//                }
//                reader.readAsDataURL(input.files[0]);
//            }
//            else {
//                swal("Sorry - you're browser doesn't support the FileReader API");
//            }
//        }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 300,
            height: 300,
        },
        enforceBoundary: false,
        enableExif: true
    });
    $('#cropImagePop').on('shown.bs.modal', function() {
        // alert('Shown pop');
        $uploadCrop.croppie('bind', {
            url: rawImg
        }).then(function() {
            console.log('jQuery bind complete');
        });
    });

    $('.item-img').on('change', function() {
        imageId = $(this).data('id');
        tempFilename = $(this).val();
        $('#cancelCropBtn').data('id', imageId);
        readFile(this);
    });
    $('#cropImageBtn').on('click', function(ev) {
        $uploadCrop.croppie('result', {
            type: 'base64',
            format: 'jpeg',
            backgroundColor:'white',
            size: {width: 300, height: 300}
        }).then(function(resp) {
            $('#image_content').val(resp);
            $('#opportunityImg').html('<img src="" class="img-responsive img-thumbnail" id="item-img-output">');
            $('#item-img-output').attr('src', resp);
            $('#cropImagePop').modal('hide');
        });
    });

</script>

@endsection