@extends('layout.master')

@section('content')

    <div class="trust-factor-box terms_con_blk" style="margin-top: 0px;">
        <!-- page heading start -->
        <div class="page_head-box green_bg_blk">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <h2 class="h2 text-center">{{$content->title}}</h2>                           
                        </div>
                    </div>
                </div>
            </div>
       
        <!-- page heading end -->
        <div class="container">
            <div class="main-text">
                <!-- <h2 class="h2 text-center">{{$content->title}}</h2> -->
                <p class="text-center">
                    {!!  $content->html_body !!}
                </p>

            </div>
        </div>
    </div>

    <div class="row-footer">
        <div>
            @if(!Auth::check())
                <div class="request-a-demo mt-0">
                    <div class="container">
                    <a href="{{route('signUp')}}"><span>Create Account</span></a>
                        <!-- <a class="registration_button" href="#"><span>Create Account</span></a> -->
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection