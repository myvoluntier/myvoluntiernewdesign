@extends('layout.frame')
@section('css')
<style>
    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }
    .select2-container--below{background: yellow};
</style>
@yield('css')
@endsection
@section('body')
<body>
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5426731.js"></script>

    <div class="offcanvas-menu">
        <div class="container">
            <div class="header-center-menu">
                <ul class="nav">
                    @include('components.non-auth.buttons_registration_and_login')
                </ul>
            </div>
        </div>
    </div>

    <div class="offcanvas-contentarea">

        <div class="wrapper_bottom_footer">
            @include('components.non-auth.header')

            <div class="row-content">
                <input class="type-user" type="hidden" value="Volunteer">

                @if(request()->confirmed)
                <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                    <strong>Hi, </strong> Your account has been confirmed. Please login to continue.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

                <div class="sign-wrapper">
                    <div class="sign-container">
                        <div class="float-label">
                            <h2 class="h2">Forgot Password?</h2>
                            <h6 class="h3">We will e-mail you a link to replace your password</h6>
                            <div class="form-group checkbox-form" id="chck_user">
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label" for="volunteer">
                                        Volunteer
                                        <input class="form-check-input" type="radio" name="loginASForg" id="volunteer" value="volunteer" checked>
                                        <span class="checkmark"></span>         
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label" for="organization">
                                        Organization
                                        <input class="form-check-input" type="radio" name="loginASForg" id="organization" value="organization">
                                        <span class="checkmark"></span>    
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label" for="suborganization">
                                        Sub Organization
                                        <input class="form-check-input" type="radio" name="loginASForg" id="suborganization" value="suborganization">
                                        <span class="checkmark"></span>    
                                    </label>
                                </div>
                            </div>
                            
                            <div class="control form-group select-text mb-30 organizationDivS" style="display:none;">
                                <div class="wrapper_input newOrgWrapper">
                                    <select id="choose_org">
                                        <option value=""></option>
                                        @if(isset($allOrganizations))
                                        @foreach($allOrganizations as $val)
                                        <option value="{{$val['org_name']}}">{{$val['org_name']}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                    <label class="label-text">Choose Organization</label>
                                </div>
                                <p class="p_invalid text-left" id="forgot_password_invalid_name"></p>
                            </div>
                            
                            <div class="control form-group select-text mb-30 parentOrganizationDiv" style="display:none;">
                                <select id="parentOrgSel">
                                    <option value=""></option>
                                    @if(!empty($subOrg))
                                    @foreach($subOrg as $val)
                                    <option value="{{$val['org_name']}}">{{$val['org_name']}}</option>
                                    @endforeach
                                    @endif
                                </select>                             
                                <p class="p_invalid text-left" id="forgot_password_invalid_subOrg_name"></p>
                                <label class="label-text">Sub Organization:</label>
                            </div>
                      

                            <div class="form-group has-float-label">
                                <div class="wrapper_input input-email forgot_pass_input_email">
                                    <input id="get_password_email" type="text" placeholder="email@example.com" value="">
                                    <label class="label-text lab-email" for="get_password_email">Email</label>
                                    <p class="p_invalid text-left" id="forgot_password_invalid_email"></p>
                                </div> 
                            </div>


                            <div class="wrapper-link text-center">
                                <a id="forgot_password_loading" style="display:none;"><span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                                <a id="forgot_password" href="#" class="button-fill">Submit</a>
                            </div>
                        </div>
                    </div></div>



                @yield('content')
            </div>
            @include('components.footer')
        </div>
    </div>

    <script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('front-end/js/popper.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/check_validate.js')}}"></script>
    <script src="{{asset('js/home-action.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('front-end/js/select2.full.js')}}"></script>
    <script src="{{asset('js/inputmask/jQuery.SimpleMask.min.js')}}"></script>
    <script>
$(document).ready(function () {
    if ($("input[name='loginASForg']:checked").val() == "organization") {
        $(".forgot_pass_input_name").show();
        $(".organizationDivS").show();
        $(".parentOrganizationDiv").hide();
        $(".sub_OrganizationDiv").hide();
    } else if ($("input[name='loginASForg']:checked").val() == "suborganization") {
        $(".forgot_pass_input_name").show();
        $(".sub_OrganizationDiv").show();
        $(".parentOrganizationDiv").show();
        $(".organizationDivS").hide();
    } else {
        $(".forgot_pass_input_name").hide();
        $(".organizationDivS").hide();
        $(".parentOrganizationDiv").hide();
        $(".sub_OrganizationDiv").hide();
    }
    
     $("#choose_org").select2({
            theme: "bootstrap",
            minimumInputLength: 2
        });

     $("#parentOrgSel").select2({
            theme: "bootstrap",
            minimumInputLength: 2
        });
    

});
    </script>
    @yield('script')
</body>
@endsection