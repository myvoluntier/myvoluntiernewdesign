<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-quiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?=asset('front-end/img/myvoluntier_social_iT3_icon.ico')?>" type="image/x-icon" />
    <title>500 - Internal Server Error</title>

    <!-- Bootstrap core CSS -->
    <link href="<?=asset('css/bootstrap.min.css')?>" rel="stylesheet">

    <!-- Animation CSS -->
    <link href="<?=asset('css/animate.css')?>" rel="stylesheet">
    <link href="<?=asset('font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=asset('css/style.css')?>" rel="stylesheet">
    <style type="text/css">
        .content-panel {
            background: linear-gradient(rgba(255,255,255,0.5), rgba(255,255,255,0.5)),url('img/error_image.png') center center no-repeat;
            width: 100%;
            background-size: cover;
            background-attachment: fixed;
        }
        .error-body {
            padding: 80px 0px;
        }
        /*.error-body h1 {
            font-size: 180px;
            font-weight: 900;
            line-height: 210px;
            color: #3bb449;
        }*/
        .error-body h1 {
            font-size: 70px;
            font-weight: 900;
            line-height: 90px;
            /*background: radial-gradient(circle, rgba(59,180,74,1) 0%, rgba(128,130,129,1) 100%);*/
            /*background: linear-gradient(90deg, rgba(59,180,74,1) 0%, rgba(128,130,129,1) 100%);*/
            /*background-color: #000000;
            background-image: linear-gradient(225deg, #000000 0%, #2ea7d2 50%, #000000 100%);*/
            background-color: #08AEEA;
            background-image: linear-gradient(0deg, #08AEEA 0%, #2AF598 100%);
            /*background-image: linear-gradient( 102.1deg,  rgba(37,94,23,1) 7.7%, rgba(246,253,213,1) 93.3% );*/
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }
        .btn-gotohome {
            background: #2ea7d2;
            color: #FFFFFF;
            border-color: #2ea7d2;
            margin-top: 10px;
        }
        .btn-gotohome:hover {
            opacity: 0.9;
            color: #fff;
        }
        .text-padding {
            padding: 15px 0px;
        }
        .error-body h3 {
            font-size: 22px;
            color: #000;
        }
        .error-body p {
            font-size: 14px;
        }
        .footer {
            border-top: none;
            position: fixed;
        }
        .error-msg {
            padding: 0px 5px;
        }
        .content-panel p {
            color: #000;
            padding-top: 5px;
        }
        .show-error pre {
            padding-bottom: 50px;
            margin-top: 10px;
        }
        </style>
</head>

<body class="fixed-nav">
    <div class="content-panel">
        <div class="container">
        <div class="row">
            <div class="error-box">
                <div class="error-body text-center">
                    <a href="{{ url('/') }}">
                        <img src="{{asset('img/logo/mvlogo.png')}}" width="20%">
                    </a>
                    <h1>500</h1>
                    <h1>Oops, something went wrong</h1>
                    <p>There was an error. Please try again later. That's all we know</p>
                    <a href="{{url('/')}}" class="btn btn-gotohome btn-rounded m-b-40">Go to Homepage</a> 
                </div>
              
                <button type="button" class="btn btn-info error-msg" data-toggle="collapse" data-target="#demo" title="Click Here To Show Error">+ </button>
                <div id="demo" class="collapse show-error">
                    <pre>{{ $exception->getMessage() }}</pre>
                </div>
            </div>
        </div>
    </div>
        <div class="footer">
            <div class="pull-right">
            </div>
            <div class="copy-right">
                <strong>My Voluntier &copy; {{date("Y")}}.</strong> All Rights Reserved
            </div>
        </div>
    </div>

<!-- Mainly scripts -->
<script src="<?=asset('js/jquery-2.1.1.js')?>"></script>
<script src="<?=asset('js/bootstrap.min.js')?>"></script>
<script src="<?=asset('js/plugins/metisMenu/jquery.metisMenu.js')?>"></script>
<script src="<?=asset('js/plugins/slimscroll/jquery.slimscroll.min.js')?>"></script>

</body>
</html>
