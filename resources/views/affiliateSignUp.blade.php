@extends('layout.frame')
@section('css')
<style>
    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }  
    .select2-container--below{background: yellow};
</style>
@yield('css')
@endsection
@section('body')
<body>
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5426731.js"></script>

<div class="offcanvas-menu">
        <div class="container">
            <div class="header-center-menu">
                <ul class="nav">
                    @include('components.non-auth.buttons_registration_and_login')
                </ul>
            </div>
        </div>
</div>


<div class="offcanvas-contentarea">
$("#org_name_label").text("School Name");
    $("#org_div").show();
<div class="wrapper_bottom_footer">
    @include('components.non-auth.header')

        <div class="row-content">
        <input class="type-user" type="hidden" value="Volunteer">

        @if(request()->confirmed)
            <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                <strong>Hi, </strong> Your account has been confirmed. Please login to continue.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        @endif



   <div class="sign-wrapper">
    <div class="sign-container">
    <div class="alert alert-success register_div" role="alert" style="display: none">
                            <span id="register_success_div"></span>
    </div>
        <form class="float-label">
            <div class="group_pic_outer_blk">
                <div class="group_pic"   
                    style="background-image:url('{{$img == NULL ? asset('front-end/img/affiliateLogo.jpg') : $img}}')">
                        <span></span>
                    </div>
            </div>   
            <h2>SIGN UP</h2>
            <h6>Welcome to MyVolun<span>tier</span>!</h6>
           
        <div class="row">
        <input type="hidden" id="org_id" value="{{$id}}">

            <div class="col-md-6 col-sm-12 firstName">
                <div class="form-group has-float-label">
                    <input type="text" class="first_name" id="first_name" value="" placeholder="First name">
                    <label for="first_name">First Name<span class="required-field"></span></label>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 lastName" >
                <div class="form-group has-float-label">
                    <input type="text" id="last_name" value="" placeholder="Last name">
                    <label for="last_name">Last Name<span class="required-field"></span></label>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-12 middleName">
                <div class="form-group has-float-label">
                    <input type="text" id="middle_initial" value="" placeholder="Middle initial">
                    <label for="middle_initial">Middle Initial</label>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 birthDate">
                <div class="form-group has-float-label wrapper_input fa-icons">
                    <input type="text" class="birth_day" id="birth_day" value="" placeholder="DOB">
                    <label for="birth_day">Birthdate<span class="required-field"></span></label>
                        <span class="focus-border"></span>
                        <i class="fas fa-calendar-alt"></i>
                        <p class="p_invalid" id="v_invalid_age">Age should be greater then or equal to 13</p>
                </div>
            </div>
        </div>
        <div class="row" id="vol_contact">
            <div class="col-md-6 col-sm-12">
                <div class="form-group has-float-label">
                    <input type="text" class="phoneUSMask" id="v_contact_num" name="v_contact_num" value="" placeholder="111-111-1111">
                    <label for="v_contact_num">Contact Number<span class="required-field"></span></label>
                    <p class="p_invalid" id="v_invalid_contact_number" style="text-align: left;">Invalid Contact Number</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group has-float-label">
                    <input type="text" id="v_zipcode" name="v_zipcode" value="" placeholder="Zip code">
                    <label for="v_zipcode">Zip Code<span class="required-field"></span></label>
                    <p class="p_invalid" id="v_invalid_zipcode_alert" style="text-align: left;">Invalid Zip Code. Please enter again</p>
                    <p class="p_invalid" id="v_location_zipcode_alert" style="text-align: left;">We can't get location from this zip code!</p>
                    
                </div>
            </div>
        </div>
        <div class="form-group has-float-label" id="v_affiliate" style="display:{{$id==1887 ? 'block' : 'none' }}">
            <input type="text" id="v_org_affiliate" name="v_org_affiliate" value="" placeholder="What is your organization affiliation ?">
            <label for="v_org_affiliate">What is your organization affiliation ?</label>
        </div>
       
        <div class="form-group checkbox-form vol_gender">
            <div class="label-block"><label>Gender:</label></div>
            <div class="form-check form-check-inline">
                <label class="form-check-label" for="male">
                    Males
                <input class="form-check-input" type="radio" name="gender" id="male" value="option1" checked>
                <span class="checkmark"></span>         
                </label>
                </div>
                <div class="form-check form-check-inline">
                <label class="form-check-label" for="female">
                    Female
                <input class="form-check-input" type="radio" name="gender" id="female" value="option2">
                <span class="checkmark"></span>    
                </label>
            </div>
        </div>
           <div class="form-group has-float-label" id="vol_mail">
                <input type="email" id="v_email" name="v_email" value="" placeholder="email@example.com">
                <label for="v_email">Email<span class="required-field"></span></label>
                <p class="p_invalid" id="v_invalid_email_alert" style="text-align: left;">Invalid Email Address</p>
				<p class="p_invalid" id="v_existing_email_alert" style="text-align: left;">Email already exist!</p>
            </div>
            <div class="form-group has-float-label alterEmail2">
                <input type="email" id="v_email2" value="" placeholder="email@example.com">
                <label for="v_email2">Alternate Email 1 (optional)</label>
                <p class="p_invalid" id="v_invalid_email2_alert" style="text-align: left;">Invalid Email Address</p>
            </div>
            <div class="form-group has-float-label alterEmail3">
                <input type="email" id="v_email3" value="" placeholder="email@example.com">
                <label for="v_email3">Alternate Email 2 (optional)</label>
                <p class="p_invalid" id="v_invalid_email3_alert" style="text-align: left;">Invalid Email Address</p>
            </div>
            <div class="row vol_pass">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group has-float-label">
                    <input type="password" name="v_password" id="v_password" value="" placeholder="••••••••">
                    <label for="v_password">Password<span class="required-field"></span></label>
                    <p class="p_invalid" id="v_invalid_password" style="text-align: left;">Please enter more than 6 letters</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group has-float-label">
                    <input type="password" name="v_confirm" id="v_confirm" value="" placeholder="••••••••">
                    <label for="v_confirm">Confirm Password</label>
                    
                    </div>
                </div>
          </div>
          
        <!--Organization selection end-->
        <div class="form-group checkbox-radio-form" id="vol_checkbox">
            <div class="form-check form-group">
                <label class="form-check-label" for="verify_aga">
                    I am older than 13 years old
                <input class="form-check-input" type="checkbox" name="exampleRadios" id="verify_aga" value="0">     
               <span class="checkmark"></span>         
                </label>
                <p class="p_invalid" id="verify_age_alert" style="text-align:left;display: none;">Please verify your age</p>
                </div>
                <div class="form-check form-group">
                  <label class="form-check-label" for="v_accept_terms">
                     I accept the <a data-type="vol" class="termsAndConditions" href="#">Terms and Conditions</a>
                      <input class="form-check-input" type="checkbox" name="exampleRadios" id="v_accept_terms" value="0">
                      <span class="checkmark"></span>    
                   </label>
                   <p class="p_invalid" id="v_terms_alert" style="text-align:left; display:none;">You need to accept our terms and conditions to register</p>
                </div>
                <div class="form-check form-group">
                  <label class="form-check-label" for="p_accept_policy">
                     I accept the <a data-type="vol" class="privacyPolicy" href="#">Privacy Policy</a>
                     <input class="form-check-input" type="checkbox" name="exampleRadios"  id="p_accept_policy" value="0">
                     <span class="checkmark"></span>    
                   </label>
                   <p class="p_invalid" id="v_policy_alert" style="text-align:left; display:none;">You need accept our privacy policy to register</p>
                </div>
                </div>
                <div class="form-group checkbox-radio-form" id="org_checkbox"  style="display:none">               
         </div>

        <div class="form-group nmb">
            <div class="wrapper-link text-center">
            <a class="button-fill vol_invite_loading" style="display:none;color:#fff;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
            <a class="button-fill vol_invite" href="javascript:">Register</a>
            </div>
        </div>

        </form>
    </div>
</div>
        @yield('content')
    </div>
    @include('components.footer')
</div>
</div>



<script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-md5/2.5.0/js/md5.min.js"></script>
    <script src="{{asset('front-end/js/popper.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/check_validate.js')}}"></script>
    <script src="{{asset('js/home-action.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('front-end/js/select2.full.js')}}"></script>
    <script src="{{asset('js/inputmask/jQuery.SimpleMask.min.js')}}"></script>
    <script>

$(document).ready(function () {
$('.phoneUSMask').simpleMask({
    'mask': ['###-###-####']
});
$('.userName').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

$('.Founded_year_format').datepicker({
    autoclose: true,
    format: 'yyyy',
    minViewMode: 2
});
var $outerwidth = $('.row-header header .outer-width-box');
var $innerwidth = $('.row-header header .inner-width-box');

function checkWidth() {

    var outersize = $outerwidth.width();
    var innersize = $innerwidth.width();
    if (innersize > outersize) {
        $('body').addClass("navmobile");
    } else {
        $('body').removeClass("navmobile");
        $('body').removeClass("offcanvas-menu-show");
    }
}

checkWidth();
$(window).resize(checkWidth);
$('.offcanvas-menu-backdrop').on('click', function (e) {
    $('body').toggleClass("offcanvas-menu-show");
    e.preventDefault();
});
$('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
    $('body').toggleClass("offcanvas-menu-show");
    e.preventDefault();
});

function doResize($wrapper, $el) {

    var scale = Math.min(
        $wrapper.outerWidth() / $el.outerWidth(),
        $wrapper.outerHeight() / $el.outerHeight()
    );
    if (scale < 1) {

        $el.css({
            '-webkit-transform': 'scale(' + scale + ')',
            '-moz-transform': 'scale(' + scale + ')',
            '-ms-transform': 'scale(' + scale + ')',
            '-o-transform': 'scale(' + scale + ')',
            'transform': 'scale(' + scale + ')'
        });
    } else {
        $el.removeAttr("style");
    }
}

doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
$(window).resize(function () {
    doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
});

$('body').on('click', '.termsAndConditions', function (e) {
    e.preventDefault();
    $('#type_tab').val($(this).data('type'));
    $('#myModalSingOrgRegistration').modal('hide');
    $('#myModalSingVolRegistration').modal('hide');
    setTimeout(function () {
        $('#myModalTermsAndConditions').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});

$('body').on('click', '.privacyPolicy', function (e) {
    e.preventDefault();
    $('#type_tab').val($(this).data('type'));
    $('#myModalSingOrgRegistration').modal('hide');
    $('#myModalSingVolRegistration').modal('hide');
    $('#myModalTermsAndConditions').modal('hide');
    setTimeout(function () {
        $('#myModalprivacyPolicy').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});
$('#btn_decline').on('click', function () {
    $('#v_terms_alert').hide();
    $('#ov_terms_alert').hide();
    $('#myModalTermsAndConditions').modal('hide');
    setTimeout(function () {
        if ($('#type_tab').val() == 'org') {
            $('#myModalSingOrgRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
        else {
            $('#myModalSingVolRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
    }, 200);
});

@if(!Auth::check())
$(document).on('click', '.vol_registr', function (e) {
    e.preventDefault();
    scroll(0, 0);
    setTimeout(function () {
        var text = 'Volunteer';
        $('.type-user').val(text);
        $('#myModalSingVolRegistration').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200)
});
$(document).on('click', '.org_registr', function (e) {
    scroll(0, 0);
    setTimeout(function () {
        e.preventDefault();
        var text = 'Organization';
        $('.type-user').val(text);
        $('#myModalOrgType').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200)
});
$(document).on('click', '.inst_registr', function (e) {
    e.preventDefault();
    scroll(0, 0);
    var text = 'Organization';
    $('.type-user').val(text);
    $('#org_type').val(1);
    $('#chooseTypeOrg').click();
    setTimeout(function () {

    }, 200);
});
@endif


$("select").select2({
    theme: "bootstrap",
    minimumResultsForSearch: -1
});

$('.wrapper_input.fa-icons i').click(function () {
    $('.wrapper_input.fa-icons input').datepicker('show');
});

$('.wrapper_input.fa-icons input').datepicker({
    'format': 'mm-dd-yyyy',
    'autoclose': true,
    'orientation': 'right',
    'todayHighlight': true
});
$('body').on('click', '.gender', function () {
    if ($('.gender-radio').val() == 'male') {
        $('.gender-radio').val('female')
    }
    else {
        $('.gender-radio').val('male')
    }
});
});

    </script>
@yield('script')
</body>
@endsection
