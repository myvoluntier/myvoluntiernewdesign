<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{$title}}</title>
</head>
<body>
<div style="text-align: center">
    <p> {!! $body !!}</p>
</div>
</body>
</html>
