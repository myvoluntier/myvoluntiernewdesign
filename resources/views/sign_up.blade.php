@extends('layout.frame')
@section('css')
<style>
    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }

    
    .select2-container--below{background: yellow};
</style>
@yield('css')
@endsection
@section('body')
<body>
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5426731.js"></script>

<div class="offcanvas-menu">
        <div class="container">
            <div class="header-center-menu">
                <ul class="nav">
                    @include('components.non-auth.buttons_registration_and_login')
                </ul>
            </div>
        </div>
</div>


<div class="offcanvas-contentarea">
$("#org_name_label").text("School Name");
    $("#org_div").show();
<div class="wrapper_bottom_footer">
    @include('components.non-auth.header')

        <div class="row-content">
        <input class="type-user" type="hidden" value="Volunteer">

        @if(request()->confirmed)
            <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                <strong>Hi, </strong> Your account has been confirmed. Please login to continue.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        @endif



   <div class="sign-wrapper">
    <div class="sign-container">
    <div class="alert alert-success register_div" role="alert" style="display: none">
                            <span id="register_success_div"></span>
    </div>
        <form class="float-label">
            <h2>SIGN UP</h2>
            <h6>Welcome to MyVolun<span>tier</span>!</h6>
        <input type="hidden" name="user_type_val" id="user_type_val" value="{{$user_type}}">
        <div class="form-group checkbox-form" id="chck_user">
            <div class="form-check form-check-inline">
                <label class="form-check-label" for="volunteer">
                    Volunteer
                <input class="form-check-input" type="radio" name="loginAS" id="volunteer" value="volunteer" checked>
                <span class="checkmark"></span>         
                </label>
                </div>
                <div class="form-check form-check-inline">
                <label class="form-check-label" for="organization">
                    Organization
                <input class="form-check-input" type="radio" name="loginAS" id="organization" value="organization">
                <span class="checkmark"></span>    
                </label>
            </div>
        </div>



        <div class="row">
                  <!--Organization selection start-->
        <div class="col-md-12 col-sm-12" id="organizationType_div">
            <div class="control form-group select-text mb-30 organizationType" style="display:none;">
            <div class="wrapper_input">
                    <select name="org_type" id="org_type" class="custom-dropdown">
                    <option value=""></option>
                        @foreach($org_type_names as $org_name)
                            <option value="{{$org_name->id}}">{{$org_name->organization_type}}</option>
                        @endforeach
                    </select>
                    <label class="label-text" >Please Select Organization Type:<span class="required-field"></span></label>
             </div>
             </div>
        </div>
            <div class="col-md-6 col-sm-12 firstName" style="display:none">
                <div class="form-group has-float-label">
                    <input type="text" class="first_name" id="first_name" value="" placeholder="First name">
                    <label for="first_name">First Name<span class="required-field"></span></label>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 lastName"  style="display:none">
                <div class="form-group has-float-label">
                    <input type="text" id="last_name" value="" placeholder="Last name">
                    <label for="last_name">Last Name<span class="required-field"></span></label>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-12 middleName" style="display:none">
                <div class="form-group has-float-label">
                    <input type="text" id="middle_initial" value="" placeholder="Middle initial">
                    <label for="middle_initial">Middle Initial</label>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 birthDate" style="display:none">
                <div class="form-group has-float-label wrapper_input fa-icons">
                    <input type="text" class="birth_day" id="birth_day" value="" placeholder="DOB">
                    <label for="birth_day">Birthdate<span class="required-field"></span></label>
                        <span class="focus-border"></span>
                        <i class="fas fa-calendar-alt"></i>
                        <p class="p_invalid" id="v_invalid_age">Age should be greater then or equal to 13</p>
                </div>
            </div>
        </div>
     
        <div class="form-group has-float-label" id="org_div" style="display:none">
            <div class="wrapper_input">
                <input name="org_name" id="org_name" class="name-panel" type="text" value="" placeholder="Organization name">
                <label class="label-text org_name_label" for="org_name" id="org_name_label"></label>
            </div>
        </div>

        <div class="form-group has-float-label org_type_div" style="display:none">
            <div class="wrapper_input">
                <input name="non_org_type" id="non_org_type" class="name-panel" type="text" value="other" placeholder="Organization type">
                <label class="label-text" for="non_org_type">Organization Type:</label>
            </div>
        </div>


        <div class="row">
        <div class="col-md-12 col-sm-12 schoolType" style="display:none">
              <div class="control form-group floating select-text">
                    <div class="wrapper_select">
                    <?php if(isset($school_type)){ ?>
                                <select id="school_type" name="school_type">
                                <option value=""></option>
                                    @foreach($school_type as $s)
                                        <option value="{{$s->id}}">{{$s->school_type}}</option>
                                    @endforeach
                                </select>
                    <?php } ?>
                    <label class="label-text">School Type</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group has-float-label ein_div" style="display: none;">
            <div class="wrapper_input">
                <input type="text" name="org_ein" id="org_ein" class="name-panel" placeholder="EIN">
                <label class="label-text" for="org_ein">EIN: </label>
            </div>
        </div>

        <div class="row">
        <div class="col-md-12 col-sm-12 foundYear" style="display:none">
                <div class="form-group has-float-label">
                <div class="wrapper_input fa-icons">
                        <input name="found_day"  id="found_day" type="text" value="" class="Founded_year_format" placeholder="Founded year">
                        <label for="found_day">Year Founded<span class="required-field"></span></label>
                        <span class="focus-border input-group-addon"></span>
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                    </div>
                </div>
        </div>
        </div>
        <div class="row" id="vol_contact"  style="display:none">
            <div class="col-md-6 col-sm-12">
                <div class="form-group has-float-label">
                    <input type="text" class="phoneUSMask" id="v_contact_num" name="v_contact_num" value="" placeholder="111-111-1111">
                    <label for="v_contact_num">Contact Number<span class="required-field"></span></label>
                    <p class="p_invalid" id="v_invalid_contact_number" style="text-align: left;">Invalid Contact Number</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group has-float-label">
                    <input type="text" id="v_zipcode" name="v_zipcode" value="" placeholder="Zip code">
                    <label for="v_zipcode">Zip Code<span class="required-field"></span></label>
                    <p class="p_invalid" id="v_invalid_zipcode_alert" style="text-align: left;">Invalid Zip Code. Please enter again</p>
                    <p class="p_invalid" id="v_location_zipcode_alert" style="text-align: left;">We can't get location from this zip code!</p>
                    
                </div>
            </div>
        </div>
        <div class="row" id="org_contact"  style="display:none">
            <div class="col-md-6 col-sm-12">
                <div class="form-group has-float-label">
                    <input type="text" class="phoneUSMask" id="o_contact_num" name="o_contact_num" value="" placeholder="111-111-1111">
                    <label for="o_contact_num">Contact Number<span class="required-field"></span></label>
                    <p class="p_invalid" id="o_invalid_contact_number" style="text-align: left;">Invalid Contact Number</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group has-float-label">
                    <input type="text" id="o_zipcode" name="o_zipcode" value="" placeholder="Zip code">
                    <label for="o_zipcode">Zip Code<span class="required-field"></span></label>
                    <p class="p_invalid" id="o_invalid_zipcode_alert" style="text-align: left;">Invalid Zip code. Please enter again</p>
                    <p class="p_invalid" id="o_location_zipcode_alert" style="text-align: left;">We can't get location from this zip code!</p>
                </div>
            </div>
        </div>
        <div class="form-group checkbox-form vol_gender" style="display:none">
            <div class="label-block"><label>Gender:</label></div>
            <div class="form-check form-check-inline">
                <label class="form-check-label" for="male">
                    Male
                <input class="form-check-input" type="radio" name="gender" id="male" value="option1" checked>
                <span class="checkmark"></span>         
                </label>
                </div>
                <div class="form-check form-check-inline">
                <label class="form-check-label" for="female">
                    Female
                <input class="form-check-input" type="radio" name="gender" id="female" value="option2">
                <span class="checkmark"></span>    
                </label>
            </div>
        </div>


           <div class="form-group has-float-label" id="vol_mail" style="display:none">
                <input type="email" id="v_email" name="v_email" value="" placeholder="email@example.com">
                <label for="v_email">Email<span class="required-field"></span></label>
                <p class="p_invalid" id="v_invalid_email_alert" style="text-align: left;">Invalid Email Address</p>
				<p class="p_invalid" id="v_existing_email_alert" style="text-align: left;">Email already exist!</p>

            </div>
            <div class="form-group has-float-label" id="org_mail" style="display:none">
                <input type="email" id="o_email" name="o_email" value="" placeholder="email@example.com">
                <label for="o_email">Email<span class="required-field"></span></label>
                <p class="p_invalid" id="o_invalid_email_alert" style="text-align: left;">Invalid Email Address.</p>
                <p class="p_invalid" id="o_existing_email_alert" style="text-align: left;">Existing Email Address</p>

            </div>
            <div class="form-group has-float-label alterEmail2" style="display:none">
                <input type="email" id="v_email2" value="" placeholder="email@example.com">
                <label for="v_email2">Alternate Email 1 (optional)</label>
                <p class="p_invalid" id="v_invalid_email2_alert" style="text-align: left;">Invalid Email Address</p>
            </div>
            <div class="form-group has-float-label alterEmail3" style="display:none">
                <input type="email" id="v_email3" value="" placeholder="email@example.com">
                <label for="v_email3">Alternate Email 2 (optional)</label>
                <p class="p_invalid" id="v_invalid_email3_alert" style="text-align: left;">Invalid Email Address</p>
            </div>
            <div class="row vol_pass">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group has-float-label">
                    <input type="password" name="v_password" id="v_password" value="" placeholder="••••••••">
                    <label for="v_password">Password<span class="required-field"></span></label>
                    <p class="p_invalid" id="v_invalid_password" style="text-align: left;">Please enter more than 6 letters</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group has-float-label">
                    <input type="password" name="v_confirm" id="v_confirm" value="" placeholder="••••••••">
                    <label for="v_confirm">Confirm Password</label>
                    
                    </div>
                </div>
          </div>

        <div class="row org_pass">
            <div class="col-md-6 col-sm-12">
                <div class="form-group has-float-label">
                <input type="password" name="o_password" id="o_password" value="" placeholder="••••••••">
                <label for="o_password">Password<span class="required-field"></span></label>
                <p class="p_invalid" id="o_invalid_password" style="text-align: left;">Enter more than 6 letters</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group has-float-label">
                <input type="password" name="o_confirm" id="o_confirm" value="" placeholder="••••••••">
                <label for="o_confirm">Confirm Password</label>
                </div>
            </div>
        </div>
          
        <!--Organization selection end-->
        <div class="form-group checkbox-radio-form" id="vol_checkbox" style="display:none">
            <div class="form-check form-group">
                <label class="form-check-label" for="verify_aga">
                    I am older than 13 years old
                <input class="form-check-input" type="checkbox" name="exampleRadios" id="verify_aga" value="0">     
               <span class="checkmark"></span>         
                </label>
                <p class="p_invalid" id="verify_age_alert" style="text-align:left;display: none;">Please verify your age</p>
                </div>
                <div class="form-check form-group">
                  <label class="form-check-label" for="v_accept_terms">
                     I accept the <a data-type="vol" class="termsAndConditions" href="#">Terms and Conditions</a>
                      <input class="form-check-input" type="checkbox" name="exampleRadios" id="v_accept_terms" value="0">
                      <span class="checkmark"></span>    
                   </label>
                   <p class="p_invalid" id="v_terms_alert" style="text-align:left; display:none;">You need to accept our terms and conditions to register</p>
                </div>
                <div class="form-check form-group">
                  <label class="form-check-label" for="p_accept_policy">
                     I accept the <a data-type="vol" class="privacyPolicy" href="#">Privacy Policy</a>
                     <input class="form-check-input" type="checkbox" name="exampleRadios"  id="p_accept_policy" value="0">
                     <span class="checkmark"></span>    
                   </label>
                   <p class="p_invalid" id="v_policy_alert" style="text-align:left; display:none;">You need accept our privacy policy to register</p>
                </div>
                </div>
                <div class="form-group checkbox-radio-form" id="org_checkbox"  style="display:none">
                <div class="form-check form-group">
                   <label class="form-check-label" for="o_accept_terms">
                    I accept the <a data-type="org" class="termsAndConditions" href="#">Terms and Conditions</a>
                    <input class="form-check-input terms-conditions-org" type="checkbox" name="exampleRadios" id="o_accept_terms" value="option4">                <span class="checkmark"></span>    
                   </label>
                   <p class="p_invalid" id="ov_terms_alert" style="text-align:left; display:none;">You need accept our terms and conditions to register</p>
                </div>
                <div class="form-check form-group">
                  <label class="form-check-label" for="op_accept_terms">
                     I accept the <a data-type="org" class="privacyPolicy" href="#">Privacy Policy</a>
                     <input class="form-check-input" type="checkbox" name="exampleRadios" id="op_accept_terms" value="0">
                     <span class="checkmark"></span>    
                   </label>
                   <p class="p_invalid" id="ov_policy_alert" style="text-align:left; display:none;">You need accept our privacy policy to register</p>
                </div>
         </div>

        <div class="form-group nmb">
            <div class="wrapper-link text-center">
            <a class="button-fill btn_regs_loading" style="display:none;color:#fff;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
            <a class="button-fill btn_regs" href="javascript:">Register</a>
            </div>
        </div>

        </form>
    </div>
</div>
        @yield('content')
    </div>
    @include('components.footer')
</div>
</div>

    @include('components.non-auth.modal_terms_and_conditions')
    @include('components.non-auth.modal_privacy_policy')

<script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('front-end/js/popper.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/check_validate.js')}}"></script>
    <script src="{{asset('js/home-action.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('front-end/js/select2.full.js')}}"></script>
    <script src="{{asset('js/inputmask/jQuery.SimpleMask.min.js')}}"></script>
    <script>
$('select#org_type').on('change', function() {
   if(this.value==1){
    $("#org_name_label").text("School Name").addClass('required-field');
    $("#org_div").show();
    $(".org_type_div").hide();
    $(".schoolType").show();
    $(".ein_div").hide();
   
   } 
   else if(this.value==2){
    $("#org_name_label").text("Organization Name").addClass('required-field');
    $("#org_div").show();
    $(".org_type_div").hide();
    $(".schoolType").hide();
    $(".ein_div").show();
   }
   else if(this.value==3){
    $("#org_name_label").text("School Name").addClass('required-field');
    $("#org_div").show();
    $(".org_type_div").hide();
    $(".schoolType").hide();
     $(".ein_div").hide();
   }
   else if(this.value==4){
    $("#org_name_label").text("School Name").addClass('required-field');
    $("#org_div").show();
    $(".org_type_div").show();
    $(".schoolType").hide();
    $(".ein_div").hide();
   }
});
$("#organization").click(function() {
        $(".organizationType").show();
        $(".org_pass").show();
        $(".vol_pass").hide();  
        $(".firstName").hide();
        $(".lastName").hide();
        $(".middleName").hide();
        $(".vol_gender").hide();
        $(".birthDate").hide();
        $(".alterEmail2").hide();
        $(".alterEmail3").hide();
        $(".foundYear").show();
        // $(".schoolType").show();
        // $("#org_div").show(); 
        $("#org_contact").show();   
        $("#vol_contact").hide(); 
        $("#vol_mail").hide();
        $("#org_mail").show();
        $("#vol_checkbox").hide();
        $("#org_checkbox").show();
        
});
$("#volunteer").click(function() {
        $(".organizationType").hide();
        $(".vol_pass").show();
        $(".org_pass").hide();
        $(".firstName").show();
        $(".lastName").show();
        $(".middleName").show();
        $(".vol_gender").show();
        $(".birthDate").show();
        $(".alterEmail2").show();
        $(".alterEmail3").show();
        $(".schoolType").hide();
        $(".foundYear").hide();
        $("#org_div").hide();
        $("#org_contact").hide();
        $("#vol_contact").show();
        $("#vol_mail").show();
        $("#org_mail").hide();
        $("#vol_checkbox").show();
        $("#org_checkbox").hide();
    });

$(document).ready(function () {
    if($("#user_type_val").val()=="volunteer"){
        $('#volunteer').prop('checked', true);  
        $("#chck_user").hide();
    }
    else if($("#user_type_val").val()=="organization"){
        $('#organization').prop('checked', true);  
        $("#chck_user").hide();
    }
    else if($("#user_type_val").val()=="institution"){
        $('#organization').prop('checked', true);
        $("#organizationType_div").hide();
        $("#org_type").val(1).find("option[value=" + 1 +"]").attr('selected', true);
        $("#chck_user").hide();
    }
if ($("input[name='loginAS']:checked").val()=="organization") {
    $(".organizationType").show();
    $(".org_pass").show();
    $(".vol_pass").hide();
    $(".firstName").hide();
    $(".lastName").hide();
    $(".middleName").hide();
    $(".vol_gender").hide();
    $(".birthDate").hide();
    $(".alterEmail2").hide();
    $(".alterEmail3").hide();
    $(".foundYear").show();
    $("#org_contact").show();
    $("#vol_contact").hide();
    $("#vol_mail").hide();
    $("#org_mail").show();
    $("#vol_checkbox").hide();
    $("#org_checkbox").show();

    if($("select#org_type").val()==1)
    {  
        $("#org_name_label").text("School Name").addClass('required-field');
        $("#org_div").show();
        $(".org_type_div").hide();
        $(".schoolType").show();
        $(".ein_div").hide();
    }
    else if($("select#org_type").val()==2){ 
        $("#org_name_label").text("Organization Name").addClass('required-field');
        $("#org_div").show();
        $(".org_type_div").hide();
        $(".schoolType").hide();
        $(".ein_div").show();
    }
    else if($("select#org_type").val()==3){
        $("#org_name_label").text("School Name").addClass('required-field');
        $("#org_div").show();
        $(".org_type_div").hide();
        $(".schoolType").hide();
        $(".ein_div").hide();
    }
    else if($("select#org_type").val()==4){

        $("#org_name_label").text("School Name").addClass('required-field');
        $("#org_div").show();
        $(".org_type_div").show();
        $(".schoolType").hide();
        $(".ein_div").hide();
    }
}else{

    $(".organizationType").hide();
    $(".vol_pass").show();
    $(".org_pass").hide();
    $(".firstName").show();
    $(".lastName").show();
    $(".middleName").show();
    $(".vol_gender").show();
    $(".birthDate").show();
    $(".alterEmail2").show();
    $(".alterEmail3").show();
    $(".schoolType").hide();
    $(".foundYear").hide();
    $("#org_div").hide();
    $("#org_contact").hide();
    $("#vol_contact").show();
    $("#vol_mail").show();
    $("#org_mail").hide();
    $("#vol_checkbox").show();
    $("#org_checkbox").hide();
}


$('.phoneUSMask').simpleMask({
    'mask': ['###-###-####']
});
$('.userName').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

$('.Founded_year_format').datepicker({
    autoclose: true,
    format: 'yyyy',
    minViewMode: 2
});
var $outerwidth = $('.row-header header .outer-width-box');
var $innerwidth = $('.row-header header .inner-width-box');

function checkWidth() {

    var outersize = $outerwidth.width();
    var innersize = $innerwidth.width();
    if (innersize > outersize) {
        $('body').addClass("navmobile");
    } else {
        $('body').removeClass("navmobile");
        $('body').removeClass("offcanvas-menu-show");
    }
}

checkWidth();
$(window).resize(checkWidth);
$('.offcanvas-menu-backdrop').on('click', function (e) {
    $('body').toggleClass("offcanvas-menu-show");
    e.preventDefault();
});
$('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
    $('body').toggleClass("offcanvas-menu-show");
    e.preventDefault();
});

function doResize($wrapper, $el) {

    var scale = Math.min(
        $wrapper.outerWidth() / $el.outerWidth(),
        $wrapper.outerHeight() / $el.outerHeight()
    );
    if (scale < 1) {

        $el.css({
            '-webkit-transform': 'scale(' + scale + ')',
            '-moz-transform': 'scale(' + scale + ')',
            '-ms-transform': 'scale(' + scale + ')',
            '-o-transform': 'scale(' + scale + ')',
            'transform': 'scale(' + scale + ')'
        });
    } else {
        $el.removeAttr("style");
    }
}

doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
$(window).resize(function () {
    doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
});
$('body').on('click', '.close_open', function () {
    $('#myModalSuccessfulReg').modal('hide');
});
$('body').on('change', '.type-user-select', function () {
    $('.type-user').val($('.type-user-select').val())
});
$('body').on('click', '.login_button_class', function () {
    $('.close').click();
    setTimeout(function () {
        $('#myModalLogin').modal('show');
        $('#login_user').val('');
        setTimeout(function () {
            $('body').addClass('modal-open');
            $('body').removeClass('offcanvas-menu-show')

        }, 500);
    }, 200);
});
$('body').on('click', '.termsAndConditions', function (e) {
    e.preventDefault();
    $('#type_tab').val($(this).data('type'));
    $('#myModalSingOrgRegistration').modal('hide');
    $('#myModalSingVolRegistration').modal('hide');
    setTimeout(function () {
        $('#myModalTermsAndConditions').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});

$('body').on('click', '.privacyPolicy', function (e) {
    e.preventDefault();
    $('#type_tab').val($(this).data('type'));
    $('#myModalSingOrgRegistration').modal('hide');
    $('#myModalSingVolRegistration').modal('hide');
    $('#myModalTermsAndConditions').modal('hide');
    setTimeout(function () {
        $('#myModalprivacyPolicy').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});

$('#btn_agree').on('click', function () {
    $('#o_accept_terms').prop('checked', true);
    $('#v_accept_terms').prop('checked', true);
    $('#v_terms_alert').hide();
    $('#ov_terms_alert').hide();
    $('#myModalTermsAndConditions').modal('hide');
});
$('#btn_decline').on('click', function () {
    $('#v_terms_alert').hide();
    $('#ov_terms_alert').hide();
    $('#myModalTermsAndConditions').modal('hide');
});
$('#btn_agree1').on('click', function () {
    $('#op_accept_terms').prop('checked', true);
    $('#p_accept_policy').prop('checked', true);
    $('#ov_policy_alert').hide();
    $('#v_policy_alert').hide();
    $('#myModalprivacyPolicy').modal('hide');
});
$('#btn_decline1').on('click', function () {
    $('#ov_policy_alert').hide();
    $('#v_policy_alert').hide();
    $('#myModalprivacyPolicy').modal('hide');
});
@if(!Auth::check())
$(document).on('click', '.vol_registr', function (e) {
    e.preventDefault();
    scroll(0, 0);
    setTimeout(function () {
        var text = 'Volunteer';
        $('.type-user').val(text);
        $('#myModalSingVolRegistration').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200)
});
$(document).on('click', '.org_registr', function (e) {
    scroll(0, 0);
    setTimeout(function () {
        e.preventDefault();
        var text = 'Organization';
        $('.type-user').val(text);
        $('#myModalOrgType').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200)
});
$(document).on('click', '.inst_registr', function (e) {
    e.preventDefault();
    scroll(0, 0);
    var text = 'Organization';
    $('.type-user').val(text);
    $('#org_type').val(1);
    $('#chooseTypeOrg').click();
    setTimeout(function () {

    }, 200);
});
@endif


$('body').on('click', '#chooseTypeOrg', function (e) {
    e.preventDefault();
    var typeOrg = $('#org_type').val();
    if (typeOrg == 1) {

        $('.ein_div').hide();
        $('.org_name_label').text('School Name:')
        $('.org_type_div').hide()
        $('.non-org-type').hide();
        $('.school_type').show();
    }

    else if (typeOrg == 2) {
        $('.org_type_div').hide();
        $('.ein_div').show();
        if($('#type-user-select-id').val() == 'SubOrganization'){
            $('.org_name_label').text('Sub Organization Name:');
        }else{
            $('.org_name_label').text('Organization Name:');
        }
        $('.school_type').hide();
        $('.non-org-type').show();
    }

    else if (typeOrg == 3) {
        $('.org_type_div').hide();
        $('.ein_div').hide();
        $('.school_type').hide();
        $('.non-org-type').hide();
    }

    else {
        $('.org_type_div').show();
        $('.ein_div').hide();
        $('.school_type').hide();
        $('.non-org-type').hide();
    }

    $('#myModalOrgType').modal('hide');
    setTimeout(function () {
        $('#myModalSingOrgRegistration').modal('show');
        $('body').removeClass('offcanvas-menu-show')
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});
$('body').on('click', '#login_button', function () {
    $('.close').click();
    $('#myModalLogin').modal('show');
    $('body').removeClass('offcanvas-menu-show');
    setTimeout(function () {
        $('body').addClass('modal-open');
    }, 500);
});


$('body').on('click', '#forPass', function (e) {
    e.preventDefault();
    $('#myModalLogin').modal('hide');
    setTimeout(function () {
        $('#myModalForgotPassword').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});
$('body').on('click', '.registration_button', function () {
    $('#myModalUserTipe').modal('show');
    $('body').removeClass('offcanvas-menu-show');
    setTimeout(function () {
        $('body').addClass('modal-open');
    }, 500);
});

$('body').on('click', '.registration_button_on_modal_form', function (e) {
    e.preventDefault();
    $('#myModalLogin').modal('hide');
    setTimeout(function () {
        $('#myModalUserTipe').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});
$("select").select2({
    theme: "bootstrap",
    minimumResultsForSearch: -1
});
$('body').on('click', '#chooseType', function (e) {
    e.preventDefault();
   
    $('#orgSelectError').css('display','none');
    var typeUser = $('.type-user').val();
    if (typeUser === 'Volunteer') {
        $('#myModalUserTipe').modal('hide');
        setTimeout(function () {
            $('#myModalSingVolRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show')
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }, 200);
    }
    else if (typeUser === 'Organization') {

        $('#myModalUserTipe').modal('hide');
        setTimeout(function () {
            $('#myModalOrgType').modal('show');
            $('body').removeClass('offcanvas-menu-show')
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
            // $('#myModalSingOrgRegistration').modal('show');
        }, 200);
    }else if (typeUser === 'SubOrganization') {
        $('#myModalUserTipe').modal('hide');
            setTimeout(function () {
                $('#myModalOrgType').modal('show');
                $('body').removeClass('offcanvas-menu-show')
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
                // $('#myModalSingOrgRegistration').modal('show');
            }, 200);
        
    }
});
$('body').on('click', '.previous', function () {
    $('#myModalSingVolRegistration').modal('hide');
    $('#myModalSingOrgRegistration').modal('hide');
    setTimeout(function () {
        if ($('.type-user').val() == 'Volunteer') {
            $('#myModalUserTipe').modal('show');
            $('body').removeClass('offcanvas-menu-show')
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
        else {
            $('#myModalOrgType').modal('show');
            $('body').removeClass('offcanvas-menu-show')
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
    }, 200);
});
$('.wrapper_input.fa-icons i').click(function () {
    $('.wrapper_input.fa-icons input').datepicker('show');
});

$('.wrapper_input.fa-icons input').datepicker({
    'format': 'mm-dd-yyyy',
    'autoclose': true,
    'orientation': 'right',
    'todayHighlight': true
});
$('body').on('click', '.gender', function () {
    if ($('.gender-radio').val() == 'male') {
        $('.gender-radio').val('female')
    }
    else {
        $('.gender-radio').val('male')
    }
});
$('.terms-conditions-vol').click(function () {

    if ($('.terms-conditions-vol-value').val() == 0) {
        $('.terms-conditions-vol-value').val(1)
    } else {
        $('.terms-conditions-vol-value').val(0)
    }
});
$('.years-old-check').click(function () {
    if ($('.years-old-check-value').val() == 0) {
        $('.years-old-check-value').val(1)
    } else {
        $('.years-old-check-value').val(0)

    }
});
$('.terms-conditions-org').click(function () {
    if ($('.terms-conditions-org-value').val() == 0) {
        $('.terms-conditions-org-value').val(1);
    } else {
        $('.terms-conditions-org-value').val(0);
    }
});
$("#subOrgSel").hide();

$("#organizationSelect").select2({
    theme: "bootstrap",
    minimumInputLength: 2
});

var parentOrgSel = $('#parentOrgSel');
var subOrgSel = $("#subOrgSel");

parentOrgSel.select2({theme: "bootstrap"}).on('change', function () {
    if (parentOrgSel.val()) {
        $('#parent_user_id').val(parentOrgSel.val());
        $("#subOrgSel").show();
        $.ajax({
            url: API_URL + 'get-sub-organization/' + parentOrgSel.val(),
            type: 'GET',
            success: function (data) {
                subOrgSel.empty();
                subOrgSel.append($("<option></option>").attr("value", "").text("Select"));
                $.each(data, function (value, key) {
                    subOrgSel.append($("<option></option>").attr("value", value).text(key)); // name refers to the objects value when you do you ->lists('name', 'id') in laravel
                });
                subOrgSel.select2({
                    theme: "bootstrap",
                    dropdownAutoWidth: true,
                    width: 'auto'
                }).on('change', function () {
                    $('#login_user').val(subOrgSel.val());
                    $('.loginEmail').hide();
                    $('.loginPass').show();
                }); //reload the list and select the first option
            }
        });
    }
}).trigger('change');

$('.loginSub, .parentOrganizationDiv, .organizationDivS').hide();

$('#loginAS').on('change', function () {
    $('#login_user,#login_password,#parent_user_id').val('');

    $('#login_user,#login_password,#parent_user_id').val('');

    $('.organizationDivS').hide();

    if ($(this).val() == "") {
        $('.loginSub, .parentOrganizationDiv').hide();
    } else if ($(this).val() == "suborganization") {
        $('.parentOrganizationDiv').show();
        $('.loginSub').hide();
        $('.loginPass').show();

    } else {
        $('.parentOrganizationDiv').hide();
        $('.loginSub').show();
        $(".select2-container--bootstrap .select2-selection--single", "#myModalLogin").css("border", "");

        if ($(this).val() == "organization")
            $('.organizationDivS').show();
    }
});

$('.parentOrgRego').hide();
$('#type-user-select-id').on('change', function () {
    $('#parent_user_id').val('');
    $('.parentOrgRego').hide();
    if ($(this).val() == "SubOrganization") {
        $('.parentOrgRego').show();
    } else {
        $('.parentOrgRego').hide();
        $(".select2-container--bootstrap .select2-selection--single", "#myModalUserTipe").css("border", "");
    }
});
});

    </script>
@yield('script')
</body>
@endsection