@extends('layout.masterForAuthUser')

@section('css')
<style>
    .main-text a{
        text-decoration: none;
    }
</style>
@endsection
<?php $userObj = new \App\User(); ?>
@section('content')
    <div class="dashboard_org">
    <div class="news-followed-box notifications_holder">
    <!-- page heading start -->
    <div class="page_head-box green_bg_blk">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <h2 class="h2 text-center">Notifications</h2>                            
                        </div>                        
                    </div>
                </div>
            </div>       
        <!-- page heading end -->

        <div class="container">
                <ul class="notifications-list">
                    @foreach($alert as $key => $a)
                        <li>

                            <div class="notification_outer_blk">
                                @if($a['alert_type'] == \App\Alert::ALERT_JOIN_OPPORTUNITY || 
                                    $a['alert_type'] == \App\Alert::ALERT_CREATE_PRIVATE_OPPORTUNITY || 
                                    $a['alert_type'] == \App\Alert::ALERT_FOLLOW || 
                                    $a['alert_type'] == \App\Alert::ALERT_ACCEPT || 
                                    $a['alert_type'] == \App\Alert::ALERT_DECLINE|| 
                                    $a['alert_type'] == \App\Alert::ALERT_DISCONNECT || 
                                    $a['alert_type'] == \App\Alert::ALERT_UPDATE_TRACK_HOURS 
                                )

                                    @if($a['sender_type']=='organization')
                                        <div class="notification_pic">
                                            @if($a['sender_logo'] == null)
                                                <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')"></div>
                                            @else
                                                <div class="avatar" style="background-image:url('{{url('/uploads')}}/{{$a['sender_logo']}}')"></div>
                                            @endif
                                        </div>
                                    @elseif($a['sender_type']=='volunteer')
                                        <div class="notification_pic">
                                            @if($a['sender_logo']==null)
                                                <div class="avatar" style="background-image:url('{{asset('img/logo/member-default-logo.png')}}')"></div>
                                            @else
                                                <div class="avatar" style="background-image:url('{{url('/uploads')}}/{{$a['sender_logo']}}')"></div>
                                            @endif
                                        </div>
                                    @endif

                                       
                                                <div class="notification_text_blk">
                                                    <div class="not_title_blk">
                                                        <div class="main-text">
                                                            <?php 
                                                            
                                                                if(!empty($a['oppotunity_id'])){
                                                                    $redirect_url = url('/organization/share_opportunity')."/".$a['oppotunity_id'];
                                                                }elseif($a['alert_type'] == \App\Alert::ALERT_JOIN_OPPORTUNITY_CONFIRM_REQUEST){
                                                                    $redirect_url = route('share.group' ,base64_encode($a['related_id']));
                                                                }else{
                                                                    $redirect_url = url('/organization/profile')."/".$a['sender_id'];
                                                                }
                                                            ?>
                                                            <a href=" {{$redirect_url}}">
                                                            <p class="name">

                                                            @if($a['alert_type'] != \App\Alert::ALERT_JOIN_OPPORTUNITY and $a['alert_type'] != \App\Alert::ALERT_CREATE_PRIVATE_OPPORTUNITY)
                                                                {{$a['sender_name']}}
                                                            @endif
                                                            {{$a['sender_name']}} {!! $a['contents'] !!}
                                                            </p>
                                                            </a>
                                                            @if($a['sender_type'] == 'organization')
                                                                <?php 
                                                                    $userData = $userObj::Select('org_name','parent_id')->where('id', $a['sender_id'])->first();
                                                                    if(!empty($userData->parent_id)){
                                                                        echo 'Parent Organization : '.$userData->parentOrgName($userData->parent_id);
                                                                    }
                                                                ?>
                                                            @endif
                                                            <div class="light">{{$a['sender_type']}} <div class="date">{{$a['date']}}</div></div>
                                                        </div>
                                                    </div>

                                                    <div class="not_button">
                                                        <div class="main-text">
                                                            <input type="hidden" class="sender_id" value="{{$a['sender_id']}}">

                                                            <input type="hidden" class="alert_type" value="{{$a['alert_type']}}">

                                                            @if($a['alert_type'] == \App\Alert::ALERT_CONNECT_CONFIRM_REQUEST)

                                                            @if($a['status'] == 0)
                                                            <a href="#" class="btn-accept"><span>Accept</span></a>
                                                            <p  style="display: none" class="approved"><i class="fa fa-check-square-o"></i> This request is approved.</p>
                                                            @else
                                                            <p class="approved" style="display: block"><i class="fa fa-check-square-o"></i> This request is approved.</p>
                                                            @endif

                                                            @elseif($a['alert_type'] == \App\Alert::ALERT_TRACK_CONFIRM_REQUEST)

                                                            @if($a['status'] == 0)
                                                                <a href="{{url('/organization/track')}}" class="process"><span>Process</span></a>
                                                            @else
                                                                <p class="approved"><i class="fa fa-check-square-o"></i>This request is approved</p>
                                                            @endif

                                                            @elseif($a['alert_type'] == \App\Alert::ALERT_GROUP_INVITATION)

                                                            @if($a['is_apporved'] == 0)
                                                            <a href="{{url('/alert/approve/'.$key.'/'.$a['related_id'].'/2')}}" type="button" class="btn btn-primary" style="color: white" onclick="return confirm('Are you sure ?');">Approve</a>
                                                            <a href="{{url('/alert/approve/'.$key.'/'.$a['related_id'].'/0')}}" type="button" class="btn btn-primary" style="color: white" onclick="return confirm('Are you sure ?');">Decline</a>
                                                            @else
                                                            <p class="approved"><i class="fa fa-check-square-o"></i>This request is approved</p>
                                                            @endif

                                                            @endif
                                                        </div>
                                                    </div>

                                                    {{--<!-- <div class="notification_date">
                                                        <div class="main-text">
                                                            <div class="date">{{$a['date']}}</div>                                                            
                                                        </div>
                                                    </div> -->--}}
                                                </div>                                    



                                @elseif($a['alert_type'] == \App\Alert::ALERT_CONNECT_CONFIRM_REQUEST || $a['alert_type'] == \App\Alert::ALERT_TRACK_CONFIRM_REQUEST  || $a['alert_type'] == \App\Alert::ALERT_GROUP_INVITATION || 
                                $a['alert_type'] == \App\Alert::ALERT_JOIN_OPPORTUNITY_CONFIRM_REQUEST || $a['alert_type'] == \App\Alert::ALERT_AFFILIATED_GROUP || $a['alert_type'] == \App\Alert::ALERT_SUB_ORGANIZATION_REQUEST)

                                    @if($a['sender_type']=='organization')
                                        <div class="notification_pic">
                                            @if($a['sender_logo'] == null)
                                                <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')"></div>
                                            @else
                                                <div class="avatar" style="background-image:url('{{url('/uploads')}}/{{$a['sender_logo']}}')"></div>
                                            @endif
                                        </div>

                                    @elseif($a['sender_type']=='volunteer')
                                        <div class="notification_pic">
                                            @if($a['sender_logo']==null)
                                                <div class="avatar" style="background-image:url('{{asset('img/logo/member-default-logo.png')}}')"></div>
                                            @else
                                                <div class="avatar" style="background-image:url('{{url('/uploads')}}/{{$a['sender_logo']}}')"></div>
                                            @endif
                                        </div>
                                    @endif
                                    <?php 
                                    if($a['alert_type'] == trim(\App\Alert::ALERT_AFFILIATED_GROUP)){
                                        $redirect_url_new = route('share.group' ,base64_encode($a['related_id']));
                                    }else{
                                        $redirect_url_new = url('/organization/profile').'/'.$a['sender_id'];
                                    }?>                                    
                                    <div class="notification_text_blk">
                                            <div class="not_title_blk">
                                                <div class="main-text">
                                                    <a href=" {{$redirect_url_new}}">
                                                        <p class="name">
                                                            @if($a['alert_type'] != \App\Alert::ALERT_JOIN_OPPORTUNITY and $a['alert_type'] != \App\Alert::ALERT_CREATE_PRIVATE_OPPORTUNITY)
                                                                {{$a['sender_name']}}
                                                            @endif
                                                             {!! $a['contents'] !!}
                                                        </p>
                                                    </a>
                                                    <div class="light">{{$a['sender_type']}}   <div class="date">{{$a['date']}}</div></div>
                                                </div>

                                            </div>
                                            

                                            <div class="not_button">
                                                <div class="main-text">
                                                    <input type="hidden" class="sender_id" value="{{$a['sender_id']}}">

                                                    <input type="hidden" class="alert_type" value="{{$a['alert_type']}}">
                                                    @if($a['alert_type'] == \App\Alert::ALERT_CONNECT_CONFIRM_REQUEST)

                                                        @if($a['status'] == 0)
                                                            <a href="#" class="process btn-accept"><span>Accept</span></a>
                                                            <p style="display: none" class="approved"><i class="fa fa-check-square-o"></i> This request is approved.</p>
                                                        @else
                                                            <p class="approved" style="display: block"><i class="fa fa-check-square-o"></i> This request is approved.</p>
                                                        @endif

                                                    @elseif($a['alert_type'] == \App\Alert::ALERT_TRACK_CONFIRM_REQUEST)

                                                        @if($a['is_apporved'] == 0)
                                                            <a href="{{url('/organization/track')}}" class="process"><span>Process</span></a>
                                                        @elseif($a['is_apporved'] == 1)
                                                            <p class="approved"><i class="fa fa-check-square-o"></i>This request is approved</p>
                                                        @elseif($a['is_apporved'] == 2)
                                                            <p class="approved">This request is declined</p>
                                                        @endif

                                                    @elseif($a['alert_type'] == \App\Alert::ALERT_GROUP_INVITATION)

                                                        @if($a['is_apporved'] == 0)
                                                            <a href="{{url('/alert/approve/'.$key.'/'.$a['related_id'].'/2')}}" class="process" onclick="return confirm('Are you sure ?');"><span>Approve</span></a>
                                                            <a href="{{url('/alert/approve/'.$key.'/'.$a['related_id'].'/0')}}" class="process" onclick="return confirm('Are you sure ?');"><span>Decline</span></a>
                                                        @elseif($a['is_apporved'] == 1)
                                                            <p class="approved"><i class="fa fa-check-square-o"></i>This request is approved</p>
                                                        @else
                                                            <p class="approved">This request is declined</p>
                                                        @endif
                                                    @elseif($a['alert_type'] == \App\Alert::ALERT_JOIN_OPPORTUNITY_CONFIRM_REQUEST)

                                                        @if($a['is_apporved'] == 0)
                                                            <a href="{{url('/organization/approveJoinOpportunity/'.$key.'/'.$a['related_id'].'/1')}}" class="process" onclick="return confirm('Are you sure ?');"><span>Approve</span></a>
                                                            <a href="{{url('/organization/approveJoinOpportunity/'.$key.'/'.$a['related_id'].'/0')}}" class="process" onclick="return confirm('Are you sure ?');"><span>Decline</span></a>
                                                        @elseif($a['is_apporved'] == 1)
                                                            <p class="approved"><i class="fa fa-check-square-o"></i>This request is approved</p>
                                                        @else
                                                            <p class="approved">This request is declined</p>
                                                        @endif
                                                  
                                                    @elseif($a['alert_type'] == \App\Alert::ALERT_AFFILIATED_GROUP)

                                                        @if($a['is_apporved'] == 0)
                                                            <a href="{{url('/organization/approveAffiliatedOrg/'.$key.'/'.$a['related_id'].'/1')}}" class="process" onclick="return confirm('Are you sure ?');"><span>Approve</span></a>
                                                            <a href="{{url('/organization/approveAffiliatedOrg/'.$key.'/'.$a['related_id'].'/0')}}" class="process" onclick="return confirm('Are you sure ?');"><span>Decline</span></a>
                                                        @elseif($a['is_apporved'] == 1)
                                                            <p class="approved"><i class="fa fa-check-square-o"></i>This request is approved</p>
                                                        @else
                                                            <p class="approved">This request is declined</p>
                                                        @endif
                                                    @elseif($a['alert_type'] == \App\Alert::ALERT_SUB_ORGANIZATION_REQUEST)

                                                        @if($a['is_apporved'] == 0)
                                                            <a href="{{url('/organization/approveSubOrg/'.$key.'/'.$a['sender_id'].'/1')}}" class="process" onclick="return confirm('Are you sure ?');"><span>Approve</span></a>
                                                            <a href="{{url('/organization/approveSubOrg/'.$key.'/'.$a['sender_id'].'/0')}}" class="process" onclick="return confirm('Are you sure ?');"><span>Decline</span></a>
                                                        @elseif($a['is_apporved'] == 1)
                                                            <p class="approved"><i class="fa fa-check-square-o"></i>This request is approved</p>
                                                        @else
                                                            <p class="approved">This request is declined</p>
                                                        @endif
                                                    @endif
                                                    
                                                </div>
                                            </div>

                                            {{--<!-- <div class="notification_date">
                                                <div class="main-text">
                                                    <div class="date">{{$a['date']}}</div>
                                                    
                                                </div>
                                            </div> -->--}}

                                        </div>
                                    
                                @endif
                            </div>
                        </li>
                    @endforeach
                    <li>
                </ul>

                <div class="wrapper-pagination">
                    {{ $paginate->links('components.pagination') }}
                </div>

            </div>

        </div>

    </div>
@endsection

@section('script')
    <script>
        
        $('.btn-accept').on('click',function () {

            var current_button = $(this);
            var sender_id = $(this).parent().find('.sender_id').val();
            var alert_type = $(this).parent().find('.alert_type').val();

            console.log(sender_id, alert_type);

            if(alert_type == 3){

                var url = '{{Auth::user()->user_role == 'organization' ? route('organization-accept-friend') : route('volunteer-accept-friend') }}';

                $.ajaxSetup({

                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var type = "POST";
                var formData = {id: sender_id}

                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    success: function (data) {

                        current_button.hide();
                        current_button.parent().find('.approved').show();
                    },

                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        })
    </script>
@endsection