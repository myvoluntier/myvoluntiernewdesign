{{--{{THIS IS FOR THE VOLUNTEER PROFILE STRUCURE IS OPPOSITE}}--}}

@extends('layout.master')

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">
    <link rel="stylesheet" href="https://foliotek.github.io/Croppie/croppie.css">

    <style>

        .wrapper-sort-table .table tr td:first-child {
            white-space: normal !important;
            word-wrap: break-word !important;
            max-width: 550px;
        }

        @media screen and (max-width: 524px){
            .wrapper-sort-table .table tr td:first-child{
                width: 100%;
                white-space: nowrap !important;
                word-wrap: normal !important;
                max-width: 100%;
            }
        }

        .alpha{
            font-family:'Open Sans',sans-serif;
            color:green;
            text-transform:uppercase;
            background:#fff;
            border-radius:20px;
            font-size: 14px;
        }

        a {
            text-decoration: none !important;
        }

        .pagination .flex-wrap .justify-content-center {
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .footable-page-arrow, .footable-page{
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .footable-page-arrow a, .footable-page a {
            position: relative;
            display: block;
            padding: 10px;
            margin: 10px 5px;
            font-size: 16px;
            line-height: 20px;
            font-family: 'Open Sans', sans-serif;
            color: #5f5f5f;
            background: none;
            border: 0 none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-decoration: none;
            cursor: auto;
            outline: none;
            z-index: auto;
        }

        .footable-page-arrow a:hover, .footable-page a:hover {
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }

        .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled {
            display: none;
        }

        #first_name, #last_name{
            background-color:#D3D3D3;

        }


        .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled{
            display: none;
        }

        .footable-page.active a{
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }

        .org_logo{
            height: 120px;
            width: 120px;
            float: left;
        }

        .org_info{
            padding-left: 15px;
            padding-top: 15px;
            display: inline-block;
            clear: both;
        }

        .org_info image{
            display: inline-block;
        }

        .org_info label{
            display: block;
        }

        .org_info_addr{
            margin-left: 15px;
            padding: 15px;
            float: right;
            clear: right;
        }
        .org_info_addr label{
            display: block;
        }

        /* #profile-description {
            margin-top: 50px; position:relative;
        }
        #profile-description .text {
            margin-bottom: 5px; color: #777; position:relative; font-size: 14px; display: block;
        }
        #profile-description .show-more {
            color: blue; position:relative; font-size: 10px; height: 20px; cursor: pointer;
        }
        #profile-description .show-more:hover {
            color: #1779dd;
        }
        #profile-description .show-more-height { height: 62px; overflow:hidden; } */

        .text-overflow {
            width:auto;
            height:120px;
            display:block;
            overflow:hidden;
            word-break: break-word;
            word-wrap: break-word;
        }

        .btn-overflow {
            display: none;
            text-decoration: none;
        }


        label.cabinet{
            display: block;
            cursor: pointer;
        }

        label.cabinet input.file{
            position: relative;
            height: 100%;
            width: auto;
            opacity: 0;
            -moz-opacity: 0;
            filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
            margin-top:-30px;
        }

        #upload-logo{
            width: 400px;
            height: 400px;
            padding-bottom:25px;
        }

        #upload-banner{
            width: 800px;
            height: 400px;
            padding-bottom:25px;
        }

        figure figcaption {
            position: absolute;
            bottom: 0;
            color: #fff;
            width: 100%;
            padding-left: 9px;
            padding-bottom: 5px;
            text-shadow: 0 0 10px #000;
        }

        #cropBannerImagePop .modal-dialog{
            max-width: 900px !important;
        }
    </style>

@endsection

@section('content')

    <div class="wrapper-profile-box">
        <div class="top-profile-info">
            <div class="img-box" style="background-image:url('{{$user->back_img == NULL ? asset('front-end/img/bg/0002.png') :  $user->back_img}} ')"></div>

            <div class="container mobile-container">
                <div class="avatar"
                     @if($user->user_role === 'organization')
                        style="background-image:url('{{$user->logo_img == NULL ? asset('front-end/img/org/001.png') : $user->logo_img}}')">
                     @else
                         style="background-image:url('{{$user->logo_img == NULL ? asset('img/logo/member-default-logo.png') : $user->logo_img}}')">
                     @endif
                    <span></span>
                </div>

                <div class="link-img">
                    @if($profile_info['is_my_profile'] == 1)
                        <a id="upload_logo_icon" href="#"><i class="fa fa-camera" aria-hidden="true"></i></a>
                        <a id="upload_logo1_icon" href="#"><i class="fa fa-picture-o" aria-hidden="true"></i></a>
                    @endif
                </div>

            </div>

            <div class="container">
                <div class="row txt-box">
                    <div class="col-12 col-md-6">
                        @if($profile_info['is_friend'] > 1 || $profile_info['is_my_profile'] == 1)
                            <div class="main-text"><p class="h2">{{$user->user_role == 'organization' ? $user->org_name : $user->first_name ." ". $user->last_name}}</p></div>
                        @else
                            <div class="main-text"><p class="h2">{{$user->user_role == 'organization' ? $user->org_name : $user->first_name ." ". substr($user->last_name,0,1)}}</p></div>
                        @endif
                        <?php
                        $address='';
                        $country = '';

                        if($user->city) {
                            $address= $address.$user->city;
                        }
                        if($user->country == "US" || $user->country == "USA"){
                            $country = 'United States';
                        }
                        if($address!='' && $user->country) {
                            $address= $address.','.$country;
                        }
                        elseif($user->country) {
                            $address= $address.$country;
                        }
                        ?>
                        @if(isset($a))
                            <p class="bold">{{$a}}</p>
                        @endif

                        <div class="text-overflow">
                            @if($user->brif)
                                {!! $user->brif !!}
                            @endif
                        </div>
                        <a class="btn-overflow" href="#">Show more</a>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="main-text">
                            <ul class="list-column clearfix">
                                <li>
                                    <p class="h2">{{$friend->count()}}</p>
                                    <p class="light">Friends </p>
                                </li>
                                <li>
                                    <p class="h2">
                                        <?php  echo $groupCount = ($user->user_role === 'organization') ? $group->count() : count((array)$group); ?>
                                    </p>
                                    <p class="light">Groups</p>
                                </li>
                                @if($user->user_role === 'organization')
                                    <li>
                                        <p class="h2">{{$affiliatedGroups->count()}}</p>
                                        <p class="light">Affiliated Groups</p>
                                    </li>
                                @endif
                                <li>
                                    <p class="h2">{{$active_oppr->count()}}</p>
                                    <p class="light">Opportunities</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-profile-info">

            <div class="container">

                <ul class="profile_tab nav nav-tabs" role="tablist">

                    @if($profile_info['is_my_profile'] == 1 || $profile_info['is_friend'] > 1 )

                        @if($profile_info['is_volunteer']==1)
                            <li class="nav-item">
                                <a class="nav-link active show" href="#activity" role="tab" data-toggle="tab"><span>Activity</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#transcript" role="tab" data-toggle="tab"><span>Transcript</span></a>
                            </li>
                        @elseif($profile_info['is_volunteer']==0)
                            <li class="nav-item">
                                <a class="nav-link active show" href="#activity" role="tab" data-toggle="tab"><span>Opportunity</span></a>
                            </li>
                        @endif
                            <li class="nav-item">
                                <a class="nav-link" href="#details" role="tab"
                                   data-toggle="tab"><span>{{$user->user_role === 'organization' ? 'Details' : 'Info' }}</span></a>
                            </li>
                    @else
                        @if($profile_info['is_volunteer']==0)
                            <li class="nav-item">
                                <a class="nav-link active show" href="#activity" role="tab" data-toggle="tab"><span>Opportunity</span></a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link {{$profile_info['is_volunteer']==1 ? 'active show' : ''}} " href="#details" role="tab"
                               data-toggle="tab"><span>{{$user->user_role === 'organization' ? 'Details' : 'Info' }}</span></a>
                        </li>

                    @endif

                    <li class="nav-item">
                        <a class="nav-link" href="#groups" role="tab"
                           data-toggle="tab"><span>Groups</span></a>
                    </li>
                    @if($user->user_role === 'organization')
                        <li class="nav-item">
                            <a class="nav-link" href="#affiliatedGroups" role="tab"
                               data-toggle="tab"><span>Affiliated Groups</span></a>
                        </li>
                        @if(!isset($user->parent_id))
                            <li class="nav-item">
                                <a class="nav-link" id="subOrganizationTab" href="#subOrganization" role="tab"
                                   data-toggle="tab"><span>Sub Organization</span></a>
                            </li>
                        @endif
                    @endif
                    @if($profile_info['is_volunteer']==1)
                        <li class="nav-item" >
                            <a class="nav-link" id="serviceProjectTab" href="#serviceProject" role="tab" data-toggle="tab"><span>Service Projects</span></a>
                        </li>
                    @endif

                </ul>

                <div class="tab-content">


                    @if($profile_info['is_my_profile'] == 1 || $profile_info['is_friend'] > 1 )

                        @if($profile_info['is_volunteer']==1)
                            <div role="tabpanel" class="tab-pane fade in active show" id="activity">
                                @include('components.profile.activity-tab')
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="transcript">
                                @include('components.profile.transcript-tab')
                            </div>
                        @elseif($profile_info['is_volunteer']==0)
                            <div role="tabpanel" class="tab-pane fade in active show" id="activity">
                                @include('components.profile.activity-tab')
                            </div>
                        @endif
                            <div role="tabpanel" class="tab-pane fade" id="details">

                                @include('components.profile.detail-tab')

                                @if($profile_info['is_my_profile'] == 1)
                                    <hr>
                                    @include('components.profile.account-tab')
                                @endif
                            </div>
                    @else

                        @if($profile_info['is_volunteer']==0)
                            <div role="tabpanel" class="tab-pane fade in active show" id="activity">
                                @include('components.profile.activity-tab')
                            </div>
                        @endif

                        <div role="tabpanel" class="tab-pane fade {{$profile_info['is_volunteer']==1 ? 'in active show' : ''}}" id="details">
                            @include('components.profile.detail-tab')
                            @if($profile_info['is_my_profile'] == 1)
                                <hr>
                                @include('components.profile.account-tab')
                            @endif
                        </div>
                    @endif


                   <div role="tabpanel" class="tab-pane fade " id="groups">
                       <div class="row">

                           @if($groupCount)
                               @foreach($group as $groupSingle)
                                   <div class="col-sm-4 mb-2">
                                       <a href="{{($authUser && ($groupSingle->is_share_able || $groupSingle->is_public==1)) ? route('share.group' ,base64_encode($groupSingle->id)) : 'javascript:void(0);'}}">
                                           <div class="card" style="background-color: #edf2f3">
                                               <div class="card-body">
                                                   <div class="main-text">
                                                       <p class="name">
                                                           {{str_limit($groupSingle->name ,30)}}
                                                           @if($groupSingle->is_public ==1)
                                                               <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                           @else
                                                               <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                           @endif
                                                       </p>
                                                       @if(isset($groupSingle->org_name))
                                                        <p><span style="font-size:13px">{{$groupSingle->org_name}}</span></p>
                                                       @endif
                                                       <p>{{"Category : ".$groupSingle->categoryName}}</p>
                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                   </div>
                               @endforeach
                           @else
                               You do not have active groups yet
                           @endif

                       </div>
                   </div>
                    @if($user->user_role === 'organization')
                    <div role="tabpanel" class="tab-pane fade " id="affiliatedGroups">
                       <div class="row">

                           @if(count($affiliatedGroups))
                               @foreach($affiliatedGroups as $affiliatedGroupSingle)
                                   <div class="col-sm-4 mb-2">
                                       <a href="{{($authUser && ($affiliatedGroupSingle->is_share_able || $affiliatedGroupSingle->is_public ==1)) ? route('share.group' ,base64_encode($affiliatedGroupSingle->id)) : 'javascript:void(0);'}}">
                                           <div class="card" style="background-color: #edf2f3">
                                               <div class="card-body">
                                                   <div class="main-text">
                                                       <p class="name">
                                                           {{str_limit($affiliatedGroupSingle->name ,30)}}
                                                           @if($affiliatedGroupSingle->is_public == 1)
                                                               <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                           @else
                                                               <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                           @endif
                                                       </p>
                                                       @if($affiliatedGroupSingle->org_name)
                                                        <p><span style="font-size:13px">{{$affiliatedGroupSingle->org_name}}</span></p>
                                                       @endif
                                                       <p>{{ "Category : ".$affiliatedGroupSingle->categoryName}}</p>
                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                   </div>
                               @endforeach
                           @else
                                 You do not have active groups yet 
                           @endif

                       </div>
                   </div>
                    <div role="tabpanel" class="tab-pane fade" id="subOrganization">
                        @include('components.profile.sub-organization')
                    </div>
                    @endif
                    @if($profile_info['is_volunteer']==1)
                        <div role="tabpanel" class="tab-pane fade" id="serviceProject">
                            @include('components.profile.service-project-tab')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

   

@endsection


@section('script')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/plugins/datetimepicker/moment.js')}}"></script>
    <script src="https://foliotek.github.io/Croppie/croppie.js"></script>

    @if($profile_info['is_my_profile'] == 1)
        <script src="{{asset('front-end//js/ckeditor/ckeditor.js')}}"></script>
        <script src="{{asset('front-end/js/ckeditor/sample.js')}}"></script>
        
        <script src="{{asset('js/check_validate.js')}}"></script>
        <script src="{{asset('front-end/js/sub-organization.js')}}"></script>
    @endif

    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            if(window.location.href.indexOf("serviceProject") > -1) {
                $('#serviceProjectTab').trigger('click');
            }
        });
        var $uploadCrop,
                tempFilename,
                rawLogoImg,
                rawBannerImg,
                imageId;
        var $selected_input;

        function readURL(input, name) {
            $selected_input = name;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                if(name == "profile"){
                    reader.onload = function (e) {
                        $('.upload-logo').addClass('ready');
                        $('#cropImagePop').modal('show');
                        rawLogoImg = e.target.result;
                    };
                }else{
                    reader.onload = function (e) {
                        $('.upload-banner').addClass('ready');
                        $('#cropBannerImagePop').modal('show');
                        rawBannerImg = e.target.result;
                    };
                }

                reader.readAsDataURL(input.files[0]);
                // console.log()e.target.result;
            }
        }

        var is_open = 0;
        $('#filter_controller').on('click', function (e) {
            if(is_open == 0){
                $('.filter_div').show();
                is_open = 1;
            }else {
                $('.filter_div').hide();
                is_open = 0;
            }
        });

        $('#share_transcript').on('click', function (e) {
            $('.success-first').hide();
            $("#share_emails").val('');
            $('.hide-email-comment').show();
            $('#comments').val('');
            $('#share_profile').show();
            $('#close_share_profile_hide').hide();
            $('#myModal').modal('show');
        });

        $('#filter_apply').on('click', function (e) {
            e.preventDefault();
            var url = API_URL + 'profile/get_transcript';
            var id = '{{$id}}';
            var opp_types = $('.opp-search-checkbox').val();
            var org_types = $('.org-search-checkbox').val();
            var start_date = $('#transcript_start_date').val();
            var end_date = $('#transcript_end_date').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "GET";

            var formData = {
                id: id,
                opp_types: opp_types,
                org_types: org_types,
                start_date: start_date,
                end_date: end_date
            };

            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                    var contentTypeScriptString = '<div id="filter_content">';
                    $.each(data.oppr, function (index,value) {
                        contentTypeScriptString +='<div class="typescript" style="padding: 15px;"><div id="">';

                        if(value.org_logo == null){
                            contentTypeScriptString +='<img src="{{ asset('front-end/img/org/001.png')}}" class="org_logo">';
                        }else {
                            contentTypeScriptString +='<img src="' +value.org_logo+ '" class="org_logo">';
                        }

                        contentTypeScriptString +='<div class="org_info"><Label><strong>' +value.org_name+ '</strong></Label><Label>' +value.contact_number+ '</Label><Label>' +value.email+ '</Label></div>';
                        contentTypeScriptString +='<div class="org_info_addr"><Label>' +value.address+ '</Label></div></div><div style="overflow: auto; clear: both;">';
                        contentTypeScriptString +='<table id="example21" class="table sortable member-table">';
                        contentTypeScriptString +='<thead><tr><th><div class="main-text"><p>Date/Time</p></div></th><th><div class="main-text"><p>Duration(hrs)</p></div></th><th><div class="main-text"><p>Opportunity Title</p></div></th><th><div class="main-text"><p>Category/Type</p></div></th><th><div class="main-text"><p>Date Validated</p></div></th><th><div class="main-text"><p>Validator Name</p></div></th></tr></thead>';
                        contentTypeScriptString +='<tbody>';

                        $.each(value.track_info, function (index,track_value) {
                            contentTypeScriptString +='<tr><td>'+track_value.logged_date+'</td><td>'+track_value.logged_mins/60+'</td><td>'+track_value.oppor_name+'</td>';
                            var category_name = '';
                            var confirmed_at = '';
                            var confirmer_name = '';
                            if(track_value.confirmed_at != null){
                                confirmed_at = track_value.confirmed_at;
                            }
                            if(track_value.confirmer_name != null){
                                confirmer_name = track_value.confirmer_name;
                            }
                            category_name = track_value.name;
                            contentTypeScriptString +='<td>'+category_name+'</td><td>'+confirmed_at+'</td><td>'+confirmer_name+'</td></tr>';
                        });
                        contentTypeScriptString +='</tbody></table></div></div>';
                    });
                    $('#filter_content').replaceWith(contentTypeScriptString);
                },

                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

    $("#transcript_end_date").prop('disabled', true);

    $('#transcript_start_date').datepicker({
        'format': 'mm-dd-yyyy',
        'autoclose': true,
        'orientation': 'right',
        'todayHighlight': true,
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $("#transcript_end_date").prop('disabled', false);
        $('#transcript_end_date').datepicker('setStartDate', endDate);
        $('#transcript_end_date').datepicker('setDate', endDate);
    }).on('clearDate', function (selected) {
        $('#transcript_end_date').datepicker('setStartDate', '+0d');
    });

    $('#transcript_end_date').datepicker({
        'format': 'mm-dd-yyyy',
    }).on('changeDate', function (selected) {
        daterangeChange();
    });
    
</script>

    <script type="text/javascript">
        var _URL = window.URL || window.webkitURL;
        $('#upload_logo_icon').on('click', function () {
            $('#upload_logo_input').click()
        });

        $('#upload_logo1_icon').on('click', function () {
            $('#upload_logo1_input').click()
        });

        $('#upload_logo_input').on('change', function () {
//            $('#upload_logo').submit()
            readURL(this, "profile");
        });
        $('#upload_logo1_input').on('change', function () {
            readURL(this, "banner");
//            var file, img;
//
//            if ((file = this.files[0])) {
//
//                img = new Image();
//
//                img.onload = function () {
//
//
//                    if (this.width < 1500 || this.height < 300) {
//
//                        $('#banner_size').val(1);
//
//                        alert('Recomended banner size 1500 X 300. Please try another');
//
//                    }
//
//                    else {
//
//                        $('#upload_logo1').submit()
//
//                        //alert(this.width+'=='+this.height);
//
//                    }
//
//                };
//
//                img.src = _URL.createObjectURL(file);
//
//            }

        });
        $('.footable').dataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "pageLength": 10,
            "pagingType": "full_numbers",
            "language": {
                "paginate": {
                    "first": "",
                    "next": '',
                    "previous": '',
                    "last": ''
                }
            }
        });
        /*$('.footable').footable({
            limitNavigation: 5,
            firstText: '1'
        });
        $('ul.pagination').each(function(){
            if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
            else $(this).find(' .footable-page-arrow:last').show();
        });

        $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
            var pagination = $(this).parents('.tab-pane.active .pagination');
            if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
            else pagination.find('.footable-page-arrow:first').show();
            if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
            else pagination.find('.footable-page-arrow:last').show();
        });*/
        @if($profile_info['is_my_profile'] == 1)
        initSample();
        @endif
        $(document).ready(function () {
            @if($user->user_role === 'volunteer' and   $profile_info['is_my_profile'] == 1)
                CKEDITOR.replace('editor');
            @endif

            /*$('.footable').footable({
                limitNavigation: 5,
                firstText: '1'
            });
            $('ul.pagination').each(function(){
                if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
                else $(this).find(' .footable-page-arrow:last').show();
            });

            $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
                var pagination = $(this).parents('.tab-pane.active .pagination');
                if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
                else pagination.find('.footable-page-arrow:first').show();
                if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
                else pagination.find('.footable-page-arrow:last').show();
            });*/

            /*.dataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });*/

            function doResize($wrapper, $el) {

                var scale = Math.min(
                    $wrapper.outerWidth() / $el.outerWidth(),
                    $wrapper.outerHeight() / $el.outerHeight()
                );

                if (scale < 1) {

                    $el.css({
                        '-webkit-transform': 'scale(' + scale + ')',
                        '-moz-transform': 'scale(' + scale + ')',
                        '-ms-transform': 'scale(' + scale + ')',
                        '-o-transform': 'scale(' + scale + ')',
                        'transform': 'scale(' + scale + ')'
                    });

                } else {
                    $el.removeAttr("style");
                }

            }

            $('.wrapper_input.fa-icons input').datepicker({
                'format': 'mm-dd-yyyy',
                'autoclose': true,
                'orientation': 'right',
                'todayHighlight': true
            });


            $('.dashboard_org .track-it-time_slider .slider-wrapper').each(function () {
                doResize($(this), $(this).find('.slider-item-scale > div'));
            });

            $(window).resize(function () {
                $('.dashboard_org .track-it-time_slider .slider-wrapper').each(function () {
                    doResize($(this), $(this).find('.slider-item-scale > div'));
                });
            });

            $('.member-table').dataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": true,
                "pageLength": 10,
                "pagingType": "full_numbers",
                "language": {
                    "paginate": {
                        "first": "",
                        "next": '',
                        "previous": '',
                        "last": ''
                    }
                }
            });
            /* $('.member-table').footable({
                 limitNavigation: 5,
                 firstText: '1'
             });
             $('ul.pagination').each(function(){
                 if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
                 else $(this).find(' .footable-page-arrow:last').show();
             });

             $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
                 var pagination = $(this).parents('.tab-pane.active .pagination');
                 if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
                 else pagination.find('.footable-page-arrow:first').show();
                 if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
                 else pagination.find('.footable-page-arrow:last').show();
             });

             if ($('.footable-page-arrow:last').text() < 5) $('.footable-page-arrow:last').hide();
             else $('.footable-page-arrow:last').show();
             console.log($('.footable-page-arrow:last').text())

             $(document).on('click', 'a[data-page]', function () { //, .footable-page-arrow
                 if ($('.footable-page:first a').data('page') == 0) $('.footable-page-arrow:first').hide();
                 else $('.footable-page-arrow:first').show();
                 if (parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
                 else $('.footable-page-arrow:last').show();
                 console.log($('.footable-page-arrow:last').text())
             });

             $('.member-table').footable({
                 limitNavigation: 5,
                 firstText: '1'
             });
             $('ul.pagination').each(function(){
                 if (Math.abs((parseInt($(this).find('.footable-page:last a').data('page')) + 1) - parseInt($(this).find('.footable-page-arrow:last a').text())) < 0.01) $(this).find('.footable-page-arrow:last').hide();
                 else $(this).find(' .footable-page-arrow:last').show();
             });

             $('.pagination').on('click', 'li a[data-page]', function () { //, .footable-page-arrow
                 var pagination = $(this).parents('.tab-pane.active .pagination');
                 if (pagination.find('.footable-page:first a').data('page') == 0) pagination.find('.footable-page-arrow:first').hide();
                 else pagination.find('.footable-page-arrow:first').show();
                 if (Math.abs((parseInt(pagination.find('.footable-page:last a').data('page')) + 1) - parseInt(pagination.find('.footable-page-arrow:last a').text())) < 0.01) pagination.find('.footable-page-arrow:last').hide();
                 else pagination.find('.footable-page-arrow:last').show();
             });*/


            $('.search-input').on('keyup', function (e) {
                if (e.keyCode == 13) {
                    $('#btn_search_page').trigger('click');
                }
            })


            $('.filter').on('click', function () {
                console.log('change')
                $('#btn_search_page').trigger('click');
            });


            var $image = $(".profile-back-image > img")


//            $("#inputImage").change(function (e) {
//
//                var file, img;
//
//                if ((file = this.files[0])) {
//
//                    img = new Image();
//
//                    img.onload = function () {
//
//
//                        if (this.width < 1500 || this.height < 300) {
//
//                            $('#banner_size').val(1);
//
//                            alert('Recomended banner size 1500 X 300. Please try another');
//
//                        }
//
//                        else {
//
//                            $('#banner_size').val(0);
//
//                            $('#upload_logo').submit();
//
//                            //alert(this.width+'=='+this.height);
//
//                        }
//
//                    };
//
//                    img.src = _URL.createObjectURL(file);
//
//                }
//
//            });

        });


        // Get all html elements for map
         @if($user->user_role !== 'volunteer')
        var latLng = new google.maps.LatLng(parseFloat($('#lat_val').val()), parseFloat($('#lng_val').val()));

        var myOptions = {
            zoom: 16,
            center: latLng,
            styles: [{
                "featureType": "water",
                "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]
            }, {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#808080"}, {"lightness": 54}]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ece2d9"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ccdca1"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#767676"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#ffffff"}]
            }, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]
            }, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.sports_complex",
                "stylers": [{"visibility": "on"}]
            }, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.business",
                "stylers": [{"visibility": "simplified"}]
            }],
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("pos_map"), myOptions);

        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon: '{{asset('front-end/img/pin.png')}}'
        });
        @endif

        $('#btn_follow').on('click', function () {

            var url = API_URL + 'organization/followOrganization';
            var id = $('#current_id').val();
            var current_button = $(this);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "POST";

            var formData = { id: id }

            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                    current_button.hide();
                    $('#btn_unfollow').show();
                },

                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


        $('#btn_unfollow').on('click', function () {

            var url = API_URL + 'organization/unfollowOrganization';
            var id = $('#current_id').val();
            var current_button = $(this);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "POST";

            var formData = { id: id}

            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
                    current_button.hide();
                    $('#btn_follow').show();
                },

                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });


    </script>

    @if($profile_info['is_my_profile'] == 1)
        <script>
            var display = $('.footable-page-arrow:nth-last-child(3)').css('display')

            if (display == 'none') {
                $('.footable-page-arrow:last-child').css('display', 'none')
            }
            else {
                $('.footable-page-arrow:last-child').css('display', 'block')
            }

            /*if(parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
            else $('.footable-page-arrow:last').show();
            $(document).on('click', '.footable-page, .footable-page-arrow', function () {

                var display = $('.footable-page-arrow:nth-child(3)').css('display')

                if(display == 'none'){
                    $('.footable-page-arrow:first-child').css('display', 'none')
                }
                else{
                    $('.footable-page-arrow:first-child').css('display', 'block')
                }

                if(parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
                else $('.footable-page-arrow:last').show();
            });*/

            $('#submit-update-account').on('click', function () {
                
//                $('#submit-update-account').text('Please Wait');

                $('.org_name_detail').text($('#org_name').val())
                $('.user_name_detail').text($('#user_id').val())
                $('.user_email_detail').text($('#email').val())
                $('.user_phone_detail').text($('#contact_num').val())
                $('.user_education_detail').text(CKEDITOR.instances.editor.getData());
                $('#orgSum').val(CKEDITOR.instances.editor.getData())

                $('#textarea_box').html(CKEDITOR.instances.editor.getData());
                
                $('#update_account_oug').submit();
            });

            $("#update_account_oug").unbind('submit').submit(function (event) {
                event.preventDefault();
                var formData = new FormData($(this)[0]);
                var flags = 0;
                
                if ($('#org_name').val() == '') {
                    $('#org_name').css("border", "1px solid #ff0000");
                    flags++;
                }
                if ($('#email').val() == '') {
                    $('#email').css("border", "1px solid #ff0000");
                    flags++;
                }
                if ($('#new_password').val() != '') {
                    if (!ValidatePassword($('#new_password').val())) {
                        flags++;
                        $('#invalid_password').show();
                        $('#new_password').css("border", "1px solid #ff0000");
                    }

                    if ($('#new_password').val() != $('#confirm_password').val()) {
                        flags++;
                        $('#invalid_confirm').show();
                        $('#confirm_password').css("border", "1px solid #ff0000");
                    }
                }
                if(flags == 0){
                    $('#submit-update-account').hide();
                    $('.btn_update_account').show();
                    $.ajax({
                        url: API_URL + 'organization/update_account',
                        type: 'POST',
                        data: formData,
                        async: true,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (return_data) {
                            $('#submit-update-account').show();
                            $('.btn_update_account').hide();
                            var result = $.parseJSON(return_data);

                            if (result.status == 1) {
                                toastr.success(result.msg, "Message");
                            } else {
                                toastr.error(result.msg, "Error");
                            }
                        }
                    });
                }else{
                    toastr.error('Form field validation.', "Error");
                }  
            });

            $('#btn_edit').click(function () {
                var modal = document.getElementById('myModal');
                var btn = document.getElementById('btn_edit');
                var span = document.getElementsByClassName("close")[0];
                btn.onclick = function () {
                    modal.style.display = "block";
                }
                span.onclick = function () {
                    modal.style.display = "none";
                }
                window.onclick = function (event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
            });

            $('#btn_save').on('click', function (e) {

                e.preventDefault();
                var flags = 0;

                if ($('#email').val() == '') {
                    $('#email').css("border", "1px solid #ff0000");
                    flags++;
                }

                if (!ValidateEmail($('#email').val())) {
                    flags++;
                    $('#invalid_email_alert').show();
                }

                if ($('#birth_day').val() == '') {
                    $('#birth_day').css("border", "1px solid #ff0000");
                    flags++;
                }

                if ($('#address').val() == '') {
                    $('#address').css("border", "1px solid #ff0000");
                    flags++;
                }

                if ($('#zipcode').val() == '') {
                    $('#zipcode').css("border", "1px solid #ff0000");
                    flags++;
                }

                if (!ValidateZipcode($('#zipcode').val())) {
                    flags++;
                    $('#zipcode').css("border", "1px solid #ff0000");
                    $('#invalid_zipcode_alert').show();
                }

                // if($('#contact_num').val()=='') {
                //     $('#contact_num').css("border", "1px solid #ff0000");
                //     flags++;
                // }

                // if (!ValidatePhoneNumber($('#contact_num').val())) {
                //     flags++;
                //     $('#invalid_contact_number').show();
                //     $('#contact_num').css("border", "1px solid #ff0000");
                // }


                if ($('#new_password').val() != '') {
                    if (!ValidatePassword($('#new_password').val())) {
                        flags++;
                        $('#invalid_password').show();
                        $('#new_password').css("border", "1px solid #ff0000");
                    }

                    if ($('#new_password').val() != $('#confirm_password').val()) {
                        flags++;
                        $('#invalid_confirm').show();
                        $('#confirm_password').css("border", "1px solid #ff0000");
                    }
                }

                if (flags == 0) {

                    $('.user_name_profile').text($('user_id').val())
                    $('.user_email_profile').text($('#email').val())

                    $('.my_summary_acount').val(CKEDITOR.instances.editor.getData())
                    $('#textarea_box').html(CKEDITOR.instances.editor.getData())

                    $('#update_account').submit()
                }

            });

            $("#update_account").unbind('submit').submit(function (event) {

                event.preventDefault();

                var formData = new FormData($(this)[0]);

                $.ajax({
                    url: API_URL + "volunteer/update_account",
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (return_data) {
                        var result = $.parseJSON(return_data);
                        $('#btn_save').text('Save Change');

                        if (result.status == 1) {
                            toastr.success(result.msg, "Message");
                        } else {
                            toastr.error(result.msg, "Error");
                        }
                    }
                });

            });

            var text = $('.text-overflow'), btn = $('.btn-overflow'), h = text[0].scrollHeight;
            if (h > 120) {
                btn.addClass('less');
                btn.css('display', 'block');
            }
            btn.click(function (e) {
                e.stopPropagation();
                if (btn.hasClass('less')) {
                    btn.removeClass('less');
                    btn.addClass('more');
                    btn.text('Show less');
                    text.animate({'height': h});
                } else {
                    btn.addClass('less');
                    btn.removeClass('more');
                    btn.text('Show more');
                    text.animate({'height': '120px'});
                }
            });

            // $(".show-more").click(function () {
            //     if($(".text").hasClass("show-more-height")) {
            //         $(this).text("Show Less");
            //     } else {
            //         $(this).text("Show More");
            //     }
            //     $(".text").toggleClass("show-more-height");
            // });

            $('.Founded_year_format').datepicker({
                autoclose: true,
                format: 'yyyy',
                minViewMode: 2
            });
            
        </script>
    @endif

@endsection