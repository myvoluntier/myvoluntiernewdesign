@extends('layout.masterForAuthUser')

@section('css')
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/jquery.bxslider.css')}}">

    <style>
        .wrapper-right-text-info ul li a{
            text-decoration: none;
        }
        hr{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('content')
    <div class="dashboard_org">

        <div class="container">

            <div class="news-followed-box pt30 wrapper-tablist">

                @if($action == "verified")
                    <p id="new_signin" class="alert-success" style="padding: 10px">Your account successfully verified!</p>
                 @endif
                 <div class="row">
                    <div class="col-12 col-md-12 vol-news-feedlist">
                  <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">News Feed</a>
                      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Opportunities</a>
                    </div>
                  </nav>
                  <div class="tab-content py-3 px-3" id="nav-tabContent">                                      
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        @if (count($feedNewsArr)>0)   
                        <ul class="news-feeds-list">                            
                                @foreach($feedNewsArr as $nawfVal)        
                                    <li>
                                         <div class="dateand-time">
                                            <span class="news-feed-icon">
                                                <i class="fa {{ !preg_match("/left/", $nawfVal['reason']) ? 'fa-check' : 'fa-exclamation-triangle'}}" aria-hidden="true">
                                                </i>
                                            </span>
                                            <span class="light feed-date">
                                                <?php
                                                $str=strtotime($nawfVal['created_at']) ;  
                                              ?>
                                                 <div>{{date('M d, Y', $str)}}</div>
                                                 <div>{{date('H:i:s A',$str)}}</div>     
                                           </span>
                                          </div>
                                           <p>
                                               <span>{{ucfirst($nawfVal['volName']) }} {{ucfirst($nawfVal['reason']) }} by <a href="{{url( $nawfVal['userurl'])}}">{{ $nawfVal['who_joined'] }}</a> on <a href="{{url( $nawfVal['utl']) }}">{{ $nawfVal['name']}}</a></span>  
                                           </p> 
                                        </li>
                                    @endforeach                               
                                  </ul>
                                @else
                            There are no news feed available at present 
                            @endif 
                         </div>
                     
                  
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        @if (count($opportunityFeeds)>0)  
                        <ul class="news-feeds-list">    
                            @foreach($opportunityFeeds as $value)
                            <li>
                                <div class="dateand-time">
                                    <span class="news-feed-icon">
                                        <i class="fa {{ !preg_match("/left/", $value['reason']) ? 'fa-check' : 'fa-exclamation-triangle'}}" aria-hidden="true">
                                        </i>
                                    </span>
                                    <span class="light feed-date">
                                        <?php
                                        $str=strtotime($value['created_at']) ;  
                                        ?>
                                            <div>{{date('M d, Y', $str)}}</div>
                                            <div>{{date('H:i:s A',$str)}}</div>     
                                    </span>
                                 </div>
                                  <p>
                                      <span>{{ucfirst($nawfVal['volName']) }} {{ucfirst($value['reason']) }} by <a href="{{url( $value['userurl'])}}">{{ $value['who_joined'] }}</a> on <a href="{{url( $value['utl']) }}">{{ $value['name']}}</a></span>  
                                  </p> 
                             </li>
                             @endforeach                                                    
                         </ul>  
                         @else
                            There are no opportunities available at present 
                            @endif                        
                      </div>                                         
                 </div>
                 <div class="col-12 col-md-12">
                    @if (count($feedNewsArr)>0)
                    <div class="wrapper-pagination pull-right" style="display:none" id="newsfeedPaginate">
                        @if (count($feedNewsArr)>0)
                          {{$feedNewsArr->render()}}
                        @endif
                     </div>
                     @endif
                    </div> 
                <div class="col-12 col-md-12">
                    @if (count($opportunityFeeds)>0)
                    <div class="wrapper-pagination pull-right" style="display:none" id="opporPaginate">
                        @if (count($opportunityFeeds)>0)
                        {{$opportunityFeeds->fragment('nav-profile')}}
                        @endif
                    </div>
                    @endif
                </div> 
                </div>
            </div>
        </div>
    </div>
 </div>

@endsection

@section('script')

    {{-- <script src="{{asset('js/jquery.bxslider-rahisified.js')}}"></script> --}}
    <script>

    $("#nav-home-tab").click(function(){
        location.replace(location.origin+location.pathname+"#nav-home");   
        $("#opporPaginate").hide();
        $("#newsfeedPaginate").show();  
    });
    $("#nav-profile-tab").click(function(){
        location.replace(location.origin+location.pathname+"#nav-profile");
        $("#newsfeedPaginate").hide();
        $("#opporPaginate").show();
    });
   
   $(document).ready(function() {
        if(window.location.href.indexOf("nav-profile")>1){
            $( "#nav-profile-tab" ).addClass('active');
            $( "#nav-home-tab" ).removeClass('active');
            $("#nav-profile").addClass('active show');
            $("#nav-home").removeClass('active show');
            $("#opporPaginate").show();
            $("#newsfeedPaginate").hide();
            }
            if($(".nav-tabs a.active").attr('id')=="nav-home-tab"){
                $("#opporPaginate").hide();
                $("#newsfeedPaginate").show(); 
            }   
            setTimeout(function () {
                $('#new_signin').fadeOut('fast');
            }, 3000);
        });
    </script>
@endsection