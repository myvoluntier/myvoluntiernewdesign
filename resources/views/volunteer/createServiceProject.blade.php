@extends('layout.masterForAuthUser')

@section('css')
    <link rel="stylesheet" href="{{asset('front-end/css/service-project.css')}}">
@endsection

@section('content')
<div class="wrapper-create-edit-group">
    <div class="container" id="service_project_box">
        <form id="post_service_project_form" role="form" method="post"
              action="{{($service_project_id == null) ? route('add-service-project') : url('/volunteer/createServiceProject').'/'. base64_encode($service_project_id)}}"
              enctype="multipart/form-data">
            <input type="hidden" id="banner_size" value="0">
            {{csrf_field()}}

            <div class="main-text border-bottom">
                <h2 class="h2">{{ ($service_project_id == null) ? 'Creating New Service Project' : 'Edit Service Project'}}</h2>
                <p class="light">{{($service_project_id == null) ? 'You can create a new Service Project right now. Fill out the information below and click "Create Service Project".' : 'You can edit your Service Project here.'}}</p>
             </div>

            <div class="form-group border-bottom">
                <div class="main-text">
                    <h3 class="h3">Service Project Info</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-12">

                    <div class="form-group">
                        <label class="label-text">Title:<span class="astriskCol">*</span></label>
                        <div class="wrapper_input">
                            <input type="text" id="title" name="title" class="form-control" value="{{ (!empty($serviceProject->title))? $serviceProject->title : '' }}" placeholder="Service Project Title">
							<span style="color:red" id="titleError"></span>
						</div>
                    </div>
                </div>

                <div class="col-6 col-md-6">
                    <div class="form-group">
                        <label class="label-text">Start Date:<span class="astriskCol">*</span></label>
                        <div class="wrapper_input fa-icons">
                            <input name="start_date" readonly id="start_date" class="form-control date_created" type="text" value="{{ (!empty($serviceProject->start_date))? date('m-d-Y',strtotime($serviceProject->start_date)) : '' }}" placeholder="Start Date ( MM-DD-YYYY )">
                            <span class="focus-border"></span>
                            <i class="fa fa-calendar" id="start_date_icon" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>

                <div class="col-6 col-md-6">
                    <div class="form-group">
                        <label class="label-text">End Date:<span class="astriskCol">*</span></label>
                        <div class="wrapper_input fa-icons">
                            <input type="text" id="end_date" readonly name="end_date" class="form-control date_created" value="{{ (!empty($serviceProject->end_date))? date('m-d-Y',strtotime($serviceProject->end_date)) : '' }}" placeholder="End Date ( MM-DD-YYYY )">
                            <span class="focus-border"></span>
                            <i class="fa fa-calendar" id="end_date_icon" aria-hidden="true"></i>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-12">
                    <div class="form-group">
                        <label class="label-text">Description:<span class="astriskCol">*</span></label>
                        <div class="wrapper_input">
                            <textarea class="form-control" id="description" name="description" placeholder="Service Project Description">{{ (!empty($serviceProject->description))? $serviceProject->description : '' }}</textarea>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-12">
                    <div class="form-group">
                        <label class="label-text">Outcome:<span class="astriskCol">*</span></label>
                        <div class="wrapper_input">
                            <textarea class="form-control" id="outcome" name="outcome" placeholder="Service Project Outcome">{{ (!empty($serviceProject->outcome))? $serviceProject->outcome : '' }}</textarea>
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-group">
                <div class="main-text border-bottom">
                    <h3 class="h3">Select the (confirmed) volunteer hours below to include in this Service Project:</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-12">
                    @include('volunteer.serviceProjectHours')
                </div>

            </div>


            <div class="wrapper_row_link">
                <div class="wrapper-link two-link margin-top">
                    <a href="{{url('/volunteer/profile#serviceProject') }}" class="red"><span>Back</span></a>
                    <a id="btn_service_project_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                    <a href="javascript:void(0);" id="btn_service_project"><span>Submit</span></a>
                </div>
            </div>

            @if($service_project_id != null)
                <input type="hidden" name="service_project_id" id="service_project_id" value="{{$service_project_id}}">
            @endif

        </form>
    </div>
</div>

@include('components.shareServiceProject')
@endsection

@section('script')

    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('js/plugins/datetimepicker/moment.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.11.3/standard-all/ckeditor.js"></script>

    <script>
        $('document').ready(function () {
            $('.date_created').css("cursor", "pointer");
            $('#start_date_icon').css("cursor", "pointer");
            $('#end_date_icon').css("cursor", "pointer");
            $('#start_date_icon').on('click', function () {
                $('#start_date').datepicker('show');
            })
            $('#end_date_icon').on('click', function () {
                $('#end_date').datepicker('show');
            })
        });

        $('#titleError').hide();
        var start = new Date();
        // set end date to max one year period:
        var end = new Date(new Date().setYear(start.getFullYear() + 1));
        $('#start_date').datepicker({
            format: 'mm-dd-yyyy',
            //startDate : start,
            //endDate   : end,
            autoclose: true
            // update "toDate" defaults whenever "fromDate" changes
        }).on('changeDate', function () {
            // set the "toDate" start to not be later than "fromDate" ends:
            $('#end_date').datepicker('setStartDate', new Date($(this).val()));
        });
        $('#end_date').datepicker({
            format: 'mm-dd-yyyy',
            //startDate : start,
            //endDate   : end,
            autoclose: true
            // update "fromDate" defaults whenever "toDate" changes
        }).on('changeDate', function () {
            // set the "fromDate" end to not be later than "toDate" starts:
            $('#start_date').datepicker('setEndDate', new Date($(this).val()));
        });


        $('#btn_service_project').on('click', function (e) {
            e.preventDefault;
            $("input, textarea").css("border", "");
            $(".cke_chrome").css("border", "1px solid #d1d1d1")
            var flags = 0;
            if ($("#title").val() == '') {
                $("#title").css("border", "1px solid #ff0000");
                flags++;
            } else {
                flags = getValidatedTitle(flags);
            }
            if ($("#start_date").val() == '') {
                $("#start_date").css("border", "1px solid #ff0000");
                flags++;
            }
            if ($("#end_date").val() == '') {
                $("#end_date").css("border", "1px solid #ff0000");
                flags++;
            }

            if (CKEDITOR.instances['description'].getData() == '') {
                $("#cke_description").css("border", "1px solid #ff0000");
                flags++;
            }
            if (CKEDITOR.instances['outcome'].getData() == '') {
                $("#cke_outcome").css("border", "1px solid #ff0000");
                flags++;
            }
            if (flags == 0) {
                inProgress();
                $('#post_service_project_form').submit();

            }
            else {
                toastr.error("Form data is invalid.", "Error");
            }
        });

        function getValidatedTitle(flags) {
            var formData = {
                title: $('#title').val(),
                serviceid: $("#service_project_id").val()
            };
            inProgress();
            $.ajax({
                type: 'POST',
                url: API_URL + 'volunteer/validated-service-project',
                data: formData,
                async: false,
                success: function (data) {
                    outProgress();
                    $('#titleError').hide();
                    if (data != "success") {

                        $("#title").css("border", "1px solid #ff0000");
                        $('#titleError').show();
                        $('#titleError').html('Title is already exist for other service project.');
                        flags++;
                    } else {
                        flags = 0;
                    }

                }
            });
            return flags;
        }

        function inProgress() {
            $('#service_project_box').addClass("in-progress");
            $('#btn_service_project_loading').show();
            $('#btn_service_project').hide();
        }

        function outProgress() {
            $('#service_project_box').removeClass("in-progress");
            $('#btn_service_project_loading').hide();
            $('#btn_service_project').show();
        }

        $('#share_service_project_modal').on('click', function (e) {
            $('#myServiceProject').modal('show');
        });

        $('#share_service_project').on('click', function () {
            var url = API_URL + 'share_service_project';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var emailsVal = $("#share_emails_sp").val();
            // var checkEmail = emailsVal.replace(/^\s+|\s+$/g, '');

            var type = "POST";

            if ($("#share_emails_sp").val() !== '') {

                $('.wrap_input_email').removeClass('has-error');
                $('.text-error').hide();
                $("#share_service_project").hide();
                $("#share_service_project_loading").show();

                var formData = {
                    emails: $("#share_emails_sp").val(),
                    comments: $('#comments_sp').val(),
                    serviceid: $("#service_project_id").val()
                };

                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    success: function (data) {
                        $("#share_service_project_loading").hide();
                        $('.success-first').show();
                        $('.hide-email-comment').hide()
                        $("#share_emails_sp").val('');
                        $('#share_service_project').hide()
                        $('#close_share_service_project_hide').show()
                        $('#comments_sp').val('');
                        $('.modal-content .modal-footer .wrapper-link')
                        $('.top-50').css('margin-top', '50px')
                    },
                    error: function (data) {
                        $("#share_service_project").show();
                        $("#share_service_project_loading").hide();
                        $('.wrap_input_email').addClass('has-error');
                        $('.text-error').show();
                        console.log('Error:', data);
                    }
                });
            } else {
                $('.wrap_input_email').addClass('has-error');
                $('.text-error').show();
            }
        });
        $('#close_share_service_project_hide').on('click', function () {
            $('#myServiceProject').modal('hide')
        });


        CKEDITOR.replace('description', {
            height: 200,
            removeButtons: '',
            extraPlugins: 'colorbutton,colordialog'
        });

        CKEDITOR.replace('outcome', {
            height: 200,
            removeButtons: '',
            extraPlugins: 'colorbutton,colordialog'
        });

    </script>
@endsection