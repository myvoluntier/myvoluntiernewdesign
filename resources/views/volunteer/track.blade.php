@extends('layout.masterForAuthUser')
@section('css')
    <link href="<?=asset('css/plugins/fullcalendar/fullcalendar.css')?>" rel="stylesheet">
    <link href="<?=asset('css/plugins/fullcalendar/fullcalendar.print.css')?>" rel='stylesheet' media='print'>
    <link rel="stylesheet" type="text/css" href="{{asset('/css/multi-select.css')}}">

    <style>

        @media screen and (max-width: 375px) {
            #calendar .fc-toolbar > div {
                display: table;
                margin: 5px auto;
                float: none;
            } 

            #calendar .fc-toolbar {
                position: relative;
                padding-bottom: 80px;
            }

            .fc-toolbar .fc-right, .fc-toolbar .fc-center {
                position: absolute;
                left: 50%;
                transform: translateX(-50%);
            }

            .fc-toolbar .fc-right {

                top: 80px;
            }

            .fc-toolbar .fc-center {
                top: 40px;
            }
        }

       @media screen and (max-width: 1024px){
            .modal-share-px {
                width: calc(100% - 270px) !important;
            }
        }

        .modal {
            overflow-x: hidden !important;
            overflow-y: auto !important;
        }

        .modal-open .modal {
            overflow-x: hidden !important;
            overflow-y: auto !important;
        }

        #calendar{
            overflow: auto;
        }

        .wrapper-calendar{
            overflow: auto;
        }
        .p_invalid {
            display: none;
            font-size: 15px;
            font-weight: bold;
            margin-top: 10px;
            margin-bottom: 3px;
            color: red !important;
            margin-left: 20%;
        }

         .fc-time-grid-event .fc-time, .fc-time-grid-event .fc-title, .fc-end-resizer {
             color: #fff !important;
         }

        /*.wrapper-sort-table .table tr td:first-child {*/
            /*white-space: normal !important;*/
            /*word-wrap: break-word !important;*/
            /*max-width: 550px;*/

        /*}*/


        @media screen and (max-width: 524px){
            .wrapper-sort-table .table tr td:first-child{
                width: 100%;
                white-space: nowrap !important;
                word-wrap: normal !important;
                max-width: 100%;
            }
        }


        a {
            text-decoration: none !important;
        }
        .pagination .flex-wrap .justify-content-center{
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .footable-page-arrow, .footable-page{
            display: list-item;
            text-align: -webkit-match-parent;
        }

        .footable-page-arrow a, .footable-page a{
            position: relative;
            display: block;
            padding: 10px;
            margin: 10px 5px;
            font-size: 16px;
            line-height: 20px;
            font-family: 'Open Sans',sans-serif;
            color: #5f5f5f;
            background: none;
            border: 0 none;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            text-decoration: none;
            cursor: auto;
            outline: none;
            z-index: auto;
        }

        .footable-page-arrow a:hover, .footable-page a:hover{
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }

        .footable-page-arrow a[data-page=prev], .footable-page-arrow a[data-page=next], .footable-page-arrow.disabled{
            display: none;
        }

        .footable-page.active a{
            z-index: auto;
            color: #fff;
            text-decoration: none;
            background-color: #38b348;
            border: 0 none;
        }

        .fc-unthemed .fc-today {
            background: #c9fff4 !important;
        }

        .time-selector .select2 {
            display: none !important;
        }
        #hourError{float:right;}

        .row-centered {
            text-align:center;
        }

    </style>

@endsection

@section('content')
    @if(if_route_pattern('volunteer-track'))
        <div class="voluntier_track_blk">
        <div class="wrapper-track">
            <div class="page_head-box green_bg_blk">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <h2 class="h2 text-center">Track</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper-tablist new-track-hours-tab">
                <div class="container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#hours" role="tab"
                               data-toggle="tab"><span>Add Hours</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#activities" role="tab"
                               data-toggle="tab"><span>My Activities</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#approvals" role="tab"
                               data-toggle="tab"><span>Pending Approvals</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    @endif

    <div class="wrapper-friends">
        <div class="wrapper-friends-list wrapper-track pt-0">
            <div class="container">
               <!-- Start CalendeHours (main) tab --> 
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="hours">

                        <!-- Start Calender (second) tab -->
                        <div class="new-track-calender-tab">
                            <div class="container">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active show" id="calenderTab" href="#calender" role="tab"
                                        data-toggle="tab"><span>Work Calender</span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#opportunity" id="opporTab" role="tab"
                                        data-toggle="tab"><span>Join Opportunities</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active show" id="calender">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="work-calendar-box">
                                        <div class="main-text">
                                            <h2 class="h2 text-center">My Work Calendar</h2>
                                        </div>
                                        <div class="main-text new-track-cal">
                                            <div class="wrapper-calendar">
                                                <div id="calendar"></div>
                                                <a href="" download id="download_ics_file"></a>
                                            </div>
                                            <input type="hidden" id="selected_date">
                                            <input type="hidden" id="is_edit" value="0">
                                            <input type="hidden" id="track_id" value="0">
                                            <input type="hidden" id="is_from_addhour" value="0">
                                            <input type="hidden" id="is_no_org" value="0">
                                            <input type="hidden" id="is_link_exist" value="0">
                                        </div>
                                    </div>
                                    <div class="help-box">
                                        <div class="main-text">
                                            <h3 class="h3">How to Add Hours?</h3>

                                            <p>It's easy!</p>

                                            <p>1. Click on the calendar for the pop up box</p>
                                            <p>2. Select the opportunity</p>
                                            <p>3. Input time range</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade new-opportunity-tab-data" id="opportunity">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="main-text">
                                        <div class="row flex-container">
                                            <div class="">
                                                <a href="#" class="add_opportunity_plus"><span  class="fa fa-plus"></span></a>
                                            </div>
                                            <div class="">
                                                <h2 class="h2">Join Opportunities</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="link-box">
                                        <ul id='external-events' class="block_section_blk">
                                            @foreach($oppr as $op)
                                                @if($op->type == 1)
                                                    <li class='external-event navy-bg' value="{{$op->id}}"><a
                                                        href="{{url('volunteer/view_opportunity/'.$op->id)}}"><span>{{$op->title}}</span></a></li>

                                                @else
                                                    <li class='external-event private-bg'  value="{{$op->id}}"><a
                                                        href="{{url('volunteer/view_opportunity/'.$op->id)}}"><span>{{$op->title}}</span></a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div style="padding-top:20px">
                                    {{$oppr->fragment('opportunity')}}
                                </div>
                              </div>
                            </div>

                        </div>       
                        <!-- End Calender (second) tab -->

                        <!-- <div class="row row-padding">
                            <div class="col-12 col-md-4">

                                <div class="main-text">
                                    <div class="row flex-container">
                                        <div class="col">
                                            <h2 class="h2">Opportunities</h2>
                                        </div>
                                        <div class="col col-auto">
                                            <a href="#" class="add_opportunity_plus"><span  class="fa fa-plus"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="link-box">
                                    <ul id='external-events'>
                                        @foreach($oppr as $op)
                                            @if($op->type == 1)
                                                <li class='external-event navy-bg' value="{{$op->id}}"><a
                                                            href="{{url('volunteer/view_opportunity/'.$op->id)}}"><span>{{$op->title}}</span></a></li>

                                            @else
                                                <li class='external-event private-bg'  value="{{$op->id}}"><a style="background: #ff7a39"
                                                            href="{{url('volunteer/view_opportunity/'.$op->id)}}"><span>{{$op->title}}</span></a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                                <div style="padding-top:20px">
                                   {{$oppr->render()}}
                               </div>
                                

                            </div>
                            <div class="col-12 col-md-8 border-left">
                                <div class="work-calendar-box">
                                    <div class="main-text">
                                        <h2 class="h2">My Work Calendar</h2>
                                    </div>
                                    <hr>
                                    <div class="main-text">
                                        <div class="wrapper-calendar">
                                            <div id="calendar"></div>
                                            <a href="" download id="download_ics_file"></a>
                                        </div>
                                        <input type="hidden" id="selected_date">
                                        <input type="hidden" id="is_edit" value="0">
                                        <input type="hidden" id="track_id" value="0">
                                        <input type="hidden" id="is_from_addhour" value="0">
                                        <input type="hidden" id="is_no_org" value="0">
                                        <input type="hidden" id="is_link_exist" value="0">
                                    </div>
                                </div>
                            </div>
                        </div> -->

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="activities">
                        <div class="wrapper-track">
                            <div class="wrapper-tablist pt-0">

                                <div class="row justify-content-center">
                                    <div class="col-12 col-sm-6 col-md-4">
                                        <div class="wrapper_select">
                                            <select class="custom-dropdown" id="action_range">
                                                <option value="30">Last Month</option>
                                                <option value="7" selected>Last Week</option>
                                                <option value="1">Yesterday</option>
                                                <option value="0">Today</option>
                                            </select>
                                            {{--<table class="table_activity table table-stripped" data-page-size="10" data-filter=#filter>--}}
                                            {{--<tbody class="track-activity-panel">--}}
                                            {{--</tbody>--}}
                                            {{--</table>--}}
                                            {{--<div id="active_pager">--}}
                                            {{--<ul id="activity_pagination" class="pagination pull-right"></ul>--}}
                                            {{--</div>--}}
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="wrapper-sort-table">
                            <div class="container-activity-table">
                            @include('volunteer.trackActivitiesList', ['activity' => $activity])
                            </div>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="approvals">
                        <div class="wrapper-friends-list wrapper-track pt-0">
                            <div class="search-friends">                                
                                    <div class="search">
                                        <label><span class="gl-icon-search"></span></label>
                                        <input id="filter" type="text" onkeyup="searchName()" placeholder="Search">
                                    </div>                                
                            </div>
                        </div>

                        <div class="wrapper-sort-table">
                            <div>
                                @if(if_route_pattern('volunteer-track'))
                                    @include('components.trackProfile.pendingApprovalsActivity', ['tracks' => $tracks])
                                    <div class="pending_unlisted_org">
                                        <h5 class="">Pending Unlisted Organizations</h5>
                                    
                                    <table class="table bordered table_my_blk">
                                        <thead>
                                        <tr>
                                            <th>
                                                <div class="main-text"><p>Organization Name</p></div>
                                            </th>
                                            <th>
                                                <div class="main-text"><p>Organization Email</p></div>
                                            </th>
                                            <th>
                                                <div class="main-text"><p>Opportunity Name</p></div>
                                            </th>
                                            <th>
                                                <div class="main-text"><p>Logged Minutes</p></div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach(@$unlistTrackOrgs as $tr)
                                            <tr>
                                                <td data-label="Organization Name">
                                                    <div class="main-text"><p class="light">{{$tr->org_name}}</p></div>
                                                </td>
                                                <td data-label="Organization Email">
                                                    <div class="main-text"><p class="light">{{$tr->org_email}}</p></div>
                                                </td>
                                                <td data-label="Opportunity Name">
                                                    <div class="main-text"><p class="light">{{$tr->oppor_name}}</p></div>
                                                </td>
                                                <td data-label="Logged Minutes">
                                                    <div class="main-text"><p class="light">{{$tr->logged_mins}}</p></div>
                                                </td>
                                            </tr>
                                        @endforeach
        
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                    </div>
                                @endif
                                
                                {{-- <div id="peending_pager"> --}}
                                    {{-- <ul id="pendding_pagination" class="pagination pull-right">{!! $tracks->render() !!}</ul> --}}
                                {{-- </div> --}}

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    </div>

    @include('components.auth.modal_pop_up_add_opportunities')
    @include('components.auth.modal_pop_up_add_hours_vol' )
    @include('components.auth.modal_pop_up_unavailable')
@endsection

@section('script')
    <script src="<?=asset('js/plugins/dataTables/jquery.dataTables.js')?>"></script>
    <script src="{{asset('front-end/js/moment.min.js')}}"></script>
    <script src="<?=asset('js/plugins/fullcalendar/moment.min.js')?>"></script>
    <script src="<?=asset('js/plugins/fullcalendar/fullcalendar.min.js')?>"></script>

    <!--script src="<?=asset('js/plugins/datapicker/bootstrap-datepicker.js')?>"></script>
    <script src="<?=asset('front-end/js/bootstrap-datepicker.js')?>"></script-->

    <script src="<?=asset('js/jquery-ui.custom.min.js')?>"></script>
    <script src="<?=asset('js/plugins/select2/select2.full.min.js')?>"></script>
    <!--<script src="<?=asset('js/plugins/footable/footable.all.min.js')?>"></script>-->
    <!--script src="<?=asset('js/plugins/paginate/paginate.js')?>"></script>
    <script src="<?=asset('js/plugins/paginate/jquery-asPaginator.js')?>"></script>
    <script src="<?=asset('js/plugins/footable/footable.all.min.js')?>"></script-->
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>--}}

    <script src="{{asset('js/jquery.multi-select.js')}}"></script>
    <script src="{{asset('front-end/js/jquery-ui.js')}}"></script>
    <script src="{{asset('js/plugins/qrcode/jquery.qrcode.min.js')}}"></script>

    <script>
    $("#calenderTab").click(function(){
    location.replace(location.origin+location.pathname+"#calender");   
    $("#calenderTab").addClass('active');
    $("#calender").addClass('active show');
    $("#opporTab").removeClass('active');
    $("#opportunity").removeClass('active show');
    });
    $("#opporTab").click(function(){
    location.replace(location.origin+location.pathname+"#opportunity");   
    $("#opporTab").addClass('active');
        $("#calenderTab").removeClass('active');
        $("#calender").removeClass('active show');
        $("#opportunity").addClass('active show');
    });
    
    $( document ).ready(function() {
  if(window.location.href.indexOf("opportunity")>1){
        $("#opporTab").addClass('active');
        $("#calenderTab").removeClass('active');
        $("#calender").removeClass('active show');
        $("#opportunity").addClass('active show');
    }else{
        $("#opporTab").removeClass('active');
        $("#calenderTab").addClass('active');
        $("#calender").addClass('active show');
        $("#opportunity").removeClass('active show');

     }

    });

        function searchName() {
            // Declare variables
            var input, filter, table, tr, td, i;
            input = document.getElementById("filter");
            filter = input.value.toUpperCase();
            table = document.getElementById("tbl_confirm_tracking");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }

        $('#time_block').multiSelect({
            afterSelect: function(values){
                var mins = $('#time_block').val().length * 30;
                $('#hours_mins').text("(" + mins + "mins)");
//                alert("Select value: "+values);
            },
            afterDeselect: function(values){
                var mins = $('#time_block').val().length * 30;
                $('#hours_mins').text("(" + mins + "mins)");
//                alert("Deselect value: "+values);
            }
        });


        $('#opp_id').on('change', function() {
            var opp_id = this.value.toString();

            var url = API_URL + 'volunteer/track/getOpportunityInfo';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "post";
            var formData = {
                opp_id: opp_id
            };

            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {
//                    var contentSeletString = '';
//                    contentSeletString = '<select id="time_block" multiple="multiple">';
//                    contentSeletString +='</select>';
//                    $('#time_block').replaceWith(contentSeletString);
                }
            });
        });

        function activityChange() {
            var range = $('#action_range').val();
            var url = API_URL + 'volunteer/track/activityView';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var type = "get";
            var formData = { range: range };

            $.ajax({
                url: url,
                type: type,
                data: formData,
                success: function (data) {
                    $('#activity_table_del').remove()
                    $('.pag').remove()
                    $('.conteiner-activity-table').append(data)
                }
            });
        }

    </script>

    <script src="{{asset('js/tracking-action.js')}}"></script>

    <script>

        $(document).ready(function () {

            $('#showQrCodeId').on('click', function () {

                let track_id = $("#ics_download_value").val();
                let url = "{{route('get-vol-track-detail')}}";

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let type = "get";
                let formData = {track_id: track_id};

                $.ajax({
                    url: url,
                    type: type,
                    data: formData,

                    success: function (response) {
                       if(response){
                           $('#track_id').val(response['id']);
                           $('#qr_image').attr('src',response['image']);
                           $('#qr_name').text(response['name']);
                           $('#qr_hours').text(response['hours']);
                           $('#qr_org_name').text(response['org_name']);
                           $('#qr_opp_name').text(response['opportunity_name']);
                           $('#qr_date').text(response['date']);
                           $('#qr_time_range').text(response['time_range']);

                           delete response.image; // image will be gone
                           $("#qrCodeVol").html("");
                           $('#qrCodeVol').qrcode({width: 85,height: 85,text: JSON.stringify([response['id'] ,response['name'] ,response['hours'] ,response['date'] ,response['time_range']])});

                       }
                       else
                           toastr.error("Something wrong with your data please contact admin.", "Error");
                    }
                });

            });

            $('#is_designated').on('click', function () {

                if ($(this).is(':checked')) {
                    $('#is_designated').val(1);
                    $("#designated_group_id").show();
                }
                else {
                    $('#is_designated').val(0);
                    $("#designated_group_id").hide();
                }
            });

            var display = $('.footable-page-arrow:nth-last-child(3)').css('display');

            if (display == 'none') {
                $('.footable-page-arrow:last-child').css('display', 'none')
            }
            else {
                $('.footable-page-arrow:last-child').css('display', 'block')
            }

            if (parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
            else $('.footable-page-arrow:last').show();

            $(document).on('click', '.footable-page, .footable-page-arrow', function () {

                var display = $('.footable-page-arrow:nth-child(3)').css('display');

                if (display == 'none') {
                    $('.footable-page-arrow:first-child').css('display', 'none')
                }
                else {
                    $('.footable-page-arrow:first-child').css('display', 'block')
                }

                if (parseInt($('.footable-page:last a').data('page')) + 1 == $('.footable-page-arrow:last').text()) $('.footable-page-arrow:last').hide();
                else $('.footable-page-arrow:last').show();
            });

            $('.modal').css({'position': 'fixed', 'overflow': 'hidden'});

            activityChange();

            function activityChange() {
                var range = $('#action_range').val();
                var url = API_URL + 'volunteer/track/activityView';

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var type = "get";
                var formData = {range: range};

                $.ajax({
                    url: url,
                    type: type,
                    data: formData,
                    success: function (data) {
                        $('#activity_table_del').remove();
                        $('.pag').remove();
                        $('.conteiner-activity-table').append(data)
                    }
                });

                var $outerwidth = $('.row-header header .outer-width-box');
                var $innerwidth = $('.row-header header .inner-width-box');

                function checkWidth() {

                    var outersize = $outerwidth.width();
                    var innersize = $innerwidth.width();

                    if (innersize > outersize) {
                        $('body').addClass("navmobile");
                    } else {
                        $('body').removeClass("navmobile");
                        $('body').removeClass("offcanvas-menu-show");
                    }
                }

                checkWidth();
                $(window).resize(checkWidth);

                $('.offcanvas-menu-backdrop').on('click', function (e) {
                    $('body').toggleClass("offcanvas-menu-show");
                    e.preventDefault();
                });

                $('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
                    $('body').toggleClass("offcanvas-menu-show");
                    e.preventDefault();
                });
            }


            $('body').on('click', '.pag > .pagination > .page-item > .page-link', function (e) {
                e.preventDefault()
                if (!($(this).parent().hasClass('active'))) {
                    var range = $('#action_range').val();
                    var url = $(this).attr('href');

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var type = "get";
                    var formData = {range: range};

                    $.ajax({
                        url: url,
                        type: type,
                        data: formData,
                        success: function (data) {
                            $('#activity_table_del').remove();
                            $('.pag').remove();
                            $('.conteiner-activity-table').append(data)
                        }
                    });
                }

            })

            $('#action_range').change(function () {
                activityChange()
            });

            /*$('.footable').footable({
                limitNavigation: 5,
                firstText: '1'
            });*/

            $('#add_on_addtime').on('click', function () {

                $('#is_from_addhour').val(1);
                $(".select_organization").select2('val', '');
                $('#org_emails').val('');
                $('.org_email_div').hide();
                $('.opp_div').hide();
                $('.opp_private_div').hide();
                $('#opp_date_div').hide();
                $('#org_not_exist').attr('checked', false);
                $('#opp_not_exist').attr('checked', false);
                $('#org_name').attr('disabled', false);
                $('#opp_name').attr('disabled', false);
                $('#private_opp_name').val('');
                $('#end_date').val('');
            });

            $('.add_opportunity_dlg').on('click', function () {

                $('#is_from_addhour').val(0);
                $('#is_no_org').val(0);
                $(".select_organization").select2('val', '');
                $('#org_emails').val('');
                $('.private_opp_div').hide();
                $('.org_email_div').hide();
                $('.opp_div').hide();
                $('.opp_private_div').hide();
                $('#opp_date_div').hide();
                $('#org_not_exist').attr('checked', false);
                $('#opp_not_exist').attr('checked', false);
                $('#org_name').attr('disabled', false);
                $('#opp_name').attr('disabled', false);
            });


            $(".add_opportunity_plus").click(function () {
                $('select').select2("enable");
                $('#org_name').prop('selectedIndex', 0);
                $('#select2-org_name-container').text('Select an Organization')
                $('#opportunity_exist_alert').hide()
                $('#unlist_org_name').val('')
                $('#opp_date_div').hide()
                // $('#checkbox_div').show()
                $('.org_does_exist').show()
                $('#private_opp_name').val('')
                $('#org_not_exist').click()
                $('#wrapper_div').hide()
                $('#is_from_addhour').val(0);
                $('#is_no_org').val(0);
                $('.org_not_exist').show()
                $('.org_private_div').hide()
                $('.opp_private_div').hide()
                $('.opp_div').hide()
                $('#org_emails').val('')
                $('#end_date').val('')
                $('#checkbox_div').prop('checked', false);
                $('#opp_not_exist').prop('checked', false);

                $('#add_opportunity').modal('show');
            });

            function doSubmit(track_id) {

                $("#add_hours").modal('hide');
                var selected_val = $('#selected_date').val();
                var date_val = selected_val.slice(0, 11);
                var ext_val = selected_val.slice(16);

                var start_time = $('#start_time').val();
                var end_time = $('#end_time').val();
                var start_val = date_val.concat(start_time, ext_val);
                var end_val = date_val.concat(end_time, ext_val);
                var comments = $('#adding_hours_comments').val();

                var is_no_org = $('#is_no_org').val();
                var title = $('#opp_id option:selected').text();

                if (is_no_org == 1) {
                    title = $('#private_opp_name_hours').val();
                }
                if ($('#is_edit').val() == 1) {

                    $('#calendar').fullCalendar('removeEvents', track_id);
                    $("#calendar").fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start_val,
                            end: end_val,
                            opp_id: $('#opp_id').val(),
                            id: track_id,
                            comments: comments
                        },
                    true);

                } else {
                    $("#calendar").fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start_val,
                            end: end_val,
                            opp_id: $('#opp_id').val(),
                            id: track_id,
                            comments: comments
                        },
                    true);
                }
            }

            $('#btn_add_hours').on('click', function (e) {
                $(".ms-selection").css("border", "");
                $('#dayError,#hourError').hide();
                e.preventDefault();

                var selected_date = $('#selected_date_for_unlist').val();
                selected_date = selected_date.slice(0, 10);

                var opp_id = $('#opp_id').val();
                var is_designated = $('#is_designated').val();
                var designated_group_id = $('#designated_group_id_select').val();

                var opp_name = $('#opp_id option:selected').text();
                var logged_mins = $('#hours_mins').text();
                logged_mins = (logged_mins.slice(1)).slice(0, -5);

                var adding_hours_comments = $('#adding_hours_comments').val();
                var tracking_id = $('#track_id').val();
                var is_edited = $('#is_edit').val();
                var is_no_org = $('#is_no_org').val();
                var timeval = $('#time_block').val().toString().split(',');
                var start_time = timeval[0];
                var end_time = timeval[timeval.length - 1];


                end_time = addMinutesToTime(end_time, 30);
                var time_block = $('#time_block').val().toString();
                if (is_no_org == 1) {
                    opp_id = 'private';
                }
                var private_opp_name = $('#private_opp_name_hours').val();
                var unlist_org_name = $('#unlist_org_name_hours').val();
                var unlist_org_email = $('#unlist_org_mail_hours').val();

                $('.opp_select_div .select2').css('border', '');
                $('.select2', '#designated_group_id').css('border', '');
                $('.ms-list', '#ms-time_block').css('border', '');
                $('#selected_date_for_unlist').css('border', '');

                var org_email = $('#org_emails').val();
                if ($("#opp_id").val() == "Select an opportunity" && private_opp_name==null) {
                    $('.opp_select_div .select2').css('border', '1px solid red');
                    toastr.error("Please select any Opportunity.", "Error");
                    return;
                }

                if (is_designated == "1" && designated_group_id == "") {
                    $('.select2', '#designated_group_id').css('border', '1px solid red');
                    toastr.error("Please select any group first.", "Error");
                    return;
                }

                if ($('#time_block').val() == "") {
                    $('.ms-list', '#ms-time_block').css('border', '1px solid red');
                    toastr.error("Please select time blocks.", "Error");
                    return;
                }
                if ($('#selected_date_for_unlist').val() == "") {
                    $('#selected_date_for_unlist').css('border', '1px solid red');
                    toastr.error("Please select date you want add hours on.", "Error");
                    return;
                }
                var restDay = 0;
                if(unlist_org_name == ""){
                    restDay = getValidatedTrackDay('0');
                }

                if (restDay == 0) {

                    $('#btn_add_hours').prop('disabled', true);
                    var url = API_URL + 'volunteer/track/addHours';

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var type = "post";
                    var formData = {
                        opp_id: opp_id,
                        is_designated: is_designated,
                        designated_group_id: designated_group_id,
                        opp_name: opp_name,
                        start_time: start_time,
                        end_time: end_time,
                        logged_mins: logged_mins,
                        selected_date: selected_date,
                        comments: adding_hours_comments,
                        is_edit: is_edited,
                        tracking_id: tracking_id,
                        time_block: time_block,
                        is_no_org: is_no_org,
                        private_opp_name: private_opp_name,
                        unlist_org_name: unlist_org_name,
                        unlist_org_email: unlist_org_email,
                        org_email: org_email
                    };

                    $('#btn_add_hours').text('Please Wait..!');

                    $.ajax({
                        type: type,
                        url: url,
                        data: formData,
                        success: function (data) {
                            // location.reload();
                            // if (data.result == 'approved track') {
                            // $('#btn_add_hours').text('Save');

                            if (data.result == 'unlist sent') {
                                toastr.success('Unlisted Opportunity request sent to organization.', "Message");
                            }

                            else if (data.result == 'approved track') {
                                $('.confirmed_track').show();
                                toastr.success('Approved Tracking Hours.', "Message");
                            } else if (data.result == 'declined track') {
                                toastr.success('Declined Tracking Hours.', "Message");
                            } else {
                                if (data.opp_logo == null) {
                                    var logo = SITE_URL + 'front-end/img/org/001.png';
                                } else {
                                    var logo = SITE_URL + 'uploads/' + data.opp_logo;
                                }
                                var currentdate = new Date();
                                var current_time = currentdate.getFullYear() + '-' + (currentdate.getMonth() + 1) + '-' + currentdate.getDate() + ' ' + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
                                var current_date = currentdate.getFullYear() + '-' + (currentdate.getMonth() + 1) + '-' + currentdate.getDate();
                                var url = SITE_URL + 'volunteer/view_opportunity/' + opp_id;
                                if (data.is_link_exist == 1) {
                                    var link = '<a href="' + url + '"><strong>' + opp_name + '</strong></a>';
                                } else {
                                    if (is_no_org == 0) {
                                        var link = '<strong>' + opp_name + '</strong>';
                                    } else {
                                        var link = '<strong>' + private_opp_name + '</strong>';
                                    }
                                }
                                if (is_edited == 0) {

                                    $('.track-activity-panel').prepend($('<tr><td style="padding-left: 50px;"><img alt="image" class="img-circle" src="' + logo + '"> <i class="fa fa-reply"></i>You Added ' + logged_mins + 'mins on Opportunity ' + link + '</td><td>' + current_time + '</td></tr>'));

                                    $('.table_pending_view').prepend($('<tr class="pending-approval" id="pending' + tracking_id + '"><td style="text-align: left; padding-left: 20px"><img alt="image" class="img-circle" src="' + logo + '"> ' + link + '</td><td>' + selected_date + '</td><td>' + start_time + '</td><td>' + end_time + '</td><td>' + logged_mins + '</td><td>' + current_date + '</td><td class="label label-warning" style="display: table-cell; font-size:13px;">Pending</td></tr>'));
                                } else {

                                    $('.track-activity-panel').prepend($('<tr><td style="padding-left: 50px;"><img alt="image" class="img-circle" src="' + logo + '"> <i class="fa fa-reply"></i>You Updated Logged Hours to ' + logged_mins + 'mins on Opportunity ' + link + '</td><td>' + current_time + '</td></tr>'));
                                    $('#' + 'pending' + tracking_id).remove();
                                    $('.table_pending_view').prepend($('<tr class="pending-approval" id="pending' + tracking_id + '"><td style="text-align: left; padding-left: 20px"><img alt="image" class="img-circle" src="' + logo + '"> ' + link + '</td><td>' + selected_date + '</td><td>' + start_time + '</td><td>' + end_time + '</td><td>' + logged_mins + '</td><td>' + current_date + '</td><td class="label label-warning" style="display: table-cell; font-size:13px;">Pending</td></tr>'));
                                }

                                doSubmit(data.track_id);
                            }

                            location.reload();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                } else {
                    //$('#empty_opportunity_alert').show();
                }
            });

            var $outerwidth = $('.row-header header .outer-width-box');
            var $innerwidth = $('.row-header header .inner-width-box');

            // $('#action_range').change(function () {
            //     active_pagination();
            // });

            function checkWidth() {

                var outersize = $outerwidth.width();
                var innersize = $innerwidth.width();

                if (innersize > outersize) {
                    $('body').addClass("navmobile");
                } else {
                    $('body').removeClass("navmobile");
                    $('body').removeClass("offcanvas-menu-show");
                }
            }

            checkWidth();
            $(window).resize(checkWidth);

            $('.offcanvas-menu-backdrop').on('click', function (e) {
                $('body').toggleClass("offcanvas-menu-show");
                e.preventDefault();
            });

            $('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
                $('body').toggleClass("offcanvas-menu-show");
                e.preventDefault();
            });


            $("select").select2({
                theme: "bootstrap",
                minimumResultsForSearch: 1
            });

            /*$('.wrapper_input.fa-icons input').datepicker({
                'format': 'mm-dd-yyyy',
                'autoclose': true,
                'orientation': 'right',
                'todayHighlight': true         
            });*/
             $('#selected_date_for_unlist').datepicker({ dateFormat: 'mm-dd-yy' });
           // $('.wrapper_input.fa-icons input').datepicker( "option", "dateFormat", "mm-dd-yyyy");

            $("#checkbox_div").click(function () {
                console.log($(this).is(":checked"));
                if ($(this).is(":checked")) {
                    $('.org_email_div').show();
                    $('#wrapper_div').show();
                    $('.org_private_div').show();
                    $('select').select2("enable", false);
                    $('#opp_date_div').show()
                } else {
                    $('#opp_date_div').hide();
                    $('#wrapper_div').css("display", "none");
                    $('select').select2("enable");
                }
            });

            $('body').on('click', '#add_on_addtime', function () {
                $('#add_hours').modal('hide');
                setTimeout(function () {
                    $('.add_opportunity_plus').click()
                }, 200);
            });

            $('body').on('click', '.close_button', function () {
                $('.close').click();
            });

            function addMinutesToTime(time, minsAdd) {
                function z(n) {
                    return (n < 10 ? '0' : '') + n;
                }
                var bits = time.split(':');
                var mins = bits[0] * 60 + +bits[1] + +minsAdd;
                return z(mins % (24 * 60) / 60 | 0) + ':' + z(mins % 60);
            }

            $(".ms-selection").css("border", "");
            $('#dayError,#hourError').hide();

            function getValidatedTrackDay(flags) {

                var formData = {
                    addDate: $('#selected_date_for_unlist').val(),
                    oppId: $('#opp_id').val()
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: API_URL + 'volunteer/track/validatedTrackDate',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#dayError').hide();
                        $("#selected_date_for_unlist").css("border", "");
                        if (data != "success") {

                            $("#selected_date_for_unlist").css("border", "1px solid #ff0000");
                            $('#dayError').css('color', '#ff0000');
                            $('#dayError').show();
                            $('#dayError').html(data);
                            flags++;
                        } else {
                            flags = 0;
                        }
                    }
                });

                if ($('#time_block').val().length > 0 && flags == 0) {
                    var resultFlag = getValidatedMaxVolunteer('0');
                    if (resultFlag == '0') {
                        return getValidatedTrackHours('0');
                    } else {
                        return 1;
                    }
                }
                return flags;
            }

            function getValidatedTrackHours(flags) {

                var formData = {
                    addDate: $('#selected_date_for_unlist').val(),
                    oppId: $('#opp_id').val(),
                    time_block: $('#time_block').val().toString(),
                    track_id: $('#track_id').val(),
                    is_edit: $('#is_edit').val()
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: API_URL + 'volunteer/track/validatedTrackHours',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#hourError').hide();
                        $(".ms-selection").css("border", "");
                        if (data != "success") {

                            $(".ms-selection").css("border", "1px solid #ff0000");
                            $('#hourError').css('color', '#ff0000');
                            $('#hourError').show();
                            $('#hourError').html(data);
                            toastr.error("Selected time is overlapped.", "Error");
                            flags++;
                        } else {
                            flags = 0;
                        }

                    }
                });
                return flags;
            }

            function getValidatedMaxVolunteer(flags) {

                var formData = {
                    addDate: $('#selected_date_for_unlist').val(),
                    oppId: $('#opp_id').val(),
                    time_block: $('#time_block').val().toString(),
                    track_id: $('#track_id').val(),
                    is_edit: $('#is_edit').val()
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: API_URL + 'volunteer/track/validatedMaxVolunteer',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#hourError').hide();
                        $(".ms-selection").css("border", "");

                        if (data != "success") {

                            $(".ms-selection").css("border", "1px solid #ff0000");
                            $('#hourError').css('color', '#ff0000');
                            $('#hourError').show();
                            $('#hourError').html(data);
                            toastr.error(data, "Error");
                            flags++;
                        } else {
                            flags = 0;
                        }

                    }
                });
                return flags;
            }

            $('.fa-calendar').css('cursor', 'pointer');
            $('.fa-calendar').on('click', function () {
               
                $('#selected_date_for_unlist').datepicker();
                 $('#selected_date_for_unlist').datepicker( "option", "dateFormat", "mm-dd-yy");
            })

        });

        $(document).on('click', '.fc-more', function () {
            var top = $('.fc-more-popover').css("top");
            var number = top.substring(0, 3)
            var px = number + 'px';
            //console.log(top)
            //console.log(top.substring(0,3))
            $('.fc-more-popover').css("top", px);
        });

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        document.addEventListener('DOMContentLoaded', function () {

            var calendarEl = document.getElementById('calendar');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'GET',
                url: '{{route('volunteer-track-view-tracks')}}',
                success: function (data) {

                    $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },

                        defaultView: 'agendaWeek',
                        height: 700,
                        defaultDate: date,
                        disableResizing: true,
                        navLinks: true, // can click day/week names to navigate views
                        editable: false,
                        eventLimit: true, // allow "more" link when too many events
                        events: data,

                        eventClick: function (calEvent, jsEvent, view) {

                            //todo: Also add this option on pending hours page just add it into function and use in both cases click on that pending 1-272
                            refereshAddHours();
                            $('#opp_id').prop('disabled', false);
                            $('.opp_select_div').show();
                            $('.private_opp_div_add_hours').hide();
                            $('#private_opp_name_hours').val('');
                            $('#unlist_org_name_hours').val('');
                            $('#unlist_org_mail_hours').val('');
                            $('#unlist_opp_name').val('');
                            // alert('Event: ' + calEvent.title);
                            $('#is_edit').val(1);
                            $('#track_id').val(calEvent.id);
                            $('#btn_remove_hours').show();
                            $('#is_no_org').val(0);
                            $('#date_day').html(calEvent.logged_date_day);
                            $('#adding_hours_comments').val(calEvent.comments);

                            if (calEvent.approved == 1) {
                                $('#btn_remove_hours').hide();
                                $('#btn_add_hours').hide();
                                $('#time_block').prop('disabled', true);
                                $('#time_block_view').prop('disabled', true);
                                $('#opp_id').prop('disabled', true);
                                $('#adding_hours_comments').prop('disabled', true);

                            } else {

                                $('#btn_remove_hours').show();
                                $('#btn_add_hours').show();
                                $('#time_block').prop('disabled', false);
                                $('#time_block_view').prop('disabled', true);
                                $('#opp_id').prop('disabled', false);
                                $('#adding_hours_comments').prop('disabled', false);
                            }

                            $('#time_block option:selected').removeAttr("selected");
                            $('#time_block_view option:selected').removeAttr("selected");

                            var dataarray = calEvent.selected_blocks.split(",");
                            i = 0, size = dataarray.length;

                            for (i; i < size; i++) {
                                $("#time_block option[value='" + dataarray[i] + "']").attr("selected", 1);
                                $("#time_block_view option[value='" + dataarray[i] + "']").attr("selected", 1);
                            }

                            $('#time_block').multiSelect("refresh");
                            $('#time_block_view').multiSelect("refresh");
                            $('.private_opp_div').hide();
                            $('.opp_select_div').show();
                            $('.confirmed_track').hide();
                            $('.add_hours_modal_title').text("Do you want make Change?");

                            if (calEvent.opp_id == 0) {
                                $('.opp_select_div').hide();
                                $('.private_opp_div').show();
                                $('#private_opp_name_hours').val(calEvent.title);
                                $('#is_no_org').val(1);
                                $('.private_opp_div_add_hours').show();
                            }
                            if (calEvent.link == 1) {
                                $('#is_link_exist').val(1);
                            } else {
                                $('#is_link_exist').val(0);
                            }
                            var startDate = calEvent.start.format("YYYY-MM-DD HH:mm");
                            $('#selected_date').val(startDate.slice(0, 11));

                            if (calEvent.end == null) {
                                var new_date = startDate.slice(0, 11);
                                var new_hour = startDate.slice(11, 13);
                                var new_mins = startDate.slice(13);

                                new_hour = parseInt(new_hour) + 2;
                                if (new_hour < 10)
                                    new_hour = '0' + new_hour;

                                var endDate = new_date + new_hour + new_mins;
                            } else {
                                var endDate = calEvent.end.format("YYYY-MM-DD HH:mm");
                            }

                            var cur_start_val = startDate.slice(11, 16);

                            $('#start_time option').filter(function () {
                                return ($(this).val() == cur_start_val);
                            }).prop('selected', true);

                            var cur_end_val = endDate.slice(11, 16);

                            $('#end_time option').filter(function () {
                                return ($(this).val() == cur_end_val);
                            }).prop('selected', true);

                            $('#opp_id option').filter(function () {
                                return ($(this).val() == calEvent.opp_id);
                            }).prop('selected', true);

                            if (calEvent.is_designated == '1') {

                                $('#is_designated').val(1);
                                $("#designated_group_id").show();
                                $('#designated_group_id_select option').filter(function () {
                                    return ($(this).val() == calEvent.designated_group_id);
                                }).prop('selected', true);

                                $('#is_designated').prop('checked', true);
                            }

//                            var t = cur_start_val.slice(0, 2);
//                            t = parseInt(t);
//                            t = t + 1;
//                            if (t < 10) {
//                                var forward_time = ("0" + t + cur_start_val.slice(2));
//                            } else {
//                                var forward_time = (t + cur_start_val.slice(2));
//                            }
//                            if (cur_end_val < cur_start_val) {
//                                $('#end_time option').filter(function () {
//                                    return ($(this).val() == forward_time); //To select Blue
//                                }).prop('selected', true);
//                            } else {
//                                forward_time = cur_end_val;
//                            }
//                            var h = parseInt(forward_time.slice(0, 2)) - parseInt(cur_start_val.slice(0, 2));
//                            var m = parseInt(forward_time.slice(3)) - parseInt(cur_start_val.slice(3));
//                            if (m < 0) {
//                                h = h - 1;
//                                m = 30;
//                            }
//                            var mins = h * 60 + m;

                            var mins = size * 30;

                            // $('#hours').text("("+h+"hrs "+m+"mins)");
                            $('#hours_mins').text("(" + mins + "mins)");
                            console.log(calEvent.logged_date);
                            
                           // $('#selected_date_for_unlist').datepicker('setDate', calEvent.logged_date);
                            $('#selected_date_for_unlist').datepicker("setDate",calEvent.logged_date);

                            $('#comments').text('');
                            $("select").select2({
                                theme: "bootstrap",
                                minimumResultsForSearch: 1
                            });

                            $('#btn_add_hours').prop('disabled', false);

                            //set modal value for view
                            var title = $('#opp_id option:selected').text();

                            var href = 'view_opportunity/'+calEvent.opp_id; // get href of anchor
                            var betaHTML = "<a target='_blank' href='" + href + "'>" + title + "   <i class=\"fa fa-external-link\"></i></a>"; // Create anchor
                            $('#oppertunity_display').html(betaHTML); // Update beta element


                            $('#hours_mins_view').text("(" + mins + "mins)");
                            $('#comment_display').html(calEvent.comments);
                            // alert(calEvent.id);
                            $('#ics_download_value').val(calEvent.id);
                            $('#eitBtnHrs').show();

                            let status = '<span class="badge badge-secondary mt-2"><i class="fa fa-clock-o"></i> Pending</span>';

                            if(calEvent.approv_status == 1){
                                $('#eitBtnHrs').hide();
                                status = '<span class="badge badge-success mt-2"><i class="fa fa-check"></i> Approved</span>';
                            }
                            if(calEvent.approv_status == 2){
                                $('#eitBtnHrs').hide();
                                status = '<span class="badge badge-danger mt-2"><i class="fa fa-times"></i> Declined</span>';
                            }

                            $('#trackStatus').html(status);

                            // $('#add_hours').modal('show');
                            $('#view_track').modal('show');
                        },

                        dayClick: function (date, allDay, jsEvent, view) {
                            var currentdate = new Date();
                            // var str_cur_time = currentdate.getTime();
                            // var selected_time = new Date(start);
                            // var str_selected_time = selected_time.getTime();
                            $('#time_block').prop('disabled', false);
                            $('#opp_id').prop('disabled', false);
                            $('#adding_hours_comments').prop('disabled', false);

                            $('option', $('#time_block')).each(function (element) {
                                $(this).removeAttr('selected').prop('selected', false);
                            });
                            $("#time_block").multiSelect('refresh');

                            function formatDate(date) {
                                var d = new Date(date),
                                    month = '' + (d.getMonth() + 1),
                                    day = '' + d.getDate(),
                                    year = d.getFullYear();
                                if (month.length < 2) month = '0' + month;
                                if (day.length < 2) day = '0' + day;
                               // return [year, month, day].join('-');
                                return [month, day, year].join('-')
                            }

                            var newDate = date;
                            var j = new Date(newDate);
                            var t = j.getTime();

                            var f = new Date(currentdate);
                            var r = f.getTime();


                            var checkDate = t > r;
                            //console.log(t , r)
                            $('#selected_date_for_unlist').val(formatDate(date));
                            $('#selected_date_for_unlist').datepicker();
                            $('#selected_date_for_unlist').datepicker( "option", "dateFormat", "mm-dd-yy");

                            $('#is_designated').val('0');
                            if ($('#is_designated').is(':checked')) {
                                $('#is_designated').trigger('click');
                            }
                            $('#dayError,#hourError').hide();
                            $("#designated_group_id").hide();

                            if (checkDate) {
                                $('#unavailable').modal('show')
                            }

                            else {

                                $('#btn_remove_hours').hide();
                                $('#btn_add_hours').show();
                                $('#opp_id').prop('disabled', false);
                                $('.opp_select_div').show()
                                $('.private_opp_div_add_hours').hide();
                                $('#private_opp_name_hours').val('');
                                $('#unlist_org_name_hours').val('');
                                $('#unlist_org_mail_hours').val('');
                                $('#unlist_opp_name').val('');
                                
                                $('#is_edit').val(0);
                                $('#btn_remove_hours').hide();
                                $('.org_not_exist').show();
                                $('.private_opp_div').hide();
                                $('#is_no_org').val(0);
                                $('.opp_select_div').show();
                                $('.confirmed_track').hide();
                                $('.declined_track').hide();
                                $('#selected_date').val(date.format('YYYY-MM-DD'));

                                $('#start_time > option[value="' + date.format('HH:mm') + '"]').attr('selected', 'true').text(date.format('(hh:mm)a'));
                                $('#end_time > option[value="' + $('#start_time > option[value="' + date.format('HH:mm') + '"]').next().val() + '"]').attr('selected', 'true').text($('#start_time > option[value="' + date.format('HH:mm') + '"]').next().text());
                                // calendar.formatDate(date,'h-mm' )
                                $('#hours_mins').text('(0min)');
                                $('#opp_id option[value="Select an opportunity"]').prop('selected', true);
                                $('#select2-opp_id-container').html("Select an opportunity");
//                                $("#opp_id").select2({
//                                    theme: "bootstrap",
//                                    minimumResultsForSearch: 1
//                                });
                                // $("select option[value=" + val + "]").attr('selected', 'true').text(text);
                                $('#adding_hours_comments').val("");
                                $('#add_hours').modal('show');
                                // $('#view_track').modal('show');
                            }

                        },
                        viewDisplay: function (view) {
                            var now = new Date();
                            var end = new Date();
                            end.setMonth(now.getMonth() + 11); //Adjust as needed

                            var cal_date_string = view.start.getMonth() + '/' + view.start.getFullYear();
                            var cur_date_string = now.getMonth() + '/' + now.getFullYear();
                            var end_date_string = end.getMonth() + '/' + end.getFullYear();

                            if (cal_date_string == cur_date_string) {
                                jQuery('.fc-button-prev').addClass("fc-state-disabled");
                            }
                            else {
                                jQuery('.fc-button-prev').removeClass("fc-state-disabled");
                            }

                            if (end_date_string == cal_date_string) {
                                jQuery('.fc-button-next').addClass("fc-state-disabled");
                            }
                            else {
                                jQuery('.fc-button-next').removeClass("fc-state-disabled");
                            }
                        }
                    });

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        });

        //download ics file
        $('#ics_download').click(function () {
            var event_id = $("#ics_download_value").val();
            var url = "{{url('api/volunteer/track/downloadICSFile/')}}" + "/" + event_id;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (result) {
                    window.location.href = result;
                }
            });
        });

        $('#org_name').on('change', function () {
            $('#opp_date_div').hide();
            $('#invalid_org_name_alert').hide();

            var org_id = $(this).val();

            if (org_id != '') {
                var url = API_URL + 'volunteer/track/getOpportunities';

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'post',
                    url: url,
                    data: {org_id: org_id},
                    success: function (data) {

                        $('#opp_name').find('option').remove().end();

                        $('#opp_name').append($("<option></option>"));

                        $.each(data.oppor, function (index, value) {
                            $('#opp_name').append($("<option></option>")
                                .attr("value", value.id).text(value.title));
                        });

                        $("select").select2({
                            theme: "bootstrap",
                            minimumResultsForSearch: 0
                        });

                        $('.org_does_exist').hide();
                        $('.opp_div').show();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
        function refereshAddHours(){
            $('#time_block').prop('disabled', false);
            $('option', $('#time_block')).each(function (element) {
                $(this).removeAttr('selected').prop('selected', false);
            });
            $("#time_block").multiSelect('refresh');
            $('#hours_mins').text('(0min)');
            $('#adding_hours_comments').val('');
            $('#selected_date_for_unlist').val('<?php echo date("m-d-Y"); ?>');
            $('#selected_date_for_unlist').datepicker();
            $('#selected_date_for_unlist').datepicker( "option", "dateFormat", "mm-dd-yy");
            $('#is_designated').val('0');
            if ($('#is_designated').is(':checked')) {
                $('#is_designated').trigger('click');
            }
            $('#dayError,#hourError').hide();
            $("#designated_group_id").hide();
        }
    </script>

@endsection