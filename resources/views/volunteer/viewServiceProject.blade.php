@extends('layout.masterForAuthUser')
@section('css')
<link rel="stylesheet" href="{{asset('front-end/css/service-project.css')}}">
<link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">
@endsection
@section('content')
<div class="wrapper-create-edit-group">
    <div  id="print_logo" style="display:none;">
        <Img src="{{asset('front-end/img/logo.jpg')}}" style="margin-bottom: 10px">
    </div>
    <div class="container" id="service_project_box">
            <div class="main-text border-bottom">
                <h2 class="h2">View Service Project <span class="hoursSpan">Total Hours : {{ $serviceProject->total_hours }}</span></h2>
                <p class="light">You can view a Service Project right now. </p>
            </div>
            @include('components.shareServiceProjectInfo')

            <div class="wrapper_row_link">
                <div class="wrapper-link two-link margin-top">
                    <a href="{{url('/volunteer/profile#serviceProject') }}" class="red"><span>Back</span></a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" id="share_service_project_modal" name="filter" class="btn btn-success" style="float:right">Share</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" name="filter_print" onclick="printServiceProject()" class="btn btn-info" style="float:right; margin-right: 10px;">Print</a>
                </div>
            </div>

            @if($service_project_id != null)
                <input type="hidden" name="service_project_id" id="service_project_id" value="{{$service_project_id}}">
            @endif

    </div>
</div>

@include('components.shareServiceProject')
@endsection

@section('script')
<script>
    function printServiceProject()
    {
        window.print();
    }
</script>
<script>
           

            $('#btn_service_project').on('click', function (e) {
    e.preventDefault;
            $("input, textarea").css("border", "");
            var flags = 0;
            if ($("#title").val() == '') {
    $("#title").css("border", "1px solid #ff0000");
            flags++;
    }
    if ($("#start_date").val() == '') {
    $("#start_date").css("border", "1px solid #ff0000");
            flags++;
    }
    if ($("#end_date").val() == '') {
    $("#end_date").css("border", "1px solid #ff0000");
            flags++;
    }
    if ($("#description").val() == '') {
    $("#description").css("border", "1px solid #ff0000");
            flags++;
    }
    if ($("#outcome").val() == '') {
    $("#outcome").css("border", "1px solid #ff0000");
            flags++;
    }

    if (flags == 0) {
    inProgress();
            $('#post_service_project_form').submit();
    }
    else {
    toastr.error("Form data is invalid.", "Error");
    }
    });
            function inProgress() {
            $('#service_project_box').addClass("in-progress");
                    $('#btn_service_project_loading').show();
                    $('#btn_service_project').hide();
                    }

        $('#share_service_project_modal').on('click', function (e) {
            $('.success-first').hide();
                $("#share_emails_sp").val('');
                $('.hide-email-comment').show()
                $('#comments_sp').val('');
                $('#share_service_project').show();
                $('#close_share_service_project_hide').hide();
            $('#myServiceProject').modal('show');
        });
        $('#share_service_project').on('click', function () {
    var url = API_URL + 'share_service_project';
            $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            })


            var emailsVal = $("#share_emails_sp").val();
            // var checkEmail = emailsVal.replace(/^\s+|\s+$/g, '');

            var type = "POST";
            if ($("#share_emails_sp").val() !== '') {

        $('.wrap_input_email').removeClass('has-error');
        $('.text-error').hide();
        $("#share_service_project").hide();
        $("#share_service_project_loading").show();
        var formData = {
            emails: $("#share_emails_sp").val(),
            comments: $('#comments_sp').val(),
            serviceid: $("#service_project_id").val()
        }
        $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function (data) {
            $("#share_service_project_loading").hide();
                    $('.success-first').show();
                    $('.hide-email-comment').hide()
                    $("#share_emails_sp").val('');
                    $('#share_service_project').hide()
                    $('#close_share_service_project_hide').show()
                    $('#comments_sp').val('');
                    $('.modal-content .modal-footer .wrapper-link');
                    $('.top-50').css('margin-top', '50px');
            },
            error: function (data) {
            $("#share_service_project").show();
                    $("#share_service_project_loading").hide();
                    $('.wrap_input_email').addClass('has-error');
                    $('.text-error').show();
                    console.log('Error:', data);
            }
        });
    } else {
        $('.wrap_input_email').addClass('has-error');
        $('.text-error').show();
    }
    });
            $('#close_share_service_project_hide').on('click', function () {
    $('#myServiceProject').modal('hide')
            });
</script>
@endsection