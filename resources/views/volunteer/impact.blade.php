@extends('layout.masterForAuthUser')

@section('css')  

    <link rel="stylesheet" href="{{asset('front-end/css/star-rating.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">

    <style>
        a {
            text-decoration: none !important;
        }
        .star{
            cursor: default;
        }
        .rating-xs {
            font-size: inherit !important;
        }
        .print-header {
            display: none;
        }

    </style>

@endsection

@section('content')
    <div class="wrapper-impact">
    <!-- page heading start -->
        <div class="page_head-box green_bg_blk">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-8 col-lg-8">
                            <h2 class="h2">Impact</h2>
                            @if(session('admin_role')=='admin' && env('APP_ENV') == 'local' )
                                <a href="{{route('addTestVolImpactData' ,Auth::user()->id)}}"><i class="fa fa-refresh" aria-hidden="true"> Load Test Data</i></a>
                            @endif
                        </div>
                        <div class="col-12 col-md-4 col-lg-4 text-right print_btn_blk">
                            <a href="#" onclick="myFunction()" class="print"><span><i class="fa fa-print" aria-hidden="true"></i></span></a>
                        </div>
                    </div>
                </div>
            </div>
       
        <!-- page heading end -->

        <div class="container">
            <!-- <div class="row">
                <div class="col print-header">
                    <img src="{{asset('front-end/img/logo.png')}}">
                    <span>Impact</span>
                </div>
            </div> -->

            <!--<div class="row align-items-center border-bottom-0 page-header">
                 <div class="col">
                    <div class="main-text">
                        <h2 class="h2">Impact</h2>
                        @if(session('admin_role')=='admin' && env('APP_ENV') == 'local' )
                            <a href="{{route('addTestVolImpactData' ,Auth::user()->id)}}"><i class="fa fa-refresh" aria-hidden="true"> Load Test Data</i></a>
                        @endif
                    </div>
                </div> -->

                <!-- <div class="col col-auto">
                    <a href="#" onclick="myFunction()" class="print"><span><i class="fa fa-print" aria-hidden="true"></i></span></a>
                </div> 
            </div>-->

            <div class="row align-items-center margin_b30 margin_mob0">
                <div class="col-12 col-md-4">
                <h3 class="h3 text-center hed_second">Ranked</h3>
                    <div class="main-text rank_hour_blk">                        
                        <div class="rank_box_blk">                            
                            <p class="mb-0 text-center green middle"><strong class="big" id="my_ranking">1</strong></p>
                            <p class="light text-center">Your Ranking Among Friends</p>
                        </div>

                        <div class="hour_box_blk">
                            <div class="hour_txt_blk">
                                <p class="mb-0 text-center green big"><strong>{{$logged_mins}}</strong><span class="green small">HOUR(S)</span></p>
                                <!-- <p class="mb-0 text-center green middle">HOUR(S)</p> -->
                            </div>
                            <p class="light text-center">Your Total Tracked Hours</p>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-md-8 border-left">
                    <div id="track-hour-friend-chart" class="wrapper-svg"></div>
                </div>
            </div>

            <!-- <div class="row border-bottom-0">
                <div class="col">
                    <div class="main-text"><h3 class="h3 hed_second margin_tb20">Ranking &amp; Tracking Hours on Organizations</h3></div>
                </div>
            </div> -->

            <div class="row margin_b30 margin_mob0">
                <div class="col-12 col-md-4">
                    <div class="main-text">
                        <h3 class="h3 hed_third">Ranking Among Organization</h3>

                        <div class="wrapper-table-impact">
                            <div>
                                <table class="imapactPaginate">
                                    <thead>
                                    <tr>
                                        <th><p class="hed_txt">#</p></th>
                                        <th><p class="hed_txt">Organizationas</p></th>
                                        <th><p class="text-center hed_txt">Rank</p></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($org_ranking  as $index => $ranking)
                                        <tr>
                                            <td><p>{{++$index}}</p></td>
                                            <td><p>{{$ranking['org_name'] ?? 'n/a'}}</p></td>
                                            <td><p class="text-center">{{$ranking['my_ranking']}}</p></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4 border-left pie-chart-print">
                    <div id="ranking-chart-id" class="wrapper-svg"></div>
                </div>
                <div class="col-12 col-md-4 border-left pie-chart-print">
                    <div id="vol-hours-chart-id" class="wrapper-svg"></div>
                </div>
            </div>

        
        </div>
    </div>
@endsection

@section('script')

    <script src="{{asset('front-end/js/star-rating.js')}}"></script>
    <script src="{{asset('front-end/js/highcharts.js')}}"></script>
    <script src="//cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

    <script>

        $(document).ready(function() {
            $('.imapactPaginate').dataTable({
                                "paging": true,
                                "lengthChange": false,
                                "searching": false,
                                "ordering": true,
                                "info": false,
                                "autoWidth": true,
                                "pageLength": 5,
                                "pagingType": "full_numbers",
                                "language": {
                                    "paginate": {
                                        "first": '<<',
                                        "next": '>',
                                        "previous": '<',
                                        "last": '>>'
                                    }
                                }
                            });
            $('.kv-fa').rating({
                displayOnly: true,
                clearButton: '<i class="fa fa-minus-circle"></i>',
                filledStar: '<i class="fa fa-star"></i>',
                emptyStar: '<i class="fa fa-star-o"></i>'
            });

            viewFriendGraph();

           
        });
    </script>

    <script>

        function viewFriendGraph() {
            var url = '{{route('volunteer-get-friend-info')}}';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var type = "post";
            $.ajax({
                type: type,
                url: url,
                success: function (data) {
                    Highcharts.chart('track-hour-friend-chart', {

                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Tracked Hours of Your Friends',
                            style:{ "color": "#27282f", "fontSize": "24px",  "fontFamily": 'Open Sans, sans-serif' }
                        },
                        plotOptions: {
                            column: {
                                colorByPoint: true
                            }
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            type: 'category',
                            labels: {
                                style: {
                                    fontSize: '14px',
                                    fontWeight: 'normal',
                                    fontFamily: 'Open Sans, sans-serif',
                                    color: '#9ca0a1'
                                }
                            },
                            categories: data.friend_name
                        },
                        yAxis: {

                            title: {
                                text: ''
                            },
                            labels: {
                                style: {
                                    fontSize: '14px',
                                    fontWeight: 'normal',
                                    fontFamily: 'Open Sans, sans-serif',
                                    color: '#9ca0a1'
                                }
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        series: [{
                            name: 'Tracked Hours',
                            data: data.logged_hours

                        }]
                    });
         
                    $('#my_ranking').html(data.rank);

                    pieChartCode(data.org_hours ,'Tracked Hours' ,'<h4 class="hed_third_margin">Ranking Among Organization</h4>','ranking-chart-id');

                    pieChartCode(data.opprTypesHoursGraph ,'Tracked Hours' ,'Hours By Opportunity Type','vol-hours-chart-id');

                    // console.log(data.opprTypesHoursGraph);
                    // console.log(data.org_hours);
             
                },

                error: function (data) {
                    console.log('Error:', data);
                }

            });
         
        }

        function pieChartCode(data ,title ,heading ,chartId) {

          Highcharts.chart(chartId, {

                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: heading,
                    style:{ "color": "#27282f", "fontSize": "24px",  "fontFamily": 'Open Sans, sans-serif' }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || '#434348',
                                fontWeight:'normal',
                                fontSize:'14px',
                                fontFamily: 'Open Sans, sans-serif'
                            }
                        }
                    }
                },
                series: [{
                    name: title,
                    data: data

                }]
            });
        }

    </script>

    <script>

        function myFunction() {
            window.print();
        }

    </script>

@endsection