@extends('layout.masterForAuthUser')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/multiselect-styles.css')}}">

    <style>
        .flying-panel{
            display: none;
            margin-left: 30%;
            margin-top: 1%;
            width: 40%;
            z-index: 99;
        }

        .light{
            margin-top:0rem !important;
        }

        .custom{
            margin-bottom:0px;
        }

        .green{
            color:green;
        }

        @media screen and (max-width: 1024px){
            .flying-panel{
                display: none;
                margin-left: 20%;
                margin-top: 1%;
                width: 60%;
                z-index: 99;
            }
        }

        @media screen and (max-width: 724px){
            .flying-panel{
                display: none;
                margin-left: 10%;
                margin-top: 1%;
                width: 80%;
                z-index: 99;
            }
        }

        @media screen and (max-width: 524px){
            .flying-panel{
                display: none;
                margin-left: 3%;
                margin-top: 1%;
                width: 94%;
                z-index: 99;
            }
        }

        @media (max-width: 575.98px){
            .wrapper-search-map .wrapper-one-links .row > div:last-child {
                padding: 0;
            }
        }

        .flex-item{
            height:auto;
        }

        .user_place1, .user_place, .search_place, .keyword{
            flex: 1;
            height: 465px;
            overflow-y: scroll;
            color:black;
        }

        .alpha{
            background-color: rgb(248, 249, 250);
            font-family: 'Open Sans',sans-serif;
        }

        a{
            color:black;
        }

        #alpha1 {
            width: calc(100% - 52px);
            padding-left: 51px;
            margin-top: -21px;
        }

        ul.key_list {
            padding: 0;
            list-style: none;
            margin:  0;
            border-left:  1px solid #ccc;
            border-right:  1px solid #ccc;
            background:  #fff;
        }

        ul.key_list li:first-child {
            border-top: 1px solid #ccc;
        }

        ul.key_list li {
            border-bottom: 1px solid #ccc;
            font-size: 12px;
        }

        ul.key_list a {
            display:  block;
            text-decoration: none;
            padding: 5px 10px;
        }

        ul.key_list a:hover {
            background:  #eee;
            color: #000;
        }

        .clearfix {clear:both;}

        .wrapper-search-map .wrapper-one-links a.calendar {
            padding: 0;
        }

    </style>
@endsection

@section('content')
    <div class="row-content wrapper_map_frame">
        <div class="flex-item">
            <div class="wrapper-search-map opp_outer_blk">
                <!-- page heading start -->
                <div class="page_head-box green_bg_blk">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-12">
                                    <h2 class="h2 text-center">Opportunities</h2>                                
                                </div>
                            
                            </div>
                        </div>
                </div>
                <!-- page heading end -->
                <div class="container">
                        <div class="wrapper-one-links clearfix">
                            <div class="row">
                            <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label class="label-text">Enter Keyword:</label>
                                <div class="wrapper-two-links clearfix">
                                    <a href="#" id="btn_search_keyword" class="sitemap"><i class="fa fa-sitemap" aria-hidden="true"></i></a>
                                    <div class="wrapper_input">
                                        <input type="text" id="input_keyword_search" class="form-control" placeholder="Type opportunity keyword here" name="keyword" >
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="col-12 col-sm-6">
                             <div class="form-group">
                                <div class="main-text">
                                <label class="label-text">Please type the city and state name, e.g. New York, NY</label>
                                </div>
                                <div class="wrapper-two-links clearfix">
                                    <a href="#" id="btn_search_location" class="sitemap"><i class="fa fa-sitemap" aria-hidden="true"></i></a>
                                    <div class="wrapper_input">
                                        <input type="text" id="input_search_loc" class="form-control" placeholder="Type place name here" value="{{ isset($place) && $place != '' ? $place  : $search_addr['city'].', '.$search_addr['state']}}" name="place" >
                                        <p class="p_invalid hide" id="empty_location" style="color:red">Please select location.</p>
                                    </div>
                                </div>
                                </div>
                            </div>

                                <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label class="label-text">Opportunity Types:</label>
                                    <div class="wrapper_select">
                                        <select class="opp-search-checkbox form-control" id="opportunity_type_select" name="opportunity_type_select" multiple>
                                            @foreach($op_type as $key=>$ot)
                                                <option value="{{$key}}" class="oppr_li" id="opprtype{{$key}}" {{ isset($opportunity_type) && $opportunity_type == $key ? 'selected' : '' }}>{{$ot['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 removepaddiv">
                                <div class="form-group">
                                    <label class="label-text">Organization Types:</label>
                                    <div class="wrapper_select">
                                        <select class="org-search-checkbox form-control" id="organization_type_select" name="organization_type_select" multiple>
                                            @foreach($og_type as $key1=>$og)
                                                <option value="{{$key1}}" class="oppr_li" id="orgtype{{$key}}" {{ isset($organization_type) && $organization_type == $key1 ? 'selected' : '' }}>{{$og['name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>
                                </div>

                                <div class="col-9 col-sm-4 removepaddiv">
                                  <div class="form-group">
                                    <label class="label-text">Distance:</label>
                                    <div class="wrapper_select">
                                        <select class="form-control distance_radius_select" id="distance_radius" name="distance_radius">
                                            <!-- <option disabled>Select Location</option> -->
                                            <option value="5">5 miles</option>
                                            <option value="10">10 miles</option>
                                            <option value="25" selected>25 miles</option>
                                            <option value="50">50 miles</option>
                                            <option value="100">100 miles</option>
                                            <option value="200">200 miles</option>
                                        </select>
                                    </div>
                                   </div>
                                </div>

                                <div class="col-3 col-sm-2 col-xs-12 opportunitycal removepaddiv">
                                   <div class="form-group">
                                    <label class="label-text"></label>
                                    <div class="wrapper_select">
                                        <a href="#" class="calendar" data-toggle="dropdown"  title="Opportunuties" id="datepicker" style="position:relative;top:20px">
                                            <div class="wrapper-calendar">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </div>
                                            <div class="dropdown-date wrapper_input">
                                                <div class="sub-title text-left">Select Date Range</div>
                                                <input class="chose-date" id="start" name="start" type="text" readonly>
                                                <span class="buffer">-</span>
                                                <input class="chose-date" id="end" name="end" type="text" readonly>
                                            </div>
                                        </a>
                                    </div>
                                   </div>
                                </div>
                                 <!-- <div class="additionalmobilediv"></div> -->

                                 <div class="col-12 col-sm-6">
                                 <div class="form-group">
                                    <label class="label-text">Partners of:</label>
                                    <div class="wrapper_select">
                                        <select class="form-control partner-search-checkbox" id="orgList" name="orgList" multiple>
                                            @foreach($orgList as $orgVal)
                                                <option value="{{$orgVal['id']}}" class="oppr_li" id="opprtype{{$orgVal['id']}}" >{{$orgVal['org_name']}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-12 col-sm-12">
                                
                                    <div class="wrapper-two-links clearfix">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input id="include_expired" style="zoom:1.5;" type="checkbox" class="form-check-input">Show expired opportunities?
                                            </label>
                                        </div>
                                    </div>
                                
                               </div>
                            </div>
                            <!-- <div class="row">
                                
                            </div> -->
                    
                    </div>
                    
                </div>
            </div>

            <div class="container map_section_blk">
                <div class="col-12 col-sm-12">
                        <h3 class="custom h3 text-center">
                        <b>We've found <strong class="green">
                                <?php
                                if(isset($opprs))
                                    echo($opprs->count());
                                else if(isset($opportunities))
                                    echo($opportunities->count());
                                else if(isset($result))
                                    echo($result->count());
                                ?>
                     </strong> volunteer opportunities near you</b>
                    </h3>
                   </div>

                    <div class="col-12 col-sm-12">
                        <div class="alpha" id="alpha"></div>
                        <div class="row align-items-center border-bottom-0 alpha" >
                            <div class="col-12 col-md-3" id="one">
                            @if(isset($opprs))
                                @if($opprs->count()>0)
                                    <div class="container user_place">
                                        @foreach($opprs as $o)
                                            <a href="{{url('volunteer/view_opportunity/'.$o->id)}}">{{$o->title}}</a><br><br>
                                        @endforeach
                                    </div>
                                @endif
                            @endif
                            </div>

                            <div class="col-12 col-md-9" id="two">
                                <div class="flex-item" >
                                    <div class="google-map-space">
                                        <div class="google-map" id="big-map">
                                            <input type="hidden" id="lat_val" value="{{$search_addr['lat']}}">
                                            <input type="hidden" id="lng_val" value="{{$search_addr['lng']}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>

    </div>

@endsection

{{--@include('components.auth.modal_pop_up_search_by_location')--}}

@section('script')

    <script src="{{asset('front-end/js/select2.full.js')}}"></script>
    {{--<script src="{{asset('front-end/js/select2.multi-checkboxes.js')}}"></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geometry&key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es"></script>
    <script>

      var map = null;
      var radius_circle;
      var markers_on_map = [];
      var geocoder;
      var infowindow;

      function initialize() {
          var address = /** @type {HTMLInputElement}*/ (
              document.getElementById('input_search_loc'));
          var my_address = new google.maps.places.Autocomplete(address);
          google.maps.event.addListener(my_address, 'place_changed', function () {
              var place = my_address.getPlace();
              // if no location is found
              if (!place.geometry) {
                  return;
              }
              var lat = place.geometry.location.lat(),
                  lng = place.geometry.location.lng();

              console.log('lat =>', lat);
              console.log('lng =>', lng);
              console.log('place =>', place.name);
          });
      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <script src="<?=asset('front-end/js/bootstrap-datepicker.js')?>"></script>
    <script src="<?=asset('js/plugins/datetimepicker/moment.js')?>"></script>
    <script src="<?=asset('js/jquery.multi-select2.js')?>"></script>
    <script src="<?=asset('js/map-helper.js')?>"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            opportunities_marker();
            document.getElementById("end").disabled = true;
            sendRequest();
        });

		$('#empty_location').hide();

		$(function(){
            $('#opportunity_type_select').multiSelect({
                noneText: 'All',
                presets: [
                    {
                        name: 'All',
                        options: []
                    }
                ]
            });

            $('#organization_type_select').multiSelect({
                noneText: 'All',
                presets: [
                    {
                        name: 'All',
                        options: []
                    }
                ]
            });
            $('#orgList').multiSelect({
                noneText: 'All',
                presets: [
                    {
                        name: 'All',
                        options: []
                    }
                ]
            });
        });
		
        function sendRequest() {

            var opp_values = $('.opp-search-checkbox').val();

            var org_values = $('.org-search-checkbox').val();
            
            var partner_values = $('.partner-search-checkbox').val();

            var start_Date = $('#start').val();

            var end_Date = $('#end').val();

            var location = $('#input_search_loc').val();

            var include_expired = $("#include_expired").is(":checked") ? 1 : 0;

            $('#empty_location').hide();

            if(location == ""){
			  $('#empty_location').show();
			  return false;
			}
            var keyword = $('#input_keyword_search').val();

            var distance_radius = $('.distance_radius_select').val();

            var url = API_URL + 'volunteer/opportunity/getSearchResult';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "get";

            var formData = {
                opp_types: opp_values,
                org_types: org_values,
                partner_ids: partner_values,
                start_date: start_Date,
                end_date: end_Date,
                include_expired: include_expired,
                location: location,
                keyword: keyword,
                distance_radius: distance_radius
            };

            console.log(formData);
            
            $.ajax({

                type: type,
                url: url,
                data: formData,

                success: function (data) {
                    if($('.calendar').hasClass('count-info open')){
                        $('.flying-panel').hide()
                    } else{
                        $('.flying-panel').show()
                    }
                    $(".green").html(data.oppr_count);

                    map = new google.maps.Map(document.getElementById('big-map'), {
                        center: new google.maps.LatLng(0, 0),
                        zoom: 3,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: true,
                        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                        navigationControl: true
                    });
                    geocoder = new google.maps.Geocoder();

                    var icon = '{{asset("front-end/img/pin.png")}}';
                    var volunteerUrl = '{{url("volunteer/view_opportunity/")}}' + "/";
                    showCloseLocations(data, icon, volunteerUrl);
                },

                error: function (data) {
                    if($('.calendar').hasClass('count-info open')){
                        $('.flying-panel').hide()

                    } else{
                        $('.flying-panel').show()
                    }
                    console.log('Error:', data);
                }

            });

        }

        $('#btn_search_keyword ,#btn_search_location').on('click',function(){
            sendRequest();
        });

        $('#input_keyword_search ,#input_search_loc').focusout(function () {
            sendRequest();
        });

        $('.opp-search-checkbox, .org-search-checkbox, .distance_radius_select, .partner-search-checkbox').on('change',function () {
            sendRequest();
        });

        $('#include_expired').on('change',function () {

            const dates = $("#start,#end");
            if($("#include_expired").is(":checked")) {
                dates.prop('disabled', true);
                dates.val('');
            }
            else {
                $("#start").prop('disabled', false);
            }

            sendRequest();
        });

        // When the window has finished loading google map

        var selector = '.explore-sidebar li';

        $(selector).on('click', function(){
            $(selector).removeClass('active');
            $(this).addClass('active');
        });

        $(document).on('click.bs.dropdown.data-api', '.dropdown', function (e) {
            e.stopPropagation();
        });

        $('#start').datepicker({
            'format': 'mm-dd-yyyy',
            'autoclose': true,
            'orientation': 'right',
            'todayHighlight': true
        }).on('changeDate', function (selected) {
            $("#end").prop('disabled', false);
            if($("#end").val() != ""){
                daterangeChange();
            }
        }).on('clearDate', function (selected) {
            $('#end').datepicker('setStartDate', '+0d');
        });


        $('#end').datepicker({
            'format': 'mm-dd-yyyy',
            startDate: '+0d'
        }).on('changeDate', function (selected) {
            daterangeChange();
        });

        function daterangeChange() {
            sendRequest();
        }

        $('.calendar .wrapper-calendar').click(function () {
            $('.calendar').toggleClass('open')
        });

        $('.calendar').click(function () {
            if($(this).hasClass('count-info open')){
                $('.flying-panel').hide()
            } else{
                $('.flying-panel').show()
            }
        });
        
        var one=document.getElementById('one').clientHeight;

        // if(one > 10)
        //     document.getElementById('two').style.height = one+'px';
        // else
       document.getElementById('two').style.height = '500px';

    function opportunities_marker()
    {
        var map = new google.maps.Map(document.getElementById('big-map'), {
            center: new google.maps.LatLng(0, 0),
            zoom: 0,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        @php $prepared_opportunities = array() @endphp;
        @if(isset($opportunities))
                @if($opportunities->count()>0)
                    @php $prepared_opportunities = $opportunities; @endphp;
                @endif
            @elseif(isset($result))
                @if($result->count()>0)
                    @php $prepared_opportunities = $result @endphp;
                @endif
            @elseif(isset($opprs))
                @if($opprs->count()>0)
                    @php $prepared_opportunities = $opprs; @endphp;
                @endif
        @endif
        @if(count($prepared_opportunities) > 0)
            var latlngbounds = new google.maps.LatLngBounds();
            var infowindow = new google.maps.InfoWindow();
            @foreach($prepared_opportunities as $o)
            var contentString = '';
                var OpLatLng = {lat: parseFloat('{{ $o->lat}}'), lng: parseFloat('{{ $o->lng}}')};
                    var marker = new google.maps.Marker({
                        position: OpLatLng,
                        map: map,
                        icon: '{{asset('front-end/img/pin.png')}}',
                    });
                latlngbounds.extend(OpLatLng);
                google.maps.event.addListener(marker, 'click', function() {
                    contentString = '<div class="map-modal-box"><div><div class="main-text">';
                    contentString = contentString +
                        '<p class="h3">{{$o->title}}</p>'+
                        '<p><strong>Opportunity</strong></p> ' +
                        '<p class="light"> {{$o->start_date}} {{$o->street_addr1}} , {{$o->city}} ,{{$o->state}}</p> ' +
                        '<p><a href="{{url('/volunteer/view_opportunity')}}/{{$o->id}}">View opportunity info</a></p>';
                    contentString = contentString + '</div></div></div>';
                    infowindow.setContent(contentString)
                    infowindow.open(map, this);
                    });
            @endforeach
            map.fitBounds(latlngbounds);
        @endif
    }

    function setTypes() {
        var organization_partner = $('#orgList').val();
        var organization_type = $('#organization_type_select').val();
        var opportunity_type = $('#opportunity_type_select').val();
        $('.organization_type').val(organization_type);
        $('.opportunity_type').val(opportunity_type);
    }

    </script>

@endsection

