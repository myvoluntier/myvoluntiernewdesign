
<table class="table sortable table_my_blk my_activities_blk" id="activity_table">
    <thead>
    <tr>
        <th colspan="2"><div class="main-text"><p>Sorting</p></div></th>
        <!-- <th></th> -->
    </tr>
    </thead>
    <tbody>
    @foreach($activity as $a)
        <tr class="pending-approval">
            <td>
                @if($a->opportunity != null && $a->opportunity->type != 2 && $a->opportunity->type != 3)
                    @if($a->opportunity->logo_img === null)
                        <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')"></div>
                        <div class="main-text"><p class="normal">{{$a->content}} <a href="{{url('/')}}/volunteer/view_opportunity/{{$a->oppor_id}}">{{$a->oppor_title}}</a></p></div>
                    @else
                        <div class="avatar" style="background-image:url('{{$a->opportunity->logo_img}}')"></div>
                        <div class="main-text"><p class="normal">{{$a->content}} <a href="{{url('/')}}/volunteer/view_opportunity/{{$a->oppor_id}}">{{$a->oppor_title}}</a></p></div>
                    @endif
                @else
                    <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')"></div>
                    <div class="main-text"><p class="normal">{{$a->content}} <a href="{{url('/')}}/volunteer/view_opportunity/{{$a->oppor_id}}">{{$a->oppor_title}}</a></p></div>
                @endif
            </td>
            <td>
                <div class="main-text block"><p class="light">{{$a->updated_at}}</p></div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>