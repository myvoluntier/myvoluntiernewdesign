<div id="filter_content">
    @foreach($tracks as $key=>$t)
        @if(isset($viewpage) && !in_array($key, $trackOrgoIds))
            @continue;
        @endif
    
    <div class="typescript" style="padding: 15px;">
        <div id="">
            <img src="{{$t['org_logo'] == NULL ? asset('front-end/img/org/001.png') : $t['org_logo']}}" class="org_logo">
            <div class="org_info">
                <Label><strong>{{$t['org_name']}}</strong></Label>
                <Label>{{$t['contact_number']}}</Label>
                <Label>{{$t['email']}}</Label>
            </div>
            <div class="org_info_addr">
                <Label>{{$t['address']}}</Label>
            </div>
        </div>
        <div style="overflow: auto; clear: both;">
            <table id="example21" class="table sortable">
                <thead>
                    <tr>
                    @if(!isset($viewpage))
                        <th>
                        <div class="main-text"><p></p></div>
                        </th>
                    @endif
                <th>
                <div class="main-text"><p>Date/Time</p></div>
                </th>
                <th>
                <div class="main-text"><p>Duration(hrs)</p></div>
                </th>
                <th>
                <div class="main-text"><p>Opportunity Title</p></div>
                </th>
                <th>
                <div class="main-text"><p>Category/Type</p></div>
                </th>
                <th>
                <div class="main-text"><p>Date Validated</p></div>
                </th>
                <th>
                <div class="main-text"><p>Validator Name</p></div>
                </th>

                </tr>
                </thead>
                <tbody>
                    @foreach($t['track_info'] as $opp)
                        @if(isset($viewpage) && !in_array($opp['id'], $trackHoursIds))
                            @continue;
                        @endif
                    <tr>
                        @if(!isset($viewpage))
                        <td><input type="checkbox" name="service_hours[]" {{ (!empty($trackHoursIds) && in_array($opp['id'], $trackHoursIds))? 'checked' : '' }}
                                   value="{{$key.'~'.$opp['oppor_id'].'~'.$opp['id'].'~'.($opp['logged_mins']/60)}}" ></td>
                        @endif
                        <td width='13%'>{{$opp['logged_date']}}</td>
                        <td>{{$opp['logged_mins']/60}}</td>
                        <td>{{$opp['oppor_name']}}</td>
                        <td>{{$opp['category_names']}}</td>
                        <td>{{$opp['confirmed_at']}}</td>
                        <td>{{\App\Opportunity::find($opp['oppor_id'])->contact_name}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endforeach
</div>
