@extends('layout.master')
@section('content')
    <div class="wrapper_home_slider clearfix">

        <div class="text-over-slider">
            <div>

                <div class="container">

                    <div class="row">
                        <div class="col-7">

                            <div class="content-to-scale">

                                <div class="main-text">
                                    <h1 class="h1">Making Service Easier</h1>
                                    <p>MyVoluntier matches people and organizations together to create positive impacts
                                        in their community, while tracking service hours, projects and generating impact
                                        reports.</p>
                                    <a href="{{route('features')}}" class="learn-more"><span>Learn more</span></a>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="slider">
            <div>
                <div class="w-slide-5"><span class="slide-item-01"></span></div>
            </div>
            <div>
                <div class="w-slide-5"><span class="slide-item-02"></span></div>
            </div>
            <div>
                <div class="w-slide-5"><span class="slide-item-03"></span></div>
            </div>
            <div>
                <div class="w-slide-5"><span class="slide-item-04"></span></div>
            </div>
        </div>

    </div>

    <div class="our-philosophy-box">
        <div class="container">
            <div class="main-text">

                <h2 class="h2 text-center">Our Philosophy</h2>
                <p class="text-center">Our philosophy is that everyone should have the chance to impact their community.
                    This is why My​Voluntier offers a variety of options for those who have a heart to serve. It’s why
                    we make it easy for volunteers, organizations and institutions to connect with causes they care
                    about.</p>
                <p class="text-center strong">
                    <strong>My​Voluntier was created for three kinds of service members:</strong>
                </p>
                @if(Auth::check())
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <p class="text-center">
                                <span class="service_members vol_registr">
                                    <span class="img volunteer"></span>
                                    <span class="text">Volunteer</span>
                                </span>
                            </p>
                        </div>
                        <div class="col-12 col-md-4">
                            <p class="text-center">
                                <span class="service_members org_registr">
                                    <span class="img organization"></span>
                                    <span class="text">Organization</span>
                                </span>
                            </p>
                        </div>
                        <div class="col-12 col-md-4">
                            <p class="text-center">
                                <span class="service_members inst_registr">
                                    <span class="img institution"></span>
                                    <span class="text ">Institution</span>
                                </span>
                            </p>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <p class="text-center">
                            <!-- <a href="#" class="service_members vol_registr"> -->
                                <a href="{{route('signUp','volunteer')}}" class="service_members">
                                    <span class="img volunteer"></span>
                                    <span class="text">Volunteer</span>
                                </a>
                            </p>
                        </div>
                        <div class="col-12 col-md-4">
                            <p class="text-center">
                                <!-- <a href="#" class="service_members org_registr"> -->
                                <a href="{{route('signUp','organization')}}" class="service_members">
                                    <span class="img organization"></span>
                                    <span class="text">Organization</span>
                                </a>
                            </p>
                        </div>
                        <div class="col-12 col-md-4">
                            <p class="text-center">
                                <!-- <a href="#" class="service_members inst_registr"> -->
                                <a href="{{route('signUp','institution')}}" class="service_members">
                                    <span class="img institution"></span>
                                    <span class="text ">Institution</span>
                                </a>
                            </p>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>

    <div class="row-footer">
        <div>
            @if(!Auth::check())
                <div class="request-a-demo mt-0">
                    <div class="container">
                        <!-- <a class="registration_button" href="#"><span>Create Account</span></a> -->
                        <a href="{{route('signUp')}}"><span>Create Account</span></a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('script')

    <script src="{{ asset('front-end/js/jquery.bxslider-rahisified.js') }}"></script>

    <script>

        $(document).ready(function () {

            $('.wrapper_home_slider > div.slider').bxSlider({
                pager: true,
                slideMargin: 0,
                speed: 1000,
                hideControlOnEnd: true,
                auto: true,
                infiniteLoop: true,
                autoReload: true,
                controls: false,
                breaks: [{screen: 0, slides: 1}],
                onSliderLoad: function () {
                    $(".wrapper_home_slider").css("visibility", "visible");
                }
            });

            //better if the SIGN UP WIndow will appear automatically, and the user type dropdown “ORganization” selected
            const searchParams = new URLSearchParams(window.location.search);

            if(searchParams.has('auto-signup')){
                $( ".registration_button_on_modal_form" ).click();
                const selector = ".type-user-select";
                $(selector).find('option').get(0).remove();
                $('.type-user').val($(selector).val());
            }
        });

    </script>
@endsection