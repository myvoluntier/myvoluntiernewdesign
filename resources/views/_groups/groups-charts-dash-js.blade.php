<script>
    $(document).ready(function () {
        Highcharts.chart('voluntier-hours-by-type{{$groupId}}', {
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                    @forelse($val_hrs_type_chart as $data)
                    ['{{$data["yAxis"]}}', {{ $data["xAxis"] }} ],
                        @empty
                    [],
                    @endforelse
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });
        Highcharts.chart('voluntier-hours-by-month{{$groupId}}', {
            chart: {
                type: 'line'
            },
            title: {
                text: ''
            },

            xAxis: {
                categories: {!! json_encode(array_reverse(getLast12MonthsY())) !!},
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            tooltip: {
                split: false
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },

            series: [          
                {
                    name: 'Hours',
                    data: {!! json_encode($vol_hours_by_month) !!}
                }
            ]
        });

        // Highcharts.chart('volnt-zipcode-chart{{$groupId}}', {
        //     chart: {
        //         type: 'pie'
        //     },
        //     title: {
        //         text: 'Volunteers by Zip Code'
        //     },
        //     tooltip: {
        //         enabled: true,
        //         formatter: function () {
        //             return '<b>' + this.y + ' volunteer</b>';
        //         }
        //     },
        //     plotOptions: {
        //         pie: {
        //             allowPointSelect: true,
        //             cursor: 'pointer',
        //             dataLabels: {
        //                 enabled: false
        //             },
        //             showInLegend: true
        //         }
        //     },
        //     legend: {
        //         itemStyle: {
        //             color: '#9ca0a1',
        //             fontSize: '12px',
        //             fontWeight: 'normal',
        //             fontFamily: 'Open Sans, sans-serif',
        //             textOverflow: 'ellipsis'
        //         }
        //     },
        //     series: [{
        //         name: '',
        //         colorByPoint: true,
        //         data: {!! json_encode(array_values($volunteers_pie_zip)) !!}
        //     }]
        // });

        Highcharts.chart('volnt-agegroup-chart{{$groupId}}', {
            chart: {
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + ' volunteer</b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                itemStyle: {
                    color: '#9ca0a1',
                    fontSize: '12px',
                    fontWeight: 'normal',
                    fontFamily: 'Open Sans, sans-serif',
                    textOverflow: 'ellipsis'
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: {!! json_encode(array_values($volunteers_pie_age)) !!}
            }]
        });
    });
</script>