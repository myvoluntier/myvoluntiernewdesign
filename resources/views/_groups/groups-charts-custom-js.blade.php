<script>
    $(document).ready(function () {
        @foreach($groupList as $lists)

        Highcharts.chart('this-month-chart{{$groupId}}', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -30,
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                   @forelse($lists->thisMonth as $date)
                    ['{{date('d-M-Y',strtotime($date->logged_date))}}', {{($date->SUM/60)}}],
                        @empty
                    [],
                    @endforelse
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('last-month-chart{{$groupId}}', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -30,
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                   @forelse($lists->lastMonth as $date)
                    ['{{date('d-M-Y',strtotime($date->logged_date))}}', {{($date->SUM/60)}}],
                        @empty
                    [],
                    @endforelse
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('last6-month-chart{{$groupId}}', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                   @forelse($lists->last6Month as $date)
                    ['{{date('d-M',strtotime($date->logged_date))}}', {{($date->SUM/60)}}],
                    @empty
                    [],
                    @endforelse
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('year-date-chart{{$groupId}}', {
            chart: {
                type: 'column'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#9ca0a1'
                    }
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + '</b>';
                }
            },
            series: [{
                name: 'Population',
                color: '#03a9f4',
                data: [
                    @forelse($lists->last12Month as $date)
                    ['{{date('M-Y',strtotime($date->logged_date))}}', {{($date->SUM/60)}} ],
                        @empty
                    [],
                    @endforelse
                ],
                dataLabels: {
                    enabled: false
                }
            }]
        });

        Highcharts.chart('hours-by-opp-type{{$groupId}}', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Hours by Opportunity Type'
            },

            xAxis: {
                categories: {!! json_encode(getLast12MonthsY()) !!},
            },
            yAxis: {
                title: {
                    text: 'Hour'
                }
            },
            tooltip: {
                split: false
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },

            series: [
                    @forelse($lists->hours_oppr_type_chart as $ind => $oppr_type)
                {
                    name: '{{$ind}}',
                    data: [{{implode( ',',$oppr_type)}}]
                },
                    @empty
                {},
                @endforelse
            ]
        });

        Highcharts.chart('top-5-opportunities-chart{{$groupId}}', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Top 5 Opportunities',
                style: {"color": "#27282f", "fontSize": "24px", "fontFamily": 'Open Sans, sans-serif'}
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                lineWidth: 0,
                tickLength: 0,
                categories: {!! json_encode(array_keys($lists->top_5_opprtunities)) !!},
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#28292e'
                    }
                }
            },
            yAxis: {
                visible: false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontSize: '14px',
                            fontFamily: 'Open Sans, sans-serif',
                            fontWeight: 'normal',
                            color: '#fff',
                            textOutline: ''
                        },
                        align: 'right'
                    },
                    showInLegend: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            series: [{
                color: '#42bd41',
                data: [
                    @if(count($lists->top_5_opprtunities)>0)
                    @foreach($lists->top_5_opprtunities as $ky => $val)
                    {{$val}},
                    @endforeach
                    @endif
                ],
                dataLabels: {
                    format: '{y} hrs'
                }
            }]
        });

        Highcharts.chart('volnt-zipcode-chart{{$groupId}}', {
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Volunteers by Zip Code'
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + ' volunteer</b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                itemStyle: {
                    color: '#9ca0a1',
                    fontSize: '12px',
                    fontWeight: 'normal',
                    fontFamily: 'Open Sans, sans-serif',
                    textOverflow: 'ellipsis'
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: {!! json_encode(array_values($lists->volunteers_pie_zip)) !!}
            }]
        });

        Highcharts.chart('volnt-agegroup-chart{{$groupId}}', {
            chart: {

                type: 'pie'
            },
            title: {
                text: 'Volunteers by Age Group'
            },
            tooltip: {
                enabled: true,
                formatter: function () {
                    return '<b>' + this.y + ' volunteer</b>';
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            legend: {
                itemStyle: {
                    color: '#9ca0a1',
                    fontSize: '8px',
                    fontWeight: 'normal',
                    fontFamily: 'Open Sans, sans-serif',
                    textOverflow: 'ellipsis'
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: {!! json_encode(array_values($lists->volunteers_pie_age)) !!}
            }]
        });

        Highcharts.chart('top-5-volonteers-chart{{$groupId}}', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Top 5 volunteer',
                style: {"color": "#27282f", "fontSize": "24px", "fontFamily": 'Open Sans, sans-serif'}
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                lineWidth: 0,
                tickLength: 0,
                categories: [
                    @if(count($lists->volunteersTop5Chart)>0)
                        @foreach(array_keys($lists->volunteersTop5Chart) as $ky)
                        '{{removeNumFromStart($ky)}}',
                        @endforeach
                    @endif
                ],
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '14px',
                        fontWeight: 'normal',
                        fontFamily: 'Open Sans, sans-serif',
                        color: '#28292e'
                    }
                }
            },
            yAxis: {
                visible: false
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontSize: '14px',
                            fontFamily: 'Open Sans, sans-serif',
                            fontWeight: 'normal',
                            color: '#fff',
                            textOutline: ''
                        },
                        align: 'right'
                    },
                    showInLegend: false
                }
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            series: [{
                color: '#42bd41',
                data: [
                    @if(count($lists->volunteersTop5Chart)>0)
                    @foreach($lists->volunteersTop5Chart as $ky => $val)
                    {{$val}},
                    @endforeach
                    @endif
                ],
                dataLabels: {
                    format: '{y} hrs'
                }
            }]
        });

        @endforeach
    });
</script>