<div class="wrapper-impact pb-0 pt-0">

    <div class="row align-items-center border-top-0 border-left-0 border-right-0">

        <div class="col col-auto">
            <a href="javascript:void(0)" onclick="window.print()" class="print">
                <span><i class="fa fa-print" aria-hidden="true"></i></span>
            </a>
        </div>
    </div>

    <div class="row align-items-center border-top-0 border-left-0 border-right-0">
        <div class="col-12 col-md-4">

            <img style="max-width: 45%" src="{{$lists->logo_img ? $lists->logo_img : asset('no-image.png')}}" class="img-thumbnail mx-auto d-block" alt="Group logo">

            <div class="main-text">
                <h3 class="h3 text-center">Ranked</h3>

                <div class="row">
                    <div class="col">
                        <p class="mb-0 text-center green middle">
                            <strong class="big">{{$lists->rank}}</strong>{{numberDescender($lists->rank)}}
                        </p>
                        <p class="light text-center">Of All <br>Groups</p></div>
                    <div class="col">
                        <p class="mb-0 text-center green middle">
                            <strong class="big">{{$lists->ranking_cat}}</strong>{{numberDescender($lists->ranking_cat)}}
                        </p>
                        <p class="light text-center">Of "{{$lists->ranking_cat_name}}" Groups</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <p class="mb-0 text-center green middle">
                            <strong class="middle">{{number_format($lists->tracked_hours/60,1)}}</strong>
                        </p>
                        <p class="mb-0 text-center green ">HOUR(S)</p>
                        <p class="light text-center">of service contributed</p>
                    </div>
                    <div class="col">
                        <p class="mb-0 text-center green middle">
                            <strong class="middle">{{number_format($lists->des_tracked_hours,1)}}</strong>
                        </p>
                        <p class="mb-0 text-center green">Designated</p>
                        <p class="mb-0 text-center green">HOUR(S)</p>
                        <p class="light text-center">of contributed</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-8 border-left">

            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link"
                       href="#thisMonth{{$key}}"
                       role="tab"
                       title="This Month"
                       data-toggle="tab"><span>This Month</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#lastMonth{{$key}}"
                       role="tab"
                       title="Last Month"
                       data-toggle="tab"><span>Last Month</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#last6Month{{$key}}"
                       role="tab"
                       title="Last 6 Month"
                       data-toggle="tab"><span>Last 6 Month</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active show" href="#y2date{{$key}}"
                       role="tab"
                       title="last 12 months"
                       data-toggle="tab"><span>last 12 months</span></a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="thisMonth{{$key}}">
                    <div id="this-month-chart{{$key}}" class="wrapper-svg"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="lastMonth{{$key}}">
                    <div id="last-month-chart{{$key}}" class="wrapper-svg"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="last6Month{{$key}}">
                    <div id="last6-month-chart{{$key}}" class="wrapper-svg"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade in active show" id="y2date{{$key}}">
                    <div id="year-date-chart{{$key}}" class="wrapper-svg"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row border-top-0 border-left-0 border-right-0 border-bottom-0">
        <div class="col-12 pb-0">
            <div class="main-text">

                @if(round($lists->tracked_hours/60) != 0)
                    <h3 class="h3">Past Records</h3>

                    @if(in_array($lists->group_id,$lists->arr5))
                        <p class="light mb-0">Top 5 Groups in the
                            State (number
                            of hours contributed during the last
                            year)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->arr))
                        <p class="light mb-0">Top 10 Groups in the
                            Country
                            (number of hours contributed during the
                            last
                            year)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->month5))
                        <p class="light mb-0">Top 5 Groups in the
                            State (number
                            of hours contributed during the last
                            month)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->month))
                        <p class="light mb-0">Top 10 Groups in the
                            Country
                            (number of hours contributed during the
                            last
                            ymonth)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->volun5))
                        <p class="light mb-0">Top 5 Groups in the
                            State (number
                            of volunteers)</p>
                    @endif
                    @if(in_array($lists->group_id,$lists->volun))
                        <p class="light mb-0">Top 10 Groups in the
                            Country
                            (number of volunteers)</p>
                    @endif
                @endif

            </div>
        </div>
    </div>

    <div class="row align-items-center border-bottom-0">
        <div class="col-12 col-md-8">
            <div id="hours-by-opp-type{{$key}}" class="wrapper-svg"></div>
        </div>

        <div class="col-12 col-md-4">
            <div class="main-text">
                <p class="mb-0 text-center green middle"><strong class="big">{{$lists->total_opportunities}}</strong></p>
                <p class="mb-0 text-center green middle">Total</p>
                <p class="light text-center">Opportunities</p>

                <p class="mt-1 mb-0 text-center green big"><strong>{{number_format($lists->avg_hrs_per_oppr ,1)}}</strong></p>
                <p class="mb-0 text-center green middle">Average</p>
                <p class="light text-center">hour per opportunity</p>
            </div>
        </div>
    </div>

    <div class="row align-items-center border-bottom-0">
        <div class="col-12 col-md-7">
            <div id="top-5-opportunities-chart{{$key}}" class="wrapper-svg"></div>
        </div>

        <div class="col-12 col-md-5 border-left">
            <div class="main-text">
                <h3 class="h3">Opportunity List</h3>
                <div class="wrapper-table-impact">
                    <div>
                        <table>
                            @if(count($lists->opprtunities_list))
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Hours</th>
                                    <th>Volunteers</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lists->opprtunities_list as $oppr)
                                    <tr>
                                        <td>{{$oppr['name']}}</td>
                                        <td>
                                            <p class="green">{{$oppr['hours']}}</p>
                                        </td>
                                        <td>{{$oppr['volonteers']}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            @else
                                <tr>
                                    Sorry..! No Opportunity found
                                </tr>
                            @endif

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row align-items-center border-bottom-0">
        <div class="col-12 col-md-12">
            <div class="row">
                <div class="col">
                    <div id="volnt-zipcode-chart{{$key}}" style="min-width: 110px; height: 250px; margin: 0 auto" class="wrapper-svg"></div>
                </div>

                <div class="col">
                    <div id="volnt-agegroup-chart{{$key}}" style="min-width: 110px; height: 250px; margin: 0 auto" class="wrapper-svg"></div>
                </div>

                <div class="col">
                    <div class="main-text">
                        <p class="mb-0 text-center green middle"><strong class="big">{{count($lists->volunteers)}}</strong></p>
                        <p class="mb-0 text-center green middle">Total</p>
                        <p class="light text-center">Volunteers</p>

                        <p class="mt-1 mb-0 text-center green big"><strong>{{number_format($lists->volunteers_avg ,1)}}</strong></p>
                        <p class="mb-0 text-center green middle">Average</p>
                        <p class="light text-center">hours per volunteer</p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row align-items-center border-bottom-0">
        <div class="col-12 col-md-7">
            <div id="top-5-volonteers-chart{{$key}}" class="wrapper-svg"></div>
        </div>

        <div class="col-12 col-md-5 border-left">
            <div class="main-text">
                <h3 class="h3">Volunteer List</h3>
                <div class="wrapper-table-impact">
                    <div>
                        <table>
                            @if(count($lists->volunteers))
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Hours</th>
                                    <th>Opportunities</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($lists->volunteers as $member)
                                    <tr>
                                        <td>{{$member['name']}}</td>
                                        <td>
                                            <p class="green">{{$member['hours']}}</p>
                                        </td>
                                        <td>{{$member['total_oppr']}}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            @else
                                <tr>
                                    Sorry..! No volunteers found
                                </tr>
                            @endif

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>