@extends('layout.master')

@section('content')

<div class="full-50-col-box">
        <div class="container">

            <div class="row no-gutters align-items-center">
                <div class="col-12 col-md-6">
                    <div class="what-myvoluntier-banner-1"></div>
                </div>

                <div class="col-12 col-md-6">

                    <div class="main-text panel">

                        <h3 class="h3">What is My​Voluntier?</h3>

                        <p>My​Voluntier is the premier web application used to find, track, and mobilize individuals to
                            better impact their communities.</p>
                        <p>Through our state-of-the-art reporting and tracking tools, we provide a suite of powerful
                            assets that allow individuals, groups, and organizations to measure the impact of their
                            service across their community.</p>

                    </div>

                </div>
            </div>


        </div>
    </div>

    <div class="trust-factor-box">
        <div class="container">
            <div class="main-text">

                <h2 class="h2 text-center">The Trust Factor</h2>
                <p class="text-center">My​Voluntier's focus has always been on the people--which is why we have become a
                    trusted name to a growing list of organizations for tracking community engagement, service hours and
                    service-learning across the country.</p>

            </div>
        </div>
    </div>

    <div class="full-50-col-box">
        <div class="container">
            <div class="row no-gutters align-items-center">
                <div class="col-12 col-md-6 order-md-1">
                    <div class="what-myvoluntier-banner-2"></div>
                </div>
                <div class="col-12 col-md-6 order-md-0">

                    <div class="main-text panel">

                        <h3 class="h3">My​Voluntier's Big Picture</h3>

                        <p>At My​Voluntier, we believe that a better world is possible, when you can connect people who
                            serve with causes they care about.</p>
                        <p>My​Voluntier was created to be a virtual hub for the service community to connect and
                            mobilize to be a driving force for positive change in our communities. Our goal? To take
                            away barriers between volunteers and organizations and bring the focus back to what matters:
                            the community.</p>

                    </div>

                </div>
            </div>

        </div>
    </div>

    <div class="row-footer">
        <div>
            @if(!Auth::check())
                <div class="request-a-demo mt-0">
                    <div class="container">
                        <!-- <a class="registration_button" href="#"><span>Create Account</span></a> -->
                        <a href="{{route('signUp')}}"><span>Create Account</span></a>
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection