@extends('layout.frame')
@section('css')
<style>
    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }

    
    .select2-container--below{background: yellow};
</style>
@yield('css')
@endsection
@section('body')
<body>
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5426731.js"></script>

<div class="offcanvas-menu">
        <div class="container">
            <div class="header-center-menu">
                <ul class="nav">
                    @include('components.non-auth.buttons_registration_and_login')
                </ul>
            </div>
        </div>
</div>


<div class="offcanvas-contentarea">
<div class="wrapper_bottom_footer">
    @include('components.non-auth.header')

        <div class="row-content">
        <input class="type-user" type="hidden" value="Volunteer">

        @if(request()->confirmed)
            <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                <strong>Hi, </strong> Your account has been confirmed. Please login to continue.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        @endif
   <div class="group-wrapper">
       <!-- page heading start -->
        <div class="page_head-box green_bg_blk">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <h2 class="h2 text-center">Join Groups</h2>                            
                        </div>
                        
                    </div>
                </div>
            </div>       
        <!-- page heading end -->

     <div class="group-container">
            <div id="inviteError" class="alert alert-danger" role="alert" style="display:none"></div>
            <div id="inviteSuccess" class="alert alert-success" role="alert" style="display:none"></div>

        <div class="group_pic_outer_blk">
            <h6 class="skip_txt text-right"><a href="{{route('thankYou')}}">Close</a></h6>
                <div class="group_pic"   
                style="background-image:url('{{$userDetail->logo_img == NULL ? asset('front-end/img/affiliateLogo.jpg') : $userDetail->logo_img}}')">
                    <span></span>
                </div>
                <h6 class="lite_txt">Our Organization</h6>
                <h3 class="big_txt">{{$userDetail->org_name}}</h3>
                @if(count($groups)>0)
                <h6 class="lite_txt">You may join the groups below:</h6>
                @else
                <h6 class="lite_txt">No group found in {{$userDetail->org_name}}</h6>
                @endif
        </div>

            <div class="link-box">                            
                                    <ul id="tabs" class="tabs_groups nav nav-tabs" role="tablist">
                                        @if(count($groups)>0)
                                            <ul id="myUL" class="groups_listing">
                                                @foreach($groups as $key => $lists)
                                                <li>
                                                <div class="group-row"> 
                                                    <div class="pic_groupername">
                                                        <div class="listing_pic"                                                                                           
                                                                style="background-image:url('{{$lists->logo_img == NULL ? asset('front-end/img/org/001.png') : $lists->logo_img}}')">
                                                            <span></span>
                                                        </div>
                                                        <div class="groupername">
                                                            <div data-groupTab='{{$key}}' class="groupTab tab_groups nav-item" id="pane-li-{{$key}}">
                                                            <a id="tab-{{$key}}" href="#pane-{{$key}}" data-toggle="tab" role="tab">{{ $lists->name }}</a>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="group-btn">
                                                        <div class="nav-item">
                                                            <button class="process btn" title="Join Group" onclick="groupJoin({{$lists->id}},{{$userId}})">Join</button>
                                                        </div>     
                                                    </div>          
                                                </li> 
                                                    
                                                @endforeach
                                            </ul> 
                                        @endif
                                    </ul>
                                </div>
                @yield('content')
            </div>
            
      
        </div>

</div>
@include('components.footer')
    @include('components.non-auth.modal_terms_and_conditions')
    @include('components.non-auth.modal_privacy_policy')

<script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('front-end/js/popper.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/check_validate.js')}}"></script>
    <script src="{{asset('js/home-action.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('front-end/js/select2.full.js')}}"></script>
 <script>
     $('.dropdown').each(function(index, dropdown) {

//Find the input search box
let search = $(dropdown).find('.search');

//Find every item inside the dropdown
let items = $(dropdown).find('.dropdown-item');

//Capture the event when user types into the search box
$(search).on('input', function() {
    filter($(search).val().trim().toLowerCase())
});

//For every word entered by the user, check if the symbol starts with that word
//If it does show the symbol, else hide it
function filter(word) {
    let length = items.length
    let collection = []
    let hidden = 0
    for (let i = 0; i < length; i++) {
        if (items[i].value.toString().toLowerCase().includes(word)) {
            $(items[i]).show()
        } else {
            $(items[i]).hide()
            hidden++
        }
    }

    //If all items are hidden, show the empty view
    if (hidden === length) {
        $(dropdown).find('.dropdown_empty').show();
    } else {
        $(dropdown).find('.dropdown_empty').hide();
    }
}

//If the user clicks on any item, set the title of the button as the text of the item
$(dropdown).find('.dropdown-menu').find('.menuItems').on('click', '.dropdown-item', function() {
    $(dropdown).find('.dropdown-toggle').text($(this)[0].value);
    $(dropdown).find('.dropdown-toggle').dropdown('toggle');
})
});
   $(document).ready(function() {
    var $outerwidth = $('.row-header header .outer-width-box');
        var $innerwidth = $('.row-header header .inner-width-box');

        function checkWidth() {

            var outersize = $outerwidth.width();
            var innersize = $innerwidth.width();
            if (innersize > outersize) {
                $('body').addClass("navmobile");
            } else {
                $('body').removeClass("navmobile");
                $('body').removeClass("offcanvas-menu-show");
            }
        }

        checkWidth();
        $(window).resize(checkWidth);
        $('.offcanvas-menu-backdrop').on('click', function(e) {
            $('body').toggleClass("offcanvas-menu-show");
            e.preventDefault();
        });
        $('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function(e) {
            $('body').toggleClass("offcanvas-menu-show");
            e.preventDefault();
        });

        function doResize($wrapper, $el) {

            var scale = Math.min(
                $wrapper.outerWidth() / $el.outerWidth(),
                $wrapper.outerHeight() / $el.outerHeight()
            );
            if (scale < 1) {

                $el.css({
                    '-webkit-transform': 'scale(' + scale + ')',
                    '-moz-transform': 'scale(' + scale + ')',
                    '-ms-transform': 'scale(' + scale + ')',
                    '-o-transform': 'scale(' + scale + ')',
                    'transform': 'scale(' + scale + ')'
                });
            } else {
                $el.removeAttr("style");
            }
        }

        doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
        @if(!Auth::check())
        $(document).on('click', '.vol_registr', function(e) {
            e.preventDefault();
            scroll(0, 0);
            setTimeout(function() {
                var text = 'Volunteer';
                $('.type-user').val(text);
                $('#myModalSingVolRegistration').modal('show');
                $('body').removeClass('offcanvas-menu-show');
                setTimeout(function() {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200)
        });

        $(document).on('click', '.org_registr', function(e) {
            scroll(0, 0);
            setTimeout(function() {
                e.preventDefault();
                var text = 'Organization';
                $('.type-user').val(text);
                $('#myModalOrgType').modal('show');
                $('body').removeClass('offcanvas-menu-show');
                setTimeout(function() {
                    $('body').addClass('modal-open');
                }, 500);
            }, 200)
        });
        $(document).on('click', '.inst_registr', function(e) {
            e.preventDefault();
            scroll(0, 0);
            var text = 'Organization';
            $('.type-user').val(text);
            $('#org_type').val(1);
            $('#chooseTypeOrg').click();
            setTimeout(function() {

            }, 200);
        });
        @endif
     $("select").select2({
            theme: "bootstrap",
            minimumResultsForSearch: -1
        });
   });
function groupJoin(groupId, userId){
         $.ajax({
                  type:'post',
                  url:BASE_URL+'volunteer/join-group',
                  data: {
                     "_token" : $('meta[name=_token]').attr('content'),
                      "id":groupId,
                      "userId":userId
                  },
                  success: function (response) {
                $(window).scrollTop(0);
                if(response.error){                       
                    $("#inviteSuccess").hide();
                    $("#inviteError").html(response.error); 
                    $("#inviteError").show();
                 }else if(response.success){                   
                    $("#inviteError").hide();
                    $("#inviteSuccess").html(response.success); 
                    $("#inviteSuccess").show();
                 }else if(response.error && response.redirect){
                        $("#inviteSuccess").hide();
                        $("#inviteError").html(response.error); 
                        $("#inviteError").show();  
                        window.location.replace(BASE_URL+response.redirect);
                 }else{
                       window.location.replace(BASE_URL+response.redirect);            
                     } 
                  },
                error: function(xhr, type, exception) { 
                    // if ajax fails display error alert
                    console.log("ajax error response type "+type);
                }
         });
}
</script>
@yield('script')
</body>
@endsection 