@extends('layout.master')

@section('css')
    <style>
        .iframe-container {
            overflow: hidden;
            padding-top: 56.25%;
            position: relative;
        }

        .iframe-container iframe {
            border: 0;
            height:100%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%;
        }

        /* 4x3 Aspect Ratio */
        .iframe-container-4x3 {
            padding-top: 75%;
        }
    </style>
@endsection

@section('content')
    <div class="container">

        <div class="iframe-container">
            <iframe scrolling="yes" src="{{env('BLOG_URL')}}" ></iframe>
        </div>


    </div>
@endsection