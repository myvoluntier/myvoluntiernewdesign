@include('admin.include.admin_header')
<link rel="stylesheet" href="{{asset('/admin/plugins/bootstraptoggle/bootstrap-toggle.min.css')}}">
<style>
    .table-responsive {
        overflow-x: initial;
    }

    .input-group-addon {
        border-left-width: 0;
        border-right-width: 0;
    }
    .input-group-addon:first-child {
        border-left-width: 1px;
    }
    .input-group-addon:last-child {
        border-right-width: 1px;
    }

    @media screen and (max-width:500px) {
        .btn-success { display:none; }
    }

    .table-responsive {
        min-height: .01%;
        overflow-x: auto !important;
    }
    .adminMailSend {float: right;}
</style>
@include('admin.include.admin_head_nav')
@include('admin.volunteers-organization.adminEmailSend')
<div class="content-wrapper full-width">
    <section class="content">
        <div class="row">

            <div class="col-xs-12">

                <div class="box">
                    @if(Session::has('success'))
                        <div class="alert alert-success">{{Session::get('success')}}</div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger">{{Session::get('error')}}</div>
                    @endif

                    <div class="row text-center" id="img_spin" style="display: none">
                        <img src="{{asset('img/loading.gif')}}" style="height: 60px" />
                    </div>

                    <div class="box-header">
                        <h2 class="box-title text-success">{{$heading}}</h2>
                        @if(Request::segment(3) != 'inactive')
                            <a class="btn btn-success" style="float: right" href="{{url('admin/import-export-csv-excel')}}">Bulk Account Creation</a>
                        @endif
                    </div>

                        <form action="" method="GET" id="product-form" class="pb-2 mb-3 border-bottom-light">
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">

                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search" placeholder="Search something..."
                                               value="{{ request()->search }}"/>

                                        <span class="input-group-addon" style="border-left: 0; border-right: 0; background-color: #3bb44a; color: white">Sort</span>

                                        <div class="form-group">
                                            <select class="form-control" name="sorting">
                                                @foreach($sortingColumns as $index => $value)
                                                <option value="{{$index}}" {{ request()->sorting == $index ? 'selected' : '' }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-default " ><i class="fa fa-search-plus" aria-hidden="true"></i></button>
                                        </span>
                                    </div>

                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </form>

                    <div class=" table-responsive  table-striped ">
                        <form id="frmuserID" action="{{url('admin/multipleDelete')}}" method="post">
                           {{--<p class="text-success">Bulk Action: <span class="deleteuser" style="cursor: pointer; color: red; display: none; ">Delete All Account</span></p>--}}
                           <input type="hidden" name="ids" class="delete_ids" >
                           {!! csrf_field() !!}
                        </form>
                        <table class="table">
                            <thead>
                                @if(Request::segment(3) != 'inactive')
                                    <th class="first"><input type="checkbox" name="check_all"></th>
                                    <th>No.</th>
                                @endif
                                <th>Name</th>
                                @if($type == 'organization' && Request::segment(3)== 'inactive')
                                <th style="width:18%;">Organization Info</th>
                                @endif
                                <th>Email</th>
                                <th>Username</th>

                                <th>Zipcode</th>
                                 @if($type == 'organization')
                                    <th>Declined</th>
                                @endif
                                <th>Status</th>
                                <th>Contact </th>
                                <th>Email Confirm</th>
                                @if($type == 'organization' && Request::segment(3)== 'all')
                                    <th>Api Access</th>
                                    <th>Domain 1</th>
                                    <th>Domain 1</th>
                                @endif
                                @if(Request::segment(3) != 'inactive')
                                    <th width="50">Action</th>
                                @endif
                            </thead>
                            <tbody>
                                @if($list->count()>0)
                                @foreach($list as $each)
                                <tr>
                                    @if(Request::segment(3) != 'inactive')
                                        <td> @if($each->is_deleted!=1)<input type="checkbox" name="check_all_id[]" class="select_all" value="{{$each->id}}">@endif</td>
                                        <td>{{$loop->iteration}}</td>
                                    @endif
                                    <td>
                                        @if($each->user_role=='volunteer') {{$each->first_name .' '.$each->last_name }}@endif
                                        @if($each->user_role=='organization') {{$each->org_name}}@endif
                                    </td>
                                    @if($type == 'organization' && Request::segment(3)== 'inactive')
                                        <td>
                                            <p><strong>Type :</strong> {{$each->getTypeNameAttribute()}}</p>
                                            @if($each->org_type == '1')
                                                <p><strong>School Type :</strong> {{$each->getSchoolType()}}</p>
                                            @elseif($each->org_type == '2')
                                                <p><strong>EIN :</strong> {{$each->ein}}</p>
                                            @endif
                                            <p><strong>Year Founded : </strong>{{$each->birth_date}}</p>
                                            <p><strong>Email : </strong>{{$each->email}}</p>
                                            <p><strong>Phone : </strong>{{$each->contact_number}}</p>
                                        </td>
                                    @endif
                                    <td>{{$each->email}}
                                        @if($type == 'organization')
                                            <a data-opportuniti-id-mail="" id="{{$each->id}}" href="#" class="edit adminMailSend"><span><i class="fa fa-envelope"></i></span></a>
                                        @endif
                                    </td>
                                    <td>{{$each->user_name}}</td>

                                    <td>{{$each->zipcode}}</td>
                                     @if($type == 'organization')
                                        <td>
                                            <form action="{{route('user.statuses.update')}}" method="POST" >
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{$each->id}}">
                                                <input type="hidden" name="column" value="declined">

                                                <input class="status" data-on="Yes" data-off="No"
                                                       {{$each->is_deleted ? 'disabled' : ''}}
                                                       @if(@$each->declined=='1') checked  @endif
                                                       data-onstyle="danger" data-offstyle="info" type="checkbox"
                                                       data-size="mini" data-toggle="toggle" >
                                            </form>
                                        </td>
                                    @endif
                                   
                                    <td>
                                        <form action="{{route('user.statuses.update')}}" method="POST" >
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" value="{{$each->id}}">
                                            <input type="hidden" name="column" value="status">

                                            <input name="status" class="status" data-on="Active" data-off="Inactive"
                                               {{$each->is_deleted ? 'disabled' : ''}}
                                               @if(@$each->status) checked @endif
                                               data-onstyle="success" data-offstyle="warning" type="checkbox"
                                               data-size="mini" data-toggle="toggle" >
                                        </form>
                                    </td>
                                    <td>
                                        {{$each->contact_number}}
                                    </td>

                                    <td>
                                        <form action="{{route('user.statuses.update')}}" method="POST" >
                                            {{csrf_field()}}
                                            <input type="hidden" name="id" value="{{$each->id}}">
                                            <input type="hidden" name="column" value="confirm_code">

                                            <input class="status" data-on="Yes" data-off="No"
                                                   {{$each->is_deleted ? 'disabled' : ''}}
                                                   @if(@$each->confirm_code=='1') checked @endif
                                                   data-onstyle="info" data-offstyle="danger" type="checkbox"
                                                   data-size="mini" data-toggle="toggle" >
                                        </form>
                                    </td>
                                    @if($type == 'organization' && Request::segment(3)== 'all')

                                        <td>
                                            <form action="{{route('user.domains.update')}}" method="POST" >
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{$each->id}}">
                                                <input type="hidden" name="column" value="is_allow_user_sync_attr">

                                                <input class="status" data-on="Yes" data-off="No" name="abc"
                                                       @if($each->apiAccess && $each->apiAccess->is_allow_user_sync_attr=='1') checked @endif
                                                       data-onstyle="success" data-offstyle="warning" type="checkbox"
                                                       data-size="mini" data-toggle="toggle" >
                                            </form>
                                        </td>

                                        <td>
                                            @if($each->domain_1)
                                                {{$each->domain_1}}
                                                <form action="{{route('user.domains.update')}}" method="POST" >
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="id" value="{{$each->id}}">
                                                    <input type="hidden" name="column" value="domain_1_confirm_code">

                                                    <input class="status" data-on="Yes" data-off="No"
                                                           {{$each->is_deleted ? 'disabled' : ''}}
                                                           @if(@$each->domain_1_confirm_code=='1') checked @endif
                                                           data-onstyle="info" data-offstyle="danger" type="checkbox"
                                                           data-size="mini" data-toggle="toggle" >
                                                </form>
                                            @else
                                             N/A
                                            @endif
                                        </td>
                                        <td>
                                            @if($each->domain_2)
                                                {{$each->domain_2}}
                                                <form action="{{route('user.domains.update')}}" method="POST" >
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="id" value="{{$each->id}}">
                                                    <input type="hidden" name="column" value="domain_2_confirm_code">

                                                    <input class="status" data-on="Yes" data-off="No"
                                                           {{$each->is_deleted ? 'disabled' : ''}}
                                                           @if(@$each->domain_2_confirm_code=='1') checked @endif
                                                           data-onstyle="info" data-offstyle="danger" type="checkbox"
                                                           data-size="mini" data-toggle="toggle" >
                                                </form>
                                            @else
                                             N/A
                                            @endif
                                        </td>
                                    @endif
                                    @if(Request::segment(3) != 'inactive')
                                    <td>
                                        @if($each->is_deleted!=1)
                                            {{--<a href="javascript:void(0);" data-id="{{$each->id}}" class="open_modal actnbtn"><i class="fa fa-edit"></i></a>--}}
                                            <a href="{{url('/admin/manage-user')}}/vol/{{$each->id}}" data-id="{{$each->id}}" class="actnbtn"><i class="fa fa-edit"></i></a>
                                            {{--<a onclick="return confirm('Are you sure to delete ?')" href="{{url('/admin/user-delete')}}/{{$each->id}}" data-id="{{$each->id}}" class="actnbtn"><i class="fa fa-trash"></i></a>--}}
                                        @endif
                                    </td>
                                    @endif
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="pull-right">
                            {{$list->render()}}
                        </div>
                    </div>

                </div>


            </div>
        </section>
</div>


<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit</h4>
                </div>
                <div class="modal-body" id="modalBody">
                    <!-- Ajax loaded data renders here -->
                </div>
            </div>
        </div>
    </div>

@include('admin.include.admin_footer')

<script src="{{asset('admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('admin/dist/js/org-vollist.js')}}"></script>

<script type="text/javascript">

    jQuery(document).ready(function($) {

        var url = "{{url('/admin/user-editing')}}";
        $(document).on('click','.open_modal',function(){
            var user_id = $(this).data("id");
            $.get(url + '/' + user_id, function (data) {
                $("#modalBody").html(data);
                $('#myModal').modal('show');
               // $('#edit_user_id').val(user_id);
            })
        });

        $('#myModal').on('shown.bs.modal', function() {
            $('.birth_date').datepicker({
                format: "mm-dd-yyyy",
                todayBtn: "linked",
                changeMonth: true,
                changeYear: true,
                autoclose: false,
                todayHighlight: true,
                zIndexOffset: 10,
                container: '#myModal modal-body'
            });
        });

        $('.first').removeClass('sorting_asc');
        //$('.first').removeClass('sorting_desc');

        $("input[name='check_all_id[]']").change(function(){
            if($("input[name='check_all_id[]']:checked").length > 0)
            {
                $('.deleteuser').html("Delete Selected Accounts");
                $('.deleteuser').show();
            }
            else
            {
                $('.deleteuser').html("");
                $('.deleteuser').hide();
            }
        })

        $('input[name="check_all"]').change(function() {
            if($(this).prop('checked')){
                $('.deleteuser').html("Delete All Account");
                $('.select_all').prop('checked',true);
                $('.deleteuser').show();
            }else{
                $('.deleteuser').html("");
                $('.select_all').prop('checked',false);
                $('.deleteuser').hide();
            }
            $('.first').removeClass('sorting_asc');
            $('.first').removeClass('sorting_desc');
        });
        $('.deleteuser').click(function(){
            var ids = new Array();
            $('input[name="check_all_id[]"]:checked').each(function() {
               ids.push(this.value);
            });

            if(ids.length <=0) {
                toastr.error("Please select minimum 1 row.", "Error");
            }else{
                var check = confirm("Are you sure you want to delete this row?");
                    if(check == true){
                        $('.delete_ids').val(ids);
                        $('#frmuserID').submit();
                    }
                }
        });

    });
</script>