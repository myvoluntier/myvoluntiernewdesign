<script src="https://cdn.ckeditor.com/4.11.3/standard-all/ckeditor.js"></script>
<div class="modal fade" id="adminEmailSendBox" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix modal-share-px">
            <div>
                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h2 class="h2">Organization <strong class="green"> Email</strong></h2>
                            <!--<h3 class="h3">You can share your profile via email!</h3>-->
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body hide-email-comment" id="selectTypeDiv">
                        
                        <div class="form-group">
                            <label class="label-text">Message:</label>
                            <div class="wrapper_input ">
                                <textarea id="message_txt" placeholder="You can enter message here"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="modal-body" style="display:block;">

                        <div class="success-first" style="display: none;text-align: left;">
                            <h3>Email has been sent successfully!!</h3>
                        </div>

                        <div class="wrapper-link top-50">
                            <a id="orgEmail_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                            <a id="orgEmailBtn" href="#"><span>Send</span></a>
                            <a style="display: none" id="orgEmail_hide" href="#"><span>Close</span></a>
                        </div>
                            <input type="hidden" id = "emaiIdToSend" value="0">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<script>
CKEDITOR.replace('message_txt', {
    height: 200,
    removeButtons: '',
    extraPlugins: 'colorbutton,colordialog'
});
       
</script>