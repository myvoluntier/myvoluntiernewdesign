<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          {{-- <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> --}}
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
              <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a>
                </li>

                <li>
                    <a target="_blank" href="{{url('/')}}"><i class="fa fa-sitemap"></i> Main Site</a>
                </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i> <span>Accounts</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/user-list')}}"><i class="fa fa-circle-o"></i> Accounts</a></li>
              </ul>
            </li>

			<li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i> <span>Groups</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/group-list')}}"><i class="fa fa-circle-o"></i> Groups</a></li>
              </ul>
            </li>

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-bank"></i> <span>Opportunities</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{url('/admin/opportunity-list')}}"><i class="fa fa-circle-o"></i> Opportunities</a></li>
                </ul>
              </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-table"></i> <span>Pending</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/pending')}}"><i class="fa fa-circle-o"></i> Users</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-file"></i> <span>Reports</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/transcript')}}"><i class="fa fa-circle-o"></i>Transcript</a></li>
              </ul>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/tracked_hours')}}"><i class="fa fa-circle-o"></i>Tracked Hours</a></li>
              </ul>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/voluntier_organization')}}"><i class="fa fa-circle-o"></i>Voluntier Organization</a></li>
              </ul>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/organization_opportunity')}}"><i class="fa fa-circle-o"></i>Organization Opportunity</a></li>
              </ul>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/member_total_hours')}}"><i class="fa fa-circle-o"></i>Member Total Hours</a></li>
              </ul>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/organization_member')}}"><i class="fa fa-circle-o"></i>Organization Member</a></li>
              </ul>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/organization_tracked_hours')}}"><i class="fa fa-circle-o"></i>Organization Tracked Hours</a></li>
              </ul>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/organization_groups')}}"><i class="fa fa-circle-o"></i>Organization Groups</a></li>
              </ul>
              <ul class="treeview-menu">
                <li><a href="{{url('/admin/organization_groups_update')}}"><i class="fa fa-circle-o"></i>Organization Groups Update</a></li>
              </ul>
            </li>

            <li class="treeview {{ Route::is('admin.sitecontent') ||
                        Route::is('admin.email.edit') ||
                        Route::is('clear.test.data') ||
                        Route::is('add.dummy.hours.get') ||
                        Route::is('admin.email.index') ? 'active' : '' }}"
            >

                  <a href="#">
                      <i class="fa fa-gear"></i> <span>Site Setting</span>
                      <i class="fa fa-angle-left pull-right"></i>
                  </a>

                  <ul class="treeview-menu">
                      <li>
                          <a href="{{route('admin.email.index')}}">
                              <i class="fa fa-envelope"></i> Emails/Text
                          </a>
                      </li>
                        <li>
                          <a href="{{route('admin.sitecontent')}}">
                              <i class="fa fa-circle-o"></i> Sitecontent
                          </a>
                      </li>
                      <li>
                          <a href="{{route('admin.attributes')}}">
                              <i class="fa fa-circle-o"></i> Attributes
                          </a>
                      </li>
                      <li>
                          <a href="{{route('log-viewer::dashboard')}}">
                              <i class="fa fa-bar-chart"></i>
                              MyVol Logs Viewer
                          </a>
                      </li>

                      <li>
                          <a href="{{route('clear.test.data')}}">
                              <i class="fa fa-remove"></i>
                              Clear Test Data
                          </a>
                      </li>

                      <li>
                          <a href="{{route('add.dummy.hours.get')}}">
                              <i class="fa fa-users"></i>
                              Add Bulk Hours
                          </a>
                      </li>
					
                </ul>
            </li>
			<li class="treeview">
                  <a href="#">
                      <i class="fa fa-support"></i> <span>Extra Links</span>
                      <i class="fa fa-angle-left pull-right"></i>
                  </a>

                  <ul class="treeview-menu">
                      <li>
                          <a href="{{env('FRESH_DESK_LINK')}}" target="_blank">
                              <i class="fa fa-desktop"></i> FreshDesk 
                          </a>
                      </li>

                      <li>
                          <a href="{{env('FRESH_CHAT_LINK')}}" target="_blank">
                              <i class="fa fa-wechat"></i>
                              FreshChat
                          </a>
                      </li>

                      <li>
                          <a href="{{env('LOG_ROCKET_LINK')}}" target="_blank">
                              <i class="fa fa-history"></i> Log Rocket</a>
                      </li>
                      <li>
                          <a href="{{env('JIRA_LINK')}}" target="_blank">
                              <i class="fa fa-list-alt"></i> Jira</a>
                      </li>
                      <li>
                          <a href="{{env('BITBUCKET_LINK')}}" target="_blank">
                              <i class="fa fa-bitbucket"></i> Bitbucket</a>
                      </li>
                </ul>
            </li>

          </ul>
        </section>
      </aside>