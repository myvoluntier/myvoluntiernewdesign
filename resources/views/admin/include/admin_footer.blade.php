
   <footer class="main-footer">
    <p class="landing-copy-right"></p>
     <?php
           try {
               echo '<pre>';
               foreach(file("http://".$_SERVER['HTTP_HOST']."/build_id.txt") as $line) {
                   print_r($line);
               }
               $modifiedDate = date("F d Y H:i:s", filemtime($_SERVER['DOCUMENT_ROOT']."/build_id.txt"));
               echo 'BUILD_MODIFIED_DATE = '.$modifiedDate;
           }
           catch(Exception $e) {
               echo 'Message: ' .$e->getMessage();
           }
		  Session::put('firstTime', '');
    ?>
   </footer>

  </div>
  <!-- jQuery 2.1.4 -->
    <script src="{{asset('admin/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('admin/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('front-end/js/select2.full.js')}}"></script>

   <!-- SlimScroll -->
    <script src="{{asset('admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('admin/plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('admin/dist/js/app.min.js')}}"></script>

    <script src="{{asset('js/global.js')}}"></script>

   <script src="{{asset('admin/plugins/bootstraptoggle/bootstrap-toggle.min.js')}}"></script>

   <script src="{{asset('js/jquery.multi-select.js')}}"></script>


   <!-- LogRocket for log user activity -->
  <script src="https://cdn.logrocket.io/LogRocket.min.js" crossorigin="anonymous"></script>
  <script>
    window.LogRocket && window.LogRocket.init('<?php echo env('LOGROCKET_KEY');?>');
    LogRocket.identify('<?php echo Auth::guard('admin')->user()->id;?>', {
      name: 'Admin',
      email: '<?php echo Auth::guard('admin')->user()->username;?>',

      // Add your own custom user variables here, ie:
      subscriptionType: 'pro'
    });

  </script>


   @include('admin.include.toaster-js')

   <script type="text/javascript">

       jQuery(document).ready(function () {
           $("#clearUsersSelect").select2();


           $('#time_block').multiSelect({
               afterSelect: function(values){
                   var mins = $('#time_block').val().length * 30;
                   $('#hours_mins').text("(" + mins + " minutes)");
               },
               afterDeselect: function(values){
                   var mins = $('#time_block').val().length * 30;
                   $('#hours_mins').text("(" + mins + " minutes)");
               }
           });

           $(".select2Cls").select2();

           function inProgress() {
               $('#example4_wrapper').addClass("in-progress");
               $('#img_spin').show();
           }

           function releaseProgress() {
               $('#example4_wrapper').removeClass("in-progress");
               $('#img_spin').hide();
           }

           //for the volunteer and organization statuses update
           $('.status').on('change', function () {
               inProgress();

               var frm = $(this).closest('form');
               frm.submit();

               {{--$.ajaxSetup({--}}
               {{--headers: {--}}
               {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
               {{--}--}}
               {{--});--}}

               {{--var type = "POST";--}}

               {{--id = $(this).attr('data-id');--}}
               {{--column = $(this).attr('data-type');--}}
               {{--ischecked = $(this).is(':checked') ? 1 : 0;--}}

               {{--var formData = { 'value':ischecked , 'column' : column , 'id' : id,--}}
               {{--"_token": "{{ csrf_token() }}"--}}
               {{--};--}}
               {{--var action = '{{route('user.statuses.update')}}';--}}

               {{--$.ajax({--}}
               {{--type: type,--}}
               {{--url: action,--}}
               {{--data: formData,--}}
               {{--error: function() {},--}}
               {{--success: function(data) {--}}
               {{--releaseProgress();--}}
               {{--if(data.success)--}}
               {{--toastr.success("Users data has been updated.", "Success!");--}}
               {{--else--}}
               {{--toastr.error("Something went wrong check Logs.", "Error!");--}}
               {{--}--}}
               {{--});--}}

           });

           $("#example1").DataTable();

           $('#example2').DataTable({
               "paging": true,
               "lengthChange": false,
               "searching": false,
               "ordering": true,
               "info": true,
               "autoWidth": false
           });

           $('#example3').DataTable({
               "paging": true,
               "lengthChange": false,
               "searching": false,
               "ordering": false,
               "info": true,
               "autoWidth": false
           });

           $('#example4').DataTable({
               "paging": true,
               "lengthChange": false,
               "searching": true,
               "ordering": false,
               "info": true,
               "autoWidth": false
           });

           $('#reports_table').DataTable({
               "paging": true,
               "lengthChange": false,
               "searching": true,
               "ordering": true,
               "info": true,
               "autoWidth": false
           });

       });
   </script>
   <script>
       $(document).ready(function () {
           var d = new Date();
           var n = d.getFullYear();
           $('.landing-copy-right').html('<strong>MyVoluntier Operations LLC &copy; ' + n + '</strong>.');
           setTimeout(function () {
               $('.alert-success').fadeOut('fast');
           }, 3000);
       });
   </script>
 <script>
        $(document).ready(function(){ $('.newclicknav').click(function(){ $('body').toggleClass('sidebar-open'); });});

        $(document).ready(function(){ $('.mainmenuclicknav').click(function(){ $('body').toggleClass('mainmenu-open'); });});
    </script>