@include('admin.include.admin_header')
@include('admin.include.admin_head_nav')

@include('admin.include.admin_side_nav')
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Opportunity
            <a class="btn btn-app" href="{{route('move-opportunity-images')}}">
                <i class="fa fa-repeat"></i> Update Opportunity logos from Directory to S3.
            </a>
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Opportunity</li>
        </ol>
    </section>

    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @if(Session::has('success'))
                        <div class="alert alert-success">{{Session::get('success')}}</div>
                    @endif
                    <div class="box-header">
                        <h2 class="box-title text-success">Manage Opportunity</h2>
                        <a class="btn btn-danger pull-right" href="{{route('opportunity-delete-in-active')}}">
                            Delete Oppr of in active users ({{$inActiveCount}} Found)</a>
                    </div>

                    <div class="box-body">
                        <table id="example1" class="table  ">
                            <thead>
                            <td>Id</td>
                            <td>Name</td>
                            <td>Type</td>
                            <td>Category Name</td>
                            <td>Organization Name</td>
                            {{--<td>Activity</td>--}}
                            <td>Zipcode</td>
                            <td>Status</td>
                            <td width="80">Action</td>
                            </thead>
                            <tbody>

                            @if($oppr_list->count()>0)
                                @foreach($oppr_list as $each)
                                    <tr>
                                        <td>{{$each->id}}</td>
                                        <td><?php echo wordwrap(ucwords($each->title), 40, '<br>', true); ?></td>
                                        <td>@if($each->type == 1) Public @else Private @endif</td>
                                        <td>{{$each->opportunity_type}}</td>
                                        <td>{{$each->org_name}}</td>
                                        {{--<td>{{$each->activity}}</td>--}}
                                        {{--<td>@if($each->type == 1){{$each->street_addr1}}, {{$each->city}}, {{$each->state}}, United State @endif</td>--}}
                                        <td>{{$each->zipcode}}</td>
                                        <td>@if($each->end_date > date("Y-m-d")) Active @else Expired @endif</td>
                                        <td>
                                            @if($each->is_deleted!=1)
                                                <a href="{{url('/admin/opportunity-edit')}}/{{$each->id}}"><i
                                                            class="fa fa-edit"></i></a>
                                                <a href="{{url('/admin/opportunity-delete')}}/{{$each->id}}"
                                                   onclick="return confirm('Are you sure you want to delete this opportunity?');"
                                                   class="actnbtn"><i class="fa fa-trash"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('admin.include.admin_footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>