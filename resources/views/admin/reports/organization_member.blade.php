@include('admin.include.admin_header')

@include('admin.include.admin_head_nav')
<!-- Left side column. contains the logo and sidebar -->
@include('admin.include.admin_side_nav')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reports
           <!--  <small>advanced tables</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h2 class="box-title text-success">Organization Member</h2>
                        <a class="btn btn-success" style="float: right;margin-left:10px" href="{{url('admin/organization_member_csv')}}">Export CSV</a>
                        <a class="btn btn-success" style="float: right" href="{{url('admin/organization_member_pdf')}}">Export PDF</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="reports_table" class="table">
                            <thead>
                                <td>Sr No.</td>
                                <td>Organization Name</td>
                                <td>Member Name</td>
                                <td>Joining Date</td>
                            </thead>

                            <tbody>
                                @if(isset($users) && $users->count()>0)
                                @php $count = 1; @endphp
                                    @foreach($users as $user)
                                        <tr>
                                            <td> {{ $count }}</td>
                                            <td> {{ $user->org_name }} </td>
                                            @if($user->member_name!='')
                                                <td> {{ $user->member_name }}</td>
                                            @else
                                                <td style="color:red"> No data found</td>
                                            @endif
                                            @if($user->join_date!='')
                                                <td> {{ $user->join_date }}</td>
                                            @else
                                                <td style="color:red"> No data found</td>
                                            @endif
                                        </tr>
                                        @php $count++; @endphp
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Modal Edit table -->
    <!-- Large modal -->



    @include('admin.include.admin_footer')