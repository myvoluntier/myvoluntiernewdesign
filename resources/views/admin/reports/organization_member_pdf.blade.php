<div class="logo"><img src="http://stage.myvoluntier.com/front-end/img/logo.png" ></div>
    <style>
    .logo {
    width:100px;
    height:100px;
    margin:10px auto;
    overflow:hidden;
    }
    .logo img {
    display:block;
    margin: 0 auto;
    max-width:100%;
    }
    .table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
    background-color: transparent;
    border-spacing: 0;
    border-collapse: collapse;
    font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-weight: 400;
    font-size:14px;
    }
    .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 0;
    }
    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: middle;
    border-top: 1px solid #ddd;
    }    

    </style>

    <table id="reports_table" class="table">
    <thead>
    <tr>
    <td>Sr No.</td>
    <td>Organization Name</td>
    <td>Member Name</td>
    <td>Joining Date</td>
    </tr>
    </thead>

    <tbody>
    @if(isset($users) && $users->count()>0)
    @php $count = 1; @endphp
    @foreach($users as $user)
    <tr>
        <td> {{ $count }}</td>
        <td> {{ $user->org_name }} </td>
        @if($user->member_name!='')
            <td> {{ $user->member_name }}</td>
        @else
            <td style="color:red"> No data found</td>
        @endif
        @if($user->join_date!='')
            <td> {{ $user->join_date }}</td>
        @else
            <td style="color:red"> No data found</td>
        @endif
    </tr>
    @php $count++; @endphp
    @endforeach
    @endif
    </tbody>
    </table>