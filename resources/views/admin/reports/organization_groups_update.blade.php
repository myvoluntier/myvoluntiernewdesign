@include('admin.include.admin_header')

@include('admin.include.admin_head_nav')
<!-- Left side column. contains the logo and sidebar -->
@include('admin.include.admin_side_nav')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#organizations").change(function(){
        var organizationid = $(this).children(":selected").attr("id");
        var organizationname = $(this).children(":selected").text();
        $("#org_name").val(organizationname);
        groups(organizationid);
    });
});

function groups(oid)
{
    $("#groups").empty();
    if(oid==0)
    {
        $("#groups").prop("disabled",true);
    }
    else
    {
        var currenttoken = $("input[name=_token]").val();
        $("#groups").prop("disabled",false);
        $.ajax({
            type:'POST',
            url:'organization_groups_get',
            data:{oid:oid, _token:currenttoken},
            dataType:'json',
            success:function(data){ 
                var count = Object.keys(data).length;
                if(count > 0)
                {
                    $.each(data,function(i,item){
                        $("#groups").append('<option id="'+data[i].id+'">'+data[i].name+'</option>');
                    });
                    $(':input[type="submit"]').prop('disabled', false);
                    $("#groups").change(function() {
                        var id = $(this).children(":selected").attr("id");
                        var name = $(this).children(":selected").text();
                        $("#group_name").val(name);
                        $("#org_type").val("Educational Institute");
                        $("#group_id1").val(id);
                    });
                    
                }
                else
                {
                    $("#groups").append('<option>No Groups Found...</option>'); 
                    $(':input[type="submit"]').prop('disabled', true);
                }
            },
            complete:function(){
            }
        });
    }
}
</script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reports
           <!--  <small>advanced tables</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h2 class="box-title text-success">Organization Groups</h2>
                        @if(isset($temp) && $temp->group_name!=NULL)
                        <a class="btn btn-success" style="float: right;margin-left:10px" href="{{url('admin/organization_groups_update_csv/'.$temp->organization_name.'/'.$temp->organization_type.'/'.$temp->group_name.'/'.$temp->group_id)}}">Export CSV</a>
                        <a class="btn btn-success" style="float: right" href="{{url('admin/organization_groups_update_pdf/'.$temp->organization_name.'/'.$temp->organization_type.'/'.$temp->group_name.'/'.$temp->group_id)}}">Export PDF</a>
                        @endif
                    </div>
                    {{ csrf_field() }}
                    <div class="box-body">
                        @if(isset($organizations))
                            <h4 style="color:green">ORGANIZATIONS</h4>
                            <select class="form-control" id="organizations" class="organizations">
                            <option id="0">Select Organization</option>
                            @foreach($organizations as $o)
                                <option id="{{$o->id}}">{{$o->org_name}}</option>
                            @endforeach
                            </select>
                        @endif
                        <h4 style="color:green">GROUPS</h4>
                        <select class="form-control" id="groups" class="groups" disabled>
                        </select>
                        <br>
                        <form method="post" action="show_users">
                        {{ csrf_field() }}
                            <input type="hidden" id="group_id1" name="groupid1" class="group_id1" value="">
                            <input type="hidden" id="org_name" name="org_name" class="org_name" value="">
                            <input type="hidden" id="org_type" name="org_type" class="org_type" value="">
                            <input type="hidden" id="group_name" name="group_name" class="group_name" value="">
                            <input type="submit" value="Show Users" class="btn btn-success" style="float:right" disabled>
                        </form>

                        <table id="reports_table" class="table">
                            <thead>
                                <td>Sr No.</td>
                                <td>Organization Name</td>
                                <td>Organization Type</td>
                                <td>Group Name</td>
                                <td>Member Names</td>
                            </thead>

                            <tbody>
                                @if(isset($temp) && count($users)>0)
                                @php $count = 1; @endphp
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$temp->organization_name}}</td>
                                    <td>{{$temp->organization_type}}</td>
                                    <td>{{$temp->group_name}}</td>
                                    <td>
                                    @foreach($users as $u)
                                        @if($u->first_name!='' && $u->first_name!=NULL)
                                        {{$u->first_name}} {{$u->last_name}} <br>
                                        @else
                                        {{$u->org_name}} <br>
                                        @endif
                                    @endforeach
                                    </td>
                                </tr>
                                @php $count++; @endphp
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('admin.include.admin_footer')