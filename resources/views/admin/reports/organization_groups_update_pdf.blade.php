<div class="logo"><img src="http://stage.myvoluntier.com/front-end/img/logo.png" ></div>
<style>
.logo {
    width:100px;
    height:100px;
    margin:10px auto;
    overflow:hidden;
    }
    .logo img {
    display:block;
    margin: 0 auto;
    max-width:100%;
    }
    .table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
    background-color: transparent;
    border-spacing: 0;
    border-collapse: collapse;
    font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-weight: 400;
    font-size:14px;
    }
    .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
    border-top: 0;
    }
    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: middle;
    border-top: 1px solid #ddd;
    }    

</style>

<table id="reports_table" class="table">
    <thead>
    <tr>
        <td>Sr No.</td>
        <td>Organization Name</td>
        <td>Organization Type</td>
        <td>Group Name</td>
        <td>Members Name</td>
    </tr>
    </thead>
    <tbody>
        @if(isset($temp))
        @php $count = 1; @endphp
                <tr>
                    <td>{{$count}}</td>
                    <td>{{$temp->organization_name}} </td>
                    <td>{{$temp->organization_type}}</td>
                    <td>{{$temp->group_name}}</td>
                    <td>
                    @if(isset($users) && count($users) > 0)
                        @foreach($users as $u)
                            @if($u->first_name!='' && $u->first_name!=NULL)
                            {{$u->first_name}} {{$u->last_name}} <br>
                            @else
                            {{$u->org_name}} <br>
                            @endif
                        @endforeach
                    @endif
                    </td>
                </tr>
                @php $count++; @endphp
        @endif
    </tbody>
</table>