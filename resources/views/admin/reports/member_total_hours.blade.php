@include('admin.include.admin_header')

@include('admin.include.admin_head_nav')
<!-- Left side column. contains the logo and sidebar -->
@include('admin.include.admin_side_nav')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reports
           <!--  <small>advanced tables</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            
            <li class="active">List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h2 class="box-title text-success">Member Opportunity Tracked Hours</h2>
                        <a class="btn btn-success" style="float: right;margin-left:10px" href="{{url('admin/member_total_hours_csv')}}">Export CSV</a>
                        <a class="btn btn-success" style="float: right" href="{{url('admin/member_total_hours_pdf')}}">Export PDF</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="reports_table" class="table">
                            <thead>
                                <td>Sr No.</td>
                                <td>Opportunity</td>
                                <td>Member</td>
                                <td>Total Tracked Hours</td>
                            </thead>

                            <tbody>
                                @if(isset($data) && count($data)>0)
                                @php $count = 1; @endphp
                                    @foreach($data as $d)
                                        <tr>
                                            <td>{{ $count }}</td>
                                            @if($d->title!='')
                                            <td> {{$d->title}} </td>
                                            @else
                                            <td style="color:red">No Data Found</td>
                                            @endif
                                            @if($d->member_name!='')
                                            <td> {{$d->member_name}} </td>
                                            @else
                                            <td style="color:red">No Data Found</td>
                                            @endif
                                            <td> {{ $d->logged_hours }}</td>
                                        </tr>
                                        @php $count++; @endphp
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Modal Edit table -->
    <!-- Large modal -->



    @include('admin.include.admin_footer')