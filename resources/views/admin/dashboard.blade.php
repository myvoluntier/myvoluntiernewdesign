@include('admin.include.admin_header')

@include('admin.include.admin_head_nav')
@include('admin.include.admin_side_nav')


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <a href="{{url('/admin/volunteer-list')}}">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Manage Volunteers</span>
                            <span class="info-box-number">{{$count['volunteer']}}</span>
                        </div>
                    </div>
                </div>
            </a>

            <a href="{{url('/admin/organization-list/all')}}">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Manage Organizations</span>
                            <span class="info-box-number">{{$count['organization']}}</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>


        <a class="btn btn-app" href="{{route('move-users-images')}}">
            <i class="fa fa-repeat"></i> Update user images from Directory to S3.
        </a>
        <br/>
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$count['volunteer']}}</h3>
                        <p>Manage Volunteers</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{url('/admin/volunteer-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$count['organization']}}</h3>
                        <p>Manage Organizations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{url('/admin/organization-list/all')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">

                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$count['public_opportunity']}}</h3>
                        <p>Public Opportunities</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{url('/admin/opportunity-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$count['private_opportunity']}}</h3>
                        <p>Private Opportunities</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="{{url('/admin/opportunity-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

        </div>

        <br/><br/>

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box box-success">
                <div class="box-header">
                    <i class="fa fa-envelope"></i>
                    <h3 class="box-title">Send Quick Email</h3>
                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-success btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                    <form action="{{route('admin-send-email')}}" method="post">
                        <div class="box-body">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input required type="email" class="form-control" name="email_to" placeholder="Email to:">
                            </div>
                            <div class="form-group">
                                <input required type="text" class="form-control" name="subject" placeholder="Subject">
                            </div>
                            <div>
                                <textarea required placeholder="Message" name="body" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            </div>

                        </div>
                        <div class="box-footer clearfix">
                            <button type="submit" class="pull-right btn btn-success">Send Now
                                <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </form>
            </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-comment"></i>
                        <h3 class="box-title">Quick SMS</h3>

                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-success btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>

                    <form action="{{route('admin-send-sms')}}" method="post">
                        <div class="box-body">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input required type="text" value="+18033618821" class="form-control" name="sms_to" placeholder="SMS to +18033618821">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" readonly placeholder="Subject">
                            </div>
                            <div>
                                <textarea required name="body" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                            </div>
                        </div>

                        <div class="box-footer clearfix">
                            <button type="submit" class="pull-right btn btn-success" id="sendEmail">Send Now
                                <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@include('admin.include.admin_footer')
   