<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MyVoluntier | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('front-end/img/favicn.png')}}" type="image/gif" sizes="16x16">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('admin/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('admin/plugins/datatables/dataTables.bootstrap.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/skins/_all-skins.min.css')}}">

    <link href="{{asset('front-end/css/jquery.bxslider.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/select2-bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/jquery.bxslider.css') }}" rel="stylesheet">
    <link href="{{asset('front-end/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/bootstrap-datepicker.css')}}" rel="stylesheet">
    <link href="{{asset('front-end/css/bootstrap-sortable.css')}}" rel="stylesheet" >
    <link href="<?=asset('front-end/css/main.css')?>" rel="stylesheet">

    <style>
        header{
            padding: 0 !important;
        }
    </style>
    @yield('admin_css')
</head>
@yield('admin_body')
</html>