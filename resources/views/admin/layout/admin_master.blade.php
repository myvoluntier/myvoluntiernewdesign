{{--ONLY USED ON OPPRTUNTY PAGE TOO MUCH CONFUSION ON THOSE ADMIN PAGES TEMPELATES--}}

@extends('admin.layout.admin_frame')

@section('admin_body')
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    @include('admin.include.admin_head_nav')
    @include('admin.include.admin_side_nav')
    @yield('admin_content')
    @include('admin.include.admin_footer')
    </div>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('front-end/js/popper.js')}}"></script>
    <script src="{{asset('front-end/js/select2.full.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap.js')}}"></script>
    <script src="{{asset('front-end/js/jquery.bxslider-rahisified.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap-slider.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/check_validate.js') }}"></script>
    <script src="{{asset('front-end/js/bootstrap-sortable.js')}}"></script>

    @yield('admin_script')

    </body>
@endsection