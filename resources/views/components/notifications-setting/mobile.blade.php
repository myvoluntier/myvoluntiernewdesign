<div class="wrapper-messages-box wrapper-account-setting-box">
<form>
    <div class="col-md-12">
        <div class="row">
            <label>
                <input data-id="all_emails_send" class="notifications_setting" {{Auth::user()->all_emails_send=='1' ? 'checked' : ''}} type="checkbox">&nbsp; Disable All Email Notifications
            </label>
            <label>
                <input data-id="all_texts_send" class="notifications_setting" {{Auth::user()->all_texts_send=='1' ? 'checked' : ''}} type="checkbox">&nbsp; Disable All Text Notifications
            </label>
        </div>
        <hr>
        <div class="row">
            <div class="col-8">Type</div>
            <div class="col-2">Mail</div>
            <div class="col-2">Text</div>
        </div>

        @include('components.notifications-setting.rows')

    </div>
</form>

</div>
