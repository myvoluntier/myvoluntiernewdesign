<div class="row" style="min-width: 420px; padding: 5px 5px 2px; overflow: hidden;">
    <form>
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-12">
                    <label>
                        <input data-id="all_emails_send" class="notifications_setting" {{Auth::user()->all_emails_send=='1' ? 'checked' : ''}} type="checkbox">&nbsp; Disable All Email Notifications
                    </label>
                </div>
                <div class="col-sm-12">
                    <label>
                        <input data-id="all_texts_send" class="notifications_setting" {{Auth::user()->all_texts_send=='1' ? 'checked' : ''}} type="checkbox">&nbsp; Disable All Text Notifications
                    </label>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-sm-8"><b>Type</b></div>
                <div class="col-sm-2"><b>Email</b></div>
                <div class="col-sm-2"><b>Texts</b></div>
            </div>

            @include('components.notifications-setting.rows')

        </div>
    </form>
</div>