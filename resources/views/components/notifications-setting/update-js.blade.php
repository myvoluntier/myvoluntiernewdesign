<script>
    $(document).ready(function() {

        $('.notifications_setting').change(function() {

            yesNo = +$(this).is( ':checked' );
            name = $(this).data("id");

            $.ajax({
                url: '{{ route('user.notifications.setting.post') }}',
                type: 'post',
                cache: false,
                data: {
                    status: yesNo,name:name,
                    _token: '{{ csrf_token() }}'
                },
                beforeSend: function () {},

                success: function (result) {
                    if (result.success) {
                        toastr.success("Your notifications settings has been updated.", "Success");
                    }
                    else
                        toastr.error("Something went wrong please try again later.", "Error");
                }
            });

        });

    });
</script>
