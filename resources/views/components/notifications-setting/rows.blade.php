<div class="row mt-1">
    <div class="col-8">Join Opportunity
        @if(Auth::user()->org_type === null)
            <small>(approved/denied)</small>
        @else
            Requests
        @endif
    </div>
    <div class="col-2 text-center">
        <label><input data-id="join_oppr_email" class="notifications_setting" {{Auth::user()->join_oppr_email=='1' ? 'checked' : ''}} type="checkbox"></label>
    </div>
    <div class="col-2 text-center">
        <label><input data-id="join_oppr_text" class="notifications_setting" {{Auth::user()->join_oppr_text=='1' ? 'checked' : ''}} type="checkbox"></label>
    </div>
</div>

<div class="row">
    <div class="col-8">Friend Request</div>
    <div class="col-2 text-center">
        <label><input data-id="friend_req_email" class="notifications_setting" {{Auth::user()->friend_req_email=='1' ? 'checked' : ''}} type="checkbox"></label>
    </div>
    <div class="col-2 text-center">
        <label><input data-id="friend_req_text" class="notifications_setting" {{Auth::user()->friend_req_text=='1' ? 'checked' : ''}} type="checkbox"></label>
    </div>
</div>

<div class="row">
    <div class="col-8">
        @if(Auth::user()->org_type === null)
            Tracked Hours <small>(approved/denied)</small>
        @else
            Confirm Tracked Hours Request
        @endif
    </div>

    <div class="col-2 text-center">
        <label><input data-id="track_hour_email" class="notifications_setting" {{Auth::user()->track_hour_email=='1' ? 'checked' : ''}} type="checkbox"></label>
    </div>
    <div class="col-2 text-center">
        <label><input data-id="track_hour_text" class="notifications_setting" {{Auth::user()->track_hour_text=='1' ? 'checked' : ''}} type="checkbox"></label>
    </div>
</div>

<div class="row">
    <div class="col-8">New Chat Message</div>
    <div class="col-2 text-center">
        <label><input data-id="chat_msg_email" class="notifications_setting" {{Auth::user()->chat_msg_email=='1' ? 'checked' : ''}} type="checkbox"></label>
    </div>
    <div class="col-2 text-center">
        <label><input data-id="chat_msg_text" class="notifications_setting" {{Auth::user()->chat_msg_text=='1' ? 'checked' : ''}} type="checkbox"></label>
    </div>
</div>