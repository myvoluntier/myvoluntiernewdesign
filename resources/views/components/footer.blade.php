@include('footer_includes.terms_condition')
@include('footer_includes.privacy_policy')
<footer>
    <style type="text/css">
        .footer_link span:first-child {
            float: none;
            padding: 0;
        }
        .footer_link span {
            float: right;
            padding: 0 5px;
        }
        .footer_link span a{
            color: #212529;
        }
        .footer_link span a:hover{
            color: #3bb44a;
        }
    </style>

    <div class="container">
        <div class="row align-items-center">
            <div class="col footer_link">
                <span class="copyright"> MyVoluntier Operations LLC &copy; {{date('Y')}} {{session('is_delegate')}}</span>
                <span><a href="{{route('privacyPolicy')}}" >Privacy Policy</a></span>
                <span><a href="{{route('termsAndConditions')}}" >Terms And Conditions</a></span>
            </div>
            <div class="col col-auto">
                <p class="social">
                    <a target="_blank" href="http://www.twitter.com/myvoluntier"><span class="gl-icon-twitter-circular"></span></a>
                    <a target="_blank" href="http://www.instagram.com/myvoluntier"><span class="gl-icon-instagram-circular"></span></a>
                    <a target="_blank" href="http://www.facebook.com/myvoluntier"><span class="gl-icon-facebook-circular"></span></a>
                </p>
            </div>
            
            <!--<div class="col col-auto">
                <span id="siteseal"><script async="" type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=feBQ2MhPIWtLZBTICZVOoKTTEyQsvv1PxbLX5OJQs20cbQ2w45itVqVYnxck"></script></span>        
            </div>--> 
            
        </div>
    </div>
</footer>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-126568283-1', 'auto');
    ga('send', 'pageview');

</script>