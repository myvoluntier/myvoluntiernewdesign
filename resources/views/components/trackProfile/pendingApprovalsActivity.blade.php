<table id="tbl_confirm_tracking" class="table sortable table_my_blk">
    <thead>
    <tr>
        <th class="opportunity_blk">
            <div class="main-text"><p>Opportunity</p></div>
        </th>
        <th class="worked_date_blk">
            <div class="main-text"><p>Worked Date</p></div>
        </th>
        <th class="clock_in_blk">
            <div class="main-text"><p>Clock In</p></div>
        </th>
        <th class="clock_blk">
            <div class="main-text"><p>Clock Out</p></div>
        </th>
        <th class="mins_blk">
            <div class="main-text"><p>Mins</p></div>
        </th>
        <th class="submitted_blk">
            <div class="main-text"><p>Submitted Time</p></div>
        </th>
        <th>
            <div class="main-text"><p>Status</p></div>
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($tracks as $tr)
        <tr>
            <td data-label="Opportunity" class="opportunity_blk">
                @if(if_route_pattern('view-voluntier-profile'))
                    @if($tr->link !== 0)
                        @if($tr->oppor_id != 0)
                        <a class="pending_approvals_blk" href="{{url('/')}}/volunteer/view_opportunity/{{$tr->oppor_id}}">
                            @if($tr->opportunity->logo_img == null)
                                <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')">
                                    <span></span>
                                </div>
                            @else
                                <div class="avatar" style="background-image:url('{{$tr->opportunity->logo_img}}')">
                                    <span></span>    
                                </div>
                            @endif
                            <div class="main-text"><p>{{$tr->oppor_name}}</p></div>
                        </a>
                        @else
                            <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')">
                                <span></span>
                            </div>
                            <div class="main-text"><p>{{$tr->oppor_name}}</p></div>
                        @endif
                    @else
                        <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')">
                            <span></span>
                        </div>
                        <div class="main-text"><p>{{$tr->oppor_name}}</p></div>
                    @endif
                @else
                    @if($tr->link != 0)
                        <a class="pending_approvals_blk" href="{{url('/')}}/volunteer/view_opportunity/{{$tr->oppor_id}}">

                            @if($tr->opp_logo == null)
                                <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')">
                                    <span></span>
                                </div>
                            @else
                                <div class="avatar" style="background-image:url('{{$tr->opp_logo}}')">
                                    <span></span>
                                </div>
                            @endif
                            <div class="main-text"><p>{{$tr->oppor_name}}</p></div>
                        </a>
                    @else
                        <div class="avatar" style="background-image:url('{{asset('front-end/img/org/001.png')}}')">
                            <span></span>
                        </div>
                        <div class="main-text"><p>{{$tr->oppor_name}}</p></div>
                    @endif
                @endif
            </td>
            <td data-dateformat="YYYY-MM-DD" data-label="Worked Date">
                <div class="main-text"><p class="light">{{date('m-d-Y',strtotime($tr->logged_date))}}</p></div>
            </td>
            <td data-label="Clock In">
                <div class="main-text"><p class="light">{{$tr->started_time}}</p></div>
            </td>
            <td data-label="Clock Out">
                <div class="main-text"><p class="light">{{$tr->ended_time}}</p></div>
            </td>
            <td data-label="Mins">
                <div class="main-text"><p class="light">{{$tr->logged_mins}}</p></div>
            </td>
            <td data-dateformat="YYYY-MM-DD" data-label="Submitted Time">
                <div class="main-text"><p class="light">{{date('m-d-Y H:i:s',strtotime($tr->updated_at))}}</p></div>
            </td>

            <td data-label="Status">
                @if($tr->approv_status == 0)
                    <span class="badge badge-secondary"><i class="fa fa-clock-o"></i> Pending</span>
                @elseif($tr->approv_status == 1)
                    <span class="badge badge-success"><i class="fa fa-check"></i> Approved</span>
                @else
                    <span class="badge badge-danger"><i class="fa fa-close"></i> Declined</span>
                @endif
            </td>
        </tr>
    @endforeach

    </tbody>
    <!-- <tfoot>
    <tr>
        <td colspan="7" style="padding :20px 0 0"></td>
    </tr>
    </tfoot> -->
</table>

