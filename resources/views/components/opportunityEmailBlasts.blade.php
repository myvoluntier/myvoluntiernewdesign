<script src="https://cdn.ckeditor.com/4.11.3/standard-all/ckeditor.js"></script>
<div class="modal fade modal-popups" id="oppEmailBlast" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix modal-share-px">
            <div>
                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h2 class="h2">Opportunity Member<strong class="green"> Email Blast</strong></h2>
                            <!--<h3 class="h3">You can share your profile via email!</h3>-->
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body hide-email-comment">
                        <div class="form-group">
                            <label class="label-text">Message:</label>
                            <div class="wrapper_input ">
                                <textarea id="message_members" placeholder="You can enter message here"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div style="display:block;">

                        <div class="success-first" style="display: none">
                            <h3>Email has been successfully sent to opportunity members!!</h3>
                        </div>

                        <div class="wrapper-link top-50 text-right">
                            <a id="oppEmailBlast_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                            <a class="button-fill" id="oppEmailBlastBtn" href="#"><span>Send</span></a>
                            <a class="button-fill button-grey" style="display: none" id="oppEmailBlast_hide" href="#"><span>Close</span></a>
                        </div>
                            <input type="hidden" id = "opportunitiIdMail" value="0">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<script>
CKEDITOR.replace('message_members', {
    height: 200,
    removeButtons: '',
    extraPlugins: 'colorbutton,colordialog'
});
       
</script>