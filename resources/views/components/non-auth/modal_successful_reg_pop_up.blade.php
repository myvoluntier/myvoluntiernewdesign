<div class="modal fade" id="myModalSuccessfulReg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div>

                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h2 class="h2">Sign Up</h2>
                            <h3 class="h3">Welcome to MyVolun<strong class="green">tier</strong>!</h3>
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="label-text">Thanks for your registration</label>
                            <div class="text">
                                Please check your email to confirm your registration. If you do not see an email, please check your spam/junk folder for an email from support@myvoluntier.com. Add support@myvoluntier.com to your list of email contacts to ensure all future notifications are received.
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="wrapper-link">
                            <a class="close_open" href="#"><span>OK</span></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
