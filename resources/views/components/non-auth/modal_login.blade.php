<div class="modal fade" id="myModalLogin"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div>
                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h2 class="h2">SIGN UP/LOGIN</h2>
                            <h3 class="h3">Welcome to MyVolun<strong class="green">tier</strong>!</h3>
                        </div>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>
                    </div>

                    <div class="modal-body">
                        <div class="alert alert-danger login_error_div" role="alert" style="display: none">
                            <span id="login_errors_div"></span>
                        </div>
                        <div class="form-group">
                            <label class="label-text">Login as:</label>
                            <div class="wrapper_input">
                                <select id="loginAS"> 
                                    <option value="">Select</option>
                                    <option value="volunteer">Volunteer</option>
                                    <option value="organization">Organization</option>
                                    <option value="suborganization">Sub organization</option>
                                </select>
                            </div>
                            <div class="text-error-select"></div>
                        </div>


                        <div class="form-group organizationDivS" style="display:none;">
                            <label class="label-text">Choose Organization:</label>
                            <div class="wrapper_input newOrgWrapper">
                                <select id="organizationSelect">
                                    <option value="">Select Organization</option>
                                    @if(isset($allOrganizations))
                                    @foreach($allOrganizations as $val)
                                        <option value="{{$val['id']}}">{{$val['org_name']}}</option>
                                    @endforeach

                                    @endif
                                </select>
                            </div>
                            <div class="text-error"></div>
                        </div>

                        <div class="form-group parentOrganizationDiv" style="display:none;">
                            <label class="label-text">Parent Organization:</label>
                            <div class="wrapper_input parentOrgWrapper">
                                <select id="parentOrgSel"> 
                                    <option value="">Select</option>
                                    @if(!empty($parentOrg))
                                        @foreach($parentOrg as $val)
                                            <option value="{{$val['id']}}">{{$val['org_name']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="text-error"></div>
                        </div>

                        <div class="form-group parentOrganizationDiv" style="display:none;">
                            <label class="label-text">Sub Organization:</label>
                            <div class="wrapper_input subOrgWrapper">
                                <select id="subOrgSel" style="display:none;"> 
                                    <option value="">Select</option>
                                </select>
                            </div>
                            <div class="text-error"></div>
                        </div>
                        <div class="form-group loginSub loginEmail">
                            <label class="label-text">Email:</label>
                            <div class="wrapper_input">
                                <input  id="login_user" type="text" placeholder="Enter your email here" value="">
                            </div>
                            <div class="text-error"></div>
                        </div>
                        <div class="form-group loginSub loginPass">
                            <label class="label-text">Password:</label>
                            <div class="wrapper_input">
                                <input id="login_password" type="password" placeholder="" value="">
                            </div>
                            <div class="text-error"></div>
                        </div>
                        <input  id="parent_user_id" type="hidden" value="">
                    </div>

                    <div class="modal-footer">
                        <div class="main-text">
                            <p class="text-right" id="forPass">
                                <a href="#">Forgot Password</a>
                            </p>
                        </div>
                        <div class="wrapper-link two-link">
                            <a href="#" class="white registration_button_on_modal_form">
                                <span>Sign UP</span>
                            </a>
                            <a id="btn_login_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                            <a id="btn_login_l" href="#"><span>Log in</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>