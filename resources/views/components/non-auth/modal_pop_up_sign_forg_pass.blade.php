<div class="modal fade" id="myModalForgotPassword" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div>

                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h2 class="h2">Forgot Password?</h2>
                            <h3 class="h3">We will e-mail you a link to replace your password</h3>
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body">
                        <div class="form-group loginAsDiv">
                            <label class="label-text">Login as:</label>
                            <div class="wrapper_input">
                                <select id="loginASForg"> 
                                    <option value="">Select</option>
                                    <option value="volunteer">I am a volunteer</option>
                                    <option value="organization">I am an organization</option>
                                    <option value="sub-organization">I am a sub organization</option>
                                </select>
                            </div>
                            <div class="text-error-select"></div>
                        </div>

                        <div class="form-group">
                            <label class="label-text lab-email"><span id="forgetHeading"></span></label>

                            <div class="wrapper_input input-email forgot_pass_input_email" style="display: none">
                                <input id="get_password_email" type="text" placeholder="" value="">
                                <p class="p_invalid" id="forgot_password_invalid_email">Invalid Email Address.</p>
                            </div>

                            <div class="wrapper_input input-email forgot_pass_input_name" style="display: none">
                                <input id="get_password_name" type="text" placeholder="" value="">
                                <p class="p_invalid" id="forgot_password_invalid_name">Invalid Organization Name.</p>
                            </div>

                            <div class="col-md-12 reg-content forgot_password_success" style="display: none">
                                <p>We sent your Link via email. Please check your email!</p>
                            </div>
                            <div class="text-error"></div>
                        </div>

                    </div>
                    <div class="modal-footer">

                        <div class="wrapper-link">
                            <a id="forgot_password_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                            <a id="forgot_password" href="#"><span>Submit</span></a>
                            <a style="display: none" id="forgot_password_close" href="#"><span>Close</span></a>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>