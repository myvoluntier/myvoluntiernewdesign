<div class="form-group">
    <div class="main-text border-bottom">
        <h3 class="h3">Service Project Info</h3>
    </div>
</div>
<div class="row">
    <div class="col-12 col-md-12">

        <div class="form-group">
            <label class="label-text">Title : </label>
            <div class="wrapper_input">
                <strong style="font-size:20px;">{{ $serviceProject->title}}</strong>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-6">
        <div class="form-group">
            <label class="label-text">Range of Date : </label>
            <div class="wrapper_input fa-icons">
                {{date('m-d-Y',strtotime($serviceProject->start_date)) }}&nbsp;&nbsp;&nbsp; To &nbsp;&nbsp;&nbsp; {{ date('m-d-Y',strtotime($serviceProject->end_date))}}
            </div>

        </div>

    </div>

    <div class="col-12 col-md-12">
        <div class="form-group">
            <label class="label-text">Description : </label>
            <div class="wrapper_input">
				<?php echo $serviceProject->description; ?>
            </div>
        </div>

    </div>
    <div class="col-12 col-md-12">
        <div class="form-group">
            <label class="label-text">Outcome : </label>
            <div class="wrapper_input">
				<?php echo $serviceProject->outcome; ?>
			</div>
        </div>

    </div>

</div>


@if(!empty($trackOrgoIds))
<div class="form-group">
    <div class="main-text border-bottom">
        <h3 class="h3">Select the (confirmed) volunteer hours below to include in this Service Project:</h3>
    </div>
</div>

<div class="row">
    <div class="col-12 col-md-12">

        @include('volunteer.serviceProjectHours')

    </div>

</div>
@endif