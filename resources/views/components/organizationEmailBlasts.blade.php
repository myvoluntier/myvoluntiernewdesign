<style>
    .modal.fade .modal-dialog{
        -webkit-transform: translate(0, 0);
        transform: translate(0, 0);
    }
</style>
<script src="https://cdn.ckeditor.com/4.11.3/standard-all/ckeditor.js"></script>
<div class="modal fade" id="orgEmailBlast" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix modal-share-px">
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="main-text">
                            <h2 class="h2">Organization <strong class="green"> Email Blast</strong></h2>
                            <!--<h3 class="h3">You can share your profile via email!</h3>-->
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body hide-email-comment" id="selectTypeDiv">
                        <div class="form-group">
                            <label class="label-text">Select Type:</label>
                            <div class="wrapper_input ">
                                <select id="selectType">
                                    <option value="">Select</option>
                                    <option value="verifiedDomain">Verified Domain</option>
                                    <option value="groupEmailBlast">Groups</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="verifyDomainDiv">
                            <label class="label-text">Verified Domain:</label>
                            <div class="wrapper_input ">
                                <select id="verifiedDomain">
                                    <option value="">Select</option>
                                    @if($user->domain_1_confirm_code == 1)
                                        <option value="{{$user->domain_1}}">{{$user->domain_1}}</option>
                                    @endif
                                    @if($user->domain_2_confirm_code == 1)
                                        <option value="{{$user->domain_2}}">{{$user->domain_2}}</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="groupEmailBlastDiv">
                            <label class="label-text">Groups:</label>
                            <div class="wrapper_input ">
                                <select id="groupEmailBlast" multiple=true placeholder="Select">
                                    @foreach($group as $key=>$groupVal)
                                        <option value="{{$groupVal->id}}">{{$groupVal->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="label-text">Message:</label>
                            <div class="wrapper_input ">
                                <textarea id="message_vol_members" placeholder="You can enter message here"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="modal-body" style="display:block;">

                        <div class="success-first" style="display: none">
                            <h3>Email has been successfully sent to volunteer members!!</h3>
                        </div>

                        <div class="wrapper-link top-50">
                            <a id="orgEmailBlast_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                            <a id="orgEmailBlastBtn" href="#"><span>Send</span></a>
                            <a style="display: none" id="orgEmailBlast_hide" href="#"><span>Close</span></a>
                        </div>
                            <input type="hidden" id = "opportunitiIdMail" value="0">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<script>
CKEDITOR.replace('message_vol_members', {
    height: 200,
    removeButtons: '',
    extraPlugins: 'colorbutton,colordialog'
});
       
</script>