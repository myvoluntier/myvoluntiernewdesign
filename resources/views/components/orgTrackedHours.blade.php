@include('components.auth.modal_pop_up_view_hours_org' )
@include('components.auth.modal_pop_up_edit_hours_org' )
<div class="wrapper-friends">
    <div class="search-friends">
        <div class="container">
            <div class="search">
                <label><span class="gl-icon-search"></span></label>

                <input type="text" class="form-control m-b" id="filterTrack" onkeyup="searchName('filterTrack', 'tbl_org_tracking_hrs')" placeholder="Search in table">
            </div>

        </div>
    </div>
    <div class="print_blk_top">
        <a href="{{url('/organization/exportTrackedHours') }}" class="btn btn-success export_btn_blk">Export Tracked Hours</a>
        <a href="#" name="filter_print" onclick="printData()" class="btn btn-success"><i class="fa fa-print"></i></a>
    </div>
    <div class="wrapper-friends-list">
        <div class="container">
            <div class="wrapper-sort-table">
                <div >
                    <table id="tbl_org_tracking_hrs" class="table sortable confirm-table example5 table_my_blk all_same_txt" >
                        <thead>
                        <tr>
                            <th class="date_service">
                                <div class="main-text"><p>Date of Service</p></div>
                            </th>
                            <th class="hour_tracked">
                                <div class="main-text"><p>Hours Tracked</p></div>
                            </th>
                            <th class="opportunity_name">
                                <div class="main-text"><p>Opportunity Name</p></div>
                            </th>
                            <th class="volunteer_blk">
                                <div class="main-text"><p>Volunteer</p></div>
                            </th>
                            <th class="volunteer_email">
                                <div class="main-text"><p>Volunteer Email Address</p></div>
                            </th>
                            <th class="volunteer_phone">
                                <div class="main-text"><p>Volunteer Phone</p></div>
                            </th>
                            <th class="text-center action_blk" data-defaultsort="disabled">
                                <div class="main-text"><p>Action</p></div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            @if(!empty($trackedHours))
                                @foreach($trackedHours as $tc)
                                    <tr>
                                        <td data-label="Date of Service">
                                            <p>{{$tc['logged_date']}}</p>
                                        </td>
                                        <td data-label="Hours Tracked">
                                           <p class="light">{{($tc['logged_mins']/60)}}</p>
                                        </td>
                                        <td data-label="Opportunity Name">
                                            <p>{{$tc['opportunity_name']}}</p>
                                        </td>
                                        <td data-label="Volunteer">
                                            <p>{{$tc['volunteer_name']}}</p>
                                        </td>
                                        <td data-label="Volunteer Email Address">
                                            <p>{{$tc['volunteer_email']}}</p>
                                        </td>
                                        <td data-label="Volunteer Phone">
                                            <p>{{$tc['volunteer_phone']}}</p>
                                        </td>
                                        <td class="two_field_blk" data-label="Action">
                                            <a class="eidtTrackHours actionBtn btn btn-success action" data-trackid="{{$tc['track_id']}}">Edit</a>
                                            
                                            <a class="viewTrackHours actionBtn btn btn-success action" data-trackid="{{$tc['track_id']}}">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                               @else
                            @endif
                        </tbody>
                    </table>
                     <div style="padding-top:20px;display:none;" id="trackHour_paginate">
                     {{$trackedHours->fragment("tracked")}}
                </div> 
                </div>
              
            </div>

        </div>
    </div>
</div>
<input type="hidden" id="trackurl" value="{{url('api/organization/viewtrack/')}}">
<script>
    function printData()
    {
        window.print();
    }
</script>