<div class="wrapper-friends">
    <div class="search-friends">
        <div class="container">

            <div class="search">
                <label><span class="gl-icon-search"></span></label>
                <input type="text" class="form-control m-b" id="filter" onkeyup="searchName('filter', 'tbl_org_tracking')" placeholder="Search in table">
            </div>

        </div>
    </div>

    <div class="wrapper-friends-list">
        <div class="container">
            <div class="wrapper-sort-table">
                <div >
                    <table id="tbl_org_tracking" class="table sortable confirm-table table_my_blk all_same_txt2">
                        <thead>
                            <tr>
                        <th class="volunteer_blk">
                        <div class="main-text"><p>Volunteer</p></div>
                        </th>
                        <th class="opportunity_blk">
                        <div class="main-text"><p>Opportunity</p></div>
                        </th>
                        <th class="date_blk">
                        <div class="main-text"><p>Date</p></div>
                        </th>
                        <th class="mins_blk">
                        <div class="main-text"><p>Mins</p></div>
                        </th>
                        <th class="submitted_time_blk">
                        <div class="main-text"><p>Submitted Time</p></div>
                        </th>
                        <th class="text-center approve_blk" data-defaultsort="disabled">
                        <div class="main-text"><p>Approve / Decline</p></div>
                        </th>
                        </tr>
                        </thead>
                        <tbody>

                            @foreach($tracks as $t)
                            <tr id="track{{$t['track_id']}}">
                                <td data-label="Volunteer">
                                    <a href="{{url('/')}}/volunteer/profile/{{$t['volunteer_id']}}" target="_blank">
                                        <div class="avatar"
                                             @if($t['volunteer_logo'] == null)
                                             style="background-image:url('{{asset('img/logo/member-default-logo.png')}}')"
                                             @else
                                             style="background-image:url('{{$t['volunteer_logo']}}')"
                                             @endif
                                             >
                                            <span></span>
                                            </div>
                                        <div class="main-text"><p>{{$t['volunteer_name']}}</p></div>
                                    </a>
                                </td>

                                <td data-label="Opportunity">
                                    <a href="{{url('/')}}/organization/edit_opportunity/{{$t['opportunity_id']}}"
                                       target="_blank">
                                        <div class="avatar"
                                             @if($t['opportunity_status'] != 0)

                                             @if($t['opportunity_logo'] == null)
                                             style="background-image:url('{{asset('front-end/img/org/001.png')}}')"
                                             @else
                                             style="background-image:url('{{$t['opportunity_logo']}}')"
                                             @endif

                                             @else
                                             style="background-image:url('{{asset('front-end/img/org/001.png')}}')"

                                             @endif
                                             >
                                             <span></span>
                                            </div>
                                        <div class="main-text" style="max-width: calc(100% - 70px);"><p>{{$t['opportunity_name']}}</p></div>
                                    </a>
                                </td>

                                <td data-label="Date">
                                    <div class="main-text"><p class="light">{{$t['logged_date']}}</p></div>
                                </td>

                                <td data-label="Mins">
                                    <div class="main-text"><p class="light">{{$t['logged_mins']}}</p></div>
                                </td>

                                <td data-label="Submitted Time">
                                    <div class="main-text"><p class="light">{{\Carbon\Carbon::parse($t['updated_at'])->format('m-d-Y H:m:s')}}</p></div>
                                </td>

                                <td class="text-center" data-label="Approve / Decline">
                                    <input type="hidden" class="track_id" value="{{$t['track_id']}}">
                                    <input type="hidden" class="volunteer_name" value="{{$t['volunteer_name']}}">
                                    <input type="hidden" class="volunteer_logo" value="{{str_replace (' ','%20',$t['volunteer_logo'])}}">
                                    <input type="hidden" class="opportunity_id" value="{{$t['opportunity_id']}}">
                                    <input type="hidden" class="opportunity_name"
                                           value="{{$t['opportunity_name']}}">
                                    <input type="hidden" class="opportunity_logo"
                                           value="{{$t['opportunity_logo']}}">
                                    <input type="hidden" class="worked_date" value="{{$t['logged_date']}}">
                                    <input type="hidden" class="started_time" value="{{$t['started_time']}}">
                                    <input type="hidden" class="ended_time" value="{{$t['ended_time']}}">
                                    <input type="hidden" class="worked_mins" value="{{$t['logged_mins']}}">
                                    <input type="hidden" class="submitted_time" value="{{$t['updated_at']}}">
                                    <input type="hidden" class="volunteer_comment" value="{{$t['comment']}}">
                                    <input type="hidden" class="opportunity_type"
                                           value="{{$t['opportunity_status']}}">
                                    <a href="#" class="action btn_action"><span>Action</span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody> 
                    </table>
                     <div style="padding-top:20px;display:none;" id="pendingTrack_paginate">
                        {{$tracks->render()}}
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>