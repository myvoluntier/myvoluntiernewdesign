<div class="form-group ">

    <input type="hidden" id="delegate_id" value=""/>

    <div class="row">

        <input type="hidden" id="delegatesURL" value="{{url('/organization/profile#orgDelegates') }}" />

        <div class="col-12 col-md-6">
            <label class="label-text">First Name:</label>
            <div class="wrapper_input">
                <input name="del_first_name" id="del_first_name" class="form-control name-panel" type="text" value="" placeholder="">
            </div>
        </div>

        <div class="col-12 col-md-6">
            <label class="label-text org_name_label">Last Name:</label>
            <div class="wrapper_input">
                <input name="del_last_name" id="del_last_name" class="form-control name-panel" type="text" value="" placeholder="">
            </div>
        </div>

        <div class="col-12 col-md-6">
            <label class="label-text">Email:</label>
            <div class="wrapper_input">
                <input name="del_email" id="del_email" class="form-control name-panel"
                       type="text"
                       value=""
                       placeholder="">

                <p class="p_invalid" id="o_invalid_del_email_alert">Email address is invalid.</p>
                <p class="p_invalid" id="o_existing_del_email_alert">Email address is already exist</p>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <label class="label-text">Comments:</label>
            <div class="wrapper_input">
                <textarea class="review-comment" name="del_comments" id="del_comments" placeholder=""></textarea>
            </div>
        </div>

    </div>
</div>
