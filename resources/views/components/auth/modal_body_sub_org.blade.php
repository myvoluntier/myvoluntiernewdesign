<style>
    .hide{display: none;}
</style>

<div class="form-group">
    <div class="row">
        <div class="col-12 col-md-6">
            <label class="label-text">Please Select Organization Type:</label>
            <div class="wrapper_select">
                <select name="o_org_type" id="o_org_type" class="custom-dropdown">
                    @foreach($org_type_names as $org_name)
                    <option value="{{$org_name->id}}" {{(isset($subOrgData[0]['org_type']) && $subOrgData[0]['org_type'] == $org_name->id) ? 'selected' : ''}} >{{$org_name->organization_type}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="text-error">
            This error occurs when the hard drive is (nearly) full. To fix this,
            the user should close some programs (to free swap file usage) and
            delete some files (normally temporary files, or other files after
            they have been backed up), or get a bigger hard drive
        </div>
        <div class="col-12 col-md-6">
            <label class="label-text">&nbsp;</label>
            <div class="wrapper-checkbox "><label>
                    <input type="checkbox" name ="o_auto_accept_friends" {{(isset($subOrgData[0]['auto_accept_friends']) && $subOrgData[0]['auto_accept_friends'] == 1) ? 'checked' : ''}}  id="o_auto_accept_friends">
                    <i class="terms-conditions-org"></i>
                    <span class="label-checkbox-text terms-conditions-org">Auto Accept Friend Requests</span>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="form-group ">
    <div class="row">
        {{--<div class="col-12 col-md-6">--}}
            {{--<label class="label-text">Organization ID:</label>--}}
            {{--<div class="wrapper_input">--}}
                {{--<input name="o_user_name" id="o_user_name" class="form-control name-panel" type="text" value="{{(isset($subOrgData[0]['user_name'])) ? $subOrgData[0]['user_name'] : ''}}" placeholder="">--}}
                {{--<p class="p_invalid" id="o_invalid_username_alert">Organization ID already exist.</p>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="col-12 col-md-6">
            <label class="label-text org_name_label">Organization Name:</label>
            <div class="wrapper_input">
                <input name="sub_org_name" id="sub_org_name" class="form-control name-panel" type="text" value="{{(isset($subOrgData[0]['org_name'])) ? $subOrgData[0]['org_name'] : ''}}" placeholder="">
            </div>
        </div>
    </div>
</div>

<div class="form-group ein_div" style="display: none">
    <label class="label-text">EIN: </label>
    <div class="wrapper_input">
        <input type="text" name="org_ein" id="org_ein" class="form-control name-panel">
    </div>
</div>

<div class="form-group org_type_div" style="display: none">
    <label class="label-text">Organization Type:</label>
    <div class="wrapper_input">
        <input name="non_org_type" id="non_org_type" class="form-control name-panel" type="text" value="other" placeholder="">
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-12 col-md-6">

            <label class="label-text">Year Founded:</label>
            <div class="wrapper_input fa-icons">
                <input name="found_day" id="found_day" class="form-control Founded_year_format" type="text" value="{{(isset($subOrgData[0]['birth_date'])) ? $subOrgData[0]['birth_date'] : ''}}" placeholder="">
                <span class="focus-border input-group-addon"></span>
                <i class="fa fa-calendar" aria-hidden="true"></i>
            </div>

        </div>
        <div class="col-12 col-md-6">

            <div class="margin-top">
                <label class="label-text">Zip Code:</label>
                <div class="wrapper_input">
                    <input name="o_zipcode" id="o_zipcode" class="form-control name-panel" type="text" value="{{(isset($subOrgData[0]['zipcode'])) ? $subOrgData[0]['zipcode'] : ''}}" placeholder="">
                    <p class="p_invalid" id="o_invalid_zipcode_alert">Invalid Zip code. Please enter again</p>
                    <p class="p_invalid" id="o_location_zipcode_alert">We can't get location from this zip code!</p>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-12 col-md-6">
            <label class="label-text">Email:</label>
            <div class="wrapper_input">
                <input name="o_email" id="o_email" class="form-control name-panel" type="text"
                       value="{{(isset($subOrgData[0]['email'])) ? $subOrgData[0]['email'] : ''}}" placeholder="">
                <p class="p_invalid" id="o_invalid_email_alert">Invalid Email Address.</p>
                <p class="p_invalid" id="o_existing_email_alert">Existing Email Address</p>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="margin-top">
                <label class="label-text">Contact Number:</label>
                <div class="wrapper_input">
                    <input name="o_contact_num" id="o_contact_num" class="form-control name-panel phoneUSMask"
                           type="text" placeholder="111-111-1111"
                           value="{{(isset($subOrgData[0]['contact_number'])) ? $subOrgData[0]['contact_number'] : ''}}">
                    <p class="p_invalid" id="o_invalid_contact_number">Invalid Contact Number</p>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="form-group">
    <div class="row">
        <div class="col-12 col-md-6">

            <label class="label-text">Password:</label>
            <div class="wrapper_input">
                <input name="o_password" id="o_password" class="form-control name-panel" type="password" value="" placeholder="">
                <p class="p_invalid" id="o_invalid_password">Enter more than 6 letters and match confirm password</p>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div class="margin-top">
                <label class="label-text">Confirm Password:</label>
                <div class="wrapper_input">
                    <input name="o_confirm" id="o_confirm" class="form-control name-panel" type="password" value="" placeholder="">
                </div>
            </div>
        </div>

        @if(isset($subOrgData))
            <small style="margin-left: 15px;"> Leave blank if you don't want to change password </small>
        @endif
        
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-12 col-md-6">
            <label class="label-text">Website URL:</label>
            <div class="wrapper_input">
                <input name="website" id="o_website" class="form-control name-panel" type="text" value="{{(isset($subOrgData[0]['website']))? $subOrgData[0]['website'] : ''}}" placeholder="">
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="margin-top">
                <label class="label-text">Facebook URL:</label>
                <div class="wrapper_input">
                    <input name="facebook_url" id="o_facebook_url" class="form-control name-panel" type="text" value="{{(isset($subOrgData[0]['facebook_url']))? $subOrgData[0]['facebook_url'] : ''}}" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-12 col-md-6">

            <label class="label-text">Twitter URL:</label>
            <div class="wrapper_input">
                <input name="twitter_url" id="o_twitter_url" class="form-control name-panel" type="text" value="{{(isset($subOrgData[0]['twitter_url']))? $subOrgData[0]['twitter_url'] : ''}}" placeholder="">
            </div>

        </div>
        <div class="col-12 col-md-6">
            <div class="margin-top">
                <label class="label-text">LinkedIn URL:</label>
                <div class="wrapper_input">
                    <input name="linkedin_url" id="o_linkedin_url" class="form-control name-panel" type="text" value="{{(isset($subOrgData[0]['linkedin_url']))? $subOrgData[0]['linkedin_url'] : ''}}" >
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="subOrgId" value="{{(isset($subOrgData[0]['id']))? $subOrgData[0]['id'] : ''}}"/>

<div class="form-group {{(isset($subOrgData))? 'hide' : ''}}">
        <div class="wrapper-checkbox "><label>
                <input id="o_accept_terms" type="checkbox">
                <i class="terms-conditions-org"></i>
                <span class="label-checkbox-text terms-conditions-org">I accept the <a data-type="org" class="termsAndConditions" href="#">Terms and Conditions</a></span>
                <input class="terms-conditions-org-value" type="hidden" value="0">
            </label>
            <p class="p_invalid" id="ov_terms_alert" style="display:none;">You need to accept our terms and conditions to register</p>
        </div>

        <div class="wrapper-checkbox "><label>
            <input id="op_accept_terms" type="checkbox">
            <i class="privacy-policy-org"></i>
            <span class="label-checkbox-text privacy-policy-org">I accept the <a data-type="org" class="privacyPolicy" href="#">Privacy Policy</a></span>
            <input class="privacy-policy-org-value" type="hidden" value="0">
            </label>
            <p class="p_invalid" id="ov_policy_alert" style="display:none;">You need accept our privacy policy to register</p>
        </div>
    </div>


