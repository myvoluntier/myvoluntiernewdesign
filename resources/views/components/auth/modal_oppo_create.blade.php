<div class="modal fade" id="myModalOppoCreate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div>
                <div class="modal-content">
                    <div class="modal-header">
						
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger login_error_div" role="alert" style="display: none">
                            <span id="login_errors_div"></span>
                        </div>
                        <?php echo Auth::user()->getOppoCreatePopupContent('1'); ?>
                    </div>

                    <div class="modal-footer">
                        <div class="wrapper-link two-link">
{{--                        <a id="btn_oppor" href="{{url('/organization/post_opportunity')}}"><span>Create an Opportunity</span></a>--}}
                            <a id="btn_oppor" href="/organization/post_opportunity"><span>Create an Opportunity</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>