<style>
    .hide{display: none;}
</style>
<div class="form-group">
    <div class="row">
        <div class="col-12 col-md-6">

            <label class="label-text">Attribute Name</label>
            <div class="wrapper_input">
                <select name="attributekey" id="attributekey" class="custom-dropdown">
                    <option value="">Select</option>
                    @foreach($atrributelookups as $val)
                    <option value="{{$val->attributekey}}" {{(isset($customAttribute['attributekey']) && $customAttribute['attributekey'] == $val->attributekey) ? 'selected' : ''}} >{{$val->attributename}}</option>
                    @endforeach
                </select>
            </div>


        </div>
        <div class="col-12 col-md-6">
            <div class="margin-top">
                <label class="label-text">Attribute Value</label>
                <div class="wrapper_input">
                    <input name="attributevalue" id="attributevalue" class="form-control name-panel" type="text" value="{{(isset($customAttribute['attributevalue']))? $customAttribute['attributevalue'] : ''}}" >
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="custAttributId" value="{{(isset($customAttribute['id']))? base64_encode($customAttribute['id']) : ''}}"/>

