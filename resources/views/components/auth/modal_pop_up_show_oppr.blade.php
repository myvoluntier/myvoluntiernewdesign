<div class="modal fade" id="showOpprVol" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div class="track-add-hour">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Opportunities and associated hours</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Name</th>
                                        <th>Contact #</th>
                                        <th>Hours</th>
                                    </tr>
                                    </thead>
                                    <tbody id="records_table"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success float-right" data-dismiss="modal">Close</button>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>