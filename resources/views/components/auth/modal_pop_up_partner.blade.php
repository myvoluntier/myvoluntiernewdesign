<div class="modal fade" id="myModalPartner" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div>
                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h3 class="h3">Partner <strong class="green">Organization</strong>!</h3>
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body" id="partnerBody">
                        @include('components.auth.modal_body_partner')
                    </div>
                    <div class="modal-footer">
                        <div class="wrapper_row_link">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="wrapper-link text-left">
                                    </div>
                                </div>

                                <div class="col-12 col-md-8">
                                    <div class="wrapper-link two-link margin-top">
                                        <a class="closeModal red" href="javascript:void(0);"><span>Close</span></a>
                                        <a class="btn_partner" href="javascript:void(0);"><span>Save</span></a>
                                        <a class="btn_regs_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>