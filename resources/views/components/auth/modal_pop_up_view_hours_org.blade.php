<style type="text/css">
    .calendar-btn a {
        border-radius: 50% !important;
        padding: 5px 12px !important;
        font-size: 12px !important;
        height: 100% !important;
    }
    .calendar-btn {
         padding: 0px 10px !important;
    }
</style>


<div id="view_track" class="modal fade  bd-example-modal-lg" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">View Hours</h4>
        <span class="modal-footer calendar-btn">
            <div class="wrapper-link">
                <a  href="#" data-toggle="modal" id="ics_download" data-event_id = ""><i class="fa fa-calendar"></i></a>
            </div>
        </span>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <div class="form-group">
        <div class="row">
            <div class="col-12 col-lg-12 time-selector mb-2">
                <div><b>Opportunity: </b><span class="oppertunity_display"></span></div>
            </div>
        </div>
    
        <div class="row">
            <div class="col-12 col-lg-12 time-selector mb-2">
                <div><b>Date & Day: </b><span id="date_day"></span></div>
            </div>
            <div class="col-12 col-lg-8 time-selector">
                <label class="label-text">Timeblock:</label>
                <select id="time_block_view" multiple='multiple' disabled>
                    <?php
                    $start = "00:00";
                    $end = "24:00";
                    $tStart = strtotime($start);
                    $tEnd = strtotime($end);
                    $tNow = $tStart;

                    while($tNow <= strtotime('-30 minutes',$tEnd)){
                    $time = date("h:i A",$tNow).'-'.date("h:i A",strtotime('+30 minutes',$tNow));
                    $val = date("H:i",$tNow)
                    ?>
                    <option value="{{$val}}">{{$time}}</option>
                    <?php    $tNow = strtotime('+30 minutes',$tNow);
                    }?>
                </select>
            </div>
            <div class="col-12 col-lg-4 align-self-center">

                <div class="margin-top">
                    <div class="form-group main-text">
                    <label class="label-text" style="padding-top: 20px">Logged Mins:</label>
                        <p id="hours_mins_view" class="mt-15">
                            (0min)
                        </p>
                </div>
            </div>
            </div>
        </div>

         <div class="row">
            <div class="col-12 col-lg-12 time-selector mt-2">
                <div><b>Comment: </b><span id="comment_display"></span></div>
                <input type="hidden" id="ics_download_value">
            </div>
        </div>
    </div>
        <div class="modal-footer text-right">
            <div class="wrapper-link two-link">
                <!-- <button type="button" class="btn btn-success" id="" data-toggle="modal" data-target="#add_hours" data-dismiss="modal">Edit</button> -->
                <!-- <a  href="#" data-toggle="modal" id="ics_download" data-event_id = ""><i class="fa fa-calendar"></i> Download ics</a> -->


                <!--<a  href="#" data-toggle="modal" data-target="#add_hours" data-dismiss="modal"><span>Edit</span></a>-->
                <!-- <button type="button" class="btn btn-success" id="btn_remove_hours" >Remove</button> -->
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <a class="white close_button" data-dismiss="modal"><span>Close</span></a>
            </div>
        </div>
      </div>

  </div>
</div>
</div>