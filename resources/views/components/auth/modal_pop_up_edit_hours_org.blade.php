<div class="modal fade" id="edit_hours" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div class="track-add-hour">
                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h2 class="h2">Edit Hours</h2>
                            <h3 class="h3">Track your hours of service to Opportunity!</h3>
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body">

                        <div class="form-group  opp_select_div">
                            <div><b>Opportunity: </b><span class="oppertunity_display"></span></div>
                            
                        </div>

                        <!--div class="row mb-2">
                            <div class="col-sm-12 col-md-5">
                                <div class="form-group">
                                    <div class="wrapper-checkbox">
                                        <label>
                                            <input type="checkbox" name="is_designated" id="is_designated">
                                            <i></i>
                                            <span class="label-checkbox-text">Designate hours for group? </span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-7">
                                <div class="form-group" id="designated_group_id" style="display: none">
                                    <select name="designated_group_id" id="designated_group_id_select">
                                        <option value="">Select any group</option>
                                       
                                    </select>
                                    <p class="p_invalid" id="empty_designated_group_id" style="display: none">Please Select Group!</p>
                                </div>
                            </div>

                        </div-->

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-8 time-selector">
                                    <label class="label-text">Select Time Block:</label>
                                    <select id="time_block" multiple='multiple'>
                                        <?php
                                        $start = "00:00";
                                        $end = "24:00";
                                        $tStart = strtotime($start);
                                        $tEnd = strtotime($end);
                                        $tNow = $tStart;

                                        while($tNow <= strtotime('-30 minutes',$tEnd)){
                                        $time = date("h:i A",$tNow).'-'.date("h:i A",strtotime('+30 minutes',$tNow));
                                        $val = date("H:i",$tNow)
                                        ?>
                                        <option value="{{$val}}">{{$time}}</option>
                                        <?php    $tNow = strtotime('+30 minutes',$tNow);
                                        }?>
                                    </select>
                                    <span id="hourError"></span>
                                </div>

                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label class="label-text">Date:</label>
                                        <div class="wrapper_input fa-icons">
                                            <input onkeydown="return false" id="selected_date_for_unlist" class="form-control" type="text"  data-date-end-date="0d" disabled="disabled">
                                            <span class="focus-border"></span>
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                        <span id="dayError"></span>
                                        <label class="label-text" style="padding-top: 20px">Logged Mins:</label>
                                        <p id="hours_mins" class="mt-15">
                                            (0min)
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label-text">Comments:</label>
                            <div class="wrapper_input">
                                <textarea name="adding_hours_comments" id="adding_hours_comments" placeholder=""></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="wrapper_row_link">
                            <div class="row">
                                <div class="col-12">
                                    <div class="wrapper-link two-link">
<!--                                         <a style="display: none" href="#" id="btn_remove_hourss"><span>Remove</span></a>
 -->                                        <a class="white close_button" data-dismiss="modal"><span>Close</span></a>
                                        <a id="btn_add_hours" href="#"><span>Save</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="opp_id" value="" />
                <input type="hidden" id="is_designated" value="" />
                <input type="hidden" id="designated_group_id_select" value="" />
                <input type="hidden" id="org_emails" value="" />
                <input type="hidden" id="is_edit" value="1" />
                <input type="hidden" id="track_id" value="0">
                <input type="hidden" id="unlist_org_mail_hours">
                <input type="hidden" id="unlist_org_name_hours">
                <input type="hidden" id="volunteer_id">
                <input type="hidden" id="opp_name">
                <!--input type="hidden" id="selected_date">--}}
                <input type="hidden" id="is_edit" value="0">
                <input type="hidden" id="track_id" value="0">
                <input type="hidden" id="is_from_addhour" value="0">
                <input type="hidden" id="is_no_org" value="0">
                <input type="hidden" id="is_link_exist" value="0"-->
            </div>
        </div>
    </div>
</div>

