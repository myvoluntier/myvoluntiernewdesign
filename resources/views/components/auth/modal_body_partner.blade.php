<style>
    .hide{display: none;}
</style>
<div class="form-group">
    <div class="row">
        <div class="col-12 col-md-12">

            <label class="label-text">Organization</label>
            <div class="wrapper_input">
                <select name="partnerkey" id="partnerkey">
                    <option value="">Select</option>
                    @foreach($orgList as $orgVal)
                        <option value="{{$orgVal['id']}}" {{(isset($partnerId['partner_org_id']) && $partnerId['partner_org_id'] == $orgVal['id']) ? 'selected' : ''}} >{{$orgVal['org_name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="selPartnerId" value="{{(isset($partnerId['id']))? base64_encode($partnerId['id']) : ''}}"/>
