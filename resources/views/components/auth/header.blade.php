<div class="row-header">
    <header>
        <div class="container">
            <div class="outer-width-box">
                <div class="inner-width-box">
                    <div class="row flex-nowrap align-items-center">
                        <div class="col col-auto">
                            <div class="header-logo">
                                <a href="{{ route('home') }}">
                                    <img src="{{ asset('front-end/img/logo.jpg') }}" width="226" height="38" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="col">
                            <div class="header-center-menu">
                                <ul class="nav flex-nowrap">
                                    @include('components.auth.nav_bar')
                                </ul>
                            </div>
                        </div>
                        <div class="col col-auto">
                            <div class="header-right-menu">
                                <ul class="nav flex-nowrap">
                                    @if(Auth::user()->user_role === 'organization')
                                        <li class="nav-item {{ active_class(if_route_pattern('organization-search')) }}">
                                    @else
                                        <li class="nav-item {{ active_class(if_route_pattern('volunteer-search')) }}">
                                    @endif
                                        <a class="nav-link"  href="{{ Auth::user()->user_role === 'organization' ? route('organization-search') : route('volunteer-search') }}">
                                            <span class="fa fa-search"></span>
                                            <span class="text">Search User/Org</span>
                                        </a>
                                    </li>

                                    {{--<li class="nav-item dropdown {{ active_class((if_route_pattern('view-organization-profile') and (if_route_param('id', null) or if_route_param('id', Auth::id()))) | (if_route_pattern('view-voluntier-profile') and (if_route_param('id', null) or if_route_param('id', Auth::id()) ))) }}">--}}
                                        {{--<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{ Auth::user()->user_role === 'organization' ? route('view-organization-profile'): route('view-voluntier-profile') }}" role="button" aria-haspopup="true" aria-expanded="false">--}}
                                            {{--<span class="gl-icon-person"></span>--}}
                                            {{--<span class="text">Account Setting</span>--}}
                                        {{--</a>--}}
                                        {{--<div class="dropdown-menu dropdown-menu-right">--}}

                                           {{--@include('components.messagesListOuter')--}}

                                        {{--</div>--}}
                                    {{--</li>--}}

                                    @if(is_null(session('is_delegate')))
                                    <li class="nav-item {{ active_class((if_route_pattern('view-organization-profile') and (if_route_param('id', null) or if_route_param('id', Auth::id()))) | (if_route_pattern('view-voluntier-profile') and (if_route_param('id', null) or if_route_param('id', Auth::id()) ))) }}">
                                        <a class="nav-link" href="{{ Auth::user()->user_role === 'organization' ? route('view-organization-profile'): route('view-voluntier-profile') }}">
                                            <span class="fa fa-user"></span>
                                            <span class="text">Account Setting</span>
                                        </a>
                                    </li>
                                    @endif


                                    {{--@if(Auth::user()->user_role === 'organization')--}}
                                        {{--<li class="nav-item {{active_class(if_route_pattern('organization-opportunity-post'))}}">--}}
                                            {{--<a class="nav-link" href="{{route('organization-opportunity-post')}}">--}}
                                                {{--<span class="gl-icon-clock"></span>--}}
                                                {{--<span class="text">Add Hours</span>--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                    {{--@else--}}
                                        {{--<li class="nav-item {{active_class(if_route_pattern('volunteer-single_track'))}}">--}}
                                            {{--<a class="nav-link" href="{{route('volunteer-single_track')}}">--}}
                                                {{--<span class="gl-icon-clock"></span>--}}
                                                {{--<span class="text">Add Hours</span>--}}
                                            {{--</a>--}}
                                        {{--</li>--}}
                                    {{--@endif--}}

                                    @if(Auth::user()->user_role === 'volunteer')
                                    <li class="nav-item share-profile-modal-send">
                                        <a class="nav-link" href="#">
                                            <span class="fa fa-share-alt"></span>
                                            <span class="text">Share Profile</span>
                                        </a>
                                    </li>
                                    @endif

                                    @if(is_null(session('is_delegate')))
                                    <li class="nav-item dropdown" id="message-box">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                            <span class="fa fa-envelope"></span>
                                            <span class="text">Messages Box</span>
                                            <span class="badge label-warning"></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <div class="wrapper-messages-box">
                                                <div class="messages-list-outer">
                                                    <div class="messages-list-inner">
                                                        <ul id="unread-messages-list"></ul>
                                                    </div>
                                                </div>
                                                <div class="read-all">
                                                    <a href="<?php echo route( Auth::user()->user_role . '-chat', [] )?>"><span>Read All Messages</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endif

                                    <li class="nav-item dropdown {{active_class(if_route_pattern('view-alert')) }}">
                                        <a class="nav-link dropdown-toggle" href="{{route('view-alert')}}" role="button" aria-haspopup="true" aria-expanded="false">
                                            <span class="fa fa-bell"></span>
                                            <span class="text">Alerts</span>
                                            <span class="badge label-alert"></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Separated link</a>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a id="helpDeskPop" class="helpDeskPopLink nav-link dropdown-toggle" href="javascript:void(0);" role="button" aria-haspopup="true"
                                           aria-expanded="false">
                                            <span class="fa fa-question"></span>
                                            <span class="text">Help</span>
                                           
                                        </a>
                                    </li>
                                    @if(is_null(session('is_delegate')))
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"
                                               role="button" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-gear"></span>
                                                <span class="text">Notifications Setting</span>
                                                <span class="badge label-warning"></span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                @include('components.notifications-setting.webs')
                                            </div>
                                        </li>
                                    @endif

                                    @if(session('admin_role') != "admin")
                                        <li class="nav-item signout-user">
                                            <a class="nav-link" href="{{route('signout_user')}}">
                                                <span class="fa fa-sign-out"></span>
                                                <span class="text">Sign out</span>
                                            </a>
                                        </li>
                                    @else
                                    <a href="{{ route('admin-dashboard') }}" style="color: red; font-size: 14px; padding: 7px;">
                                        Admin-{{session('admin_id')}}
                                    </a>

                                    {{--<li class="nav-item">--}}
                                        {{--<a href="{{ route('admin-dashboard') }}">--}}
                                           {{--<span class="fa fa-adn"></span>--}}
                                           {{--<span class="text">Sign out</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>

<div class="row-header-mobile">

    <header>
        <div class="container">
            <div class="outer-width-box">
                <div class="inner-width-box">
                    <div class="row flex-nowrap align-items-center">
                        <div class="col col-auto">
                            <a href="#" class="navtoggler"><span>Menu</span></a>
                        </div>
                        <div class="col">
                            <div class="header-logo">
                                <a href="{{ route('home') }}">
                                    <img src="{{ asset('front-end/img/logo.jpg') }}" width="226" height="38" alt="">
                                </a></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </header>

</div>
@include('components.auth.modal_pop_up_helpdesk')
{{--<script src="https://www.gstatic.com/firebasejs/5.5.2/firebase.js"></script>--}}
<script src="https://www.gstatic.com/firebasejs/4.13.0/firebase.js"></script>
<script>
    // Initialize Firebase
//    var config = {
//        apiKey: "AIzaSyA7prSz7zHUjGpk5cVmebRGMVItpvGadf4",
//        authDomain: "myvoluntier-dev.firebaseapp.com",
//        databaseURL: "https://myvoluntier-dev.firebaseio.com",
//        projectId: "myvoluntier-dev",
//        storageBucket: "myvoluntier-dev.appspot.com",
//        messagingSenderId: "321532507246"
//    };

    var config = {
        apiKey: "AIzaSyB-dJAxQJ1K4XG38bV9q-_H3grri86_mz4",
        authDomain: "myvoluntierproject-cd034.firebaseapp.com",
        databaseURL: "https://myvoluntierproject-cd034.firebaseio.com",
        projectId: "myvoluntierproject-cd034",
        storageBucket: "",
        messagingSenderId: "632239583862"
    };
    firebase.initializeApp(config);
    var user = {};
    var cid;
    var current = null;
    var total_unread = 0;
</script>

<script src="{{asset('js/firebase-chat.js')}}"></script>
