<style>
    .viewTutorial{float: right;
    font-size: 13px;
    line-height: 2.4;}
    #myModalHelpDesk .modal-content .modal-header .main-text{
        width:50%;
    }
</style>
<div class="modal fade" id="myModalHelpDesk" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div>


                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h3 class="h3">Help<strong class="green"> Tickets</strong>!</h3>
                        </div>
                        <span class="viewTutorial"><a href="{{route('articles')}}">View Video Tutorial</a></span>
                    </div>
                    <div class="modal-body">
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/shell.js"></script>
                        <script>
                            hbspt.forms.create({
                                portalId: "5426731",
                                formId: "ab20d926-5071-4fb2-94f3-8a9dceb18978"
                            });
                        </script>
                    </div>
                    <div class="modal-footer">
                        <div class="wrapper_row_link">
                            <div class="row">
                                <div class="col-12 col-md-12">

                                    <div class="wrapper-link two-link margin-top">
                                        <a class="closeModal red" href="javascript:void(0);"><span>Close</span></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<style>
@media (min-width: 992px) {

     #myModalHelpDesk > .modal-lg{
        max-width: 565px !important;
    }

}
@media (max-width: 993px) {

     #myModalHelpDesk > .modal-lg{
        max-width: 490px !important;
    }

}
</style>