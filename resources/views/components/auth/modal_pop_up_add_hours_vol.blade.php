<style type="text/css">
    .calendar-btn a {
        border-radius: 50% !important;
        padding: 5px 12px !important;
        font-size: 12px !important;
        height: 100% !important;
    }
    .calendar-btn {
         padding: 0px 10px !important;
    }
</style>

<div class="modal fade" id="add_hours" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div class="track-add-hour new-track-hou-popup">
                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h2 class="h2">Add Hours</h2>
                            <h3 class="h3">Track your hours of service to Opportunity!</h3>
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body">

                        <div class="form-group opp_select_div">
                            <label class="label-text">Opportunity:</label>
                            <div class="wrapper_select">
                                <select class="custom-dropdown add_select_opportunity" id="opp_id">
                                    <option value="Select an opportunity">Select an opportunity</option>
                                    @foreach($oppr as $op)
                                        <option value="{{$op->id}}">{{$op->title}}</option>
                                    @endforeach
                                </select>
                                <p class="p_invalid" id="empty_opportunity_alert" style="display: none">Please Select
                                    Opportunity!</p>
                            </div>
                        </div>

                        <div id="opportunity_track_info" class="mb-2">
                            {{--Date:   Time:   - Days of Week:  via AJAX on change of OPP--}}
                        </div>

                        <div class="row mb-3">
                            <div class="col-sm-12 col-md-5">
                                <div class="form-group">
                                    <div class="wrapper-checkbox">
                                        <label>
                                            <input type="checkbox" name="is_designated" id="is_designated">
                                            <i></i>
                                            <span class="label-checkbox-text">Designate hours for group? </span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-sm-12 col-md-12 col-12">
                                <div class="form-group" id="designated_group_id" style="display: none">
                                    <select name="designated_group_id" id="designated_group_id_select">
                                        <option value="">Select any group</option>
                                        @foreach($all_groups_list as $group)
                                            <option value="{{$group->id}}">{{$group->name}}</option>
                                        @endforeach
                                    </select>
                                    <p class="p_invalid" id="empty_designated_group_id" style="display: none">Please
                                        Select Group!</p>
                                </div>
                            </div>

                        </div>

                        <div style="display: none" class="form-group private_opp_div_add_hours">
                            <label class="label-text">Unlisted Opportunity:</label>
                            <div class="wrapper_input">
                                <input readonly id="private_opp_name_hours" value="" placeholder="">
                                <input type="hidden" id="unlist_org_mail_hours">
                                <input type="hidden" id="unlist_org_name_hours">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 col-12 time-selector">
                                    <label class="label-text">Select Time Block:</label>
                                    <select id="time_block" multiple='multiple'>
                                        <?php
                                        $start = "00:00";
                                        $end = "24:00";
                                        $tStart = strtotime($start);
                                        $tEnd = strtotime($end);
                                        $tNow = $tStart;

                                        while($tNow <= strtotime('-30 minutes', $tEnd)){
                                        $time = date("h:i A", $tNow) . '-' . date("h:i A", strtotime('+30 minutes', $tNow));
                                        $val = date("H:i", $tNow)
                                        ?>
                                        <option value="{{$val}}">{{$time}}</option>
                                        <?php    $tNow = strtotime('+30 minutes', $tNow);
                                        }?>
                                    </select>
                                    <span id="hourError"></span>
                                </div>

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 col-12 date-block">
                                    <div class="form-group">
                                        <label class="label-text">Date:</label>
                                        <div class="wrapper_input fa-icons">
                                            <input onkeydown="return false" id="selected_date_for_unlist"
                                                   class="form-control" type="text" data-date-end-date="0d">
                                            <span class="focus-border"></span>
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                        <span id="dayError"></span>
                                        <label class="label-text" style="padding-top: 20px">Logged Mins:</label>
                                        <p id="hours_mins" class="mt-15">
                                            (0min)
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group comments">
                            <label class="label-text">Comments:</label>
                            <div class="wrapper_input">
                                <textarea name="adding_hours_comments" id="adding_hours_comments"
                                          placeholder=""></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="wrapper_row_link">
                            <div class="row">
                                <div class="col-12">
                                    <div class="wrapper-link two-link">
                                        <a class="white close_button"><span>Close</span></a>
                                        <a id="btn_add_hours" href="#"><span>Save</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--input type="hidden" id="selected_date">--}}
                <input type="hidden" id="is_edit" value="0">
                <input type="hidden" id="track_id" value="0">
                <input type="hidden" id="is_from_addhour" value="0">
                <input type="hidden" id="is_no_org" value="0">
                <input type="hidden" id="is_link_exist" value="0"-->
            </div>
        </div>
    </div>
</div>

<div id="view_track" class="modal fade  bd-example-modal-lg" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">View Hours</h4>
        <span class="modal-footer calendar-btn">
            <div class="wrapper-link">
                <a  href="#" data-toggle="modal" id="ics_download" data-event_id = ""><i class="fa fa-calendar"></i></a>
            </div>
        </span>
          <div id="trackStatus"></div>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <div class="form-group">
        <div class="row">
            <div class="col-12 col-lg-12 time-selector mb-2">
                <div><b>Opportunity: </b><span id="oppertunity_display"></span></div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-12 time-selector mb-2">
                <div><b>Date & Day: </b><span id="date_day"></span></div>
            </div>
            <div class="col-12 col-lg-8 time-selector">
                <label class="label-text">Timeblock:</label>
                <select id="time_block_view" multiple='multiple' disabled>
                    <?php
                    $start = "00:00";
                    $end = "24:00";
                    $tStart = strtotime($start);
                    $tEnd = strtotime($end);
                    $tNow = $tStart;

                    while($tNow <= strtotime('-30 minutes',$tEnd)){
                    $time = date("h:i A",$tNow).'-'.date("h:i A",strtotime('+30 minutes',$tNow));
                    $val = date("H:i",$tNow)
                    ?>
                    <option value="{{$val}}">{{$time}}</option>
                    <?php    $tNow = strtotime('+30 minutes',$tNow);
                    }?>
                </select>
            </div>
            <div class="col-12 col-lg-4 align-self-center">

                <div class="margin-top">
                    <div class="form-group main-text">
                    <label class="label-text" style="padding-top: 20px">Logged Mins:</label>
                        <p id="hours_mins_view" class="mt-15">
                            (0min)
                        </p>
                </div>
            </div>
            </div>
        </div>

         <div class="row">
            <div class="col-12 col-lg-12 time-selector mt-2">
                <div><b>Comment: </b><span id="comment_display"></span></div>
                <input type="hidden" id="ics_download_value">
            </div>
        </div>
    </div>
        <div class="modal-footer text-right">

            <div class="wrapper-link two-link">

                <a  href="#" id="showQrCodeId" data-toggle="modal" data-target="#qrCodeModal"><span>Show QR CODE</span></a>

                <a id="eitBtnHrs" href="#" data-toggle="modal" data-target="#add_hours" data-dismiss="modal"><span>Edit</span></a>
                <!-- <button type="button" class="btn btn-success" id="btn_remove_hours" >Remove</button> -->
                <a id="btn_remove_hours" href="#"><span>Remove</span></a>
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <a class="white close_button" data-dismiss="modal"><span>Close</span></a>
            </div>
        </div>
      </div>

  </div>
</div>
</div>

<div class="modal fade" id="qrCodeModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLongTitle">QR CODE</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p>This code can be scanned by the Organization Administrator to confirm your volunteer hours associated with this opportunity.</p>

                <div class="row row-centered">
                    <input type="hidden" id="track_id">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <img id="qr_image" src="https://via.placeholder.com/100" alt="Image Logo" class="img-thumbnail">
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 my-auto">
                        <h6 id="qr_name">John Doe</h6>
                        <span class="text-success" id="qr_hours">3 Hours</span>
                    </div>

                    <div class="col-lg-5 col-md-5 col-sm-6"
                         style="font-size: 12px;background: #a9a9a973; border-radius: 6px">
                        <br/>
                        <b>Organization:</b> <span id="qr_org_name"></span> <br>
                        <b>Opportunity:</b> <span id="qr_opp_name"></span> <br>
                        <b>Data:</b> <span id="qr_date"></span><br>
                        <b>Time Tracked:</b> <span id="qr_time_range"></span> <br>
                        <br/>
                    </div>
                </div>

                <br>
                <div class="row row-centered">
                    <div style="margin: auto; width: 50%; padding: 5px;">
                        <div id="qrCodeVol"></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary float-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
