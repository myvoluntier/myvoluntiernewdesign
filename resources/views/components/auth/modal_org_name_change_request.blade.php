
<div class="modal fade" id="orgNameChangeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Request Name Change</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <form method="post" action="{{url('/store')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                
                            <div class="col-12 col-md-12">
                                <label class="label-text">Organization Name:</label>
                                <div class="wrapper_input">
                                    <input  name="temp_org_name" id="temp_org_name" type="text" value="{{Auth::user()->org_name}}" required>
                                </div>
                            </div>
                
                            
                        </div>
                        <label for="proof">Proof of Identity:</label>
                
                        <div class="wrapper_input">
                            <input type="file" name="poi_org" id="poi_org" multiple accept='image/png,image/jpg,image/jpeg' required>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <small>Please upload a copy of your valid ID in PNG or JPEG</small>
                        </div>
                    </div>
                
                    <div class="modal-footer">
                        <input type="submit" name="submit" value="Submit" class="btn btn-success" style="float:right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="float:right;">Close</button><br>
                    </div>
                </form>

            </div>
        </div>
    </div>