<style>
    .hide{display:none !important;}
    .show{display:block !important;}
    .massDomainMail {margin-left:5%; color:#42bd41;}
</style>
@if($profile_info['is_volunteer'] === 0)
@include('components.auth.modal_org_name_change_request')
    <form id="update_account_oug">
        {{csrf_field()}}
        @include('components.organizationEmailBlasts')
        <div class="main-text border_blk">
        <h5 class="title_org">Account Info <a data-opportuniti-id-mail="" id="" href="#" class="edit massDomainMail"><span><i class="fa fa-envelope"></i></span></a></h5>    
        
        <div class="row">
            <div class="col-12 col-md-4">
                <label class="label_text_blk">Organization Name:</label>
                <div class="wrapper_input">
                    <input class="input_blk gray_bg_input" disabled name="org_name" id="org_name" type="text" value="{{Auth::user()->org_name}}" placeholder="">
                </div>
            </div>       
        @if(Auth::user()->approval_org != 'PENDING')        
            <div class="col-12 col-md-4"> 
                 <label class="label_text_blk free_field"><br></label>              
                <div  class="alpha">                   
                    <button type="button" class="btn btn-success alpha" data-toggle="modal" data-target="#orgNameChangeModal">
                        <span><b>Request Name Change</b></span>
                    </button>
                </div>
            </div>
         </div>
        @endif
        <div class="row">
            <div class="col-12 col-md-4">
                <label class="label_text_blk">Organization ID:</label>
                <div class="wrapper_input">
                    <input class="input_blk gray_bg_input" type="text" readonly value="{{Auth::user()->user_name}}" placeholder="">
                </div>
            </div>

            <div class="col-12 col-md-4">
                <label class="label_text_blk">Organization Type:</label>
                <div class="wrapper_input mb_m_10">
                    <select name="org_type" class="form-control input_blk gray_bg_input" id="org_type">
                        @foreach($org_type_names as $org_name)
                            <option {{$org_name->id == Auth::user()->org_type ? 'selected' : ''}} value="{{$org_name->id}}">{{$org_name->organization_type}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-12 col-md-4">
                <label class="label_text_blk free_field"> <br/></label>
                <div class="wrapper-checkbox mt-2">
                    <label>
                        <input type="checkbox" value="1" name ="auto_accept_friends" {{Auth::user()->auto_accept_friends ? 'checked' : ''}} id="auto_accept_friends">
                        <i></i>
                        <span class="label-checkbox-text">Auto Accept Friend Requests</span>
                    </label>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-12 col-md-4">
                <label class="label_text_blk">Email:</label>
                <div class="wrapper_input input_blk gray_bg_input">
                    {{(Auth::user()->email)}}
                    &nbsp;&nbsp;
                    <?php if(Auth::user()->confirm_code == '1'){?>
                        <span style="color:green;">confirmed</span>
                    <?php }else{?>
                        <span style="color:red;">pending</span>
                    <?php }?>
                </div>

            </div>
            <div class="col-12 col-md-8">
                <label class="label_text_blk">Website URL:</label>
                <div class="wrapper_input">
                    <input class="input_blk" name="website" id="website" type="text" value="{{Auth::user()->website}}" placeholder="">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4">
                <label class="label_text_blk">Associated Email Domain 1:</label>
                <div class="wrapper_input input_blk gray_bg_input">
                    <?php if((Auth::user()->domain_1)) {
                            $domain1Dis = 'hide'; ?>
                            <div id="disdomain_1">
                                {{(Auth::user()->domain_1)}}
                                &nbsp;&nbsp;
                                <?php if(Auth::user()->domain_1_confirm_code == '1'){?>
                                    <span style="color:green;">confirmed</span>
                                <?php }else{?>
                                    <span style="color:red;">pending</span>
                                <?php }?>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0);" 
                                   onclick="confirm_modal_info('domain_1');" 
                                   class="actnbtn">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        <?php }else{ $domain1Dis = 'show';  } ?>
                        <input class="{{$domain1Dis}}" name="domain_1" id="domain_1" type="text" value="{{Auth::user()->domain_1}}" placeholder="">
                </div>

            </div>
            <div class="col-12 col-md-4">
                <label class="label_text_blk">Associated Email Domain 2:</label>
                <div class="wrapper_input input_blk gray_bg_input">
                     <?php if((Auth::user()->domain_2)) {
                            $domain2Dis = 'hide'; ?>
                            <div id="disdomain_2">
                                {{(Auth::user()->domain_2)}}
                                &nbsp;&nbsp;
                                <?php if(Auth::user()->domain_2_confirm_code == '1'){?>
                                    <span style="color:green;">confirmed</span>
                                <?php }else{?>
                                    <span style="color:red;">pending</span>
                                <?php }?>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0);" 
                                   onclick="confirm_modal_info('domain_2');" 
                                   class="actnbtn">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </div>
                        <?php }else{ $domain2Dis = 'show';  } ?>
                        <input class="{{$domain2Dis}}" name="domain_2" id="domain_2" type="text" value="{{Auth::user()->domain_2}}" placeholder="">
                </div>

            </div>

            <div class="col-12 col-md-2">
                <label class="label_text_blk">Year Founded:</label>
                <div class="wrapper_input fa-icons date_blk">
                    <?php $user_bod = Auth::user()->birth_date; 
                        $newDate = date("Y", strtotime($user_bod)); 
                    ?>
                    <input name="birth_day" type="text" class="Founded_year_format gray_bg_input" value="{{$user_bod}}"
                           placeholder="">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </div>
            </div>


        </div>

        <div class="row">            
            <div class="col-12 col-md-4">
                <label class="label_text_blk">Address:</label>
                <div class="wrapper_input">
                    <input name="address" id="address" class="input_blk" type="text" value="{{Auth::user()->location}}" placeholder="">
                </div>

            </div>
            <div class="col-12 col-md-2">
                <label class="label_text_blk">Zip Code:</label>
                <div class="wrapper_input">
                    <input name="zipcode" id="zipcode" class="input_blk gray_bg_input" type="text" value="{{Auth::user()->zipcode}}" placeholder="">
                </div>
            </div>
            <div class="col-12 col-md-2">
                <label class="label_text_blk">Contact Number:</label>
                  {{-- <div class="form-group"> --}}
                <div class="input-group">
                    <input type="text" name="contact_num" class="input_blk gray_bg_input" id="contact_num" placeholder="111-111-1111" class="form-control phoneUSMask" value="{{Auth::user()->contact_number}}">
                </div>
              {{-- </div> --}}
            </div>
        </div>

        <div class="row">
           <div class="col-12 col-md-4"> 
                <label class="label_text_blk">Facebook URL:</label>
                <div class="wrapper_input">
                    <input name="facebook_url" class="input_blk" id="facebook_url" type="text" value="{{Auth::user()->facebook_url}}"
                        placeholder="">
                </div>
           </div>
           <div class="col-12 col-md-4">
                <label class="label_text_blk">Twitter URL:</label>
                <div class="wrapper_input">
                    <input name="twitter_url" class="input_blk" id="twitter_url" type="text" value="{{Auth::user()->twitter_url}}" placeholder="">
                </div>
           </div>
           <div class="col-12 col-md-4">
                <label class="label_text_blk">LinkedIn URL:</label>
                <div class="wrapper_input">
                    <input name="linkedin_url" class="input_blk" id="linkedin_url" type="text" value="{{Auth::user()->linkedin_url}}"
                        placeholder="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-4">
                <label class="label_text_blk">New Password:</label>
                <div class="wrapper_input">
                    <input name="new_password" class="input_blk" id="new_password" type="password" value="" placeholder="">
                </div>
            </div>
            <div class="col-12 col-md-4">
                <label class="label_text_blk">Confirm Password:</label>
                <div class="wrapper_input">
                    <input name="confirm_password" class="input_blk" id="confirm_password" type="password" value="" placeholder="">
                </div>

            </div>
        </div>

        <label class="label_text_blk">Summary:</label>
        <div id="editor">
               {!! Auth::user()->brif !!}
        </div>

        <input id="orgSum" name="org_summary" value='' type="hidden">
        <div class="wrapper-link">
            <a id="submit-update-account" href="javascript:void(0);"><span>Save Change</span></a>
            <a class="btn_update_account" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
        </div>

    </div>
    </form>

@elseif($profile_info['is_volunteer'] === 1)
    <form id="update_account" method="post" action="">
    <div class="row">
        <div class="col-12 order-0 col-md-8 order-md-0">
            <div class="main-text">
                <p class="title">My Account Information</p>
                <div class="row">
                    <div class="col-12 col-md-6 ">
                        <label class="label-text">First Name:</label>
                        <div class="wrapper_input">
                            <input disabled name="user_id" id="first_name" type="text" value="{{Auth::user()->first_name}}" placeholder="">
                        </div>

                    </div>
                    <div class="col-12 col-md-6 ">
                        <label class="label-text">Last Name:</label>
                        <div class="wrapper_input">
                            <input disabled name="user_id" id="last_name" type="text" value="{{Auth::user()->last_name}}" placeholder="">
                        </div>

                
                        @if(Auth::user()->approval != 'PENDING')
                        
                        <div  class="wrapper-link alpha">
                            <button type="button" class="btn btn-success alpha" data-toggle="modal" data-target="#aModal">
                                <span><b>Request Name Change</b></span>
                            </button>
                        </div>
                        @endif

                    </div>
                </div>


                <div class="row">
                    <div class="col-12 col-md-6">

                        <label class="label-text">User Name:</label>
                        <div class="wrapper_input">
                            <input readonly type="text" value="{{Auth::user()->user_name}}" >
                        </div>

                    </div>
                    <div class="col-12 col-md-6">

                        <label class="label-text">Email:</label>
                        <div class="wrapper_input">
                            {{(Auth::user()->email)}}
                            &nbsp;&nbsp;
                            <?php if(Auth::user()->confirm_code == '1'){?>
                                <span style="color:green;">confirmed</span>
                            <?php }else{?>
                                <span style="color:red;">pending</span>
                            <?php }?>
                            <!--input name="email" id="email" type="text" value="{{Auth::user()->email}}" placeholder=""-->
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">

                        <label class="label-text">Alternate Email 1:</label>
                        <div class="wrapper_input">
                            
                            <?php if((Auth::user()->email2)) {
                                    $email2Dis = 'hide'; ?>
                                    <div id="disemail2">
                                        {{(Auth::user()->email2)}}
                                        &nbsp;&nbsp;
                                        <?php if(Auth::user()->confirm_code2 == '1'){?>
                                            <span style="color:green;">confirmed</span>
                                        <?php }else{?>
                                            <span style="color:red;">pending</span>
                                        <?php }?>
                                        &nbsp;&nbsp;
                                        <a href="javascript:void(0);" 
                                           onclick="confirm_modal_info('email2');" 
                                           data-id="{{Auth::user()->id}}" class="actnbtn">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </div>
                            <?php }else{ $email2Dis = 'show';  } ?>
                            <input class="{{$email2Dis}}" name="email2" id="email2" type="email" value="{{Auth::user()->email2}}" placeholder=""> 

                        </div>

                    </div>
                    <div class="col-12 col-md-6">

                        <label class="label-text">Alternate Email 2:</label>
                        <div class="wrapper_input">
                            
                            <?php if((Auth::user()->email3)) { $email3Dis = 'hide'; ?>
                                    <div id="disemail3">
                                            {{(Auth::user()->email3)}}
                                            &nbsp;&nbsp;&nbsp;
                                            <?php if(Auth::user()->confirm_code3 == '1'){?>
                                                <span style="color:green;">confirmed</span>
                                            <?php }else{?>
                                                <span style="color:red;">pending</span>
                                            <?php }?>
                                            &nbsp;&nbsp;
                                            <a href="javascript:void(0);" 
                                               onclick="confirm_modal_info('email3');" 
                                               data-id="{{Auth::user()->id}}" class="actnbtn">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                    </div>
                            <?php }else{ $email3Dis = 'show'; } ?>
                                    <input class="{{$email3Dis}}" name="email3" id="email3" type="email" value="{{Auth::user()->email3}}" placeholder=""> 
                        </div>


                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">

                        <label class="label-text">Middle Initial:</label>
                        <div class="wrapper_input">
                            <input name="middle_initial" id="middle_initial" type="text" value="{{Auth::user()->middle_initial}}"
                                   placeholder="">
                        </div>

                    </div>
                    <div class="col-12 col-md-6">

                        <label class="label-text">Gender:</label>
                        <div class="row">
                            <?php if(Auth::user()->gender == 'male') {?>

                            <div class="col-12 col-md-6">

                                <div class="wrapper-checkbox"><label>
                                        <input value="male" type="radio" name="gender" checked>
                                        <i></i>
                                        <span class="label-checkbox-text">Male</span>
                                    </label></div>

                            </div>
                            <div class="col-12 col-md-6">

                                <div class="wrapper-checkbox"><label>
                                        <input value="female" type="radio" name="gender">
                                        <i></i>
                                        <span class="label-checkbox-text">Female</span>
                                    </label></div>

                            </div>

                            <?php }else{?>

                            <div class="col-12 col-md-6">

                                <div class="wrapper-checkbox"><label>
                                        <input value="male" type="radio" name="gender">
                                        <i></i>
                                        <span class="label-checkbox-text">Male</span>
                                    </label></div>

                            </div>
                            <div class="col-12 col-md-6">

                                <div class="wrapper-checkbox"><label>
                                        <input value="female" type="radio" name="gender" checked>
                                        <i></i>
                                        <span class="label-checkbox-text">Female</span>
                                    </label></div>

                            </div>

                            <?php }?>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6">

                        <label class="label-text">Birthdate:</label>
                        <div class="wrapper_input fa-icons">
                            <input name="birth_day" id="birth_day" type="text" value="{{dateMdyNoSlashFormat(Auth::user()->birth_date)}}"
                                   placeholder="">
                            <span class="focus-border"></span>
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                        </div>

                    </div>
                    <div class="col-12 col-md-6">

                        <label class="label-text">Show Birthdate:</label>
                        <div class="row">
                            <div class="col-12 col-md-6">

                                <div class="wrapper-checkbox"><label>
                                        <input type="radio"
                                               <?php echo (Auth::user()->show_age == 'Y') ? 'checked' : '';?> value="Y"
                                               id="optionsRadios3" name="show_age">
                                        <i></i>
                                        <span class="label-checkbox-text">Show Age</span>
                                    </label></div>

                            </div>
                            <div class="col-12 col-md-6">

                                <div class="wrapper-checkbox"><label>
                                        <input type="radio"
                                               <?php echo (Auth::user()->show_age == 'N') ? 'checked' : '';?> value="N"
                                               id="optionsRadios4" name="show_age">
                                        <i></i>
                                        <span class="label-checkbox-text">Show Birthdate</span>
                                    </label></div>

                            </div>
                        </div>

                    </div>
                </div>


                <div class="row">
                    <div class="col-12 col-md-6">

                        <label class="label-text">Show Address:</label>
                        <div class="row">
                            <div class="col-12 col-md-6">

                                <div class="wrapper-checkbox"><label>
                                        <input type="radio"
                                               <?php echo (Auth::user()->show_address == 'Y') ? 'checked' : '';?> value="Y"
                                               id="optionsRadios1" name="show_address">
                                        <i></i>
                                        <span class="label-checkbox-text">Yes</span>
                                    </label></div>

                            </div>
                            <div class="col-12 col-md-6">

                                <div class="wrapper-checkbox"><label>
                                        <input type="radio"
                                               <?php echo (Auth::user()->show_address == 'N') ? 'checked' : '';?> value="N"
                                               id="optionsRadios2" name="show_address">
                                        <i></i>
                                        <span class="label-checkbox-text">No</span>
                                    </label></div>

                            </div>
                        </div>

                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <label class="label-text">Preferred Opportunity Type(s):</label>
                            <div class="wrapper_select">
                                <select name="opportunity_type[]" id="opportunity_type" class="form-control custom-dropdown select2" multiple>
                                    @foreach($opportunityCategory as $oc)
                                    <option <?php if (!empty($selectedCat) && in_array($oc->id, $selectedCat)) { ?> selected <?php } ?> value="{{$oc->id}}">{{$oc->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12 col-md-6">

                        <label class="label-text">Zip Code:</label>
                        <div class="wrapper_input">
                            <input name="zipcode" id="zipcode" type="text" value="{{Auth::user()->zipcode}}"
                                   placeholder="">
                        </div>

                    </div>
                    <div class="col-12 col-md-6">

                        <label class="label-text">Contact Number:</label>    

                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="contact_num" id="contact_num" placeholder="111-111-1111" class="form-control phoneUSMask" value="{{Auth::user()->contact_number}}">
                            </div>
                        </div>

                    </div>
                </div>


                <div class="row">
                    <div class="col-12 col-md-6">
                        <label class="label-text">New Password:</label>
                        <div class="wrapper_input">
                            <input name="new_password" id="new_password" type="password" value="" placeholder="">
                        </div>

                    </div>
                    <div class="col-12 col-md-6">
                        <label class="label-text">Confirm Password:</label>
                        <div class="wrapper_input">
                            <input type="password" name="confirm_password" id="confirm_password" value="" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input name="my_summary" class="my_summary_acount" type="hidden">
        <div class="col-12 order-1 col-md-12 order-md-2">

            <div class="main-text">

                <label class="label-text">Summary:</label>

                <textarea id="editor">
                    {{Auth::user()->brif}}
                </textarea>
                <span class="alert alert-success in alert-dismissable resultupdatemsg" style="display: none;">Your account is successfully updated!</span>
                <div  class="wrapper-link">
                    <a id="btn_save" href="#"><span>Save Change</span></a>
                    <a class="btn_update_account" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                </div>

            </div>

        </div>
    </div>
    </form>
    <div class="modal fade" id="aModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Request Name Change</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <form method="post" action="{{url('/store')}}" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="row">
                
                            <div class="col-12 col-md-6">
                                <label class="label-text">First Name:</label>
                                <div class="wrapper_input">
                                    <input  name="first_name1" id="first_name1" type="text" value="{{Auth::user()->first_name}}" required>
                                </div>
                            </div>
                
                            <div class="col-12 col-md-6">
                                <label class="label-text">Last Name:</label>
                                <div class="wrapper_input">
                                    <input name="last_name1" id="last_name1" type="text" value="{{Auth::user()->last_name}}" required>
                                </div>
                            </div>
                        </div>
                        <label for="proof">Proof of Identity:</label>
                
                        <div class="wrapper_input">
                            <input type="file" name="image" id="image" multiple accept='image/*' required>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <small>Please upload a copy of your valid ID or driver license in PNG or JPEG</small>
                        </div>
                    </div>
                
                    <div class="modal-footer">
                        <input type="submit" name="submit" value="Submit" class="btn btn-success" style="float:right;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="float:right;">Close</button><br>
                    </div>
                </form>

            </div>
        </div>
    </div>
    
@endif
<div class="modal fade" id="delete_modal_vol">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">
                <div class="modal-header">
                    <h4 class="modal-name" style="text-align:center;">
                        Are You Sure You Want To Delete ?
                    </h4>
                </div>
                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="javascript:void(0);" 
                        onclick="delete_alteremail();" 
                        class="btn btn-danger" id="delete_link_account" data-column="nt">Delete</a>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

<script>
    function confirm_modal_info(columnName){
        
        jQuery('#delete_modal_vol').modal('show', {backdrop: 'static'});
        jQuery('#delete_link_account').attr('data-column', columnName);
    }
    function delete_alteremail(){
        var column = jQuery('#delete_link_account').attr('data-column');
        var formData = {
            columnName: column
        }
        var url = API_URL + 'volunteer/delete_alteremail';
        if(column == 'domain_1' || column == 'domain_2'){
            url = API_URL + 'organization/delete_domain';
        }
        
        jQuery.ajax({
            type: 'POST',
            url: url,
            data: formData,
            success: function (data) {
                jQuery('#dis'+column).hide();
                jQuery('#'+column).val('');
                jQuery('#'+column).removeClass('hide');
                jQuery('#delete_modal_vol').modal('hide');
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        
    }
    
</script>
