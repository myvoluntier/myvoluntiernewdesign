 <div class="container">

                <ul class="profile_tab nav nav-tabs" role="tablist">

                    @if($profile_info['is_my_profile'] == 1 || $profile_info['is_friend'] > 1 )

                        @if($profile_info['is_volunteer']==1)
                            <li class="nav-item">
                                <a class="nav-link active show" href="#activity" role="tab" data-toggle="tab"><span>Activity</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#transcript" role="tab" data-toggle="tab"><span>Transcript</span></a>
                            </li>
                        @elseif($profile_info['is_volunteer']==0)
                            <li class="nav-item">
                                <a class="nav-link active show" href="#activity" role="tab" data-toggle="tab"><span>Opportunity</span></a>
                            </li>
                        @endif
                            <li class="nav-item">
                                <a class="nav-link" href="#details" role="tab"
                                   data-toggle="tab"><span>{{$user->user_role === 'organization' ? 'Details' : 'Info' }}</span></a>
                            </li>
                    @else
                        @if($profile_info['is_volunteer']==0)
                            <li class="nav-item">
                                <a class="nav-link active show" href="#activity" role="tab" data-toggle="tab"><span>Opportunity</span></a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link {{$profile_info['is_volunteer']==1 ? 'active show' : ''}} " href="#details" role="tab"
                               data-toggle="tab"><span>{{$user->user_role === 'organization' ? 'Details' : 'Info' }}</span></a>
                        </li>

                    @endif

                    <li class="nav-item">
                        <a class="nav-link" href="#groups" role="tab"
                           data-toggle="tab"><span>Groups</span></a>
                    </li>
                    @if($user->user_role === 'organization')
                        <li class="nav-item">
                            <a class="nav-link" href="#affiliatedGroups" role="tab"
                               data-toggle="tab"><span>Affiliated Groups</span></a>
                        </li>
                        @if(!isset($user->parent_id))
                            <li class="nav-item">
                                <a class="nav-link" id="subOrganizationTab" href="#subOrganization" role="tab"
                                   data-toggle="tab"><span>Sub Organization</span></a>
                            </li>
                        @endif
                        <li class="nav-item">
                                <a class="nav-link" id="orgPartnerTab" href="#orgPartner" role="tab"
                                   data-toggle="tab"><span>Partners</span></a>
                            </li>
                    @endif
                    @if($profile_info['is_volunteer']==1)
                        <li class="nav-item" >
                            <a class="nav-link" id="serviceProjectTab" href="#serviceProject" role="tab" data-toggle="tab"><span>Service Projects</span></a>
                        </li>
                        <li class="nav-item" >
                            <a class="nav-link" id="customAttributesTab" href="#customAttributes" role="tab" data-toggle="tab"><span>Custom Attributes</span></a>
                        </li>
                    @endif

                </ul>

                <div class="tab-content">

                    @if($profile_info['is_my_profile'] == 1 || $profile_info['is_friend'] > 1 )

                        @if($profile_info['is_volunteer']==1)
                            <div role="tabpanel" class="tab-pane fade in active show" id="activity">
                                @include('components.profile.activity-tab')
                            </div>

                            <div role="tabpanel" class="tab-pane fade" id="transcript">
                                @include('components.profile.transcript-tab')
                            </div>
                        @elseif($profile_info['is_volunteer']==0)
                            <div role="tabpanel" class="tab-pane fade in active show" id="activity">
                                @include('components.profile.activity-tab')
                            </div>
                        @endif
                            <div role="tabpanel" class="tab-pane fade" id="details">

                                @include('components.profile.detail-tab')

                                @if($profile_info['is_my_profile'] == 1)
                                    <hr>
                                    @include('components.profile.account-tab')
                                @endif
                            </div>

                    @else

                        @if($profile_info['is_volunteer']==0)
                            <div role="tabpanel" class="tab-pane fade in active show" id="activity">
                                @include('components.profile.activity-tab')
                            </div>
                        @endif

                        <div role="tabpanel" class="tab-pane fade {{$profile_info['is_volunteer']==1 ? 'in active show' : ''}}" id="details">
                            @include('components.profile.detail-tab')
                            @if($profile_info['is_my_profile'] == 1)
                                <hr>
                                @include('components.profile.account-tab')
                            @endif
                        </div>
                    @endif


                   <div role="tabpanel" class="tab-pane fade " id="groups">
                       <div class="row">

                           @if($groupCount > 0)
                                @if($profile_info['is_volunteer']==0 && $authUser)
                                    <div class="col-sm-12 mb-12 wrapper-link" style="margin:0px !important;">
                                        <div style="margin:10px !important;">
                                            <a id="export_member" href="{{url('/organization/group-members') }}"><span>Export Members</span></a>
                                        </div>
                                    </div>
                                @endif
                               @foreach($group as $groupSingle)
                                   <div class="col-sm-4 mb-2">
                                       <a href="{{$groupSingle->is_share_able || $groupSingle->is_public ==1 ? route('share.group' ,base64_encode($groupSingle->id)) : '#'}}">
                                           <div class="card" style="background-color: #edf2f3">
                                               <div class="card-body">
                                                   <div class="main-text">
                                                       <p class="name">
                                                           {{str_limit($groupSingle->name ,30)}}
                                                           @if($groupSingle->is_public ==1)
                                                               <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                           @else
                                                               <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                           @endif
                                                       </p>
                                                       @if(isset($groupSingle->org_name))
                                                        <p><span style="font-size:13px">{{$groupSingle->org_name}}</span></p>
                                                       @endif
                                                       @if(isset($groupSingle->categoryName))
                                                            <p>{{"Category : ".$groupSingle->categoryName}}</p>
                                                       @endif
                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                   </div>
                               @endforeach
                           @else
                               You do not have active groups yet
                           @endif

                       </div>
                   </div>
                    @if($user->user_role === 'organization')
                    <div role="tabpanel" class="tab-pane fade " id="affiliatedGroups">
                       <div class="row">

                           @if(count($affiliatedGroups))
                               @foreach($affiliatedGroups as $affiliatedGroupSingle)
                                   <div class="col-sm-4 mb-2">
                                       <a href="{{$affiliatedGroupSingle->is_share_able || $affiliatedGroupSingle->is_public==1 ? route('share.group' ,base64_encode($affiliatedGroupSingle->id)) : '#'}}">
                                           <div class="card" style="background-color: #edf2f3">
                                               <div class="card-body">
                                                   <div class="main-text">
                                                       <p class="name">
                                                           {{str_limit($affiliatedGroupSingle->name ,30)}}
                                                           @if($affiliatedGroupSingle->is_public==1)
                                                               <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                           @else
                                                               <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                           @endif
                                                       </p>
                                                       @if($affiliatedGroupSingle->org_name)
                                                        <p><span style="font-size:13px">{{$affiliatedGroupSingle->org_name}}</span></p>
                                                       @endif
                                                       @if(isset($affiliatedGroupSingle->categoryName))
                                                        <p>{{ "Category : ".$affiliatedGroupSingle->categoryName}}</p>
                                                       @endif

                                                   </div>
                                               </div>
                                           </div>
                                       </a>
                                   </div>
                               @endforeach
                           @else
                                 There are no affiliated groups
                           @endif

                       </div>
                   </div>
                    <div role="tabpanel" class="tab-pane fade" id="subOrganization">
                        @include('components.profile.sub-organization')
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="orgPartner">
                        @include('components.profile.partner-tab')
                    </div>
                    @endif
                    @if($profile_info['is_volunteer']==1)
                        <div role="tabpanel" class="tab-pane fade" id="serviceProject">
                            @include('components.profile.service-project-tab')
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="customAttributes">
                            @include('components.profile.custom-attribute-tab')
                        </div>
                    @endif
                </div>
            </div>
        