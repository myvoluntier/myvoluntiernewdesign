@if($profile_info['is_my_profile'] == 1)
<div class="report_typescript" style="padding: 15px">
    <a href="javascript:void(0);" name="filter_print"  class="add_partner btn btn-success" style="float:left; margin-bottom: 20px;">Add Partner</a>
</div>


@endif

<style>
    .actionBtn {
        font-size: 11px;
        padding: 3px 10px 3px 10px;
    }

    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }

    .error-has {
        border: 1px solid #ff0000 !important;
    }
</style>

<div id="filter_content">
    <div style="overflow: auto; clear: both;">
        <table id="example21" class="table sortable">
            <thead>
            <tr>
                <th>
                    <div class="main-text"><p>Partner Organization</p></div>
                </th>
                <th>
                    <div class="main-text"><p>Action</p></div>
                </th>
            </tr>

            </thead>
            <tbody>

            @if(count($partners) > 0)
                @foreach($partners as $partnerValue)
                    <tr>
                        <td><?php echo wordwrap($partnerValue['org_name'], 45, '<br>', true); ?></td>
                        <td>
                             @if($profile_info['is_my_profile'] == 1)
                            <div class="buttons">
                                <a href="javascript:void(0);"
                                   data-partnerid= "{{base64_encode($partnerValue['id'])}}"
                                   class="actionBtn btn btn-success editPartnerBtn"><span>Edit</span></a>
                                | &nbsp;
                                <a data-opportuniti-id="{{$partnerValue['id']}}"
                                   href="javascript:void(0);"
                                   onclick="confirm_modal('<?php echo route('delete-partner') . '/' . base64_encode($partnerValue['id']); ?>');"
                                   class="actionBtn btn btn-danger"><span>Delete</span></a>
                            </div>
                            @endif
                        </td>

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan='4' style="text-align:center"> There are no partner organizations</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

</div>

<div class="modal fade" id="delete_modal_partner">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h4 class="modal-name" style="text-align:center;">
                    Are You Sure You Want To Delete ?
                </h4>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" class="btn btn-danger" id="delete_partner_link">Delete</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            </div>
            <input type="hidden" id="partnerURL" value="{{url('/organization/profile#orgPartner') }}" />
        </div>
    </div>
</div>

@include('components.auth.modal_pop_up_partner')

<script>
    function confirm_modal(url) {

        jQuery('#delete_modal_partner').modal('show', {backdrop: 'static'});
        document.getElementById('delete_partner_link').setAttribute('href', url);
    }

    function printData() {
        window.print();
    }
</script>