<?php if($profile_info['is_my_profile'] == 1){ ?>
    <div class="report_typescript" style="padding: 15px">
        <a href="{{route('create-service-project')}}" name="filter_print" class="btn btn-success" style="float:left; margin-bottom: 20px;">Create Service Project</a>
    </div>
<?php } ?>
<style>
.actionBtn{font-size: 11px;
    padding: 3px 10px 3px 10px;}
</style>

<div id="filter_content">
   <div style="overflow: auto; clear: both;">
		<table id="example21" class="table sortable">
			<thead>
			<tr>
				<th>
					<div class="main-text"><p>Service Project Title</p></div>
				</th>
				<th>
					<div class="main-text"><p>Total Hours</p></div>
				</th>
				<th>
					<div class="main-text"><p>No of Opportunities</p></div>
				</th>
				<th>
					<div class="main-text"><p>Start Date</p></div>
				</th>
				<th>
					<div class="main-text"><p>End Date</p></div>
				</th>
				<th>
					<div class="main-text"><p></p></div>
				</th>

			</tr>
			</thead>
			<tbody>
				@if(!empty($serviceProjects))
					@foreach($serviceProjects as $value)
						<tr>
							<td><?php echo wordwrap($value['title'],45,'<br>', true); ?></td>
							<td>{{$value['total_hours']}}</td>
							<td>{{$value['number_opp']}}</td>
							<td>{{dateMDY($value['start_date'])}}</td>
							<td>{{dateMDY($value['end_date'])}}</td>
							<td>
                                                            @if($profile_info['is_my_profile'] == 1)
								<div class="buttons">
									<a href="{{route('view-service-project').'/'.base64_encode($value['id'])}}" class="actionBtn btn btn-info"><span>View</span></a>
									| &nbsp;
									<a href="{{route('edit-service-project').'/'.base64_encode($value['id'])}}" class="actionBtn btn btn-success"><span>Edit</span></a>
									| &nbsp;
									<a data-opportuniti-id="{{$value['id']}}" 
                                                                           href="javascript:void(0);" onclick="confirm_modal_service('<?php echo route('delete-service-project').'/'.base64_encode($value['id']); ?>');"
                                                                           class="actionBtn btn btn-danger"><span>Delete</span></a>
								</div>
                                                            @endif
							</td>
						</tr>
					@endforeach
				@else
						<tr>
							<td colspan='6' style="text-align:center"> No Record Found !!!</td>
						</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="delete_modal">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h4 class="modal-name" style="text-align:center;">
                    Are You Sure You Want To Delete ?
                </h4>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" class="btn btn-danger" id="delete_link">Delete</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    function confirm_modal_service(url){
        jQuery('#delete_modal').modal('show', {backdrop: 'static'});
        document.getElementById('delete_link').setAttribute('href', url);
    }
    function printData()
    {
        window.print();
    }
</script>