
@if($user->user_role === 'organization')
    <div class="main-text margin_bottom_20 border_blk">
    <div class="row">
        <div class="col-12 col-md-12">
            <h5 class="title_org">Organization Details</h5>
        </div>
        <div class="col-12 col-md-4">
            <div class="main-text">				
				<label class="label_text_blk">Organization Name:</label>
                <div class="org_name_detail input_blk gray_bg_input">{{$user->org_name}}</div>

                <!--p class="bold">Organization ID:</p>
                <p class="org_name_detail">{{$user->id}}</p-->
                @if($authUser)
                    <label class="label_text_blk">Contact Email:</label>
                    <div class="user_email_detail input_blk gray_bg_input">{{$user->email}}</div>

                    {{-- <p class="bold">Phone Number:</p> --}}
                    <label class="label_text_blk">Phone Number:</label>
                    <div class="user_phone_detail input_blk gray_bg_input">{{ $user->contact_number }}</div>
                    @if($profile_info['is_my_profile'] == 1 || $profile_info['is_friend'])@endif
                @endif
            </div>

        </div>

        <div class="col-12 col-md-4">
            <div class="dashboard_org">
                <div class="track-it-time_slider">
                    <div class="slider">
                        <div>

                            <div class="slider-wrapper">
                                <div>
                                    <div class="slider-item-scale">

                                        <div>
                                            <span>IMPACTS</span>
                                            <span>{{$profile_info['tracks_hours']}}</span>
                                            <span>HOURS</span>

                                            @if($profile_info['is_my_profile'] == 1)
                                                <a href="{{ route('organization-track') }}"><span>Track</span></a>
                                            @elseif(isset(Auth::user()->id))
                                                <div class="wrapper_connect_button">
                                                    @if($profile_info['is_friend'] == 0)
                                                        <a id="btn_connect" href="#"><span>Connect</span></a>
                                                        <a id="btn_pending" style="display: none" href="#"><span>Pending..</span></a>

                                                    @elseif($profile_info['is_friend'] == 1)
                                                        <a id="btn_pending"  href="#"><span>Pending..</span></a>

                                                    @elseif($profile_info['is_friend'] == 3)
                                                        <a id="btn_accept" href="#"><span>Accept</span></a>
                                                    @else
                                                        <?php
                                                        $args = ['chatId' => $chat_id];
                                                        if($chat_deleted){
                                                            $args['create'] = true;
                                                            $args['opponent'] = $user->id;
                                                        }
                                                        ?>

                                                        @if(Auth::user()->user_role == 'organization')
                                                            <a id="btn_disconnect" href="{{url('/organization/disconnectFriend/'.$user->id)}}" style="margin-bottom: 10px;"><span>disconnect</span></a>
                                                        @else
                                                            <a id="btn_disconnect" href="{{url('/volunteer/disconnectFriend/'.$user->id)}}" style="margin-bottom: 10px;"><span>disconnect</span></a>
                                                        @endif

                                                        <br/>
                                                        <a id="btn_chat" href="<?php echo route( Auth::user()->user_role . '-chat', $args )?>"><span>Chat</span></a>
                                                    @endif
                                                </div>
                                            @endif

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>    
        <div class="col-12 col-md-4">
            <div class="details_content">
                    <div class="map-box" id="pos_map">
                        <input type="hidden" id="lat_val" value="{{$user->lat}}">
                        <input type="hidden" id="lng_val" value="{{$user->lng}}">
                    </div>

                    <div class="main-text">
                        <p class="bold">Primary Location:</p>
                        <p>{{implode(', ', array_filter([$user->city, $user->state, $user->zipcode]))}}</p>

                        @if($user->website)
                            <p class="bold">WebSite:</p>
                            <p>{{$user->website}}</p>
                        @endif

                        <p class="bold">Organization Type:</p>
                        <p>{{$user->type_name}}</p>

                    </div>
                </div>
        </div>
    </div>
    </div>
@else

<div class="row">
    <div class="col-12 order-0 col-md-8 order-md-0">

        @if($profile_info['is_my_profile'] == 1 || $profile_info['is_friend'] > 1)
            <div class="main-text">
                <form action="{{url('/volunteer/commentStatus')}}" method="post">
                    {{ csrf_field() }}
                    <p class="title">Personal Info</p>

                    @if($user->location)
                        <p class="bold">User Name:</p>
                        <p class="user_name_profile">{{$user->user_name}}</p>
                    @endif

                    <p class="bold">Email:</p>
                    <p class="user_email_profile">{{$user->email}}</p>

                    @if(Auth::user()->id  != $user->id)
                        @if($user->post_status)
                            <p class="bold">Status:</p>
                            <p>{{$user->post_status}}</p>
                            <hr>

                            {{--<!-- <p class="bold">Comments:</p>--}}
                            {{--@foreach($comments as $c)--}}
                                {{--@if($c->status_user_id == $user->id)--}}
                                {{--<i>{{$c->commenter_name}}</i>: {{$c->body}} <br><br>--}}
                            {{--@endif--}}
                            {{--@endforeach -->--}}

                        @endif
                    @endif
                </form>

                <?php
                $dob = $user->birth_date;
                $diff = (date('Y') - date('Y', strtotime($dob)));
                ?>
                @if($user->show_age == 'Y' && $user->user_role == 'volunteer')
                    <p class="bold">Age:</p>
                    <p>{{$diff}}</p>
                @elseif($user->show_age == 'N' && $user->user_role == 'volunteer')
                    @if(($profile_info['is_my_profile'] == 0) && ($profile_info['is_friend'] == 1))
                        <p class="bold">Birthdate:</p>
                        <p>{{$user->birth_date}}</p>
                    @endif
                @elseif($user->user_role == 'organization')
                    @if(($profile_info['is_my_profile'] == 0) && ($profile_info['is_friend'] == 1))
                        <p class="bold">Birthdate:</p>
                        <p>{{$user->birth_date}}</p>
                    @endif
                @endif

                @if($user->show_address  == 'Y' && $user->user_role == 'volunteer')
                    <p class="bold">Primary Location:</p>
                    <p>{{$user->city}}, {{$user->state}}, {{$user->country}} {{$user->zipcode}}</p>
                @endif
            </div>
        @endif
    </div>
    <div class="col-12 order-2 col-md-4 order-md-1">
        <div class="dashboard_org">

            <!--
            <div class="track-it-time_slider">
                <div class="slider">
                    <div>

                        <div class="slider-wrapper">
                        <div>
                            <div class="slider-item-scale">

                                <div>
                                    <span>IMPACTS</span>
                                    <span>37</span>
                                    <span>HOURS</span>
                                    <a href="#"><span>Add hours</span></a>
                                </div>

                            </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
            -->

            <div class="wrapper_green_bg">
                <div class="main-text">
                    <p>IMPACTS</p>
                    <p class="bold">{{$profile_info['logged_hours']}}</p>
                    <p class="light">HOUR(S)</p>
                    @if($profile_info['is_my_profile'] == 0)
                        <div class="wrapper_connect_button">
                            @if($profile_info['is_friend'] == 0)

                                <a id="btn_connect" href="#"><span>Connect</span></a>

                                <a id="btn_pending" style="display: none" href="#"><span>Pending..</span></a>


                            @elseif($profile_info['is_friend'] == 1)
                                <a id="btn_pending"  href="#"><span>Pending..</span></a>

                            @elseif($profile_info['is_friend'] == 3)
                                <a id="btn_accept"  href="#"><span>Accept</span></a>

                            @else
                                <?php $args = ['chatId' => $chat_id];
                                if($chat_deleted){
                                    $args['create'] = true;
                                    $args['opponent'] = $user->id;
                                }
                                ?>
                                @if(Auth::user()->user_role == 'organization')
                                    <a id="btn_disconnect" href="{{url('/organization/disconnectFriend/'.$user->id)}}" style="margin-bottom: 10px;"><span>disconnect</span></a>
                                @else
                                    <a id="btn_disconnect" href="{{url('/volunteer/disconnectFriend/'.$user->id)}}"  style="margin-bottom: 10px;"><span>disconnect</span></a>
                                @endif
                                </br>
                                <a id="btn_chat"
                                   href="<?php echo route( Auth::user()->user_role . '-chat', $args )?>"><span>Chat</span></a>
                            @endif

                        </div>
                    @else
                        <a href="{{url('/volunteer/single_track')}}"><span>Add hours</span></a>
                    @endif

                </div>
            </div>
        </div>

    </div>
</div>

@endif