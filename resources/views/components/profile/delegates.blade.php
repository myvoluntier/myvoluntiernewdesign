@if($authUser)
<div class="report_typescript" style="padding: 15px">
    <a href="javascript:void(0);" class="registerDelegate btn btn-success" style="float:left; margin-bottom: 20px;">Create Delegate</a>
</div>
@endif

<div id="filter_content">
    <div style="overflow: auto; clear: both;">
        <table id="delegateTable" class="table sortable">
            <thead>
            <tr>

                <th><div class="main-text"><p>Name</p></div></th>
                @if($authUser)
                    <th>
                        <div class="main-text"><p>Email</p></div>
                    </th>
                    <th>
                        <div class="main-text"><p>Comments</p></div>
                    </th>
                    <th>
                        <div class="main-text"><p>Action</p></div>
                    </th>
                @endif

            </tr>
            </thead>
            <tbody>
            @if($orgDelegates)
                @foreach($orgDelegates as $delegate)
                    <tr>
                        <td>{{$delegate->first_name}} {{$delegate->last_name}}</td>

                        @if($authUser)
                            <td>{{$delegate->email}}</td>
                            <td>{{$delegate->comments}}</td>

                            <td>
                                <div class="buttons">
                                    <a href="javascript:void(0);"
                                       data-delId="{{base64_encode($delegate->id)}}"
                                       data-delFirst="{{$delegate->first_name}}"
                                       data-delLast="{{$delegate->last_name}}"
                                       data-delEmail="{{$delegate->email}}"
                                       data-delComments="{{$delegate->comments}}"
                                       class="actionBtn btn btn-success editDelegateBtn"><span>Edit</span></a>

                                    | &nbsp;

                                    <a href="javascript:void(0);"
                                       onclick="confirm_modal_delegate('<?php echo route('delete-delegate' ,$delegate->id)?>');"
                                       class="actionBtn btn btn-danger"><span>Delete</span></a>

                                </div>
                            </td>
                        @endif
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan='5' style="text-align:center"> No Record Found !!!</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="delegateRegistrationModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
        <div class="modal-dialog-fix">
            <div>
                <div class="modal-content">
                    <div class="modal-header">

                        <div class="main-text">
                            <h3 class="h3">Organization<strong class="green"> Delegates</strong>!</h3>
                        </div>

                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </a>

                    </div>
                    <div class="modal-body" id="subOrgBody">
                        @include('components.auth.modal_form_delegate')
                    </div>

                    <div class="modal-footer">
                        <div class="wrapper_row_link">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="wrapper-link two-link margin-top">
                                        <a class="btn_delegate_close red" href="javascript:void(0);"><span>Close</span></a>
                                        <a class="btn_delegate_save" href="javascript:void(0);"><span>Submit</span></a>
                                        <a class="btn_delegate_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="delete_modal_delegate">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h4 class="modal-name" style="text-align:center;">
                    Are You Sure You Want To Delete DELEGATE?
                </h4>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" class="btn btn-danger" id="delete_delegate_link">Delete</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>

    function confirm_modal_delegate(url) {
        jQuery('#delete_modal_delegate').modal('show', {backdrop: 'static'});
        document.getElementById('delete_delegate_link').setAttribute('href', url);
    }

</script>
