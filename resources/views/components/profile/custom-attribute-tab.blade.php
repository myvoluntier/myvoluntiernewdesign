@if($profile_info['is_my_profile'] == 1)
<div class="report_typescript" style="padding: 15px">
    <a href="javascript:void(0);" name="filter_print"  class="add_attribute btn btn-success" style="float:left; margin-bottom: 20px;">Add Custom Attribute</a>
</div>


@endif

<style>
    .actionBtn {
        font-size: 11px;
        padding: 3px 10px 3px 10px;
    }

    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }

    .error-has {
        border: 1px solid #ff0000 !important;
    }
</style>

<div id="filter_content">
    <div style="overflow: auto; clear: both;">
        <table id="example21" class="table sortable">
            <thead>
            <tr>
                <th>
                    <div class="main-text"><p>Sr.</p></div>
                </th>
                <th>
                    <div class="main-text"><p>Attribute Key</p></div>
                </th>
                <th>
                    <div class="main-text"><p>Attribute Value</p></div>
                </th>
                <th>
                    <div class="main-text"><p>Action</p></div>
                </th>
            </tr>

            </thead>
            <tbody>

            @if(count($customAttributes) > 0)
                @foreach($customAttributes as $subValue)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><?php echo wordwrap($subValue['attributekey'], 45, '<br>', true); ?></td>
                        <td><?php echo wordwrap($subValue['attributevalue'], 45, '<br>', true); ?></td>
                        <td>
                            <?php if($profile_info['is_my_profile'] == 1){ ?>
                            <div class="buttons">
                                <a href="javascript:void(0);"
                                   data-cust-attrid= "{{base64_encode($subValue['id'])}}"
                                   class="actionBtn btn btn-success editBtn"><span>Edit</span></a>
                                | &nbsp;
                                <a data-opportuniti-id="{{$subValue['id']}}"
                                   href="javascript:void(0);"
                                   onclick="confirm_modal('<?php echo route('delete-customattribute') . '/' . base64_encode($subValue['id']); ?>');"
                                   class="actionBtn btn btn-danger"><span>Delete</span></a>
                            </div>
                            <?php }?>
                        </td>

                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan='4' style="text-align:center"> No Custom attributes found !!!</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

<hr>
    <div class="main-text">

        <p class="title">Groups that you were automatically enrolled in</p>

        <div class="row">
            @foreach($dynamic_group as $groupSingle)
                <div class="col-sm-4 mb-2">
                    <a href="{{$groupSingle->is_share_able ? route('share.group' ,base64_encode($groupSingle->id)) : 'javascript:void(0)'}}">
                        <div class="card" style="background-color: #edf2f3">
                            <div class="card-body">
                                <div class="main-text">
                                    <p class="name">
                                        {{str_limit($groupSingle->name ,30)}}
                                        <span title="Dynamic Group" class="badge badge-success pull-right"><small><i class="fa fa-users"></i></small></span>
                                    </p>
                                    @if(isset($groupSingle->org_name))
                                        <p><span style="font-size:13px">{{$groupSingle->org_name}}</span></p>
                                    @endif
                                    @if(isset($groupSingle->categoryName))
                                        <p>{{"Category : ".$groupSingle->categoryName}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="modal fade" id="delete_modal_cust_attr">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h4 class="modal-name" style="text-align:center;">
                    Are You Sure You Want To Delete ?
                </h4>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" class="btn btn-danger" id="delete_cust_attr_link">Delete</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            </div>
            <input type="hidden" id="custAttrURL" value="{{url('/volunteer/profile#customAttributes') }}" />
        </div>
    </div>
</div>
@include('components.auth.modal_pop_up_cust_attr')

<script>
    function confirm_modal(url) {

        jQuery('#delete_modal_cust_attr').modal('show', {backdrop: 'static'});
        document.getElementById('delete_cust_attr_link').setAttribute('href', url);
    }

    function printData() {
        window.print();
    }
</script>