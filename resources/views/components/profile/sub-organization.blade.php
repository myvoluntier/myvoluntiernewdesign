@if($authUser)
<div class="report_typescript" style="padding: 15px">
    <a href="javascript:void(0);" name="filter_print"  class="suborg_registr btn btn-success" style="float:left; margin-bottom: 20px;">Create Sub Organization</a>
</div>
<div class="report_typescript" style="float:right;">
    <a href="javascript:void(0);" onclick="filterDeactive('NOT YET ACTIVATED')" name="filter_print"  class="btn btn-success" style="float:left; margin-bottom: 10px;">Show Inactive</a>
    <a href="javascript:void(0);" onclick="filterDeactive('')" class="btn btn-info" style="float:left; margin: 0 0 10px 10px;">Reset</a>
</div>
@endif
<style>
    .actionBtn{font-size: 11px;padding: 3px 10px 3px 10px;}
    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }

    .error-has {
        border: 1px solid #ff0000 !important;
    }
</style>

    <div id="filter_content">
    <div style="overflow: auto; clear: both;">
        <table id="subOrgTable" class="table sortable">
            <thead>
                <tr>
            <!--
            <th>
            <div class="main-text"><p>Organization ID</p></div>
            </th>
            -->
            <th>
            <div class="main-text"><p>Name</p></div>
            </th>
            @if($authUser)
            <th>
            <div class="main-text"><p>Email</p></div>
            </th>
            <th>
            <div class="main-text"><p>Phone Number</p></div>
            </th>
            <th>
            <div class="main-text"><p>Action</p></div>
            </th>
            @endif

            </tr>
            </thead>
            <tbody>
                @if(!empty($subOrganization) > 0)
                    @foreach($subOrganization as $subValue)
                        <tr>
                            <!--<td><?php echo wordwrap($subValue['user_name'], 45, '<br>', true); ?></td>--}}-->
                            <td><?php echo wordwrap($subValue['org_name'], 45, '<br>', true); ?></td>
                            @if($authUser)
                            <td>{{$subValue['email']}}</td>
                            <td>{{$subValue['contact_number']}}</td>
                            
                            <td>
                                <div class="buttons">
                                    <a href="javascript:void(0);" data-sub-orgid = {{base64_encode($subValue['id'])}} class="actionBtn btn btn-success editSubOrgBtn"><span>Edit</span></a>
                                    | &nbsp;
                                    @if($subValue['status'] == 1)
                                        <a data-opportuniti-id="{{$subValue['id']}}" 
                                           href="javascript:void(0);" onclick="confirm_modal_suborg('<?php echo route('update-suborg-status') . '/' . $subValue['id']. '/' . $subValue['id']. '/0'; ?>', 'Deactivate');"
                                           class="actionBtn btn btn-success"><span>Deactivate</span></a>
                                    @else
                                        <a data-opportuniti-id="{{$subValue['id']}}" 
                                       href="javascript:void(0);" onclick="confirm_modal_suborg('<?php echo route('update-suborg-status') . '/' . $subValue['id']. '/' . $subValue['id']. '/1'; ?>', 'Activate');"
                                       class="actionBtn btn btn-danger"><span>Not Yet Activated</span></a>
                                    @endif
                                </div>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan='5' style="text-align:center"> No Record Found !!!</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="delete_modal_suborg">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top:100px;">
            <div class="modal-header">
                <h4 class="modal-name" style="text-align:center;">
                    Are You Sure You Want To <span id="modalLbl"> Active </span> ?
                </h4>
            </div>
            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                <a href="#" class="btn btn-danger" id="delete_link_suborg">Delete</a>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            </div>
            <input type="hidden" id="subOrgURL" value="{{url('/organization/profile#subOrganization') }}" />
        </div>
    </div>
</div>
@include('components.auth.modal_pop_up_sub_org')

<script>

    function confirm_modal_suborg(url, lableVal) {
        $('#modalLbl, #delete_link_suborg').html(lableVal);
        jQuery('#delete_modal_suborg').modal('show', {backdrop: 'static'});
        document.getElementById('delete_link_suborg').setAttribute('href', url);
    }
    function printData()
    {
        window.print();
    }
    function filterDeactive(textSearch) {
        // Declare variables
        var input, filter, table, tr, td, i, txtValue;
       // input = document.getElementById("myInput");
        //filter = input.value.toUpperCase();
        filter = textSearch;
        table = document.getElementById("subOrgTable");
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[3];
          if (td) {
            txtValue = td.textContent || td.innerText;
            console.log(txtValue);
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }
      }

</script>
