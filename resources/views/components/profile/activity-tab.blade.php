<style>
    .activity_holder {
        float: left;
        width: 90%;
    }

    .upload_att {
        float: right;
        width: 10%;
        padding:0;
    }

    .upload_att .choose {
        position: relative;
        width: 30px;
        height: 30px;
        background: #42bd41;
        color: #fff;
        text-align: center;
        border-radius: 100%;
        line-height: 30px;
        cursor: pointer;
        display: inline-block;
    }

    .upload_att .choose input {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        cursor: pointer;
        height: 100%;
        opacity: 0;
        z-index: 99;
    }

    .activity_holder input {
        margin-right: -74px;
        background: #42bd41;
    }
    .title_blk_tex{
        padding: 0 0 10px;
    }

    @media only screen and (max-width: 768px) {
        .activity_holder {
            width: 60%;
        }

        .upload_att {
            width: 40%;
        }
    }
</style>
@if($user->user_role !== 'organization')
    
    @if($profile_info['is_my_profile']==1)
     
        @if($comments->count()>0)
            @foreach($comments as $c)
            <?php $date = date('F j, Y', strtotime($c->logged_date)); ?>
            <div class="container"> 
                <div class="title_blk_tex">
                    <b><a href="{{url('volunteer/view_opportunity/'.$c->id)}}">{{$c->oppor_name}}</a></b> completed with <b><a href="{{url('/volunteer/show_organization/'.$c->org_id)}}">{{$c->org_name}}</a></b> on <b>{{$date}}</b> for <b>{{$c->logged_mins/60}} hours.</b>
                </div>
            <div class="clearfix">
                @if($c->comment==NULL)
                <div class="activity_holder">
                    <form method="post" action="{{url('/volunteer/commentStatus')}}">
                        <div class="form-group">
                            <textarea class="form-control" rows="1" id="comment" name="comment" required></textarea>
                        </div>
                        <input type="submit" name="add_comment" class="btn btn-success" value="Post Comment" style="float:right";>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="id" value="{{$c->id}}">
                    </form>
                </div>
                @else
                <p>{{$c->comment}}</p>
                <form method="post" action="{{url('/delete_comment')}}">
                    <input type="submit" name="add_comment" class="btn btn-danger" value="Delete Comment" style="float:right";>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{$c->id}}">
                </form>
                @endif
                @if($c->image==NULL)
                <div class="upload_att">
                    <form method="post" action="{{url('/upload_image')}}" enctype="multipart/form-data">
                        <div class="choose">
                            <i class="fa fa-paperclip"></i>
                            <input type="file" name="image" multiple accept='image/*' required >
                        </div>
                        <div class="choose">
                            <i class="fa fa-upload"></i>
                            <input type="submit" name="add_comment" class="btn btn-success" value="Upload Image" style="float:right";>
                        </div>
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="id" value="{{$c->id}}">
                    </form>
                </div>
                @else
                
                <img src="{{$c->image}}" height="250px" width="250px"/>
                <form method="post" action="{{url('/delete_image')}}">
                    <input type="submit" name="add_comment" class="btn btn-danger" value="Delete Image" style="float:right";>
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{$c->id}}">
                </form>
                @endif
                
                </div>
                <br>
                <!-- <div  class="wrapper-link alpha">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        <span><b>Edit Comment</b></span>
                    </button>
                </div> -->
            </div>
            @endforeach
            <div style="padding-top:20px">
                {{$comments->fragment('activity')}}
            </div>
        @endif

        @elseif($profile_info['is_friend']>0)
            @if($comments->count()>0)
                @foreach($comments as $c)
                <?php $date = date('F j, Y', strtotime($c->logged_date)); ?>
                <div class="container">
                    @if(!empty(Auth::user()) && Auth::user()->user_role == 'organization')
                        <b><a target="blank" href="{{url('organization/view_opportunity/'.$c->id)}}">{{$c->oppor_name}}</a></b> completed with <b><a href="{{url('/volunteer/show_organization/'.$c->org_id)}}">{{$c->org_name}}</a></b> on <b>{{$date}}</b> for <b>{{$c->logged_mins/60}} hours.</b>
                    @elseif(empty(Auth::user()) && $user->user_role == 'organization')
                        <b><a target="blank" href="{{url('organization/share_opportunity/'.$c->id)}}">{{$c->oppor_name}}</a></b> completed with <b><a href="{{url('/volunteer/show_organization/'.$c->org_id)}}">{{$c->org_name}}</a></b> on <b>{{$date}}</b> for <b>{{$c->logged_mins/60}} hours.</b>
                    @else
                        <b><a href="{{url('volunteer/view_opportunity/'.$c->id)}}">{{$c->oppor_name}}</a></b> completed with <b><a href="{{url('/volunteer/show_organization/'.$c->org_id)}}">{{$c->org_name}}</a></b> on <b>{{$date}}</b> for <b>{{$c->logged_mins/60}} hours.</b>
                    @endif
                    <div class="clearfix">
                        <p>{{$c->comment}}</p>
                        @if($c->image)
                        <img src="{{asset($c->image)}}" height="250px" width="250px"/>
                        @endif
                    </div>
                </div>       
                @endforeach
            @endif
        @endif

@elseif($user->user_role == 'organization')
        @if($opportunity->count()>0)
            @if($authUser)
            <div class="wrapper_row_link" style="float:right;width:40%;">
                    <div class="wrapper-link two-link margin-top">
                        <a id="export_member" href="{{url('/organization/opportunities-members') }}"><span>Export Members</span></a>
                    </div>
                </div>
            @endif
            @foreach($opportunity as $o)
                <?php $date = date('F j, Y', strtotime($o->created_at)); ?>
                    <div class="container">
                        @if(!empty(Auth::user()) && Auth::user()->user_role == 'organization')
                            <div class="title_blk_tex"><b><a target="blank" href="{{url('organization/view_opportunity/'.$o->id)}}">{{$o->title}}</a></b> created on <b>{{$date}}</b> </div>
                        @elseif(empty(Auth::user()) && $user->user_role == 'organization')
                            <div class="title_blk_tex"><b><a target="blank" href="{{url('organization/share_opportunity/'.$o->id)}}">{{$o->title}}</a></b> created on <b>{{$date}}</b> </div>
                        @else
                            <div class="title_blk_tex"><b><a href="{{url('volunteer/view_opportunity/'.$o->id)}}">{{$o->title}}</a></b> created on <b>{{$date}}</b> </div>
                        @endif
                    </div>      
            @endforeach
        @else
            <div class="container">Sorry, No opportunity found...!</div>
        @endif  
    
    @endif