<div  id="filter_controller" style="padding: 10px 15px 10px 15px; background: #efefef; border: solid 1px #333;">
    <label style="margin-bottom: 0;">Filter Results:</label>
    <img src="{{asset('img/arrow_down.png')}}" style="float: right; width: 20px; padding-top: 7px; opacity:0.3;">
</div>

<div class="filter_div" style="padding: 15px; background: #efefef; display: none">
    <div class="row">
        <div class="col-12 col-md-6">
            <label class="label-filter">Opportunity Types:</label>
            <div class="wrapper_select">
                <select class="opp-search-checkbox form-control" id="opportunity_type_select">
                    <option value="" class="oppr_li" id="opprtypeAll">All</option>
                    @foreach($oppr_types as $key=>$ot)
                        <option value="{{$key}}" class="oppr_li" id="opprtype{{$key}}">{{$ot['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <label class="label-filter">Organization Types:</label>
            <div class="wrapper_select">
                <select class="org-search-checkbox form-control" id="organization_type_select">
                    <option value="" class="oppr_li" id="orgtypeAll">All</option>
                    @foreach($org_types as $key=>$or)
                        <option value="{{$key}}" class="oppr_li" id="orgtype{{$key}}">{{$or['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="label-filter" style="margin-left: 15px">Date Range:</label>
        <div class="col-12 col-md-3">
            <input type="text" id="transcript_start_date" name="transcript_start_date" class="form-control chose-date" placeholder="Start Date">
        </div>
        <div class="col-12 col-md-3">
            <input type="text" id="transcript_end_date" class="form-control chose-date" placeholder="End Date">
        </div>
        <div class="col-12 col-md-2">
            <a href="#" id="filter_apply" name="filter" class="btn btn-success">Apply Filter</a>
        </div>
    </div>
</div>

<div class="report_typescript" style="padding:15px 0">
    <a href="#" id="share_transcript" name="filter" class="btn btn-success" style="float:right">Share</a>
    <a href="#" name="filter_print" onclick="printData()" class="btn btn-success" style="margin-right: 10px;">Export (PDF)</a>
</div>

<div class="voluntier_info" style="display: none">
    <Img src="{{asset('front-end/img/logo.jpg')}}" style="margin-bottom: 10px">
    <h4><strong>Volunteer</strong></h4>
    <div style="padding-left: 45px">
        <label>Name: <strong>{{$user->first_name.' '.$user->last_name}}</strong></label><br>
        <label>Contact: {{$user->contact_number}}</label><br>
        <label>Email: {{$user->email}}</label><br>
    </div>
    <h4><strong>Transcript</strong></h4>
</div>

<div id="filter_content">
    @foreach($tracks as $key=>$t)
            <div class="typescript">
                <div id="">
                         <div class="tran_title_outer">
                                <div class="tran_logo_blk">
                                    <img src="{{$t['org_logo'] == NULL ? asset('front-end/img/org/001.png') : $t['org_logo']}}" class="org_logo">
                                </div>
                                <div class="tran_info_addr">
                                    <div class="org_info">
                                        <Label><strong>{{$t['org_name']}}</strong></Label>
                                        <Label>{{$t['contact_number']}}</Label>
                                        <Label>{{$t['email']}}</Label>
                                    </div>
                                    <div class="org_info_addr">
                                        <Label>{{$t['address']}}</Label>
                                    </div>
                                </div>
                        </div>
                </div>
            

            <div style="overflow: auto; clear: both;">
                <table id="example21" class="table sortable">
                    <thead>
                    <tr>
                        <th>
                            <div class="main-text"><p>Date/Time</p></div>
                        </th>
                        <th>
                            <div class="main-text"><p>Duration(hrs)</p></div>
                        </th>
                        <th style="width:18%;">
                            <div class="main-text"><p>Opportunity Title</p></div>
                        </th>
                        <th>
                            <div class="main-text"><p>Category/Type</p></div>
                        </th>
                        <th>
                            <div class="main-text"><p>Date Validated</p></div>
                        </th>
                        <th style="width:18%;">
                            <div class="main-text"><p>Validator Name</p></div>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($t['track_info'] as $opp)
                        <tr>
                            <td>{{date('m-d-Y',strtotime($opp['logged_date']))}}</td>
                            <td>{{$opp['logged_mins']/60}}</td>
                            <td>{{$opp['oppor_name']}}</td>
                            <!--td>{{$oppr_types[$opp['category_type']]['name']}}</td-->
							<td>{{$opp['category_names']}}</td>
                            <td>{{($opp['confirmed_at'] != "")? date('m-d-Y',strtotime($opp['confirmed_at'])) : ""}}</td>
                            <td>{{\App\Opportunity::find($opp['oppor_id'])->contact_name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach
    <div style="padding-top:20px">
    {{$tracks->fragment('transcript')}}
     </div>
</div>
<script src="{{asset('js/jquery-3.3.1.js')}}"></script>
<script>
 $( document ).ready(function() {
  if(window.location.href.indexOf("transcript")>1){
        $("#transcriptTab").parent().addClass('active');
        $("#activityTab").parent().removeClass('active');
        $("#home").removeClass('active show');
        $("#menu1").addClass('active show');
    }else if(window.location.href.indexOf("activity")>1){
        $("#transcriptTab").parent().removeClass('active');
        $("#activityTab").parent().addClass('active');
        $("#home").addClass('active show');
        $("#menu1").removeClass('active show');
    }
 });

 $("#activityTab").click(function(){
    location.replace(location.origin+location.pathname+"#activity");  
    $("#menu1").removeClass('active show');
    $("#home").addClass('active show');
 });
 $("#transcriptTab").click(function(){
    location.replace(location.origin+location.pathname+"#transcript");  
    $("#menu1").addClass('active show');
    $("#home").removeClass('active show');
 });
 $("#infoTab").click(function(){
    $("#menu1").removeClass('active show');
    $("#home").removeClass('active show');
 });
 $("#groupTab").click(function(){
    $("#menu1").removeClass('active show');
    $("#home").removeClass('active show');
 });
 $("#serviceTab").click(function(){
    $("#menu1").removeClass('active show');
    $("#home").removeClass('active show');
 });
 $("#customTab").click(function(){
    $("#menu1").removeClass('active show');
    $("#home").removeClass('active show');
 });
 
    function printData()
    {
        window.print();
    }
</script>