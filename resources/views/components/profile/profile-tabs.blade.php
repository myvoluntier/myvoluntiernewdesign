<main>
    <article class="panel-group bs-accordion" id="accordion" role="tablist" aria-multiselectable="true">
        @if($profile_info['is_my_profile'] == 1 || $profile_info['is_friend'] > 1 )
        @if($profile_info['is_volunteer']==1)
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="activityLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#activity" aria-expanded="false" aria-controls="activity">
                        <span>Activity</span>
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="false"></span>
                    </a>
                </h4>
            </div>

            <div id="activity" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.activity-tab')
                </div>
            </div>
        </section>
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="transcriptLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#transcript" aria-expanded="false" aria-controls="transcript">
                        <span>Transcript</span>
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="transcript" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.transcript-tab')
                </div>
            </div>
        </section>
        @elseif($profile_info['is_volunteer']==0)
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="activityLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#activity" aria-expanded="false" aria-controls="activity">
                        <span>Opportunity</span>
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="activity" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" >
                <div class="panel-body">
                    @include('components.profile.activity-tab')
                </div>
            </div>
        </section>
        @endif


        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="detailsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#details" aria-expanded="false" aria-controls="details">
                        {{$user->user_role === 'organization' ? 'Details' : 'Info' }}
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="details" class="panel-collapse collapse" role="tabpanel" aria-labelledby="details" style="height: 0px;">
                <div class="panel-body">

                    @include('components.profile.detail-tab')
                    @if($profile_info['is_my_profile'] == 1)
                        <hr>
                        @include('components.profile.account-tab')
                    @endif
                </div>
            </div>
        </section>
        @else
        @if($profile_info['is_volunteer']==0)
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="activityLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#activity" aria-expanded="false" aria-controls="activity">
                        Opportunity
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="activity" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.activity-tab')
                </div>
            </div>
        </section>
        @endif
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="detailsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#details" aria-expanded="false" aria-controls="details">
                        {{$user->user_role === 'organization' ? 'Details' : 'Info' }}
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="details" class="panel-collapse collapse" role="tabpanel" aria-labelledby="details">
                <div class="panel-body">
                    @include('components.profile.detail-tab')
                    @if($profile_info['is_my_profile'] == 1)
                        <hr>
                        @include('components.profile.account-tab')
                    @endif
                </div>
            </div>
        </section>
        @endif
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="groupsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#groups" aria-expanded="false" aria-controls="groups">
                        Groups
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="groups" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <div class="row">

                        @if($groupCount > 0)
                        @if($profile_info['is_volunteer']==0 && $authUser)
                        <div class="col-sm-12 mb-12 wrapper-link" style="margin:0px !important;">
                            <div style="margin:10px !important;">
                                <a id="export_member" href="{{url('/organization/group-members') }}"><span>Export Members</span></a>
                            </div>
                        </div>
                        @endif
                        @foreach($group as $groupSingle)
                        <div class="col-sm-4 mb-2">
                            <a href="{{$groupSingle->is_share_able || $groupSingle->is_public ==1 ? route('share.group' ,base64_encode($groupSingle->id)) : '#'}}">
                                <div class="card" style="background-color: #edf2f3">
                                    <div class="card-body">
                                        <div class="main-text">
                                            <p class="name">
                                                {{str_limit($groupSingle->name ,30)}}
                                                @if($groupSingle->is_public ==1)
                                                <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                @else
                                                <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                @endif
                                            </p>
                                            @if(isset($groupSingle->org_name))
                                            <p><span style="font-size:13px">{{$groupSingle->org_name}}</span></p>
                                            @endif
                                            @if(isset($groupSingle->categoryName))
                                            <p>{{"Category : ".$groupSingle->categoryName}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        @else
                        <div class="col-sm-12 mb-12">
                            You do not have active groups yet
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </section>
        @if($user->user_role === 'organization')
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="affiliatedGroupsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#affiliatedGroups" aria-expanded="false" aria-controls="affiliatedGroups">
                        Affiliated Groups
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="affiliatedGroups" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <div class="row">

                        @if(count($affiliatedGroups))
                        @foreach($affiliatedGroups as $affiliatedGroupSingle)
                        <div class="col-sm-4 mb-2">
                            <a href="{{$affiliatedGroupSingle->is_share_able || $affiliatedGroupSingle->is_public==1 ? route('share.group' ,base64_encode($affiliatedGroupSingle->id)) : '#'}}">
                                <div class="card" style="background-color: #edf2f3">
                                    <div class="card-body">
                                        <div class="main-text">
                                            <p class="name">
                                                {{str_limit($affiliatedGroupSingle->name ,30)}}
                                                @if($affiliatedGroupSingle->is_public==1)
                                                <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                @else
                                                <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                @endif
                                            </p>
                                            @if($affiliatedGroupSingle->org_name)
                                            <p><span style="font-size:13px">{{$affiliatedGroupSingle->org_name}}</span></p>
                                            @endif
                                            @if(isset($affiliatedGroupSingle->categoryName))
                                            <p>{{ "Category : ".$affiliatedGroupSingle->categoryName}}</p>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        @else
                        <div class="col-sm-12 mb-12">
                               There are no affiliated groups
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </section>

        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="orgDelegatesTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#orgDelegates" aria-expanded="false" aria-controls="orgDelegates">
                        Delegates
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>

            <div id="orgDelegates" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.delegates')
                </div>
            </div>
        </section>

        @if(!isset($user->parent_id))
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="subOrganizationTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#subOrganization" aria-expanded="false" aria-controls="subOrganization">
                        Sub Organization
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="subOrganization" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.sub-organization')
                </div>
            </div>
        </section>
        @endif
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="orgPartnerTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#orgPartner" aria-expanded="false" aria-controls="orgPartner">
                        Partners
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="orgPartner" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.partner-tab')
                </div>
            </div>
        </section>
        @endif
        @if($profile_info['is_volunteer']==1)
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="serviceProjectTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#serviceProject" aria-expanded="false" aria-controls="serviceProject">
                        Service Projects
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="serviceProject" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.service-project-tab')
                </div>
            </div>
        </section>
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="customAttributesTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#customAttributes" aria-expanded="false" aria-controls="customAttributes">
                        Custom Attributes
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="customAttributes" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                   @include('components.profile.custom-attribute-tab')
                </div>
            </div>
        </section>
        @endif

    </article>
</main>
