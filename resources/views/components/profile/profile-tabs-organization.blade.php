
<main>
<style>
        /* my css add */
        .fade.in {
            opacity: 1;
        }
        .nav-tabs {
            border-bottom: none;
        }
        .nav1.nav-tabs1 .item li.active a {
            background-color:#fff;
            color: #000;
        }
        .nav1.nav-tabs1 button{
            font-size:30px !important;
        }
        .nav1.nav-tabs1 button.owl-prev {
            position: absolute;
            left: 0px;
            /* top: 37%; */
            top:16%;
            transform: translateY(-50%);
            background-color: white !important;
            width: 20px;
            height:40px;
            border-radius: 100% !important;
            color: #000 !important;
            margin:0;
            outline:0;
        }

        button.owl-next {
            position: absolute;
            right: 0px;
            /* top: 37%; */
            top:16%;
            transform: translateY(-50%);
            background-color: white!important;
            width: 20px;
            height:40px;
            border-radius: 100%!important;
            color: #000!important;
            margin:0;
            outline:0;
        }


        .owl-dots {
            display: none;
        }


        .item {
            display: flex;
            justify-content: center; 
        }
        .item  li a {
            display: flex;
            align-items: center;
            justify-content: center;
            color: #000;
            /* text-transform: uppercase; */
            flex-wrap: wrap;
            text-align: center;
            font-size: 20px; 
            line-height:50px;
            text-decoration: none;
            font-weight:400;
        }
        .tab-content,
        .tab-content .panel-body {
            padding:0;
        }
        .tab-content .panel-body .container{
            padding:0;
            width:100%;  
        }
         .tab-content #home1 .panel-body .container a{
            color:#42bd41;
        }
        .tab-content #home .panel-body .activity_holder{
            width:100%;
            float: none;
        }
        .tab-content #home .panel-body .activity_holder input{
            margin-right:inherit;
        }

        .tab-content #home .upload_att{
            float:left;
        }
        .tab-content #home textarea{
            resize:none;
        }
        .nav1.nav-tabs1{
            padding: 0;
            margin: 0;
            list-style: none;
        }
        /* .nav1.nav-tabs1 .owl-stage-outer{
            border-bottom: 1px solid #ccc;
            padding-bottom:20px;
        } */
        .nav1.nav-tabs1 .owl-stage-outer .owl-stage{
            border-bottom: 1px solid rgb(204, 204, 204);
            min-width: 100%;
            padding-bottom: 20px;
        }
        .nav1.nav-tabs1 .item li.active a{
            position: relative;
        }
        .nav1.nav-tabs1 .item li.active a:after {
            content: "";
            display: block;
            color: #0084ff;
            font-size: 16px;
            position: absolute;
            bottom: -38px;
            transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            /* width: 0; 
            height: 0; 
            border-left: 20px solid transparent;
            border-right: 20px solid transparent;
            border-bottom: 20px solid #ccc; */

            box-sizing: border-box;
            height:2vw;
            width:2vw;
            border-style: solid;
            border-color: #ccc;
            /* border-width: 0px 1px 1px 0px; */
            transform: rotate(45deg);
            transition: border-width 150ms ease-in-out;

            border-color: #ccc #fff #fff #ccc;
            border-width: 1px 0px 0px 1px;
            z-index: 9999;
            background: #fff;
        }

        .typescript .table{
            border: 1px solid #cdcdcd;
        }

    </style>

        <ul class="nav1 nav-tabs1">
            <div class="owl_1 owl-carousel owl-theme">
                <div class="item">
                    <li class="active"><a data-toggle="tab" id="activityTab" href="#home1">Opportunity</a></li>
                </div>
                <div class="item">
                    <li><a data-toggle="tab" id="transcriptTab" href="#menu11">Details</a></li>
                </div>
                <div class="item">
                    <li><a data-toggle="tab" href="#menu22">Groups</a></li>
                </div>
                <div class="item">
                    <li><a data-toggle="tab" href="#menu33">Affiliated Groups</a></li>
                </div>
                 <div class="item">
                    <li><a data-toggle="tab" href="#menu44">Delegates</a></li>
                </div>
                 <div class="item">
                    <li><a data-toggle="tab" href="#menu55">Sub Organization</a></li>
                </div>
                <div class="item">
                    <li><a data-toggle="tab" href="#menu66">Partners</a></li>
                </div>
                
                
            </div>
        </ul>
        <div class="tab-content">
            <div id="home1" class="tab-pane fade in active">
                 <div class="panel-body">
                    @include('components.profile.activity-tab')
                </div>
            </div>

            <div id="menu11" class="tab-pane fade">
               <div class="panel-body">
                    @include('components.profile.detail-tab')
                    @if($profile_info['is_my_profile'] == 1)
                        @include('components.profile.account-tab')
                    @endif
                </div>
            </div>

           
            <div id="menu22" class="tab-pane fade">
               
                    <div class="panel-body">
                        <div class="row">

                            @if($groupCount > 0)
                            @if($profile_info['is_volunteer']==0 && $authUser)
                            <div class="col-sm-12 mb-12 wrapper-link" style="margin:0px !important;">
                                <div style="margin:10px !important;">
                                    <a id="export_member" href="{{url('/organization/group-members') }}"><span>Export Members</span></a>
                                </div>
                            </div>
                            @endif
                            @foreach($group as $groupSingle)
                            <div class="col-sm-4 mb-2">
                                <a href="{{$groupSingle->is_share_able || $groupSingle->is_public ==1 ? route('share.group' ,base64_encode($groupSingle->id)) : '#'}}">
                                    <div class="card" style="background-color: #edf2f3">
                                        <div class="card-body">
                                            <div class="main-text">
                                                <p class="name">
                                                    {{str_limit($groupSingle->name ,30)}}
                                                    @if($groupSingle->is_public ==1)
                                                    <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                    @else
                                                    <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                    @endif
                                                </p>
                                                @if(isset($groupSingle->org_name))
                                                <p><span style="font-size:13px">{{$groupSingle->org_name}}</span></p>
                                                @endif
                                                @if(isset($groupSingle->categoryName))
                                                <p>{{"Category : ".$groupSingle->categoryName}}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                            @else
                            <div class="col-sm-12 mb-12">
                                You do not have active groups yet
                            </div>
                            @endif

                        </div>
                    </div>
               
            </div>
            
            <div id="menu33" class="tab-pane fade">
               <div class="panel-body">
                    <div class="row">
                        @if(count($affiliatedGroups))
                        @foreach($affiliatedGroups as $affiliatedGroupSingle)
                        <div class="col-sm-4 mb-2">
                            <a href="{{$affiliatedGroupSingle->is_share_able || $affiliatedGroupSingle->is_public==1 ? route('share.group' ,base64_encode($affiliatedGroupSingle->id)) : '#'}}">
                                <div class="card" style="background-color: #edf2f3">
                                    <div class="card-body">
                                        <div class="main-text">
                                            <p class="name">
                                                {{str_limit($affiliatedGroupSingle->name ,30)}}
                                                @if($affiliatedGroupSingle->is_public==1)
                                                <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                @else
                                                <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                @endif
                                            </p>
                                            @if($affiliatedGroupSingle->org_name)
                                            <p><span style="font-size:13px">{{$affiliatedGroupSingle->org_name}}</span></p>
                                            @endif
                                            @if(isset($affiliatedGroupSingle->categoryName))
                                            <p>{{ "Category : ".$affiliatedGroupSingle->categoryName}}</p>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        @else
                        <div class="col-sm-12 mb-12">
                            There are no affiliated groups
                        </div>
                        @endif

                    </div>
                </div>
            </div>
            <div id="menu44" class="tab-pane fade">
                 <div class="panel-body">
                    @include('components.profile.delegates')
                </div>
            </div>
            <div id="menu55" class="tab-pane fade">
                <div class="panel-body">
                    @include('components.profile.sub-organization')
                </div>
            </div>

            <div id="menu66" class="tab-pane fade">
                <div class="panel-body">
                    @include('components.profile.partner-tab')
                </div>
            </div>

        </div>  



   
    {{-- <article class="panel-group bs-accordion" id="accordion" role="tablist" aria-multiselectable="true">
         
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="activityLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#activity" aria-expanded="false" aria-controls="activity">
                        Opportunity
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="activity" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.activity-tab')
                </div>
            </div>
        </section>
       
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="detailsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#details" aria-expanded="false" aria-controls="details">
                        {{$user->user_role === 'organization' ? 'Details' : 'Info' }}
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="details" class="panel-collapse collapse" role="tabpanel" aria-labelledby="details">
                <div class="panel-body">
                    @include('components.profile.detail-tab')
                    @if($profile_info['is_my_profile'] == 1)
                        <hr>
                        @include('components.profile.account-tab')
                    @endif
                </div>
            </div>
        </section>
        
      
        
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="groupsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#groups" aria-expanded="false" aria-controls="groups">
                        Groups
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="groups" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <div class="row">

                        @if($groupCount > 0)
                        @if($profile_info['is_volunteer']==0 && $authUser)
                        <div class="col-sm-12 mb-12 wrapper-link" style="margin:0px !important;">
                            <div style="margin:10px !important;">
                                <a id="export_member" href="{{url('/organization/group-members') }}"><span>Export Members</span></a>
                            </div>
                        </div>
                        @endif
                        @foreach($group as $groupSingle)
                        <div class="col-sm-4 mb-2">
                            <a href="{{$groupSingle->is_share_able || $groupSingle->is_public ==1 ? route('share.group' ,base64_encode($groupSingle->id)) : '#'}}">
                                <div class="card" style="background-color: #edf2f3">
                                    <div class="card-body">
                                        <div class="main-text">
                                            <p class="name">
                                                {{str_limit($groupSingle->name ,30)}}
                                                @if($groupSingle->is_public ==1)
                                                <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                @else
                                                <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                @endif
                                            </p>
                                            @if(isset($groupSingle->org_name))
                                            <p><span style="font-size:13px">{{$groupSingle->org_name}}</span></p>
                                            @endif
                                            @if(isset($groupSingle->categoryName))
                                            <p>{{"Category : ".$groupSingle->categoryName}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        @else
                        <div class="col-sm-12 mb-12">
                            You do not have active groups yet
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </section>
        
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="affiliatedGroupsLbl" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#affiliatedGroups" aria-expanded="false" aria-controls="affiliatedGroups">
                        Affiliated Groups
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="affiliatedGroups" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <div class="row">

                        @if(count($affiliatedGroups))
                        @foreach($affiliatedGroups as $affiliatedGroupSingle)
                        <div class="col-sm-4 mb-2">
                            <a href="{{$affiliatedGroupSingle->is_share_able || $affiliatedGroupSingle->is_public==1 ? route('share.group' ,base64_encode($affiliatedGroupSingle->id)) : '#'}}">
                                <div class="card" style="background-color: #edf2f3">
                                    <div class="card-body">
                                        <div class="main-text">
                                            <p class="name">
                                                {{str_limit($affiliatedGroupSingle->name ,30)}}
                                                @if($affiliatedGroupSingle->is_public==1)
                                                <span title="Public Group" class="badge badge-success pull-right"><small><i class="fa fa-unlock"></i></small></span>
                                                @else
                                                <span title="Private Group" class="badge badge-danger pull-right"><small><i class="fa fa-lock"></i></small></span>
                                                @endif
                                            </p>
                                            @if($affiliatedGroupSingle->org_name)
                                            <p><span style="font-size:13px">{{$affiliatedGroupSingle->org_name}}</span></p>
                                            @endif
                                            @if(isset($affiliatedGroupSingle->categoryName))
                                            <p>{{ "Category : ".$affiliatedGroupSingle->categoryName}}</p>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        @else
                        <div class="col-sm-12 mb-12">
                            There are no affiliated groups
                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </section>

        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="orgDelegatesTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#orgDelegates" aria-expanded="false" aria-controls="orgDelegates">
                        Delegates
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>

            <div id="orgDelegates" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.delegates')
                </div>
            </div>
        </section>

       
        
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="subOrganizationTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#subOrganization" aria-expanded="false" aria-controls="subOrganization">
                        Sub Organization
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="subOrganization" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.sub-organization')
                </div>
            </div>
        </section>
        
       
        
        <section class="panel panel-default">
            <div class="panel-heading" role="tab" >
                <h4 class="panel-title">
                    <a id="orgPartnerTab" class="nav-link collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#orgPartner" aria-expanded="false" aria-controls="orgPartner">
                        Partners
                        <span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span>
                    </a>
                </h4>
            </div>
            <div id="orgPartner" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    @include('components.profile.partner-tab')
                </div>
            </div>
        </section>
        
       
    </article>
</main> --}}
