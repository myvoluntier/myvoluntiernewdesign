@extends('layout.frame')

@section('css')
    <style>
        .org_logo{
            height: 120px;
            width: 120px;
            float: left;
        }

        .org_info{
            padding-left: 15px;
            padding-top: 15px;
            display: inline-block;
            clear: both;
        }

        .org_info image{
            display: inline-block;
        }

        .org_info label{
            display: block;
        }

        .org_info_addr{
            margin-left: 15px;
            padding: 15px;
            float: right;
            clear: right;
        }
        .org_info_addr label{
            display: block;
        }
    </style>
@endsection

@section('body')
    <div class="row-content">
    <div class="wrapper-profile-box">

        <div class="top-profile-info">
            <form id="upload_logo" role="form" method="post" action="{{url('api/organization/profile/upload_logo')}}"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" accept="image/*" name="file_logo" id="upload_logo_input" hidden="true">
            </form>

            <form id="upload_logo1" role="form" method="post"
                  action="{{url('api/organization/profile/upload_back_img')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="file" accept="image/*" name="file_logo" id="upload_logo1_input" hidden="true">
            </form>

            <div class="img-box"
                 style="background-image:url('{{$user->back_img == NULL ? asset('front-end/img/bg/0002.png') :  $user->back_img}} ')"></div>


            <div class="container mobile-container">
                <div class="avatar"
                     style="background-image:url('{{$user->logo_img == NULL ? asset('front-end/img/org/001.png') : $user->logo_img}}')">
                    <span></span></div>


                <div class="link-img">

                </div>

            </div>

            <div class="container">
                <div class="row txt-box">

                    <div class="col-12 col-md-6">
                        <div class="main-text"><p class="h2">{{$user->user_role == 'organization' ? $user->org_name : $user->first_name ." ". $user->last_name}}</p></div>
                    </div>

                    <div class="col-12 col-md-6">
                        <div class="main-text">

                            <ul class="list-column clearfix">
                                <li>
                                    @if($user_role == 'organization')
                                        <p class="h2">{{count($members)}}</p>
                                        <p class="light">Members</p>
                                    @else
                                        <p class="h2">{{$friend->count()}}</p>
                                        <p class="light">Friends</p>
                                    @endif
                                </li>
                                <li>
                                    <p class="h2">{{$group->count()}}</p>
                                    <p class="light">Groups</p>
                                </li>
                                <li>
                                    @if($user_role == 'organization')
                                        <p class="h2">{{$active_oppr->count()}}</p>
                                        <p class="light">Opportunities</p>
                                    @else
                                        <p class="h2">{{$opportunity->count()}}</p>
                                        <p class="light">Opportunities</p>
                                    @endif

                                </li>
                            </ul>

                        </div>
                    </div>

                </div>
            </div>

        </div>

        <!-- -->

        <div class="text-profile-info">

            <div class="container">
                <ul class="nav nav-tabs" role="tablist">

                    <li class="nav-item">
                        <a class="nav-link active show" href="#info" role="tab"
                           data-toggle="tab"><span>Info</span></a>
                    </li>
                    {{--@if($user->user_role === 'volunteer')--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="#transcript" role="tab"--}}
                           {{--data-toggle="tab"><span>Transcript</span></a>--}}
                    {{--</li>--}}
                    {{--@endif--}}

                </ul>


                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active show" id="info">


                        @if($user->user_role === 'organization')
                            <div class="row">
                                <div class="col-12 col-md-8">

                                    <div class="main-text">
                                        <p class="title">Personal Info</p>
                                        <p class="bold">Organization ID:</p>
                                        <p class="user_name_detail">{{$user->user_name}}</p>
                                        <p class="bold">Organization Name:</p>
                                        <p class="org_name_detail">{{$user->org_name}}</p>
                                        <p class="bold">Contact Email:</p>
                                        <p class="user_email_detail">{{$user->email}}</p>
                                        <p class="bold">Phone Number:</p>
                                        <p class="user_phone_detail">{{$user->contact_number}}</p>
                                        <p class="bold">{{$user->type_name}}</p>
                                        <div class="textarea_box user_education_detail">
                                            {!!html_entity_decode(strip_tags($user->brif))!!}
                                        </div>

                                    </div>

                                </div>
                                <div class="col-12 col-md-4">

                                    <div class="dashboard_org">
                                        <div class="track-it-time_slider">
                                            <div class="slider">
                                                <div>

                                                    <div class="slider-wrapper">
                                                        <div>
                                                            <div class="slider-item-scale">

                                                                <div>
                                                                    <span>IMPACTS</span>
                                                                    @if($user_role == 'organization')
                                                                        <span>{{$tracks_hours}}</span>
                                                                    @else
                                                                        <span>{{$logged_hours}}</span>
                                                                    @endif

                                                                    <span>HOURS</span>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="details_content">
                                        <div class="map-box" id="pos_map">
                                            <input type="hidden" id="lat_val" value="{{$user->lat}}">
                                            <input type="hidden" id="lng_val" value="{{$user->lng}}">
                                        </div>
                                        <div class="main-text">
                                            <p class="bold">Primary Location:</p>
                                            <p>{{implode(', ', array_filter([$user->city, $user->state, $user->country, $user->zipcode]))}}</p>

                                            @if($user->website)

                                                <p class="bold">WebSite:</p>
                                                <p>{{$user->website}}</p>

                                            @endif


                                            <p class="bold">Organization Type:</p>
                                            <p>{{$user->type_name}}</p>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        @else

                            <div class="row">
                                <div class="col-12 order-0 col-md-8 order-md-0">

                                    <div class="main-text">
                                        <p class="title">Personal Info</p>

                                        <p class="bold">User Name:</p>
                                        <p>{{$user->user_name}}</p>

                                        <p class="bold">Contact Email:</p>
                                        <p>{{$user->email}}</p>


                                        <?php
                                        $dob = $user->birth_date;
                                        $diff = (date('Y') - date('Y', strtotime($dob)));
                                        ?>

                                        @if($user->show_age == 'Y' && $user->user_role == 'volunteer')

                                            <p class="bold">Age:</p>

                                            <p>{{$diff}}</p>

                                        @elseif($user->show_age == 'N' && $user->user_role == 'volunteer')


                                        @endif
                                        <p class="bold">Summary</p>
                                    <p>{!!html_entity_decode(strip_tags($user->brif))!!}</p>

                                    </div>

                                </div>

                                <div class="col-12 order-2 col-md-4 order-md-1">


                                    <div class="dashboard_org">
                                        <!--
                                        <div class="track-it-time_slider">
                                            <div class="slider">
                                                <div>

                                                    <div class="slider-wrapper">
                                                    <div>
                                                        <div class="slider-item-scale">

                                                            <div>
                                                                <span>IMPACTS</span>
                                                                <span>37</span>
                                                                <span>HOURS</span>
                                                                <a href="#"><span>Add hours</span></a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        -->

                                        <div class="wrapper_green_bg">
                                            <div class="main-text">
                                                <p>IMPACTS</p>

                                                @if($user_role == 'organization')
                                                    <p class="bold">{{$tracks_hours}}</p>
                                                @else
                                                    <p class="bold">{{$logged_hours}}</p>
                                                @endif

                                                <p class="light">HOUR(S)</p>
                                            </div>

                                        </div>
                                        <div class="details_content">
                                            <div class="map-box" id="pos_map">
                                                <input type="hidden" id="lat_val" value="{{$user->lat}}">
                                                <input type="hidden" id="lng_val" value="{{$user->lng}}">
                                            </div>
                                            <div class="main-text">
                                                <p class="bold">Primary Location:</p>
                                                <p>{{implode(', ', array_filter([$user->city, $user->state, $user->country, $user->zipcode]))}}</p>

                                                @if($user->website)

                                                    <p class="bold">WebSite:</p>
                                                    <p>{{$user->website}}</p>

                                                @endif

                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        @endif


                    </div>

                    @if($user->user_role === 'volunteer')
                        <div role="tabpanel" class="tab-pane fade in active show" id="transcript">
                            <div class="main-text" style="padding-top: 20px;">
                                <p class="title">Transcript</p>
                            </div>
                            <div id="filter_content">
                                @foreach($tracks as $key=>$t)
                                    <div class="typescript" style="padding: 15px;">
                                        <div id="">
                                            <img src="{{$t['org_logo'] == NULL ? asset('front-end/img/org/001.png') : $t['org_logo']}}" class="org_logo">
                                            <div class="org_info">
                                                <Label><strong>{{$t['org_name']}}</strong></Label>
                                                <Label>{{$t['contact_number']}}</Label>
                                                <Label>{{$t['email']}}</Label>
                                            </div>
                                            <div class="org_info_addr">
                                                <Label>{{$t['address']}}</Label>
                                            </div>
                                        </div>
                                        <div style="overflow: auto; clear: both;">
                                            <table id="example21" class="table sortable member-table">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div class="main-text"><p>Date/Time</p></div>
                                                    </th>
                                                    <th>
                                                        <div class="main-text"><p>Duration(hrs)</p></div>
                                                    </th>
                                                    <th>
                                                        <div class="main-text"><p>Opportunity Title</p></div>
                                                    </th>
                                                    <th>
                                                        <div class="main-text"><p>Category/Type</p></div>
                                                    </th>
                                                    <th>
                                                        <div class="main-text"><p>Date Validated</p></div>
                                                    </th>
                                                    <th>
                                                        <div class="main-text"><p>Validator Name</p></div>
                                                    </th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($t['track_info'] as $opp)
                                                    <tr>
                                                        <td>{{$opp['logged_date']}}</td>
                                                        <td>{{$opp['logged_mins']/60}}</td>
                                                        <td>{{$opp['oppor_name']}}</td>
                                                        <td>{{$oppr_types[$opp['category_type']]['name']}}</td>
                                                        <td>{{$opp['confirmed_at']}}</td>
                                                        <td>{{$opp['confirmer_name']}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    @endif

                </div>

            </div>

        </div>

    </div>
    </div>

    <script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es"></script>
    <script>

        var latLng = new google.maps.LatLng(parseFloat('{{$user->lat}}'), parseFloat('{{$user->lng}}'));
        var myOptions = {
            zoom: 16,
            center: latLng,
            styles: [{
                "featureType": "water",
                "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]
            }, {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{"color": "#808080"}, {"lightness": 54}]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ece2d9"}]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [{"color": "#ccdca1"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{"color": "#767676"}]
            }, {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{"color": "#ffffff"}]
            }, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]
            }, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.sports_complex",
                "stylers": [{"visibility": "on"}]
            }, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {
                "featureType": "poi.business",
                "stylers": [{"visibility": "simplified"}]
            }],
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("pos_map"), myOptions);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon: '{{asset('front-end/img/pin.png')}}'
        });
    </script>

@endsection


