@extends('layout.frame')
@section('css')
<style>
    .p_invalid, .po_invalid {
        display: none;
        color: red;
        text-align: center;
        font-size: 10px;
    }

    
    .select2-container--below{background: yellow};
</style>
@yield('css')
@endsection
@section('body')
<body>
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5426731.js"></script>

<div class="offcanvas-menu">
        <div class="container">
            <div class="header-center-menu">
                <ul class="nav">
                    @include('components.non-auth.buttons_registration_and_login')
                </ul>
            </div>
        </div>
</div>


<div class="offcanvas-contentarea">
$("#org_name_label").text("School Name");
    $("#org_div").show();
<div class="wrapper_bottom_footer">
    @include('components.non-auth.header')

        <div class="row-content">
        <input class="type-user" type="hidden" value="Volunteer">

        @if(request()->confirmed)
            <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
                <strong>Hi, </strong> Your account has been confirmed. Please login to continue.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        @endif

   <div class="sign-wrapper">
    <div class="sign-container">
          @if(isset($showMsg))
                            <div class="text">
                            We sent your Link via email. Please check your email!
                        </div>
          @else
          <h2>SIGN UP</h2>
         <h6>Welcome to MyVolun<span>tier</span>!</h6>
         <div class="form-group">  
              <label class="label-text">Thanks for your registration</label>
                            <div class="text">
                                Please check your email to confirm your registration. If you do not see an email, please check your spam/junk folder for an email from support@myvoluntier.com. Add support@myvoluntier.com to your list of email contacts to ensure all future notifications are received.
                            </div>
         </div>
          @endif
        
         <div class="form-group">
            <div class="wrapper-link two-link login-wrap-link">
            Go to login page&nbsp;&nbsp;<p><a href="{{route('signIn')}}">Click here!</a></p>
                </div>
            </div>
     </div>
  </div>
        @yield('content')
    </div>
    @include('components.footer')
</div>
</div>



<script src="{{asset('front-end/js/jquery-3.3.1.slim.js')}}"></script>
    <script src="{{asset('js/jquery-3.3.1.js')}}"></script>
    <script src="{{asset('front-end/js/popper.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap.js')}}"></script>
    <script src="{{asset('js/global.js')}}"></script>
    <script src="{{asset('js/check_validate.js')}}"></script>
    <script src="{{asset('js/home-action.js')}}"></script>
    <script src="{{asset('front-end/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('front-end/js/select2.full.js')}}"></script>
    <script src="{{asset('js/inputmask/jQuery.SimpleMask.min.js')}}"></script>
    <script>
$('select#org_type').on('change', function() {
   if(this.value==1){
    $("#org_name_label").text("School Name");
    $("#org_div").show();
    $(".org_type_div").hide();
    $(".schoolType").show();
    $(".ein_div").hide();
   
   } 
   else if(this.value==2){
    $("#org_name_label").text("Organization Name");
    $("#org_div").show();
    $(".org_type_div").hide();
    $(".schoolType").hide();
    $(".ein_div").show();
   }
   else if(this.value==3){
    $("#org_name_label").text("School Name");
    $("#org_div").show();
    $(".org_type_div").hide();
    $(".schoolType").hide();
     $(".ein_div").hide();
   }
   else if(this.value==4){
    $("#org_name_label").text("School Name");
    $("#org_div").show();
    $(".org_type_div").show();
    $(".schoolType").hide();
    $(".ein_div").hide();
   }
});
$("#organization").click(function() {
        $(".organizationType").show();
        $(".org_pass").show();
        $(".vol_pass").hide();  
        $(".firstName").hide();
        $(".lastName").hide();
        $(".middleName").hide();
        $(".vol_gender").hide();
        $(".birthDate").hide();
        $(".alterEmail2").hide();
        $(".alterEmail3").hide();
        $(".foundYear").show();
        // $(".schoolType").show();
        // $("#org_div").show(); 
        $("#org_contact").show();   
        $("#vol_contact").hide(); 
        $("#vol_mail").hide();
        $("#org_mail").show();
        $("#vol_checkbox").hide();
        $("#org_checkbox").show();
        
});
$("#volunteer").click(function() {
        $(".organizationType").hide();
        $(".vol_pass").show();
        $(".org_pass").hide();
        $(".firstName").show();
        $(".lastName").show();
        $(".middleName").show();
        $(".vol_gender").show();
        $(".birthDate").show();
        $(".alterEmail2").show();
        $(".alterEmail3").show();
        $(".schoolType").hide();
        $(".foundYear").hide();
        $("#org_div").hide();
        $("#org_contact").hide();
        $("#vol_contact").show();
        $("#vol_mail").show();
        $("#org_mail").hide();
        $("#vol_checkbox").show();
        $("#org_checkbox").hide();
    });

$(document).ready(function () {
if ($("input[name='loginAS']:checked").val()=="organization") {
    $(".organizationType").show();
    $(".org_pass").show();
    $(".vol_pass").hide();
    $(".firstName").hide();
    $(".lastName").hide();
    $(".middleName").hide();
    $(".vol_gender").hide();
    $(".birthDate").hide();
    $(".alterEmail2").hide();
    $(".alterEmail3").hide();
    $(".foundYear").show();
    $("#org_contact").show();
    $("#vol_contact").hide();
    $("#vol_mail").hide();
    $("#org_mail").show();
    $("#vol_checkbox").hide();
    $("#org_checkbox").show();

    if($("select#org_type").val()==1)
    {  
        $("#org_name_label").text("School Name");
        $("#org_div").show();
        $(".org_type_div").hide();
        $(".schoolType").show();
        $(".ein_div").hide();
    }
    else if($("select#org_type").val()==2){ 
        $("#org_name_label").text("Organization Name");
        $("#org_div").show();
        $(".org_type_div").hide();
        $(".schoolType").hide();
        $(".ein_div").show();
    }
    else if($("select#org_type").val()==3){
        $("#org_name_label").text("School Name");
        $("#org_div").show();
        $(".org_type_div").hide();
        $(".schoolType").hide();
        $(".ein_div").hide();
    }
    else if($("select#org_type").val()==4){

        $("#org_name_label").text("School Name");
        $("#org_div").show();
        $(".org_type_div").show();
        $(".schoolType").hide();
        $(".ein_div").hide();
    }
}else{

    $(".organizationType").hide();
    $(".vol_pass").show();
    $(".org_pass").hide();
    $(".firstName").show();
    $(".lastName").show();
    $(".middleName").show();
    $(".vol_gender").show();
    $(".birthDate").show();
    $(".alterEmail2").show();
    $(".alterEmail3").show();
    $(".schoolType").hide();
    $(".foundYear").hide();
    $("#org_div").hide();
    $("#org_contact").hide();
    $("#vol_contact").show();
    $("#vol_mail").show();
    $("#org_mail").hide();
    $("#vol_checkbox").show();
    $("#org_checkbox").hide();
}


$('.phoneUSMask').simpleMask({
    'mask': ['###-###-####']
});
$('.userName').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

$('.Founded_year_format').datepicker({
    autoclose: true,
    format: 'yyyy',
    minViewMode: 2
});
var $outerwidth = $('.row-header header .outer-width-box');
var $innerwidth = $('.row-header header .inner-width-box');

function checkWidth() {

    var outersize = $outerwidth.width();
    var innersize = $innerwidth.width();
    if (innersize > outersize) {
        $('body').addClass("navmobile");
    } else {
        $('body').removeClass("navmobile");
        $('body').removeClass("offcanvas-menu-show");
    }
}

checkWidth();
$(window).resize(checkWidth);
$('.offcanvas-menu-backdrop').on('click', function (e) {
    $('body').toggleClass("offcanvas-menu-show");
    e.preventDefault();
});
$('.wrapper_bottom_footer > .row-header-mobile header a.navtoggler').on('click', function (e) {
    $('body').toggleClass("offcanvas-menu-show");
    e.preventDefault();
});

function doResize($wrapper, $el) {

    var scale = Math.min(
        $wrapper.outerWidth() / $el.outerWidth(),
        $wrapper.outerHeight() / $el.outerHeight()
    );
    if (scale < 1) {

        $el.css({
            '-webkit-transform': 'scale(' + scale + ')',
            '-moz-transform': 'scale(' + scale + ')',
            '-ms-transform': 'scale(' + scale + ')',
            '-o-transform': 'scale(' + scale + ')',
            'transform': 'scale(' + scale + ')'
        });
    } else {
        $el.removeAttr("style");
    }
}

doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
$(window).resize(function () {
    doResize($(".wrapper_home_slider .text-over-slider"), $(".wrapper_home_slider .content-to-scale"));
});
$('body').on('click', '.close_open', function () {
    $('#myModalSuccessfulReg').modal('hide');
});
$('body').on('change', '.type-user-select', function () {
    $('.type-user').val($('.type-user-select').val())
});
$('body').on('click', '.login_button_class', function () {
    $('.close').click();
    setTimeout(function () {
        $('#myModalLogin').modal('show');
        $('#login_user').val('');
        setTimeout(function () {
            $('body').addClass('modal-open');
            $('body').removeClass('offcanvas-menu-show')

        }, 500);
    }, 200);
});
$('body').on('click', '.termsAndConditions', function (e) {
    e.preventDefault();
    $('#type_tab').val($(this).data('type'));
    $('#myModalSingOrgRegistration').modal('hide');
    $('#myModalSingVolRegistration').modal('hide');
    setTimeout(function () {
        $('#myModalTermsAndConditions').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});

$('body').on('click', '.privacyPolicy', function (e) {
    e.preventDefault();
    $('#type_tab').val($(this).data('type'));
    $('#myModalSingOrgRegistration').modal('hide');
    $('#myModalSingVolRegistration').modal('hide');
    $('#myModalTermsAndConditions').modal('hide');
    setTimeout(function () {
        $('#myModalprivacyPolicy').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});

$('#btn_agree').on('click', function () {
    $('#o_accept_terms').prop('checked', true);
    $('#v_accept_terms').prop('checked', true);
    $('#v_terms_alert').hide();
    $('#ov_terms_alert').hide();
    $('#myModalTermsAndConditions').modal('hide');
    setTimeout(function () {
        if ($('#type_tab').val() == 'org') {
            $('#myModalSingOrgRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
        else {
            $('#myModalSingVolRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
    }, 200);
});
$('#btn_decline').on('click', function () {
    $('#v_terms_alert').hide();
    $('#ov_terms_alert').hide();
    $('#myModalTermsAndConditions').modal('hide');
    setTimeout(function () {
        if ($('#type_tab').val() == 'org') {
            $('#myModalSingOrgRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
        else {
            $('#myModalSingVolRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
    }, 200);
});
$('#btn_agree1').on('click', function () {
    $('#op_accept_terms').prop('checked', true);
    $('#p_accept_policy').prop('checked', true);
    $('#ov_policy_alert').hide();
    $('#v_policy_alert').hide();
    $('#myModalprivacyPolicy').modal('hide');
    setTimeout(function () {
        if ($('#type_tab').val() == 'org') {
            $('#myModalSingOrgRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
        else {
            $('#myModalSingVolRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
    }, 200);
});
$('#btn_decline1').on('click', function () {
    $('#ov_policy_alert').hide();
    $('#v_policy_alert').hide();
    $('#myModalprivacyPolicy').modal('hide');
    setTimeout(function () {
        if ($('#type_tab').val() == 'org') {
            $('#myModalSingOrgRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
        else {
            $('#myModalSingVolRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show');
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
    }, 200);
});
@if(!Auth::check())
$(document).on('click', '.vol_registr', function (e) {
    e.preventDefault();
    scroll(0, 0);
    setTimeout(function () {
        var text = 'Volunteer';
        $('.type-user').val(text);
        $('#myModalSingVolRegistration').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200)
});
$(document).on('click', '.org_registr', function (e) {
    scroll(0, 0);
    setTimeout(function () {
        e.preventDefault();
        var text = 'Organization';
        $('.type-user').val(text);
        $('#myModalOrgType').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200)
});
$(document).on('click', '.inst_registr', function (e) {
    e.preventDefault();
    scroll(0, 0);
    var text = 'Organization';
    $('.type-user').val(text);
    $('#org_type').val(1);
    $('#chooseTypeOrg').click();
    setTimeout(function () {

    }, 200);
});
@endif


$('body').on('click', '#chooseTypeOrg', function (e) {
    e.preventDefault();
    var typeOrg = $('#org_type').val();
    if (typeOrg == 1) {

        $('.ein_div').hide();
        $('.org_name_label').text('School Name:')
        $('.org_type_div').hide()
        $('.non-org-type').hide();
        $('.school_type').show();
    }

    else if (typeOrg == 2) {
        $('.org_type_div').hide();
        $('.ein_div').show();
        if($('#type-user-select-id').val() == 'SubOrganization'){
            $('.org_name_label').text('Sub Organization Name:');
        }else{
            $('.org_name_label').text('Organization Name:');
        }
        $('.school_type').hide();
        $('.non-org-type').show();
    }

    else if (typeOrg == 3) {
        $('.org_type_div').hide();
        $('.ein_div').hide();
        $('.school_type').hide();
        $('.non-org-type').hide();
    }

    else {
        $('.org_type_div').show();
        $('.ein_div').hide();
        $('.school_type').hide();
        $('.non-org-type').hide();
    }

    $('#myModalOrgType').modal('hide');
    setTimeout(function () {
        $('#myModalSingOrgRegistration').modal('show');
        $('body').removeClass('offcanvas-menu-show')
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});
$('body').on('click', '#login_button', function () {
    $('.close').click();
    $('#myModalLogin').modal('show');
    $('body').removeClass('offcanvas-menu-show');
    setTimeout(function () {
        $('body').addClass('modal-open');
    }, 500);
});


$('body').on('click', '#forPass', function (e) {
    e.preventDefault();
    $('#myModalLogin').modal('hide');
    setTimeout(function () {
        $('#myModalForgotPassword').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});
$('body').on('click', '.registration_button', function () {
    $('#myModalUserTipe').modal('show');
    $('body').removeClass('offcanvas-menu-show');
    setTimeout(function () {
        $('body').addClass('modal-open');
    }, 500);
});

$('body').on('click', '.registration_button_on_modal_form', function (e) {
    e.preventDefault();
    $('#myModalLogin').modal('hide');
    setTimeout(function () {
        $('#myModalUserTipe').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);
    }, 200);
});
$("select").select2({
    theme: "bootstrap",
    minimumResultsForSearch: -1
});
$('body').on('click', '#chooseType', function (e) {
    e.preventDefault();
   
    $('#orgSelectError').css('display','none');
    var typeUser = $('.type-user').val();
    if (typeUser === 'Volunteer') {
        $('#myModalUserTipe').modal('hide');
        setTimeout(function () {
            $('#myModalSingVolRegistration').modal('show');
            $('body').removeClass('offcanvas-menu-show')
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }, 200);
    }
    else if (typeUser === 'Organization') {

        $('#myModalUserTipe').modal('hide');
        setTimeout(function () {
            $('#myModalOrgType').modal('show');
            $('body').removeClass('offcanvas-menu-show')
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
            // $('#myModalSingOrgRegistration').modal('show');
        }, 200);
    }else if (typeUser === 'SubOrganization') {
        $('#myModalUserTipe').modal('hide');
            setTimeout(function () {
                $('#myModalOrgType').modal('show');
                $('body').removeClass('offcanvas-menu-show')
                setTimeout(function () {
                    $('body').addClass('modal-open');
                }, 500);
                // $('#myModalSingOrgRegistration').modal('show');
            }, 200);
        
    }
});
$('body').on('click', '.previous', function () {
    $('#myModalSingVolRegistration').modal('hide');
    $('#myModalSingOrgRegistration').modal('hide');
    setTimeout(function () {
        if ($('.type-user').val() == 'Volunteer') {
            $('#myModalUserTipe').modal('show');
            $('body').removeClass('offcanvas-menu-show')
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
        else {
            $('#myModalOrgType').modal('show');
            $('body').removeClass('offcanvas-menu-show')
            setTimeout(function () {
                $('body').addClass('modal-open');
            }, 500);
        }
    }, 200);
});
$('.wrapper_input.fa-icons i').click(function () {
    $('.wrapper_input.fa-icons input').datepicker('show');
});

$('.wrapper_input.fa-icons input').datepicker({
    'format': 'mm-dd-yyyy',
    'autoclose': true,
    'orientation': 'right',
    'todayHighlight': true
});
$('body').on('click', '.gender', function () {
    if ($('.gender-radio').val() == 'male') {
        $('.gender-radio').val('female')
    }
    else {
        $('.gender-radio').val('male')
    }
});
$('.terms-conditions-vol').click(function () {

    if ($('.terms-conditions-vol-value').val() == 0) {
        $('.terms-conditions-vol-value').val(1)
    } else {
        $('.terms-conditions-vol-value').val(0)
    }
});
$('.years-old-check').click(function () {
    if ($('.years-old-check-value').val() == 0) {
        $('.years-old-check-value').val(1)
    } else {
        $('.years-old-check-value').val(0)

    }
});
$('.terms-conditions-org').click(function () {
    if ($('.terms-conditions-org-value').val() == 0) {
        $('.terms-conditions-org-value').val(1);
    } else {
        $('.terms-conditions-org-value').val(0);
    }
});
$("#subOrgSel").hide();

$("#organizationSelect").select2({
    theme: "bootstrap",
    minimumInputLength: 2
});

var parentOrgSel = $('#parentOrgSel');
var subOrgSel = $("#subOrgSel");

parentOrgSel.select2({theme: "bootstrap"}).on('change', function () {
    if (parentOrgSel.val()) {
        $('#parent_user_id').val(parentOrgSel.val());
        $("#subOrgSel").show();
        $.ajax({
            url: API_URL + 'get-sub-organization/' + parentOrgSel.val(),
            type: 'GET',
            success: function (data) {
                subOrgSel.empty();
                subOrgSel.append($("<option></option>").attr("value", "").text("Select"));
                $.each(data, function (value, key) {
                    subOrgSel.append($("<option></option>").attr("value", value).text(key)); // name refers to the objects value when you do you ->lists('name', 'id') in laravel
                });
                subOrgSel.select2({
                    theme: "bootstrap",
                    dropdownAutoWidth: true,
                    width: 'auto'
                }).on('change', function () {
                    $('#login_user').val(subOrgSel.val());
                    $('.loginEmail').hide();
                    $('.loginPass').show();
                }); //reload the list and select the first option
            }
        });
    }
}).trigger('change');

$('.loginSub, .parentOrganizationDiv, .organizationDivS').hide();

$('#loginAS').on('change', function () {
    $('#login_user,#login_password,#parent_user_id').val('');

    $('#login_user,#login_password,#parent_user_id').val('');

    $('.organizationDivS').hide();

    if ($(this).val() == "") {
        $('.loginSub, .parentOrganizationDiv').hide();
    } else if ($(this).val() == "suborganization") {
        $('.parentOrganizationDiv').show();
        $('.loginSub').hide();
        $('.loginPass').show();

    } else {
        $('.parentOrganizationDiv').hide();
        $('.loginSub').show();
        $(".select2-container--bootstrap .select2-selection--single", "#myModalLogin").css("border", "");

        if ($(this).val() == "organization")
            $('.organizationDivS').show();
    }
});

$('.parentOrgRego').hide();
$('#type-user-select-id').on('change', function () {
    $('#parent_user_id').val('');
    $('.parentOrgRego').hide();
    if ($(this).val() == "SubOrganization") {
        $('.parentOrgRego').show();
    } else {
        $('.parentOrgRego').hide();
        $(".select2-container--bootstrap .select2-selection--single", "#myModalUserTipe").css("border", "");
    }
});
});

    </script>
@yield('script')
</body>
@endsection