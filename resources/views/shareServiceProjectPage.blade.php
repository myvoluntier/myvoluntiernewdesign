@extends('layout.frame')


@section('css')
<link rel="stylesheet" href="{{asset('front-end/css/service-project.css')}}">
<link rel="stylesheet" href="{{asset('front-end/css/print.css')}}" media="print">
@endsection
@section('body')
<div class="wrapper-create-edit-group">
	
        <div class="container" style="margin-bottom:20px;">
			<div class="outer-width-box">
                <div class="inner-width-box">
                    <div class="row flex-nowrap align-items-center">
                        <div class="col col-auto">
                            <div class="header-logo">
                                <a href="http://dev3.myvoluntier.loc/public">
                                    <img src="http://dev3.myvoluntier.loc/public/front-end/img/logo.jpg" width="226" height="38" alt="">
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    
    <div class="container" id="service_project_box">
        <div class="main-text border-bottom">
            <h2 class="h2">Share Service Project <span class="hoursSpan">Total Hours : {{ $serviceProject->total_hours }}</span></h2>
            <p class="light">You can view a Service Project right now. </p>
        </div>
        @include('components.shareServiceProjectInfo')
       
        <div class="wrapper_row_link">
            <div class="wrapper-link two-link margin-top">
                <a href="javascript:void(0);" name="filter_print" onclick="printServiceProject()" class="btn btn-info" style="float:right; margin-right: 10px;">Print</a>
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;
        </div>
    </div>
</div>
@endsection
 <script>
    function printServiceProject()
    {
        window.print();
    }
</script>

