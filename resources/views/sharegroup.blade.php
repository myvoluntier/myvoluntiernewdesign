@extends('layout.master')

@section('css')
    <style>
        img {
            width: 70px;
        }
        a {
            text-decoration: none !important;
        }
        .pagination .flex-wrap .justify-content-center{
            margin: 0;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            border-radius: 0;
            padding: 0;
        }

        .select2-container {
            width: 100% !important;
        }


    </style>
@endsection

@section('content')
    <div class="wrapper-groups_org">
        <div class="container">
            <div class="row row-padding">
                <div class="col-12">
                    @if(count($groupList)>0)
                        @foreach($groupList as $key => $lists)
                            <div class="card tab-pane fade show active">
                                <div class="card-body">
                                    <div class="line-button">
                                        <div class="main-text">
                                            <div class="row align-items-center">
                                                <span class="btn-action-join" data-id="{{$lists->id}}"></span>
                                                <div class="col-12">
                                                    @if(auth()->user() )
                                                        <h2 class="h2">{{$lists->name}}

                                                            @if(auth()->id() == $lists->creator_id)
                                                                <a class="main-link pull-right">
                                                                    Owner
                                                                </a>
                                                            @elseif($isAlreadyMember != '012')
                                                                <a class="main-link pull-right">
                                                                    @if($isAlreadyMember=='0')
                                                                        Declined
                                                                    @elseif($isAlreadyMember=='1')
                                                                        Pending
                                                                    @elseif($isAlreadyMember=='2')
                                                                        Already a member
                                                                    @endif
                                                                </a>
                                                            @elseif($lists->is_public == 1)
                                                                <a href="javascript:void(0);"
                                                                   data-id="{{$lists->id}}"
                                                                   class="btn-action-join main-link pull-right"><span><i class="fa fa-unlock"></i> Ask to Join</span></a>
                                                            @else
                                                                <a href="javascript:void(0);"
                                                                   data-id="{{$lists->id}}"
                                                                   class="joinPrivateGroup main-link pull-right"><span><i class="fa fa-lock"></i> Ask to Join </span></a>
                                                            @endif
                                                            <a class="main-link pull-right btn-action-join-pending" style="display: none">Pending</a>
                                                            <a href="javascript:void(0);"
                                                               class="shareGroup main-link pull-right mr-1"><span>Share Group</span></a>
                                                        </h2>
                                                    @else
                                                        <h2 class="h2">{{$lists->name}}
                                                            <a data-toggle="modal" data-target="#login_dig" id="login_button" class="main-link pull-right">Sign In To See More</a>
                                                        </h2>
                                                    @endif
                                                    
                                                    <p class="mt-m10">Group Owner: {{$lists->contact_name}}</p>
                                                    <p class="mt-m10">Group Category: {{ucfirst($lists->groupCategory->name)}}</p>
                                                    
                                                    @if(!empty($lists->affiliated_org_id))
                                                        <p class="mt-m10">Status : {{($lists->affiliated_status == '1') ? 'Active' : 'Inactive'}}</p>
                                                        <p class="mt-m10 ">Affiliated Organization : {{ucfirst($lists->affiliatedOrg->org_name)}}</p>
                                                        <?php 
                                                            if($lists->affiliatedOrg->logo_img == NULL){
                                                                    $logo = asset('img/org/001.png');
                                                            }else{
                                                                    $logo = $lists->affiliatedOrg->logo_img;
                                                            }
                                                        ?>
                                                        <div class="wrapper-opportunities">
                                                            <div class="col-3 col-md-2 tab-content" style="padding-left:0px;">
                                                            <div class="avatar" style="background-image:url('{{ $logo}}')"></div>
                                                        </div>
                                                        </div>
                                                        
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="banner-text-box  shift-15">
                                        <div class="row no-gutters">
                                            <div class="col-12 col-md-8" @if($lists->banner_image!='') style="background-image: url('{{$lists->banner_image}}')"@endif></div>
                                            <div class="col-12 col-md-4">
                                                <div class="main-text">
                                                    <p>IMPACTS</p>
                                                    <p class="bold">{{round($lists->tracked_hours/60)}}</p>
                                                    <p class="light">HOUR(S)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="wrapper-tablist  shift-15">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active show" href="#members" role="tab"
                                                   data-toggle="tab" aria-selected="false"><span>Members</span></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#impact" role="tab" data-toggle="tab"
                                                   aria-selected="false"><span>Impact</span></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active show" id="members">
                                            <div class="main-text">
                                                <div class="wrapper-sort-table wrapper-table-org">
                                                    <div>
                                                        @if(($isAlreadyMember=='2' || auth()->id() == $lists->creator_id) || $lists->is_public == 1)
                                                        <table  class="table sortable example">
                                                            <thead>
                                                            <tr>
                                                                <th>
                                                                    <div class="main-text"><p>Name</p></div>
                                                                </th>
                                                                <th>
                                                                    <div class="main-text"><p>Location</p>
                                                                    </div>
                                                                </th>
                                                                <th>
                                                                    <div class="main-text"><p>Impact</p>
                                                                    </div>
                                                                </th>
                                                                <th>
                                                                    <div class="main-text"><p>Rating</p>
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @foreach($lists->members as $members)
                                                                <tr>
                                                                    <td>{{($members->user_role == 'organization') ? $members->org_name : $members->first_name.' '.$members->last_name}}</td>
                                                                    <td>{{implode(', ', array_filter([$members->city, $members->state]))}}</td>
                                                                    <td>
                                                                        <p class="green">{{$members->impact/60}}
                                                                            hour(s)</p></td>
                                                                    <td>{{empty($members->mark) ? 0 : $members->mark}}</td>
                                                                </tr>
                                                            @endforeach

                                                            </tbody>
                                                        </table>
                                                        @else
                                                            You cannot see the members of private group please ask to join.
                                                            <br/><br/><br/><br/>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div role="tabpanel" class="tab-pane fade" id="impact">
                                            @include('_groups.impact-tab-html')
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @endforeach
                @endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="joinModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
            <div class="modal-dialog-fix">
                <div>
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="main-text">
                                <h2 class="h3">Please enter the secret passcode to join this group.</h2>
                            </div>
                            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                <span>&times;</span>
                            </a>
                        </div>

                        <div class="modal-body">
                            <div>
                                <div class="form-group">
                                    <label class="label-text">Enter Passcode Here</label>
                                    <div class="wrapper_select">
                                        <input name="passcode" id="secretPasscode" class="form-control" required>
                                        <span class="w-100"></span>
                                        <small id="KeywordsHelpBlock" class="text-muted">
                                            <i class="fa fa-question-circle"></i>
                                            Please enter secret Passcode that you received in email from owner.
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <div class="wrapper-link">
                                <a id="joinPrivateBtn" href="#"><span>Join Group</span></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(isset($lists))
        <div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg  modal-dialog-centered" role="document">
            <div class="modal-dialog-fix">
                <div>
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="main-text">
                                <h3 class="h3">Share with friends & others</h3>
                                <p><strong class="green"> {{url('/sharegroup/'.base64_encode($lists->id))}} </strong></p>
                            </div>
                            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                <span>&times;</span>
                            </a>
                        </div>
                        <form  id="shareForm" action="{{route('share.group.send.emails')}}" method="post">
                            <div class="modal-body">
                                {{csrf_field()}}

                                <input type="hidden" name="link" value="{{url('/sharegroup/'.base64_encode($lists->id))}}">
                                <input type="hidden" name="group_id" value="{{$lists->id}}">
                                <div class="form-group">
                                    <label class="label-text">Please select friends:</label>
                                    <div class="wrapper_select">
                                        <select name="friend_emails[]" id="shareFriendsGroupSelect2" class="form-control custom-dropdown select2" multiple>
                                            @foreach($myFriends as $friend)
                                                <option value="{{$friend->email}}">{{$friend->email}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="label-text">Write Others Emails:</label>
                                    <div class="wrapper_select">
                                        <textarea id="mailsText" rows="5" name="others_emails" style="width: 100%" placeholder="Write emails one per line"></textarea>
                                        <span class="w-100"></span>
                                        <small id="KeywordsHelpBlock" class=" text-muted">
                                            <i class="fa fa-question-circle"></i>
                                            Please write one email per line to avoid issue. No white spaces allowed
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="wrapper-link">
                                    <a id="sendEmails" onclick="document.getElementById('shareForm').submit();" href="#"><span>Send To All</span></a>
                                    <a class="btn_email_loading" style="display:none;"> <span><i class="fa fa-spinner fa-spin"></i> Please wait...!</span></a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('script')

    <script src="{{asset('js/highcharts.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    @include('_groups.groups-charts-js')
    <script>

        $(function() {

            $("#shareFriendsGroupSelect2").select2({
                placeholder: "Select friends",
                allowClear: true
            });

            $('#sendEmails').on('click', function () {
                $(this).hide();
                $('.btn_email_loading').show();
            });

            //remove white spaces for emails
            $("#mailsText").keyup(function(e){
                if(this.value.match(" ")){
                    this.value = this.value.replace(" ", "");
                    toastr.warning("Whitespaces not allowed in emails.", "Warning");
                }
            });

            var role = "{{auth()->user() ? auth()->user()->user_role : ''}}";
            var roleUrl = role == 'volunteer' ? 'volunteer/joinGroup' : 'organization/joinGroup';

            $('.shareGroup').on('click', function () {
                $('#shareModal').modal('show');
            });

            $('.joinPrivateGroup').on('click', function () {
                $('#joinModal').modal('show');
            });

            $('#joinPrivateBtn').on('click', function () {
                $("#secretPasscode").focus();
                var group_id = $(".btn-action-join").data('id');
                var private_passcode = $('#secretPasscode').val();
                if (private_passcode == '') {
                    toastr.error("Passcode field is required.", "Message");
                    return false;
                }
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('check.private.group.pascode')}}",
                    data: { group_id: group_id , private_passcode: private_passcode},
                    success: function (data) {

                        if(data.result == true){
                            $('#joinModal').modal('hide');
                            $('.joinPrivateGroup').hide();
                            $(".btn-action-join").click();
                        }
                        else{
                            toastr.error("Your secret passcode is invalid.", "Message");
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        toastr.error("Something went wrong..!", "error");
                    }
                });
            });


            $('.btn-action-join').on('click', function () {
                var url = API_URL + roleUrl;

                var group_id = $(this).data('id');
                var current_button = $(this);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var type = "POST";
                var formData = { group_id: group_id };


                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    success: function (data) {

                        current_button.hide();
                        $('.btn-action-join-pending').show();

                        if(data.autoAccept == true){
                            toastr.success("Your joining request has been approved.", "Message");
                            $('.btn-action-join-pending').text('Approved')
                        }
                        else{
                            toastr.success("Your joining request has been sent.", "Message");
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        toastr.error("Something went wrong..!", "error");
                    }
                });
            });
        });

        $('.example').dataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            "pageLength": 10,
            "pagingType": "full_numbers",
            "language": {
                "paginate": {
                    "first": "",
                    "next": '',
                    "previous": '',
                    "last": ''
                }
            }
        });

    </script>
@endsection

