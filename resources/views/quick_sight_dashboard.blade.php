@extends('layout.master')

@section('content')

    <div class="full-50-col-box">
        <div class="container" style="height:100%">
            <div>
                <h4>Domain Organization
                    <a href="{{$url}}" target="_blank">View Detail</a>
                </h4>
            </div>

            {{--<iframe height="800px" style=";padding:0;margin:0;border:0;display:block;" src="{{$url}}"></iframe>--}}
            <div id="dashboardContainer"></div>
        </div>
    </div>

@endsection

@section('script')
    @include('admin.include.toaster-js')

    <script src="https://unpkg.com/amazon-quicksight-embedding-sdk@1.0.5/dist/quicksight-embedding-js-sdk.min.js"></script>

    <script>
        var dashboard;

        function onDashboardLoad(payload) {
            toastr.warning("Dashboard is fully loaded", "Success");
        }

        function onError(payload) {
            toastr.error("Dashboard is not loaded", "Error");
        }

        var containerDiv = document.getElementById("dashboardContainer");

        var options = {
            url: '{{$url}}',
            container: containerDiv,
            scrolling: "no",
            height: "750px"
        };
        dashboard = QuickSightEmbedding.embedDashboard(options);

        dashboard.on('error', onError);
        dashboard.on('load', onDashboardLoad);
    </script>
@endsection