<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/clear-cache', function() {
    $run = Artisan::call('config:clear');
    $run = Artisan::call('view:clear');
    $run = Artisan::call('cache:clear');
    $run = Artisan::call('config:cache');
    return redirect('https://myvoluntier.com/');
});

Route::get('/abc/test', 'TestController@index')->name('testing');
Route::get('/abc/test/addTestOpprData/{opprId}', 'TestController@addTestOpprData')->name('addTestOpprData');
Route::get('/abc/test/addTestGroupsData', 'TestController@addTestGroupsData')->name('addTestGroupsData');
Route::get('/abc/test/addTestOrgData/{orgId}', 'TestController@addTestOrgData')->name('addTestOrgData');

Route::get('/abc/test/addTestVolImpactData/{userId}', 'TestController@addTestVolImpactData')->name('addTestVolImpactData');

Route::get('/organizationlist', 'Organization\HomeCtrl@nonAuthOrganization')->name('nonAuthOrganization');
Route::get('/vieworganization/profile/{id?}', 'Organization\HomeCtrl@viewProfile')->name('view-organization-profile');

Route::group(['middleware' => 'web'], function () {

    Route::get('api/get-sub-organization/{orgId}', 'UserCtrl@getSubOrganization');

    Route::auth();
    Route::get('/', 'UserCtrl@showLanding')->name('home');
    Route::get('/features', 'UserCtrl@showFeatures')->name('features');
    Route::get('/pricing', 'UserCtrl@showPricing');
    Route::get('/request', 'UserCtrl@showRequest')->name('request');
    Route::get('/login', function () {
        return redirect()->to('/');
    });
    Route::get('/terms_conditions', 'UserCtrl@terms_conditions');
    Route::post('api/reg_volunteer', 'UserCtrl@regVolunteer');
    Route::post('api/reg_organization', 'UserCtrl@regOrganization');
    Route::post('api/reg_delegate', 'UserCtrl@regDelegate');
    Route::get('api/delete_delegate/{id}', 'UserCtrl@deleteDelegate')->name('delete-delegate');

    Route::post('api/login_user', 'UserCtrl@login');
    Route::get('/confirm_account/{user_name}/{token}', 'UserCtrl@confirmAccount');
    Route::post('api/forgot_password', 'UserCtrl@forgotPassword');
    Route::get('/forgot_password/{user_name}/{token}', 'UserCtrl@changeForgotPassword');


    Route::post('api/create_new_password', 'UserCtrl@createNewPassword');
    Route::post('api/forgot_username', 'UserCtrl@forgotUsername');

    /*shared profile*/
    Route::get('shared-profile/{shared_id}/{code}', 'SharingCtrl@sharedProfilePage');
    Route::get('shared-transcript/{shared_id}/{checkval}/{code}', 'SharingCtrl@sharedTranscriptPage');
    Route::get('shared-serviceproject/{shared_id}/{code}', 'Volunteer\ServiceProjectCtrl@sharedServiceProjectPage');
    /*check expired shared profile*/
    Route::get('/check_shared_profile', 'SharingCtrl@checkSharedProfile');
    /*send request*/
    Route::post('api/send_request', 'SharingCtrl@sendRequest');


    Route::post("store", 'UserCtrl@updateName');
    Route::post("upload_image", 'UserCtrl@activityImageUpload');

    Route::post('/status_post', 'UserCtrl@statusPost');
    Route::post('/delete_image', 'UserCtrl@imageDelete');
    Route::post('/delete_comment', 'UserCtrl@commentDelete');
    Route::get('/about_us', 'UserCtrl@about_us')->name('about_us');
    Route::get('/quick-sight-dashboard', 'UserCtrl@quickSightDashboard')->name('quick_sight_dashboard');
    Route::get('/articles', 'UserCtrl@articles')->name('articles');

    Route::get('/terms-and-conditions', 'UserCtrl@termsAndConditions')->name('termsAndConditions');
    Route::get('/privacy-policy', 'UserCtrl@privacyPolicy')->name('privacyPolicy');



    //admin
    Route::get('/admin/index', 'Admin\AdminLoginController@index');
    Route::get('/admin/login', 'Admin\AdminLoginController@adminLogin')->name('admin-login-get');
    Route::post('/admin-login', 'Admin\AdminLoginController@login')->name('admin-login-post');
    /* admin organization mail send */
        Route::post('api/admin_email_send', 'Admin\UsersController@adminEmailSend');
    /*end admin organization mail send */
    Route::group(['prefix' => '/admin', 'middleware' => 'admin'], function () {


        Route::post('/volunteer-status/update', 'Admin\UsersController@updateUserStatuses')->name('user.statuses.update');
        Route::post('/organization-domain/update', 'Admin\UsersController@updateUserDomains')->name('user.domains.update');

        Route::get('/dashboard', 'Admin\AdminLoginController@dashboard')->name('admin-dashboard');
        Route::post('/send-email-now', 'Admin\AdminLoginController@sendEmailNow')->name('admin-send-email');
        Route::post('/send-sms-now', 'Admin\AdminLoginController@sendSMSNow')->name('admin-send-sms');

        Route::get('/volunteer-list', 'Admin\UsersController@volunteer_listing')->name('volunteer-list');

        Route::get('/organization-list/{type}', 'Admin\UsersController@organization_listing')->name('organization-list');

        Route::get('/manage-user/{type}/{id}', 'Admin\UsersController@manageUser')->name('login-to-user');

        Route::get('/pending', 'Admin\UsersController@pending')->name('group-list');

        Route::get('/admin-logout', 'Admin\AdminLoginController@logout')->name('admin-logout');
        Route::get('/resetPassword', 'Admin\AdminLoginController@resetPassword')->name('admin-resetPassword');
        Route::post('/forgot', 'Admin\AdminLoginController@forgot')->name('admin-forgot');
        Route::get('/user-delete/{id}', 'Admin\UsersController@deleting')->name('admin-delete');
        Route::get('/user-editing/{id}', 'Admin\UsersController@editing')->name('admin-editing');
        Route::get('/cancel-list', 'Admin\UsersController@cancelUser');
        Route::post('/editPost', 'Admin\UsersController@editPost');
        Route::post('/multipleDelete', 'Admin\UsersController@multipleDelete');
        Route::get('import-export-csv-excel', 'Admin\UsersController@importExportExcelORCSV');
        Route::post('load-csv-excel', 'Admin\UsersController@importFileToShowRecords')->name('load.csv.file');
        Route::post('import-csv-excel', 'Admin\UsersController@importFileIntoDB')->name('import-csv-excel');

        Route::get('/showusertemdata/{data}', 'Admin\UsersController@showUserTemData');
        Route::post('/showusertemdatapost', 'Admin\UsersController@showUserTemDataPost');
        Route::get('/group-list', 'Admin\GroupManageController@listing')->name('group-list');
        Route::get('/group-delete/{id}', 'Admin\GroupManageController@deleting')->name('group-delete');
        Route::get('/group-editing/{id}', 'Admin\GroupManageController@editing')->name('group-editing');
        Route::post('/groupeditPost', 'Admin\GroupManageController@editPost');
        Route::get('/group_add_vol/{id}', 'Admin\GroupManageController@groupAddVol');
        Route::post('/group_add_vol_post', 'Admin\GroupManageController@groupAddVolPost');
        Route::post('/approve', 'Admin\UsersController@approve');
        Route::post('/reject', 'Admin\UsersController@reject');

        Route::get('/opportunity-list', 'Admin\OpportunityManageController@opportunityList')->name('opportunity-list');
        Route::get('/opportunity-delete/{id}', 'Admin\OpportunityManageController@deleteOpportunity')->name('opportunity-delete');
        Route::get('/opportunity-edit/{id}', 'Admin\OpportunityManageController@editOpportunity')->name('opportunity-edit');
        Route::post('api/admin/edit_opportunity/{id}', 'Admin\OpportunityManageController@updateOpportunity');

        Route::get('/delete-inactive-user-oppr', 'Admin\OpportunityManageController@opportunityDeleteInActive')->name('opportunity-delete-in-active');


        Route::get('/transcript', 'Admin\ReportsController@trackedHoursReport');
        Route::get('/transcript_csv', 'Admin\ReportsController@trackedHoursReportCSV');
        Route::get('/transcript_pdf', 'Admin\ReportsController@trackedHoursReportPdf');

        Route::get('/tracked_hours', 'Admin\ReportsController@trackedHoursReport');
        Route::get('/tracked_hours_csv', 'Admin\ReportsController@trackedHoursReportCSV');
        Route::get('/tracked_hours_pdf', 'Admin\ReportsController@trackedHoursReportPdf');

        Route::get('/voluntier_organization', 'Admin\ReportsController@voluntierOrganization');
        Route::get('/voluntier_organization_csv', 'Admin\ReportsController@voluntierOrganizationCSV');
        Route::get('/voluntier_organization_pdf', 'Admin\ReportsController@voluntierOrganizationPdf');

        Route::get('/member_total_hours', 'Admin\ReportsController@memberTotalHours');
        Route::get('/member_total_hours_csv', 'Admin\ReportsController@memberTotalHoursCSV');
        Route::get('/member_total_hours_pdf', 'Admin\ReportsController@memberTotalHoursPdf');

        Route::get('/organization_opportunity', 'Admin\ReportsController@organizationOpportunity');
        Route::get('/organization_opportunity_csv', 'Admin\ReportsController@organizationOpportunityCSV');
        Route::get('/organization_opportunity_pdf', 'Admin\ReportsController@organizationOpportunityPdf');

        Route::get('/organization_member', 'Admin\ReportsController@organizationMember');
        Route::get('/organization_member_csv', 'Admin\ReportsController@organizationMemberCSV');
        Route::get('/organization_member_pdf', 'Admin\ReportsController@organizationMemberPdf');

        Route::get('/organization_tracked_hours', 'Admin\ReportsController@organizationTrackedHours');
        Route::get('/organization_tracked_hours_csv', 'Admin\ReportsController@organizationTrackedHoursCSV');
        Route::get('/organization_tracked_hours_pdf', 'Admin\ReportsController@organizationTrackedHoursPdf');

        Route::get('/organization_groups', 'Admin\ReportsController@organizationGroups');
        Route::get('/organization_groups_csv', 'Admin\ReportsController@organizationGroupsCSV');
        Route::get('/organization_groups_pdf', 'Admin\ReportsController@organizationGroupsPdf');

        Route::get('/organization_groups_update', 'Admin\ReportsController@showUsers');
        Route::get('/organization_groups_update_csv/{organization_name?}/{organization_type?}/{group_name?}/{group_id?}', 'Admin\ReportsController@showUsersCSV');
        Route::get('/organization_groups_update_pdf/{organization_name?}/{organization_type?}/{group_name?}/{group_id?}', 'Admin\ReportsController@showUsersPdf');

        Route::post('/organization_groups_get', 'Admin\ReportsController@groupsUpdate');
        Route::post('/show_users', 'Admin\ReportsController@showUsers');

        
        //  Email+Texts Templates
        Route::group(['prefix' => 'emails-text', 'namespace' => 'Admin'], function () {

            Route::get('/', [
                'as' => 'admin.email.index',
                'uses' => 'EmailsTemplatesController@index'
            ]);

            Route::get('/edit/{id}', [
                'as' => 'admin.email.edit',
                'uses' => 'EmailsTemplatesController@edit'
            ]);

            Route::post('/update', [
                'as' => 'admin.email.update',
                'uses' => 'EmailsTemplatesController@update'
            ]);
        });

        Route::get('/clear-test-data', [
            'as' => 'clear.test.data',
            'uses' => 'Admin\AdminSettingsController@index'
        ]);

        Route::post('/clear-test-data/update', [
            'as' => 'clear.test.data.update',
            'uses' => 'Admin\AdminSettingsController@updateData'
        ]);

        Route::get('/add-dummy-hours', [
            'as' => 'add.dummy.hours.get',
            'uses' => 'Admin\AdminSettingsController@addHoursIndex'
        ]);

        Route::post('/add-dummy-hours-post', [
            'as' => 'clear.test.data.post',
            'uses' => 'Admin\AdminSettingsController@addHoursPost'
        ]);

        Route::get('/site-content', [
            'as' => 'admin.sitecontent',
            'uses' => 'Admin\AdminSettingsController@siteContents'
        ]);

        Route::get('/site-content-edit/{id}', [
            'as' => 'admin.sitecontent.edit',
            'uses' => 'Admin\AdminSettingsController@siteContentEdit'
        ]);

        Route::post('/site-content-edit', [
            'as' => 'admin.sitecontent.update',
            'uses' => 'Admin\AdminSettingsController@siteContentUpdate'
        ]);

        Route::get('/attributes', [
            'as' => 'admin.attributes',
            'uses' => 'Admin\AdminSettingsController@attributes'
        ]);

        Route::get('/attribute-add', [
            'as' => 'admin.attributes.save',
            'uses' => 'Admin\AdminSettingsController@attributeSave'
        ]);
        Route::get('/attribute-edit/{id}', [
            'as' => 'admin.attribute.edit',
            'uses' => 'Admin\AdminSettingsController@attributeSave'
        ]);
        Route::post('/attribute-adddata', [
            'as' => 'admin.attributes.save',
            'uses' => 'Admin\AdminSettingsController@attributeSave'
        ]);
        Route::get('/move-groups-images', [
            'as' => 'move-groups-images',
            'uses' => 'Admin\GroupManageController@moveGroupImagesToS3'
        ]);
        Route::get('/move-opportunity-images', [
            'as' => 'move-opportunity-images',
            'uses' => 'Admin\OpportunityManageController@moveOpportunityImagesToS3'
        ]);

        Route::get('/move-users-images', [
            'as' => 'move-users-images',
            'uses' => 'Admin\UsersController@moveUsersImagesToS3'
        ]);

        
    
    });
});

    Route::group(['middleware' => 'org'], function () {

    /* homepage */
    //Route::get('/organization', 'Organization\HomeCtrl@viewHome')->name('home-organization');
    Route::get('/organization/report', 'Organization\HomeCtrl@viewHome')->name('organization-report');
    Route::get('/organization', 'Organization\OpportunityCtrl@viewReport')->name('home-organization');

    Route::get('/organization/home/{action?}', 'Organization\HomeCtrl@viewHome');
    Route::post('api/organization/profile/upload_logo', 'Organization\HomeCtrl@upload_logo');
    Route::get('/organization/search', 'Organization\HomeCtrl@Search')->name('organization-search');
    Route::post('api/organization/joinGroup', 'Organization\HomeCtrl@joinGroup');

    /* account page */
    Route::get('/organization/accountSetting', 'Organization\HomeCtrl@viewEditAccount')->name('organization-account-setting');
    Route::post('api/organization/update_account', 'UserCtrl@update_account');

    /* profile page */
    Route::get('/organization/profile/{id?}', 'Organization\HomeCtrl@viewProfile')->name('view-organization-profile');
    Route::post('api/organization/profile/upload_back_img', 'Organization\HomeCtrl@upload_back_img');
    Route::get('/organization/show_organization/{id?}', 'Organization\HomeCtrl@viewProfile')->name('organization-profile');

    /* opportunity */
    Route::get('/organization/opportunity/{id?}', 'Organization\OpportunityCtrl@viewManageOpportunity')->name('organization-opportunity');
    Route::get('/organization/post_opportunity', 'Organization\OpportunityCtrl@viewPostingPage')->name('organization-opportunity-post');
    Route::post('api/organization/post_opportunity', 'Organization\OpportunityCtrl@postOpportunity');
    Route::get('/organization/edit_opportunity/{id?}', 'Organization\OpportunityCtrl@viewUpdatingPage');
    Route::post('api/organization/edit_opportunity/{id}', 'Organization\OpportunityCtrl@updateOpportunity');
    Route::get('/organization/view_opportunity/{id?}', 'Volunteer\OpportunityCtrl@viewOpportunityPage')->name('view-opportunity-profil');
    Route::post('api/organization/delete_opportunity', 'Organization\OpportunityCtrl@deleteOpportunity')->name('api-organization-delete_opportunity');
    Route::post('api/organization/opportunity/accept_member', 'Organization\OpportunityCtrl@acceptOpportunityMember');
    Route::post('api/organization/delete_domain', 'UserCtrl@delete_domain');


    Route::get('/organization/clone_opportunity/{id?}', 'Organization\OpportunityCtrl@cloneOpportunity');
    Route::post('api/organization/validated-opportunity', 'Organization\OpportunityCtrl@validatedOpportunity');


    /* track */
    Route::get('/organization/track', 'Organization\TrackingCtrl@viewTrackingPage')->name('organization-track');
    Route::post('api/organization/track/approve', 'Organization\TrackingCtrl@approveTrack');
    Route::post('api/organization/track/decline', 'Organization\TrackingCtrl@declineTrack');
    Route::get('/organization/track/customConfirm/{track_id}/{confirm_code}', 'Organization\TrackingCtrl@confirmCustomTrack');
    Route::get('/organization/approve/{id?}/{status?}', 'Organization\TrackingCtrl@approveGroupRequest');
    Route::get('/organization/approveJoinOpportunity/{id?}/{related_id?}/{status?}', 'Organization\OpportunityCtrl@approveJoinOpportunityRequest');

    Route::get('/organization/approve-by-qr-track-detail', 'Organization\TrackingCtrl@approveTrackingDetailOrg')->name('approve-org-track-detail');

    /* Group */
    Route::get('/organization/group', 'Volunteer\GroupCtrl@viewGroupPage')->name('organization-group');
    Route::post('/organization/dGroup-domain-attributes', 'Organization\GroupCtrl@dGroupDomainAttributes')->name('org-dGroup-domain-attr');
    Route::post('/organization/get-dynamic-group-rules', 'Organization\GroupCtrl@getDynamicGroupRules')->name('get-dynamic-group-rules');
    Route::get('/organization/group/group_add/{id?}', 'Organization\GroupCtrl@viewGroupAddPage')->name('organization-group-group_add');
    Route::post('api/organization/group/create_org_group', 'Organization\GroupCtrl@createGroup');
    Route::post('api/organization/group/change_org_group', 'Organization\GroupCtrl@changeGroup');
    Route::get('/organization/group/view_group_detail/{group_id}', 'Organization\GroupCtrl@viewGroupDetail');
    Route::get('/organization/group/group_search', 'Organization\GroupCtrl@searchGroup');
    Route::post('api/organization/group/get_user', 'Organization\GroupCtrl@getUserList')->name('api-organization-group-get-user');
    Route::post('api/organization/group/add_user_invitation', 'Organization\GroupCtrl@addUserInvitation')->name("org_add_user_invitation");
    Route::post('api/organization/group/add_group_invitation', 'Organization\GroupCtrl@addGroupInvitation');
    Route::post('api/organization/group/get_members_groups_by_name', 'Organization\GroupCtrl@getUserByGroup')->name('get_user_by_group');

    Route::get('/organization/approveAffiliatedOrg/{id?}/{related_id?}/{status?}', 'Organization\GroupCtrl@approveAffiliatedOrgRequest');

    /* Follow */
    Route::post('api/organization/followOrganization', 'Organization\HomeCtrl@followOrganization');
    Route::post('api/organization/unfollowOrganization', 'Organization\HomeCtrl@unfollowOrganization');
    Route::post('api/organization/followGroup', 'Organization\GroupCtrl@followGroup');
    Route::post('api/organization/unfollowGroup', 'Organization\GroupCtrl@unfollowGroup');

    /* Join to Group */
    Route::post('api/organization/jointoGroup', 'Organization\GroupCtrl@jointoGroup')->name('org.join.group');
    Route::get('/organization/leave_group/{id?}', 'Organization\GroupCtrl@leaveGroup');

    /* Friend */
    Route::get('/organization/friend', 'Organization\FriendCtrl@viewFriendPage')->name('organization-friend');
    Route::post('/organization/friend_accept_reject', 'Volunteer\FriendCtrl@accept_reject')->name('organization-friend-accept-reject');
    Route::post('api/organization/group/get_friend', 'Organization\FriendCtrl@getFriend')->name('organization-get-friend');

    /* connect Friend */
    Route::post('api/organization/connectOrganization', 'Organization\HomeCtrl@connectOrganization');
    Route::post('api/organization/acceptFriend', 'Organization\HomeCtrl@acceptFriend')->name('organization-accept-friend');
    Route::get('/organization/disconnectFriend/{id}', 'Organization\HomeCtrl@disconnectFriend');

    /* Organization Impact */
    Route::get('/organization/impact', 'Organization\OpportunityCtrl@organizationImpactPage')->name('organization-impact');
    Route::post('/organization/impact-vol-oppor/', 'Organization\OpportunityCtrl@impactVolOppr')->name('organization-impact-vol-opprs');

    /* send message */
    Route::post('api/organization/sendmessage', 'Organization\HomeCtrl@sendMessage');

    /* send message */
    Route::get('/organization/chat', 'ChatController@index')->name('organization-chat');
    Route::post('api/opportunity_email_blast', 'Organization\OpportunityCtrl@emailblast');

    /* start sub organization route */
    Route::get('api/organization/edit-sub-organization/{id?}', 'Organization\HomeCtrl@editSubOrganization')->name('edit-sub-organization');
    Route::get('/organization/deleteSubOrganization/{id?}', 'Organization\HomeCtrl@deleteSubOrganization')->name('delete-sub-organization');
    /* end sub organization route */

    /*export opportunity members */
    Route::get('/organization/export-members/{id?}', 'Organization\OpportunityCtrl@exportOppMembers');
    Route::get('/organization/opportunities-members', 'Organization\OpportunityCtrl@exportAllOppMembers');
    /* end export members*/

    /*export group members */
    Route::get('/organization/export-group-members/{id?}', 'Organization\GroupCtrl@exportGroupMembers');
    Route::get('/organization/group-members', 'Organization\GroupCtrl@exportAllGroupMembers');
    /* end export group members*/

    /* organization mail blast */
    Route::post('api/organization_email_blast', 'Organization\HomeCtrl@orgEmailblast');
    /*end organization email blast */

    /* partners */
    Route::post('api/add-partner', 'Organization\PartnerCtrl@savePartner');
    Route::get('api/edit-partner/{id?}', 'Organization\PartnerCtrl@savePartner')->name('edit-partner');
    Route::get('/organization/deletePartner/{id?}', 'Organization\PartnerCtrl@deletePartner')->name('delete-partner');
    /* end partners */

    /* approve/decline sub organization */
    Route::get('/organization/approveSubOrg/{id?}/{related_id?}/{status?}', 'Organization\HomeCtrl@approveSubOrgRequest')->name('update-suborg-status');
    /* end approve */
    
    /* for organization track */
    Route::get('api/organization/viewtrack/{id}', 'Organization\TrackingCtrl@viewTrack');
    Route::post('api/organization/track/validatedTrackDate', 'Organization\TrackingCtrl@validatedTrackDate');
    Route::post('api/organization/track/validatedTrackHours', 'Organization\TrackingCtrl@validatedTrackHours');
    Route::post('api/organization/track/validatedMaxVolunteer', 'Organization\TrackingCtrl@validatedMaxVolunteer');
    Route::post('api/organization/track/addHours', 'Organization\TrackingCtrl@addHours')->name('organization-track-addHours');
     Route::get('/organization/exportTrackedHours', 'Organization\TrackingCtrl@exportTrackedHours');
    /* end organization track */
});

Route::group(['middleware' => 'vol'], function () {

    Route::post('api/volunteer/org-email-exist', 'Volunteer\HomeCtrl@checkOrgEmailExist')->name('check-org-exist');
    /* homepage */
    Route::get('/volunteer', 'Volunteer\HomeCtrl@viewHome')->name('home-volunteer');
    Route::get('/volunteer/home/{action?}', 'Volunteer\HomeCtrl@viewHome');
    Route::post('api/volunteer/upload_logo', 'Volunteer\HomeCtrl@upload_logo');
    Route::get('/volunteer/search', 'Volunteer\HomeCtrl@Search')->name('volunteer-search');
    Route::post('api/volunteer/joinGroup', 'Volunteer\HomeCtrl@joinGroup')->name('volt.join.group');
    /* account page */
    Route::get('/volunteer/accountSetting', 'Volunteer\HomeCtrl@viewEditAccount')->name('volunteer-account-setting');;
    Route::post('api/volunteer/update_account', 'UserCtrl@update_volunteer_account');
    Route::post('api/volunteer/delete_alteremail', 'UserCtrl@delete_alteremail')->name('delete-volunteer-alteremail');
    /* profile page */
    Route::get('/volunteer/profile/{id?}', 'Volunteer\HomeCtrl@viewProfile')->name('view-voluntier-profile');
    Route::post('api/volunteer/profile/upload_back_img', 'Volunteer\HomeCtrl@upload_back_img');

    /* impact */
    Route::get('/volunteer/impact', 'Volunteer\HomeCtrl@viewImpact')->name('volunteer-impact');
    Route::post('api/volunteer/impact/getFriendInfo', 'Volunteer\HomeCtrl@getFriendInfo')->name('volunteer-get-friend-info');
    Route::get('volunteer/impact/getFriendInfo', 'Volunteer\HomeCtrl@viewImpact');

    /* opportunities */
    Route::get('/volunteer/opportunity', 'Volunteer\OpportunityCtrl@viewOpportunity')->name('volunteer-opportunity');
    Route::get('/volunteer/view_opportunity/{id?}', 'Volunteer\OpportunityCtrl@viewOpportunityPage')->name('volunteer-view-opportunity-profile');
    Route::get('/volunteer/show_organization/{id?}', 'Organization\HomeCtrl@viewProfile')->name('organization-profile');

    Route::get('/volunteer/get_opportunity/{id}', 'Volunteer\OpportunityCtrl@getOpportunityTimeInfo')->name('get-opportunity-time-info');

    Route::post('api/volunteer/opportunity/join', 'Volunteer\OpportunityCtrl@joinOpportunity');
    Route::post('api/volunteer/find_opportunity_on_map', 'Volunteer\OpportunityCtrl@findOnMap');
    Route::get('api/volunteer/opportunity/getSearchResult', 'Volunteer\OpportunityCtrl@getSearchResult');

    /* track */
    Route::get('/volunteer/track', 'Volunteer\TrackingCtrl@viewTrackingPage')->name('volunteer-track');
    Route::get('/volunteer/single_track', 'Volunteer\TrackingCtrl@viewSingleTrackingPage')->name('volunteer-single_track');
    Route::post('api/volunteer/track/getOpportunities', 'Volunteer\TrackingCtrl@getOpportunityNames');
    Route::post('api/volunteer/track/getOpportunityInfo', 'Volunteer\TrackingCtrl@getOpportunityInfo');
    Route::post('api/volunteer/track/joinToOpportunity', 'Volunteer\TrackingCtrl@joinToOpportunity');
    Route::post('api/volunteer/track/addHours', 'Volunteer\TrackingCtrl@addHours')->name('volunteer-track-addHours');
    Route::post('api/volunteer/track/removeHours', 'Volunteer\TrackingCtrl@removeHours');
    Route::get('api/volunteer/track/viewTracks', 'Volunteer\TrackingCtrl@viewTracks')->name('volunteer-track-view-tracks');
    Route::get('api/volunteer/track/activityView', 'Volunteer\TrackingCtrl@activityView');


    Route::get('api/volunteer/track/downloadICSFile/{id}', 'Volunteer\TrackingCtrl@downloadICSFile');

    Route::post('api/volunteer/track/validatedTrackDate', 'Volunteer\TrackingCtrl@validatedTrackDate');
    Route::post('api/volunteer/track/validatedTrackHours', 'Volunteer\TrackingCtrl@validatedTrackHours');
    Route::post('api/volunteer/track/validatedMaxVolunteer', 'Volunteer\TrackingCtrl@validatedMaxVolunteer');

    Route::get('/volunteer/get-track-detail', 'Volunteer\TrackingCtrl@getTrackingDetail')->name('get-vol-track-detail');

    /* Group */
    Route::post('api/volunteer/group/get_members_groups_by_name', 'Volunteer\GroupCtrl@getUserByGroup')->name('volunteer-get-user-by-group');

    Route::get('/volunteer/group', 'Volunteer\GroupCtrl@viewGroupPage')->name('volunteer-group');
    Route::get('/volunteer/group/group_add/{id?}', 'Volunteer\GroupCtrl@viewGroupAddPage')->name('volunteer-group-group_add');
    Route::post('api/volunteer/group/create_vol_group', 'Volunteer\GroupCtrl@createGroup');
    Route::post('api/volunteer/group/change_org_group', 'Volunteer\GroupCtrl@changeGroup');
    Route::post('api/volunteer/group/get_user', 'Volunteer\GroupCtrl@getUserList')->name('api-volunteer-group-get-user');
    Route::post('api/volunteer/group/add_user_invitation', 'Volunteer\GroupCtrl@addUserInvitation')->name('vol_add_user_invitation');
    //Route::get('/volunteer/group/{id}','Volunteer\GroupCtrl@viewCertainGroupPage')->name('volunteer-certain-group');
    /*Follow*/
    Route::post('api/volunteer/followOrganization', 'Volunteer\HomeCtrl@followOrganization');
    Route::post('api/volunteer/unfollowOrganization', 'Volunteer\HomeCtrl@unfollowOrganization');
    Route::post('api/volunteer/followGroup', 'Volunteer\GroupCtrl@followGroup');
    Route::post('api/volunteer/unfollowGroup', 'Volunteer\GroupCtrl@unfollowGroup');
    /*Join to Group*/
    Route::post('api/volunteer/jointoGroup', 'Volunteer\GroupCtrl@jointoGroup');
    Route::get('/volunteer/leave_group/{id?}', 'Volunteer\GroupCtrl@leaveGroup');

    /*Friend*/
    Route::get('/volunteer/friend', 'Volunteer\FriendCtrl@viewFriendPage')->name('volunteer-friend');
    Route::post('/volunteer/friend_accept_reject', 'Volunteer\FriendCtrl@accept_reject')->name('volunteer-friend-accept-reject');
    Route::post('api/volunteer/group/get_friend', 'Volunteer\FriendCtrl@getFriend')->name('volunteer-get-friend');
    /*connect Friend*/
    Route::post('api/volunteer/connectOrganization', 'Volunteer\HomeCtrl@connectOrganization');
    Route::post('api/volunteer/acceptFriend', 'Volunteer\HomeCtrl@acceptFriend')->name('volunteer-accept-friend');
    Route::get('/volunteer/disconnectFriend/{id}', 'Volunteer\HomeCtrl@disconnectFriend');
    /* send message */

    Route::post('api/volunteer/sendmessage', 'Volunteer\HomeCtrl@sendMessage');
    /* send message */
    Route::get('/volunteer/chat', 'ChatController@index')->name('volunteer-chat');

    Route::post('/volunteer/commentStatus/{id?}', 'Volunteer\HomeCtrl@statusComment');

    /* service project */
    Route::get('/volunteer/edit-service-project/{id?}', 'Volunteer\ServiceProjectCtrl@createServiceProject')->name('edit-service-project');
    Route::get('/volunteer/createServiceProject', 'Volunteer\ServiceProjectCtrl@createServiceProject')->name('create-service-project');
    Route::post('/volunteer/createServiceProject', 'Volunteer\ServiceProjectCtrl@createServiceProject')->name('add-service-project');
    Route::post('/volunteer/createServiceProject/{id?}', 'Volunteer\ServiceProjectCtrl@createServiceProject');
    Route::get('/volunteer/view-service-project/{id?}', 'Volunteer\ServiceProjectCtrl@viewServiceProject')->name('view-service-project');
    Route::get('/volunteer/deleteServiceProject/{id?}', 'Volunteer\ServiceProjectCtrl@deleteServiceProject')->name('delete-service-project');
    Route::post('api/volunteer/validated-service-project/', 'Volunteer\ServiceProjectCtrl@validatedServiceProject');
    /* end service project */
    /* custom attributes */
    Route::post('api/add-customattribute', 'Volunteer\CustomAttributeCtrl@saveCustomAttribute');
    Route::get('api/edit-customattribute/{id?}', 'Volunteer\CustomAttributeCtrl@saveCustomAttribute')->name('edit-customattribute');
    Route::get('/volunteer/deleteCustomAttribute/{id?}', 'Volunteer\CustomAttributeCtrl@deleteCustomAttribute')->name('delete-customattribute');
    /* end custom attributes */
});

Route::group(['middleware' => 'auth'], function () {

    Route::post('/send-chat-notify', 'ChatController@sendChatNotifyEmail')->name('send-chat-notify');

    Route::post('update-notifications-setting', [
        'as' => 'user.notifications.setting.post',
        'uses' => 'UserSettingController@updateUserNotificationSettings'
    ]);

    Route::post('organization/opportunity/share_to_friends', 'Organization\OpportunityCtrl@share_to_friends')->name('opportunity-share-to-friends');

    Route::get('/signout_user', 'UserCtrl@signout')->name('signout_user');
    /* share_profile */
    Route::post('api/share_profile', 'SharingCtrl@shareProfile');
    Route::post('api/share_transcript', 'SharingCtrl@shareTranscript');

    Route::post('api/share_service_project', 'SharingCtrl@shareServiceProject');
    /* get Message */
    Route::post('api/getMessages', 'SharingCtrl@getMessage');

    /* Alert */
    Route::get('viewAlert', 'SharingCtrl@viewAlertPage')->name('view-alert');
    Route::post('api/getAlert', 'SharingCtrl@getAlert');
    Route::get('/chat/token/{uid}', 'ChatController@create_custom_token')->name('chat-token');

    Route::post('api/deleteChat/{id}', 'ChatController@deleteChat');
    /* get transcript */
    Route::get('api/profile/get_transcript', 'Volunteer\HomeCtrl@getTranscript');
    Route::get('/alert/approve/{id?}/{group_id?}/{status?}', 'Volunteer\TrackingCtrl@approveGroupRequest');
    Route::post('check-private-group-pascode', [
        'as' => 'check.private.group.pascode',
        'uses' => 'SharingCtrl@checkPrivateGroupPascode'
    ]);

    Route::post('api/check-private-group', 'UserSettingController@checkGroupPasscode');
    Route::get('api/set-intial-login', 'HomeController@setIntialLogin');
    Route::post('api/organization/validated-group', 'Organization\GroupCtrl@validatedGroup');
});

Route::get('/sharegroup/{id}', 'SharingCtrl@shareGroup')->name('share.group');
Route::post('/sharegroup/send-emails', 'SharingCtrl@sendShareEmails')->name('share.group.send.emails');

Route::get('/search', 'Volunteer\OpportunityCtrl@search');
Route::get('/update', 'Volunteer\OpportunityCtrl@update');


Route::get('/organization/share_opportunity/{id?}', 'Organization\OpportunityCtrl@shareOpportunity')->name('share_opportunity_view');

Route::get('/sign-up/{user_type?}', 'UserCtrl@signUp')->name('signUp');
Route::get('/sign-in', 'UserCtrl@signIn')->name('signIn');
Route::get('/forget-password', 'UserCtrl@viewforgetpassword')->name('forgot-password.view');

Route::get('/organization/group-member/{groupId}', 'Volunteer\GroupCtrl@viewGroupMember')->name('organization-group-member');
Route::get('/organization/group-impact/{groupId}', 'Volunteer\GroupCtrl@viewGroupImpact')->name('organization-group-impact');

Route::get('/thank-you/{message?}', function ($message=null) {
    return view('thank-you',['showMsg'=>$message]);
})->name('thankYou');
Route::get('/sign-up-affiliate/{id}', 'UserCtrl@signUpAffiliate');
Route::get('/organization/export-members-hours', 'Organization\OpportunityCtrl@memberHours');
Route::get('/organization/export-members-type', 'Organization\OpportunityCtrl@volunteerHoursByType');
Route::get('/organization/export-members-age', 'Organization\OpportunityCtrl@volunteersByAgeGroup');
Route::get('/affiliate-landing-page/{id}', 'UserCtrl@affiliateLandingPage');
Route::post('/volunteer/join-group', 'UserCtrl@addGroupInvitation');
Route::get('/organization/survey_opportunity/{id?}', 'Organization\OpportunityCtrl@surveyOpportunity');
Route::get('/organization/post-survey-opportunity/{id?}', 'Organization\OpportunityCtrl@addSurvey')->name('organization-opportunity-survey-add');
Route::get('/organization/result-survey-opportunity', 'Organization\OpportunityCtrl@resultSurvey')->name('organization-opportunity-survey-result');






