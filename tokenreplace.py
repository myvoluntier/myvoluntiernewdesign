import os
import sys

inputfilename = sys.argv[1] 
fRead = open(inputfilename, 'r') 
tokenized = fRead.read()
original = tokenized
print tokenized

for k, v in os.environ.items():
    token = "TOKEN_%s" % (k)
    #print token
    tokenized = tokenized.replace(token,v) 
    #print "%s=%s" % (k, v)

fBackup = open(inputfilename + ".bkup" , "w")
fBackup.write(original)

fUpdate = open(inputfilename, "w")
fUpdate.write(tokenized)

print tokenized
