<?php

use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertRecordIntoEmailTempelatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailsTemplate::updateOrCreate(['type' => 'unlistedOpportunityOrganization'],[
            'title' => 'Volunteer has tracked hours',
            'body' => '<h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><img src="https://i.imgur.com/7sdRh9G.png" width="112"><br></h2><div><h2 style="text-align: center; " times="" new="" roman";="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" center;"="">Hi,<font size="5">&nbsp;</font>{{oppor_name}}</h2></div><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">The Volunteer has tracked hours against your opportunity "{{oppor_name}}" but is not yet listed in the system.&nbsp;</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">Please signup as an organization by using this email {{org_mail}} by clicking here.</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">{{link}}</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><br></p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">Thanks</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">support@myvoluntier.com</p>',
            'text_body' => 'The Volunteer has tracked hours against a volunteer opportunity that belongs to the Organization but is not yet listed in the system',
            'type' => 'unlistedOpportunityOrganization',
            'table_obj' => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
