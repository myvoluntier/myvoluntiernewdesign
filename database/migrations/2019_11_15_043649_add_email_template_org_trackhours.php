<?php
use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailTemplateOrgTrackhours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailsTemplate::updateOrCreate(['type' => 'organizationUpdateTrackHours'],[
            'title' => 'Organization Update Track Hours',
            'body' => '<h2 times="" new="" roman";="" text-align:="" center;"="" style="color: rgb(0, 0, 0); text-align: center;"><img src="http://myvoluntier.com/public/front-end/img/logo.jpg" alt="" align="none"><br></h2><h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><div style="color: rgb(51, 51, 51); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px;"><span style="font-weight: 700;"><font size="3">{{userTbl->org_name}}  edit to {{logged_hours}} on "{{opp_name}}"</font></span><br></div></h2><h2 times="" new="" roman";="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" center;"="" style="color: rgb(51, 51, 51); text-align: center;"><br></h2><h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><div style="color: rgb(51, 51, 51); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px; text-align: start;"><div style="text-align: center;">Opportunity Type:&nbsp; {{opp_type}}</div><div style="text-align: center;">Opportunity Name:&nbsp; {{opp_name}}</div><div style="text-align: center;">Logged Hours:&nbsp; {{logged_hours}}</div><div style="text-align: center;">Logged Date:&nbsp; {{logged_date}}</div><div style="text-align: center;"><br></div><div style="text-align: center;"><br></div><div style="text-align: center;">Thanks</div><div style="text-align: center;">support@myvoluntier.com</div></div></h2>',
            'text_body' => '{{userTbl->org_name}} changed to {{logged_hours}} on "{{opp_name}}" {{link}}',
            'type' => 'organizationUpdateTrackHours',
            'table_obj' => 'userTbl,opprTbl  i.e  {{opprTbl->id}} for the id column',
        ]);
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
