<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsSettingsColumnsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->boolean('chat_msg_text')->after('status')->default(0)->comment('Disable Chat Message Text 1 yes/no');
            $table->boolean('chat_msg_email')->after('status')->default(0)->comment('Disable Chat Message  Emails yes/no');
            $table->boolean('track_hour_text')->after('status')->default(0)->comment('Disable Track Hour Text yes/no');
            $table->boolean('track_hour_email')->after('status')->default(0)->comment('Disable Track Hour Emails yes/no');
            $table->boolean('friend_req_text')->after('status')->default(0)->comment('Disable Friend Request Emails yes/no');
            $table->boolean('friend_req_email')->after('status')->default(0)->comment('Disable Friend Text yes/no');
            $table->boolean('join_oppr_text')->after('status')->default(0)->comment('Disable Join Opportunity Request Text yes/no');
            $table->boolean('join_oppr_email')->after('status')->default(0)->comment('Disable Join Opportunity Request Emails yes/no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('join_oppr_email');
            $table->dropColumn('join_oppr_text');
            $table->dropColumn('friend_req_email');
            $table->dropColumn('friend_req_text');
            $table->dropColumn('track_hour_email');
            $table->dropColumn('track_hour_text');
            $table->dropColumn('chat_msg_email');
            $table->dropColumn('chat_msg_text');
        });
    }
}
