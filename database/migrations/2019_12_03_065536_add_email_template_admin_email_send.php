<?php
use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailTemplateAdminEmailSend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailsTemplate::updateOrCreate(['type' => 'adminEmailSendTemplate'],[
            'title' => 'Admin Email Send To Organization',
            'body' => '<h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><img src="https://i.imgur.com/7sdRh9G.png" width="112"><br></h2><div><h2 style="text-align: center; " times="" new="" roman";="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" center;"=""><div style="font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;"=""><font size="4">Hi,&nbsp;<font color="#000000">{{userTbl-&gt;org_name}}&nbsp;&nbsp;</font></div><div style="font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;"=""><font color="#000000"><br></font></div><div style="font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;"=""><span style="color: rgb(0, 0, 0);"><br></span></div><div style="font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;"=""><font size="5">{{content}}</font></div><div style="font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;"=""><br></div><div style="font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;="" text-align:="" start;"=""><div style="text-align: center;"><div><br></div><div><br></div><div><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><font size="4"><font color="#000000">Sender Name :&nbsp;</font>{{sender_name}}</font></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><font size="4"><font color="#000000">Sender Email:&nbsp;</font>{{sender_email}}</font></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><br></p></div></div></div></h2></div><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">Thanks</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">contact@myvoluntier.com</p>',
            'text_body' => '',
            'type' => 'adminEmailSendTemplate',
            'table_obj' => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
