<?php

use App\Group_category as GroupCategory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupCategoryTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::dropIfExists('group_categories');
        Schema::create('group_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });

        GroupCategory::updateOrCreate(['name' => 'Class'], ['name' => 'Class',
            'is_deleted' => '0'
        ]);
        GroupCategory::updateOrCreate(['name' => 'Organization/Club'], ['name' => 'Organization/Club',
            'is_deleted' => '0'
        ]);
        GroupCategory::updateOrCreate(['name' => 'Other'], ['name' => 'Other',
            'is_deleted' => '0'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('group_categories');
    }

}
