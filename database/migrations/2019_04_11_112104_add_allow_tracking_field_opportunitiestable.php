<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllowTrackingFieldOpportunitiestable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::statement("ALTER TABLE `opportunities` ADD `allow_tracking` TINYINT(1) NOT NULL DEFAULT '1' COMMENT '0-no,1-yes' AFTER `auto_accept`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `opportunities` DROP `allow_tracking`;");
    }
}
