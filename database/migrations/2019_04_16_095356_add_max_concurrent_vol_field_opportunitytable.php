<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxConcurrentVolFieldOpportunitytable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `opportunities` ADD `max_concurrent_vol` VARCHAR(10) NOT NULL DEFAULT 'null' AFTER `allow_tracking`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `opportunities` DROP `max_concurrent_vol`;");
    }
}
