<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceProjectHourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::dropIfExists('service_project_hours');
        Schema::create('service_project_hours', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('service_project_id');
			$table->integer('orgnization_id')->nullable();
			$table->integer('oppor_id')->nullable();
			$table->integer('track_hours_id')->nullable();
			$table->string('hours',10)->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_project_hours');
    }
}
