<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlterEmailsUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `users` ADD `email2` VARCHAR(200) NULL DEFAULT NULL AFTER `confirm_code`, ADD `confirm_code2` VARCHAR(100) NULL DEFAULT NULL AFTER `email2`, ADD `email3` VARCHAR(200) NULL DEFAULT NULL AFTER `confirm_code2`, ADD `confirm_code3` VARCHAR(100) NULL DEFAULT NULL AFTER `email3`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `users`
                DROP `email2`,
                DROP `confirm_code2`,
                DROP `email3`,
                DROP `confirm_code3`;"
        );
    }
}
