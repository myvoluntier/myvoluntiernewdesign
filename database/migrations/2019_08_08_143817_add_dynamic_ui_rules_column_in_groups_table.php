<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDynamicUiRulesColumnInGroupsTable extends Migration
{

    public function up()
    {
        Schema::table('groups', function(Blueprint $table) {
            $table->text('dynamic_ui_rules')->nullable()->after('private_passcode');
            $table->string('applicable_domains')->nullable()->after('private_passcode');
            $table->integer('is_public')->comment('1=public,0=private	,2=dynamic')->change();

            //todo: need to update the name of this column to make group type.
        });
    }

    public function down()
    {
        Schema::table('groups', function($table) {
            $table->dropColumn('dynamic_ui_rules');
            $table->dropColumn('applicable_domains');
        });
    }
}
