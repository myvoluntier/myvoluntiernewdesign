<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrivatePasscodeInOpportunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opportunities', function(Blueprint $table) {
            $table->text('private_passcode')->after('auto_accept')->comment('only if auto_accept true')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opportunities', function(Blueprint $table) {
            $table->dropColumn('private_passcode');
        });
    }
}
