<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDelegatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('delegates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('org_id');
            $table->string('first_name', 150)->nullable();
            $table->string('last_name', 150)->nullable();
            $table->string('email', 150)->nullable();
            $table->string('password', 255)->nullable();

            $table->string('confirm_code', 120)->nullable();
            $table->string('remember_token', 255)->nullabl();
            $table->text('comments')->nullabl();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delegates');
    }
}
