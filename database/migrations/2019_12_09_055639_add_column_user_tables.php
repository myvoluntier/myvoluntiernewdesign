<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `users` ADD `approval_org` VARCHAR(10) NULL DEFAULT NULL AFTER `temp_last_name`, ADD `proof_of_identity_org` VARCHAR(50) NULL DEFAULT NULL AFTER `approval_org`, ADD `temp_org_name` VARCHAR(100) NULL DEFAULT NULL AFTER `proof_of_identity_org`;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `users`
              DROP `approval_org`,
              DROP `proof_of_identity_org`,
                DROP `temp_org_name`;");
        
    }
}
