<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreatedAndUpdatedColumnToSelectedCat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('selected_category', function($table) {
            $table->integer('updated_by')->after('status');
            $table->integer('created_by')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('selected_category', function($table) {
            $table->dropColumn('updated_by');
            $table->dropColumn('created_by');
        });

    }
}