<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEndedDateInUnlistedTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unlist_track_org_hours', function(Blueprint $table) {
            $table->string('end_date' ,20)->after('oppor_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unlist_track_org_hours', function($table) {
            $table->dropColumn('end_date');
        });
    }
}