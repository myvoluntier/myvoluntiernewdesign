<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryFieldInSelectedCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //remove all record from selected_category
		DB::statement('TRUNCATE `selected_category`;');
		$opp_array = array();
		$selected_oppor = DB::table('selected_category')->select('opportunities_id')->distinct('opportunities_id')->get();
		if(!empty($selected_oppor))
		{   
			foreach ($selected_oppor as $key => $cat) {
				
				array_push($opp_array, $cat->opportunities_id);
			}
		}
		$opportunityCategory = DB::table('opportunities')->select('id','category_id', 'is_deleted')->where('is_deleted',0)->whereNotIn('id', $opp_array)->get();
		foreach($opportunityCategory as $key=>$val){
			// Insert some stuff
			$catId = ($val->category_id != 0)? $val->category_id : 7;
			DB::table('selected_category')->insert(
				array(
					'opportunities_id' => $val->id,
					'category_id' => $catId,
				)
			);
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
