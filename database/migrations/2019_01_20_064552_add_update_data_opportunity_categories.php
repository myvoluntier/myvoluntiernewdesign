<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdateDataOpportunityCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Insert some stuff
		DB::table('opportunity_categories')->insert(
			array(
				'id' => 28,
				'name' => 'Government'
			)
		);
		
		// Update some stuff
		DB::table('opportunity_categories')->where(['name' => 'Education & Literacy'])->update(['name' => 'Education']);
		DB::table('opportunity_categories')->where(['name' => 'Housing & Homelessness'])->update(['name' => 'Housing']);
		DB::table('opportunity_categories')->where(['name' => 'Human Rights & Advocacy'])->update(['name' => 'Human Rights']);
		DB::table('opportunity_categories')->where(['name' => 'Children & Youth'])->update(['name' => 'Youth']);
		DB::table('opportunity_categories')->where(['name' => 'Emergency & Safety'])->update(['name' => 'Emergency Support']);
		DB::table('opportunity_categories')->where(['name' => 'Health & Medicine'])->update(['name' => 'Health']);
		DB::table('opportunity_categories')->where(['name' => 'Legal & Injustices'])->update(['name' => 'Legal']);
		
		// Update is_deleted to true
		DB::table('opportunity_categories')->where(['name' => 'Politics'])->update(['is_deleted' => '1']);
		DB::table('opportunity_categories')->where(['name' => 'International'])->update(['is_deleted' => '1']);
		DB::table('opportunity_categories')->where(['name' => 'Community'])->update(['is_deleted' => '1']);
		DB::table('opportunity_categories')->where(['name' => 'Sports & Recreation'])->update(['is_deleted' => '1']);
		DB::table('opportunity_categories')->where(['name' => 'Disaster Relief'])->update(['is_deleted' => '1']);
		DB::table('opportunity_categories')->where(['name' => 'Crisis Support'])->update(['is_deleted' => '1']);
		DB::table('opportunity_categories')->where(['name' => 'Arts & Culture'])->update(['is_deleted' => '1']);
		DB::table('opportunity_categories')->where(['name' => 'Employment'])->update(['is_deleted' => '1']);
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
