<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributelookupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('attributelookup');
        Schema::create('attributelookup', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attributekey',100)->nullable();
            $table->string('attributename',100)->nullable();
            $table->text('attributedescr')->nullable();
            $table->string('is_deleted', 2)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributelookup');
    }
}
