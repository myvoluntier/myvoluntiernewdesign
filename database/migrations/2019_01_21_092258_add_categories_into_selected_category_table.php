<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoriesIntoSelectedCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::statement('ALTER TABLE `selected_category` CHANGE `created_by` `created_by` INT(11) NULL, CHANGE `updated_by` `updated_by` INT(11) NULL;');
		
		$opp_array = array();
		$selected_oppor = DB::table('selected_category')->select('opportunities_id')->distinct('opportunities_id')->get();
		if(!empty($selected_oppor))
		{   
			foreach ($selected_oppor as $key => $cat) {
				
				array_push($opp_array, $cat->opportunities_id);
			}
		}
		$opportunityCategory = DB::table('opportunities')->select('id','type', 'is_deleted')->where('type','<>',0)->where('is_deleted',0)->whereNotIn('id', $opp_array)->get();
		foreach($opportunityCategory as $key=>$val){
			// Insert some stuff
			DB::table('selected_category')->insert(
				array(
					'opportunities_id' => $val->id,
					'category_id' => $val->type,
				)
			);
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
