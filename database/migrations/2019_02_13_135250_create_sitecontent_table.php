<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitecontentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		//Please below the below comment when you first time migration.
		
        Schema::create('sitecontents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('key')->nullable();
            $table->string('type')->nullable();
            $table->text('html_body')->nullable();
            $table->timestamps();
        });
		
		 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('sitecontents');
    }
}
