<?php
use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAlteremailConfirmEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailsTemplate::updateOrCreate(['type' => 'confirmAlternateEmail'],[
            'title' => 'Confirm Alternate Email',
            'body' => '<h2 times="" new="" roman";="" text-align:="" center;"="" style="color: rgb(0, 0, 0); text-align: center;"><img src="http://myvoluntier.com/public/front-end/img/logo.jpg" alt="" align="none" open="" sans",="" sans-serif;="" font-size:="" 14px;"="" style="color: rgb(51, 51, 51);"><br></h2><div><h2 times="" new="" roman";="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" center;"="" style="text-align: center;">Welcome to MyVoluntier.com</h2></div><div style="text-align: center;"><br></div><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">Please verify your alternate email address by clicking the link below.<br></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">{{link}}</p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);"><br></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">Thanks</p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">support@myvoluntier.com</p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);"><br></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">For a quick video tutorial visit: http://shorturl.at/ceI38<br></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center;"><a href="http://shorturl.at/ceI38" title="" target="_blank"><img src="https://i.imgur.com/olbnIzW.png" alt="" align="none"></a></p>',
            'text_body' => '',
            'type' => 'confirmAlternateEmail',
            'table_obj' => '(userTbl ) use table name with column i.e userTbl->id to show user id',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
