<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablsForSyncVolunteersUsersData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_syncs', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('batch_id')->comment('use unix timestamp as batch');
            $table->integer('org_id');
            $table->string('email');
            $table->string('first_name');
            $table->string('last_name');
            $table->date('created_date');
            $table->string('response_code')->comment('update,doesnotexist,removed.');
            $table->timestamps();
        });

        Schema::create('user_sync_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_sync_id');
            $table->integer('volunteer_id');
            $table->integer('org_id');
            $table->string('attribute_key');
            $table->string('attribute_value');
            $table->timestamps();
        });

        Schema::create('api_access', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('org_id');
            $table->boolean('is_allow_user_sync_attr');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_syncs');
        Schema::dropIfExists('user_sync_attributes');
        Schema::dropIfExists('api_access');
    }
}
