<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\EmailsTemplate;

class InsertRecordIntoEmailtemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         EmailsTemplate::updateOrCreate(['type' => 'shareServiceProject'],[
            'title' => 'MyVoluntier Share Service Project',
            'body' => '<h2 times="" new="" roman";="" text-align:="" center;"="" style="color: rgb(0, 0, 0); text-align: center;"><img src="https://i.imgur.com/7sdRh9G.png" width="112"><br></h2><h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><div style="color: rgb(51, 51, 51); font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;"=""><span style="font-weight: 700;"><font size="3">{{userTbl-&gt;first_name}} {{userTbl-&gt;last_name}} shared his/her service project on MyVoluntier.com</font></span><br></div><div style="color: rgb(51, 51, 51); font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;="" text-align:="" start;"=""><br></div><div style="color: rgb(51, 51, 51); font-family: " open="" sans",="" sans-serif;="" font-size:="" 14px;="" text-align:="" start;"=""><div style="text-align: center;"><div><div style="font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px; text-align: start;"><div style="text-align: center;"><font size="3">{{content}}</font></div><div style="text-align: center;"><br></div><div style="text-align: center;"><div>Click below link to visit the&nbsp;<span style="font-weight: 700;">Shared Service Project Page</span><br></div><div><span style="font-weight: 700;"><br></span></div><div>{{link}}<br></div><div><br></div><div><br></div><div><br></div></div></div><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px; color: rgb(0, 0, 0);">Thanks</p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="font-family: &quot;Open Sans&quot;, sans-serif; font-size: 14px; color: rgb(0, 0, 0);">support@myvoluntier.com</p></div></div></div></h2>',
            'text_body' => '{{userTbl->first_name}} {{userTbl->last_name}} shared their service project on MyVoluntier.com {{link}}',
            'type' => 'shareServiceProject',
            'table_obj' => '',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
