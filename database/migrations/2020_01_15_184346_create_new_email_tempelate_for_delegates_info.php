<?php

use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewEmailTempelateForDelegatesInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailsTemplate::updateOrCreate(['type' => 'delegateCreate'],[
            'title' => 'You have been assigned as a delegate on MyVoluntier',
            'body' => '<h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><img src="https://i.imgur.com/7sdRh9G.png" width="112"><br></h2><div><br></div><div><div style="text-align: center;"><br></div></div><div style="text-align: center;">Hi,&nbsp;<font color="#000000">{{name}} you are invited to participate in&nbsp;<a href="https://myvoluntier.com/" title="MyVoluntier.com" target="_blank">MyVoluntier.com</a></font></div><div style="text-align: center;"><font color="#000000"><br></font></div><div style="text-align: center;"><b>You can use the below credentials to participate in the site.</b></div><div><div style="text-align: center;"><div><br></div><div><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><font color="#000000">Email :&nbsp;</font>{{email}}</p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><font color="#000000">Password:&nbsp;</font>{{password}}<br></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><font color="#000000">Organization :&nbsp;</font>{{org_name}}<br></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><span style="color: rgb(0, 0, 0);"><br></span></p></div><div><br></div></div></div><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">Thanks</p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">support@myvoluntier.com</p>',
            'text_body' => 'You have been assigned as a delegate on MyVoluntier',
            'type' => 'delegateCreate',
            'table_obj' => 'userTbl',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
