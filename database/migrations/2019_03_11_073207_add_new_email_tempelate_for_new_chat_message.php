<?php

use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewEmailTempelateForNewChatMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailsTemplate::updateOrCreate(['type' => 'chatNotify'],[
            'title' => 'You have received chat message',
            'body' => '<h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><img src="https://i.imgur.com/7sdRh9G.png" width="112"><br></h2><div><br></div><div><div style="text-align: center;"><br></div></div><div style="text-align: center;">Hi,&nbsp;<font color="#000000">{{userTbl-&gt;first_name}} you received chat message.</font></div><div style="text-align: center;"><span style="color: rgb(0, 0, 0);"><br></span></div><div style="text-align: center; "><font size="5">{{message}}</font></div><div style="text-align: center; "><br></div><div style="text-align: center; "><br></div><div><div style="text-align: center;"><div><br></div><div><br></div><div><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><font color="#000000">Sender Name :&nbsp;</font>{{sender_name}}</p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><font color="#000000">Sender Email:&nbsp;</font>{{sender_email}}<br></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><span style="color: rgb(0, 0, 0);"><br></span></p></div><div><br></div></div></div><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">Thanks</p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center; color: rgb(0, 0, 0);">support@myvoluntier.com</p>',
            'text_body' => 'You have a new chat message via MyVoluntier.com',
            'type' => 'chatNotify',
            'table_obj' => 'userTbl',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
