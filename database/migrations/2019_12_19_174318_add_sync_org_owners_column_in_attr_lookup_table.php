<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSyncOrgOwnersColumnInAttrLookupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attributelookup', function(Blueprint $table) {
            $table->string('sync_org_owners')->nullable()->after('attributedescr');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attributelookup', function($table) {
            $table->dropColumn('sync_org_owners');
        });
    }
}
