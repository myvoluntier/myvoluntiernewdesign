<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDesignatedColumnsInTrackedHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('tracked_hours', function(Blueprint $table) {
            $table->integer('designated_group_id')->nullable()->after('link');
            $table->boolean('is_designated')->nullable()->after('link')->default(0);
        });

        Schema::table('unlist_track_org_hours', function(Blueprint $table) {
            $table->integer('designated_group_id')->nullable()->after('oppor_name');
            $table->boolean('is_designated')->nullable()->after('oppor_name')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tracked_hours', function($table) {
            $table->dropColumn('designated_group_id');
            $table->dropColumn('is_designated');
        });

        Schema::table('unlist_track_org_hours', function($table) {
            $table->dropColumn('designated_group_id');
            $table->dropColumn('is_designated');
        });
    }
}
