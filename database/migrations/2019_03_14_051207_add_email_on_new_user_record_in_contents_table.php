<?php

use App\Models\EmailsTemplate;
use App\Models\Sitecontent;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailOnNewUserRecordInContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Sitecontent::updateOrCreate(['type' => 'userRegistrationNotification'],[
            'title' => 'contact@myvoluntier.com',
            'html_body' => '<h6 style="text-align: center;"><font color="#000000" size="3"><br></font></h6><h6 style="text-align: center;"><font color="#000000" size="5">Update an email in the above box to send new user notification</font></h6><h6 style="text-align: center;"><font color="#000000" size="5"><br></font></h6><h6 style="text-align: center;"><font color="#000000" size="3">Also, can you add an additional notification that goes out to contact@myvoluntier.com whenever a new user (volunteer/org) registers on the website? Right now we have no way to know when a user is pending verification. If we don\'t check the "status" column on the admin panel we would never know.</font></h6><h6 style="text-align: center;"><font color="#000000" size="3"><br></font></h6><h6 style="text-align: center;"><font color="#000000" size="3">And make the email address that gets notified configurable...in case I want to change it to something other than contact@myvoluntier.com</font></h6>',
            'type' => 'userRegistrationNotification',
        ]);

        EmailsTemplate::updateOrCreate(['type' => 'userRegistrationNotification'],[
            'title' => 'New User Registered',
            'body' => '<h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><img src="https://i.imgur.com/7sdRh9G.png" width="112"><br></h2><div><h2 style="text-align: center; " times="" new="" roman";="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" center;"="">Hi, Admin</h2></div><div><br></div><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">The new user has been registered on&nbsp;myvoluntier.com.<br>Below are some details about that user<br><br></p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><br></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center;"><font color="#000000">First Name: {{userTbl-&gt;first_name}}</font></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center;"><font color="#000000">Last Name: {{userTbl-&gt;last_name}}</font></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center;"><font color="#000000">Email Address: {{userTbl-&gt;email}}</font></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center;"><font color="#000000">Phone Number: {{userTbl-&gt;contact_number}}</font></p><p times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="" style="text-align: center;"><font color="#000000"><br></font></p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">Thanks</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">support@myvoluntier.com</p>',
            'text_body' => 'No Phone Available',
            'table_obj' => '( userTbl ) use table name with any column i.e userTbl->id to show user id',
            'type' => 'userRegistrationNotification',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
