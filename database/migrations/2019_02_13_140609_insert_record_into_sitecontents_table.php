<?php

use App\Models\Sitecontent;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertRecordIntoSitecontentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Sitecontent::updateOrCreate(['type' => 'createOpportunityPopup'],
            ['title' => 'Create Opportunity Popup',
                'html_body' => '<h6 style="text-align: center; color: rgb(0, 0, 0);"><font size="3">It looks like you don\'t have any active Opportunities set up.&nbsp;<br></font><font size="3"><span style="color: rgb(51, 51, 51);">Would you like to create an Volunteer Opportunity now&nbsp; &nbsp;?</span></font></h6>',
                'type' => 'createOpportunityPopup'
            ]);

        Sitecontent::updateOrCreate(['type' => 'createOpportunityPopup'],[
            'title' => 'Create Opportunity Popup',
            'html_body' => '<h6 style="text-align: center; color: rgb(0, 0, 0);"><font size="3">It looks like you don\'t have any active Opportunities set up.&nbsp;<br></font><font size="3"><span style="color: rgb(51, 51, 51);">Would you like to create an Volunteer Opportunity now&nbsp; &nbsp;?</span></font></h6>',
            'type' => 'createOpportunityPopup',
        ]);

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
