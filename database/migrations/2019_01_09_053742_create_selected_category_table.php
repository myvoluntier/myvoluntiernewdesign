<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelectedCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('selected_category');

        Schema::create('selected_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('opportunities_id');
            $table->integer('category_id');
            $table->boolean('status')->default(1)->comment("category : 1 = active, 0 = deactive");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}