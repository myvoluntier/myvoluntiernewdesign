<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAffiliatedStatusGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `groups` ADD `affiliated_org_id` INT(10) NULL DEFAULT NULL AFTER `status`;");
        DB::statement("ALTER TABLE `groups` ADD `affiliated_status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0-inactive,1-active' AFTER `status`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //ALTER TABLE `groups`
        DB::statement("DROP `affiliated_org_id`,DROP `affiliated_status`;");
  
    }
}
