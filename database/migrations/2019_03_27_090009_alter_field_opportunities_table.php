<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFieldOpportunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `opportunities` CHANGE `description` `description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `opportunities` CHANGE `additional_info` `additional_info` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `opportunities` CHANGE `activity` `activity` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `opportunities` CHANGE `qualification` `qualification` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `opportunities` CHANGE `description` `description` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
                 CHANGE `additional_info` `additional_info` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL');
        DB::statement('ALTER TABLE `opportunities` CHANGE `activity` `activity` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, 
                 CHANGE `qualification` `qualification` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL');
    }
}
