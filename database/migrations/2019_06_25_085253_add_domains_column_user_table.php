<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDomainsColumnUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `users` ADD `domain_1` VARCHAR(100) NULL DEFAULT NULL AFTER `confirm_code3`, ADD `domain_1_confirm_code` VARCHAR(100) NULL DEFAULT NULL AFTER `domain_1`, ADD `domain_2` VARCHAR(100) NULL DEFAULT NULL AFTER `domain_1_confirm_code`, ADD `domain_2_confirm_code` VARCHAR(100) NULL DEFAULT NULL AFTER `domain_2`;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `users`
            DROP `domain_1`,
            DROP `domain_1_confirm_code`,
            DROP `domain_2`,
            DROP `domain_2_confirm_code`;"
        );
    }
}
