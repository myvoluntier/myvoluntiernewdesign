<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnlistTrackOrgHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('unlist_track_org_hours');

        Schema::create('unlist_track_org_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('volunteer_id');
            $table->string('oppor_name', 255);
            $table->string('org_name', 255);
            $table->string('org_email', 255);
            $table->string('logged_date' ,20)->nullable();
            $table->string('started_time', 45)->nullable();
            $table->string('ended_time', 45)->nullable();
            $table->integer('logged_mins')->nullable();
            $table->text('description')->nullable();
            $table->text('selectd_timeblocks')->nullable();
            $table->string('confirm_code' ,50);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unlist_track_org_hours');

    }
}
