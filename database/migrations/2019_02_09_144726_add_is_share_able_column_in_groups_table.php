<?php

use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsShareAbleColumnInGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function(Blueprint $table) {
            $table->boolean('is_share_able')->after('is_public')->default(0);
        });

        EmailsTemplate::updateOrCreate(['type' => 'sharedGroupLink'],[
            'title' => 'Someone Shared Group',
            'body' => '<h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><img src="https://i.imgur.com/7sdRh9G.png" width="112"><br></h2><div><h2 style="text-align: center; " times="" new="" roman";="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" center;"="">Someone share group link via MyVoluntier.com</h2></div><div style="text-align: center; "><br></div><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">Please login or signup to see more detail of the group.<br></p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">{{link}}</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><br></p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">Thanks</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">support@myvoluntier.com</p>',
            'text_body' => 'no phone available',
            'type' => 'sharedGroupLink',
            'table_obj' => 'n/a',
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function($table) {
            $table->dropColumn('is_share_able');
        });
    }
}
