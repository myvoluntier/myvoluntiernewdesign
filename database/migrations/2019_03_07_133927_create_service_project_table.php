<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::dropIfExists('service_projects');
        Schema::create('service_projects', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('volunteer_id');
            $table->string('title',100)->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->text('description')->nullable();
			$table->text('outcome')->nullable();
			$table->string('total_hours',10)->default(0);
			$table->string('number_opp',10)->default(0);
			$table->tinyInteger('is_deleted')->default(0);
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_projects');
    }
}
