<?php

use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertRecordIntoEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailsTemplate::updateOrCreate(['type' => 'userStatusUpdate'],[
            'title' => 'MyVoluntier Status Updated',
            'body' => '<h2 style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" text-align:="" center;"=""><img src="https://i.imgur.com/7sdRh9G.png" width="112"><br></h2><div><h2 style="text-align: center; " times="" new="" roman";="" color:="" rgb(0,="" 0,="" 0);="" text-align:="" center;"="">Hi,<font size="5">&nbsp;</font>{{name}}</h2></div><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">Your status has been updated to&nbsp; {{status}}.&nbsp;</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"=""><br></p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">Thanks</p><p style="text-align: center; color: rgb(0, 0, 0);" times="" new="" roman";="" font-size:="" medium;="" text-align:="" center;"="">support@myvoluntier.com</p>',
            'text_body' => 'Your status has been updated to {{status}}.',
            'type' => 'userStatusUpdate',
            'table_obj' => '( userTbl ) use table name with any column i.e userTbl->id to show user id',
        ]);

        //only 1 time
        \App\User::where(['is_deleted' => '0' ,'confirm_code' => '1'])->update(['status' => '1']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
