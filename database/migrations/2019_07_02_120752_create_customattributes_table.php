<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomattributesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::dropIfExists('customattributes');
        Schema::create('customattributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid');
            $table->string('attributekey', 100)->nullable();
            $table->string('attributevalue', 100)->nullable();
            $table->string('is_deleted', 2)->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('customattributes');
    }

}
