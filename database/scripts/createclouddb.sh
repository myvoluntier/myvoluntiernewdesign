#!/bin/bash

echo "arguments - project=${1} instance=${2} tier=${3} dbrootpassword=${4} dbuserpassword=${5}"
project=$1
instance=$2
tier=$3
DB_ROOT_PASSWORD=$4
DB_USER_PASSWORD=$5

INSTANCES=`gcloud sql --project="$project" instances list`
if [[ $INSTANCES = *"$instance"* ]]; then
  echo "INSTANCES It's there!"
else
  echo "INSTANCES It's not there!"
  gcloud sql --project=$project instances create $instance --tier=$tier --region=us-central1
fi

wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
  chmod +x cloud_sql_proxy
  echo "starting cloud proxy"
  ./cloud_sql_proxy -instances=${project}:us-central1:${instance}=tcp:3306 &
  echo "sleeping for 10 seconds"
  sleep 10s
  


DATABASES=`gcloud sql --project="$project" databases list --instance="$instance"`
  echo $DATABASES
if [[ $DATABASES = *"myvoluntier"* ]]; then
  echo "DATABASES It's there!"
else
   echo "DATABASES It's not there! Creating myvoluntier"   
   gcloud sql --project=$project users set-password root % --instance=$instance --password=$DB_ROOT_PASSWORD
   gcloud sql --project=$project users create voluntier_user --instance=$instance --password=$DB_USER_PASSWORD --host=%
   gcloud sql --project=$project databases create myvoluntier --instance=$instance 
  echo "db was created. running sql scripts"
  #wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy
  #chmod +x cloud_sql_proxy
  #echo "starting cloud proxy"
  #./cloud_sql_proxy -instances=${project}:us-central1:${instance}=tcp:3306 &
  #echo "sleeping for 10 seconds"
  #sleep 10s
  echo "running sql scripts - CREATE TABLES"
  mysql --host 127.0.0.1 -u "voluntier_user" "-p${DB_USER_PASSWORD}" "myvoluntier" < "database/scripts/createtables.sql"
  echo "running sql scripts - LOAD DATA"
  mysql --host 127.0.0.1 -u "voluntier_user" "-p${DB_USER_PASSWORD}" "myvoluntier" < "database/scripts/loadtestdata.sql"
fi

mysql --host 127.0.0.1 -u "voluntier_user" "-p${DB_USER_PASSWORD}" "myvoluntier" -e "CREATE TABLE IF NOT EXISTS sessions (id text, 
user_id int, ip_address varchar(45), user_agent text, payload text, last_activity int);"