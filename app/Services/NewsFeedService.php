<?php

namespace App\Services;

use App\Opportunity_member;
use App\Group_member;
use App\Opportunity;
use App\Tracking;
use App\Group;
use App\User;
use Illuminate\Support\Facades\Auth;


class NewsFeedService
{
    public function transformNewsFeedToArrayInfo($newsFeeds, $typeUser)
    {
        $fieldFeedNewsArr = $typeUser === 'organization' ? 'created_at' : 'who_joined_imag';
        $users = [];
        $feedNewsArr = [];
        foreach ($newsFeeds as $key => $value) {
            if (array_key_exists($value->who_joined, $users)) {
                $userWhoJoined = $users[$value->who_joined];
            } else {
                $userWhoJoined = $value->user;
                $users[$value->who_joined] = $userWhoJoined;
            }
            if(is_null($userWhoJoined)) continue;
            $first_name = $userWhoJoined->first_name ? $userWhoJoined->first_name : '';
            $last_name = $userWhoJoined->last_name ? $userWhoJoined->last_name : '';
            $user_role = $userWhoJoined->user_role;
            if ($user_role == 'organization') {
                $uname = $userWhoJoined->org_name;
                $logoDef = 'front-end/img/org/001.png';
                $userurl = 'organization/profile/' . $value->who_joined;
            } elseif ($user_role == 'volunteer') {
                $uname = $first_name . ' ' . $last_name;
                $logoDef = 'img/noprofilepic.png';
                $userurl = 'volunteer/profile/' . $value->who_joined;
            }

            if ($typeUser === 'organization') {
                $created_at = $value->created_at ? $value->created_at->format(config('app.date_time_format_default')) : '';
            } else {

                $created_at = $value->created_at ? $value->created_at->format(config('app.date_time_format_default')) : '';
                $logo_img = $userWhoJoined->logo_img ? $userWhoJoined->logo_img : $logoDef;
            }
            
            $name = '';
            $volName='';
            $utl = 'javascript:void(0)';
            if ($value['table_name'] == 'group_members') {
                $groupMember = Group_member::find($value['table_id']);
                if($groupMember){
                    $group_id = $groupMember->group_id;
                    $name = Group::find($group_id)->name;
                    $utl = 'sharegroup/' . base64_encode($group_id);
                }

            } else if ($value['table_name'] == 'opportunity_members') {
               
                $opporObj = Opportunity_member::find($value['table_id']);
                if(!empty($opporObj)){
                    $oppor_id = $opporObj->oppor_id;
                    $name = Opportunity::find($oppor_id)->title;
                    if(Auth::user()->user_role == 'organization'){
                        $utl = 'organization/view_opportunity/' . $oppor_id;
                    }else{
                        $utl = 'volunteer/view_opportunity/' . $oppor_id;
                    }
                }

            } else if ($value['table_name'] == 'opportunities') {
                $oppObj = Opportunity::find($value['table_id']);                
                $utl = '';
                if(!empty($oppObj)){
                    $name = $oppObj->title;
                    if(Auth::user()->user_role == 'organization'){
                        $utl = 'organization/view_opportunity/' . $value['table_id'];
                    }else{
                        $utl = 'volunteer/view_opportunity/' . $value['table_id'];
                    }
                }

            } else if ($value['table_name'] == 'tracked_hours') {
                $tracking_hour = Tracking::find($value['table_id']);
                if(isset($tracking_hour->volunteer_id)){                   
                   
                $userName=User::select('first_name','last_name')->where('id',$tracking_hour->volunteer_id)->first(); 
                $volName=$userName->first_name.' '.$userName->last_name."'s";          
                }else{
                    $volName="";
                }

                $name = $tracking_hour ? $tracking_hour->oppor_name : '';
                $link = $tracking_hour && $tracking_hour->opportunity ? $tracking_hour->opportunity->id : '';          
                if(Auth::user()->user_role == 'organization') {
                    $utl = 'organization/view_opportunity/' . $link;

                }else{
                    $utl = 'volunteer/view_opportunity/' . $link;
                }
            }
           

            $feedNewsArr[] = [
                'who_joined' => $uname,
                'reason' => $value['reason'],
                'name' => $name,
                'tablename'=>$value['table_name'],
                'utl' => $utl,
                'volName'=> $volName,
                'userurl' => $userurl,
                'created_at' => $created_at,
                $fieldFeedNewsArr => $typeUser === 'organization' ? $created_at : $logo_img
            ];
           
        }
        if(sizeof($newsFeeds)>0)
            return $feedNewsArr;
        else
            return;
    }

}
