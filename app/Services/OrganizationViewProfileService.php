<?php

namespace App\Services;

use App\Follow;
use App\Friend;
use App\Group_member;
use App\Models\Delegate;
use App\Opportunity_member;
use App\Tracking;
use App\SelectedCategoryUserpref;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Opportunity;
use App\Organization_type;
use App\Opportunity_category;
use App\Customattributes;
use App\Models\Attributelookup; 
use App\Service_project;
use App\Partner;
class OrganizationViewProfileService
{
    public function viewProfile($id = null)
    {
        $user = User::find($id);
        $loggedInUser = Auth::user();

        if ($user->user_role == 'volunteer') {
            
            $logged_hours = Tracking::where('volunteer_id', $id)->where('is_deleted',
                '<>', 1)->where('approv_status', 1)->sum('logged_mins');

            $logged_hours = $logged_hours / 60;

            $today = date('Y-m-d');
            $my_opportunities = Opportunity_member::where('user_id', $id)->where('status', 1)->where('is_deleted',
                '<>', 1)->pluck('oppor_id')->toArray();
            $opportunities = Opportunity::whereIn('id', $my_opportunities)->where('type',
                1)->where('is_deleted', '<>', 1)->
            where('end_date', '>', $today)->get();

            $groupsQuery = DB::table('groups')->join('group_members', 'groups.id',
                '=', 'group_members.group_id')->where('group_members.user_id',
                $id)->where('group_members.is_deleted', '<>', 1)->where('group_members.status',
                Group_member::APPROVED)->where('groups.is_deleted', '<>', 1)
                ->select('groups.*','group_members.role_id');

            $groups = (clone $groupsQuery)->where('is_public' ,'<>',2)->get();
            $dynamicGroups = (clone $groupsQuery)->where('is_public' ,2)->get()->toArray();

            $friends = DB::table('users')->join('friends', 'users.id', '=',
                'friends.friend_id')->where('friends.user_id', $id)->where('friends.is_deleted', '<>', 1)->where('friends.status',
                2)->where('users.is_deleted', '<>', 1)->where('users.confirm_code',
                1)->select('users.*')->get();

            $profile_info = array();
            $profile_info['is_my_profile'] = 0;
            if ($id == $loggedInUser->id) {
                $profile_info['is_my_profile'] = 1;
            }
            $is_friend = Friend::where('user_id', $loggedInUser->id)->where('friend_id',
                $id)->where('is_deleted', '<>', 1)->get()->first();
            if ($is_friend == null) {
                $profile_info['is_friend'] = 0;
            } else {
                $profile_info['is_friend'] = $is_friend->status;
            }
            $profile_info['is_volunteer'] = 1;
            $profile_info['logged_hours'] = $logged_hours;

	        $deleted = true;
	        $chat_id = DB::table('user_chat')->where('user_id', $loggedInUser->id)->where('to_user_id', $id)->select('*')->first();
	        if($chat_id == null)
		        $chat_id = DB::table('user_chat')->where('user_id', $id)->where('to_user_id', $loggedInUser->id)->select('*')->first();
	        if($chat_id != null)
	        {
		        $deleted = false;
		        if($chat_id->status == 1 && $chat_id->user_id == $loggedInUser->id) $deleted = true;
		        if($chat_id->status == 2 && $chat_id->to_user_id == $loggedInUser->id) $deleted = true;
		        $chat_id = $chat_id->chat_id;
            }
            $comments= Tracking::with('user')->where([['volunteer_id','=',$id],['approv_status','=','1']])->orderBy('tracked_hours.created_at','DESC')->paginate(10);

            $oppr_types = Opportunity_category::all();
            $org_types = Organization_type::all();
            $sa_oppr_type = array();
            $sa_org_type = array();
            foreach ($oppr_types as $ot){
                $sa_oppr_type[$ot->id]['name'] = $ot->name;
            }
            foreach ($org_types as $or){
                $sa_org_type[$or->id]['name'] = $or->organization_type;
            }

            $my_orgs = Tracking::where('volunteer_id',$user->id)->where('is_deleted','<>',1)->where('approv_status','=',1)->groupBy('org_id')->pluck('org_id')->toArray();
            $org_info = User::whereIn('id', $my_orgs)->get();
            $type_script = array();
            $tracks = Tracking::where('volunteer_id',$user->id)->where('is_deleted','<>',1)->where('approv_status','=',1)->orderBy('logged_date','desc')->get()->groupBy('org_id')->toArray();

            foreach ($org_info as $info){
                $type_script[$info->id]['org_name'] = $info->org_name;
                $type_script[$info->id]['org_logo'] = $info->logo_img;
                $type_script[$info->id]['contact_number'] = $info->contact_number;
                $type_script[$info->id]['zipcode'] = $info->zipcode;
                $type_script[$info->id]['address'] = $info->city.', '.$info->state.' '.$info->zipcode.', '.$info->country;
                $type_script[$info->id]['email'] = $info->email;
                $type_script[$info->id]['primary_contact'] = '';
                $type_script[$info->id]['track_info'] = $tracks[$info->id];
            }

            $serviceProjects = Service_project::where('volunteer_id', $user->id)->where('is_deleted', '<>', 1)->get()->toArray();
            $customAttributes = Customattributes::where('userid', $user->id)->where('is_deleted', '<>', 1)->get()->toArray();
            $atrributelookups = Attributelookup::where('is_deleted', '<>', 1)->orderBy('attributename', 'ASC')->get();
            $opportunityCategory = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();
            $selectedCat = array();
            $all_categories = SelectedCategoryUserpref::where('user_id', $user->id)->get()->toArray();

            if (!empty($all_categories)) {
                foreach ($all_categories as $key => $cat) {
                    array_push($selectedCat, $cat['category_id']);
                }
            }
            
            return view('organization.profile',[
                'user' => $user,
                'profile_info' => $profile_info,
                'active_oppr' => $opportunities,
                'group' => $groups,
                'dynamic_group' => $dynamicGroups,
                'friend' => $friends,
                'page_name' => 'vol_profile',
                'comments' => $comments,
                'typeView' => 'onlyView',
                'atrributelookups' => $atrributelookups,
                'serviceProjects' => $serviceProjects,
                'customAttributes' => $customAttributes,
                'chat_id' => $chat_id,
                'chat_deleted' => $deleted,
                'oppr_types' => $sa_oppr_type,
                'org_types' => $sa_org_type,
                'tracks' => $type_script,
                'id' => $id,
                'opportunityCategory' => $opportunityCategory,
                'selectedCat' => $selectedCat,
             
            ]);
        } 

        else {
           $isUpdated = False;
          if(($user->invite_link==NULL)||($user->invite_link=="")){
             $invite_id=strtolower(str_replace(' ', '-', $user->org_name)).'-'.$user->id;
             $isUpdated = User::where('id','=',$user->id)->update(['invite_link' => $invite_id]);
          }
            if($isUpdated){
                $invite_link = $invite_id;
            }else{
                $invite_link = $user->invite_link;
            }
            $id = (isset($id) && !empty($id)) ? $id : $loggedInUser->id;
            $profile_info = array();
            $profile_info['is_volunteer']  = 0;
            $profile_info['is_my_profile'] = 1;

            if (empty(Auth::user()) || $id != $loggedInUser->id) {
                 $profile_info['is_my_profile'] = 0;
            }
            $is_followed = null;
            if(!empty(Auth::user())){
                $is_followed = Follow::where('follower_id', $id)->where('followed_id',$loggedInUser->id)->where('is_deleted', '<>', 1)->get()->first();
            }
            if ($is_followed == null) {
                $profile_info['is_followed'] = 0;
            } else {
                $profile_info['is_followed'] = 1;
            }
            $is_friend = null;
            if(!empty(Auth::user())){
                $is_friend = Friend::where('user_id', $id)->where('friend_id',$loggedInUser->id)->where('is_deleted', '<>', 1)->get()->first();
            }
            if ($is_friend == null) {
                $profile_info['is_friend'] = 0; //this is not good solution but a lot of things need to remove when this will removed here. no need of this now
                //there should not be any restrictions on an organizations display page. EVEN IF THEY ARE NOT "FRIENDS", when viewing the org page, a volunteer (or any other org) should be able to see the list of all the org's opportunities and ALL of the org's contact details etc
            } else {
                $profile_info['is_friend'] = $is_friend->status;
            }

            $tracks_hours                 = Tracking::where('org_id', $id)->where('approv_status',1)->where('is_deleted', '<>', 1)->sum('logged_mins');
            $profile_info['tracks_hours'] = $tracks_hours / 60;

            $today       = date("Y-m-d");
            $active_oppr = Opportunity::where('org_id', $id)->where('type',1)->where('is_deleted', '<>', '1')->
            where('end_date', '>=',$today)->orderBy('created_at', 'desc')->get();

            $groupsQuery = DB::table('groups')
                            ->join('group_members', 'groups.id','=', 'group_members.group_id')
                            ->join('group_categories', 'groups.group_category_id', '=', 'group_categories.id')
                            ->where('group_members.user_id',$id)
                            ->where('group_members.is_deleted', '<>', 1)
                            ->where('groups.is_deleted','<>', 1);

            $groups = (clone $groupsQuery)->where('groups.affiliated_org_id', null)->select('groups.*', 'group_members.role_id', 'group_categories.name as categoryName')->get();
            $dynamicGroups = [];

            $affiliatedGroups = DB::table('groups')
                                ->join('group_members', 'groups.id','=', 'group_members.group_id')
                                ->join('group_categories', 'groups.group_category_id', '=', 'group_categories.id')
                                ->join('users', 'groups.affiliated_org_id', '=', 'users.id')
                                ->where('group_members.user_id',$id)
                                ->where('group_members.is_deleted', '<>', 1)
                                ->where('groups.is_deleted','<>', 1)
                                ->where('groups.affiliated_org_id','<>', null)
                                ->where('groups.affiliated_status', 1)
                                ->select('groups.*', 'group_members.role_id', 'group_categories.name as categoryName', 'users.org_name as org_name')
                                ->get();

            $my_members = DB::table('opportunity_members')->where('opportunity_members.org_id',$id)->where('opportunity_members.is_deleted', '<>', 1)->
            join('users', 'opportunity_members.user_id', '=', 'users.id')->leftjoin('reviews', 'reviews.review_to', 'users.id')->select('*')->groupBy('opportunity_members.user_id')->get();
            $members    = array();

            foreach ($my_members as $m) {
                $members[$m->user_id] = $m;
            }

            $friends = DB::table('users')->join('friends', 'users.id', '=','friends.friend_id')->where('friends.user_id', $id)
            ->where('friends.is_deleted', '<>', 1)->where('friends.status',2)->where('users.is_deleted', '<>', 1)->where('users.confirm_code',1)->select('users.*')->get();

            $deleted = true;
	        $chat_id = DB::table('user_chat')->where('user_id', $id)->where('to_user_id', $id)->select('*')->first();

	        if($chat_id == null)
	            $chat_id = DB::table('user_chat')->where('user_id', $id)->where('to_user_id', $id)->select('*')->first();
	        if($chat_id != null)
	        {
		        $deleted = false;
		        if($chat_id->status == 1 && $chat_id->user_id == $id) $deleted = true;
		        if($chat_id->status == 2 && $chat_id->to_user_id == $id) $deleted = true;
		        $chat_id = $chat_id->chat_id;
            }

            $opp = DB::table('opportunities')->where('org_id',$id)->where('is_deleted', '<>', 1)->get();
            
            $subOrganization = User::select('id','org_name','user_name','email','contact_number','status')
                            ->where('parent_id',$id)->where('is_deleted', '<>', 1)
                            ->orderBy('id','DESC')
                            ->get()
                            ->toArray();

            $delegates = Delegate::select('id','org_id','first_name','last_name','email','comments')
                ->where('org_id',$id)
                ->orderBy('id','DESC')
                ->get();

            $viewPage = 'organization.profile';
            $authUser = true;
            if(empty(Auth::user())){
                $viewPage = 'non_auth_profile';
                $authUser = false;
            }elseif($profile_info['is_my_profile'] == 0){
                $authUser = false;
            }
            
            $partners = app('App\Http\Controllers\Organization\PartnerCtrl')->getPartners($id);
            $orgList = app('App\Http\Controllers\Organization\PartnerCtrl')->getOrgList($partners['selPartners']);
            return view($viewPage,
                [
                    'user' => $user,
                    'profile_info' => $profile_info,
                    'active_oppr' => $active_oppr,
                    'group' => $groups,
                    'dynamic_group' => $dynamicGroups,
                    'affiliatedGroups' => $affiliatedGroups,
                    'members' => $members,
                    'friend' => $friends,
                    'page_name' => 'org_profile',
                    'org_type_names' => Organization_type::all(),
                    'chat_id' => $chat_id,
                    'chat_deleted' => $deleted,
                    'opportunity' => $opp,
                    'id' => $id,
                    'subOrganization' => $subOrganization,
                    'orgDelegates' => $delegates,
                    'authUser' => $authUser,
                    'partners' => $partners['partners'],
                    'orgList' => $orgList,
                    'inviteLink'=>$invite_link
                ]);
            
        }
    }
}
