<?php
/**
 * Created by PhpStorm.
 * User: Amir.V
 * Date: 10.11.2019
 * Time: 14:24
 */

namespace App\Services;

use Aws\QuickSight\QuickSightClient;
use Illuminate\Support\Facades\Log;

class AmazonRequests
{
	public $clientObj;

	function __construct()
	{
        $this->clientObj  = new QuickSightClient(['region'=>'us-east-1','version' => 'latest']);
    }


    public static function testingCode(){

        //$clientSts = new StsClient([
//            'region' => 'us-east-1',
//            'version' => 'latest'
//        ]);
//
//        $roleToAssumeArn = 'arn:aws:iam::581081770241:role/myvoluntierdashboard';
//
//        $result = $clientSts->assumeRole([
//            'RoleArn' => $roleToAssumeArn,
//            'RoleSessionName' => 'amirDEV1@seersol.com'
//        ]);
//
//        $sessionIds = [
//            'sessionId'    => $result['Credentials']['AccessKeyId'],
//            'sessionKey' => $result['Credentials']['SecretAccessKey'],
//            'sessionToken'  => $result['Credentials']['SessionToken']
//        ];

//        $uri = 'https://signin.aws.amazon.com/federation?Action=getSigninToken&Session='.json_encode($sessionIds);


        // output AssumedRole credentials, you can use these credentials
        // to initiate a new AWS Service client with the IAM Role's permissions

//        $quickSiteClient = new  QuickSightClient([
//            'region'=>'us-east-1',
//            'version' => 'latest',
//            'credentials' =>  [
//                'key'    => $result['Credentials']['AccessKeyId'],
//                'secret' => $result['Credentials']['SecretAccessKey'],
//                'token'  => $result['Credentials']['SessionToken']
//            ]
//        ]);

    }

    public static function createAmazonUserIAM($clientQuickSight)
    {

        try {

            $email = auth()->user()->email;

            $response = $clientQuickSight->registerUser([
                'version' => 'latest', // REQUIRED
                'AwsAccountId' => '581081770241', // REQUIRED
                'Email' => $email,
                'IamArn' => 'arn:aws:iam::581081770241:role/myvoluntierdashboard',
                'IdentityType' => 'IAM', // REQUIRED IAM|QUICKSIGHT
                'UserRole' => 'READER', // REQUIRED READER:AUTHOR:ADMIN
                'SessionName' => $email,
                'Namespace' => 'default', // REQUIRED
            ]);

            return 'Created';
            //return json_decode($response->getBody()->getContents(), TRUE);

        } catch (\Exception $ex) {

            Log::warning('createAmazonUserIAM DUPLICATE found ' . $ex->getMessage() . $ex->getLine() . $ex->getFile());
            return null;
        }

    }

    public static function amazonRegisterUserGetEmbedURL()
    {

        try {

            $DashboardId = env('AWS_QUICK_DASHBOARD_ID');

            $clientQuickSight = new QuickSightClient(['region' => 'us-east-1', 'version' => 'latest']);

            self::createAmazonUserIAM($clientQuickSight);

            $response = $clientQuickSight->getDashboardEmbedUrl([
                'AwsAccountId' => '581081770241', // REQUIRED
                'DashboardId' => $DashboardId, // REQUIRED
                'IdentityType' => 'IAM',
                'SessionLifetimeInMinutes' => '60',
                'UndoRedoDisabled' => true,
                'ResetDisabled' => true,
            ])->toArray();

            if (isset($response['Status']) && $response['Status'] == 200) {
                log::info("EmbedUrl " . $response['EmbedUrl']);
                return $response['EmbedUrl'];
            } else {
                Log::error('getDashboardEmbedUrl API ' .\GuzzleHttp\json_encode($response));
                return null;
            }

        } catch (\Exception $ex) {

            Log::error('PostAmazonRegisterUser API ' . $ex->getMessage() . $ex->getLine() . $ex->getTraceAsString());
            
            return null;
        }
    }

}