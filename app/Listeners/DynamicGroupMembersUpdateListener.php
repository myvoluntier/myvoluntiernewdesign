<?php

namespace App\Listeners;

use App\Chat;
use App\Events\DynamicGroupMembersUpdateEvent;
use App\Group;
use App\Group_member;
use App\Http\Controllers\Organization\GroupCtrl;
use App\Services\ChatService;
use App\User;


class DynamicGroupMembersUpdateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public $groupCtrlObject = null;

    public function __construct()
    {
        $this->groupCtrlObject = new GroupCtrl();
    }

    /**
     * Handle the event.
     *
     * @param  DynamicGroupMembersUpdateEvent  $event
     * @return void
     */
    public function handle(DynamicGroupMembersUpdateEvent $event)
    {
        //todo: Make it as JOB with all attributes and commands run via queue due to time

        $key = key($event->data);
        $value = $event->data[$key];
        $type = $event->type;

        if($type == 'delete') {
            $this->deleteAttributeGroupChange($key ,$value);
        }

        elseif($type == 'add') {
            $this->addAttributeGroupChange($key ,$value);
        }

        elseif($type == 'update') {

            $key = key($event->data[0]);

            $oldValue = $event->data[0][$key];
            $newValue = $event->data[1][$key];

            $this->deleteAttributeGroupChange($key ,$oldValue);
            $this->addAttributeGroupChange($key ,$newValue);
        }
    }

    public function deleteAttributeGroupChange($key ,$value){

        $removeGroupMembers = [];

        $isKeyExistGroups = Group::dynamicAct()->where('dynamic_ui_rules', 'LIKE', '%' . $key . '%')->get();

        foreach ($isKeyExistGroups as $group) {

            list($where, $whereOr) = $this->groupCtrlObject->recursiveFunction(\GuzzleHttp\json_decode($group->dynamic_ui_rules ,true));

            if(!in_array( $group->id,$removeGroupMembers))
                foreach ($where as $condition) {

                    if($condition['key'] == $key && $condition['opr'] == '=' && $condition['vale'] == $value) {
                        $removeGroupMembers[] = $group->id;
                        break;
                    }
                }

            if(!in_array( $group->id,$removeGroupMembers))
                foreach ($whereOr as $condition) {

                    if($condition['key'] == $key && $condition['opr'] == '=' && $condition['vale'] == $value) {
                        $removeGroupMembers[] = $group->id;
                        break;
                    }
                }
        }

        $chatService = new ChatService();
        $user = \auth()->user();

        foreach ($removeGroupMembers as $group_id) {

            $chatsGroup = Chat::where('group_id', $group_id)->first();
            $chatService->removeUserFromChat($chatsGroup->chat_id, $user->user_name, 'groups');
            Group_member::where('user_id', $user->id)->where('group_id', $group_id)->update(['is_deleted' => 1]);;
        }
    }

    public function addAttributeGroupChange($key ,$value){


        $addGroupMembers = [];

        $isKeyExistGroups = Group::dynamicAct()->where('dynamic_ui_rules', 'LIKE', '%' . $key . '%')->get();

        foreach ($isKeyExistGroups as $group) {

            list($where, $whereOr) = $this->groupCtrlObject->recursiveFunction(\GuzzleHttp\json_decode($group->dynamic_ui_rules ,true));

            if(!in_array( $group->id,$addGroupMembers))
                foreach ($where as $condition) {

                    if($condition['key'] == $key && $condition['opr'] == '=' && $condition['vale'] == $value) {
                        $addGroupMembers[] = $group->id;
                        break;
                    }
                }

            if(!in_array( $group->id,$addGroupMembers))
                foreach ($whereOr as $condition) {

                    if($condition['key'] == $key && $condition['opr'] == '=' && $condition['vale'] == $value) {
                        $addGroupMembers[] = $group->id;
                        break;
                    }
                }
        }

        $chatService = new ChatService();
        $user = \auth()->user();

        foreach ($addGroupMembers as $group_id) {

            $chatsGroup = Chat::where('group_id', $group_id)->first();
            $group = Group::find($group_id);

            $group_member = new Group_member;
            $group_member->group_id = $group_id;
            $group_member->user_id = $user->id;
            $group_member->role_id = Group::GROUP_MEMBER;
            $group_member->status = Group_member::APPROVED;
            $group_member->save();

            $chatService->addUserToChat($user, $chatsGroup->chat_id, $group->name, 'groups');
        }
    }
}
