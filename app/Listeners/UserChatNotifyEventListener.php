<?php

namespace App\Listeners;

use App\Events\UserChatNotifyEvent;
use App\Mail\UsersDefaultMail;
use App\Models\EmailsTemplate;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserChatNotifyEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserChatNotifyEvent  $event
     * @return void
     */
    public function handle(UserChatNotifyEvent $event)
    {
        $receiver = $event->receiver;
        $sender = auth()->user();

        $email = EmailsTemplate::where('type' , 'chatNotify')->first();


        if ($receiver && $receiver->chat_msg_email == '0') //if enabled emails
        {
            $data['message'] = $event->message;
            $data['sender_name'] = $sender->getFullNameVolunteer();
            $data['sender_email'] = $sender->email;

            $bodyEmail = replaceBodyContents($email->body ,$email->type, $receiver ,null ,null ,$data);

            \Mail::to($receiver->email)->send(new UsersDefaultMail($bodyEmail ,$email->title));
        }

        if ($receiver && $receiver->chat_msg_text == '0' && $receiver->contact_number) //if texts enabled
        {
            sendSMSByTwilio($receiver->contact_number, $email->text_body);
        }

    }
}
