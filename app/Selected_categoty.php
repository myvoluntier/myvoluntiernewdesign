<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Selected_categoty extends Model
{
	protected $table = 'selected_category';
	protected $guarded = [];

    public function opprCategory(){
        return $this->hasMany(Opportunity_category::class, 'id', 'category_id');
    }

    public function category(){
        return $this->belongsTo(Opportunity_category::class, 'category_id', 'id');
    }

}
