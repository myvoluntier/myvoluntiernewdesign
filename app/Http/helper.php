<?php

use Illuminate\Support\Facades\Schema;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Log;

function dateYmdFormat($date, $format = 'm-d-Y')
{
    try {
        return \Carbon\Carbon::createFromFormat($format, $date)->format('Y-m-d');
    } catch (\Exception $ex) {
        Log::error($ex->getMessage() . ' line ' . $ex->getLine());
        return null;
    }
}

function dateMdySlashFormat($date)
{

    try {

        return \Carbon\Carbon::createFromFormat('m-d-Y', $date)->format('m/d/Y');
    } catch (\Exception $ex) {
        Log::error($ex->getMessage() . ' line ' . $ex->getLine());
        return null;
    }
}

function dateMdyNoSlashFormat($date)
{
    try {
        return \Carbon\Carbon::createFromFormat('m/d/Y', $date)->format('m-d-Y');
    } catch (\Exception $ex) {
        Log::error($ex->getMessage() . ' line ' . $ex->getLine());
        return null;
    }

}

function dateYmd($date){
    return \Carbon\Carbon::parse($date)->format('Y-m-d');
}

function dateDMonth($date){
    return \Carbon\Carbon::parse($date)->format('d M');
}

function dateMMYY($date){
    return \Carbon\Carbon::parse($date)->format('M-Y');
}

function dateDDMMYY($date){
    return \Carbon\Carbon::parse($date)->format('d-M-Y');
}

function dateMDY($date){
    return \Carbon\Carbon::parse($date)->format('m-d-Y');
}

function dateOnlyMonth($date){
    return \Carbon\Carbon::parse($date)->format('M');
}

function getLast12MonthsY(){
    $months = [];
     for ($i = 0; $i <= 11; $i++) {
        $months[] = date("M-Y", strtotime( date( 'Y-m-01' )." -$i months"));
    }
    return $months;
}

function convertUserName($email){
    return strtr($email, array("@" => "_", "." => "_"));
}

function numberDescender($value){

    if($value==1)
        $final = 'st';
    elseif($value==2)
        $final = 'nd';
    elseif($value==3)
        $final = 'rd';
    else
        $final = 'th';

    return $final;
}


function rand_color() {
    return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
}

function removeNumFromStart($string){
    return preg_replace('#^\d+#', '', $string);
}


function deleteFileFromS3($file_path){

    try {

        $original_path = str_replace(env('AWS_BUCKET_URL'), "", $file_path);

        \Storage::disk('s3')->delete($original_path);
        return true;

    } catch (\Exception $e) {
        Log::warning('deletion of file error ' . Auth::id(), [$file_path]);
        return null;
    }
}


function fileUploadOnS3($deletable = null, $file, $folder, $name){

    $base_url = config('filesystems.disks.s3.url');

    if($deletable){
        $this->deleteFileFromS3($deletable);
    }

    $filename = date('mdYhi').$name.'.'.$file->extension();
    $filePath = 'uploads/'.$folder.'/'. $filename;

    \Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');

    return ($base_url.$filePath);
}


function base64fileUploadOnS3($deletable = null, $file, $folder, $name){

    $base_url = config('filesystems.disks.s3.url');

    if($deletable){
        deleteFileFromS3($deletable);
    }

    list($baseType, $image) = explode(';', $file);
    list(, $image) = explode(',', $image);

    $image = base64_decode($image);

    $imageName = date('mdYhi').$name.'.png';
    $filePath = 'uploads/'.$folder.'/'. $imageName;

    $is_upload = \Storage::disk('s3')->put($filePath, $image, 'public');

    return ($base_url.$filePath);
}


/////////////////////////////ALL EMAIL & TEXTS RELATED WORK HERE///////////////////
function sendSMSByTwilio($toPhone ,$message_body)
{

    if(is_null($message_body)) return true;

    $sid = env('TWILIO_SID');
    $token = env('TWILIO_SECRET');
    $response = true;

    $fromPhone = '+18035922306';

    //would be good if all the SMS messages continue to come to me in the test environments....but in production send the SMS messages to the actual contact numbers.
    $toPhone = env('APP_ENV') == 'production' ? '+1'.str_replace("-","",$toPhone) : '+18033618821';

    try {
        $client = new Client($sid, $token);

        $client->messages->create(
            $toPhone,
            array(
                'from' => $fromPhone,
                'body' => $message_body
            )
        );

    } catch (Exception $e) {
        $response = true;

        \Log::error($e->getMessage().' error during sending twillio message in line '.$e->getLine().' to '.$toPhone);
    }

    return $response;
}

function replaceTextBodyContents($body ,$type, $user=null, $org=null, $oppr=null, $dataArray=null){

    if ($user && strpos($body, 'userTbl') !== false) {
        $body = getMessageColumnsAndReplace($user ,'userTbl' ,$body);
    }

    if ($org && strpos($body, 'orgTbl') !== false) {
        $body = getMessageColumnsAndReplace($org ,'orgTbl' ,$body);
    }

    if ($oppr && strpos($body, 'opprTbl') !== false) {
        $body = getMessageColumnsAndReplace($oppr ,'opprTbl' ,$body);
    }

    if ($dataArray){

        foreach ($dataArray as $ind => $val){
            $body = simpleReplace('{{'.$ind.'}}', $val, $body);
        }
    }

    $body = replaceContentsWithMessageTypes($body, $type, $user, $oppr, $dataArray);

    return $body;
}

function replaceContentsWithEmailTypes($body, $type, $user, $oppr, $dataArray)
{
    if ($type == 'confirmAccount' || $type == 'confirmAlternateEmail') {
        $path = url('/confirm_account') . '/' . $user->user_name . '/' . $user->confirm_code;

        $link = '<a href="' . $path . '">Start MyVoluntier</a>';
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'forgotPassword') {
        $path = url('/forgot_password') . '/' . $user->user_name . '/' . $user->forgot_status;

        $link = '<a href="' . $path . '">Click To Reset Password</a>';
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'requestConfirmTrackedHoursAdd' || $type == 'requestConfirmTrackedHoursChange') {

        $link = '<a href="' . url('/organization/track') . '">Please confirm tracked hours</a>';
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'responseJoinOpportunityAccept' || $type == 'responseJoinOpportunityReceive') {

        $path = url('/volunteer/view_opportunity') . '/' . $oppr->id;

        $link = '<a href="' . $path . '">' . $oppr->title . '</a>';
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'shareProfileVolunteer') {

        $path = url('/shared-profile') . '/' . $dataArray['shared_id'] . '/' . $dataArray['code'];

        $link = '<a href="' . $path . '">Visit Profile</a>';
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'shareProfileOrg') {

        $path = url('/profile') . '/' . $dataArray['shared_id'] . '/' . $dataArray['code'];

        $link = '<a href="' . $path . '">Visit Profile</a>';
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'shareTranscript') {

        $path = url('/shared-transcript') . '/' . $dataArray['shared_id'] . '/' .$dataArray['showDataOrNot'].'/'. $dataArray['code'];

        $link = '<a href="' . $path . '">Visit Transcript Page</a>';
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'trackConfirm') {

        $path = url('/organization/track/customConfirm') . '/' . $dataArray['track_id'] . '/' . $dataArray['confirm_code'];

        $link = '<a href="' . $path . '">Visit Transcript Page</a>';
        $body = str_replace("{{link}}", $link, $body);
    }elseif ($type == 'shareServiceProject') {

        $path = url('/shared-serviceproject') . '/' . $dataArray['shared_id'] . '/' . $dataArray['code'];

        $link = '<a href="' . $path . '">Visit Service Project Page</a>';
        $body = str_replace("{{link}}", $link, $body);
    }

    return $body;
}

function replaceContentsWithMessageTypes($body, $type, $user, $oppr, $dataArray)
{
    if ($type == 'confirmAccount' || $type == 'confirmAlternateEmail') {

        $link = url('/confirm_account') . '/' . $user->user_name . '/' . $user->confirm_code;
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'forgotPassword') {

        $link = url('/forgot_password') . '/' . $user->user_name . '/' . $user->forgot_status;
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'requestConfirmTrackedHoursAdd' || $type == 'requestConfirmTrackedHoursChange') {

        $link = url('/organization/track');
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'responseJoinOpportunityAccept' || $type == 'responseJoinOpportunityReceive') {

        $link = url('/volunteer/view_opportunity') . '/' . $oppr->id;
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'shareProfileVolunteer') {

        $link = url('/shared-profile') . '/' . $dataArray['shared_id'] . '/' . $dataArray['code'];
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'shareProfileOrg') {

        $link = url('/profile') . '/' . $dataArray['shared_id'] . '/' . $dataArray['code'];
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'shareTranscript') {

        $link = url('/shared-transcript') . '/' . $dataArray['shared_id'] . '/' . $dataArray['code'];
        $body = str_replace("{{link}}", $link, $body);
    } elseif ($type == 'trackConfirm') {

        $link = url('/organization/track/customConfirm') . '/' . $dataArray['track_id'] . '/' . $dataArray['confirm_code'];
        $body = str_replace("{{link}}", $link, $body);
    }elseif ($type == 'shareServiceProject') {

        $link = url('/shared-serviceproject') . '/' . $dataArray['shared_id'] . '/' . $dataArray['code'];
        $body = str_replace("{{link}}", $link, $body);
    }

    return $body;
}

function replaceBodyContents($body ,$type, $user=null, $org=null, $oppr=null, $dataArray = null){

    if ($user && strpos($body, 'userTbl') !== false) {
        $body = getColumnsAndReplace($user ,'userTbl' ,$body);
    }

    if ($org && strpos($body, 'orgTbl') !== false) {
        $body = getColumnsAndReplace($org ,'orgTbl' ,$body);
    }

    if ($oppr && strpos($body, 'opprTbl') !== false) {
        $body = getColumnsAndReplace($oppr ,'opprTbl' ,$body);
    }

    if ($dataArray){
        foreach ($dataArray as $ind => $val){
            $body = simpleReplace('{{'.$ind.'}}', $val, $body);
        }
    }

    $body = replaceContentsWithEmailTypes($body, $type, $user, $oppr, $dataArray);

    return $body;
}

function simpleReplace($search, $value, $body){
    return $final = str_replace($search, $value, $body);
}

function getColumnsAndReplace($object , $key, $body){

    // get the column names for the table
    $columns_names = Schema::getColumnListing($object->getTable());

    // create array where column names are keys, and values are null
    $columns = array_fill_keys($columns_names, null);

    // merge the populated values into the base array
    $array = array_merge($columns, $object->attributesToArray());

    //check one by one column if it exist replace otherwise skip
    foreach($array as $index => $value){
        $search = '{{'.$key."-&gt;".$index.'}}';
        $value = $value ? $value : ' ';

        //some columns will be array so skip those to avoid exception of invalid type
        if(is_int($value) || is_string($value))
            $body =  str_replace($search, $value, $body);
    }

    return $body;
}

function getMessageColumnsAndReplace($object , $key, $body){
    // get the column names for the table
    $columns_names = Schema::getColumnListing($object->getTable());

    // create array where column names are keys, and values are null
    $columns = array_fill_keys($columns_names, null);

    // merge the populated values into the base array
    $array = array_merge($columns, $object->attributesToArray());

    //check one by one column if it exist replace otherwise skip
    foreach($array as $index => $value){

        $search = '{{'.$key."->".$index.'}}';
        $value = $value ? $value : '';
        //some columns will be array so skip those to avoid exception of invalid type
        if(is_int($value) || is_string($value))
            $body =  str_replace($search, $value, $body);
    }

    return $body;
}