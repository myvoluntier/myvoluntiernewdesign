<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

class Userlist extends Controller
{
    //Copied All Code to UsersController to make it good files structure.

	public function change_status($id,$status)
	{
		if(!isset($id) || !isset($status)){
			return redirect()->back()->with('error', "Status can't chnage");
		}
		$user = User::find($id);
		$user->confirm_code = $status;
		$user_status = $user->save();

		if($user_status){
			return redirect()->back()->with('success', "Status changed successfully");
		}else{
			return redirect()->back()->with('error', "Status can't updated");
		}
	}

}
