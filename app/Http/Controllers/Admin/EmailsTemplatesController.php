<?php

namespace App\Http\Controllers\Admin;

use App\Models\EmailsTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailsTemplatesController extends Controller
{
    public function index(){
        $emails = EmailsTemplate::all();
        return view('admin.site-settings.emailsTexts.index' ,compact('emails'));
    }

    public function edit($id){
        $email = EmailsTemplate::find($id);
        return view('admin.site-settings.emailsTexts.edit' ,compact('email'));
    }

    public function update(Request $request){

        $email = EmailsTemplate::FirstOrNew(['type' => $request->mail_type]);
        $email->title = $request->title;
        $email->body = $request->mail_body;
        $email->text_body = $request->text_body;
        $email->table_obj = $request->table_obj;
        $email->save();
        session()->flash('app_message', 'Emails successfully Updated');

        return redirect(route('admin.email.index'));
    }
}
