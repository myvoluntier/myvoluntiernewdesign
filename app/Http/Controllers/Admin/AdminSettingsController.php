<?php

namespace App\Http\Controllers\Admin;

use App\Activity;
use App\Alert;
use App\Chat;
use App\Comment;
use App\Friend;
use App\Group;
use App\Group_member;
use App\Http\Controllers\Volunteer\TrackingCtrl;
use App\News;
use App\NewsfeedModel;
use App\Opportunity;
use App\Opportunity_member;
use App\Selected_categoty;
use App\Services\ChatManager;
use App\Services\ChatService;
use App\Share;
use App\Tracking;
use App\User;
use App\Models\Sitecontent;
use App\Models\Attributelookup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdminSettingsController extends Controller {

    public function index() {

        $stats = $this->usersStatsCalculation();
        $isProceeded = false;

        return view('admin.site-settings.clear-test-data', compact('stats', 'isProceeded'));

    }

    public function updateData(Request $request) {

        if ($request->password != 'web123456') {
            session()->flash('app_error', 'Your given password is invalid.');
            return back();
        }

        $response['errorsIds'] = [];
        $response['proceededIds'] = [];

        if ($request->user_ids)
            $userIds = $request->user_ids;
        else
            $userIds = $request->user_ids_comma ? explode(',', $request->user_ids_comma) : [];

        if (count($userIds) == 0) {
            session()->flash('app_error', 'Please select some ids to clean data.');
            return back();
        }

        foreach ($userIds as $userId) {

            try {
                $userObj = User::find(trim($userId));

                if ($userObj) {

                    DB::beginTransaction();
                    $this->deleteUserChildData($userObj);
                    //sub-organization deleted
                    $userSubOrgObj = User::where('parent_id', trim($userId))->get();

                    if ($userSubOrgObj) {
                        foreach ($userSubOrgObj as $userSubOrgId) {
                            $userSubObj = User::find(trim($userSubOrgId->id));
                            if ($userSubObj) {
                                $this->deleteUserChildData($userSubObj);
                                $userSubObj->delete();
                            }
                        }
                    }
                    $userObj->delete();

                    DB::commit();
                    $response['proceededIds'][] = $userId;
                }
            } catch (\Exception $e) {
                Log::warning($e->getMessage() . ' for user id: ' . $userId);
                DB::rollback();
                $response['errorsIds'][] = $userId;
            }
        }

        $stats = $this->usersStatsCalculation();
        $isProceeded = true;

        session()->flash('app_message', 'You database has been updated please see log warnings for error ids.');

        return view('admin.site-settings.clear-test-data', compact('stats', 'isProceeded', 'response'));
    }

    public function deleteUserChildData($userObj) {

        $userId = $userObj->id;
        //activities
        $userObj->activities()->delete();

        //alerts
        $userObj->alertReceivers()->delete();
        $userObj->alertSenders()->delete();

        //alerts
        $userObj->alertReceivers()->delete();

        $chatService = new ChatService();
        $chatManager = new ChatManager();

        Chat::where('user_id', $userId)->orWhere('to_user_id', $userId)->delete();
        Comment::where('commenter_id', $userId)->delete();

        $userObj->follower()->delete();
        $userObj->followed()->delete();

        $userObj->friends()->delete();
        $userObj->whereUserFriends()->delete();

        $userObj->groups()->delete(); //its pivot table need to check its working or not
        Group_member::where('user_id', $userId)->delete();
        Group::where('creator_id', $userId)->delete();

        $userObj->groupOrg()->delete();

        $userObj->MessagesWhereYouReceiver()->delete();
        $userObj->MessagesWhereYouSender()->delete();

        News::where('user_id', $userId)->delete();

        $userObj->newsFeeds()->delete();

        $allUserRelatedOppr = Opportunity::where('org_id', $userId)->get();

        if ($allUserRelatedOppr) {

            foreach ($allUserRelatedOppr as $oppr) {
                $oppr->activities()->delete();
                $oppr->tracks()->delete();
                $oppr->opprSelectedCategories()->delete();

                Selected_categoty::where('opportunities_id', $oppr->id)->delete();
                Opportunity_member::where('oppor_id', $oppr->id)->delete();
                $vols = Opportunity_member::where('oppor_id', $oppr->id)->where('status', 1)->get();
                $chat_grp_oppr = Chat::where('group_id', $oppr->id)->first();

                foreach ($vols as $vol) {
                    $u = User::find($vol->user_id);
                    if ($u && $chat_grp_oppr)
                        $chatService->removeUserFromChat($chat_grp_oppr->chat_id, $u->user_name, 'organizations');
                }
                if ($chat_grp_oppr)
                    $chatManager->deleteChat($chat_grp_oppr);

                $oppr->delete();
            }
        }

        $chats = Chat::where('user_id', $userId)->get();

        foreach ($chats as $chat) {
            $chatService->removeChat($chat, $userObj->user_name);
            Chat::where('chat_id', $chat->chat_id)->delete();
        }

        $userObj->tracksVoluntier()->delete();
        $userObj->tracksOrganization()->delete();

        $userObj->opportunityOrg()->delete();

        Opportunity_member::where('user_id', $userId)->orWhere('org_id', $userId)->delete();

        $userObj->reviewFrom()->delete();
        $userObj->reviewTo()->delete();

        Share::where('shared_user_id', $userId)->delete();

        $userObj->reviewTo()->delete();

        Tracking::where('volunteer_id', $userId)->delete();
        Tracking::where('org_id', $userId)->delete();

        //service projects delete
        $userObj->serviceProjects()->delete();
    }

    public function usersStatsCalculation() {

        $stats['users'] = User::all();
        $stats['totalUser'] = User::get()->count();
        $stats['totalOppr'] = Opportunity::get()->count();
        $stats['totalGrps'] = Group::get()->count();
        $stats['totalAlerts'] = Alert::get()->count();
        $stats['totalTracking'] = Tracking::get()->count();
        $stats['totalNewsFeeds'] = NewsfeedModel::get()->count();
        $stats['totalOpperMembers'] = Opportunity_member::get()->count();
        $stats['totalNewsFeed'] = NewsfeedModel::get()->count();
        $stats['totalFriends'] = Friend::get()->count();
        $stats['totalChat'] = Chat::get()->count();

        return $stats;
    }

    public function addHoursIndex(){

        $voluntiers = User::where('user_role' ,'volunteer')->get();
        $opportunities = Opportunity::active()->get();

        return view('admin.site-settings.add-dummy-hours', compact('voluntiers', 'opportunities'));
    }

    public function joinOpportunity($user_id ,$oppor_id)
    {
        $opportunity = Opportunity::find($oppor_id);
        $receiver_id = $opportunity->org_id;

        $oppor_member = new Opportunity_member;
        $oppor_member->oppor_id = $oppor_id;
        $oppor_member->user_id = $user_id;

        $oppor_member->status = 1;

        $oppor_member->org_id = $receiver_id;
        $oppor_member->save();

        $activity_data = new Activity;
        $activity_data->user_id = $user_id;
        $activity_data->oppor_id = $oppor_id;
        $activity_data->oppor_title = $opportunity->title;
        $activity_data->link = 0;

        $activity_data->content = "You Joined on Opportunity";
        $activity_data->type = 1;

        $activity_data->created_at = date("Y-m-d H:i:s");
        $activity_data->updated_at = date("Y-m-d H:i:s");
        $activity_data->is_deleted = 0;
        $activity_data->save();

        $alert = new Alert;
        $alert->receiver_id = $receiver_id;
        $alert->sender_id = $user_id;
        $alert->sender_type = 'volunteer';
        $alert->alert_type = Alert::ALERT_JOIN_OPPORTUNITY;
        $alert->related_id = $oppor_member->id;
        $alert->contents = 'Joined Opportunity | ' . $opportunity->title;
        $alert->save();

    }


    public function addHoursPost(Request $request)
    {
        $count = 0;

        foreach ($request->user_ids as $id) {

            try {

                $voluntier = User::find($id);

                $opp_id = $request->input('opportunity_id');
                $opportunity = Opportunity::find($opp_id);

                $this->joinOpportunity($id ,$opp_id);

                $is_designated = 0;
                $designated_group_id = null;

                $approve_status = $request->input('is_auto_approve') == 'on' ? 1 : 0;

                $opp_name = $opportunity->title;
                $volunteer_id = $voluntier->id;

                $time_blocks = $request->input('time_block');

                $start_time = reset($time_blocks);
                $end_time = end($time_blocks);


                $logged_mins = count($time_blocks) * 30;
                $logged_date = $request->input('date');

                $comments = 'Auto Hours';

                $unlist_org_email = null;

                $time_block = implode(',', $time_blocks);

                $tracks = new Tracking;
                $tracks->volunteer_id = $volunteer_id;
                $tracks->oppor_id = $opp_id;
                $is_link = Opportunity::find($opp_id)->type;

                if ($is_link == 1) {
                    $tracks->link = 1;
                }

                $tracks->oppor_name = $opp_name;

                if($approve_status) {
                    $tracks->approv_status = $approve_status;
                    $tracks->confirm_code = null;
                    $tracks->confirmed_at = date("Y-m-d H:i:s");
                }
                else
                    $tracks->approv_status = $approve_status;

                $tracks->designated_group_id = $designated_group_id;
                $tracks->is_designated = $is_designated;
                $tracks->org_id = Opportunity::find($opp_id)->org_id;
                $tracks->started_time = $start_time;
                $tracks->ended_time = $end_time;
                $tracks->logged_mins = $logged_mins;
                $tracks->logged_date = $logged_date;
                $tracks->description = $comments;
                $tracks->selectd_timeblocks = $time_block;
                $tracks->save();


                $add_activity = new Activity;
                $add_activity->user_id = $volunteer_id;
                $add_activity->oppor_id = $opp_id;

                if ($is_link == 1) {
                    $add_activity->link = 1;
                }

                $add_activity->oppor_title = $opp_name;
                $add_activity->content = "You Added " . $logged_mins . "mins on Opportunity ";
                $add_activity->type = Activity::ACTIVITY_ADD_HOURS;
                $add_activity->save();

                if(!$approve_status) {
                    $alert = new Alert;
                    $alert->receiver_id = $tracks->org_id;
                    $alert->sender_id = $volunteer_id;
                    $alert->sender_type = 'volunteer';
                    $alert->related_id = $tracks->id;
                    $alert->alert_type = Alert::ALERT_TRACK_CONFIRM_REQUEST;
                    $alert->contents = ' asking confirm logged hours on your opportunity.';
                    $alert->save();
                }
                $count++;
            }

            catch (\Exception $ex) {
                Log::error($ex->getMessage() . $ex->getTraceAsString().$ex->getLine());
            }
        }

        $message = 'Auto tracked hours done for only '.$count. ' volunteers';
        session()->flash('app_message', $message);

        return back();
    }


    public function joinAsChat($track ,$opportunity ,$vol){
        //todo: need to call when all finish.
        $opp_chat = Chat::where('group_id', $track->oppor_id)->first();
        $manager      = new ChatManager();

        if($opp_chat == null)
        {
            $chat_service = new ChatService();
            $photo        = $opportunity->logo_img === null ? asset('img/org/001.png') : asset( 'uploads/' . $opportunity->logo_img );
            $chatId       = $chat_service->createChat( $opportunity->title, $photo );
            $chat_service->addUserToChat( $vol, $chatId, $opportunity->title, 'organizations' );
            $chat           = new Chat();
            $chat->chat_id  = $chatId;
            $chat->user_id  = $vol->id;
            $chat->group_id = $opportunity->id;
            $chat->type     = 'organizations';
            $chat->save();
        }

        $manager->joinOrganization( $vol, $opportunity );
    }


    public function siteContents() {

        $siteContents = Sitecontent::all();
        return view('admin.site-settings.sitecontent', compact('siteContents'));
    }

    public function siteContentEdit($id) {
        $sitecontent = Sitecontent::find($id);
        return view('admin.site-settings.sitecontent_edit', compact('sitecontent'));
    }

    public function siteContentUpdate(Request $request) {

        $sitecontent = Sitecontent::find($request->id);
        $updated = $sitecontent->update($request->except('_token', 'id'));

        if ($updated) {
            session()->flash('app_message', 'Site Contents updated successfully.');
        } else {
            session()->flash('app_error', 'Something went wrong..!');
        }

        return redirect()->route('admin.sitecontent');
    }
    
    public function attributes(){
        $attributes = Attributelookup::all()->toArray();
        
        return view('admin.attributes.attributelist',compact('attributes'));
    }

    public function attributeSave($id = null,Request $request) {

        if(!empty($id)){
            $attribute = Attributelookup::find($id);
        }else{
            $attribute = new Attributelookup();
        }

        $organizations = User::where('user_role', 'organization')->where('is_deleted', '<>', 1)->pluck('org_name' ,'id')->toArray();

        ksort($organizations);

        if(!empty($request->input())) {

            try{

                if(!empty(trim($request->input('id')))){
                    $attribute = Attributelookup::find($request->input('id'));
                }

                $attribute->attributekey = trim($request->input('attributekey'));
                $attribute->attributename = trim($request->input('attributename'));
                $attribute->attributedescr = trim($request->input('attributedescr'));
                $attribute->sync_org_owners = implode(',',$request->sync_org_owners);

                $attribute->save();

                session()->flash('app_message', 'Attribute save successfully.');

            } catch (Exception $ex) {
               Log::error( $ex->getMessage() .' adding attribute by admin');
               session()->flash('app_error', 'Something went wrong..!');
            }

            return redirect()->route('admin.attributes');
        }

        $attribute->sync_org_owners = explode(',' ,$attribute->sync_org_owners);

        return view('admin.attributes.attributeform',compact('attribute' ,'organizations'));

    }
}
