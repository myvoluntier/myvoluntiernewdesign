<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Log;
use Session;
use App\Group;
use App\User;
use App\Group_member;
use Image;

class GroupManageController extends Controller
{
   	public function listing()
   	{
		$session=Auth::guard('admin')->user();
		// if($session)
		// {
		// 	return redirect('/admin-login');
		// }
		$userlist = Group::all();
		return view('admin/grouplist')->with('list',$userlist);
	} 
	
	public function deleting(Request $req)
	{
		$groupmodel = Group::find($req->id);
		$groupmodel->is_deleted = 1;		
		$groupmodel->save();
		$req->session()->flash('success', 'Group successfully Deleted');
		return redirect('/admin/group-list');
		
	}
	public function editing(Request $req)
	{
		$groupmodel = Group::find($req->id);
		return view('admin/group_edit')->with('userdetails',$groupmodel);
	}
	
	public function editPost(Request $req)
	{
        if (!empty($req->input())) {

            $groupmodel = Group::find($req->input('edit_group_id'));

            $groupmodel->name = $req->input('grp_name');
            $groupmodel->contact_name = $req->input('admin_name');
            $groupmodel->contact_email = $req->input('admin_email');
            $groupmodel->created_at = $req->input('date_created');
            $groupmodel->description = $req->input('description');

            if ($req->file('logo_img')) {
                $groupmodel->logo_img = fileUploadOnS3($groupmodel->logo_img,$req->image_logo ,Group::S3PathLogo,'logo');

//                $img->resize(100, 100, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save($destinationPaththumb . '/' . $input['imagename']);
            }


            if ($req->file('file_banner')) {
                $groupmodel->banner_image = fileUploadOnS3($groupmodel->banner_image,$req->image_banner ,Group::S3PathBan,'ban');
            }

            $groupmodel->save();
            $req->session()->flash('success', 'Data successfully Edited');
            return redirect('/admin/group-list');
        }
	}
	
	public function groupAddVol(Request $req){

		$session=Auth::guard('admin')->user();
		if(count($session)==0){
			return redirect('/admin-login');
		}
		$groupDetails = Group::find($req->id);
		$userlist = User::where('user_role','volunteer')->where('is_deleted',0)->get();
		return view('admin/group_add_vol')->with('list',$userlist)->with('group_id',$req->id)->with('groupDetails',$groupDetails);
	} 
	
	public function groupAddVolPost(Request $req){
		$session=Auth::guard('admin')->user();
		if(count($session)==0){
			return redirect('/admin-login');
		}
		$vol_ids=explode(',',$req->input('vol_ids'));
		$group_id = $req->input('group_id');
		$groupmodel = Group::find($group_id);
		foreach($vol_ids as $vol_id){
			$Group_member = Group_member::where('group_id',$group_id)->where('user_id',$vol_id)->count();
			if($Group_member==0){
				$groupmember = new Group_member;
				$groupmember->group_id = $group_id;	
				$groupmember->user_id = $vol_id;
				$groupmember->role_id = 2;
				$groupmember->status  = 2;
				$groupmember->created_at  = date('Y-m-d H:i:s');
				$groupmember->updated_at  = date('Y-m-d H:i:s');
				$groupmember->is_deleted  = 0;	
				$groupmember->save();
			}			
		}

		$req->session()->flash('success', 'Member successfully inserted into group '.$groupmodel->name);
		return redirect('/admin/group-list');
	}


	public function moveGroupImagesToS3(){

	    $groups = Group::all();
	    $count = 0;

        ini_set('max_execution_time', 600); //300 seconds = 5 minutes

        foreach ($groups as $group){

            $logo = $group->logo_img;
            $banner = $group->banner_image;

	       try{

               if(!is_null($logo) && !substr_count($logo, 'amazonaws.com')) {

                   $logoPath = asset('/uploads/volunteer_group_logo'). '/' . $logo;

                   $base_url = config('filesystems.disks.s3.url');

                   $serverPath = Group::S3PathLogo.'/'. $logo;

                   \Storage::disk('s3')->put($serverPath, file_get_contents(preg_replace("/ /", "%20", $logoPath)), 'public');

                   $group->update(['logo_img' => $base_url.$serverPath]);

                   $count++;
               }


               if(!is_null($banner) && !substr_count($banner, 'amazonaws.com')) {

                   $bannerPath = asset('/uploads/volunteer_group_logo'). '/' . $logo;

                   $base_url = config('filesystems.disks.s3.url');

                   $serverPath = Group::S3PathBan.'/'. $banner;

                   \Storage::disk('s3')->put($serverPath, file_get_contents(preg_replace("/ /", "%20", $bannerPath)), 'public');

                   $group->update(['banner_image' => $base_url.$serverPath]);

               }
           }

           catch (\Exception $ex) {
               log::info($ex->getMessage(). ' for the group id please check manually. '.$group->id);
           }
        }

        log::alert($count. ' groups images updated successfully on the s3 amazon from directory.');

        session()->flash('success', $count. ' groups images uploaded successfully on the s3 amazon from directory.');

        return back();

    }

}
