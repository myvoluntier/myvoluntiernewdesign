<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Group;
use App\Opportunity;
use App\Group_member;
use PDF;
use DB;
use Response;

class ReportsController extends Controller
{
    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////
    public function transcriptReport()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        return view('admin/reports/transcript',compact('users'));
    }

    public function transcriptReportCSV()
    {

    }

    public function transcriptReportPdf()
    {

    }

    public function trackedHoursReport()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        return view('admin/reports/tracked_hours',compact('users'));
    }

    public function trackedHoursReportCSV()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($users) {
            $users->name = $users->first_name . ' ' . $users->last_name;
            $users->joining_date = date('m/d/Y',strtotime($users->created_at));  
            $users->total_tracked_hours = $users->getLoggedHoursSum(); 
        });
        $csvExporter->build($users, ['name' => 'Voluntier Name', 'joining_date' => 'Joining Date', 'total_tracked_hours' => 'Total Hours Tracked'])->download('tracked_hours_report.csv');
    }

    public function trackedHoursReportPdf()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        $html = view('admin/reports/tracked_hours_pdf',compact('users'))->render();
        $options = PDF::getOptions();
        $options->setIsRemoteEnabled(true);
        return PDF::load($html)->setOptions($options)->filename('tracked_hours_report.pdf')->download();
    }

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    public function voluntierOrganization()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        if($users->count() > 0)
        {
            foreach($users as $user)
            {
                $organization_name = '';
                if($user->tracksVoluntier()->count() > 0)
                {
                    foreach($user->tracksVoluntier()->groupBy('org_id')->get() as $organization){
                        $organization_name .= ($organization->getOrgNameAttribute() != '') ? $organization->getOrgNameAttribute() . ' , ' : '';  
                    }
                }
                $user->organization_name = rtrim($organization_name,' , ');
            }
        }
        return view('admin/reports/voluntier_organization',compact('users'));
    }

    public function voluntierOrganizationCSV()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($users) {
            $users->name = $users->first_name . ' ' . $users->last_name;
            $users->joining_date = date('m/d/Y',strtotime($users->created_at));  
            $organization_name = '';
                if($users->tracksVoluntier()->count() > 0)
                {
                    foreach($users->tracksVoluntier()->groupBy('org_id')->get() as $organization){
                        $organization_name .= ($organization->getOrgNameAttribute() != '') ? $organization->getOrgNameAttribute() . ' , ' : '';  
                    }
                }
            $users->organization_name = rtrim($organization_name,' , ');
        });
        $csvExporter->build($users, ['name' => 'Voluntier Name', 'joining_date' => 'Joining Date', 'organization_name' => 'Organization Name'])->download('Voluntier_organization_report.csv');
    }

    public function voluntierOrganizationPdf()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        if($users->count() > 0)
        {
            foreach($users as $user)
            {
                $organization_name = '';
                if($user->tracksVoluntier()->count() > 0)
                {
                    foreach($user->tracksVoluntier()->groupBy('org_id')->get() as $organization){
                        $organization_name .= ($organization->getOrgNameAttribute() != '') ? $organization->getOrgNameAttribute() . ' , ' : '';  
                    }
                }
                $user->organization_name = rtrim($organization_name,' , ');
            }
        }
        $html = view('admin/reports/voluntier_organization_pdf',compact('users'))->render();
        $options = PDF::getOptions();
        $options->setIsRemoteEnabled(true);
        return PDF::load($html)->setOptions($options)->filename('voluntier_organization_report.pdf')->download();
    }

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    public function memberTotalHours()
    {
        $data = Opportunity::where(['is_deleted' => 0])->get();
        if($data->count() > 0)
        {   
            foreach($data as $opportunity)
            {
                $members_name = '';
                $member_exists = array();
                $logged_hours = 0;
                if($opportunity->users()->count() > 0)
                {
                    foreach($opportunity->users()->get() as $member){
                            if(!in_array($member->id,$member_exists)){
                            $member_exists[] = $member->id;
                            $members_name .=  $member->first_name . ' ' . $member->last_name. ' , ';
                            $logged_hours += $member->getLoggedHoursSum();
                            }
                    }
                } 
            $opportunity->member_name = rtrim($members_name,' , ');
            $opportunity->logged_hours = $logged_hours;
            }
        }
        return view ('admin/reports/member_total_hours', compact('data'));
    }

    public function memberTotalHoursCSV()
    {
        $opportunities = Opportunity::where(['is_deleted' => 0])->select(array('id','title'))->get();

        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($opportunities) {
                $members_name = '';
                $member_exists = array();
                $logged_hours = 0;
                if($opportunities->users()->count() > 0)
                {
                    foreach($opportunities->users()->get() as $member){
                            if(!in_array($member->id,$member_exists)){
                            $member_exists[] = $member->id;
                            $members_name .=  $member->first_name . ' ' . $member->last_name. ' , ';
                            $logged_hours += $member->getLoggedHoursSum();
                            }
                    }
                } 
            $opportunities->member_name = rtrim($members_name,' , ');
            $opportunities->logged_hours = $logged_hours;
        });
        $csvExporter->build($opportunities, ['title' => 'Opportunity', 'member_name' => 'Member', 'logged_hours' => 'Total Hours Tracked'])->download('member_total_hours.csv');
    }

    public function memberTotalHoursPdf()
    {
        $data = Opportunity::where(['is_deleted' => 0])->get();
        if($data->count() > 0)
        {   
            foreach($data as $opportunity)
            {
                $members_name = '';
                $member_exists = array();
                $logged_hours = 0;
                if($opportunity->users()->count() > 0)
                {
                    foreach($opportunity->users()->get() as $member)
                    {
                        if(!in_array($member->id,$member_exists))
                        {
                            $member_exists[] = $member->id;
                            $members_name .=  $member->first_name . ' ' . $member->last_name. ' , ';
                            $logged_hours += $member->getLoggedHoursSum();
                        }
                    }
                } 
                $opportunity->member_name = rtrim($members_name,' , ');
                $opportunity->logged_hours = $logged_hours;
            }
        }
        $html= view ('admin/reports/member_total_hours_pdf', compact('data'))->render();
        $options = PDF::getOptions();
        $options->setIsRemoteEnabled(true);
        return PDF::load($html)->setOptions($options)->filename('members_total_hours.pdf')->download();
    }
    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    public function organizationOpportunity()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        if($users->count() > 0)
        {
            foreach($users as $user)
            {
                $opportunity_name = '';
                $organization_name = '';    
                $tracked_hours = 0;
                if($user->tracksVoluntier()->count() > 0)
                {
                    foreach($user->tracksVoluntier()->groupBy('org_id')->get() as $organization){
                        $organization_name .= ($organization->getOrgNameAttribute() != '') ? $organization->getOrgNameAttribute() . ' , ' : '';  
                    }
                    foreach($user->tracksVoluntier()->groupBy('oppor_id')->get() as $organization){
                        $opportunity_name .= ($organization->getOppNameAttribute() != '') ? $organization->getOppNameAttribute() . ' , ' : '';  
                    }
                    foreach($user->tracksVoluntier()->groupBy('oppor_name')->get() as $organization3){
                        if($organization3->getLoggedHours()!='')
                        $tracked_hours = $tracked_hours + $organization3->getLoggedHours();
                    }
                }
                        
                $user->opportunity_name = rtrim($opportunity_name,' , ');
                $user->organization_name = rtrim($organization_name,' , ');
                $user->tracked_hours = rtrim($tracked_hours,' , ');
            }
        }
        return view('admin/reports/organization_opportunity',compact('users'));
    }

    public function organizationOpportunityCSV()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($users) {
            $opportunity_name = '';
            $organization_name = '';    
            $tracked_hours = 0;
            $users->name = $users->first_name . ' ' . $users->last_name;
            if($users->tracksVoluntier()->count() > 0)
            {
                foreach($users->tracksVoluntier()->groupBy('org_id')->get() as $organization){
                    $organization_name .= ($organization->getOrgNameAttribute() != '') ? $organization->getOrgNameAttribute() . ' , ' : '';  
                }
                foreach($users->tracksVoluntier()->groupBy('oppor_id')->get() as $organization){
                    $opportunity_name .= ($organization->getOppNameAttribute() != '') ? $organization->getOppNameAttribute() . ' , ' : '';  
                }
                foreach($users->tracksVoluntier()->groupBy('oppor_name')->get() as $organization3){
                    if($organization3->getLoggedHours()!='')
                    $tracked_hours = $tracked_hours + $organization3->getLoggedHours();
                }
            }
                    
            $users->opportunity_name = rtrim($opportunity_name,' , ');
            $users->organization_name = rtrim($organization_name,' , ');
            $users->tracked_hours = rtrim($tracked_hours,' , ');
        });
        $csvExporter->build($users, ['name' => 'Voluntier Name', 'organization_name' => 'Organization Name', 'opportunity_name' => 'Opportunity Name', 'tracked_hours' => 'Total Hours Tracked'])->download('organization_opportunity_report.csv');
    }

    public function organizationOpportunityPdf()
    {
        $users = User::where(['user_role' => 'volunteer', 'is_deleted' => 0])->get();
        if($users->count() > 0)
        {
            foreach($users as $user)
            {
                $opportunity_name = '';
                $organization_name = '';    
                $tracked_hours = 0;
                if($user->tracksVoluntier()->count() > 0)
                {
                    foreach($user->tracksVoluntier()->groupBy('org_id')->get() as $organization){
                        $organization_name .= ($organization->getOrgNameAttribute() != '') ? $organization->getOrgNameAttribute() . ' , ' : '';  
                    }
                    foreach($user->tracksVoluntier()->groupBy('oppor_id')->get() as $organization){
                        $opportunity_name .= ($organization->getOppNameAttribute() != '') ? $organization->getOppNameAttribute() . ' , ' : '';  
                    }
                    foreach($user->tracksVoluntier()->groupBy('oppor_name')->get() as $organization3){
                        if($organization3->getLoggedHours()!='')
                        $tracked_hours = $tracked_hours + $organization3->getLoggedHours();
                    }
                }
                        
                $user->opportunity_name = rtrim($opportunity_name,' , ');
                $user->organization_name = rtrim($organization_name,' , ');
                $user->tracked_hours = rtrim($tracked_hours,' , ');
            }
        }
        $html = view('admin/reports/organization_opportunity_pdf',compact('users'))->render();
        $options = PDF::getOptions();
        $options->setIsRemoteEnabled(true);
        return PDF::load($html)->setOptions($options)->filename('organization_opportunity_report.pdf')->download();
    }

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    public function organizationMember()
    {
        $users = User::where(['user_role' => 'organization', 'is_deleted' => 0])->get();
        if($users->count() > 0)
        {   
            foreach($users as $user)
            {
                $members_name = '';
                $join_date = '';
                $member_exists = array();
                if($user->opportunityOrg()->count() > 0)
                {
                    foreach($user->opportunityOrg()->get() as $organization){
                        if($organization->users()->count() > 0){
                            foreach($organization->users()->get() as $member){
                                if(!in_array($member->id,$member_exists)){
                                $member_exists[] = $member->id;
                                $members_name .=  $member->first_name . ' ' . $member->last_name. ' , ';
                                $join_date .= date('m/d/Y',strtotime($member->created_at)) . ' , ';
                                }
                            }
                        }
                    }
                } 
            $user->member_name = rtrim($members_name,' , ');
            $user->join_date = rtrim($join_date,' , ');
            }
        }
        return view('admin/reports/organization_member',compact('users'));
    }

    public function organizationMemberCSV()
    {
        $users = User::where(['user_role' => 'organization', 'is_deleted' => 0])->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($users) {
            $members_name = '';
            $join_date = '';
            $member_exists = array();
            if($users->opportunityOrg()->count() > 0)
            {
                foreach($users->opportunityOrg()->get() as $organization){
                    if($organization->users()->count() > 0){
                        foreach($organization->users()->get() as $member){
                            if(!in_array($member->id,$member_exists)){
                            $member_exists[] = $member->id;
                            $members_name .=  $member->first_name . ' ' . $member->last_name. ' , ';
                            $join_date .= date('m/d/Y',strtotime($member->created_at)) . ' , ';
                            }
                        }
                    }
                }
            } 
        $users->member_name = rtrim($members_name,' , ');
        $users->join_date = rtrim($join_date,' , ');
        });
        $csvExporter->build($users, ['org_name' => 'Organization Name', 'member_name' => 'Member Names', 'join_date' => 'Joining Dates'])->download('organization_members_report.csv');
    }

    public function organizationMemberPdf()
    {
        $users = User::where(['user_role' => 'organization', 'is_deleted' => 0])->get();
        if($users->count() > 0)
        {   
            foreach($users as $user)
            {
                $members_name = '';
                $join_date = '';
                $member_exists = array();
                if($user->opportunityOrg()->count() > 0)
                {
                    foreach($user->opportunityOrg()->get() as $organization){
                        if($organization->users()->count() > 0){
                            foreach($organization->users()->get() as $member){
                                if(!in_array($member->id,$member_exists)){
                                $member_exists[] = $member->id;
                                $members_name .=  $member->first_name . ' ' . $member->last_name. ' , ';
                                $join_date .= date('m/d/Y',strtotime($member->created_at)) . ' , ';
                                }
                            }
                        }
                    }
                } 
            $user->member_name = rtrim($members_name,' , ');
            $user->join_date = rtrim($join_date,' , ');
            }
        }
        $html = view('admin/reports/organization_member_pdf',compact('users'))->render();
        $options = PDF::getOptions();
        $options->setIsRemoteEnabled(true);
        return PDF::load($html)->setOptions($options)->filename('organization_members_report.pdf')->download();
    }

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    public function organizationTrackedHours()
    {
        $organizations = User::where(['user_role' => 'organization', 'is_deleted' => 0])->get(); 
        if($organizations->count() > 0)
        {
            foreach($organizations as $organization)
            {
                $organization_name = '';
                $opportunity_name = '';
                $tracked_hours = 0;    
                if($organization->tracksOrganization()->count() > 0)
                {
                    foreach($organization->tracksOrganization()->groupBy('org_id')->get() as $organization1){
                        $organization_name .= ($organization1->getOrgNameAttribute() != '') ? $organization1->getOrgNameAttribute() . ' , ' : ''; 
                    }
                    foreach($organization->tracksOrganization()->groupBy('oppor_name')->get() as $organization2){
                        $opportunity_name .= ($organization2->getOppNameAttribute() != '') ? $organization2->getOppNameAttribute() . ' , ' : '';
                    }

                    foreach($organization->tracksOrganization()->groupBy('oppor_name')->get() as $organization3){
                        if($organization3->getLoggedHours()!='')
                        $tracked_hours = $tracked_hours + $organization3->getLoggedHours();
                    }
                }
                $organization->organization_name = rtrim($organization_name,' , ');
                $organization->opportunity_name = rtrim($opportunity_name,' , ');
                $organization->tracked_hours = rtrim($tracked_hours,' , ');
            }
        }
        return view('admin/reports/organization_tracked_hours',compact('organizations'));
    }

    public function organizationTrackedHoursCSV()
    {
        $users = User::where(['user_role' => 'organization', 'is_deleted' => 0])->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($users) {
            $organization_name = '';
                $opportunity_name = '';
                $tracked_hours = 0;    
                if($users->tracksOrganization()->count() > 0)
                {
                    foreach($users->tracksOrganization()->groupBy('org_id')->get() as $organization1){
                        $organization_name .= ($organization1->getOrgNameAttribute() != '') ? $organization1->getOrgNameAttribute() . ' , ' : ''; 
                    }
                    foreach($users->tracksOrganization()->groupBy('oppor_name')->get() as $organization2){
                        $opportunity_name .= ($organization2->getOppNameAttribute() != '') ? $organization2->getOppNameAttribute() . ' , ' : '';
                    }

                    foreach($users->tracksOrganization()->groupBy('oppor_name')->get() as $organization3){
                        if($organization3->getLoggedHours()!='')
                        $tracked_hours = $tracked_hours + $organization3->getLoggedHours();
                    }
                }
                $users->organization_name = rtrim($organization_name,' , ');
                $users->opportunity_name = rtrim($opportunity_name,' , ');
                $users->tracked_hours = rtrim($tracked_hours,' , ');
        });
        $csvExporter->build($users, ['org_name' => 'Organization Name', 'opportunity_name' => 'opportunity Name', 'tracked_hours' => 'Total Tracked Hours'])->download('organization_tracked_hours_report.csv');
    }

    public function organizationTrackedHoursPdf()
    {
        $organizations = User::where(['user_role' => 'organization', 'is_deleted' => 0])->get(); 
        if($organizations->count() > 0)
        {
            foreach($organizations as $organization)
            {
                $organization_name = '';
                $opportunity_name = '';
                $tracked_hours = 0;    
                if($organization->tracksOrganization()->count() > 0)
                {
                    foreach($organization->tracksOrganization()->groupBy('org_id')->get() as $organization1){
                        $organization_name .= ($organization1->getOrgNameAttribute() != '') ? $organization1->getOrgNameAttribute() . ' , ' : ''; 
                    }
                    foreach($organization->tracksOrganization()->groupBy('oppor_name')->get() as $organization2){
                        $opportunity_name .= ($organization2->getOppNameAttribute() != '') ? $organization2->getOppNameAttribute() . ' , ' : '';
                    }
                    foreach($organization->tracksOrganization()->groupBy('oppor_name')->get() as $organization3){
                        if($organization3->getLoggedHours()!='')
                        $tracked_hours = $tracked_hours + $organization3->getLoggedHours();
                    }
                }
                $organization->organization_name = rtrim($organization_name,' , ');
                $organization->opportunity_name = rtrim($opportunity_name,' , ');
                $organization->tracked_hours = rtrim($tracked_hours,' , ');
            }
        }
        $html = view('admin/reports/organization_tracked_hours_pdf',compact('organizations'))->render();
        $options = PDF::getOptions();
        $options->setIsRemoteEnabled(true);
        return PDF::load($html)->setOptions($options)->filename('organization_tracked_hours_report.pdf')->download();
    }

    
    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    public function organizationGroups()
    {
        $organizations = User::where(['user_role' => 'organization', 'is_deleted' => 0, 'org_type' => 1])->get();
        if($organizations->count() > 0)
        {
            foreach($organizations as $organization)
            {
                $group_name = '';
                $member_name = '';
                $member_exist = array();
                if($organization->groupOrg()->count() > 0)
                {
                    foreach($organization->groupOrg()->get() as $organization1){
                        $group_name .= ($organization1->name != '') ? $organization1->name . ' , ' : '';
                        if($organization1->users()->count() > 0){
                            foreach($organization1->users()->get() as $member){
                                if(!in_array($member->id,$member_exist)){
                                    $member_name .= ((!is_null($member->first_name)) ? $member->first_name . ' ' . $member->last_name  : $member->org_name) . ' , ';
                                    $member_exist[] = $member->id;     
                                }
                            }
                        } 
                    }
                }
                $organization->group_name = rtrim($group_name,' , ');
                $organization->member_name = rtrim($member_name,' , ');
            }
        }
        return view('admin/reports/organization_groups',compact('organizations'));
    }

    public function organizationGroupsCSV()
    {
        $organizations = User::where(['user_role' => 'organization', 'is_deleted' => 0, 'org_type' => 1])->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($organizations) {
            $group_name = '';
            $member_name = '';
            $member_exist = array();
            if($organizations->groupOrg()->count() > 0)
            {
                foreach($organizations->groupOrg()->get() as $organization1){
                    $group_name .= ($organization1->name != '') ? $organization1->name . ' , ' : '';
                    if($organization1->users()->count() > 0){
                        foreach($organization1->users()->get() as $member){
                            if(!in_array($member->id,$member_exist)){
                                $member_name .= ((!is_null($member->first_name)) ? $member->first_name . ' ' . $member->last_name  : $member->org_name) . ' , ';
                                $member_exist[] = $member->id;     
                            }
                        }
                    } 
                }
            }
            $organizations->group_name = rtrim($group_name,' , ');
            $organizations->member_name = rtrim($member_name,' , ');
            $organizations->type = 'Educational Institute';
        });
        $csvExporter->build($organizations, ['org_name' => 'Organization Name', 'type' => 'Organization Type', 'group_name' => 'Groups Name', 'member_name' => 'Member Name'])->download('organization_groups_report.csv');
    }

    public function organizationGroupsPdf()
    {
        $organizations = User::where(['user_role' => 'organization', 'is_deleted' => 0, 'org_type' => 1])->get();
        if($organizations->count() > 0)
        {
            foreach($organizations as $organization)
            {
                $group_name = '';
                $member_name = '';
                $member_exist = array();
                if($organization->groupOrg()->count() > 0)
                {
                    foreach($organization->groupOrg()->get() as $organization1){
                        $group_name .= ($organization1->name != '') ? $organization1->name . ' , ' : '';
                        if($organization1->users()->count() > 0){
                            foreach($organization1->users()->get() as $member){
                                if(!in_array($member->id,$member_exist)){
                                    $member_name .= ((!is_null($member->first_name)) ? $member->first_name . ' ' . $member->last_name  : $member->org_name) . ' , ';
                                    $member_exist[] = $member->id;     
                                }
                            }
                        } 
                    }
                }
                $organization->group_name = rtrim($group_name,' , ');
                $organization->member_name = rtrim($member_name,' , ');
            }
        }
        $html = view('admin/reports/organization_groups_pdf',compact('organizations'))->render();
        $options = PDF::getOptions();
        $options->setIsRemoteEnabled(true);
        return PDF::load($html)->setOptions($options)->filename('organization_groups_report.pdf')->download();
    }

    /////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////

    public function groupsUpdate(Request $req)
    {
        $groups = Group::where('creator_id',$req->oid)->get();
        return Response::json($groups);
    }
    public function showUsers(Request $req)
    {
        $organizations = User::where(['user_role' => 'organization', 'is_deleted' => 0, 'org_type' => 1])->get();
        $group_id = $req->groupid1;
        $users_id = Group_member::where('group_id',$group_id)->get();
        $id=array();
        foreach($users_id as $u)
        {
            $id[] = $u->id;
        }
        $users = User::whereIn('id',$id)->get();
        $temp = new \stdClass();
        $temp->organization_name= $req->org_name;
        $temp->organization_type= $req->org_type;
        $temp->group_name= $req->group_name;
        $temp->group_id = $group_id;
        return view('admin/reports/organization_groups_update',compact('organizations','users','temp'));
    }
    public function showUsersPdf($organization_name,$organization_type,$group_name,$group_id)
    {
        $users_id = Group_member::where('group_id',$group_id)->get();
        $id=array();
        foreach($users_id as $u)
        {
            $id[] = $u->id;
        }
        $users = User::whereIn('id',$id)->get();
        $temp = new \stdClass();
        $temp->organization_name= $organization_name;
        $temp->organization_type= $organization_type;
        $temp->group_name= $group_name;
        $html = view('admin/reports/organization_groups_update_pdf',compact('temp','users'))->render();
        $options = PDF::getOptions();
        $options->setIsRemoteEnabled(true);
        return PDF::load($html)->setOptions($options)->filename('organization_groups_update.pdf')->download();
    }
    public function showUsersCSV($organization_name,$organization_type,$group_name,$group_id)
    {
        $temp = Group_member::where('group_id',$group_id)->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($temp) use($organization_name,$organization_type,$group_name){
            $member_name='';
            $users = User::where('id',$temp->id)->get();   
            foreach($users as $member)
            {
                $member_name .= ((!is_null($member->first_name)) ? $member->first_name . ' ' . $member->last_name  : $member->org_name) . ' , ';
            }
            $temp->member_name = rtrim($member_name,' , ');
            $temp->organization_name= $organization_name;
            $temp->organization_type= $organization_type;
            $temp->group_name= $group_name;
        });
        $csvExporter->build($temp, ['organization_name' => 'Organization Name', 'organization_type' => 'Organization Type', 'group_name' => 'Groups Name','member_name' => 'Member Names'])->download('organization_groups_update_report.csv');
    }
}

?>