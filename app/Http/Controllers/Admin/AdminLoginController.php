<?php

namespace App\Http\Controllers\admin;
use App\Opportunity;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Illuminate\Support\Facades\Log;
use Session;
use App\Http\Requests\AdminLoginRequest;
use App\Http\Requests\ForgotRequest;
use App\Models\Admin;
use Twilio\Rest\Client;

class AdminLoginController extends Controller
{
    public function index(){
        return redirect()->route('admin-login-get');
    }

    public function adminLogin(){
        $session = Auth::guard('web')->user();
        return view('admin.auths.login');
    }

    public function sendEmailNow(Request $request){

        try{

            $to = $request->email_to;
            $subject = $request->subject;
            $body = $request->body;

            \Mail::send('emails.default', ['body' => $body ,'title' => $subject], function($message) use($to ,$subject) {
                $message->to($to, 'MyVol Admin')->subject($subject);
            });

            session()->flash('app_message', 'your email has been sent successfully.');

            return back();

        }
        catch (\Exception $e){
            session()->flash('app_error', 'Error..!'.$e->getMessage());
            return back();
        }
    }

    public function sendSMSNow(Request $request){

        try{

            $sid = env('TWILIO_SID');
            $token = env('TWILIO_SECRET');

            $fromPhone = '+18035922306';

            $client = new Client($sid, $token);

            $client->messages->create(
                $request->sms_to,
                array(
                    'from' => $fromPhone,
                    'body' => $request->body
                )
            );

            session()->flash('app_message', 'your SMS has been sent successfully.');

            return back();

        }
        catch (\Exception $e){
            session()->flash('app_error', 'Error..!'.$e->getMessage());
            return back();
        }
    }
	
	public function login(AdminLoginRequest $req)
	{ 
		if(!empty($req->input())) 
		{
			$admin = Admin::where('username', $req->input('email'))->first();

			if($admin)
			{
				if(Hash::check($req->input('password'), $admin->password))
				{
					$mapData = ['username' => $req->input('email'), 'password' => $req->input('password')];

					if (Auth::guard('admin')->attempt($mapData)) {
						Auth::guard('admin')->login($admin);
					}

					$data = Auth::guard('admin')->user();

					session(['admin_role' => 'admin']);
					return redirect('/admin/dashboard');
				}

				else {
                    Log::warning('admin trying to login but with incorrect password.');
                    session()->flash('warning', 'Your given password is incorrect');
					return redirect('/admin/login');
				}
			}
			else
			{
				session()->flash('warning', 'User Not found in the system');
				return redirect('/admin/login');
			}
		}
	}
	
	public function logout(Request $req)
	{
		Auth::logout();
        $req->session()->flush();
        return redirect('/admin/login');
	}
	
	public function resetPassword()
	{		
        return view('admin/reset');
	}
	
	public function forgot(ForgotRequest $req)
	{  
		if(!empty($req->input())) 
		{
			$userData = Admin::where('username', $req->input('email'))->first();

			if(count($userData) > 0) {
				$adminModel = Admin::find($userData->id);
				$password=Hash::make($req->input('password'));
				$adminModel->password = $password;
				$adminModel->save();
				$req->session()->flash('success', 'password changed');
				return redirect('/admin/login');
			}

			else {
				$req->session()->flash('warning', 'denied');
				return redirect('/admin/resetPassword');
			}
		}
	}

	public function dashboard()
	{
	    $count['volunteer'] = User::where('user_role','volunteer')->count();
	    $count['organization'] = User::where('user_role','organization')->count();
	    $count['private_opportunity'] = Opportunity::whereNotNull('private_passcode')->count();
	    $count['public_opportunity'] = Opportunity::whereNull('private_passcode')->count();

        return view('admin.dashboard',compact('count'));
	}
}
