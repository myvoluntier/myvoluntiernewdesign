<?php
/**
 * Created by PhpStorm.
 * User: HappyBear
 * Date: 11/7/2018
 * Time: 8:11 AM
 */

namespace App\Http\Controllers\admin;


use App\Opportunity;
use App\Opportunity_category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OpportunityManageController
{
    public function opportunityList(){
        $session=Auth::guard('admin')->user();

        $oppr_list = Opportunity::where('is_deleted',0)->get();

        $inActiveCount = Opportunity::join('users', 'users.id', '=', 'opportunities.org_id')
            ->where('opportunities.is_deleted', 0)
            ->where('users.status','<>', 1)
            ->count();
        return view('admin/opportunitylist',compact('oppr_list' ,'inActiveCount'));
    }

    public function deleteOpportunity(Request $req){
        $id = $req->id;
        if($id != null){
            $oppr = Opportunity::find($id);
            $oppr->is_deleted = 1;
            $oppr->save();
            $req->session()->flash('success', 'Opportunity successfully Deleted');
            return redirect('/admin/opportunity-list');
        }
    }

    public function opportunityDeleteInActive(){

        $count = 0;

        $opprs = DB::select( DB::raw("SELECT opportunities.id FROM opportunities JOIN users ON users.id = opportunities.org_id where users.status<>1;"));

        foreach ($opprs as $opp) {

            $oppr = Opportunity::find($opp->id);
            if(!$oppr) continue;

            $oppr->is_deleted = 1;
            $oppr->save();
            $count++;
        }


        session()->flash('success', $count. ' opportunities mark as deleted.');
        return back();
    }

    public function editOpportunity(Request $req){
        $id = $req->id;
        if($id != null){
            $oppr = Opportunity::find($id);
            return view('admin/opportunity_edit',['oppr_info' => $oppr, 'opportunity_category' => Opportunity_category::all(), 'user_info' => Auth::user(), 'page_name' => '']);
        }
    }

    public function updateOpportunity(Request $request, $id)
    {
        $opportunity = Opportunity::find($id);
        $opportunity->title = $request->get('title');
        $opportunity->category_id = $request->get('opportunity_type');
        $opportunity->description = $request->get('description');
        $opportunity->min_age = $request->get('min_age');
        $opportunity->activity = $request->get('activity');
        $opportunity->qualification = $request->get('qualification');
        $opportunity->street_addr1 = $request->get('street1');
        $opportunity->street_addr2 = $request->get('street2');
        $opportunity->city = $request->get('city');
        $opportunity->state = $request->get('state');
        $opportunity->zipcode = $request->get('zipcode');
        $opportunity->additional_info = $request->get('add_info');
        $opportunity->start_date = date("Y-m-d", strtotime($request->get('start_date')));
        $opportunity->end_date = date("Y-m-d", strtotime($request->get('end_date')));
        $opportunity->start_at = $request->get('start_at');
        $opportunity->end_at = $request->get('end_at');
        $opportunity->weekdays = $request->get('weekday_vals');
        $opportunity->contact_name = $request->get('contact_name');
        $opportunity->contact_email = $request->get('contact_email');
        $opportunity->auto_accept = $request->get('auto_accept');
        $opportunity->contact_number = $request->get('contact_phone');

        $location = $this->getLocation($request->get('street1') . ', ' . $request->get('city') . ', ' . $request->get('state'));

        if ($location != 'error') {
            $opportunity->lat = $location['lat'];
            $opportunity->lng = $location['lng'];
        }

        if ($request->hasFile('file_logo')) {
            $opportunity->logo_img = fileUploadOnS3($opportunity->logo_img,$request->file_logo ,'opportunity','logo');
        }

        $opportunity->save();
        
        $request->session()->flash('success', 'Opportunity successfully Updated');

        return redirect('/admin/opportunity-list');
    }

    public function moveOpportunityImagesToS3(){

        $opportunities = Opportunity::all();
        $count = 0;

        ini_set('max_execution_time', 800); //300 seconds = 5 minutes

        foreach ($opportunities as $opportunity){

            $logo = $opportunity->logo_img;

            try{

                if(!is_null($logo) && !substr_count($logo, 'amazonaws.com')) {

                    $logoPath = asset('/uploads'). '/' . $logo;

                    $base_url = config('filesystems.disks.s3.url');

                    $serverPath = Opportunity::S3PathLogo.'/'. $logo;

                    \Storage::disk('s3')->put($serverPath, file_get_contents(preg_replace("/ /", "%20", $logoPath)), 'public');


                    $opportunity->update(['logo_img' => $base_url.$serverPath]);

                    $count++;
                }
            }

            catch (\Exception $ex) {
                log::info($ex->getMessage(). ' for the oppr id please check manually. '.$opportunity->id);
            }
        }

        log::alert($count. ' opportunities images updated successfully on the s3 amazon from directory.');

        session()->flash('success', $count. ' opportunities images updated successfully on the s3 amazon from directory.');
        return back();

    }
}