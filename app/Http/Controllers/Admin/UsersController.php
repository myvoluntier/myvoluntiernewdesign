<?php

namespace App\Http\Controllers\Admin;

use App\Mail\UsersDefaultMail;
use App\Models\ApiAccess;
use App\Models\EmailsTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;

use App\Mail\TestEmail;
use Illuminate\Support\Facades\DB;
use Auth;
use Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Mail;
use Session;
use App\User;
use App\Usertemp;
use App\Group;
use App\Group_member;
use App\School_type;
use App\Organization_type;
use Image;

///////////////////////////////ADMIN SIDE/////////////////////

class UsersController extends Controller
{

    public function volunteer_listing()
    {
        $sortingColumns = User::sortingColumns();

        $heading = 'Manage Volunteer';

        $query = User::where('user_role', 'volunteer');

        $search = Input::get('search');
        $sorting = Input::get('sorting') ?? 'created_at';

        if ($search)
            $query->searchingUser($search);

        $list = (clone $query)->orderBy($sorting, 'ASC')->paginate(100);

        if ($search || $sorting) {
            $list->appends(['search' => $search, 'sorting' => $sorting,]);
        }

        $user = DB::table('user_temp')->get();

        if (count($user)) {
            DB::table('user_temp')->delete();
        }

        $type = 'volunteer';

        return view('admin.volunteers-organization.index', compact('heading', 'list', 'type' ,'sortingColumns'));

    }

    public function organization_listing($type = 'all')
    {
        $sortingColumns = User::sortingColumns();

        $heading = 'Manage Organizations';
        $query = User::with('apiAccess')->where('user_role', 'organization');

        if ($type == 'inactive') {
            $heading = 'Manage Inactive Organizations';
            $query = User::where('user_role', 'organization')
                ->where('is_deleted', '0')
                ->where(function ($q) {
                    $q->where('status', 0)
                    ->orwhere('confirm_code', '<>', '1')
                    ->orwhere('declined', '<>', '0');
                })
                ->whereNull('parent_id'); //Exclude Sub-Orgs from list because Parent Orgs confirm Sub-Orgs
        }

        $search = Input::get('search');
        $sorting = Input::get('sorting') ?? 'created_at';

        if ($search)
            $query->searchingUser($search);
        $list = (clone $query)->orderBy($sorting, 'ASC')->paginate(100);

        if ($search || $sorting) {
            $list->appends(['search' => $search, 'sorting' => $sorting,]);
        }

        $user = DB::table('user_temp')->get();

        if (count($user)) {
            DB::table('user_temp')->delete();
        }

        $type = 'organization';
     
        //code repeating in user voll type too same code
        return view('admin.volunteers-organization.index', compact('heading', 'list', 'type' ,'sortingColumns'));

    }


    public function updateUserStatuses(Request $request)
    {

        try {

            $column = $request->column;
            $user = User::findOrFail($request->id);
            if($column == 'declined' && $user->declined == '0'){
                $user->status = '0';
            }
            $user->$column = $user->$column == '1' ? '0' : '1';
            
            $user->save();

            $this->sendEmailForUserStatusUpdate($column, $user);
            session()->flash('app_message', 'Users data has been updated');

            return back();
        } catch (\Exception $e) {
            Log::error('Error in update status ' . $e->getMessage());
            session()->flash('app_error', 'Something went wrong. ' . $e->getMessage());
            return back();
        }
    }

    public function sendEmailForUserStatusUpdate($column, $user, $type = 'confirm_email')
    {
        //check type in case of one more email your account is confirmed etc...

        $templateType = 'userStatusUpdate';

        $data['name'] = '';

        if ($user->user_role == 'volunteer')
            $data['name'] = $user->first_name . ' ' . $user->last_name;
        else
            $data['name'] = $user->org_name;

        if ($column == 'status')
            $data['status'] = $user->status == '0' ? 'Inactive' : 'Active';
        elseif ($type == 'domain') {
            $data['status'] = $user->$column == '0' ? 'Inactive' : 'Confirmed';
        } else {
            $data['status'] = $user->confirm_code == '0' ? 'Inactive' : 'Confirmed';
        }

        $email = EmailsTemplate::where('type', $templateType)->first();

        $body = replaceBodyContents($email->body, $email->type, $user, null, null, $data);

        $messageBody = replaceTextBodyContents($email->text_body, $email->type, $user, null, null, $data);

        if ($type == 'domain') {
            $body = str_replace('status', 'domain', $body);
            $messageBody = str_replace('status', 'domain', $messageBody);
        }

        sendSMSByTwilio($user->contact_number, $messageBody);

        \Mail::to($user->email)->send(new UsersDefaultMail($body, $email->title));

    }

    public function manageUser($type = null, $id = null)
    {

        if (Auth::loginUsingId($id)) {
            session(['admin_id' => $id]);
            if ($type == "vol") {
                return redirect()->to('/volunteer/');
            } elseif ($type == "org") {
                return redirect()->to('/organization/');
            }
        }
    }

    public function pending()
    {
        $session = Auth::guard('admin')->user();
        $userlist = DB::table('users')
        ->where('approval', 'PENDING')
        ->orwhere('approval_org', 'PENDING')
        ->orderBy('is_deleted', 'ASC')->get();
        return view('admin/userlist')->with('list', $userlist);
    }

    public function deleting(Request $req)
    {

        $userModel = User::find($req->id);
        $userModel->is_deleted = 1;
        $userModel->save();
        $req->session()->flash('success', 'Data successfully Deleted');
        if ($userModel->user_role == "organization") {
            return redirect('/admin/organization-list');
        } else {
            return redirect('/admin/volunteer-list');
        }
    }

    public function editing(Request $req)
    {

        $usermodel = User::find($req->id);
        $School_type = School_type::all()->pluck('id', 'school_type')->prepend('', 'Select');
        $organizationType = Organization_type::all()->pluck('id', 'organization_type')->prepend('', 'Select');

        return view('admin.user_edit', compact('usermodel', 'School_type', 'organizationType'));
    }

    public function editPost(Request $req)
    {

        if (!empty($req->input())) {

            $userModel = User::find($req->input('edit_user_id'));

            if ($req->input('edit_user_role') == 'organization') {
                $userModel->user_name = $req->input('user_name');
                $userModel->org_name = $req->input('org_name');
                $userModel->school_type = $req->input('school_type');
                $userModel->logo_img = $req->input('logo_img');
                $userModel->contact_number = $req->input('contact_number');
                $userModel->birth_date = $req->input('birth_date');
                $userModel->ein = $req->input('ein');
                $userModel->org_type = $req->input('org_type');
                $userModel->nonprofit_org_type = $req->input('nonprofit_org_type');
                //$userModel->location = $req->input('location');
                $userModel->zipcode = $req->input('zipcode');
                $location = $this->getLocation($req->input('zipcode'));
                $address = $this->getAddress($req->input('zipcode'));

                if ($location != 'error') {
                    $userModel->lat = $location['lat'];
                    $userModel->lng = $location['lng'];
                }
                if ($address != 'error') {
                    $userModel->city = $address['city'];
                    $userModel->state = $address['state'];
                    $userModel->country = $address['country'];
                }

                if ($req->file('logo_img')) {
                    $userModel->logo_img = fileUploadOnS3($userModel->logo_img, $req->logo_img, User::S3PathLogo, 'logo');
                    //croping resize(100, 100
                }

                $userModel->brif = $req->input('brif');

            } else {
                $userModel->user_name = $req->input('user_name');
                $userModel->first_name = $req->input('first_name');
                $userModel->last_name = $req->input('last_name');
                $userModel->gender = $req->input('gender');
                $userModel->birth_date = $req->input('birth_date');
                $userModel->contact_number = $req->input('contact_number');
                $userModel->zipcode = $req->input('zipcode');
                $location = $this->getLocation($req->input('zipcode'));
                $address = $this->getAddress($req->input('zipcode'));

                if ($location != 'error') {
                    $userModel->lat = $location['lat'];
                    $userModel->lng = $location['lng'];
                }

                if ($address != 'error') {
                    $userModel->city = $address['city'];
                    $userModel->state = $address['state'];
                    $userModel->country = $address['country'];
                }

                if ($req->file('logo_img')) {
                    $userModel->logo_img = fileUploadOnS3($userModel->logo_img, $req->logo_img, User::S3PathLogo, 'logo');
                    //croping resize(100, 100
                }

                $userModel->brif = $req->input('brif');
            }

            $userModel->location = $req->input('location');
            $userModel->website = $req->input('website');
            $userModel->facebook_url = $req->input('facebook_url');
            $userModel->twitter_url = $req->input('twitter_url');
            $userModel->linkedin_url = $req->input('linkedin_url');
            $userModel->save();

            $req->session()->flash('success', 'Data successfully Updated');
            return redirect('/admin/pending');
        }
    }

    public function moveUsersImagesToS3()
    {

        $users = User::all();

        $count = 0;

        ini_set('max_execution_time', 800); //300 seconds = 5 minutes

        foreach ($users as $user) {

            $logo = $user->logo_img;

            $banner = $user->back_img;

            try {

                if (!is_null($logo) && !substr_count($logo, 'amazonaws.com')) {

                    $logoPath = asset('/uploads/') . '/' . $logo;

                    $base_url = config('filesystems.disks.s3.url');

                    $serverPath = User::S3PathLogo . '/' . $logo;

                    \Storage::disk('s3')->put($serverPath, file_get_contents(preg_replace("/ /", "%20", $logoPath)), 'public');

                    $user->update(['logo_img' => $base_url . $serverPath]);

                    $count++;
                }


                if (!is_null($banner) && !substr_count($banner, 'amazonaws.com')) {

                    $bannerPath = asset('/uploads') . '/' . $logo;

                    $base_url = config('filesystems.disks.s3.url');

                    $serverPath = User::S3PathBan . '/' . $banner;

                    \Storage::disk('s3')->put($serverPath, file_get_contents(preg_replace("/ /", "%20", $bannerPath)), 'public');

                    $user->update(['back_img' => $base_url . $serverPath]);

                }
            } catch (\Exception $ex) {
                log::info($ex->getMessage() . ' for the user id please check manually. ' . $user->id);
            }
        }

        log::alert($count . ' users images updated successfully on the s3 amazon from directory.');

        session()->flash('app_success', $count . ' users images uploaded successfully on the s3 amazon from directory.');

        return back();

    }

    public function multipleDelete(Request $req)
    {

        $ids = explode(',', $req->input('ids'));

        foreach ($ids as $id) {
            $userModel = User::find($id);
            $userModel->is_deleted = 1;
            $userModel->save();
        }

        $req->session()->flash('success', 'Data successfully Deleted');
        return redirect('/admin/user-list');

    }

    public function importExportExcelORCSV()
    {
        return view('admin.users.file_import_export');
    }

    public function importFileToShowRecords(Request $request)
    {

        $dataArray = [];
        if ($request->hasFile('sample_file')) {
            $path = $request->file('sample_file')->getRealPath();
            $extension = $request->file('sample_file')->getClientOriginalExtension();

            if ($extension == 'xls' || $extension == 'xlsx') {
                $dataArray = \Excel::load($path)->get();
                \Excel::load($path)->setFilename('excel_file')->store($extension, public_path('temp'));
            }
        } else if ($request->data) {

            $dataArray = $this->parseTextCSVData($request->data);

            \Excel::create('file', function ($excel) use ($dataArray) {
                $excel->sheet('excel_file', function ($sheet) use ($dataArray) {
                    $sheet->fromArray($dataArray);
                });
            })->setFilename('excel_file')->store('xls', public_path('temp'));

            $dataArray = \Excel::load(public_path('temp/excel_file.xls'))->get();
        }

        if (count($dataArray) > 0 && $this->validData($dataArray))
            return view('admin.users.file_load', compact('dataArray'));

        session()->flash('app_error', 'Please select file or insert valid data in Text Box');

        return back(); //show error please fill data first
    }

    public function validData($data)
    {

        $response = false;
        foreach ($data->toArray() as $array) {
            $response = count($array) == 22 ? true : false;
        }

        return $response;
    }

    public function importFileIntoDB(Request $request)
    {

        $ignore_indexes = explode(',', $request->ignore_indexes);

        $totalInsertions = 0;

        $data = \Excel::load(public_path('temp/excel_file.xls'))->get();
        if ($data->count()) {
            foreach ($data as $key => $val) {

                if (!empty($ignore_indexes[0]) && in_array($key, $ignore_indexes)) continue;

                $array = json_decode(json_encode($val), true);
                $user = User::where('email', $array['email'])->count();

                if ($user == 0) {
                    $userModel = new User();

                    if ($array['user_role'] == 'organization') {
                        $userModel->org_name = $array['org_name'];
                        $userModel->org_type = $array['org_type'];
                        $userModel->school_type = $array['school_type'];
                        $userModel->nonprofit_org_type = $array['nonprofit_org_type'];
                    } else {
                        $userModel->first_name = $array['first_name'];
                        $userModel->last_name = $array['last_name'];
                        $userModel->gender = $array['gender'];
                    }

                    $userModel->ein = $array['ein'];
                    $userModel->user_role = $array['user_role'];
                    $userModel->user_name = $array['user_name'];
                    $userModel->email = $array['email'];
                    $userModel->birth_date = $array['birth_date'];
                    $userModel->password = Hash::make($array['password']);
                    $userModel->zipcode = $array['zipcode'];
                    $userModel->location = $array['location'];
                    $userModel->city = $array['city'];
                    $userModel->state = $array['state'];
                    $userModel->country = $array['country'];
                    $userModel->lat = $array['lat'];
                    $userModel->lng = $array['lng'];
                    $userModel->contact_number = $array['contact_number'];
                    //$userModel->brif  =  $array['brif'];
                    //$userModel->website  =  $array['website'];
                    //$userModel->facebook_url  =  $array['facebook_url'];
                    //$userModel->twitter_url =  $array['twitter_url'];
                    //$userModel->linkedin_url =  $array['linkedin_url'];
                    //$userModel->status =  1;
                    //$userModel->is_deleted =  0;

                    if ($userModel->save()) {
                        $totalInsertions++;
                    }
                }
            }
        }

        if ($totalInsertions == 0) {
            session()->flash('app_error', 'Members insertion failed due to repetition of emails');
        } else {
            session()->flash('app_message', 'New members successfully imported.');
        }

        return redirect('/admin/volunteer-list');
    }

    public function parseTextCSVData($data)
    {
        $array_csv = [];
        $final = [];

        try {

            $lines = explode("\n", $data);
            $heading = str_getcsv($lines[0]);

            foreach ($lines as $line) {
                $array_csv[] = str_getcsv($line);
            }

            foreach ($array_csv as $index => $array) {
                if ($index == 0) continue;
                $final [] = (array_combine($heading, $array_csv[$index]));
            }

            return $final;

        } catch (\Exception $e) {
            return [];
        }
    }

    public function showUserTemData(Request $req)
    {
        $list = Usertemp::get();
        $grouplist = Group::get();
        $userCount = Usertemp::get()->count();
        return view('admin.showUserTemData', compact('list', 'userCount', 'grouplist'));
    }

    public function showUserTemDataPost(Request $req)
    {
        $group = $req->input('group');
        $select_group = $req->input('select_group');
        $list = Usertemp::get();
        $userCount = Usertemp::get()->count();
        if ($userCount > 0) {
            foreach ($list as $key => $value) {
                $userModel = new User;
                $groupmember = new Group_member;

                $json = json_encode($value);
                $value = json_decode($json, true);
                $userModel->org_name = $value['org_name'];
                $userModel->org_type = $value['org_type'];
                $userModel->school_type = $value['school_type'];
                $userModel->nonprofit_org_type = $value['nonprofit_org_type'];

                $userModel->first_name = $value['first_name'];
                $userModel->last_name = $value['last_name'];
                $userModel->gender = $value['gender'];
                $userModel->ein = $value['ein'];
                $userModel->user_role = $value['user_role'];
                $userModel->user_name = $value['user_name'];
                $userModel->email = $value['email'];
                $userModel->confirm_code = 1;
                $userModel->birth_date = $value['birth_date'];
                $userModel->password = $value['password'];
                $userModel->zipcode = $value['zipcode'];
                $userModel->location = $value['location'];
                $userModel->city = $value['city'];
                $userModel->state = $value['state'];
                $userModel->country = $value['country'];
                $userModel->lat = $value['lat'];
                $userModel->lng = $value['lng'];
                $userModel->contact_number = $value['contact_number'];
                //print_r($userModel);
                $userModel->save();
                $lastInsertId = $userModel->id;
                if ($group == 1) {
                    $Group_member = Group_member::where('group_id', $select_group)->where('user_id', $lastInsertId)->count();
                    if ($Group_member == 0) {
                        $groupmember->group_id = $select_group;
                        $groupmember->user_id = $lastInsertId;
                        $groupmember->role_id = 2;
                        $groupmember->status = 2;
                        $groupmember->created_at = date('Y-m-d H:i:s');
                        $groupmember->updated_at = date('Y-m-d H:i:s');
                        $groupmember->is_deleted = 0;
                        $groupmember->save();
                    }
                }
                Usertemp::where('id', $value['id'])->delete();
            }
            return redirect('/admin/user-list');
        }
    }

    public function cancelUser()
    {
        Usertemp::truncate();

        return redirect('/admin/user-list');
    }

    public function getLocation($zipcode)
    {
        /*get location from zipcode*/
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $zipcode . "&key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es";
        $result = json_decode(file_get_contents($url), true);

        if ($result['results'] == []) {
            return 'error';
        } else {
            return $result['results'][0]['geometry']['location'];
        }
    }

    public function getAddress($zipcode)
    {
        /*get address from zipcode*/
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $zipcode . "&key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es";
        $json = json_decode(file_get_contents($url), true);
        $city = '';
        $state = '';
        $country = '';

        if ($json['results'] == []) {
            return 'error';
        } else {
            foreach ($json['results'] as $result) {
                foreach ($result['address_components'] as $addressPart) {
                    if ((in_array('locality', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
                        $city = $addressPart['long_name'];
                    else if ((in_array('administrative_area_level_1', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
                        $state = $addressPart['short_name'];
                    else if ((in_array('country', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
                        $country = $addressPart['long_name'];
                }
            }
            $address = array();
            $address['city'] = $city;
            $address['state'] = $state;
            $address['country'] = $country;

            return $address;
        }
    }

    public function approve(Request $req)
    {
        $userModel = User::find($req->input('edit_user_id'));
        if($userModel->user_role == 'organization'){
            $userModel->org_name = $userModel->temp_org_name;
            $userModel->temp_org_name = NULL;
            $userModel->approval_org = 'APPROVED'; 
        }else{
            $email = $userModel->email;
            $userModel->first_name = $userModel->temp_first_name;
            $userModel->last_name = $userModel->temp_last_name;
            $userModel->temp_first_name = NULL;
            $userModel->temp_last_name = NULL;
            $userModel->approval = 'APPROVED'; 
        }
       
        $userModel->save();
        // $data = ['message' => 'Your name request is approved!'];
        // Mail::to($email)->send(new TestEmail($data));
        $req->session()->flash('success', 'Requested data successfully approved');
        return redirect('/admin/pending');
    }

    public function reject(Request $req)
    {
        $userModel = User::find($req->input('edit_user_id'));
        if($userModel->user_role == 'organization'){
            $userModel->temp_org_name = NULL;
            $userModel->approval_org = 'REJECTED';
        }else{
            $email = $userModel->email;
            $userModel->temp_first_name = NULL;
            $userModel->temp_last_name = NULL;
            $userModel->approval = 'REJECTED';
        }
        $userModel->save();
        // $data = ['message' => 'Your name request is rejected. Try sometime again....!'];
        // Mail::to($email)->send(new TestEmail($data));
        $req->session()->flash('success', 'Requested data rejected');
        return redirect('/admin/pending');
    }

    public function updateUserDomains(Request $request)
    {

        try {

            $column = $request->column;
            if($column == 'is_allow_user_sync_attr'){

                $apiAccess = ApiAccess::firstOrCreate([
                    'org_id' => $request->id
                ], [
                    'is_allow_user_sync_attr' => '0'
                ]);

                $apiAccess->$column = $apiAccess->$column == '1' ? '0' : '1';
                $apiAccess->save();

            }
            else{

                $user = User::findOrFail($request->id);
                $user->$column = $user->$column == '1' ? '0' : '1';
                $user->save();

                $this->sendEmailForUserStatusUpdate($column, $user, 'domain');
            }

            session()->flash('app_message', 'Users data has been updated');

            return back();
        } catch (\Exception $e) {
            Log::error('Error in update status ' . $e->getMessage());
            session()->flash('app_error', 'Something went wrong. ' . $e->getMessage());
            return back();
        }
    }
    /* admin Email Send  */
    public function adminEmailSend(Request $request){
       
        $userId = $request->input('userid');
        $message =  trim($request->input('message'));
        if(!empty($userId) && !empty($message)){
            $user = User::findOrFail($userId);
            $this->sendEmail($message,$user);
            
        }else{
            $errorMsg = 'Missing email or message.';
            return Response::json(['result' => 'error', 'message'=>$errorMsg]);
        }
        return Response::json(['result' => 'success']);
    }
    //Method to send email for service project 
    public function sendEmail($message, $user) {
        $userAuth = Auth::user();
        $fromName = 'Administrator';
        $subject = 'Myvoluntier : Organization Email';
        $data['content'] = $message;
        $data['sender_name'] = $fromName;
        $data['sender_email'] = 'contact@myvoluntier.com';
        $email = EmailsTemplate::where('type', 'adminEmailSendTemplate')->first();
        $body = replaceBodyContents($email->body, $email->type, $user, null, null, $data);
       
        \Mail::to($user->email)->send(new UsersDefaultMail($body, $subject));

    }
}
