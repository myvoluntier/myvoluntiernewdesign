<?php

namespace App\Http\Controllers\Organization;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Alert;
use App\Friend;
use App\User;
use App\Chat;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Pagination\Paginator;
use App\Services\ChatManager;
use App\Services\ChatService;

class FriendCtrl extends Controller
{
    public function viewFriendPage()
    {
        if(session('is_delegate')) abort(403);

        //this is for the ORGANIZATION todo:same code used in the FriendsController of VOL 99% same
        //in any do change in this function please do it in FriendsController of ORG.
        $user_id = Auth::user()->id;

        $query = DB::table('users')
            ->where('users.is_deleted', '<>', 1)
            ->where('users.confirm_code', 1);

        $new_request = (clone $query)
            ->join('friends', 'users.id', '=', 'friends.user_id')
            ->where('friends.friend_id', $user_id)
            ->where('friends.is_deleted', '<>', 1)
            ->where('friends.status', 1)
            ->select('users.*', 'friends.*')
            ->get();

        $friends = (clone $query)
            ->join('friends', 'users.id', '=', 'friends.friend_id')
            ->where('friends.user_id', $user_id)
            ->where('friends.is_deleted', '<>', 1)
            ->where('friends.status', 2)
            ->select('users.*', 'friends.*')
            ->paginate(12);

        $OtherVolunteers = (clone $query)
            ->where('users.user_role', 'volunteer')
            ->inRandomOrder()->limit(10)->get();

        $OtherOrgs = (clone $query)
            ->where('users.user_role', 'organization')
            ->inRandomOrder()->limit(10)->get();


        return view('organization.friend', [
            'page_name' => 'org_friend',
            'nameClassUl' => 'friends-list-main',
            'friend_requests' => $new_request,
            'friends' => $friends,
            'user_id' => $user_id,
            'OtherVolunteers' => $OtherVolunteers,
            'OtherOrgs' => $OtherOrgs
            ]);

        //AS volunteers friends page
    }


    public function getFriend(Request $request)
    {
        //this is for the ORGNIZATION todo:same code used in the FriendsController of VOL 99% same
        //in any do change in this function please do it in FriendsController of ORG.

        $keyword = $request->get('keyword');
        $query = DB::table('users')
            ->where('users.is_deleted', '<>', 1)
            ->where('users.confirm_code', 1)
            ->where(function ($query) use ($keyword) {
                $query->where("users.first_name", "LIKE", "%$keyword%")
                    ->orWhere("users.last_name", "LIKE", "%$keyword%")
                    ->orWhere("users.org_name", "LIKE", "%$keyword%");
            });
            
        $new_request =  DB::table('users')
            ->where('users.is_deleted', '<>', 1)
            ->where('users.confirm_code', 1)
            ->join('friends', 'users.id', '=', 'friends.user_id')
            ->where('friends.friend_id', Auth::user()->id)
            ->where('friends.is_deleted', '<>', 1)
            ->where('friends.status', 1)
            ->select('users.*', 'friends.*')
            ->get();

        $friends = (clone $query)
            ->join('friends', 'users.id', '=', 'friends.friend_id')
            ->where('friends.user_id', Auth::user()->id)
            ->where('friends.is_deleted', '<>', 1)
            ->where('friends.status', 2)
            ->select('users.*', 'friends.*')
            ->paginate(12);

        $OtherVolunteers = (clone $query)
            ->where('users.user_role', 'volunteer')
            ->inRandomOrder()->limit(10)->get();

        $OtherOrgs = (clone $query)
            ->where('users.user_role', 'organization')
            ->inRandomOrder()->limit(10)->get();

        return view('organization._friendsUserList', [
            'page_name' => 'org_friend',
            'nameClassUl' => 'friends-list-search',
            'friend_requests' => $new_request,
            'friends' => $friends,
            'OtherVolunteers' => $OtherVolunteers,
            'OtherOrgs' => $OtherOrgs
        ]);

    }

    public function accept_reject(Request $request)
    {
        $user_id = Auth::user()->id;
        $id = $request->sender;

        $friend = Friend::find($request->id);
        $post_user_id = $request->user_id;
        $post_friend_id = $request->sender;
        $type = $request->type;
        $status = $request->status;

        if ($user_id != $post_user_id) {
            return Response::json(['error' => 'Something error occurred. Try again later.',
                'success' => '0']);
        }

        if ($status == 2) {

            $friend->status = $request->status;
            $friend->save();
            $update = Friend::where('friend_id', '=', $post_friend_id)
                ->where('user_id', '=', $user_id)
                ->where('status', '=', 3);
            $update->update(['status' => '2']);

            $chatManager = new ChatManager();
            $chatManager->join($id);

            return Response::json(['error' => '', 'success' => '1']);
        }
        if ($status == 0) {
            $friend->is_deleted = 1;
            $friend->save();
            Alert::where('receiver_id', $user_id)->where('sender_id', $post_friend_id)->delete();
            Alert::where('receiver_id', $post_friend_id)->where('sender_id', $user_id)->delete();
            if ($type == 1) {
                $update = Friend::where('user_id', '=', $user_id)
                    ->where('friend_id', '=', $post_friend_id)
                    ->update(['is_deleted' => 1]);
            } else {
                $update = Friend::where('user_id', '=', $post_friend_id)
                    ->where('friend_id', '=', $user_id)
                    ->update(['is_deleted' => 1]);

                /*$chatManager = new ChatManager;
                $removed = $chatManager->removeUserFromGroup($id);
                $chatManager->deleteChat($removed);*/
            }
            //$chat = Chat::where('user_id', $user_id)->whereNull('to_user_id')->first();
            //if($chat != null){
            //}
            return Response::json(['error' => '', 'success' => '1']);
        }
        return Response::json(['error' => 'Something error occurred. Try again later..',
            'success' => '0']);
    }
}
