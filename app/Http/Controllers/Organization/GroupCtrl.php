<?php

namespace App\Http\Controllers\Organization;

use App\Chat;
use App\Customattributes;
use App\Events\DynamicGroupMembersUpdateEvent;
use App\Follow;
use App\Friend;
use App\Group;
use App\Group_member;
use App\Group_member_role;
use App\Group_category;
use App\Models\Attributelookup;
use App\Services\ChatService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;
use App\CommonModel;
use App\Alert;
use Image;
use Validator;
use DB;
use App\Services\ImageExif;

class GroupCtrl extends Controller
{
    public $where = [];
    public $whereOr = [];
    public $condition = null;

    public function operatorsUI(){

        return [
            'equal' => '=',
            'not_equal'=>  '!='  ,
            'greater' => '>' ,
            'less'=> '<' ,
            'greater_or_equal' => '>=',
            'less_or_equal' => '<='  ,
        ];

    }

    function msort($array, $key, $sort_flags = SORT_REGULAR)
    {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';

                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v->$key_key;
                        }

                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }

                arsort($mapping, $sort_flags);
                $sorted = array();

                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }

                return $sorted;
            }
        }

        return $array;
    }

//    public function viewCertainGroupPage($id = null){
//
//        $session = Auth::guard('web')->user();
//        $allGroupLists = CommonModel::getAll('group_members', array(array('group_members.is_deleted', '=', 0), array('group_members.status', '=', 2)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');
//        $groupLists = CommonModel::getAll('group_members', array(array('user_id', '=', $session->id), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');
//
//        /**********************FOR ALL GROUP***********************/
//
//        $allGroupList = array();
//
//        if (count($allGroupLists) > 0) {
//            foreach ($allGroupLists as $keys => $values) {
//                $total_hours = 0;
//                $allGroupList[] = $values;
//                $allGroupList[$keys]->tracked_hours = 0;
//                $occations = CommonModel::getAll('group_members', array(array('group_id', '=', $values->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id')));
//
//                if (count($occations) > 0) {
//                    foreach ($occations as $occation) {
//                        $hours = CommonModel::getAll('tracked_hours', array(array('volunteer_id', '=', $occation->user_id), array('approv_status', '=', 1)), '', 'logged_mins', '', '');
//                        if (count($hours) > 0) {
//                            foreach ($hours as $track) {
//                                $total_hours = $total_hours + $track->logged_mins;
//                            }
//                        }
//                    }
//                }
//                $allGroupList[$keys]->tracked_hours = $total_hours;
//            }
//        }
//
//        $allGroupList = $this->msort($allGroupList, array('tracked_hours'));
//
//        /**********************FOR ALL GROUP***********************/
//
//        $groupList = array();
//
//        if (count($groupLists) > 0) {
//            foreach ($groupLists as $key => $value) {
//
//                $total_hours = 0;
//                $groupList[] = $value;
//                $groupList[$key]->members = array();
//                $groupList[$key]->tracked_hours = 0;
//                $groupList[$key]->datewise = array();
//                $groupList[$key]->sixmonthwise = array();
//                $groupList[$key]->lastYearwise = array();
//                $groupList[$key]->Yearwise = array();
//                $groupList[$key]->CountryWiseCountlastMonth = array();
//                $groupList[$key]->groupStateWiseCountlastMonth = array();
//
//                $groupList[$key]->creatorDetails = CommonModel::getAllRow('users', array(array('id', '=', $value->creator_id)));
//
//                $occations = CommonModel::getAll('group_members', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id'), array('reviews', 'reviews.review_to', 'users.id')), '', '', array('group_members.id'));
//
//                if (count($occations) > 0) {
//                    foreach ($occations as $occation) {
//                        $hours = CommonModel::getAll('tracked_hours', array(array('volunteer_id', '=', $occation->user_id),
//                            array('approv_status', '=', 1)), '', 'logged_mins', '', '');
//
//                        if (count($hours) > 0) {
//                            $occation->impact = 0;
//                            foreach ($hours as $track) {
//                                $occation->impact += $track->logged_mins;
//                                $total_hours = $total_hours + $track->logged_mins;
//                            }
//                        }
//                        else{
//                            $occation->impact = 0;
//                        }
//                        $groupList[$key]->members[] = $occation;
//
//                        //todo: needs to replace with $value->getGroupMembersData()
//                    }
//                }
//
//                $groupList[$key]->tracked_hours = $total_hours;
//                $rank = $this->myfunction($allGroupList, 'group_id', $groupList[$key]->group_id);
//                $groupList[$key]->rank = $rank;
//                $lastmonthFirstDay = date('Y-m-d', strtotime("first day of -1 month"));
//                $lastmonthlastDay = date('Y-m-d', strtotime("last day of -1 month"));
//                $groupList[$key]->datewise = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthFirstDay), array('logged_date', '<=', $lastmonthlastDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'logged_date', array('logged_date', 'SUM' => 'logged_mins'));
//
//                $sixmonthFirstDay = date('Y-m-d', strtotime("first day of -6 month"));
//                $currentDay = date('Y-m-d');
//                $groupList[$key]->sixmonthwise = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $sixmonthFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));
//                $lastYearFirstDay = date('Y-m-d', strtotime("first day of -12 month"));
//
//                $currentDay = date('Y-m-d');
//                $groupList[$key]->lastYearwise = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));
//
//                $currentDay = date('Y-m-d');
//
//                $groupList[$key]->Yearwise = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'YEAR', array('logged_date', 'SUM' => 'logged_mins', 'YEAR' => 'logged_date'));
//                $groupList[$key]->CountryWiseCountlastMonth = $CountryWiseCountlastMonth = CommonModel::getAll('groups', array(array('country', '=', $groupList[$key]->creatorDetails->country)), array(array('users', 'users.id', 'groups.creator_id')), 'groups.id', '', '', '', 0, 10);
//                $groupList[$key]->CountryWiseCountlastMonth5 = $CountryWiseCountlastMonth5 = CommonModel::getAll('groups', array(array('state', '=', $groupList[$key]->creatorDetails->state)), array(array('users', 'users.id', 'groups.creator_id')), 'groups.id', '', '', '', 0, 5);
//                $groupList[$key]->arr = array();
//                $groupList[$key]->month = array();
//                $groupList[$key]->arr5 = array();
//                $groupList[$key]->month5 = array();
//
//                if (count($CountryWiseCountlastMonth) > 0) {
//                    foreach ($CountryWiseCountlastMonth as $k => $groups) {
//                        $groupList[$key]->yearly[$k]['country'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));
//                        if (count($groupList[$key]->yearly[$k]['country']) > 0) {
//                            array_push($groupList[$key]->arr, $groups->id);
//                        }
//
//                        $groupList[$key]->monthly[$k]['month'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthlastDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));
//                        if (count($groupList[$key]->monthly[$k]['month']) > 0) {
//                            array_push($groupList[$key]->month, $groups->id);
//                        }
//                    }
//                }
//
//                if (count($CountryWiseCountlastMonth5) > 0) {
//                    foreach ($CountryWiseCountlastMonth5 as $k => $groups) {
//                        $groupList[$key]->yearly[$k]['country'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));
//
//                        if (count($groupList[$key]->yearly[$k]['country']) > 0) {
//                            array_push($groupList[$key]->arr5, $groups->id);
//                        }
//
//                        $groupList[$key]->monthly[$k]['month'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthlastDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));
//
//                        if (count($groupList[$key]->monthly[$k]['month']) > 0) {
//                            array_push($groupList[$key]->month5, $groups->id);
//                        }
//                    }
//                }
//
//                $groupList[$key]->volun = array();
//                $groupList[$key]->volun5 = array();
//                $volunteer = CommonModel::getAll('group_members', array(array('country', '=', $groupList[$key]->creatorDetails->country), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id'), array('users', 'users.id', 'group_members.user_id')), '', 'SUM', 'group_id', array('group_id', 'SUM' => 'group_id'), 0, 10);
//                $volunteer5 = CommonModel::getAll('group_members', array(array('state', '=', $groupList[$key]->creatorDetails->state), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id'), array('users', 'users.id', 'group_members.user_id')), '', 'SUM', 'group_id', array('group_id', 'SUM' => 'group_id'), 0, 5);
//
//                if (count($volunteer) > 0) {
//                    foreach ($volunteer as $ks => $volun) {
//                        array_push($groupList[$key]->volun, $volun->group_id);
//                    }
//                }
//
//                if (count($volunteer5) > 0) {
//                    foreach ($volunteer as $ks => $volun) {
//                        array_push($groupList[$key]->volun5, $volun->group_id);
//                    }
//                }
//            }
//        }
//
//        return view('organization.groups', ['page_name' => 'org_group'], compact('groupList'));
//    }

    function myfunction($products, $field, $value)
    {
        foreach ($products as $key => $product) {
            if ($product->$field === $value)
                return $key + 1;
        }

        return false;
    }

    public function dGroupDomainAttributes(Request $request){

        if(is_null($request->domains))
            return response()->json(['success' => false,'response'=>[]]);

        $final = $this->getFiltersForUi($request->domains);

        return response()->json(['success' => true, 'response' => $final]);
    }

    public function getFiltersForUi($domains){

        $allVolunteersQuery = User::where('user_role' ,'volunteer');

        foreach ($domains as $index => $domain){

            if($index == 0)
                $allVolunteersQuery->where('email', 'like', "%{$domain}%");
            else
                $allVolunteersQuery->OrWhere('email', 'like', "%{$domain}%");
        }

        $attributesKeys = Attributelookup::pluck('attributename' ,'attributekey')->toArray();

        $allVolunteersIds = $allVolunteersQuery->pluck('id')->toArray();

        $customAttributesQuery = Customattributes::whereIn('userid' ,$allVolunteersIds);
        $final = [];

        foreach ($attributesKeys as $key => $name){

            $dt = (clone $customAttributesQuery)->where('attributekey' ,$key)->pluck('attributevalue')->toArray();

            $final [] = [
                'id' => $key,
                'label' => $name,
                'type' => 'string',
                'plugin' => 'select2',
                'plugin_config' => [
                    'multiple' => "",
                    'data' => $dt,
                ],
                'operators' => [
                    'equal', 'not_equal', 'greater', 'less', 'greater_or_equal', 'less_or_equal',
                ]
            ];
        }

        return $final;
    }

    public function viewGroupAddPage($id = null)
    {

        $affiliatedOrgs = array();
        $friendDatas = Friend::where('user_id', Auth::user()->id)->where('status', Friend::FRIEND_APPROVED)->get();

        if(!empty($friendDatas)) {
            foreach ($friendDatas as $val){
                if(strtolower($val->friendUser->user_role) == 'organization'){
                    $affiliatedOrgs[$val->friendUser->id] = $val->friendUser->first_name.' '.$val->friendUser->last_name;
                }
            }
        }

        $user_ids_domain[] = Auth::user()->id;

        if (!empty(Auth::user()->parent_id))
            $user_ids_domain[] = Auth::user()->parent_id;

        $verifiedDomains =  User::whereIn('id' ,$user_ids_domain)->where(function ($q){
            $q->where('domain_1_confirm_code' ,'1');
            $q->orWhere('domain_2_confirm_code' ,'1');
        })->get();

        $groupCategory = Group_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();

        $data = [
            'affiliatedOrgs' => $affiliatedOrgs,
            'group_id' => $id,
            'groupCategory' => $groupCategory,
            'verifiedDomains' => $verifiedDomains,
            'domains_array' => [],
            'page_name' => 'org_group',
        ];

        if ($id == null){
            return view('organization.create_group', $data);

        }else {

            $data['group_info'] = Group::find($id);
            $data['domains_array'] = explode(',', $data['group_info']->applicable_domains);

            return view('organization.create_group', $data);
        }
    }


    public function getDynamicMembersAgainstUI($domains ,$rules){

       $allVolunteersQuery = User::where('user_role' ,'volunteer');

        $usersIdsWithDomainAttrAll = [];

        foreach ($domains as $index => $domain){

            if($index == 0)
                $allVolunteersQuery->where('email', 'like', "%{$domain}%");
            else
                $allVolunteersQuery->OrWhere('email', 'like', "%{$domain}%");

            $usersIds = Customattributes::where([
                'attributekey' => 'domain' ,
                'attributevalue' => $domain
            ])->pluck('userid')->toArray();

            $usersIdsWithDomainAttrAll = array_merge($usersIdsWithDomainAttrAll ,$usersIds);

        }

        $allVolunteersIds = $allVolunteersQuery->pluck('id')->toArray();

        $allVolunteersIds = array_unique(array_merge($usersIdsWithDomainAttrAll ,$allVolunteersIds));

        if($rules) {

            list($where, $whereOr) = self::recursiveFunction($rules);

            $query =  Customattributes::latest();

            foreach ($where as $condition) {

                $query->where(function ($query) use ($condition) {
                    $query->where('attributekey' ,$condition['key'])
                        ->where('attributevalue' ,$condition['opr'],$condition['vale']);
                });
            }

            foreach ($whereOr as $condition) {

                $query->orWhere(function ($query) use ($condition) {
                    $query->where('attributekey' ,$condition['key'])
                        ->where('attributevalue' ,$condition['opr'],$condition['vale']);
                });
            }

            $customAttributeFilterIds = $query->pluck('userid')->toArray();

            $customAttributesQuery = Customattributes::whereIn('userid' ,$allVolunteersIds)->pluck('userid')->toArray();

            $attributesMatUserId = array_intersect($customAttributeFilterIds ,$customAttributesQuery);

            $dynamicMembers = User::whereIn('id' ,$attributesMatUserId)->get();
        }
        else
            $dynamicMembers = User::whereIn('id' ,$allVolunteersIds)->get();

        if($dynamicMembers->count() == 0)
            $dynamicMembers = collect([]);

        return $dynamicMembers;
    }

    public function createGroup(Request $request)
    {
        $domains = $request->domains;
        $rules = json_decode($request->rules ,true);
        $dynamicMembers = [];

        if($request->group_type == 2) //its dynamic group so need to add members dynamically
            $dynamicMembers = $this->getDynamicMembersAgainstUI($domains ,$rules);

        $validator = Validator::make($request->all(), [
            'file_logo' => 'max:5000',
            'file_banner' => 'max:5000',
        ]);

        if ($validator->fails()) {
            \Session::flash('error', 'image bigger than 5 mb');
            return redirect()->back();
        }

        $user_id = Auth::user()->id;
        $group_name = $request->get('group_name');
        $description = $request->get('description');
        $contact_name = $request->get('contact_name');
        $contact_email = $request->get('contact_email');
        $contact_phone = $request->get('contact_phone');
        $group_type = $request->get('group_type');
        $group_category_id = $request->get('group_category_id');
        $affiliated_org_id = $request->get('affiliated_org_id');

        $group = new Group;
        $group->creator_id = $user_id;
        $group->name = $group_name;
        $group->description = $description;
        $group->contact_name = $contact_name;
        $group->contact_email = $contact_email;
        $group->contact_phone = $contact_phone;
        $group->group_category_id = $group_category_id;
        $group->affiliated_org_id = $affiliated_org_id;
        $group->status = 1;
        $group->is_public = $group_type;
        $group->auto_accept_join = $request->get('auto_accept_join') ? 1 : 0;
        $group->private_passcode = $request->get('is_share_able') ? $request->get('private_passcode') : null;
        $group->is_share_able = $request->get('is_share_able') ? 1 : 0;

        //for the dynamic group
        $group->dynamic_ui_rules = $request->rules; //its already in string format
        $group->applicable_domains = $domains && $group_type == 2? implode(',',$domains) : '';

        if ($request->get('image_logo')) {
            $group->logo_img = base64fileUploadOnS3(null,$request->image_logo ,Group::S3PathLogo,$user_id.'logo');
        }

        if ($request->get('image_banner')) {
            $group->banner_image = base64fileUploadOnS3(null,$request->image_banner ,Group::S3PathBan,$user_id.'ban');
        }

        $group->save();

        if(!empty($affiliated_org_id)){

            $alert = new Alert;
            $alert->related_id = $group->id;
            $alert->receiver_id = $affiliated_org_id;
            $alert->sender_id = Auth::user()->id;
            $alert->sender_type = Auth::user()->user_role;
            $alert->alert_type = Alert::ALERT_AFFILIATED_GROUP;
            $alert->contents = " has added ".$group_name." affiliated group.";
            $alert->save();
        }

        $chatService = new ChatService();
        $logo = $group->logo_img === null ? asset('img/org/001.png') : $group->logo_img;
        $chatId = $chatService->createChat($group->name, $logo);
        $chatService->addUserToChat(Auth::user(), $chatId, $group->name, 'groups');

        $chat = new Chat();
        $chat->chat_id = $chatId;
        $chat->user_id = $user_id;
        $chat->group_id = $group->id;
        $chat->type = 'groups';
        $chat->save();

        $group_member = new Group_member;
        $group_member->group_id = $group->id;
        $group_member->user_id = $user_id;
        $group_member->role_id = Group::GROUP_ADMIN;
        $group_member->status = Group_member::APPROVED;
        $group_member->save();

        foreach ($dynamicMembers as $member){

            $group_member = new Group_member;
            $group_member->group_id = $group->id;
            $group_member->user_id = $member->id;
            $group_member->role_id = Group::GROUP_MEMBER;
            $group_member->status = Group_member::APPROVED;
            $group_member->save();

            $chatService->addUserToChat($member, $chatId, $group->name, 'groups');
        }

        return redirect()->to('/organization/group');
    }

    public function changeGroup(Request $request)
    {
        $domains = $request->domains;
        $rules = json_decode($request->rules ,true);

        $dynamicMembers = [];

        $user_id = auth()->id();
        $validator = Validator::make($request->all(), [
            'file_logo' => 'max:5000',
            'file_banner' => 'max:5000',
        ]);

        if ($validator->fails()) {
            \Session::flash('error', 'image bigger than 5 mb');
            return redirect()->back();
        }

        $group_id = $request->get('group_id');
        $group_name = $request->get('group_name');
        $description = $request->get('description');
        $contact_name = $request->get('contact_name');
        $contact_email = $request->get('contact_email');
        $contact_phone = $request->get('contact_phone');
        $group_type = $request->get('group_type');
        $group_category_id = $request->get('group_category_id');

        $group = Group::find($group_id);
        $group->name = $group_name;
        $group->description = $description;
        $group->contact_name = $contact_name;
        $group->contact_email = $contact_email;
        $group->contact_phone = $contact_phone;
        $group->group_category_id = $group_category_id;
        $group->auto_accept_join = $request->get('auto_accept_join') ? 1 : 0;
        $group->private_passcode = $request->get('is_share_able') ? $request->get('private_passcode') : null;
        $group->is_share_able = $request->get('is_share_able') ? 1 : 0;

        //for the dynamic group
        $group->dynamic_ui_rules = $request->rules; //its already in string format
        $group->applicable_domains = $domains && $group_type == 2 ? implode(',',$domains) : '';

        $group->is_public = $group_type;

        if ($request->get('image_logo')) {
            $group->logo_img = base64fileUploadOnS3($group->logo_img,$request->image_logo ,Group::S3PathLogo,$user_id.'logo');
        }

        if ($request->get('image_banner')) {
            $group->banner_image = base64fileUploadOnS3($group->banner_image,$request->image_banner ,Group::S3PathBan,$user_id.'ban');
        }

        if(!empty($group->affiliated_org_id)){
            $group->affiliated_status = 0;
            $alert = new Alert;
            $alertGroup = Alert::where('receiver_id', $group->affiliated_org_id)
                    ->where('sender_id', Auth::user()->id)
                    ->where('related_id', $group_id)
                    ->where('is_apporved', 0)
                    ->first();

            if(empty($alertGroup)){
                $alert->related_id = $group_id;
                $alert->receiver_id = $group->affiliated_org_id;
                $alert->sender_id = Auth::user()->id;
                $alert->sender_type = Auth::user()->user_role;
                $alert->alert_type = Alert::ALERT_AFFILIATED_GROUP;
                $alert->contents = " has updated ".$group_name." affiliated group.";
                $alert->save();
            }else{
               $alertGroup->is_checked = 0; 
               $alertGroup->save();
            }
        }

        $group->save();

        $chatsGroup = Chat::where('group_id', $group->id)->first();
        $chatService = new ChatService();
        $logo = $group->logo_img === null ? asset('img/org/001.png') : $group->logo_img;
        $chatService->updateChatInfo($chatsGroup->id, $group->name,  $logo);

        $group = Group::find($request->get('group_id'));

        if($request->group_type == 2) //its dynamic group so need to add members dynamically
        {
            $dynamicMembers = $this->getDynamicMembersAgainstUI($domains ,$rules);

            $membersNow = $dynamicMembers->pluck('id')->toArray();

            $alreadyMembers = Group_member::activeMember()->where('group_id',$group->id)->pluck('user_id')->toArray();

            $dynamicMembersAdd =  User::where('id' ,'<>' ,$user_id)->whereIn('id' ,array_diff($membersNow,$alreadyMembers))->get();
            $dynamicMembersRemove =  User::where('id' ,'<>' ,$user_id)->whereIn('id' ,array_diff($alreadyMembers ,$membersNow))->get();

            foreach ($dynamicMembersAdd as $member){

                $group_member = new Group_member;
                $group_member->group_id = $group->id;
                $group_member->user_id = $member->id;
                $group_member->role_id = Group::GROUP_MEMBER;
                $group_member->status = Group_member::APPROVED;
                $group_member->save();

                $chatService->addUserToChat($member, $chatsGroup->chat_id, $group->name, 'groups');
            }

            foreach ($dynamicMembersRemove as $user) {

                $chatService->removeUserFromChat($chatsGroup->chat_id, $user->user_name, 'groups');
                Group_member::where('user_id', $user->id)->where('group_id', $group->id)->update(['is_deleted' => 1]);;
            }
        }

        return redirect()->to('/organization/group');
    }

    public function recursiveFunction($array){

        $operators = $this->operatorsUI();

        foreach ($array as $key => $value) {

            if($key == 'condition'){
                $this->condition = $value;
            }

            elseif($key == 'rules') {

                $dt = current($value);

                $new = isset($dt['rules']) ? $dt['rules'] : $value;

                foreach ($new as $keyRule => $valueRule) {

                    if(isset($valueRule['condition']))
                        $this->recursiveFunction($valueRule);

                    else if(isset($valueRule['field'])) {

                        if ($this->condition == 'AND') {
                            $this->where[] = [
                                'key' => $valueRule['field'],
                                'opr' => $operators[$valueRule['operator']],
                                'vale' => $valueRule['value'],
                            ];
                        } else
                            $this->whereOr[] = [
                                'key' => $valueRule['field'],
                                'opr' => $operators[$valueRule['operator']],
                                'vale' => $valueRule['value'],
                            ];
                    }
                }
            }
        }

        return  [$this->where,$this->whereOr];

    }

    public function getDynamicGroupRules(Request $request)
    {
        $group = Group::find($request->groupId);

        $domains_array = $group ? explode(',', $group->applicable_domains) : [];
        $filtersForUi = $this->getFiltersForUi($domains_array);
        $rulesForUi = $group ? $group->dynamic_ui_rules : [];

        return ['filtersForUi' => json_encode($filtersForUi ,true), 'rulesForUi' => $rulesForUi ];
    }

    public function getUserByGroup(Request $request)
    {
        $keyword = trim($request->get('keyword'));
        $groupId = trim($request->get('groupId'));

        $lists = DB::table('users')
            ->where(function ($query) use ($keyword) {
                $query->where("users.first_name", "LIKE", "%$keyword%")
                    ->orWhere("users.last_name", "LIKE", "%$keyword%")
                    ->orWhere("users.org_name", "LIKE", "%$keyword%");
            })
            ->join('group_members', 'users.id', '=', 'group_members.user_id')
            ->where('group_members.group_id', $groupId)
            ->select('users.*')
            ->get();

        return view('organization.groupsMembersList', ['lists' => $lists]);
    }

    public function getUserList(Request $req)
    {
        $keyword = $req->get('keyword');
        $groupId = $req->get('groupId');
        $getUserList = User::where('first_name', 'like', '%' . $keyword . '%')->get();
        $getUserGroup = DB::table('groups')->where('name', 'like', '%' . $keyword . '%')->get();
        $html ='';
        if ($getUserList->count()) { 
            $html .= '<div class="searchResult" data-id='.$groupId.'>
            <h6><b>Volunteers</b></h6>
            <div id="volError" style="display:none;color:red">Please check volunteer</div>
             <form action="' . url('api/organization/group/add_user_invitation') . '" method="post" id="sendFrm">
                        <ul style=" position: relative; text-align: left;">';
            foreach ($getUserList as $key => $value) {
                $html .= '<li style="cursor:pointer; list-style-type: none;" class="listUser" data-label="user">
                          <div class="checkbox-wrap radio-wrap">
                            <label class="checkbox-label">
                            <div class="test-wrap"><input type="checkbox" name="list_user_id" data-label="user"  value="' . $value->id . '"> <i></i> </div><span>' . $value->first_name . ''. $value->last_name. 
                        '</span></div></label></li>';
            }
            $html .= csrf_field();
            $html .= '  </ul>
                            <input type="hidden" name="group_id" value="' . $groupId . '">
                        </form>
                     </div>';
        }
        
        if ($getUserGroup->count()) {
            // $html .="<hr>";
       //  $add= url('api/organization/group/add_group_invitation');
            $html .= '<div class="searchResult" data-id='.$groupId.'>
                       <h6><b>Groups</b></h6>
                        <ul class="row" style=" position: relative; text-align: left;">';
            foreach ($getUserGroup as $key => $value) {
                $html .= '<li class="" style="cursor:pointer; list-style-type: none;" class="listUser" data-label="group">
                
                         <div class="checkbox-wrap"><label class="checkbox-label">
                         <div class="test-wrap"  style="display:none">
                         <input type="radio" checked name="list_group_id[]"  data-label="group" value="' . $value->id . '"> 
                          <i></i></div><span>' . $value->name . 
                            '</div></label><form class="addGroupInvitation">
                                <input type="hidden" value="' . $value->id . '" name="id">
                                <input type="hidden" value="' . $value->name . '" name="name">
                                <input class="btn btn-success" onclick="this.form.submited=this.value;" type="submit" value="Send">';
                            //  '<form action="' . $add . '" method="post">
                            //     <input type="hidden" value="' . $value->id . '" name="id">
                            //     <input type="hidden" value="' . $value->name . '" name="name">
                            //     <input class="btn btn-success" type="submit" value="Send">';
                            $html .= csrf_field();
                $html .= ' </span></form></li>';
            }
        }
        
        $html .= '  </ul>
                        <input type="hidden" name="group_id" value="' . $groupId . '">
                    </div>';
            
        echo $html;
        die;
    }

     public function addUserInvitation(Request $req)
    {   
        $list_user_id= array();
        foreach ($req->get('data') as $value) {
                 
                if($value['name']=="list_user_id"){
                    $list_user_id[]= $value['value'];
                }
                else if($value['name']=="group_id"){
                    $group_id= $value['value'];
                } 
          }       
        $session = Auth::guard('web')->user();
        $last_id = 0;
   
        if ($list_user_id) {
            foreach ($list_user_id as $value) {
                $user_id = $value;
                if ($session->id != $value) {

                    $getUserData = User::where('id', $value)->get();
                    $groupData = Group::where('id', $group_id)->get();
                    $GroupMemberData = Group_member::where('user_id', $value)->where('group_id', $group_id)->where('is_deleted', 0)->first();
                   
                    if ($GroupMemberData === null) {
                        $group_member = new Group_member;
                        $group_member->group_id = $group_id;
                        $group_member->user_id = $value;
                        $group_member->role_id = 2;
                        $group_member->status = 1;
                        $group_member->save();

                        $last_id = $group_member->id;

                        $alerts = new Alert;
                        $alerts->receiver_id = $value;
                        $alerts->sender_id = $session->id;
                        $alerts->sender_type = $getUserData[0]->user_role;
                        $alerts->alert_type = 10;
                        $alerts->contents = 'sent you an invitation to join ' . $groupData[0]->name . ' group';
                        $alerts->is_checked = 0;
                        $alerts->related_id = $last_id;
                        $alerts->save();
                    } else {
                        $checkAlert = Alert::where('receiver_id', $value)
                            ->where('sender_id', $session->id)
                            ->where('sender_type', $getUserData[0]->user_role)
                            ->where('alert_type', 10)
                            ->where('contents', 'sent you an invitation to join ' . $groupData[0]->name . ' group')
                            ->where('related_id', $GroupMemberData->id)
                            ->first();
                        if ($checkAlert !== null) {
                            // $req->session()->flash('error', 'Already send');
                            // return redirect()->to('/organization/group');
                             return response()->json(['error'=>'Already send','redirect'=>"/organization/group" ,"group_id"=>$group_id]);

                        }

                        $alerts = new Alert;
                        $alerts->receiver_id = $value;
                        $alerts->sender_id = $session->id;
                        $alerts->sender_type = $getUserData[0]->user_role;
                        $alerts->alert_type = 10;
                        $alerts->contents = 'sent you an invitation to join ' . $groupData[0]->name . ' group';
                        $alerts->is_checked = 0;
                        $alerts->related_id = $GroupMemberData->id;
                        $alerts->save();
                    }
                }
            }
        }

        if ($last_id == 0) {
            // $req->session()->flash('error', 'Already added');
           
           return response()->json(['error'=>'Already added',"group_id"=>$group_id]);

        } else {
            // $req->session()->flash('success', 'Successfully send');

            return response()->json(['success'=>'Successfully send',"group_id"=>$group_id]);

        }
         return response()->json(['redirect'=>"/organization/group","group_id"=>$group_id]);
       //  return redirect()->to('/organization/group');
    }

    public function addGroupInvitation(Request $req)
    {
        $group_id = $req->get('id');
        $last_id=0;
        // $group_id = $req->get('group_id1');
        // $list_group_id = $req->get('list_group_id');
         $session = Auth::guard('web')->user();
        // $last_id = 0;
        $users=DB::table('group_members')->where('group_id',$group_id)->get();
        $temp=array();
        $a=0;
        if($users)
        {
            foreach($users as $u)
            {
                $temp[$a] = $u->user_id;
                $a = $a + 1;
            }

            $list_user_id=$temp;
        }
        else
        {
            $list_user_id=0;
            $last_id=0;
        }
      
        if ($list_user_id!=0 && $list_user_id!=NULL) {
      
            foreach ($list_user_id as $value) {
                $user_id = $value;
                if ($session->id != $value) {

                    $getUserData = User::where('id', $value)->get();
                    $groupData = Group::where('id', $group_id)->get();
                    $GroupMemberData = Group_member::where('user_id', $value)->where('group_id', $group_id)->where('is_deleted', 0)->first();
                    if ($GroupMemberData === null) {
                        $group_member = new Group_member;
                        $group_member->group_id = $group_id;
                        $group_member->user_id = $value;
                        $group_member->role_id = 2;
                        $group_member->status = 1;
                        $group_member->save();

                        $last_id = $group_member->id;

                        $alerts = new Alert;
                        $alerts->receiver_id = $value;
                        $alerts->sender_id = $session->id;
                        $alerts->sender_type = $getUserData[0]->user_role;
                        $alerts->alert_type = 10;
                        $alerts->contents = 'sent you an invitation to join ' . $groupData[0]->name . ' group';
                        $alerts->is_checked = 0;
                        $alerts->related_id = $last_id;
                        $alerts->save();
                    } else {
                        $checkAlert = Alert::where('receiver_id', $value)
                            ->where('sender_id', $session->id)
                            ->where('sender_type', $getUserData[0]->user_role)
                            ->where('alert_type', 10)
                            ->where('contents', 'sent you an invitation to join ' . $groupData[0]->name . ' group')
                            ->where('related_id', $GroupMemberData->id)
                            ->first();
                        if ($checkAlert !== null) {
                           // $req->session()->flash('error', 'Already send');
                             return response()->json(['error'=>'Already send','redirect'=>"/organization/group"]);
                           // return redirect()->to('/organization/group');
                        }
                        
                        $alerts = new Alert;
                        $alerts->receiver_id = $value;
                        $alerts->sender_id = $session->id;
                        $alerts->sender_type = $getUserData[0]->user_role;
                        $alerts->alert_type = 10;
                        $alerts->contents = 'sent you an invitation to join ' . $groupData[0]->name . ' group';
                        $alerts->is_checked = 0;
                        $alerts->related_id = $GroupMemberData->id;
                        $alerts->save();
                    }
                }
            }
        }
        
        if ($list_user_id==0 && $last_id == 0) {   
            return response()->json(['error'=>'Already added']);
          //  $req->session()->flash('error', 'Already added');
       } else {         
           return response()->json(['success'=>'Successfully send']);
           // $req->session()->flash('success', 'Successfully send');
        }
            return response()->json(['redirect'=>"/organization/group"]);
          // return redirect()->to('/organization/group');
    }

    public function viewGroupDetail($group_id)
    {
        $group_info = array();

        $group = Group::find($group_id);
        $group_info['group_id'] = $group->id;
        $my_group = Group_member::where('group_id', $group_id)->where('user_id', Auth::user()->id)->where('is_deleted', '<>', 1)->get()->first();

        if ($my_group == null) {
            $group_info['is_my_group'] = 0;
        } else {
            $group_info['is_my_group'] = $my_group->status;
        }

        $my_follow = Follow::where('follower_id', Auth::user()->id)->where('type', 'group')->where('followed_id', $group_id)->where('is_deleted', '<>', 1)->get()->first();

        if ($my_follow == null) {
            $group_info['is_followed'] = 0;
        } else {
            $group_info['is_followed'] = 1;
        }

        $group_info['group_name'] = $group->name;
        $group_info['group_logo'] = $group->logo_img;
        $group_info['group_description'] = $group->description;
        $group_info['group_contact_name'] = $group->contact_name;
        $group_info['group_contact_email'] = $group->contact_email;
        $group_info['group_contact_number'] = $group->contact_phone;
        $created_at = explode(" ", $group->created_at);
        $group_info['group_created_at'] = $created_at[0];
        $creator = User::find($group->creator_id);
        $group_info['org_id'] = $creator->id;
        $group_info['org_name'] = $creator->org_name;
        $group_info['org_logo'] = $creator->logo_img;

        $member_info = array();

        $group_members = Group_member::where('group_id', $group_id)->where('status', Group_member::APPROVED)->where('is_deleted', '<>', 1)->get();

        foreach ($group_members as $gm) {
            $member_info[$gm->id]['user_role'] = $gm->role_id;
            $member_info[$gm->id]['member_id'] = $gm->user_id;                                                               
            $member = User::find($gm->user_id);

            if ($member->user_role == 'organization') {
                $member_info[$gm->id]['member_name'] = $member->org_name;
                $member_info[$gm->id]['group'] = 'Organization';
            } else {
                $member_info[$gm->id]['member_name'] = $member->first_name . ' ' . $member->last_name;
            }

            $member_info[$gm->id]['member_logo'] = $member->logo_img;
            $member_info[$gm->id]['member_email'] = $member->email;
            $member_info[$gm->id]['member_number'] = $member->contact_number;
        }
        return view('organization.view_group_detail', ['group_info' => $group_info, 'member_info' => $member_info, 'page_name' => '']);
    }

    public function searchGroup(Request $request)
    {
        $keyword = $request->input('keyword');
        $my_id = Auth::user()->id;
        $my_group_ids = Group_member::where('user_id', $my_id)->where('status', Group_member::APPROVED)->where('is_deleted', '<>', 1)->groupBy('group_id')->selectRaw('group_id')->pluck('group_id')->toArray();

        if ($keyword == null) {
            $friend_id = Friend::where('user_id', $my_id)->where('status', Friend::FRIEND_APPROVED)->pluck('friend_id')->toArray();
            $group_ids = Group_member::whereIn('user_id', $friend_id)->where('status', Group_member::APPROVED)->groupBy('group_id')->selectRaw('group_id')->pluck('group_id')->toArray();
            $groups = Group::whereIn('id', $group_ids)->whereNotIn('id', $my_group_ids)->get();

            $result = array();
            $i = 0;

            foreach ($groups as $g) {
                $result[$i]['group_id'] = $g->id;
                $result[$i]['group_name'] = $g->name;
                $result[$i]['group_logo'] = $g->logo_img;
                $result[$i]['owner_id'] = $g->creator_id;

                if (User::find($g->creator_id)->user_role == 'organization') {
                    $result[$i]['owner_name'] = User::find($g->creator_id)->org_name;
                } else {
                    $result[$i]['owner_name'] = User::find($g->creator_id)->first_name . ' ' . User::find($g->creator_id)->last_name;
                }

                $result[$i]['members'] = Group_member::where('group_id', $g->id)->where('is_deleted', '<>', 1)->count();
                $result[$i]['is_followed'] = Follow::where('follower_id', $my_id)->where('type', 'group')->where('followed_id', $g->id)->where('is_deleted', '<>', 1)->count();
                $result[$i]['status'] = 0;
                $is_member = Group_member::where('group_id', $g->id)->where('user_id', $my_id)->where('is_deleted', '<>', 1)->get()->first();

                if ($is_member != null) {
                    $result[$i]['status'] = $is_member->status;
                }
                $i++;
            }
            return view('organization.group_search', ['groups' => $result, 'keyword' => $keyword, 'page_name' => 'org_group']);

        } else {
            $groups = Group::whereNotIn('id', $my_group_ids)->where('is_deleted', '<>', 1)->where(function ($query) use ($keyword) {
                $keyword_terms = explode(' ', $keyword);

                foreach ($keyword_terms as $terms) {
                    $query->orWhere("name", "LIKE", "%$terms%")
                        ->orWhere("description", "LIKE", "%$terms%");
                }
            })->orderBy('created_at', 'desc')->get();

            $result = array();
            $i = 0;

            foreach ($groups as $g) {
                $result[$i]['group_id'] = $g->id;
                $result[$i]['group_name'] = $g->name;
                $result[$i]['group_logo'] = $g->logo_img;
                $result[$i]['owner_id'] = $g->creator_id;

                if (User::find($g->creator_id)->user_role == 'organization') {
                    $result[$i]['owner_name'] = User::find($g->creator_id)->org_name;
                } else {
                    $result[$i]['owner_name'] = User::find($g->creator_id)->first_name . ' ' . User::find($g->creator_id)->last_name;
                }

                $result[$i]['members'] = Group_member::where('group_id', $g->id)->where('is_deleted', '<>', 1)->count();
                $result[$i]['is_followed'] = Follow::where('follower_id', $my_id)->where('type', 'group')->where('followed_id', $g->id)->where('is_deleted', '<>', 1)->count();
                $result[$i]['status'] = 0;

                $is_member = Group_member::where('group_id', $g->id)->where('user_id', $my_id)->where('is_deleted', '<>', 1)->get()->first();

                if ($is_member != null) {
                    $result[$i]['status'] = $is_member->status;
                }

                $i++;
            }

            return view('organization.group_search', ['groups' => $result, 'keyword' => $keyword, 'page_name' => 'org_group']);
        }
    }

    public function followGroup(Request $request)
    {
        $group_id = $request->input('group_id');
        $my_id = Auth::user()->id;
        $is_exist = Follow::where('follower_id', $my_id)->where('type', 'group')->where('followed_id', $group_id)->get()->first();

        if ($is_exist == null) {
            $follower = new Follow;
            $follower->follower_id = $my_id;
            $follower->type = 'group';
            $follower->followed_id = $group_id;
            $follower->save();
        } else {
            $is_exist->is_deleted = 0;
            $is_exist->save();
        }

        return Response::json(['result' => 'success']);
    }

    public function unfollowGroup(Request $request)
    {
        $group_id = $request->input('group_id');
        $my_id = Auth::user()->id;
        $is_exist = Follow::where('follower_id', $my_id)->where('type', 'group')->where('followed_id', $group_id)->get()->first();
        $is_exist->is_deleted = 1;
        $is_exist->save();

        return Response::json(['result' => 'success']);
    }

    public function jointoGroup(Request $request)
    {
        //i don't think those are using anywhere as only HomeCtrl Join group using
        $group_id = $request->input('group_id');
        $my_id = Auth::user()->id;
        $is_exist = Group_member::where('group_id', $group_id)->where('user_id', $my_id)->get()->first();

        if ($is_exist == null) {

            $chat = Chat::where('group_id', $group_id)->first();
            $chatService = new ChatService();
            $chatService->addUserToChat(Auth::user(), $chat->chat_id, 'groups' , Auth::user()->user_role);

            $member = new Group_member;
            $member->group_id = $group_id;
            $member->user_id = $my_id;
            $member->role_id = Group_member_role::MEMBER;
            $member->status = Group_member::PENDING;
            $member->save();

        } else {
            $is_exist->is_deleted = 1;
            $is_exist->status = Group_member::PENDING;
            $is_exist->save();
        }

        return Response::json(['result' => 'success']);

    }

    public function leaveGroup($groupId)
    {
        $session = Auth::guard('web')->user();
        Group_member::where('user_id', $session->id)->where('group_id', $groupId)->update(['is_deleted' => 1]);
        $chat = Chat::where('group_id', $groupId)->first();
        $chatService = new ChatService();
        $chatService->removeUserFromChat($chat->chat_id, Auth::user()->user_name, 'groups');

        return redirect()->to('/organization/group');
    }
    /**
     * Method to validate group name
     * @param \Illuminate\Http\Request $request
     */
    public function validatedGroup(Request $request)
    {
        $name = trim($request->input('group_name'));
        $id = trim($request->input('group_id'));

        $groupObj = Group::where('name', $name)
            ->where('is_deleted', '0')
            ->where('creator_id', Auth::user()->id);
        if (!empty($id)) {
            $groupObj = $groupObj->where('id', '!=', $id);
        }
        $groupObj = $groupObj->get();
        if (count($groupObj) == '0') {
            echo 'success';
        } else {
            echo "Group name is already exist.";
        }
        exit;
    }

    public function approveAffiliatedOrgRequest($id, $related_id, $status = 1) {

        $group = Group::find($related_id);

        if (!empty($group)) {

            if ($status == 1) {
                Alert::where('id', $id)->update(['is_apporved' => 1]);
            } else {
                Alert::where('id', $id)->update(['is_apporved' => 2]);
            }
            $group->affiliated_status = $status;
            $group->save();

        }
        return redirect()->to('/viewAlert');
    }
    
    //start code for export group members 
    
    //export single opportunity members data
    public function exportGroupMembers($id = null){
        if($id){
            $groupObj = Group::Select('id','name')->find($id);
            $members = Group_member::where('group_id', $id)->where('user_id','<>', Auth::user()->id)->where('status', 2)->get();
            $getHeaders = app('App\Http\Controllers\Organization\OpportunityCtrl')->getHeaderData($groupObj->name);
            $columns = $getHeaders['columns'];
            $headers = $getHeaders['headers'];
            $atrributelookups = $getHeaders['atrributelookups'];
            $callback = function() use ($members, $columns, $atrributelookups)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);
                foreach ($members as $m) {
                    $vol = User::find($m->user_id);
                    if ($vol != null) {
                        $columnsVal = app('App\Http\Controllers\Organization\OpportunityCtrl')->getMemberInfo($vol, $atrributelookups, $m);
                        fputcsv($file, $columnsVal);
                    }
                }
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);
        }
    }
    
    //export data for all groups of organization 
    public function exportAllGroupMembers(){
        $getHeaders = app('App\Http\Controllers\Organization\OpportunityCtrl')->getHeaderData('Group');
        $columns = $getHeaders['columns'];
        $headers = $getHeaders['headers'];
        $atrributelookups = $getHeaders['atrributelookups'];

        $columns = array_merge(array('Group Name'), $columns);
            
        $active_group = Group::Select('id','name as title')
                ->where('creator_id', Auth::user()->id)
                ->where('is_deleted', '<>', '1')
                ->orderBy('created_at', 'desc')
                ->get();
        
        $callback = function() use ($active_group, $columns, $atrributelookups)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($active_group as $key=>$data){
                if(!empty($data->group_member)){
                    foreach ($data->group_member as $mkey=>$mdata){
                        
                        $vol = User::find($mdata->user_id);
                        if ($vol != null) {
                            $columnsVal = app('App\Http\Controllers\Organization\OpportunityCtrl')->getMemberInfo($vol, $atrributelookups, $mdata);
                            $columnsVal = array_merge(array($data->title), $columnsVal);
                            fputcsv($file, $columnsVal);
                        }
                    }
                }
            }
            fclose($file);
        };
       return Response::stream($callback, 200, $headers);
    }
    //end code for export group members 
}
