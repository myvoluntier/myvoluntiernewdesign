<?php

namespace App\Http\Controllers\Organization;

use App\Chat;
use App\Alert;
use App\Events\UserChatNotifyEvent;
use App\Follow;
use App\Friend;
use App\Group;
use App\Message;
use App\Group_member;
use App\Group_member_role;
use App\Opportunity_member;
use App\Tracking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Opportunity;
use App\Organization_type;
use Illuminate\Support\Facades\Redirect;
use Image;
use App\NewsfeedModel;
use App\Services\NewsFeedService;
use App\Services\OrganizationViewProfileService as viewService;
use App\Services\ChatManager;
use App\Services\ChatService;
use App\Services\ImageExif;
use App\Models\EmailsTemplate;
use App\Mail\SharingProfileMail;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class HomeCtrl extends Controller {

    public function dashboard() {

        $user = Auth::user();
        $heading = 'Dashboard';
        return view('organization.dashboard');
    }

    public function viewHome($action = null, NewsFeedService $newsFeedService) {
        $user = Auth::user();

        $org_id = $user->id;

        $tracks_hours = Tracking::where('org_id', $org_id)
                        ->where('approv_status', 1)
                        ->where('is_deleted', '<>', 1)
                        ->sum('logged_mins') / 60;

        $regional_organizations = User::where('user_role', 'organization')
                ->where('confirm_code', 1)
                ->where('is_deleted', '<>', 1)
                ->where("state", "like", "%$user->state%")
                ->where("city", "like", "%$$user->city%")
                ->pluck('id')
                ->toArray();

        $org_tracks = Tracking::where('is_deleted', '<>', 1)
                ->where('approv_status', 1)
                ->whereIn('org_id', $regional_organizations)
                ->groupBy('org_id')
                ->selectRaw('sum(logged_mins)/60 as logged_mins')
                ->pluck('logged_mins')
                ->toArray();

        $i = 1;

        foreach ($org_tracks as $ot) {
            if (floatval($ot) > $tracks_hours) {
                $i++;
            }
        }

        $follow_data = Follow::where('follower_id', $org_id)
                ->where('is_deleted', '<>', 1)
                ->orderBy('updated_at', 'desc')
                ->get();

        $follows = array();

        foreach ($follow_data as $f) {
            $follows[$f->id]['id'] = $f->followed_id;
            $follows[$f->id]['type'] = $f->type;

            $followerUser = $f->user;
            if ($f->type == 'organization') {
                $follows[$f->id]['name'] = $followerUser->org_name;
                $follows[$f->id]['logo'] = $followerUser->logo_img;

                $follows[$f->id]['logged_hours'] = Tracking::where('org_id', $f->followed_id)
                                ->where('approv_status', 1)
                                ->where('is_deleted', '<>', 1)
                                ->sum('logged_mins') / 60;
            } else {
                $follows[$f->id]['name'] = $followerUser->name;
                $follows[$f->id]['logo'] = $followerUser->logo_img;
                $member_ids = Group_member::where('group_id', $f->followed_id)
                        ->where('status', Group_member::APPROVED)
                        ->where('is_deleted', '<>', 1)
                        ->pluck('user_id')
                        ->toArray();

                $follows[$f->id]['logged_hours'] = Tracking::whereIn('volunteer_id', $member_ids)
                                ->where('is_deleted', '<>', 1)
                                ->where('approv_status', 1)
                                ->sum('logged_mins') / 60;
            }

            $date = explode(' ', $f->updated_at);
            $follows[$f->id]['followed_date'] = $date[0];
        }

        $getAllFrndId = Friend::where('user_id', $user->id)
                ->where('status', Friend::FRIEND_APPROVED)
                ->whereHas('friendUser', function ($query) {
                    $query->whereNotNull('org_type');
                })
                ->pluck('friend_id')
                ->toArray();

        $newsFeedData = NewsfeedModel::whereIn('who_joined', $getAllFrndId)
                ->orWhere('related_id', $user->id) //updated logic by seersol
                ->orderBy('created_at', 'desc')
                ->paginate(15);

        $feedNewsArr = array();

        if (!empty($newsFeedData[0])) {
            $feedNewsArr = $newsFeedService->transformNewsFeedToArrayInfo($newsFeedData, 'organization');
        }

        return view('organization.home',
                ['newsFeedData' => $newsFeedData, 'logged_hours' => $tracks_hours, 'regional_ranking' => $i, 'follows' => $follows, 'feedNewsArr' => $feedNewsArr,
                    'user' => auth()->user(), 'action' => $action]);
    }

    public function viewEditAccount() {
        return view('organization.accountsetting',
                ['page_name' => '', 'org_type_names' => Organization_type::all()]);
    }

    public function viewProfile($id = null, viewService $viewService) {
        if ($id == null) {
            if (session('is_delegate'))
                abort(403);

            $id = Auth::user()->id;
        }

        return $viewService->viewProfile($id);
    }

    public function upload_logo(Request $request) {
        if ($request->get('image_logo')) {
               
            $get_user = Auth::user();
            $get_user->logo_img = base64fileUploadOnS3($get_user->logo_img, $request->image_logo, User::S3PathLogo, $get_user->id . 'logo');

            $save = $get_user->save();
            if ($save) {
                $allChatsUser = Chat::where('user_id', Auth::user()->id)->whereNotNull('to_user_id')->get();
                $chatService = new ChatService();

                $chatService->updateUserInfo($get_user);

                if ($get_user->user_role === 'organization')
                    $logo = $get_user->logo_img === null ? asset('img/org/001.png') : $get_user->logo_img;
                else {
                    $logo = $get_user->logo_img === null ? asset('img/logo/member-default-logo.png') : $get_user->logo_img;
                }

                foreach ($allChatsUser as $chat) {
                    $chatService->updateChatInfo($chat->chat_id, $get_user->user_name, $logo, $get_user->user_name);
                }

                $allChatsUser = Chat::where('to_user_id', $get_user->id)->get();

                foreach ($allChatsUser as $chat) {
                    $opponent = User::find($chat->user_id);
                    $chatService->updateChatInfo($chat->chat_id, $get_user->org_name, $logo, $opponent->user_name);
                }
            }
        }

        session()->flash('app_message', 'Logo image uploaded successfully.');

        return back();
    }

    public function upload_back_img(Request $request) {
        if ($request->get('image_banner')) {
            $get_user = Auth::user();
            $get_user->back_img = base64fileUploadOnS3($get_user->back_img, $request->image_banner, User::S3PathBan, $get_user->id . 'ban');
            $get_user->save();
        }

        session()->flash('app_message', 'Back image uploaded successfully.');

        return back();
    }

    public function Search(Request $request) {
        $keyword = $request->input('keyword');
        $my_id = Auth::user()->id;
        $search_filter = is_null($request->input('filter')) ? '' : $request->input('filter');
        $filerts = ['v' => 'Volunteer', 'r' => 'Organization', 'g' => 'Group', 'p' => 'Opportunities'];

        if ($search_filter == '') {
            $org_result = User::where('is_deleted', '<>', 1)->where('status', 1)->where(function ($query) use ($keyword) {
                        $keyword_terms = explode(' ', $keyword);
                        foreach ($keyword_terms as $terms) {
                            $query->orWhere("org_name", "LIKE", "%$terms%")
                                    ->orWhere("first_name", "LIKE", "%$terms%")
                                    ->orWhere("last_name", "LIKE", "%$terms%")
                                    ->orWhere("brif", "LIKE", "%$terms%")
                                    ->orWhere("city", "LIKE", "%$terms%")
                                    ->orWhere("state", "LIKE", "%$terms%")
                                    ->orWhere("ein", "LIKE", "%$terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();

            $result = array();
            $i = 0;
            foreach ($org_result as $o) {
                if ($o->id != $my_id) {
                    $result[$i]['id'] = $o->id;
                    $result[$i]['group_id'] = 0;
                    if ($o->user_role == 'organization') {
                        $result[$i]['name'] = $o->org_name;
                    } else {
                        $result[$i]['name'] = $o->first_name . ' ' . $o->last_name;
                    }
                    $result[$i]['user_role'] = $o->user_role;
                    $result[$i]['logo_img'] = $o->logo_img;
                    $result[$i]['city'] = $o->city;
                    $result[$i]['state'] = $o->state;
                    $result[$i]['country'] = $o->country;
                    $friend = Friend::where('user_id', $my_id)->where('friend_id',
                                    $o->id)->where('is_deleted', '<>', 1)->get()->first();
                    if ($friend == null) {
                        $result[$i]['is_friend'] = 0;
                    } else {
                        $result[$i]['is_friend'] = $friend->status;
                    }
                    $result[$i]['is_followed'] = Follow::where('follower_id', $my_id)->where('type',
                                    'organization')->where('followed_id', $o->id)->where('is_deleted',
                                    '<>', 1)->count();
                    $i++;
                }
            }
            $grp_result = Group::where('is_deleted', '<>', 1)->where('is_public',
                            '<>', 0)->where(function ($grp_query) use ($keyword) {
                        $grp_keyword_terms = explode(' ', $keyword);
                        foreach ($grp_keyword_terms as $grp_terms) {
                            $grp_query->orWhere("name", "LIKE", "%$grp_terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();
            $j = $i;
            foreach ($grp_result as $g) {
                if ($g->id != $my_id) {
                    $result[$j]['id'] = $g->id;
                    $result[$j]['group_id'] = $g->id;
                    $result[$j]['name'] = $g->name;
                    $result[$j]['user_role'] = 'group';
                    $result[$j]['logo_img'] = $g->logo_img;
                    $result[$j]['city'] = "";
                    $result[$j]['state'] = "";
                    $result[$j]['country'] = "";
                    $friend = Group_member::where('user_id', $my_id)->where('group_id',
                                    $g->id)->where('status', 2)->get()->first();
                    if ($friend == null) {
                        $result[$j]['is_friend'] = 0;
                    } else {
                        $result[$j]['is_friend'] = 1;
                    }
                    $result[$j]['is_followed'] = Follow::where('follower_id', $my_id)->where('type',
                                    'organization')->where('followed_id', $g->id)->where('is_deleted',
                                    '<>', 1)->count();
                    $j++;
                }
            }
            $opportunity_result = Opportunity::where('is_deleted', '<>', 1)->where(function ($opportunity_query) use ($keyword) {
                        $opportunity_keyword_terms = explode(' ', $keyword);
                        foreach ($opportunity_keyword_terms as $opportunity_terms) {
                            $opportunity_query->orWhere("title", "LIKE",
                                    "%$opportunity_terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();
            $k = $j;
            foreach ($opportunity_result as $o) {
                if ($o->id != $my_id) {
                    $result[$k]['id'] = $o->id;
                    $result[$k]['group_id'] = $o->id;
                    $result[$k]['name'] = $o->title;
                    $result[$k]['user_role'] = 'opportunity';
                    $result[$k]['logo_img'] = $o->logo_img;
                    $result[$k]['city'] = $o->city;
                    $result[$k]['state'] = $o->state;
                    $result[$k]['country'] = "";
                    $friend = Group_member::where('user_id', $my_id)->where('group_id',
                                    $o->id)->where('status', 2)->get()->first();
                    if ($friend == null) {
                        $result[$k]['is_friend'] = 0;
                    } else {
                        $result[$k]['is_friend'] = 1;
                    }
                    $result[$k]['is_followed'] = Follow::where('follower_id', $my_id)->where('type',
                                    'organization')->where('followed_id', $o->id)->where('is_deleted',
                                    '<>', 1)->count();
                    $k++;
                }
            }
        } else {
            $search_filter = explode(' ', (string) $search_filter);
            $vol_result = User::where('is_deleted', '<>', 1)
                            ->where('status', 1)->where('user_role', '=', 'volunteer')
                            ->where(function ($query) use ($keyword) {
                                $keyword_terms = explode(' ', $keyword);
                                foreach ($keyword_terms as $terms) {
                                    $query->orWhere("org_name", "LIKE", "%$terms%")
                                    ->orWhere("first_name", "LIKE", "%$terms%")
                                    ->orWhere("last_name", "LIKE", "%$terms%")
                                    ->orWhere("brif", "LIKE", "%$terms%")
                                    ->orWhere("city", "LIKE", "%$terms%")
                                    ->orWhere("state", "LIKE", "%$terms%")
                                    ->orWhere("ein", "LIKE", "%$terms%");
                                }
                            })->orderBy('created_at', 'desc')->get();

            $result = array();
            $i = 0;
            if (in_array('v', $search_filter)) {
                foreach ($vol_result as $o) {
                    if ($o->id != $my_id) {
                        $result[$i]['id'] = $o->id;
                        $result[$i]['group_id'] = 0;
                        if ($o->user_role == 'organization') {
                            $result[$i]['name'] = $o->org_name;
                        } else {
                            $result[$i]['name'] = $o->first_name . ' ' . $o->last_name;
                        }
                        $result[$i]['user_role'] = $o->user_role;
                        $result[$i]['logo_img'] = $o->logo_img;
                        $result[$i]['city'] = $o->city;
                        $result[$i]['state'] = $o->state;
                        $result[$i]['country'] = $o->country;
                        $friend = Friend::where('user_id', $my_id)->where('friend_id',
                                        $o->id)->where('is_deleted', '<>', 1)->get()->first();
                        if ($friend == null) {
                            $result[$i]['is_friend'] = 0;
                        } else {
                            $result[$i]['is_friend'] = $friend->status;
                        }
                        $result[$i]['is_followed'] = Follow::where('follower_id', $my_id)->where('type',
                                        'organization')->where('followed_id', $o->id)->where('is_deleted',
                                        '<>', 1)->count();
                        $i++;
                    }
                }
            }

            $org_result = User::where('is_deleted', '<>', 1)
                            ->where('user_role', '=', 'organization')
                            ->where('status', 1)
                            ->where(function ($query) use ($keyword) {
                                $keyword_terms = explode(' ', $keyword);
                                foreach ($keyword_terms as $terms) {
                                    $query->orWhere("org_name", "LIKE", "%$terms%")
                                    ->orWhere("first_name", "LIKE", "%$terms%")
                                    ->orWhere("last_name", "LIKE", "%$terms%")
                                    ->orWhere("brif", "LIKE", "%$terms%")
                                    ->orWhere("city", "LIKE", "%$terms%")
                                    ->orWhere("state", "LIKE", "%$terms%")
                                    ->orWhere("ein", "LIKE", "%$terms%");
                                }
                            })->orderBy('created_at', 'desc')->get();
            if (in_array('r', $search_filter)) {
                foreach ($org_result as $o) {
                    if ($o->id != $my_id) {
                        $result[$i]['id'] = $o->id;
                        $result[$i]['group_id'] = 0;
                        if ($o->user_role == 'organization') {
                            $result[$i]['name'] = $o->org_name;
                        } else {
                            $result[$i]['name'] = $o->first_name . ' ' . $o->last_name;
                        }
                        $result[$i]['user_role'] = $o->user_role;
                        $result[$i]['logo_img'] = $o->logo_img;
                        $result[$i]['city'] = $o->city;
                        $result[$i]['state'] = $o->state;
                        $result[$i]['country'] = $o->country;
                        $friend = Friend::where('user_id', $my_id)->where('friend_id',
                                        $o->id)->where('is_deleted', '<>', 1)->get()->first();
                        if ($friend == null) {
                            $result[$i]['is_friend'] = 0;
                        } else {
                            $result[$i]['is_friend'] = $friend->status;
                        }
                        $result[$i]['is_followed'] = Follow::where('follower_id', $my_id)->where('type',
                                        'organization')->where('followed_id', $o->id)->where('is_deleted',
                                        '<>', 1)->count();
                        $i++;
                    }
                }
            }

            $grp_result = Group::where('is_deleted', '<>', 1)->where('is_public',
                            '<>', 0)->where(function ($grp_query) use ($keyword) {
                        $grp_keyword_terms = explode(' ', $keyword);
                        foreach ($grp_keyword_terms as $grp_terms) {
                            $grp_query->orWhere("name", "LIKE", "%$grp_terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();
            $j = $i;
            if (in_array('g', $search_filter)) {
                foreach ($grp_result as $g) {
                    if ($g->id != $my_id) {
                        $result[$j]['id'] = $g->id;
                        $result[$j]['group_id'] = $g->id;
                        $result[$j]['name'] = $g->name;
                        $result[$j]['user_role'] = 'group';
                        $result[$j]['logo_img'] = $g->logo_img;
                        $result[$j]['city'] = "";
                        $result[$j]['state'] = "";
                        $result[$j]['country'] = "";
                        $friend = Group_member::where('user_id', $my_id)->where('group_id',
                                        $g->id)->where('status', 2)->get()->first();
                        if ($friend == null) {
                            $result[$j]['is_friend'] = 0;
                        } else {
                            $result[$j]['is_friend'] = 1;
                        }
                        $result[$j]['is_followed'] = Follow::where('follower_id', $my_id)->where('type',
                                        'organization')->where('followed_id', $g->id)->where('is_deleted',
                                        '<>', 1)->count();
                        $j++;
                    }
                }
            }
            $opportunity_result = Opportunity::where('is_deleted', '<>', 1)->where(function ($opportunity_query) use ($keyword) {
                        $opportunity_keyword_terms = explode(' ', $keyword);
                        foreach ($opportunity_keyword_terms as $opportunity_terms) {
                            $opportunity_query->orWhere("title", "LIKE",
                                    "%$opportunity_terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();
            $k = $j;
            if (in_array('p', $search_filter)) {
                foreach ($opportunity_result as $o) {
                    if ($o->id != $my_id) {
                        $result[$k]['id'] = $o->id;
                        $result[$k]['group_id'] = $o->id;
                        $result[$k]['name'] = $o->title;
                        $result[$k]['user_role'] = 'opportunity';
                        $result[$k]['logo_img'] = $o->logo_img;
                        $result[$k]['city'] = $o->city;
                        $result[$k]['state'] = $o->state;
                        $result[$k]['country'] = "";
                        $friend = Group_member::where('user_id', $my_id)->where('group_id',
                                        $o->id)->where('status', 2)->get()->first();
                        if ($friend == null) {
                            $result[$k]['is_friend'] = 0;
                        } else {
                            $result[$k]['is_friend'] = 1;
                        }
                        $result[$k]['is_followed'] = Follow::where('follower_id', $my_id)->where('type',
                                        'organization')->where('followed_id', $o->id)->where('is_deleted',
                                        '<>', 1)->count();
                        $k++;
                    }
                }
            }
        }
        if ($search_filter == '') {
            $search_filter = array();
        }
        $result = $this->paginate($result);
        $result->withPath(url()->current());
        return view('organization.search',
                ['keyword' => $keyword, 'result' => $result, 'filter' => $search_filter, 'page_name' => '']);
    }

    public function paginate($items, $perPage = 20, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function followOrganization(Request $request) {
        $id = $request->input('id');
        $my_id = Auth::user()->id;
        $is_exist = Follow::where('follower_id', $my_id)->where('type',
                        'organization')->where('followed_id', $id)->get()->first();
        if ($is_exist == null) {
            $follower = new Follow;
            $follower->follower_id = $my_id;
            $follower->type = 'organization';
            $follower->followed_id = $id;
            $follower->save();
        } else {
            $is_exist->is_deleted = 0;
            $is_exist->save();
        }
        $alert = new Alert;
        $alert->receiver_id = $id;
        $alert->sender_id = $my_id;
        $alert->sender_type = 'organization';
        $alert->alert_type = Alert::ALERT_FOLLOW;
        $alert->contents = ' followed you!';
        $alert->save();
        return Response::json(['result' => 'success']);
    }

    public function unfollowOrganization(Request $request) {
        $id = $request->input('id');
        $my_id = Auth::user()->id;
        $is_exist = Follow::where('follower_id', $my_id)->where('type',
                        'organization')->where('followed_id', $id)->get()->first();
        $is_exist->is_deleted = 1;
        $is_exist->save();

        $alert = new Alert;
        $alert->receiver_id = $id;
        $alert->sender_id = $my_id;
        $alert->sender_type = 'organization';
        $alert->alert_type = Alert::ALERT_FOLLOW;
        $alert->contents = ' unfollowed you!';
        $alert->save();
        return Response::json(['result' => 'success']);
    }

    public function connectOrganization(Request $request) {
        $id = $request->input('id');
        $currentUser = Auth::user();
        $receiverUser = User::find($id);
        $my_id = $currentUser->id;

        $is_exist = Friend::where('user_id', $my_id)->where('friend_id', $id)->get()->first();
        if ($is_exist == null) {
            $mine = new Friend;
            $mine->user_id = $my_id;
            $mine->friend_id = $id;
            $mine->status = Friend::FRIEND_PENDING;
            $mine->save();

            $friend = new Friend;
            $friend->user_id = $id;
            $friend->friend_id = $my_id;
            $friend->status = Friend::FRIEND_GET_REQUEST;
            $friend->save();
        } else {
            $is_exist->status = Friend::FRIEND_PENDING;
            $is_exist->is_deleted = 0;
            $is_exist->save();

            $friend = Friend::where('user_id', $id)->where('friend_id',
                            $my_id)->get()->first();
            $friend->is_deleted = 0;
            $friend->status = Friend::FRIEND_GET_REQUEST;
            $friend->save();
        }


        $alert = new Alert;
        $alert->receiver_id = $id;
        $alert->sender_id = $my_id;
        $alert->sender_type = 'organization';
        $alert->alert_type = Alert::ALERT_CONNECT_CONFIRM_REQUEST;
        $alert->contents = ' want keep connection with you.';
        $alert->save();

        return Response::json(['result' => 'success']);
    }

    public function disconnectFriend($id = null) {
        $my_id = Auth::user()->id;
        $is_exist_mine = Friend::where('user_id', $my_id)->where('friend_id', $id)->get()->first();
        if ($is_exist_mine != null) {
            $is_exist_mine->delete();
        }
        $is_exist_opp = Friend::where('user_id', $id)->where('friend_id', $my_id)->get()->first();
        if ($is_exist_opp != null) {
            $is_exist_opp->delete();
        }
        $alert = new Alert;
        $alert->receiver_id = $id;
        $alert->sender_id = $my_id;
        $alert->sender_type = 'organization';
        $alert->alert_type = Alert::ALERT_DISCONNECT;
        $alert->contents = ' disconnect with you!';
        $alert->save();

        $new_request = DB::table('users')
                ->join('friends', 'users.id', '=', 'friends.user_id')
                ->where('friends.friend_id', $my_id)
                ->where('friends.is_deleted', '<>', 1)
                ->where('friends.status', 1)
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', 1)
                ->select('users.*', 'friends.*')
                ->get();

        $friends = DB::table('users')
                ->join('friends', 'users.id', '=', 'friends.friend_id')
                ->where('friends.user_id', $my_id)
                ->where('friends.is_deleted', '<>', 1)
                ->where('friends.status', 2)
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', 1)
                ->select('users.*', 'friends.*')
                ->paginate(12);

        return view('organization.friend',
                ['page_name' => 'org_friend',
                    'nameClassUl' => 'friends-list-main',
                    'friend_requests' => $new_request,
                    'friends' => $friends,
                    'user_id' => $my_id]);
    }

    public function acceptFriend(Request $request) {
        $sender = $request->input('id');
        $receiver = Auth::user()->id;

        $this->acceptFriendRequestOrg($sender, $receiver);

        return Response::json(['result' => 'success']);
    }

    /**
     * @param $id
     */
    public function acceptFriendRequestOrg($receiver_id, $sender_id) {
        $mine = Friend::where('user_id', $sender_id)->where('friend_id', $receiver_id)->get()->first();
        if ($mine) {
            $mine->status = Friend::FRIEND_APPROVED;
            $mine->save();
        }

        $friend = Friend::where('user_id', $receiver_id)->where('friend_id',
                        $sender_id)->get()->first();
        if ($friend) {
            $friend->status = Friend::FRIEND_APPROVED;
            $friend->save();
        }

        $alert = new Alert;
        $alert->receiver_id = $receiver_id;
        $alert->sender_id = $sender_id;
        $alert->sender_type = 'organization';
        $alert->alert_type = Alert::ALERT_ACCEPT;
        $alert->contents = ' accept connection.';
        $alert->save();

        $chatManager = new ChatManager();
        $chatManager->join($receiver_id);
        return true;
    }

    public function joinGroup(Request $request) {
        //SAME CODE REPEATING IN VOLONTEER HOMECTRL LINE 1157
        $group_id = $request->input('group_id');
        $group_list = Group::where('id', $group_id)->first();
        $creator = User::find($group_list->creator_id);
        $sender = Auth::user();
        $related_id = 0;

        $autoAccept = false;

        $is_exist = Group_member::where('group_id', $group_id)->where('user_id',
                        $sender->id)->get()->first();

        if ($is_exist == null) {
            $member = new Group_member;
            $member->group_id = $group_id;
            $member->user_id = $sender->id;
            $member->role_id = Group_member_role::MEMBER;
            $member->status = Group_member::PENDING;

            if ($group_list->auto_accept_join) {
                $autoAccept = true;
                $member->status = Group_member::APPROVED;
            }

            $member->save();
            $related_id = $member->id;
        } else {
            $is_exist->is_deleted = 0;
            $is_exist->status = Group_member::PENDING;

            if ($group_list->auto_accept_join) {
                $autoAccept = true;
                $is_exist->status = Group_member::APPROVED;
            }

            $is_exist->save();
            $related_id = $is_exist->id;
        }

        if ($autoAccept) {
            $chatManager = new ChatManager();
            $chatManager->joinToGroup($group_id);

            $newsfeed = new NewsfeedModel;
            $newsfeed->who_joined = $sender->id;
            $newsfeed->related_id = $group_list->creator_id;
            $newsfeed->table_name = 'group_members';
            $newsfeed->table_id = $related_id;
            $newsfeed->reason = 'joined in a new group';
            $newsfeed->created_at = date('Y-m-d H:i:s');
            $newsfeed->updated_at = date('Y-m-d H:i:s');
            //$newsfeed->save();
        } else
            $this->sendGroupJoinAlert($creator, $sender, $group_list, $related_id);

        return Response::json(['result' => 'success', 'autoAccept' => $autoAccept]);
    }

    public function sendGroupJoinAlert($receiver, $sender, $group, $related_id) {
        $alerts = new Alert;
        $alerts->receiver_id = $receiver->id;
        $alerts->sender_id = $sender->id;
        $alerts->sender_type = $receiver->user_role;
        $alerts->alert_type = 10;
        $alerts->related_id = $related_id;
        $alerts->contents = 'MyVoluntier want to join ' . $group->name . ' group';
        $alerts->save();
    }

    public function sendMessage(Request $request) {

        $sender_id = Auth::user()->id;
        $msg = new Message;

        if ($sender_id != null) {

            $msg->receiver_id = $request->input('receiver_id');
            $msg->content = $request->input('content');
            $msg->sender_id = $sender_id;
            $msg->created_at = date('Y-m-d H:i:s');
            $msg->updated_at = date('Y-m-d H:i:s');

            $save = $msg->save();

            if ($save) {
                $returnArr = array('status' => '1', 'msg' => 'Message send successfully!');
                if ($msg)
                    event(new UserChatNotifyEvent($msg));
            } else {
                $returnArr = array('status' => '0', 'msg' => 'Failure');
            }

            echo json_encode($returnArr);
            die();
        }
    }

    public function editSubOrganization($id = null) {
        $id = base64_decode($id);
        $subOrg = User::where('id', $id)
                ->where('is_deleted', '<>', 1)
                ->get()
                ->toArray();

        return view('organization.edit_sub_org', ['org_type_names' => Organization_type::all(), 'subOrgData' => $subOrg]);
    }

    //Method to delete Service Project
    public function deleteSubOrganization($id = null) {

        if (!empty($id)) {
            $id = base64_decode($id);
            $userObj = User::find($id);
            $userObj->is_deleted = 1;
            $userObj->save();
            //Service_project::where('id', $id)->where('volunteer_id', Auth::user()->id)->delete();
            //Service_project_hour::where('service_project_id', $id)->delete();
            session()->flash('app_message', 'You have successfully deleted sub organization.');
        } else {
            session()->flash('app_error', 'Something went wrong.');
        }

        return redirect()->to('/organization/profile#subOrganization');
    }

    public function nonAuthOrganization() {

        $heading = 'Manage Organizations';

        $list = User::where('user_role', 'organization')->where('is_deleted', '0')->orderBy('id', 'DESC')->get();

        $org_type_names = \App\Organization_type::all();
        $school_type = \App\School_type::all();
        //code repeating in user voll type too same code
        return view('non_auth_organization', compact('heading', 'list', 'org_type_names', 'school_type'));
    }

    /* organization email blast */

    public function orgEmailblast(Request $request) {
        $domain = trim($request->input('verifiedDomain'));
        $groups = $request->input('groups');
        $message = trim($request->input('message'));
        if (!empty($domain)) {
            $members = $this->getVolnteerMemeber($domain);
            $errorMsg = 'No volunteer members found for selected domain.';
            if (empty($members->toArray())) {
                return Response::json(['result' => 'error', 'message' => $errorMsg]);
            }
            foreach ($members as $vr) {
                $this->sendEmailToVolMemeber($message, $vr);
            }
        } elseif (!empty($groups)) {
            $members = $this->getGroupMemebers($groups);
            $errorMsg = "No member found for selected group(s).";
            if (empty($members->toArray())) {
                return Response::json(['result' => 'error', 'message' => $errorMsg]);
            }
            foreach ($members as $vr) {
                $this->sendEmailToVolMemeber($message, $vr->user);
            }
        }
        return Response::json(['result' => 'success']);
    }

    /**
     * Method to get Volunteer for particular domain.
     * @param type $domain
     * @return type
     */
    private function getVolnteerMemeber($domain) {
        if (strpos($domain, 'www.') !== false) {
            $domain = explode('www.', $domain)[1];
        }
        $volnteers = User::select('email', 'id', 'first_name', 'last_name')
                        ->where('user_role', 'volunteer')->where('is_deleted', '0')
                        ->where('confirm_code', '1')
                        ->where(function ($query) use ($domain) {
                            $query->where('email', 'like', '%' . $domain . '%');
                        })->Orwhere(function ($query) use ($domain) {
                    $query->where('email2', 'like', '%' . $domain . '%')
                            ->where('confirm_code2', '1');
                })->Orwhere(function ($query) use ($domain) {
                    $query->where('email3', 'like', '%' . $domain . '%')
                            ->where('confirm_code3', '1');
                })->get();
        return $volnteers;
    }

    /**
     * Method to get Group memebers
     * @param type $groupIds
     * @return type
     */
    private function getGroupMemebers($groupIds = array()) {
        $members = Group_member::join('users', 'users.id', '=', 'group_members.user_id')
                ->whereIn('group_id', $groupIds)
                ->where('group_members.status', Group_member::APPROVED)
                ->where('group_members.is_deleted', '<>', 1)
                ->where('users.id', '<>', Auth::user()->id)
                ->select(DB::raw('DISTINCT(user_id)'), 'users.email', 'users.id', 'users.first_name', 'users.last_name')
                ->get();
        return $members;
    }

    //Method to send email for service project 
    public function sendEmailToVolMemeber($message, $user) {
        $userAuth = Auth::user();
        $fromName = $userAuth->org_name;
        $subject = 'Organization Email Blast' . ':' . $fromName;
        $data['content'] = $message;
        $data['sender_name'] = $fromName;
        $data['sender_email'] = $userAuth->email;
        $email = EmailsTemplate::where('type', 'organizationEmailBlast')->first();
        $body = replaceBodyContents($email->body, $email->type, $user, null, null, $data);
        \Mail::to($user->email)->send(new SharingProfileMail($body, $subject, $userAuth->email, $fromName));
    }

    /* end organization email blast */

    public function approveSubOrgRequest($id, $related_id, $status = 1) {

        $user = User::find($related_id);

        if (!empty($user)) {

            if ($status == 1) {
                if ($id != $related_id) {
                    Alert::where('id', $id)->update(['is_apporved' => 1]);
                }
                $user->confirm_code = '1';
                $user->status = 1;
                $user->save();
            } else {
                if ($id != $related_id) {
                    Alert::where('id', $id)->update(['is_apporved' => 2]);
                }
                $user->confirm_code = null;
                $user->status = 0;
                $user->save();
            }
        }
        if ($id == $related_id) {
            session()->flash('app_message', 'Status has been updated.');
            return redirect()->to('/organization/profile#subOrganization');
        } else {
            return redirect()->to('/viewAlert');
        }
    }

}
