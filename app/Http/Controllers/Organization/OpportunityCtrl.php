<?php

namespace App\Http\Controllers\Organization;

use App\Alert;
use App\Chat;
use App\Mail\UsersDefaultMail;
use App\Mail\SharingProfileMail;
use App\Models\EmailsTemplate;
use App\Services\ChatManager;
use App\Opportunity;
use App\Opportunity_category;
use App\NewsfeedModel;
use App\Opportunity_member;
use App\Services\ChatService;
use App\Tracking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Auth;
use App\Http\Controllers\Controller;
use App\News;
use App\User;
use App\CommonModel;
use Illuminate\Support\Facades\DB;
use App\Services\ImageExif;
use App\Selected_categoty;
use Illuminate\Support\Facades\Validator;
use App\Organization_type;
use App\School_type;
use App\Models\Attributelookup;
use App\Customattributes;
use App\{Group,Group_member,Survey,SurveyOptions,SurveyQuestions};
use Redirect;
use Illuminate\Support\Facades\Input;
use App\SurveyResult;
use Illuminate\Support\Facades\Crypt;
use  App\SurveyEmail;

class OpportunityCtrl extends Controller {

    public function viewPostingPage()
    {
        $opportunityCategory = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();
        $states = Opportunity::getStates();
        return view('organization.post_opportunity', [
            'user_info' => Auth::user(),
            'opportunity_category' => $opportunityCategory,
            'states' => $states
        ]);

    }

    public function viewUpdatingPage($id = null) {

        $opportunityCategory = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();
        $states = Opportunity::getStates();

        if ($id != null) {
            $exist_oppr = Opportunity::owner()->find($id);
            if(!$exist_oppr) return view('errors.403');

            $cat_array = array();
            $all_categories = Selected_categoty::where('opportunities_id', $id)->get()->toArray();

            if (!empty($all_categories)) {
                foreach ($all_categories as $key => $cat) {
                    array_push($cat_array, $cat['category_id']);
                }
            }

            $exist_oppr->selected_cat = $cat_array;
            $members = Opportunity_member::select('id')->where('oppor_id', $id)->where('status', 1)->limit(1)->get()->toArray();

            return view('organization.update_opportunity', [
                'members' => $members,
                'states' => $states,
                'oppr_info' => $exist_oppr,
                'opportunity_category' => $opportunityCategory,
                'user_info' => Auth::user(), 'page_name' => ''
            ]);

        } else {

            return view('organization.post_opportunity', [
                'user_info' => Auth::user(),
                'opportunity_category' => $opportunityCategory,
                'states' => $states,
            ]);
        }
    }

    public function viewManageOpportunity()
    {
        $today = date("Y-m-d");
        $active_oppr = Opportunity::owner()->where('type', 1)->where('is_deleted', '<>', 1)->where('end_date', '>=', $today)->orderBy('created_at', 'desc')->paginate(2);
        $expired_oppr = Opportunity::owner()->where('type', 1)->where('is_deleted', '<>', 1)->where('end_date', '<', $today)->orderBy('created_at', 'desc')->paginate(2);
        return view('organization.opportunity', [
            'active_oppors' => $active_oppr,
            'expired_oppors' => $expired_oppr,
            'user_info' => Auth::user(),
            'page_name' => 'org_oppor'
        ]);
    }

    public function viewOpportunity($id) {

        $logeed_in_user = Auth::user();
        $oppr = Opportunity::find($id);

        $all_frnds = DB::table('users')
                ->join('friends', 'users.id', '=', 'friends.friend_id')
                ->where('friends.user_id', Auth::user()->id)
                ->where('friends.is_deleted', '<>', 1)
                ->where('friends.status', 2)
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', 1)
                ->select('users.*', 'friends.*')
                ->get();

        $opportunityCategory = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();

        $cat_array = array();
        $all_categories = Selected_categoty::where('opportunities_id', $id)->get()->toArray();

        if (!empty($all_categories)) {
            foreach ($all_categories as $key => $cat) {
                array_push($cat_array, $cat['category_id']);
            }
        }

        $oppr->selected_cat = $cat_array;

        return view('organization.viewOpportunity', ['oppr_info' => $oppr, 'opportunity_category' => $opportunityCategory, 'user' => $logeed_in_user, 'page_name' => '', 'all_friends' => $all_frnds]);
    }

    public function shareOpportunity($id) {

        $oppr = Opportunity::find($id);

        $opportunityCategory = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();

        $cat_array = array();
        $all_categories = Selected_categoty::where('opportunities_id', $id)->get()->toArray();

        if (!empty($all_categories)) {
            foreach ($all_categories as $key => $cat) {
                array_push($cat_array, $cat['category_id']);
            }
        }

        $oppr->selected_cat = $cat_array;
        return view('organization.shareOpportunity', ['oppr_info' => $oppr, 'opportunity_category' => $opportunityCategory, 'page_name' => '', 'org_type_names' => Organization_type::all(), 'school_type' => School_type::all()]);
    }

    public function share_to_friends(Request $request) {

        $logeed_in_user = Auth::user();
        $oppr = Opportunity::find($request->opportunity_id);
        $share_to = $request->friend_ids;

        if (!empty($share_to) && $oppr) {

            $message = $logeed_in_user->user_name . " share " . $oppr->title . " opportunity with you.";

            foreach ($share_to as $key => $to) {
                $insert_notification = new Alert;
                $insert_notification->receiver_id = $to;
                $insert_notification->sender_id = $logeed_in_user->id;
                $insert_notification->sender_type = $logeed_in_user->user_role;
                $insert_notification->alert_type = 1;
                $insert_notification->contents = $message;

                //$insert_notification->is_checked    =   1;
                $insert_notification->oppotunity_id = $request->opportunity_id;
                $insert_notification->save();
            }
            session()->flash('app_message', 'Opportunity shared successfully');
        } else {
            session()->flash('app_message', 'Opportunity/Friends not found');
        }

        return \Redirect::back();
    }

    public function postOpportunity(Request $request) {

        $nonExpire = $request->get('non_expiring') ? 1 : 0;
        $allowTracking = (!empty($request->get('allow_tracking')))? 1 : 0;

        $opportunity = new Opportunity;
        $opportunity->org_id = Auth::user()->id;
        $opportunity->org_type = Auth::user()->org_type;
        $opportunity->title = $request->get('title');
        // $opportunity->category_id = $request->get('opportunity_type');
        $opportunity->description = $request->get('description');
        $opportunity->min_age = $request->get('min_age');
        $opportunity->activity = $request->get('activity');
        $opportunity->qualification = $request->get('qualification');
        $opportunity->street_addr1 = $request->get('street1');
        $opportunity->street_addr2 = $request->get('street2');
        $opportunity->city = $request->get('city');
        $opportunity->state = $request->get('state');
        $opportunity->zipcode = $request->get('zipcode');
        $opportunity->max_concurrent_vol = $request->get('max_concurrent_vol');
        $opportunity->additional_info = $request->get('add_info');
        $opportunity->allow_tracking = $allowTracking;
        $opportunity->start_date = dateYmdFormat($request->get('start_date'));

        $opportunity->non_expiring = $nonExpire;
        $opportunity->end_date = $nonExpire ? Carbon::now()->addYear(20)->toDateString() : dateYmdFormat($request->get('end_date'));
//        I like your 1st suggestion. Add 20 years and make it the end date of the non-expiring flag is set.

        $opportunity->start_at = $request->get('start_at');
        $opportunity->end_at = $request->get('end_at');
        $opportunity->weekdays = $request->get('weekday_vals');
        $opportunity->contact_name = $request->get('contact_name');
        $opportunity->contact_email = $request->get('contact_email');

        $opportunity->auto_accept = $request->get('auto_accept');
        $opportunity->private_passcode = $request->get('auto_accept') == 1 ? $request->get('private_passcode') : null;

        $opportunity->contact_number = $request->get('contact_phone');
        $opportunity->is_deleted = 0;
        $location = $this->getLocation($request->get('street1') . ', ' . $request->get('city') . ', ' . $request->get('state'));

        if ($location != 'error') {
            $opportunity->lat = $location['lat'];
            $opportunity->lng = $location['lng'];
        }

        $photo = asset('img/org/001.png');

        if ($request->get('image_content')) {
            $opportunity->logo_img = base64fileUploadOnS3(null,$request->image_content ,Opportunity::S3PathLogo,$opportunity->org_id.'logo');
            $photo = $opportunity->logo_img;
        }

        $opportunity->save();

        // save category -- start
        $opportunity_id = $opportunity->id;

        if (isset($request->opportunity_type) && $opportunity_id) {
            $opportunity_type_array = $request->opportunity_type;

            foreach ($opportunity_type_array as $key => $opportunity_type_id) {
                $selected_cat = new Selected_categoty;
                $selected_cat->opportunities_id = $opportunity_id;
                $selected_cat->category_id = $opportunity_type_id;
                $selected_cat->status = 1;
                $selected_cat->created_by = Auth::user()->id;
                $selected_cat->updated_by = Auth::user()->id;
                $selected_cat->save();
            }
        }
        // save category -- end

        $chat_service = new ChatService();
        $chatId = $chat_service->createChat($opportunity->title, $photo);
        $chat_service->addUserToChat(Auth::user(), $chatId, $opportunity->title, 'organizations');
        $chat = new Chat();
        $chat->chat_id = $chatId;
        $chat->user_id = Auth::user()->id;
        $chat->group_id = $opportunity->id;
        $chat->type = 'organizations';
        $chat->save();

        $insert_id = $opportunity->id;

        if ($insert_id) {
            $newsfeed = new NewsfeedModel;
            $newsfeed->who_joined = Auth::user()->id;
            $newsfeed->related_id = 0;
            $newsfeed->table_name = 'opportunities';
            $newsfeed->table_id = $insert_id;
            $newsfeed->reason = 'added opportunity';
            $newsfeed->created_at = date('Y-m-d H:i:s');
            $newsfeed->updated_at = date('Y-m-d H:i:s');
            $newsfeed->save();
        }

        return redirect()->to('/organization/view_opportunity/' . $opportunity->id);
    }

    public function updateOpportunity(Request $request, $id) {

        $nonExpire = $request->get('non_expiring') ? 1 : 0;

        $opportunity = Opportunity::find($id);
        $opportunity->title = $request->get('title');
        // $opportunity->category_id = $request->get('opportunity_type');
        $opportunity->description = $request->get('description');
        $opportunity->min_age = $request->get('min_age');
        $opportunity->activity = $request->get('activity');
        $opportunity->qualification = $request->get('qualification');
        $opportunity->street_addr1 = $request->get('street1');
        $opportunity->street_addr2 = $request->get('street2');
        $opportunity->city = $request->get('city');
        $opportunity->state = $request->get('state');
        $opportunity->zipcode = $request->get('zipcode');
        $opportunity->max_concurrent_vol = $request->get('max_concurrent_vol');
        $opportunity->additional_info = $request->get('add_info');
        $opportunity->allow_tracking = $request->get('allow_tracking');

        $opportunity->start_date = dateYmdFormat($request->get('start_date'));
        $opportunity->non_expiring = $nonExpire;
        $opportunity->end_date = $nonExpire ? Carbon::now()->addYear(20)->toDateString() : dateYmdFormat($request->get('end_date'));
//      I like your 1st suggestion. Add 20 years and make it the end date of the non-expiring flag is set.

        //        "start_date" => "02-29-2020"
        //        "end_date" => "04-30-2021"

        $opportunity->start_at = $request->get('start_at');
        $opportunity->end_at = $request->get('end_at');
        $opportunity->weekdays = $request->get('weekday_vals');
        $opportunity->contact_name = $request->get('contact_name');
        $opportunity->contact_email = $request->get('contact_email');
        $opportunity->auto_accept = $request->get('auto_accept');
        $opportunity->private_passcode = $request->get('auto_accept') == 1 ? $request->get('private_passcode') : null;

        $opportunity->contact_number = $request->get('contact_phone');

        $location = $this->getLocation($request->get('street1') . ', ' . $request->get('city') . ', ' . $request->get('state'));

        if ($location != 'error') {
            $opportunity->lat = $location['lat'];
            $opportunity->lng = $location['lng'];
        }

        $photo = asset('img/org/001.png');

        if ($request->get('image_content')) {
            $opportunity->logo_img = base64fileUploadOnS3($opportunity->logo_img,$request->image_content ,Opportunity::S3PathLogo,$opportunity->org_id.'logo');
            $photo = $opportunity->logo_img;
        }

        $opportunity->save();

        if (isset($request->opportunity_type) && $id) {

            Selected_categoty::where('opportunities_id', $id)->delete();
            $opportunity_type_array = $request->opportunity_type;

            foreach ($opportunity_type_array as $key => $opportunity_type_id) {
                $selected_cat = new Selected_categoty;
                $selected_cat->opportunities_id = $id;
                $selected_cat->category_id = $opportunity_type_id;
                $selected_cat->status = 1;
                $selected_cat->created_by = Auth::user()->id;
                $selected_cat->updated_by = Auth::user()->id;
                $selected_cat->save();
            }
        }

        $opp_chat = Chat::where('group_id', $opportunity->id)->first();
        $manager = new ChatManager();

        if ($opp_chat == null) {
            $chat_service = new ChatService();
            $chatId = $chat_service->createChat($opportunity->title, $photo);
            $chat_service->addUserToChat(Auth::user(), $chatId, $opportunity->title, 'organizations');
            $chat = new Chat();
            $chat->chat_id = $chatId;
            $chat->user_id = Auth::user()->id;
            $chat->group_id = $opportunity->id;
            $chat->type = 'organizations';
            $chat->save();
        }

        $members = Opportunity_member::where('oppor_id', $opportunity->id)->where('status', 1)->get();

        foreach ($members as $m) {
            $vol = User::find($m->user_id);
            if ($vol != null) {
                $manager->joinOrganization($vol, $opportunity);
            }
        }

        return redirect()->to('/organization/view_opportunity/' . $id);
    }

    public function acceptOpportunityMember(Request $request) {

        $oppr_id = $request->input('oppr_id');
        $user_id = $request->input('user_id');
        $member = Opportunity_member::where('oppor_id', $oppr_id)->where('user_id', $user_id)->get()->first();
        $member->status = 1;
        $member->save();
    }

    public function deleteOpportunity(Request $request) {

        $id = $request->input('oppr_id');
        $opportunity = Opportunity::find($id);
        $opportunity->is_deleted = 1;
        $opportunity->save();
        $chat = Chat::where('group_id', $id)->first();
        $chatManager = new ChatManager();
        $chatService = new ChatService();
        $vols = Opportunity_member::where('oppor_id', $id)->where('status', 1)->get();
        foreach ($vols as $vol) {
            $u = User::find($vol->user_id);
            $chatService->removeUserFromChat($chat->chat_id, $u->user_name, 'organizations');
        }
        $chatManager->deleteChat($chat);
        return Response::json(['result' => $id]);
    }

    public function getLocation($address) {
        /* get location from address */
        $address = str_replace(' ', '+', $address);
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $address . "&key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es";
        $result = json_decode(file_get_contents($url), true);
        if ($result['results'] == []) {
            return 'error';
        } else
            return $result['results'][0]['geometry']['location'];
    }

    public function approveJoinOpportunityRequest($id, $related_id, $status = 1) {

        $opportunity_member = Opportunity_member::find($related_id);

        if ($opportunity_member != null) {

            if ($status == 1) {
                $opportunity_member->status = $status;
                $opportunity_member->save();
                Alert::where('id', $id)->update(['related_id' => 0, 'is_apporved' => 1]);
            } else {
                $opportunity_member->delete();
                Alert::where('id', $id)->update(['related_id' => 0, 'is_apporved' => 2]);
            }

            $this->responseEmailForJoiningOpportunity($opportunity_member->user_id, $opportunity_member->oppor_id);
        }
        return redirect()->to('/viewAlert');
    }

    public function responseEmailForJoiningOpportunity($user_id, $opp_id) {

        $user = User::find($user_id);
        $opportunity = Opportunity::find($opp_id);

        $email = EmailsTemplate::where('type', 'confirmJoinOpportunityRequest')->first();

        if ($user->join_oppr_email == '0') { //if enabled emails
            $body = replaceBodyContents($email->body, $email->type, $user, null, $opportunity);
            \Mail::to($user->email)->send(new UsersDefaultMail($body, $email->title));
        }

        if ($user->join_oppr_text == '0' && $user->contact_number) { //if texts enabled
            $messageBody = replaceTextBodyContents($email->text_body, $email->type, $user, null, $opportunity);
            sendSMSByTwilio($user->contact_number, $messageBody);
        }
    }
    
    public function getAllOpp($opportunityLists, $orgTotalHours){

        $allOpportunities = [];
        foreach ($opportunityLists as $Oppor){

            $top5OpprChart[number_format($Oppor->getLoggedHoursSum(),1)] = $Oppor->title;
            $orgTotalHours = $orgTotalHours + $Oppor->getLoggedHoursSum() ;

            $oppr['name'] = $Oppor->title;
            $oppr['hours'] = $Oppor->getLoggedHoursSum();
            $oppr['volonteers'] = $Oppor->users->count();

            $allOpportunities[] = $oppr;
        }
        return $allOpportunities;
    }

    public function organizationImpactPage(){
      
        ini_set('memory_limit' , '2048M');
        $currentUser = $user = Auth::guard('web')->user();
        $top5OpprChart = [];
        $orgTotalHours = 0;
        $impactsData['ranking_all'] = 0;

      //        $volList['lastMonth'] = $volList['last6Month']  = $volList['lastYear']  = $volList['lastYearToDate'] = [];

        $subOrgId[] = Auth::user()->id;
        $subOrganization = User::select('id')->where('parent_id',Auth::user()->id)->where('is_deleted','<>',1)->get();
        if(!empty($subOrganization)){
            foreach ($subOrganization as $subOrg){
               $subOrgId[] = $subOrg->id;
            }
        }
        $opportunityLists = Opportunity::whereIn('org_id', $subOrgId)->active()->get();
        $allOpportunities = $this->getAllOpp($opportunityLists, $orgTotalHours);
        
        
        //        $opportunityGroupByCategory = Opportunity::owner()
        //            ->where('opportunities.is_deleted',0)
        //            ->join('opportunity_categories', 'opportunity_categories.id', '=', 'opportunities.category_id')
        //            ->selectRaw('opportunity_categories.name, COUNT(opportunities.id) as total')
        //            ->groupBy('category_id')
        //            ->get();

        $trackedHourOverall = Tracking::confirmedNotDel()
            ->select( [ 'org_id' ,
                \DB::raw("SUM(logged_mins) as SUM")
            ])
            ->groupBy('org_id')
            ->orderBy('SUM' ,'DESC')
            ->get();


//        $trackedHourForOrg = $trackedHourOverall->where('org_id' ,$currentUser->id)->first();

        $thisMonthFirstDay = date('Y-m-d', strtotime("first day of this month"));
        $currentDay = date('Y-m-d');
        //$impactsData['this_month_chart'] = CommonModel::getAll('tracked_hours', array(array('org_id', '=', $currentUser->id), array('approv_status', '=', 1), array('logged_date', '>=', $thisMonthFirstDay), array('logged_date', '<=', $currentDay)), '', '', '', 'logged_date', array('logged_date', 'SUM' => 'logged_mins'));
        $impactsData['this_month_chart'] = CommonModel::getAll('tracked_hours', 
            array(array('approv_status', '=', 1),
                array('logged_date', '>=', $thisMonthFirstDay), 
                array('logged_date', '<=', $currentDay)
            ), '', '', '',
            'logged_date', 
            array('logged_date', 'SUM' => 'logged_mins'),
            '',
            '',
            array('org_id', $subOrgId)  
            );
        
        $lastmonthFirstDay = date('Y-m-d', strtotime("first day of -1 month"));
        $lastmonthlastDay = date('Y-m-d', strtotime("last day of -1 month"));
        $impactsData['last_month_chart'] = CommonModel::getAll('tracked_hours', 
                    array(array('approv_status', '=', 1), 
                        array('logged_date', '>=', $lastmonthFirstDay), 
                        array('logged_date', '<=', $lastmonthlastDay)
                    ), '', '', '', 
                    'logged_date', 
                    array('logged_date', 'SUM' => 'logged_mins'),
                    '',
                    '',
                    array('org_id', $subOrgId) 
                );

        $sixmonthFirstDay = date('Y-m-d', strtotime("first day of -6 month"));
        $impactsData['last_6month_chart'] = CommonModel::getAll('tracked_hours', 
                        array(array('approv_status', '=', 1), 
                            array('logged_date', '>=', $sixmonthFirstDay), 
                            array('logged_date', '<=', $currentDay)
                        ), '', '', '', 
                        'MONTH', 
                        array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'),
                        '',
                        '',
                        array('org_id', $subOrgId)
                    );

        $lastYearFirstDay = date('Y-m-d', strtotime("first day of -12 month"));

        $impactsData['year_date_chart'] = CommonModel::getAll('tracked_hours', 
                    array(array('approv_status', '=', 1), 
                        array('logged_date', '>=', $lastYearFirstDay), 
                        array('logged_date', '<=', $currentDay)
                    ), '', '', '', 
                    'MONTH', 
                    array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'),
                    '',
                    '',
                    array('org_id', $subOrgId)
                );
        
        array_multisort(array_column($allOpportunities, 'hours'), SORT_DESC, $allOpportunities);

        foreach ($trackedHourOverall as $index => $track) {
            if(in_array($track->org_id,$subOrgId)) {
                $impactsData['ranking_all'] = $index + 1;
                break;
            }
        }
        
        
        $impactsData['ranking_org_type'] = $this->getOrgnizationTypeRanking($currentUser);

        $impactsData['ranking_type_name'] = $currentUser->type_name;

        $top5_list = $this->findTopRecordsForOrgnization($currentUser);

        $opportunity_member = Opportunity_member::whereIn('org_id', $subOrgId)->get();

        foreach($opportunity_member as $member) {

            if($member->created_at >= $lastmonthFirstDay && $member->created_at <= $lastmonthlastDay){
                $volList['lastMonth'][] =  $member;
            }

            if($member->created_at >= $sixmonthFirstDay && $member->created_at <= $currentDay){
                $volList['last6Month'][] =  $member;
            }

            if($member->created_at >= $lastYearFirstDay && $member->created_at <= $currentDay){
                $volList['lastYear'][] =  $member;
            }

            if($member->created_at <= $currentDay){
                $volList['lastYearToDate'][] =  $member;
            }
        }
         

        list(
            $impactsData['hours_oppr_type_chart'],
            $impactsData['total_opportunities'],
            $impactsData['tracked_hours'],
            $impactsData['avg_hrs_per_oppr'],
            $impactsData['top_5_opprtunities'],
            $impactsData['opprtunities_list'],
            $impactsData['volnt_ids_tracked_hours'],
            $impactsData['top_5_sub_org']) = $this->getOrgOpprsData($currentUser, $subOrgId);

        list(
            $impactsData['volunteers'],
            $impactsData['volunteers_avg'],
            $impactsData['volunteers_pie_zip'],
            $impactsData['volunteers_pie_age'],
            $impactsData['volunteersTop5Chart']) = $this->getOrgOpprVolonteers($impactsData['volnt_ids_tracked_hours'], $subOrgId);

        unset($impactsData['volnt_ids_tracked_hours']);
        
   

        $isVerifiedDomain =  User::where('id' ,Auth::user()->id)->where(function ($q){
            $q->where('domain_1_confirm_code' ,'1');
            $q->orWhere('domain_2_confirm_code' ,'1');
        })->first();

        //$impactsData['top_5_vol_chart']['heading'] = array_keys($impactsData['volunteersTop5Chart']);
        
        
       
        return view('organization.impact', ['page_name' => 'impact'], compact(
            'impactsData' ,
            'top5_list' ,
            'user',
            'isVerifiedDomain'
        ));

    }

    public function getOrgOpprVolonteers($orgVolunteersIds, $orgIds=array()){

        $data = [];
        $pieChartZip = [];
        $pieChartAge = [];
        $top5Chart = [];
        $pChartAge['13 – 17'] = $pChartAge['18 – 22'] =$pChartAge['23 – 30'] = $pChartAge['30 and older'] = 0;
        $totalHours = 0;

        $all_volunteersIds = [];

        $query = User::activeUser()->whereIn('id' ,$orgVolunteersIds);

        $grpMembers = (clone $query)->get();

        foreach ($grpMembers as $index => $user) {

            $object = [];
            $object['id'] = $user->id;
            $object['name'] = $user->getFullNameVolunteer();

            $object['hours'] = $user->tracksHoursForOrg(auth()->id(), $orgIds);
            $object['total_oppr'] = $user->totalOpprForOrgVolonteer(auth()->id(), $orgIds)->count();

            $top5Chart[$user->id.$object['name']] = $object['hours'];

            $data[] = $object;

            $totalHours = $totalHours + $object['hours'];
        }

        $all_volunteers_zip = (clone $query)
            ->groupBy('zipcode')
            ->selectRaw('zipcode, count(*) as total')
            ->orderBy('total' ,'asc')
            ->get();


        $all_volunteers_age = (clone $query)
            ->select( [
                \DB::raw("floor((DATEDIFF(CURRENT_DATE, STR_TO_DATE(birth_date, '%m/%d/%Y'))/365)) as age"),
                \DB::raw("count(*) as total")
            ])
            ->groupBy('age')
            ->orderBy('total' ,'asc')
            ->get();

        foreach ($all_volunteers_zip as $user){

            $pChartZip['name'] = "$user->zipcode";
            $pChartZip['y'] =  (int)$user->total;
            $pChartZip['color'] = rand_color();

            $pieChartZip[] = $pChartZip;
        }

        foreach ($all_volunteers_age as $user){

            if($user->age >= 13 && $user->age <= 17 )
                $pChartAge['13 – 17'] +=  (int)$user->total;

            if($user->age >= 18 && $user->age <= 22 )
                $pChartAge['18 – 22'] +=  (int)$user->total;

            if($user->age >= 23 && $user->age <= 30 )
                $pChartAge['23 – 30'] +=  (int)$user->total;

            if($user->age >= 30  )
                $pChartAge['30 and older'] +=  (int)$user->total;
        }

        ksort($pChartAge);

        foreach ($pChartAge as $index => $value){

            $array['name'] = "$index";
            $array['y'] = (int)$value;
            $array['color'] = rand_color();

            $pieChartAge[] = $array; //age group pie
        }

        array_multisort(array_column($data, 'hours'), SORT_DESC, $data);
        arsort($top5Chart);

        $top5Chart = array_slice($top5Chart, 0, 5, true);
//        $data = array_slice($data, 0, 10, true); or data tables

        $avg = count($data) > 0 ?  number_format($totalHours / count($data),1) : 0;

        return [$data , $avg ,$pieChartZip ,$pieChartAge ,$top5Chart];
    }

    public function getOrgOpprsData($org, $orgIds=array()){

        //same code used for the groups controller too but with different logic
        $top5OpprChart = [];
        $orgTotalHours = 0;
        $volnt_ids_tracked_hours = [];

        $trackings = Tracking::confirmedNotDel()
            ->whereIn('org_id', $orgIds)
            ->where('oppor_id', '<>' ,0)
            ->groupBy('oppor_id')
            ->groupBy('logged_date')
            ->selectRaw('org_id,oppor_id ,logged_date, sum(logged_mins)/60 as opp_logged_hours')
            ->get();

//      $Oppr_ids = $trackings->pluck('oppor_id')->toArray();

        $Opportunities = Opportunity::active()->whereIn('org_id', $orgIds)->get();

        $allOpportunities = [];
        $top5SubOpprChart = [];
        foreach ($Opportunities as $Oppor){

            if(count($Oppor->users->pluck('id'))){
                $volnt_ids_tracked_hours = array_merge($volnt_ids_tracked_hours ,$Oppor->users->pluck('id')->toArray());
            }

            $top5OpprChart[$Oppor->title] = number_format($Oppor->getLoggedHoursSum(),1);
            if($Oppor->org_id != Auth::user()->id){
                
                $top5SubOpprChart[$Oppor->org_name] = number_format($Oppor->getLoggedHoursSum(),1);
            }
            $orgTotalHours = $orgTotalHours + $Oppor->getLoggedHoursSum() ;

            $oppr['name'] = $Oppor->title;
            $oppr['hours'] = $Oppor->getLoggedHoursSum();
            $oppr['volonteers'] = $Oppor->tracks ? $Oppor->tracks()->groupby('volunteer_id')->count() : 0;

            $allOpportunities[] = $oppr;
        }

        $orgTotalHoursAvg = count($Opportunities) > 0 ? $orgTotalHours / count($Opportunities) : 0;

        $yAxis = getLast12MonthsY();

        $headings = [];

        foreach ($trackings as $index => $tracking){

            $orgFind = Opportunity::active()->where(['org_id' => $tracking->org_id ,'id' => $tracking->oppor_id])->first();

            if($orgFind && $tracking->category_names){

                foreach (explode(',' ,$tracking->category_names) as $cat){

                    if (array_key_exists($cat ,$headings)){

                        if (isset($headings[$cat][dateMMYY($tracking->logged_date)]))
                            $headings[$cat][dateMMYY($tracking->logged_date)] = $headings[$cat][dateMMYY($tracking->logged_date)] + $tracking->opp_logged_hours;

                        else
                            $headings[$cat][dateMMYY($tracking->logged_date)] = $tracking->opp_logged_hours;
                    }

                    else
                        $headings[$cat][dateMMYY($tracking->logged_date)] = $tracking->opp_logged_hours;
                }
            }
        }

        $hours_oppr_type_chart = [];

        foreach ($headings as $ind => $heading){

            $hours_oppr_type_chart[$ind] = [0,0,0,0,0,0,0,0,0,0,0,0];

            foreach ($heading as $index =>  $hour){
                $ok = array_search($index, $yAxis);
                $hours_oppr_type_chart[$ind][$ok] = $hour;
            }
        }
        
        arsort($top5OpprChart);
        arsort($top5SubOpprChart);
        $top5OpprChart = array_slice($top5OpprChart, 0, 5, true);
        $top5SubOpprChart = array_slice($top5SubOpprChart, 0, 5, true);
        
        array_multisort(array_column($allOpportunities, 'hours'), SORT_DESC, $allOpportunities);
        
        return [$hours_oppr_type_chart ,count($allOpportunities) ,$orgTotalHours ,$orgTotalHoursAvg ,$top5OpprChart ,$allOpportunities ,$volnt_ids_tracked_hours, $top5SubOpprChart];
    }

    public function getOrgnizationTypeRanking($org){

        $allOrganizations = User::where('org_type' ,$org->org_type)->pluck('id');
        
        $tracked_hours = Tracking::confirmedNotDel()
            ->whereIn('org_id' ,$allOrganizations)
            ->groupBy('org_id')
            ->selectRaw('org_id, sum(logged_mins)/60 as logged_hours')
            ->orderBy('logged_hours' ,'desc')
            ->get();

        $rank = 0;

        foreach ($tracked_hours as $index => $track) {
            if($track->org_id == $org->id) {
                $rank = $index + 1;
                break;
            }
        }

        return $rank;
    }

    public function findTopRecordsForOrgnization($currentUser){

        $top5_list = array();
        $top5_list['is_top5_state_lastYear'] = 0;
        $top5_list['is_top10_country_lastYear'] = 0;
        $top5_list['is_top5_state_lastMonth'] = 0;
        $top5_list['is_top10_country_lastMonth'] = 0;
        $top5_list['is_top5_state'] = 0;
        $top5_list['is_top10_country'] = 0;

        $organizer_id = $currentUser->id;
        $country = $currentUser->country;
        $state = $currentUser->state;
        $last_year = date('Y') - 1;

        $lastmonthFirstDay = date('Y-m-d', strtotime("first day of -1 month"));
        $lastmonthlastDay = date('Y-m-d', strtotime("last day of -1 month"));

        $lastYear1 = $last_year.'-01-01';
        $lastYear12 = $last_year.'-12-31';

        $top_5_state_year = DB::table('users')
            ->join('opportunities', 'users.id', '=', 'opportunities.org_id')
            ->join('tracked_hours', 'opportunities.id', '=', 'tracked_hours.oppor_id')
            ->where('tracked_hours.approv_status', 1)
            ->where('tracked_hours.created_at', '>=', $lastYear1)
            ->where('tracked_hours.created_at', '<=', $lastYear12)
            ->where('users.is_deleted', '<>', 1)
            ->where('users.confirm_code', '=', 1)
            ->where('users.state', '=', "'" . $state . "'")
            ->groupBy('tracked_hours.org_id')
            ->orderBy(DB::raw('SUM(tracked_hours.logged_mins)', 'desc'))
            ->limit(5)
            ->select('tracked_hours.org_id')
            ->get();

        $top_5_state_year = $this->create_array_fron_2D($top_5_state_year, 'org_id');
        if (in_array($organizer_id, $top_5_state_year)) {
            $top5_list['is_top5_state_lastYear'] = 1;
        }

        $top_10_country_year = DB::table('users')
                ->join('opportunities', 'users.id', '=', 'opportunities.org_id')
                ->join('tracked_hours', 'opportunities.id', '=', 'tracked_hours.oppor_id')
                ->where('tracked_hours.approv_status', 1)
                ->where('tracked_hours.created_at', '>=', $lastYear1)
                ->where('tracked_hours.created_at', '<=', $lastYear12)
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', '=', 1)
                ->where('users.country', '=', $country)
                ->groupBy('tracked_hours.org_id')
                ->orderBy(DB::raw('SUM(tracked_hours.logged_mins)', 'desc'))
                ->limit(10)
                ->select('tracked_hours.org_id')
                ->get();

        $top_10_country_year = $this->create_array_fron_2D($top_10_country_year, 'org_id');
        if (in_array($organizer_id, $top_10_country_year)) {
            $top5_list['is_top10_country_lastYear'] = 1;
        }

        $top_10_country_month = DB::table('users')
                ->join('opportunities', 'users.id', '=', 'opportunities.org_id')
                ->join('tracked_hours', 'opportunities.id', '=', 'tracked_hours.oppor_id')
                ->where('tracked_hours.approv_status', 1)
                ->where('tracked_hours.created_at', '>=', $lastmonthFirstDay)
                ->where('tracked_hours.created_at', '<=',$lastmonthlastDay)
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', '=', 1)
                ->where('users.country', '=', $country)
                ->groupBy('tracked_hours.org_id')
                ->orderBy(DB::raw('SUM(tracked_hours.logged_mins)', 'desc'))
                ->limit(10)
                ->select('tracked_hours.org_id')
                ->get();

        $top_10_country_month = $this->create_array_fron_2D($top_10_country_month, 'org_id');
        if (in_array($organizer_id, $top_10_country_month)) {
            $top5_list['is_top10_country_lastMonth'] = 1;
        }

        $top_5_state_month = DB::table('users')
                ->join('opportunities', 'users.id', '=', 'opportunities.org_id')
                ->join('tracked_hours', 'opportunities.id', '=', 'tracked_hours.oppor_id')
                ->where('tracked_hours.approv_status', 1)
                ->where('tracked_hours.created_at', '>=', $lastmonthFirstDay)
                ->where('tracked_hours.created_at', '<=', $lastmonthlastDay)
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', '=', 1)
                ->where('users.state', '=', $state)
                ->groupBy('tracked_hours.org_id')
                ->orderBy(DB::raw('SUM(tracked_hours.logged_mins)', 'desc'))
                ->limit(5)
                ->select('tracked_hours.org_id')
                ->get();

        $top_5_state_month = $this->create_array_fron_2D($top_5_state_month, 'org_id');
        if (in_array($organizer_id, $top_5_state_month)) {
            $top5_list['is_top5_state_lastMonth'] = 1;
        }

        $top_5_state_vol = DB::table('users')
                ->join('opportunities', 'users.id', '=', 'opportunities.org_id')
                ->join('tracked_hours', 'opportunities.id', '=', 'tracked_hours.oppor_id')
                ->where('tracked_hours.approv_status', 1)
                //->where('tracked_hours.created_at', '>=', "'".$lastmonthFirstDay."'")
                //->where('tracked_hours.created_at', '<=', "'".$lastmonthlastDay."'")
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', '=', 1)
                ->where('users.state', '=', $state)
                ->groupBy('tracked_hours.org_id')
                ->orderBy(DB::raw('COUNT(tracked_hours.logged_mins)', 'desc'))
                ->limit(5)
                ->select('tracked_hours.org_id')
                ->get();

        $top_5_state_vol = $this->create_array_fron_2D($top_5_state_vol, 'org_id');
        if (in_array($organizer_id, $top_5_state_vol)) {
            $top5_list['is_top5_state'] = 1;
        }

        $top_10_country_vol = DB::table('users')
                ->join('opportunities', 'users.id', '=', 'opportunities.org_id')
                ->join('tracked_hours', 'opportunities.id', '=', 'tracked_hours.oppor_id')
                ->where('tracked_hours.approv_status', 1)
                //->where('tracked_hours.created_at', '>=', "'".$lastmonthFirstDay."'")
                //->where('tracked_hours.created_at', '<=', "'".$lastmonthlastDay."'")
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', '=', 1)
                ->where('users.country', '=', $country)
                ->groupBy('tracked_hours.org_id')
                ->orderBy(DB::raw('COUNT(tracked_hours.logged_mins)', 'desc'))
                ->limit(10)
                ->select('tracked_hours.org_id')
                ->get();

        $top_10_country_vol = $this->create_array_fron_2D($top_10_country_vol, 'org_id');
        if (in_array($organizer_id, $top_10_country_vol)) {
            $top5_list['is_top10_country'] = 1;
        }

        return $top5_list;
            }

    public function impactVolOppr(Request $request){

        $user = User::find($request->id); //vol_id

        $org_id = auth()->id(); //auth user
        $data = [];
        $opprIds = Opportunity_member::where('user_id', $user->id)->where('org_id', $org_id)->pluck('oppor_id')->toArray();

        $opportunities = Opportunity::where('is_deleted', '<>', 1)->whereIn('id', $opprIds)->get();

        $count = 0;
        foreach ($opportunities as $oppr) {
            $data[$count]['title'] = $oppr->title;
            $data[$count]['city'] = $oppr->city;
            $data[$count]['state'] = ($oppr->state) ? $oppr->state : 'n/a';
            $data[$count]['contact_name'] = ($oppr->contact_name) ? $oppr->contact_name : 'n/a';
            $data[$count]['contact_number'] = ($oppr->contact_number) ? $oppr->contact_number : 'n/a';
            $data[$count]['hours'] = $user->getLoggedHoursSum();
            $count++;
        }
        return Response::json($data);
    }

    function create_array_fron_2D($array, $key) {
        $ret_array = array();
        if (!empty($array)) {
            foreach ($array as $val) {
                array_push($ret_array, $val->$key);
            }
        }
        return $ret_array;
    }

    function msort($array, $key, $sort_flags = SORT_REGULAR) {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v->$key_key;
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                arsort($mapping);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }
                return $sorted;
            }
        }
        return $array;
    }

    function myfunction($products, $field, $value)
    {
        foreach ($products as $key => $product) {
            if ($product->$field === $value)
                return $key + 1;
        }
        return false;
    }

    public function cloneOpportunity($id = null) {

        $opportunityCategory = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();
        $states = Opportunity::getStates();
        if ($id != null) {
            $exist_oppr = Opportunity::find($id);
            $cat_array = array();
            $all_categories = Selected_categoty::where('opportunities_id', $id)->get()->toArray();

            if (!empty($all_categories)) {
                foreach ($all_categories as $key => $cat) {
                    array_push($cat_array, $cat['category_id']);
                }
            }

            $exist_oppr->selected_cat = $cat_array;

            return view('organization.update_opportunity', [
                'cloneOpportunity'=>true,
                'oppr_info' => $exist_oppr,
                'opportunity_category' => $opportunityCategory,
                'user_info' => Auth::user(),
                'states' => $states,
                'page_name' => ''
            ]);
        } else {
            return view('organization.post_opportunity', [
                'user_info' => Auth::user(),
                'opportunity_category' => $opportunityCategory,
                'states' => $states,
                'page_name' => ''
            ]);
        }
    }
    public function validatedOpportunity(Request $request){
        $title =  trim($request->input('title'));
        $opp_id =  trim($request->input('opp_id'));
        $getData = Opportunity::where('title', $title)
                            ->where('is_deleted', '0')
                            ->where('org_id', Auth::user()->id)
                            ->where('type', '1')
                            ->where('id','!=', $opp_id)
                            ->get();

        if(count($getData) == '0'){
                echo 'success';
        }else{
                echo "Title is already exist for other opportunity.";
        }
        exit;
    }
    
    public function emailblast(Request $request){
        $opprId =  trim($request->input('opprId'));
        $message =  trim($request->input('message'));
        
        $opportunity = Opportunity::select('title','id')->find($opprId);
        $members = Opportunity_member::where('oppor_id', $opprId)->where('status', 1)->get();
        foreach ($members as $m) {
            $user = User::select('email','first_name','last_name')->find($m->user_id);
            $this->sendEmailToMemeber($message,$user, $opportunity);
        }
        return Response::json(['result' => 'success']);
    }
    //Method to send email for service project 
    public function sendEmailToMemeber($message, $user, $opportunity) {
        $userAuth = Auth::user();
        $fromName = $userAuth->org_name;
        $subject = $opportunity->title.':'.$fromName;
        $data['content'] = $message;
        $data['sender_name'] = $fromName;
        $data['sender_email'] = $userAuth->email;
        $email = EmailsTemplate::where('type', 'opportunityEmailBlast')->first();
        $body = replaceBodyContents($email->body, $email->type, $user, null, null, $data);
         \Mail::to($user->email)->send(new SharingProfileMail($body, $subject, $userAuth->email, $fromName));

    }
    //start code for export opportunity members 
    //set headers and columns of CSV
    public function getHeaderData($fileName = 'Memebers'){
        $fileName = str_replace(" ", '_', $fileName);
        $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=".$fileName."_members.csv",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
        );

        $columns = array('Email','Alternate Email 1','Alternate Email 2', 'First Name', 'Middle Name', 'Last Name', 'Phone', 'Address', 'Age', 'Total Track Hours');
        $attributeNames = [];
        $atrributelookups = Attributelookup::select('attributekey','attributename')->where('is_deleted', '<>', 1)->orderBy('attributename', 'ASC')->get()->toArray();
        foreach ($atrributelookups as $key=>$val){
            $attributeNames[] = $val['attributename'];
        }
        $columns = array_merge($columns, $attributeNames);
        return array('headers'=>$headers, 'columns'=>$columns, 'atrributelookups'=>$atrributelookups);
    }
    

    //get member information
    public function getMemberInfo($vol, $atrributelookups, $member){
        $address = $vol->location;
        $dob =  date('Y-m-d',strtotime($vol->birth_date));
        $birthdate = new \DateTime($dob);
        $today   = new \DateTime('today');
        $age = $birthdate->diff($today)->y;
        $trackHours = $this->getTrackHours($member);
        $columnsVal = array($vol->email, $vol->email2, $vol->email3, $vol->first_name, $vol->middle_initial, $vol->last_name, $vol->contact_number, $address, $age, $trackHours);
        if(!empty($atrributelookups)){
            $attributeVal = [];
            foreach ($atrributelookups as $key=>$val){
                 $attributeVal[] = $this->getAttributeVal($val['attributekey'], $member->user_id);
            }
            $columnsVal = array_merge($columnsVal, $attributeVal);
        }
        return $columnsVal;
    }
    
    //export single opportunity members data
    public function exportOppMembers($id = null){
        if($id){
            $oppObj = Opportunity::Select('id','title')->find($id);
            $members = Opportunity_member::where('oppor_id', $id)->where('status', 1)->get();
            $getHeaders = $this->getHeaderData($oppObj->title);
            $columns = $getHeaders['columns'];
            $headers = $getHeaders['headers'];
            $atrributelookups = $getHeaders['atrributelookups'];
            $callback = function() use ($members, $columns, $atrributelookups)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);
                foreach ($members as $m) {
                    $vol = User::find($m->user_id);
                    if ($vol != null) {
                        $columnsVal = $this->getMemberInfo($vol, $atrributelookups, $m);
                        fputcsv($file, $columnsVal);
                    }
                }
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);
        }
    }
    
    //get custom attribute value of member 
    public function getAttributeVal($attributeKey, $userid){
        $customAttrs = Customattributes::where('userid', $userid)->where('attributekey', $attributeKey)->where('is_deleted', '<>', 1)->get()->toArray();
        $custAttrVal = [];
        if(!empty($customAttrs)){
            foreach ($customAttrs as $ckey=>$cval){
                $custAttrVal[] = $cval['attributevalue'];
            }
            return implode('|', $custAttrVal);
        }
        return '';
    }

    //get track hours 
    public function getTrackHours($member){
        $tracked_hours = Tracking::confirmedNotDel();
        if(isset($member->oppor_id)) {
            $opporId = $member->oppor_id;
            $tracked_hours = $tracked_hours->where('oppor_id' ,$opporId);
        }elseif(isset($member->group_id)){
             $groupId = $member->group_id;
             $tracked_hours = $tracked_hours->where('designated_group_id', $member->group_id);
        }
        $userId = $member->user_id;
        $tracked_hours = $tracked_hours->where('volunteer_id' ,$userId)
                            ->groupBy('volunteer_id')
                            ->selectRaw('sum(logged_mins)/60 as logged_hours')
                            ->orderBy('logged_hours' ,'desc')
                            ->get()->toArray();
        if(!empty($tracked_hours)){
            return number_format($tracked_hours[0]['logged_hours'],1);
        }
        return '0';
    }
    
    //export data for all opportunity of organization 
    public function exportAllOppMembers(){
        $getHeaders = $this->getHeaderData('Opportunity');
        $columns = $getHeaders['columns'];
        $headers = $getHeaders['headers'];
        $atrributelookups = $getHeaders['atrributelookups'];

        $columns = array_merge(array('Opportunity Name'), $columns);
            
        $active_oppr = Opportunity::Select('id','title')
                ->where('org_id', Auth::user()->id)
                ->where('type',1)
                ->where('is_deleted', '<>', '1')
                ->orderBy('created_at', 'desc')
                ->get();
        $callback = function() use ($active_oppr, $columns, $atrributelookups)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($active_oppr as $key=>$data){
                if(!empty($data->opportunity_member)){
                    foreach ($data->opportunity_member as $mkey=>$mdata){
                        
                        $vol = User::find($mdata->user_id);
                        if ($vol != null) {
                            $columnsVal = $this->getMemberInfo($vol, $atrributelookups, $mdata);
                            $columnsVal = array_merge(array($data->title), $columnsVal);
                            fputcsv($file, $columnsVal);
                        }
                    }
                }
            }
            fclose($file);
        };
       return Response::stream($callback, 200, $headers);
    }
    //end code for export opportunity members 
 
    public function viewReport(){
        if (Auth::check()) { 
            $vol_hours_by_month = [];
            $groupList;
            $curr_user = Auth::user();
            if(Auth::user()->user_role === 'organization'){                
                $orgId=Auth::user()->id; //949
                $groupList['groupId'] = $orgId; 
                //-------------- Voluntier Hours by Month Start -------------------
                $lastYearFirstDay = date('Y-m-d', strtotime("first day of -12 month"));    
                $currentDay = date('Y-m-d');   
                $org_hrs_data = CommonModel::getAll('tracked_hours', array(array('org_id', '=', $orgId), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), '', '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));                       
                $yAxis = array_reverse(getLast12MonthsY());   
                foreach ($yAxis as $ind => $month){
                    $hours=0;
                    foreach ($org_hrs_data as $index =>  $data){                
                        if(date("m", strtotime( $month)) === date("m", strtotime( $data->logged_date))){
                            $hours = ((int)$data->SUM)/60;
                        }               
                    }
                    array_push($vol_hours_by_month, $hours);
                }
                //-------------- Voluntier Hours by Month End ---------------------
                $opportunities =Opportunity::select('id')->where('org_id',$orgId)->get();
                $data= Opportunity_member::select('user_id')->distinct('user_id')->whereIn('oppor_id',$opportunities->pluck('id')->toArray())->get();
                $org_vols = $data->pluck('user_id')->toArray();
                list($groupList['val_hrs_type_chart']) = $this->getGrpMemberOpprsData($org_vols);
                list(
                    $groupList['volunteers'],
                    $groupList['volunteers_pie_zip'],
                    $groupList['volunteers_pie_age']) = $this->getGrpMemberOpprVolonteers($org_vols);
              
            //-------- groups ------------------
            $groupCount=[];
             $groupsQuery = DB::table('groups')
                    ->join('group_members', 'groups.id','=', 'group_members.group_id')
                    ->join('group_categories', 'groups.group_category_id', '=', 'group_categories.id')
                    ->where('group_members.user_id',$orgId)
                    ->where('group_members.is_deleted', '<>', 1)
                    ->where('groups.is_deleted','<>', 1);

            $groups = (clone $groupsQuery)->where('groups.affiliated_org_id', null)->select('groups.*', 'group_members.role_id', 'group_categories.name as categoryName')->get();
           
            foreach($groups as $grp)
            {
                $groupCount[$grp->id] = DB::table('group_members')
                ->join('users','group_members.user_id','=', 'users.id')
                ->where('group_members.group_id',$grp->id)
                ->where('users.user_role',"volunteer")
                ->where('group_members.status', '=', 2)
                ->count();
            }
          return view('organization.viewReport', $groupList,compact('vol_hours_by_month','curr_user','groups','groupCount'));

            }
        // ====================
        }else{
            return redirect()->to('/sign-in');
        }
    }

    public function memberHours(){
        $vol_hours_by_month = [];
        $groupList;
        $curr_user = Auth::user();               
            $orgId=Auth::user()->id; //949
            $groupList['groupId'] = $orgId; 
            //-------------- Voluntier Hours by Month Start -------------------
            $lastYearFirstDay = date('Y-m-d', strtotime("first day of -12 month"));    
            $currentDay = date('Y-m-d');   
            $org_hrs_data = CommonModel::getAll('tracked_hours', array(array('org_id', '=', $orgId), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), '', '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));                
            $yAxis = array_reverse(getLast12MonthsY());
            foreach ($yAxis as $ind => $month){
                $hours=0;
                foreach ($org_hrs_data as $index =>  $data){                
                    if(date("m", strtotime( $month)) === date("m", strtotime( $data->logged_date))){
                        $hours = ((int)$data->SUM)/60;
                    }               
                }
                array_push($vol_hours_by_month, $hours);
            }
                   
                //export csv file
             $fileName = str_replace(" ", '_', $curr_user->org_name);
               $headers = array(
                     "Content-type" => "text/csv",
                     "Content-Disposition" => "attachment; filename=".$fileName."_hours.csv",
                     "Pragma" => "no-cache",
                     "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                     "Expires" => "0"
             );
             $columns = $yAxis;
              $callback = function() use($vol_hours_by_month, $columns) {
                 $file = fopen('php://output', 'w');
                   fputcsv($file, $columns);
                   fputcsv($file, $vol_hours_by_month);
                 fclose($file);
                }; 
            return Response::stream($callback, 200, $headers);
    }
   

    public function volunteerHoursByType(){
        $groupList;
        $curr_user = Auth::user();               
            $orgId=Auth::user()->id; //949
            $volType = [];
            $volHours=[];
       
            //-------------- Voluntier Hours by Month End ---------------------
            $opportunities =Opportunity::select('id')->where('org_id',$orgId)->get();
            $data= Opportunity_member::select('user_id')->distinct('user_id')->whereIn('oppor_id',$opportunities->pluck('id')->toArray())->get();
            $org_vols = $data->pluck('user_id')->toArray();
            list($groupList['val_hrs_type_chart']) = $this->getGrpMemberOpprsData($org_vols);
            foreach($groupList['val_hrs_type_chart'] as $Axis){
                $volType[]=$Axis['yAxis'];
            }
            foreach($groupList['val_hrs_type_chart'] as $Axis){
                $volHours[]=$Axis['xAxis'];
            }

                //export csv file

             $fileName = str_replace(" ", '_', $curr_user->org_name);
               $headers = array(
                     "Content-type" => "text/csv",
                     "Content-Disposition" => "attachment; filename=".$fileName."_type.csv",
                     "Pragma" => "no-cache",
                     "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                     "Expires" => "0"
             );
             $columns = $volType;
              $callback = function() use($volHours, $columns) {
                 $file = fopen('php://output', 'w');
                   fputcsv($file, $columns);
                   fputcsv($file, $volHours);
                 fclose($file);
                }; 
             return Response::stream($callback, 200, $headers);
    }

    public function volunteersByAgeGroup(){
        $groupList;
        $curr_user = Auth::user();               
            $orgId=Auth::user()->id; //949
            $groupList['groupId'] = $orgId; 
            $volRange = [];
            $vols=[];
       
            //-------------- Voluntier Hours by Month End ---------------------
            $opportunities =Opportunity::select('id')->where('org_id',$orgId)->get();
            $data= Opportunity_member::select('user_id')->distinct('user_id')->whereIn('oppor_id',$opportunities->pluck('id')->toArray())->get();
            $org_vols = $data->pluck('user_id')->toArray();
               list(
                $groupList['volunteers'],
                $groupList['volunteers_pie_zip'],
                $groupList['volunteers_pie_age']) = $this->getGrpMemberOpprVolonteers($org_vols);
             
                foreach($groupList['volunteers_pie_age'] as $Axis){
                    $volRange[]=str_replace('–','to',$Axis['name']);
                }
                foreach($groupList['volunteers_pie_age'] as $Axis){
                    $vols[]=$Axis['y'];
                }
               
                //export csv file
               $fileName = str_replace(" ", '_', $curr_user->org_name);
               $headers = array(
                     "Content-type" => "text/csv",
                     "Content-Disposition" => "attachment; filename=".$fileName."_age.csv",
                     "Pragma" => "no-cache",
                     "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                     "Expires" => "0"
             );
             $columns = $volRange;
              $callback = function() use($vols, $columns) {
                 $file = fopen('php://output', 'w');
                   fputcsv($file, $columns);
                   fputcsv($file, $vols);
                 fclose($file);
                }; 
            return Response::stream($callback, 200, $headers);
    }
   
    // ===========================================
    public function getGrpMemberOpprsData($grpMembersIds){
        $top5OpprChart = [];
        $opprTotalHours = 0;
        $trackings = Tracking::confirmedNotDel()
            ->where('oppor_id', '<>' ,0)
            ->groupBy('oppor_id')
            ->groupBy('logged_date')
            ->whereIn('volunteer_id' ,$grpMembersIds)
            ->selectRaw('oppor_id ,logged_date, sum(logged_mins)/60 as opp_logged_hours')
            ->get();
        $Oppr_ids = $trackings->pluck('oppor_id')->toArray();
        $Opportunities = Opportunity::whereIn('id' ,$Oppr_ids)->get();
        $allOpportunities = [];
        foreach ($Opportunities as $Oppor){
            $hours = $Oppor->getLoggedHoursSumByMembers($grpMembersIds);
            $top5OpprChart[$Oppor->title] = number_format($hours,1);
            $opprTotalHours = $opprTotalHours + $hours;
            $oppr['name'] = $Oppor->title;
            $oppr['hours'] = $hours;
            $oppr['volonteers'] = $Oppor->opportunity_member ? $Oppor->opportunity_member->whereIn('user_id' ,$grpMembersIds)->groupby('user_id')->count() : 0;

            $allOpportunities[] = $oppr;
        }
        $headings = [];
        foreach ($trackings as $tracking){
            if($tracking->category_names){
                foreach (explode(',' ,$tracking->category_names) as $cat){
                    $cat = trim( $cat );
                    if (array_key_exists($cat ,$headings)){                        
                        if (isset($headings[$cat]))
                            $headings[$cat] = (float)$headings[$cat] + (float)$tracking->opp_logged_hours;
                        else
                            $headings[$cat] = (float)$tracking->opp_logged_hours;
                    }
                    else
                        $headings[$cat] = (float)$tracking->opp_logged_hours;
                }
            }
        }
        $val_hrs_type_chart =[];
        foreach ($headings as $ind => $heading){ 
            $temp['yAxis'] = $ind;
            $temp['xAxis'] = $heading;
            $val_hrs_type_chart[] = $temp;
        }      
        return [$val_hrs_type_chart];
    }


    public function getGrpMemberOpprVolonteers($grpMembersIds){
        $data = [];
        $pieChartZip = [];
        $pieChartAge = [];
        $top5Chart = [];
        $pChartAge['13 – 17'] = $pChartAge['18 – 22'] =$pChartAge['23 – 30'] = $pChartAge['30 and older'] = 0;
        $totalHours = 0;
        $all_volunteersIds = [];
        $query = User::whereIn('id' ,$grpMembersIds);
        $grpMembers = (clone $query)->get();
        foreach ($grpMembers as $index => $user) {
            $object = [];
            $object['name'] = $user->getFullNameVolunteer();
            $object['hours'] = $user->getLoggedHoursSum();
            $object['total_oppr'] = $user->opportunity->count();
            $top5Chart[$user->id.$object['name']] = number_format($object['hours'],1);
            $data[] = $object;
            $totalHours = $totalHours + $object['hours'];
        }
        $all_volunteers_zip = (clone $query)
            ->groupBy('zipcode')
            ->selectRaw('zipcode, count(*) as total')
            ->orderBy('total' ,'asc')
            ->get();

        $all_volunteers_age = (clone $query)
            ->select( [
                \DB::raw("floor((DATEDIFF(CURRENT_DATE, STR_TO_DATE(birth_date, '%m/%d/%Y'))/365)) as age"),
                \DB::raw("count(*) as total")
            ])
            ->groupBy('age')
            ->orderBy('total' ,'asc')
            ->get();

        foreach ($all_volunteers_zip as $user){
            $pChartZip['name'] = "$user->zipcode";
            $pChartZip['y'] = (int)$user->total;
            $pChartZip['color'] = rand_color();
            $pieChartZip[] = $pChartZip;
        }

        foreach ($all_volunteers_age as $user){
            if($user->age >= 13 && $user->age <= 17 )
                $pChartAge['13 – 17'] +=  (int)$user->total;

            if($user->age >= 18 && $user->age <= 22 )
                $pChartAge['18 – 22'] +=  (int)$user->total;

            if($user->age >= 23 && $user->age <= 30 )
                $pChartAge['23 – 30'] +=  (int)$user->total;

            if($user->age >= 30  )
                $pChartAge['30 and older'] +=  (int)$user->total;
        }
        ksort($pChartAge);
        foreach ($pChartAge as $index => $value){
            $array['name'] = "$index";
            $array['y'] = (int)$value;
            $array['color'] = rand_color();
            $pieChartAge[] = $array; //age group pie
        }
        array_multisort(array_column($data, 'hours'), SORT_DESC, $data);
        arsort($top5Chart);
        $top5Chart = array_slice($top5Chart, 0, 5, true);
        $avg = count($data) > 0 ?  number_format($totalHours / count($data),1) : 0;        
        $data = array_slice($data, 0, 25, true); // datatable
        return [$data ,$pieChartZip ,$pieChartAge];
    }

    public function surveyOpportunity($id=null){
        if (Auth::check()) { 
        $data=Survey::where(array('opportunity_id'=>$id,'is_deleted'=>0))
        ->orderBy('id','desc')
        ->get();

        $arr=[];
        foreach($data as $key=>$details){
         $arr[$key]['id']= $details->id;
         $arr[$key]['organization_id']= $details->organization_id;
         $arr[$key]['opportunity_id']= $details->opportunity_id;
         $arr[$key]['survey_type']= $details->survey_type;
         $arr[$key]['status']= $details->status;
         $arr[$key]['created_at']= $details->created_at;
         $arr[$key]['survey_name']= $details->survey_name;
        $ans=SurveyResult::where('survey_id','=',$details->id)->groupBy('user_id')->get();
        $arr[$key]['answered']=count(SurveyResult::where('survey_id','=',$details->id)->groupBy('user_id')->get());
        $arr[$key]['skipped']=$details->status=="close" ? count(Opportunity_member::where(array('oppor_id'=>$details->opportunity_id,'org_id'=>$details->organization_id))->get())-$arr[$key]['answered']  : 0 ;
        }
      
        return view('organization.survey.survey_opportunity',['id'=>$id ,'data'=>$arr]);
        }else{
        return redirect()->to('/sign-in');
        }
    }

    public function addSurvey($id=null){
        if (Auth::check()) { 
           $org_id= Auth::user()->id;
           $dates= Opportunity::select('start_date','end_date')->where('id','=',$id)->first();
        return view('organization.survey.add_survey_questions',['id'=>$id,'org_id'=>$org_id,'dates'=>$dates]);
        }else{
            return redirect()->to('/sign-in');
        }
      } 

      public function resultSurvey($id){        
          if (Auth::check()) { 
            $ids= explode("/",decrypt($id));        
            $survey_id=$ids[0];
            $org_id=$ids[1];
            $oppor_id=$ids[2];
           
            $surName=Survey::where(array('id'=>$survey_id,'organization_id'=>$org_id,'opportunity_id'=>$oppor_id))->first();
            $totalsurvey=SurveyResult::where('survey_id','=',$survey_id)->distinct('user_id')->count('user_id');
             $resultArr = array();
             $survey= SurveyQuestions::where('survey_id','=',$survey_id)->get()->toArray();
            foreach($survey as $key=>$list){
                $total=count(SurveyResult::where(array('survey_id'=>$survey_id))->groupBy('user_id')->get());
                $resultArr[$key]['question']=$list['questions'];
                $resultArr[$key]['answered'] = count(SurveyResult::where(array('survey_id'=>$survey_id,'question_id'=>$list['id']))->groupBy('user_id','question_id')->get());
                $resultArr[$key]['skipped'] = $total - $resultArr[$key]['answered'];
                $options=SurveyOptions::where(array('survey_id'=>$survey_id,'question_id'=>$list['id']))->get()->toArray();
                $res=0;
              foreach($options as $k=>$option){                 
                $resultArr[$key]['Options'][$k]['answerchoice']=$option['options'];
                $resultArr[$key]['Options'][$k]['response'] = count(SurveyResult::where(array('survey_id'=>$survey_id,'option_id'=>$option['id']))->groupBy('user_id','question_id')->get());
                 if($resultArr[$key]['Options'][$k]['response']){
                    $resultArr[$key]['Options'][$k]['responsepercent'] = round(($resultArr[$key]['Options'][$k]['response']/$resultArr[$key]['answered'])*100 , 2);
                 }else{
                    $resultArr[$key]['Options'][$k]['responsepercent']=0;
                 }
                
                $res= $res+$resultArr[$key]['Options'][$k]['response'];
                             }
                 $resultArr[$key]['totalresponses']=$res;
               }
             
    return view('organization.survey.survey_result',['result'=>$resultArr,'surveyName'=>$surName]);          
           }else{
               return redirect()->to('/sign-in');
             }
      }

      public function saveSurvey(Request $request){
             $ques_id =[];
            $survey= Survey::insertGetId([
            "organization_id"=>$request->org_id,
            "opportunity_id"=>$request->oppor_id,
            "survey_name"=> $request->name,
            "survey_type"=>$request->survey_type,
            "status"=>"pending",
            "survey_start"=>$request->surveyStartDate,
            "survey_end"=>$request->surveyEndDate,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            ]);
        
            $ques = $request->ques_option;
            foreach($ques as $q){
            $ques_id[]= SurveyQuestions::insertGetId([
            "survey_id"=>$survey,
            "question_type"=>$q['ques_type'],
            "questions"=>$q['question'],
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            ]);
            }

            $ques_id_count=count($ques_id);
                    foreach($ques as $k=>$Q){      
                        if($ques_id_count>0){
                                foreach($Q as $key=>$i){                            
                                    if($key=="question"||$key=="ques_type"){
                                        continue;
                                    }
                                    SurveyOptions::insert([
                                    "survey_id"=>$survey,
                                    "question_id"=>$ques_id[$k],
                                    "options"=>$i,
                                    'created_at' => \Carbon\Carbon::now(),
                                    'updated_at' => \Carbon\Carbon::now(),
                                    ]);
                                }
                                $ques_id_count--;  
                            } 
                        }
      return Response::json(['result' => "success" ,'oppor_id'=>$request->oppor_id]);

        }

    public function updateSurvey(Request $request){
        if (Auth::check()) { 
          $survey_id=$request->survey_id;
          $ques_id=$request->ques_id;
          $question=$request->question;
          $options=$request->quesAndoption;
          $quesType=$request->ques_type;
          $surveyName= $request->surveyName;
          $surveyStartDate=$request->surveyStartDate;
          $surveyEndDate=$request->surveyEndDate;
          $surveyType=$request->surveyType;
          
          Survey::where('id','=',$survey_id)->update(array('survey_name'=>$surveyName,'survey_type'=>$surveyType,'survey_start'=>$surveyStartDate,'survey_end'=>$surveyEndDate));
          SurveyQuestions::where(array('survey_id'=>$survey_id,'id'=>$ques_id))->update(array('questions'=>$question,'question_type'=>$quesType));
          SurveyOptions::where(array('survey_id'=>$survey_id,'question_id'=>$ques_id))->delete();
         
          foreach($options[0] as $key=>$value){       
           SurveyOptions::insert([
            "survey_id"=>$survey_id,
            "question_id"=>$ques_id,
            "options"=>$value,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
            ]);
          }
        return Response::json(['result' => "success"]);
        }else{
        return redirect()->to('/sign-in');
        }  
    }

    public function addSurveyQues(Request $request){
        if (Auth::check()) { 
            $survey_id=$request->survey_id;
            $question=$request->question;
            $options=$request->option;
            $quesType=$request->ques_type;
            $surveyName = $request->surName;
            $surveyStartDate=$request->surveyStartDate;
            $surveyEndDate=$request->surveyEndDate;
            $surveyType=$request->surveyType;

            Survey::where('id','=',$survey_id)->update(array('survey_name'=>$surveyName,'survey_type'=>$surveyType,'survey_start'=>$surveyStartDate,'survey_end'=>$surveyEndDate));
               $quesID= SurveyQuestions::insertGetId([
                "survey_id"=>$survey_id,
                "question_type"=> $quesType,
                "questions"=>$question,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                ]);

             for($i=1;$i<=count($options[0]);$i++){
                  $ques_id[]= SurveyOptions::insert([
                    "survey_id"=>$survey_id,
                    "question_id"=>$quesID,
                    "options"=>$options[0]['option_'.$i],
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                    ]);
              }
            return Response::json(['result' => "success"]);
        }else{
            return redirect()->to('/sign-in');
            }  
      }
    public function previewSurvey($id){
        if (Auth::check()) { 
            $surName=Survey::where('id','=',$id)->get();
            $option= SurveyQuestions::where('survey_id','=',$id)->with('quesOption')->get();         
            return view('organization.survey.survey_preview',['name'=>json_decode($surName),'options'=>json_decode($option)]);
        }else{
        return redirect()->to('/sign-in');
       }  
    }

    public function editSurvey($id,$opporId){
        if (Auth::check()) { 
            $surName=Survey::where('id','=',$id)->first();
            $option= SurveyQuestions::where('survey_id','=',$id)->with('quesOption')->get(); 
            $org_id= Auth::user()->id;
            $dates= Opportunity::select('start_date','end_date')->where('id','=',$opporId)->first();        
            return view('organization.survey.survey_edit',['name'=>json_decode($surName),'options'=>json_decode($option),'dates'=>$dates]);
            }else{
            return redirect()->to('/sign-in');
        }  
    }

    public function publishSurvey($id){
        if (Auth::check()) { 
            Survey::where('id','=',$id)->update(['status'=>'published']) ;
            return Redirect::back();
        }else{
            return redirect()->to('/sign-in');
        }    
    }

    public function sendEmailSurvey(Request $request){
        if (Auth::check()) { 
            $dataIds =explode("/", decrypt($request->code));
            $survey_id = $dataIds[0];
            $org_id= $dataIds[1];
            $oppor_id = $dataIds[2];
            $vol_ids=$request->vol_id;
            $volDetails= DB::table('opportunity_members')
             ->where('opportunity_members.org_id','=',$org_id)
             ->where('oppor_id','=',$oppor_id)
             ->whereIn('user_id',$vol_ids)
             ->join('users','opportunity_members.user_id','=','users.id')
             ->join('opportunities','opportunity_members.oppor_id','=','opportunities.id')
             ->select('users.id','opportunity_members.oppor_id','users.email','users.first_name','users.last_name','opportunities.title')
             ->get();

            foreach($volDetails as $list){   
                $orgName= User::where('id','=',$org_id)->get()->toArray();
                $fromName=$orgName[0]['org_name'];
                $surveyName =Survey::where(array('id'=>$survey_id,'organization_id'=>$orgName[0]['id']))->pluck('survey_name')->toArray();
                $subject=$surveyName[0];            
                $path = url('/survey-page').'/'.$list->id.'/'.$request->code;
                $body = '<p>Hi '.$list->first_name.' '.$list->last_name.'</p>
                <p>Thanks for joining the opportunity <b>'.$list->title.'</b></p>
                <p>We value your feedback and we would love to hear that what you have experienced about <b>'.$list->title.'.</b></p>
                <p>Please take less than 5 minutes to complete this survey <a href="' . $path .'">Visit Survey Page</a> and tell us what you loved or what you like to add.</p>
                <p>Thank you for your time & thoughts-we truly appreciate it.</p>';
                 \Mail::to($list->email)->send(new SharingProfileMail($body, $subject, $orgName[0]['email'], $fromName));
        
            }
            return Response::json(['success'=>"Mail successfully send"]);

        }else{
            return redirect()->to('/sign-in');
        }     
    }

    public function mailSurveyPage($id, $data){
            $ids=explode("/",decrypt($data));     
            $surName=Survey::where('id','=',$ids[0])->get();
            $option= SurveyQuestions::where('survey_id','=',$ids[0])->with('quesOption')->get();
            $check=SurveyResult::where(array('survey_id'=>$ids[0],'user_id'=>$id))->count();    
            if($check>0){
                $surveyFilled= "exist";
            }else{
                $surveyFilled= "not exist";
            }
            return view('organization.survey.survey_page',['name'=>json_decode($surName),'options'=>json_decode($option),'exist'=>$surveyFilled]);
    }

    public function saveAnswerSurvey(Request $request){
        $details=$request->all();
        $user_id=$request->vol_id;
        $survey_id=$request->survey_id;
        $questions= $request->questions;
      
        foreach($questions as $ques){
            foreach($details as $key=>$det){
              if($key=="ques_".$ques){
                        if(is_array($det)){
                            foreach($det as $d){
                            SurveyResult::create([
                            "survey_id"=>$request->survey_id,
                            "user_id"=>$request->vol_id,
                            "question_id"=>$ques,
                            "option_id"=>$d
                            ]);
                            }
                        }else{
                            SurveyResult::create([
                                "survey_id"=>$request->survey_id,
                                "user_id"=>$request->vol_id,
                                "question_id"=>$ques,
                                "option_id"=>$det
                            ]);
                        }
                 }
             }
            
        }
      $volName=  User::where('id','=',$user_id)->get()->toArray(); 
      $survey=Survey::where('id','=',$survey_id)->get()->toArray();
      $orgDetail= User::where('id','=',$survey[0]['organization_id'])->get()->toArray();
      $fromName=$orgDetail[0]['org_name'];
      $surveyName =Survey::where(array('id'=>$survey_id,'organization_id'=>$survey[0]['organization_id']))->pluck('survey_name')->toArray();
      $subject=$surveyName[0];   
      $body = '<div class="container text-center">
                <p><h3>Hi '.$volName[0]['first_name'].' '.$volName[0]['last_name'].'</h3></p>
                <p><h4>Thanks for participating in the survey and we will improve the things based on your feedback. Your response will keep confidential and will not be used for any other purpose.<h4></p>
                <p><h4>Thank You!</h4></p>
                </div>';
       \Mail::to($volName[0]['email'])->send(new SharingProfileMail($body, $subject, $orgDetail[0]['email'], $fromName));
      return view('organization.survey.survey_thankyou',['name'=>$volName]);
        
    }
    public function getVolunteer($ids){
      if (Auth::check()) { 
            $dataIds =explode("/", decrypt($ids));     //ids (survey_id/organization id /opportunity id)
            $survey_id = $dataIds[0];
            $org_id= $dataIds[1];
            $oppor_id = $dataIds[2];
            $volDetails= DB::table('opportunity_members')
             ->where('opportunity_members.org_id','=',$org_id)
             ->where('oppor_id','=',$oppor_id)
             ->join('users','opportunity_members.user_id','=','users.id')
             ->join('opportunities','opportunity_members.oppor_id','=','opportunities.id')
             ->select('users.id','users.email','users.first_name','users.last_name')
             ->get();
             return Response::json(['details'=>$volDetails]);
        }else{
            return redirect()->to('/sign-in');
        }     
    }

    public function deleteSurvey(Request $request){ 
        Survey::where('id','=', $request->survey_id)->update(array('is_deleted'=>1));
        return Response::json(['message'=>'success']);
    }

    
    public function sendSurveyMail(){
     $date = date("Y-m-d");
     $surveys= DB::table('survey')
     ->where('survey.status','=','published')
     ->where('survey.survey_start','<=', $date)
     ->where('survey.survey_end','>=', $date)
    ->join('opportunity_members','opportunity_members.oppor_id','=','survey.opportunity_id')
    ->join('users','opportunity_members.user_id','=','users.id')
    ->join('opportunities','opportunity_members.oppor_id','=','opportunities.id')
    ->select('opportunity_members.user_id','survey.id','survey.opportunity_id','users.email','users.first_name','users.last_name','opportunities.title','survey.status','survey.survey_end','survey.survey_start')
    ->get();

    foreach($surveys as $survey){ 
       $orgId=Survey::where('id','=',$survey->id)->pluck('organization_id')->first();
       $orgName=User::where('id','=',$orgId)->pluck('org_name')->first();
         $code=encrypt($survey->id."/".$survey->user_id."/".$survey->opportunity_id); 
         $fromName= $orgName;  
         $subject = $survey->title;         
         $path = url('/survey-page').'/'.$survey->user_id.'/'.$code;
         $body = '<p>Hi '.$survey->first_name.' '.$survey->last_name.'</p>
                <p>Thanks for joining the opportunity <b>'.$survey->title.'</b></p>
                <p>We value your feedback and we would love to hear that what you have experienced about <b>'.$survey->title.'.</b></p>
                <p>Please take less than 5 minutes to complete this survey <a href="' . $path .'">Visit Survey Page</a> and tell us what you loved or what you like to add.</p>
                <p>Thank you for your time & thoughts-we truly appreciate it.</p>';
       
                //send survey mail or not
         $count=SurveyEmail::where(array('survey_id'=>$survey->id,'voluntier_id'=>$survey->user_id))->count();
        if($count==0){  
            //save email record 
           SurveyEmail::insert([
            "survey_id"=>$survey->id,
            "opportunity_id"=>$survey->opportunity_id,
            "voluntier_id"=>$survey->user_id,
            "email_body"=>$body,
        ]);            
         \Mail::to($survey->email)->send(new SharingProfileMail($body, $subject ,     $survey->email, $fromName));
          }
        }
    }
   
    public function closeSurvey(){
        $date = date("Y-m-d");
        $surveys= Survey::where(array('survey_end'=>$date,'status'=>'published'))
        ->update(array('status'=>'closed'));
    }



}
