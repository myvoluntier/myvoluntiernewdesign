<?php

namespace App\Http\Controllers\Organization;

use App\Activity;
use App\Alert;
use App\Services\ChatManager;
use App\Opportunity;
use App\Review;
use App\Chat;
use App\Services\ChatService;
use App\Tracking;
use App\User;
use App\NewsfeedModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Mail\UsersDefaultMail;
use App\Models\EmailsTemplate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class TrackingCtrl extends Controller {

    public function viewTrackingPage(Request $request) {
        $org_id = Auth::user()->id;
        $pendingHours = Tracking::where('org_id', $org_id)->where('approv_status', 0)->where('is_deleted', '<>', 1)->orderBy('updated_at', 'desc')->get();
        $trackedHours = Tracking::where('org_id', $org_id)->where('approv_status', '<>', 0)->where('is_deleted', '<>', 1)->orderBy('updated_at', 'desc')->get();
        $pendingHoursPaginate = $this->paginate($this->getTrackList($pendingHours));
        $trackHoursPaginate = $this->paginate($this->getTrackList($trackedHours));
        $pendingHoursPaginate->withPath(url()->current());
        $trackHoursPaginate->withPath(url()->current());
        return view('organization.track', ['tracks' => $pendingHoursPaginate, 'trackedHours' => $trackHoursPaginate,'page_name' => 'org_track']);
    }

    public function viewTrack($id = null) {
        $org_id = Auth::user()->id;
        $tracks = Tracking::where('id', $id)->where('is_deleted', '<>', 1)->orderBy('updated_at', 'desc')->get();
        $trackData = $this->getTrackList($tracks);
        // print_r($trackData);exit;
        return Response::json($trackData);
    }

    private function getTrackList($tracks) {
        $confirm_tracks = array();
        foreach ($tracks as $t) {
            $volunteer = User::find($t->volunteer_id);
            $opportunity = Opportunity::find($t->oppor_id);
            $confirm_tracks[$t->id]['track_id'] = $t->id;
            $confirm_tracks[$t->id]['volunteer_id'] = $t->volunteer_id;
            $confirm_tracks[$t->id]['volunteer_name'] = $volunteer->first_name . ' ' . $volunteer->last_name;
            $confirm_tracks[$t->id]['volunteer_logo'] = $volunteer->logo_img;
            $confirm_tracks[$t->id]['volunteer_email'] = $volunteer->email;
            $confirm_tracks[$t->id]['volunteer_phone'] = $volunteer->contact_number;
            $confirm_tracks[$t->id]['volunteer_id'] = $t->volunteer_id;
            $confirm_tracks[$t->id]['opportunity_id'] = $t->oppor_id;
            $confirm_tracks[$t->id]['opportunity_name'] = $t->oppor_name;
            $confirm_tracks[$t->id]['opportunity_status'] = $opportunity->category_id;
            $confirm_tracks[$t->id]['opportunity_logo'] = $opportunity->logo_img;
            $confirm_tracks[$t->id]['logged_date'] = date('m-d-Y', strtotime($t->logged_date));
            $confirm_tracks[$t->id]['started_time'] = $t->started_time;
            $confirm_tracks[$t->id]['ended_time'] = $t->ended_time;
            $confirm_tracks[$t->id]['logged_mins'] = $t->logged_mins;
            $confirm_tracks[$t->id]['updated_at'] = $t->updated_at;
            $confirm_tracks[$t->id]['comment'] = $t->description;
            $confirm_tracks[$t->id]['selected_blocks'] = $t->selectd_timeblocks;
            $confirm_tracks[$t->id]['logged_date_day'] = dateMDY($t->logged_date) . ' (' . date('l', strtotime($t->logged_date)) . ')';

            $confirm_tracks[$t->id]['is_designated'] = $t->is_designated;
            $confirm_tracks[$t->id]['designated_group_id'] = $t->designated_group_id;
            $confirm_tracks[$t->id]['start'] = $t->logged_date . ' ' . $t->started_time;
            $confirm_tracks[$t->id]['end'] = $t->logged_date . ' ' . $t->ended_time;
        }
        return $confirm_tracks;
    }

    public function approveTrack(Request $request) {

        $org_id = Auth::user()->id;
        $track_id = $request->input('track_id');
        $track = Tracking::find($track_id);

        $this->approveTracksStatusUpdate($track ,$org_id);

        if ($request->input('is_review') == 1) {
            $review = Review::where('review_from', $org_id)->where('review_to', $track->volunteer_id)->where('is_deleted', '<>', 1)->first();
            if ($review == null) {
                $review = new Review;
                $review->review_from = $org_id;
                $review->review_to = $track->volunteer_id;
                $review->mark = $request->input('review_score');
                $review->comment = $request->input('review_comment');
                $review->save();
            } else {
                $review->review_from = $org_id;
                $review->review_to = $track->volunteer_id;
                $review->mark = $request->input('review_score');
                $review->comment = $request->input('review_comment');
                $review->save();
            }
        }

        Return Response::json(['result' => 'success']);
    }

    public function approveTrackingDetailOrg(){

        try{

            $opportunity = Opportunity::find(\request()->oppr_id);

            $track = $opportunity->tracks()->where('approv_status' ,Tracking::STATUS_PENDING)->first();

            if(!$track){
                return null;
            }

            $org_id = Auth::user()->id;
            $this->approveTracksStatusUpdate($track ,$org_id);

            $user = $track->user;

            $organization = $track->organization;

            $data = [
                "id" => $track->id,
                "name" => $user->first_name .' '.$user->first_nam,
                "image" => $user->logo_img , #'https://via.placeholder.com/100'
                "org_name" => $organization->org_name,
                "opportunity_name" => $track->oppor_name,
                "date" => Carbon::parse($track->logged_date)->format('F d, Y'),
                "time_range" =>  date("g:iA", strtotime($track->started_time)) .' - '.date("g:iA", strtotime($track->ended_time)),
                "hours" => $track->logged_mins/60 .' Hours',
            ];

            return $data;
        }

        catch (\Exception $ex){
            Log::error('approveTrackingDetailOrg error OPPR ID tracking approve by QR CODE'.request()->oppr_id.' user Id Logged In' .auth()->id());
            return null;
        }

    }


    public function approveTracksStatusUpdate($track ,$org_id)
    {

        $track_id = $track->id;
        $track->approv_status = Tracking::STATUS_APPROVE;
        $track->confirm_code = null;
        $track->confirmed_at = date("Y-m-d H:i:s");
        $update = $track->save();

        $alert = Alert::where('related_id', $track_id)->where('receiver_id', $org_id)->where('alert_type', 4)->first();

        if ($alert) {
            $alert->is_apporved = 1;
            $alert->save();
        }

        $vol = User::find($track->volunteer_id);
        $opportunity = Opportunity::find($track->oppor_id);
        $opp_chat = Chat::where('group_id', $track->oppor_id)->first();
        $manager = new ChatManager();

        if ($opp_chat == null) {
            $chat_service = new ChatService();
            $photo = $opportunity->logo_img === null ? asset('img/org/001.png') : asset('uploads/' . $opportunity->logo_img);
            $chatId = $chat_service->createChat($opportunity->title, $photo);
            $chat_service->addUserToChat(Auth::user(), $chatId, $opportunity->title, 'organizations');
            $chat = new Chat();
            $chat->chat_id = $chatId;
            $chat->user_id = Auth::user()->id;
            $chat->group_id = $opportunity->id;
            $chat->type = 'organizations';
            $chat->save();
        }

        $manager->joinOrganization($vol, $opportunity);

        if ($update) {
            $newsfeed = new NewsfeedModel;
            $newsfeed->who_joined = $org_id;
            $newsfeed->related_id = $track->volunteer_id;
            $newsfeed->table_name = 'tracked_hours';
            $newsfeed->table_id = $track_id;
            $newsfeed->reason = 'New activity';
            $newsfeed->created_at = date('Y-m-d H:i:s');
            $newsfeed->updated_at = date('Y-m-d H:i:s');
            $newsfeed->save();
        }
        return $track;
        
    }

    public function declineTrack(Request $request) {
        $org_id = Auth::user()->id;
        $track_id = $request->input('track_id');
        $track = Tracking::find($track_id);
        $track->approv_status = Tracking::STATUS_DECLINE;
        $track->confirm_code = null;
        $track->save();

        $alerts = Alert::where('related_id', $track_id)->where('receiver_id', $org_id)->where('alert_type', 4)->get();
        if ($alerts != null) {
            $alert = $alerts->first();
            $alert->is_apporved = 2;
            $alert->save();
        }
        Return Response::json(['result' => 'success']);
    }

    public function confirmCustomTrack($track_id, $confirm_code) {
        $org_id = Auth::user()->id;
        $track = Tracking::find($track_id);

        if ($track->confirm_code == $confirm_code) {
            $new_opp = new Opportunity;
            $new_opp->org_id = $org_id;
            $new_opp->org_type = User::find($org_id)->org_type;
            $new_opp->title = $track->oppor_name;
            $new_opp->type = 2;
            $new_opp->save();

            $track->org_id = $org_id;
            $track->oppor_id = $new_opp->id;
            $track->save();

            $confirm_tracks = array();
            $volunteer = User::find($track->volunteer_id);
            $opportunity = Opportunity::find($track->oppor_id);
            $confirm_tracks['track_id'] = $track->id;
            $confirm_tracks['volunteer_id'] = $track->volunteer_id;
            $confirm_tracks['volunteer_name'] = $volunteer->first_name . ' ' . $volunteer->last_name;
            $confirm_tracks['volunteer_logo'] = $volunteer->logo_img;
            $confirm_tracks['opportunity_id'] = $track->oppor_id;
            $confirm_tracks['opportunity_name'] = $track->oppor_name;
            $confirm_tracks['opportunity_status'] = $opportunity->category_id;
            $confirm_tracks['opportunity_logo'] = $opportunity->logo_img;
            $confirm_tracks['logged_date'] = $track->logged_date;
            $confirm_tracks['started_time'] = $track->started_time;
            $confirm_tracks['ended_time'] = $track->ended_time;
            $confirm_tracks['logged_mins'] = $track->logged_mins;
            $confirm_tracks['updated_at'] = $track->updated_at;
            $confirm_tracks['comment'] = $track->description;
            return view('organization.confirmCustomTrack', ['track' => $confirm_tracks, 'page_name' => '']);
        } else {
            return redirect()->to('/');
        }
    }

    public function approveGroupRequest($group_id = '', $status = '') {
        $Group_member = Group_member::where('id', $group_id)->get();
        if (count($Group_member) > 0) {
            Group_member::where('id', $group_id)->update(['status' => $status]);
            $is_approved = 2;
            if ($status == 2) {
                $chatManager = new ChatManager();
                $chatManager->joinToGroup($Group_member[0]->group_id);
                $is_approved = 1;
            }
            Alert::where('related_id', $group_id)->update(['related_id' => 0, 'is_apporved' => $is_approved]);
        }

        return redirect()->to('/viewAlert');
    }

    /*
     * Method to validated days allow by opportunity
     */

    public function validatedTrackDate(Request $request) {

        $addDate = dateYmdFormat(trim($request->input('addDate'))); # Carbon::parse()->format('Y-d-m');
        $oppId = trim($request->input('oppId'));

        $oppObj = Opportunity::find($oppId);

        if (!empty($oppObj)) {

            $addDateTime = new \DateTime($addDate);
            $startDateTime = new \DateTime($oppObj->start_date);
            $endDateTime = new \DateTime($oppObj->end_date);
            if ($addDateTime >= $startDateTime && $addDateTime <= $endDateTime) {
                $weekDays = explode(',', $oppObj->weekdays);
                $dayName = date('l', strtotime($addDate));

                if (in_array($dayName, $weekDays)) {
                    echo "success";
                } else {
                    echo "Selected opportunity does not allow for selected day. Allow Day(s) " . implode(', ', $weekDays);
                }
            } else {
                echo "Selected opportunity does't allow for selected date.<br>Start Date: " . dateMDY($oppObj->start_date) . " End Date: " . dateMDY($oppObj->end_date);
            }
        } else {
            echo "Invalid selected opportunity.";
        }
        exit;
    }

    /*
     * Method to get validate track hours
     */

    public function validatedTrackHours(Request $request) {

        $addDate = dateYmdFormat(trim($request->input('addDate'))); # Carbon::parse()->format('Y-d-m');

        $oppId = trim($request->input('oppId'));
        $trackId = trim($request->input('track_id'));
        $isEdit = trim($request->input('is_edit'));
        $timeBlock = explode(',', $request->input('time_block'));
        $loggeDate = date('Y-m-d', strtotime($addDate));
        $oppObj = Opportunity::find($oppId);

        if (!empty($oppObj)) {
            if (strtotime(current($timeBlock)) >= strtotime($oppObj->start_at) && strtotime(end($timeBlock)) < strtotime($oppObj->end_at)) {
                $trackHoursObj = Tracking::select('id', 'selectd_timeblocks')->where('oppor_id', $oppId)
                                ->where('logged_date', $loggeDate)->where('is_deleted', '0')->where('volunteer_id', Auth::user()->id);
                if ($isEdit) {
                    $trackHoursObj = $trackHoursObj->where('id', '<>', $trackId);
                }
                $trackHoursObj = $trackHoursObj->orderBy('id', 'DESC')->get();

                if (!empty($trackHoursObj)) {
                    foreach ($trackHoursObj as $key => $val) {
                        $existTrackHours = explode(',', $val->selectd_timeblocks);
                        if (count(array_intersect($timeBlock, $existTrackHours)) > 0) {
                            echo "Selected time is overlapped.";
                            exit;
                        }
                    }

                    echo "success";
                } else {
                    echo "Invalid selected time.";
                }
            } else {
                echo "Invalid time for selected opportunity. It is start " . $oppObj->start_at . ' and end ' . $oppObj->end_at;
            }
        } else {
            echo "Invalid selected time.";
        }
        exit;
    }

    /*
     * Method to get validate max concurrent volunteer
     */

    public function validatedMaxVolunteer(Request $request) {

        $addDate = dateYmdFormat(trim($request->input('addDate'))); # Carbon::parse()->format('Y-d-m');

        $oppId = trim($request->input('oppId'));
        $trackId = trim($request->input('track_id'));
        $isEdit = trim($request->input('is_edit'));
        $timeBlock = explode(',', $request->input('time_block'));
        $loggeDate = date('Y-m-d', strtotime($addDate));
        $oppObj = Opportunity::find($oppId);

        if (!empty($oppObj)) {
            if (!isset($oppObj->max_concurrent_vol) || empty($oppObj->max_concurrent_vol)) {
                echo "success";
                exit;
            }
            if (strtotime(current($timeBlock)) >= strtotime($oppObj->start_at) && strtotime(end($timeBlock)) < strtotime($oppObj->end_at)) {
                $trackHoursObj = Tracking::select('id', 'selectd_timeblocks')->where('oppor_id', $oppId)
                                ->where('logged_date', $loggeDate)->where('is_deleted', '0')->where('volunteer_id', '<>', Auth::user()->id);

                if ($isEdit) {
                    $trackHoursObj = $trackHoursObj->where('id', '<>', $trackId);
                }
                $trackHoursObj = $trackHoursObj->orderBy('id', 'DESC')->get();
                if (!empty($trackHoursObj)) {
                    $cntVolunteer = 0;
                    foreach ($trackHoursObj as $key => $val) {
                        $existTrackHours = explode(',', $val->selectd_timeblocks);

                        if (count(array_intersect($timeBlock, $existTrackHours)) > 0) {
                            $cntVolunteer++;
                        }

                        if ($cntVolunteer >= $oppObj->max_concurrent_vol) {
                            echo "Selected duration is over max concurrent volunteer limit (" . $oppObj->max_concurrent_vol . ")";
                            exit;
                        }
                    }

                    echo "success";
                } else {
                    echo "Invalid selected time.";
                }
            } else {
                echo "Invalid time for selected opportunity. It is start " . $oppObj->start_at . ' and end ' . $oppObj->end_at;
            }
        } else {
            echo "Invalid selected time.";
        }
        exit;
    }

    public function addHours(Request $request) {

        $opp_id = $request->input('opp_id');
        $opportunity = Opportunity::find($opp_id);
        $is_designated = $request->is_designated == '1' ? 1 : 0;
        $designated_group_id = $is_designated ? $request->designated_group_id : null;

        $opp_name = $request->input('opp_name');
        $org_id = Auth::user()->id;
        $volunteer_id = $request->input('volunteer_id');
        $start_time = $request->input('start_time');
        $end_time = $request->input('end_time');
        $logged_mins = $request->input('logged_mins');

        $logged_date = dateYmdFormat($request->input('selected_date'));

        $comments = $request->input('comments');
        $is_edit = $request->input('is_edit');
        $tracking_id = $request->input('tracking_id');
        $is_no_org = $request->input('is_no_org');
        $org_email = $request->input('org_email');


        $private_opp_name = $request->input('private_opp_name');
        $unlist_org_name = $request->input('unlist_org_name');
        $unlist_org_email = $request->input('unlist_org_email');
        $end_date = date("Y-m-d", strtotime($request->input('selected_date')));

        $time_block = $request->input('time_block');

        $logged_date = strtotime($logged_date);
        $logged_date = date('Y-m-d', $logged_date);

        $tracks = Tracking::find($tracking_id);
        
        $tracks->oppor_id = $opp_id;
        $is_link = Opportunity::find($opp_id)->type;
        if ($is_link == 1) {
            $tracks->link = 1;
        }
        $tracks->oppor_name = $opp_name;
        $tracks->designated_group_id = $designated_group_id;
        $tracks->is_designated = $is_designated;
        $tracks->org_id = Opportunity::find($opp_id)->org_id;
        $tracks->started_time = $start_time;
        $tracks->ended_time = $end_time;
        $tracks->logged_mins = $logged_mins;
        $tracks->logged_date = $logged_date;
        $tracks->description = $comments;
        $tracks->selectd_timeblocks = $time_block;
        $tracks->save();

        $add_activity = new Activity;
        $add_activity->user_id = $org_id;
        $add_activity->oppor_id = $opp_id;
        if ($is_link == 1) {
            $add_activity->link = 1;
        }
        $add_activity->oppor_title = $opp_name;
        $add_activity->content = "You Updated Logged Hours to " . $logged_mins . " mins on Opportunity ";
        $add_activity->type = Activity::ACTIVITY_ADD_HOURS;
        $add_activity->save();
        
        $user = User::where('id', $volunteer_id)->first();
        
        $alert = new Alert;
        $alert->receiver_id  = $volunteer_id;
        $alert->sender_id = $org_id;
        $alert->sender_type = 'organization';
        $alert->alert_type = Alert::ALERT_UPDATE_TRACK_HOURS;
        $alert->contents = 'had been adjusted service hours for opportunity (<strong>'.$opp_name.'</strong>)';
        $alert->save();

        $this->sendConfirmEmailForUpdateHours($opportunity, $user, 1, 1, $opp_name, $logged_mins / 60, $logged_date);

        $opp_logo = Opportunity::find($opp_id)->logo_img;

        return Response::json(['result' => 'success', 'track_id' => $tracks->id, 'opp_logo' => $opp_logo, 'is_link_exist' => $is_link]);
    }
    
    public function sendConfirmEmailForUpdateHours($opportunity,$user, $is_edit, $opp_type, $opp_name, $added_hours, $logged_date){
       try{
           $type = 'organizationUpdateTrackHours';

           $data['opp_type'] = $opp_type =='1' ? 'Private Opportunity' : 'Public Opportunity';
           $data['logged_hours'] = $added_hours;
           $data['opp_name'] = $opp_name;
           $data['logged_date'] = $logged_date;

           $emailTemp = EmailsTemplate::where('type', $type)->first();

           if($user->track_hour_email=='0') //if enabled emails
           {
               $orgUser = Auth::user();
               $body = replaceBodyContents($emailTemp->body, $emailTemp->type, $orgUser, null, $opportunity, $data);
               \Mail::to($user->email)->send(new UsersDefaultMail($body, $emailTemp->title));
           }

       }
       Catch(\Exception $ex){

           Log::error($ex->getMessage().' error in sendConfirmEmailForAddedHours in line'.$ex->getLine());
       }
	}

     //export tracked hours data
    public function exportTrackedHours($id = null){
        $org_id = Auth::user()->id;
        $trackedHours = Tracking::where('org_id', $org_id)->where('approv_status', '<>', 0)->where('is_deleted', '<>', 1)->orderBy('updated_at', 'desc')->get();
       $resultData =  $this->getTrackList($trackedHours);
        if(!empty($resultData)){
           
            $getHeaders = $this->getHeaderData();
            $columns = $getHeaders['columns'];
            $headers = $getHeaders['headers'];
            $callback = function() use ($resultData, $columns)
            {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);
                foreach ($resultData as $tc) {
                    $columnsVal = array($tc['logged_date'], ($tc['logged_mins']/60), $tc['opportunity_name'], $tc['volunteer_name'], $tc['volunteer_email'], $tc['volunteer_phone']);
                    fputcsv($file, $columnsVal);
                    
                }
                fclose($file);
            };
            return Response::stream($callback, 200, $headers);
        }
    }
     //set headers and columns of CSV
    public function getHeaderData($fileName = 'Tracked Hours'){
        $fileName = str_replace(" ", '_', $fileName);
        $headers = array(
                "Content-type" => "text/csv",
                "Content-Disposition" => "attachment; filename=".$fileName.".csv",
                "Pragma" => "no-cache",
                "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                "Expires" => "0"
        );

        $columns = array('Date of Service','Hours Tracked','Opportunity Name', 'Volunteer', 'Volunteer Email Address', 'Volunteer Phone');
       
        return array('headers'=>$headers, 'columns'=>$columns);
    }

    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
