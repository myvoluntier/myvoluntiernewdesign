<?php

namespace App\Http\Controllers\Organization;


use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use App\User;
class PartnerCtrl extends Controller
{
    public function savePartner($id = null, Request $request) {
        $userId = (isset(Auth::user()->id)) ? Auth::user()->id : "";
        if(!empty($id)){
            $id = base64_decode($id);
            $partnerId = Partner::find($id)->toArray();
            $orgList = User::select('id','org_name')
                            ->where('id','<>',Auth::user()->id)
                            ->where('user_role', 'organization')
                            ->where('is_deleted', '<>', 1)
                            ->orderBy('id','DESC')
                            ->get()
                            ->toArray();
            return view('components.auth.modal_body_partner', [ 'orgList'=>$orgList,'partnerId' => $partnerId]);
            
        }
        if (!empty($userId)) {
            $partnerid = $request->input('partnerid');
            $selPartnerid = (!empty($request->input('selPartnerId')))? base64_decode($request->input('selPartnerId')): '';
            if (empty($selPartnerid)) {
                $getPartner = Partner::select('id')
                        ->where('org_id', $userId)
                        ->where('is_deleted','<>',1)
                        ->where('partner_org_id', $partnerid)
                        ->get()->toArray();
                if(!empty($getPartner)){
                    return Response::json(['result' => 'exist']);
                }
                $partnerObj = new Partner();
                
            } else {
                $partnerObj = Partner::find($selPartnerid);
                if(empty($partnerObj)){
                    return Response::json(['result' => 'invalid_id']);
                }
            }
            $partnerObj->org_id = $userId;
            $partnerObj->partner_org_id = $partnerid;
            $partnerObj->save();

            session()->flash('app_message', 'Data has been saved successfully.');
        }
        return Response::json(['result' => 'success']);
    }
    
    public function deletePartner($id = null) {
        $userId = (isset(Auth::user()->id)) ? Auth::user()->id : "";
        if($id){
            $id = base64_decode($id);
            $partnerObj = Partner::find($id);
            $partnerObj->is_deleted = '1';
            $partnerObj->save();
            session()->flash('app_message', 'Data has been deleted successfully.'); 
        }else {
            session()->flash('app_error', 'Something went wrong.');
        }

        return redirect()->to('/organization/profile#orgPartner');
        
    }
    public function getOrgList($selPartner = array()){
        $orgList = User::select('id','org_name')
                            ->where('id','<>',Auth::user()->id)
                            ->where('user_role', 'organization')
                            ->where('is_deleted', '<>', 1)
                            ->whereNotIn('id',$selPartner)
                            ->orderBy('id','DESC')
                            ->get()
                            ->toArray();
        
        return $orgList;
    }
    public function getPartners($id){
        $partners = Partner::select('partners.*','users.org_name')
                        ->join('users', 'partners.partner_org_id', '=', 'users.id')
                        ->where('partners.org_id', $id)
                        ->where('partners.is_deleted', '<>', 1)
                        ->get()->toArray();
        $selPartner = [];
        if(!empty($partners)){
            foreach ($partners as $partVal){
                $selPartner[] = $partVal['partner_org_id'];
            }
        }
        $result['partners'] = $partners;
        $result['selPartners'] = $selPartner;
        return $result;
    }
}
