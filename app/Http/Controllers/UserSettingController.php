<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserSettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function updateUserNotificationSettings(Request $request){

        try{
            $name = $request['name'];
            $status = $request['status'];

            $currentUser = auth()->user();
            if($name == 'all_emails_send' || $name == 'all_texts_send'){
                //when all setting trigger then have to disable all others
                foreach ($currentUser->allNotificationsSendColumns($name) as $value){
                    $currentUser->$value = $status;
                }
            }
            else
                $currentUser->$name = $status;

            if($currentUser->save())
                return response()->json(['success' => true]);
            else
                return response()->json(['success' => false]);
        }
        catch (\Exception $e){
            return response()->json(['success' => false]);
        }
    }
}
