<?php

namespace App\Http\Controllers;

use App\Alert;
use App\Friend;
use App\Group;
use App\Group_member;
use App\Http\Controllers\Volunteer\GroupCtrl;
use App\Mail\SharingProfileMail;
use App\Mail\UsersDefaultMail;
use App\Message;
use App\Models\EmailsTemplate;
use App\Opportunity_member;
use App\Share;
use App\Tracking;
use App\CommonModel;
use App\User;
use App\Opportunity;
use App\School_type;
use App\Organization_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use App\Opportunity_category;

class SharingCtrl extends Controller {

    public function sharedProfilePage($shared_id, $code) {
        $shared_info = Share::where('id', '=', $shared_id)->first();

        if ($shared_info === null) {
            return view('home', ['org_type_names' => Organization_type::all(), 'school_type' => School_type::all()]);
        } else {
            $id = $shared_info->shared_user_id;
            $today = date("Y-m-d");
            if ($shared_info->is_deleted == 0 && $shared_info->expired_at < $today && strcmp($shared_info->codes, $code) == 0) {
                $user = User::find($id);
                $today = date("Y-m-d");
                if ($user->user_role == 'volunteer') {
                    $user_id = $user->id;
                    $logged_hours = Tracking::where('volunteer_id', $user_id)->where('is_deleted', '<>', 1)->where('approv_status', 1)->sum('logged_mins');
                    $logged_hours = $logged_hours / 60;

                    $today = date('Y-m-d');
                    $my_opportunities = Opportunity_member::where('user_id', $user_id)->where('status', 1)->where('is_deleted', '<>', 1)->pluck('oppor_id')->toArray();
                    $opportunities = Opportunity::whereIn('id', $my_opportunities)->where('type', 1)->where('is_deleted', '<>', 1)->
                                    where('end_date', '>', $today)->get();

                    $groups = DB::table('groups')->join('group_members', 'groups.id', '=', 'group_members.group_id')->where('group_members.user_id', $user_id)->
                                    where('group_members.is_deleted', '<>', 1)->where('groups.is_deleted', '<>', 1)->select('groups.*', 'group_members.role_id')->get();

                    $friends = DB::table('users')->join('friends', 'users.id', '=', 'friends.friend_id')->where('friends.user_id', $user_id)->
                                    where('friends.is_deleted', '<>', 1)->where('users.is_deleted', '<>', 1)->where('users.confirm_code', 1)->select('users.*')->get();


                    $my_orgs = Tracking::where('volunteer_id', $user->id)->where('is_deleted', '<>', 1)->where('approv_status', '=', 1)->groupBy('org_id')->pluck('org_id')->toArray();
                    $org_info = User::whereIn('id', $my_orgs)->get();
                    $type_script = array();
                    $tracks = Tracking::where('volunteer_id', $user->id)->where('is_deleted', '<>', 1)->where('approv_status', '=', 1)->orderBy('logged_date', 'desc')->get()->groupBy('org_id')->toArray();

                    foreach ($org_info as $info) {
                        $type_script[$info->id]['org_name'] = $info->org_name;
                        $type_script[$info->id]['org_logo'] = $info->logo_img;
                        $type_script[$info->id]['contact_number'] = $info->contact_number;
                        $type_script[$info->id]['zipcode'] = $info->zipcode;
                        $type_script[$info->id]['address'] = $info->city . ', ' . $info->state . ' ' . $info->zipcode . ', ' . $info->country;
                        $type_script[$info->id]['email'] = $info->email;
                        $type_script[$info->id]['primary_contact'] = '';
                        $type_script[$info->id]['track_info'] = $tracks[$info->id];
                    }

                    $oppr_types = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();
                    $org_types = Organization_type::all();
                    $sa_oppr_type = array();
                    $sa_org_type = array();
                    foreach ($oppr_types as $ot) {
                        $sa_oppr_type[$ot->id]['name'] = $ot->name;
                    }
                    foreach ($org_types as $or) {
                        $sa_org_type[$or->id]['name'] = $or->organization_type;
                    }

                    return view('sharedprofile', ['user' => $user, 'opportunity' => $opportunities, 'group' => $groups, 'friend' => $friends, 'logged_hours' => $logged_hours, 'user_role' => 'volunteer', 'oppr_types' => $sa_oppr_type, 'org_types' => $sa_org_type, 'tracks' => $type_script]);
                } else {
                    $tracks_hours = Tracking::where('org_id', $id)->where('approv_status', 1)->where('is_deleted', '<>', 1)->sum('logged_mins');
                    $tracks_hours = $tracks_hours / 60;

                    $today = date("Y-m-d");
                    $active_oppr = Opportunity::where('org_id', $id)->where('type', 1)->where('is_deleted', '<>', '1')->where('end_date', '>=', $today)->orderBy('created_at', 'desc')->get();

                    $groups = DB::table('groups')->join('group_members', 'groups.id', '=', 'group_members.group_id')->where('group_members.user_id', $id)->
                                    where('group_members.is_deleted', '<>', 1)->where('groups.is_deleted', '<>', 1)->select('groups.*', 'group_members.role_id')->get();

                    $my_members = DB::table('opportunity_members')->where('opportunity_members.org_id', $id)->where('opportunity_members.is_deleted', '<>', 1)->
                                    join('users', 'opportunity_members.user_id', '=', 'users.id')->select('users.*')->get();
                    $members = array();

                    foreach ($my_members as $m) {
                        $members[$m->id] = $m;
                    }

                    return view('sharedprofile', ['user' => $user, 'tracks_hours' => $tracks_hours, 'active_oppr' => $active_oppr, 'group' => $groups, 'members' => $members, 'user_role' => 'organization']);
                }
            } else {
                return view('home', ['org_type_names' => Organization_type::all(), 'school_type' => School_type::all()]);
            }
        }
    }

    public function sharedTranscriptPage($shared_id,$checkval,$code) {
        $shared_info = Share::where('id', '=', $shared_id)->first();
        if ($shared_info === null) {
            return view('home', ['org_type_names' => Organization_type::all(), 'school_type' => School_type::all()]);
        } else {
            $id = $shared_info->shared_user_id;
            $today = date("Y-m-d");
            if ($shared_info->is_deleted == 0 && $shared_info->expired_at > $today && strcmp($shared_info->codes, $code) == 0) {
                $user = User::find($id);
                $today = date("Y-m-d");
                if ($user->user_role == 'volunteer') {
                    $user_id = $user->id;
                    $logged_hours = Tracking::where('volunteer_id', $user_id)->where('is_deleted', '<>', 1)->where('approv_status', 1)->sum('logged_mins');
                    $logged_hours = $logged_hours / 60;

                    $today = date('Y-m-d');
                    $my_opportunities = Opportunity_member::where('user_id', $user_id)->where('status', 1)->where('is_deleted', '<>', 1)->pluck('oppor_id')->toArray();
                    $opportunities = Opportunity::whereIn('id', $my_opportunities)->where('type', 1)->where('is_deleted', '<>', 1)->
                                    where('end_date', '>', $today)->get();

                    $groups = DB::table('groups')->join('group_members', 'groups.id', '=', 'group_members.group_id')->where('group_members.user_id', $user_id)->
                                    where('group_members.is_deleted', '<>', 1)->where('groups.is_deleted', '<>', 1)->select('groups.*', 'group_members.role_id')->get();

                    $friends = DB::table('users')->join('friends', 'users.id', '=', 'friends.friend_id')->where('friends.user_id', $user_id)->
                                    where('friends.is_deleted', '<>', 1)->where('users.is_deleted', '<>', 1)->where('users.confirm_code', 1)->select('users.*')->get();


                    $my_orgs = Tracking::where('volunteer_id', $user->id)->where('is_deleted', '<>', 1)->where('approv_status', '=', 1)->groupBy('org_id')->pluck('org_id')->toArray();
                    $org_info = User::whereIn('id', $my_orgs)->get();
                    $type_script = array();
                    $tracks = Tracking::where('volunteer_id', $user->id)->where('is_deleted', '<>', 1)->where('approv_status', '=', 1)->orderBy('logged_date', 'desc')->get()->groupBy('org_id')->toArray();

                    foreach ($org_info as $info) {
                        $type_script[$info->id]['org_name'] = $info->org_name;
                        $type_script[$info->id]['org_logo'] = $info->logo_img;
                        $type_script[$info->id]['contact_number'] = $info->contact_number;
                        $type_script[$info->id]['zipcode'] = $info->zipcode;
                        $type_script[$info->id]['address'] = $info->city . ', ' . $info->state . ' ' . $info->zipcode . ', ' . $info->country;
                        $type_script[$info->id]['email'] = $info->email;
                        $type_script[$info->id]['primary_contact'] = '';
                        $type_script[$info->id]['track_info'] = $tracks[$info->id];
                    }

                    $oppr_types = Opportunity_category::all();
                    $org_types = Organization_type::all();
                    $sa_oppr_type = array();
                    $sa_org_type = array();
                    foreach ($oppr_types as $ot) {
                        $sa_oppr_type[$ot->id]['name'] = $ot->name;
                    }
                    foreach ($org_types as $or) {
                        $sa_org_type[$or->id]['name'] = $or->organization_type;
                    }


                    return view('sharedtranscript', ['user' => $user, 'shared_info' => $shared_info, 'opportunity' => $opportunities, 'group' => $groups, 'friend' => $friends, 'logged_hours' => $logged_hours, 'user_role' => 'volunteer', 'oppr_types' => $sa_oppr_type, 'org_types' => $sa_org_type, 'tracks' => $type_script , 'checkval'=>$checkval]);
                } else {
                    $tracks_hours = Tracking::where('org_id', $id)->where('approv_status', 1)->where('is_deleted', '<>', 1)->sum('logged_mins');
                    $tracks_hours = $tracks_hours / 60;

                    $today = date("Y-m-d");
                    $active_oppr = Opportunity::where('org_id', $id)->where('type', 1)->where('is_deleted', '<>', '1')->where('end_date', '>=', $today)->orderBy('created_at', 'desc')->get();

                    $groups = DB::table('groups')->join('group_members', 'groups.id', '=', 'group_members.group_id')->where('group_members.user_id', $id)->
                                    where('group_members.is_deleted', '<>', 1)->where('groups.is_deleted', '<>', 1)->select('groups.*', 'group_members.role_id')->get();

                    $my_members = DB::table('opportunity_members')->where('opportunity_members.org_id', $id)->where('opportunity_members.is_deleted', '<>', 1)->
                                    join('users', 'opportunity_members.user_id', '=', 'users.id')->select('users.*')->get();
                    $members = array();
                    foreach ($my_members as $m) {
                        $members[$m->id] = $m;
                    }

                    return view('sharedtranscript', ['user' => $user, 'shared_info' => $shared_info, 'tracks_hours' => $tracks_hours, 'active_oppr' => $active_oppr, 'group' => $groups, 'members' => $members, 'user_role' => 'organization']);
                }
            } else {
                return view('home', ['org_type_names' => Organization_type::all(), 'school_type' => School_type::all()]);
            }
        }
    }

    public function shareProfile(Request $request) {
        $user = Auth::user();
        $emails = $request->input('emails');
        $comments = $request->input('comments');
        $code = $this->generateRandomString(20);
        $shr_email = explode(',', $emails);

        if ($user->user_role == 'volunteer') {
            for ($i = 0; $i < count($shr_email); $i++) {
                $email = $shr_email[$i];
                $shared_id = $this->saveSharedInfo($code, $user->id, $email);
                $this->sendShareProfileEmail(1, $user, $shared_id, $comments, $email, $code);
            }
        } else {
            for ($i = 0; $i < count($shr_email); $i++) {
                $email = $shr_email[$i];
                $shared_id = $this->saveSharedInfo($code, $user->id, $email);
                $this->sendShareProfileEmail(0, $user, $shared_id, $comments, $email, $code);
            }
        }
        return Response::json(['result' => 'success']);
    }

    public function sendShareProfileEmail($user_type, $user, $shared_id, $comments, $to, $code) {

        $type = $user_type == 1 ? 'shareProfileVolunteer' : 'shareProfileOrg';

        $fromName = $user_type == 1 ? $user->first_name . ' ' . $user->last_name : $user->org_name;

        $data['content'] = $comments;
        $data['shared_id'] = $shared_id;
        $data['code'] = $code;

        $email = EmailsTemplate::where('type', $type)->first();

        if ($user->friend_req_email == '0') { //if enabled emails
            $body = replaceBodyContents($email->body, $email->type, $user, null, null, $data);
            \Mail::to($to)->send(new SharingProfileMail($body, $fromName . ' ' . $email->title, $user->email, $fromName));
        }

        if ($user->friend_req_text == '0' && $user->contact_number) { //if texts enabled
            $messageBody = replaceTextBodyContents($email->text_body, $email->type, $user, null, null, $data);
            sendSMSByTwilio($user->contact_number, $messageBody);
        }
    }

    public function sendShareTranscriptEmail($user, $shared_id, $comments, $to, $code ,$showDataOrNot) {
        $fromName = $user->first_name . ' ' . $user->last_name;
        $data['content'] = $comments;
        $data['shared_id'] = $shared_id;
        $data['code'] = $code;
        $data['showDataOrNot']=$showDataOrNot;
        $email = EmailsTemplate::where('type', 'shareTranscript')->first();
        if ($user->friend_req_email == '0') { //if enabled emails
            $body = replaceBodyContents($email->body, $email->type, $user, null, null, $data);
            \Mail::to($to)->send(new SharingProfileMail($body, $fromName . ' ' . $email->title, $user->email, $fromName));
        }

        if ($user->friend_req_text == '0' && $user->contact_number) { //if texts enabled
            $messageBody = replaceTextBodyContents($email->text_body, $email->type, $user, null, null, $data);
            sendSMSByTwilio($user->contact_number, $messageBody);
        }
    }

    public function shareTranscript(Request $request) {
        $showDataOrNot = $request->checkedTranscript;
        $user = Auth::user();
        $comments = $request->input('comments');
        $code = $this->generateRandomString(20);
        $shr_emails = explode(',', $request->input('emails'));
        if ($user->user_role == 'volunteer') {
            foreach ($shr_emails as $email) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $shared_id = $this->saveSharedInfo($code, $user->id, $email);
                    $this->sendShareTranscriptEmail($user, $shared_id, $comments, $email, $code ,$showDataOrNot);
                }
            }
        }
        return Response::json(['result' => 'success']);
    }

    public function checkSharedProfile() {
        $today = date("Y-m-d");
        $expired_profile = Share::where('expired_at', '<=', $today)->where('is_deleted', '<>', '1')->update(['is_deleted' => '1']);
    }

    public function saveSharedInfo($code, $user_id, $email) {
        $currentDate = date("Y-m-d"); // current date
        $share = new Share();
        $share->codes = $code;
        $share->shared_user_id = $user_id;
        $share->email = $email;
        $share->expired_at = date("Y-m-d", strtotime("+1 week"));
        $share->save();
        return $share->id;
    }

    public function generateRandomString($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function sendRequest(Request $request) {
        $data['first_name'] = $request->input('first_name');
        $data['last_name'] = $request->input('last_name');
        $data['org_name'] = $request->input('org_name');
        $data['email'] = $request->input('email');
        $data['phone'] = $request->input('phone');
        $data['comment'] = $request->input('comment');

        $name = $data['first_name'] . ' ' . $data['last_name'];


        $emailTemp = EmailsTemplate::where('type', 'sendRequest')->first();

        $body = replaceBodyContents($emailTemp->body, $emailTemp->type, null, null, null, $data);
        //no need to check because its admin
        \Mail::to('support@myvoluntier.com')->send(new SharingProfileMail($body, $name . ' ' . $emailTemp->title, $data['email'], $name));

        return Response::json(['result' => 'success']);
    }

    public function getMessage(Request $request) {
        $result = Message::where('receiver_id', $request->input('user_id'))->where('is_read', 0)->get();
        return Response::json(['result' => $result]);
    }

    public function getAlert(Request $request) {
        $result = Alert::where('receiver_id', $request->input('user_id'))->where('is_checked', 0)->count();
        return Response::json(['result' => $result]);
    }

    public function viewAlertPage() {
        $user = Auth::user();
        $alerts = Alert::where('receiver_id', $user->id)->orderBy('created_at', 'desc')->paginate(8);

        foreach ($alerts as $a) {
            $a->is_checked = 1;
            $a->save();
        }

        $alert_contents = array();

        foreach ($alerts as $a) {

            $sender = User::find($a->sender_id);
            $alert_contents[$a->id]['sender_id'] = $a->sender_id;
            $alert_contents[$a->id]['alert_type'] = $a->alert_type;
            $alert_contents[$a->id]['contents'] = $a->contents;
            $alert_contents[$a->id]['sender_logo'] = $sender->logo_img;
            $alert_contents[$a->id]['sender_type'] = $a->sender_type;
            $alert_contents[$a->id]['related_id'] = $a->related_id;
            $alert_contents[$a->id]['is_apporved'] = $a->is_apporved;
            $alert_contents[$a->id]['status'] = 0;
            $alert_contents[$a->id]['oppotunity_id'] = $a->oppotunity_id;

            if ($a->alert_type == Alert::ALERT_CONNECT_CONFIRM_REQUEST) {
                $friend = Friend::where('user_id', $a->sender_id)->where('friend_id', $user->id)->where('is_deleted', '<>', 1)->where('status', Friend::FRIEND_APPROVED)->get()->first();
                if ($friend != null) {
                    $alert_contents[$a->id]['status'] = 1;
                }
            }
            $alert_contents[$a->id]['date'] = $a->created_at->format(config('app.date_time_format'));
            if ($a->sender_type == 'volunteer') {
                $alert_contents[$a->id]['sender_name'] = $sender->first_name . ' ' . $sender->last_name;
            } elseif ($a->sender_type == 'organization') {
                $alert_contents[$a->id]['sender_name'] = $sender->org_name;
            }
        }
        
        return view('notification', ['alert' => $alert_contents, 'page_name' => '', 'paginate' => $alerts]);
    }

    public function shareGroup(Request $req) {

        $member = Group_member::where([
                    'group_id' => base64_decode($req->id),
                    'role_id' => '2',
                    'user_id' => \auth()->id(),
                    'is_deleted' => 0,
                ])->first();

        $groupsCtrl = new GroupCtrl();

        $isAlreadyMember = $member ? $member->status : '012'; //dummy logic to check the user is not member of the group

        $allGroupLists = CommonModel::getAll('group_members', array(array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');
        $groupLists = CommonModel::getAll('group_members', array(array('groups.id', '=', base64_decode($req->id)), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');

        $allGroupList = array();

        if (count($allGroupLists) > 0) {

            foreach ($allGroupLists as $keys => $values) {
                
                $total_hours = 0;
                $allGroupList[] = $values;

                $allGroupList[$keys]->tracked_hours = 0;

                $grpMembers = CommonModel::getAll('group_members', array(array('group_id', '=', $values->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id')));

                if (count($grpMembers) > 0) {
                    foreach ($grpMembers as $memberObj) {
                        $hours = CommonModel::getAll('tracked_hours', array(array('volunteer_id', '=', $memberObj->user_id), array('approv_status', '=', 1)), '', 'logged_mins', '', '');
                        if (count($hours) > 0) {
                            foreach ($hours as $track) {
                                $total_hours = $total_hours + $track->logged_mins;
                            }
                        }
                    }
                }

                $allGroupList[$keys]->tracked_hours = $total_hours;
                $allGroupList[$keys]->total_members = count($grpMembers);
                $allGroupList[$keys]->cat_name = Group::getGroupCategory(count($grpMembers));
            }
        }

        $allGroupList = $this->msort($allGroupList, array('tracked_hours'));

        $groupList = array();
        $count=0;

        if (count($groupLists) > 0) {

            foreach ($groupLists as $key => $value) {

                $count++;

                $groupList[] = $value;
                $groupList[$key]->members = array();
                $groupList[$key]->tracked_hours = 0;
                $groupList[$key]->datewise = array();
                $groupList[$key]->sixmonthwise = array();
                $groupList[$key]->lastYearwise = array();
                $groupList[$key]->Yearwise = array();
                $groupList[$key]->CountryWiseCountlastMonth = array();
                $groupList[$key]->groupStateWiseCountlastMonth = array();

                $groupList[$key]->creatorDetails = CommonModel::getAllRow('users', array(array('id', '=', $value->creator_id)));
                $groupList[$key]->groupCategory = CommonModel::getAllRow('group_categories', array(array('id', '=', $value->group_category_id)));
                if(!empty($value->affiliated_org_id)){
                    $groupList[$key]->affiliatedOrg = CommonModel::getAllRow('users', array(array('id', '=', $value->affiliated_org_id)));
                    //print_r($groupList[$key]->affiliatedOrg);exit;
                    
                }
                $grpMembers = CommonModel::getAll('group_members', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id'), array('reviews', 'reviews.review_to', 'users.id')), '', '', array('group_members.id'));

                list(
                    $groupList[$key]->tracked_hours,
                    $groupList[$key]->des_tracked_hours,
                    $groupList[$key]->members,
                    $groupList[$key]->ranking_cat,
                    $groupList[$key]->ranking_cat_name) = $groupsCtrl->getGrpRankingHoursNameInfo($allGroupList, $grpMembers ,$value);

                $allMembersIds = collect($groupList[$key]->members)->pluck('user_id')->toArray();

                list(
                    $groupList[$key]->hours_oppr_type_chart,
                    $groupList[$key]->total_opportunities,
                    $groupList[$key]->avg_hrs_per_oppr,
                    $groupList[$key]->top_5_opprtunities,
                    $groupList[$key]->opprtunities_list) = $groupsCtrl->getGrpMemberOpprsData($allMembersIds);

                list(
                    $groupList[$key]->volunteers,
                    $groupList[$key]->volunteers_avg,
                    $groupList[$key]->volunteers_pie_zip,
                    $groupList[$key]->volunteers_pie_age,
                    $groupList[$key]->volunteersTop5Chart) = $groupsCtrl->getGrpMemberOpprVolonteers($allMembersIds ,$value);


                $rank = $groupsCtrl->myfunction($allGroupList, 'group_id', $groupList[$key]->group_id);
                $groupList[$key]->rank = $rank;

                $thisMonthFirstDay = date('Y-m-d', strtotime("first day of this month"));
                $currentDay = date('Y-m-d');
                $groupList[$key]->thisMonth = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=',$thisMonthFirstDay), array('logged_date', '<=',  $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'logged_date', array('logged_date', 'SUM' => 'logged_mins'));


                $lastmonthFirstDay = date('Y-m-d', strtotime("first day of -1 month"));
                $lastmonthlastDay = date('Y-m-d', strtotime("last day of -1 month"));
                $groupList[$key]->lastMonth = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthFirstDay), array('logged_date', '<=', $lastmonthlastDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'logged_date', array('logged_date', 'SUM' => 'logged_mins'));


                $sixmonthFirstDay = date('Y-m-d', strtotime("first day of -6 month"));
                $currentDay = date('Y-m-d');
                $groupList[$key]->last6Month = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $sixmonthFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));


                $lastYearFirstDay = date('Y-m-d', strtotime("first day of -12 month"));

                $currentDay = date('Y-m-d');

                $groupList[$key]->last12Month = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));
                $currentDay = date('Y-m-d');

//              $groupList[$key]->Yearwise = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'YEAR', array('logged_date', 'SUM' => 'logged_mins', 'YEAR' => 'logged_date'));

                if($groupList[$key]->creatorDetails!=null)
                {
                    $groupList[$key]->CountryWiseCountlastMonth = $CountryWiseCountlastMonth = CommonModel::getAll('groups', array(array('country', '=', $groupList[$key]->creatorDetails->country)), array(array('users', 'users.id', 'groups.creator_id')), 'groups.id', '', '', '', 0, 10);
                    $groupList[$key]->CountryWiseCountlastMonth5 = $CountryWiseCountlastMonth5 = CommonModel::getAll('groups', array(array('state', '=', $groupList[$key]->creatorDetails->state)), array(array('users', 'users.id', 'groups.creator_id')), 'groups.id', '', '', '', 0, 5);
                }

                $groupList[$key]->arr = array();
                $groupList[$key]->month = array();
                $groupList[$key]->arr5 = array();
                $groupList[$key]->month5 = array();

                if(isset($CountryWiseCountlastMonth))
                {
                    if (count($CountryWiseCountlastMonth) > 0) {
                        foreach ($CountryWiseCountlastMonth as $k => $groups) {
                            $groupList[$key]->yearly[$k]['country'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));

                            if (count($groupList[$key]->yearly[$k]['country']) > 0) {
                                array_push($groupList[$key]->arr, $groups->id);
                            }
                            //print_r($groupList[$key]->yearly[$k]);
                            $groupList[$key]->monthly[$k]['month'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthlastDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));

                            if (count($groupList[$key]->monthly[$k]['month']) > 0) {
                                array_push($groupList[$key]->month, $groups->id);
                            }
                        }
                    }
                }

                if(isset($CountryWiseCountlastMonth5))
                {
                    if (count($CountryWiseCountlastMonth5) > 0) {
                        foreach ($CountryWiseCountlastMonth5 as $k => $groups) {
                            $groupList[$key]->yearly[$k]['country'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));

                            if (count($groupList[$key]->yearly[$k]['country']) > 0) {
                                array_push($groupList[$key]->arr5, $groups->id);
                            }
                            //print_r($groupList[$key]->yearly[$k]);

                            $groupList[$key]->monthly[$k]['month'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthlastDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));

                            if (count($groupList[$key]->monthly[$k]['month']) > 0) {
                                array_push($groupList[$key]->month5, $groups->id);
                            }
                        }
                    }
                }

                $groupList[$key]->volun = array();
                $groupList[$key]->volun5 = array();

                if($groupList[$key]->creatorDetails)
                {
                    $volunteer = CommonModel::getAll('group_members', array(array('country', '=', $groupList[$key]->creatorDetails->country), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id'), array('users', 'users.id', 'group_members.user_id')), '', 'SUM', 'group_id', array('group_id', 'SUM' => 'group_id'), 0, 10);
                    $volunteer5 = CommonModel::getAll('group_members', array(array('state', '=', $groupList[$key]->creatorDetails->state), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id'), array('users', 'users.id', 'group_members.user_id')), '', 'SUM', 'group_id', array('group_id', 'SUM' => 'group_id'), 0, 5);

                    if (count($volunteer) > 0) {
                        foreach ($volunteer as $ks => $volun) {
                            array_push($groupList[$key]->volun, $volun->group_id);
                            //print_r($groupList[$key]->yearly[$k]);
                        }
                    }

                    if (count($volunteer5) > 0) {
                        foreach ($volunteer as $ks => $volun) {
                            array_push($groupList[$key]->volun5, $volun->group_id);
                            //print_r($groupList[$key]->yearly[$k]);
                        }
                    }
                }
            }
            //echo "<pre>"; print_r($groupList); echo "</pre>"; die;
        }

        $myFriends = DB::table('users')
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', 1)
                ->join('friends', 'users.id', '=', 'friends.friend_id')
                ->where('friends.user_id', \auth()->id())
                ->where('friends.is_deleted', '<>', 1)
                ->where('friends.status', 2)
                ->select('users.*', 'friends.*')
                ->get();

        if(count($groupList)  == 0) //it means that group is deleted from the db todo: need to mke it single object rather that multiple
            return back();

        return view('sharegroup', [
            'myFriends' => $myFriends,
            'isAlreadyMember' => $isAlreadyMember,
            'org_type_names' => \App\Organization_type::all(),
            'school_type' => \App\School_type::all(),
            'page' => 'home',
            'groupList' => $groupList]
        );
    }

    function msort($array, $key, $sort_flags = SORT_REGULAR) {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v->$key_key;
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                arsort($mapping, $sort_flags);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }
                return $sorted;
            }
        }
        return $array;
    }

    function myfunction($products, $field, $value) {
        foreach ($products as $key => $product) {
            if ($product->$field === $value)
                return $key + 1;
        }
        return false;
    }

    public function sendShareEmails(Request $request) {

        try {

            $otherEmails = [];
            $count = 0;
            $group = Group::find($request->group_id);

            $array = explode("\n", trim($request->others_emails));

            for ($i = 0; $i < count($array); $i++) {
                $otherEmails[] = trim($array[$i]);
            }

            $friendsEmails = $request->friend_emails ? $request->friend_emails : [];

            $allEmails = array_merge($friendsEmails, $otherEmails);

            if (count($allEmails) > 0) {
                foreach ($allEmails as $emailText) {
                    if (filter_var($emailText, FILTER_VALIDATE_EMAIL)) {

                        $email = EmailsTemplate::where('type', 'sharedGroupLink')->first();
                        $private = $group->is_public ? '' : '<b>Passcode : </b> ' . $group->private_passcode . '';
                        $link = $private . '<br/><br/><a href="' . $request->link . '">Click To See More</a>';
                        $body = str_replace("{{link}}", $link, $email->body);

                        \Mail::to($emailText)->send(new UsersDefaultMail($body, $email->title));

                        $count++;
                    }
                }
                session()->flash('app_message', $count . ' Emails sent successfully.');
            } else
                session()->flash('app_error', 'Invalid data found please use correct emails.');

            return back();
        } catch (\Exception $e) {

            Log::error($e->getMessage() . 'Invalid Emails by user on group share page');
            session()->flash('app_error', 'Invalid data found please use correct emails.');
//            dd($e);
            return back();
        }
    }

    public function checkPrivateGroupPascode(Request $request) {


        $response['result'] = false;
        $group = Group::where(['id' => $request->group_id, 'private_passcode' => $request->private_passcode])->first();
        if ($group)
            $response['result'] = true;

        return Response::json($response);
    }

    //Method to share Service Project
    public function shareServiceProject(Request $request) {
        $user = Auth::user();
        $emails = $request->input('emails');
        $comments = $request->input('comments');
        $serviceid = $request->input('serviceid');
        
        $code = base64_encode($this->generateRandomString(20).'~'.$serviceid);
        
        $shr_email = explode(',', $emails);

        if ($user->user_role == 'volunteer') {
            for ($i = 0; $i < count($shr_email); $i++) {
                $email = $shr_email[$i];
                if(!empty($email)){
                    $shared_id = $this->saveSharedInfo($code, $user->id, $email);
                    $this->sendShareServiceProjectEmail($user, $shared_id, $comments, $email, $code);
                }
            }
        }

        return Response::json(['result' => 'success']);
    }

    //Method to send email for service project 
    public function sendShareServiceProjectEmail($user, $shared_id, $comments, $to, $code) {

        $fromName = $user->first_name . ' ' . $user->last_name;
        $data['content'] = $comments;
        $data['shared_id'] = $shared_id;
        $data['code'] = $code;
        $email = EmailsTemplate::where('type', 'shareServiceProject')->first();
        if ($user->friend_req_email == '0') { //if enabled emails
            $body = replaceBodyContents($email->body, $email->type, $user, null, null, $data);
             \Mail::to($to)->send(new SharingProfileMail($body, $fromName . ' ' . $email->title, $user->email, $fromName));
        }

        if ($user->friend_req_text == '0' && $user->contact_number) { //if texts enabled
            $messageBody = replaceTextBodyContents($email->text_body, $email->type, $user, null, null, $data);
            sendSMSByTwilio($user->contact_number, $messageBody);
        }
    }

}
