<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Volunteer\UnlistedOrganizationOpprHourController;
use App\Mail\UsersDefaultMail;
use App\Models\Delegate;
use App\Models\EmailsTemplate;
use App\Models\Sitecontent;
use App\Models\UnlistedOrgnizationOpprHour;
use App\School_type;
use App\Services\AmazonRequests;
use App\Tracking;
use App\User;
use App\Group_member;
use App\Group;
use App\Chat;
use App\VoluntierOrganizationMapping;
use App\Organization_type;
use App\Opportunity_member;
use App\SelectedCategoryUserpref;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Comments;
use App\Services\ChatService;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Alert;

class UserCtrl extends Controller
{

    public $parentOrg = null;
    public $allOrganizations = null;
    public $subOrg = null;

    public function __construct() {

        $query = User::select('id','org_name')->where('user_role','organization')
            ->where('is_deleted', '<>',1)->orderBy('org_name', 'asc');
      
        $this->parentOrg = (clone $query)->where('parent_id',null)->where('status','<>',0)->get()->toArray();
        $this->allOrganizations = (clone $query)->get()->toArray();
        $this->subOrg = (clone $query)->where('parent_id','<>',null)->where('status','<>',0)->get()->toArray();

    }

    public function showLanding(Request $request)
    {
        if (!Auth::check()) {
            return view('home', [
                'parentOrg' => $this->parentOrg,
                'allOrganizations' => $this->allOrganizations,
                'org_type_names' => Organization_type::all(),
                'school_type' => School_type::all()
            ]);

        } else {

            if (Auth::user()->user_role == 'volunteer') {
                //return redirect()->to('/volunteer/');
                return view('home', [
                    'org_type_names' => Organization_type::all(),
                    'school_type' => School_type::all()
                ]);

            } else {
                //return redirect()->to('/organization/');
                return view('home', [
                    'org_type_names' => Organization_type::all(),
                    'school_type' => School_type::all()
                ]);
            }
        }

    }

    public function showFeatures(Request $request)
    {
        return view('features', [
            'parentOrg' => $this->parentOrg,
            'allOrganizations' => $this->allOrganizations,
            'org_type_names' => Organization_type::all(),
            'school_type' => School_type::all()
        ]);
    }

    public function articles(Request $request)
    {
        return view('articles', [
            'parentOrg' => $this->parentOrg,
            'allOrganizations' => $this->allOrganizations,
            'org_type_names' => Organization_type::all(),
            'school_type' => School_type::all()
        ]);
    }

    public function showPricing(Request $request)
    {
        return view('pricing', [
            'parentOrg' => $this->parentOrg,
            'allOrganizations' => $this->allOrganizations,
            'org_type_names' => Organization_type::all(),
            'school_type' => School_type::all(),
            'page' => 'pricing'
        ]);
    }

    public function showRequest(Request $request)
    {
        return view('request', [
            'parentOrg' => $this->parentOrg,
            'allOrganizations' => $this->allOrganizations,
            'org_type_names' => Organization_type::all(),
            'school_type' => School_type::all()
        ]);
    }

    public function terms_conditions(Request $request)
    {
        return view('termsandconditions', [
            'parentOrg' => $this->parentOrg,
            'allOrganizations' => $this->allOrganizations,
            'org_type_names' => Organization_type::all(),
            'school_type' => School_type::all()
        ]);
    }

    public function regVolunteer(Request $request)
    {
        $check_email = User::where('email', '=', $request->input('email'))->where('user_role', 'volunteer')->count();

        if ($check_email > 0)
            return Response::json(['result' => 'email exist']);

        $location = $this->getLocation($request->input('zipcode'));

        if ($location == 'error')
            return Response::json(['result' => 'invalid zipcode']);

        $address = $this->getAddress($request->input('zipcode'));

        if ($address == 'error') {
            return Response::json(['result' => 'invalid zipcode']);
        }

        $user = new User;
        $user->user_role = 'volunteer';
        $user->user_name = convertUserName('v-'.$request->input('email'));
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->org_affiliate = $request->has('org_affiliate') ? $request->input('org_affiliate') : '';
        $user->email = $request->input('email');
        $user->email2 = $request->input('email2');
        $user->email3 = $request->input('email3');
        $user->password = bcrypt($request->input('password'));
        $user->birth_date = dateMdySlashFormat($request->input('birth_day'));
        $user->gender = $request->input('gender');
        $user->contact_number = $request->input('contact_number');
        $user->zipcode = $request->input('zipcode');
        $user->middle_initial = $request->input('middle_initial');
        $user->confirm_code = str_random(50);
        $user->auto_accept_friends = $request->input('auto_accept_friends');

        if(!empty($request->input('email2'))){
            $user->confirm_code2 = str_random(50);
        }
        if(!empty($request->input('email3'))){
            $user->confirm_code3 = str_random(50);
        }

        if ($location != 'error') {
            $user->lat = $location['lat'];
            $user->lng = $location['lng'];
        }

        if ($address != 'error') {
            $user->city = $address['city'];
            $user->state = $address['state'];
            $user->country = $address['country'];
        }

        $user->status = 0;
        $user->save();
               

        if($user->id && $request->input('org_id')){
            VoluntierOrganizationMapping::create([
                "voluntier_id"=>$user->id,
                "organization_id"=>$request->input('org_id')
                ]);
         }


        // $chatService = new ChatService();
        // $chatService->createChatUser($user->user_name, $user->first_name . ' ' . $user->last_name, asset('img/logo/member-default-logo.png'));
        // $this->sendUserConfirmationEmail($user ,'confirmAccount', true);
        // $this->alterEmailConfirmation($user->id);
          
        return Response::json(['result' => 'success' ,'user_id'=>$user->id]);
    }

    public function sendUserConfirmationEmail($user , $type, $isSignup = false){


        $email = EmailsTemplate::where('type' , $type)->first();
        $body = replaceBodyContents($email->body ,$email->type, $user);

         \Mail::to($user->email)->send(new UsersDefaultMail($body ,$email->title));


        if($isSignup){
            //Notify Admin for the new user
            $emailAdmin = EmailsTemplate::where('type' , 'userRegistrationNotification')->first();

            $body = replaceBodyContents($emailAdmin->body ,$emailAdmin->type, $user);

            $siteContent = Sitecontent::where('type' ,'userRegistrationNotification')->first();


            \Mail::to($siteContent->title)->send(new UsersDefaultMail($body ,$emailAdmin->title));
        }
    }

    public function regOrganization(Request $request)
    {
        $userId = (isset(Auth::user()->id)) ? Auth::user()->id : "";
        $subOrgid = $userName = "";

        if (!empty($userId)) {

            $subOrgid = $request->input('sub_org_id');

            if (empty($subOrgid)) {
                $user = new User;
            } else {
                $user = User::find($subOrgid);
            }

            $user->org_name = $request->input('sub_org_name');
            $user->website = $request->input('website');
            $user->facebook_url = $request->input('facebook_url');
            $user->twitter_url = $request->input('twitter_url');
            $user->linkedin_url = $request->input('linkedin_url');
            $user->auto_accept_friends = $request->get('auto_accept_friends') ? '1' : '0';

            if (!empty($subOrgid)) {
                $user->id = $subOrgid;
            } else {
                $user->confirm_code = '1';
                $user->status = 1;
            }

            $check_email = User::where('email', '=', $request->input('email'))
                ->where('parent_id', $userId)
                ->where('user_role', 'organization')
                ->where('is_deleted', '<>', 1);

            if (!empty($subOrgid)) {
                $check_email->where('id', '<>', $subOrgid);
            }

            $check_email = $check_email->count();

            if ($check_email > 0) {
                return Response::json(['result' => 'email exist']);
            }
            $user->parent_id = $userId;

        } else {
            $user = new User;
            if(!empty($request->input('parent_org'))){
                $user->parent_id = $request->input('parent_org');
            }
            $user->org_name = $request->input('org_name');
            $user->confirm_code = str_random(50);

            $check_email = User::where('email', '=', $request->input('email'))->where('user_role', 'organization')->count();

            if ($check_email > 0) {
                return Response::json(['result' => 'email exist']);
            }

        }

        $location = $this->getLocation($request->input('zipcode'));

        if ($location == 'error')
            return Response::json(['result' => 'invalid zipcode']);

        $address = $this->getAddress($request->input('zipcode'));

        if ($address == 'error') {
            return Response::json(['result' => 'invalid zipcode']);
        }

        $user->user_role = 'organization';
        $user->user_name = convertUserName(empty($subOrgid) ? 'o-'.$request->input('email') : 's-'.$request->input('email'));

        if ($address != 'error') {
            $user->city = $address['city'];
            $user->state = $address['state'];
            $user->country = $address['country'];
        }

        $user->email = $request->input('email');

        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->birth_date = $request->input('birth_day');
        $user->contact_number = $request->input('contact_number');
        $user->zipcode = $request->input('zipcode');
        $user->org_type = $request->input('org_type');

        if ($request->input('org_type') !== null) {
            if ($request->input('org_type') == Organization_type::EDUCATIONAL_INSTITUTION) {
                $user->school_type = $request->input('school_type');
            }

            if ($request->input('org_type') == Organization_type::NONPROFIT) {
                $user->ein = $request->input('ein');
            }

            if ($request->input('org_type') == Organization_type::NGO_NONPROFIT) {
                $user->nonprofit_org_type = $request->input('nonprofit_org_type');
            }
        };


     /*   $location = $this->getLocation($request->input('zipcode'));

        if ($location != 'error') {
            $user->lat = $location['lat'];
            $user->lng = $location['lng'];
        }*/

        $user->auto_accept_friends = $request->get('auto_accept_friends');
        $user->save();

        if (empty($subOrgid)) {
            $chatService = new ChatService();

            $chatService->createChatUser($user->user_name, $user->org_name, asset('img/org/001.png'));

            $this->mapUnlistedOrgOprHours($user);
        }

//	    $chatId = $chatService->createChat($user->org_name, asset('front-end/img/org/001.png'));
//	    $chatService->addUserToChat($user, $chatId, $user->org_name, 'organizations');
//	    $chat = new Chat();
//	    $chat->chat_id = $chatId;
//	    $chat->user_id = $user->id;
//	    $chat->type = 'organizations';
//	    $chat->save();

        if ($userId == "") {
            if(!empty($request->input('parent_org'))){
                $alert = new Alert;
                $alert->receiver_id = $request->input('parent_org');
                $alert->sender_id = $user->id;
                $alert->sender_type = $user->user_role;
                $alert->alert_type = Alert::ALERT_SUB_ORGANIZATION_REQUEST;
                $alert->contents = $user->org_name." has requested a Sub-Organization account.";
                $alert->save();
            }else{
                $this->sendUserConfirmationEmail($user, 'confirmAccount', true);
            }
        } else {
            session()->flash('app_message', 'Data has been saved successfully.');
        }
        return Response::json(['result' => 'success']);
    }

    public function regDelegate(Request $request)
    {
        try{

            $orgId = auth()->id();
            $organization = User::find($orgId);

            $user = new Delegate();

            if($request->input('delegate_id')){
                $id = base64_decode($request->input('delegate_id'));
                $user = Delegate::find($id);
            }
            else{

                $mail_exist = Delegate::where('email', $request->email)->first();
                $mail_exist_own = User::where([
                    'email'=> $request->email,
                    'id'=> $orgId,
                ])->first();

                if ($mail_exist || $mail_exist_own) {
                    return Response::json(['result' => 'email exist']);
                }

                $randomPassword = str_random(6);

                $user->password = bcrypt($randomPassword);
                $user->email = $request->input('email');

                $data = [
                    'name' => $request->input('first_name') . ' ' . $request->input('last_name'),
                    'email' => $request->input('email'),
                    'password' => $randomPassword,
                    'org_name' => $organization->org_name,
                ];

                $email = EmailsTemplate::where('type' , 'delegateCreate')->first();
                $body = replaceBodyContents($email->body ,$email->type, null, null, null, $data);

                //also send random password
                \Mail::to($request->input('email'))->send(new UsersDefaultMail($body ,$email->title));
            }

            $user->first_name = $request->input('first_name');
            $user->org_id = $orgId;
            $user->last_name = $request->input('last_name');

            $user->comments = $request->input('comments');
            $user->confirm_code = 1;
            $user->remember_token = str_random(10);

            $user->save();

            return Response::json(['result' => 'success']);
        }
        catch (\Exception $e){

            Log::error('error during reg Delegate' . $e->getMessage().$e->getLine());
            return Response::json(['result' => 'error']);
        }
    }

    public function deleteDelegate($id) {

        try{
            $delegateObj = Delegate::where('org_id' ,auth()->id())->where('id',$id)->first();

            $delegateObj->delete();
            session()->flash('app_message', 'Delegate has been deleted successfully.');
        }
        catch (\Exception $e){
            session()->flash('app_error', 'Something went wrong.');
        }

        return redirect()->to('/organization/profile#orgDelegates');

    }

    public function mapUnlistedOrgOprHours($user) {

        $allUnlistedEntries = UnlistedOrgnizationOpprHour::where('org_email' ,$user->email)->get();

        foreach ($allUnlistedEntries as $unlist){

            $unlistedOpprCtrl = new UnlistedOrganizationOpprHourController();
            $unlistedOpprCtrl->copyUnlistedOrgHours($unlist ,$user);

            //delete temp data that is already copied to the original table
            $unlist->delete();
        }
    }

    public function login(Request $request)
    {
        \Session::forget('is_delegate'); // <------- add this line
        $response['errors'] = true;
        $role = null;
        $username = $request->input('user_id');
        $password = $request->input('password');
        $loginAs = strtolower($request->input('login_as'));

        $parentId = $request->parent_user_id;
        
        if ($loginAs == 'suborganization') {
            $loginAs = 'organization';
        }

        $parentIdCond = [];

        if($loginAs == 'volunteer' && is_null($parentId)) {
            $user = User::where('email', $username)->where('user_role', $loginAs)->first();
        }
        else
        {
            $organization = User::where('id' ,$request->organization_id)->where('user_role' ,'organization')->first();
            $delegate = Delegate::where(['email' => $username ,'org_id' => $request->organization_id])->first();
            if ($organization && $delegate && password_verify($password, $delegate->password)) {
                $response['errors'] = false;
                $response['role'] = 'organization';

                if (Auth::loginUsingId($organization->id)) {
                    session(['is_delegate' => $delegate->id]);
                    return response()->json($response);
                }
            }

            //todo: also need to check the organization here. Correct organization()$request->organization_id
            $query = User::where('email', $username)->where('user_role', $loginAs);
            if($parentId){
                $query->where('parent_id', $parentId);
            }
            $user = $query->first();
            
            $parentIdCond = ['parent_id' => $request->input('parent_user_id')];

        }

        if ($user && $user->is_deleted == '0') {
            $role = ucfirst($user->user_role);

            if ($user->confirm_code == '1') {
                if ($user->status == '1') {
                    if (Auth::attempt($parentIdCond + ['email' => $username, 'password' => $password, 'user_role' => $loginAs])) {
                        $response['errors'] = false;
                        $response['role'] = strtolower($role);
                    } else {
                        $response['message'] = "Incorrect email/password for login as " . $request->input('login_as');
                    }
                } else {
                    $response['message'] = "Your {$role} account is currently pending verification by a MyVoluntier account specialist. You will be notified once complete";
                }

            } else {
                $response['message'] = "Email has not been verified for your {$role} account please check inbox";
                $this->sendUserConfirmationEmail($user, 'confirmAccount');
            }
        } else {

            $response['message'] = 'Incorrect email/password for login as ' . $request->input('login_as');
        }

        return response()->json($response);
    }


    public function signout(){

        Auth::logout();
        \Session::forget('is_delegate');
        return redirect()->to('/');
    }

    public function getLocation($zipcode)
    {
        /*get location from zipcode*/
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $zipcode . "&key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es";
        $result = json_decode(file_get_contents($url), true);

        if ($result['results'] == []) {
            return 'error';
        } else {
            return $result['results'][0]['geometry']['location'];
        }

        /*get location from IP address*/

//		$ip = $request->ip();

//		$details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"),true);

//		return $details['loc'];

    }


    public function getAddress($zipcode)
    {
        /*get address from zipcode*/
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $zipcode . "&key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es";
        $json = json_decode(file_get_contents($url), true);

        $city = '';
        $state = '';
        $country = '';

        if ($json['results'] == []) {
            return 'error';
        } else {
            foreach ($json['results'] as $result) {
                foreach ($result['address_components'] as $addressPart) {
                    if ((in_array('locality', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
                        $city = $addressPart['long_name'];
                    else if ((in_array('administrative_area_level_1', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
                        $state = $addressPart['short_name'];
                    else if ((in_array('country', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
                        $country = $addressPart['long_name'];
                }
            }

            $address = array();

            $address['city'] = $city;
            $address['state'] = $state;
            $address['country'] = $country;

            return $address;
        }
        /*get address from IP address*/

//		$ip = $request->ip();

//		$details = json_decode(file_get_contents("http://ipinfo.io/23.247.115.83/json"),true);

//		$city = $details['city'];

//		$state = $details['region'];

//		$country = $details['country'];

//		return $city.','.$state.','.$country;

    }


    public function update_account(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if ($user != null) {
            //$user->org_name = $request->get('org_name');
            $user->website = $request->get('website');
            $user->birth_date = $user->user_role == 'organization' ? $request->input('birth_day') : dateMdySlashFormat($request->input('birth_day'));
            $user->org_type = $request->get('org_type');
            $user->zipcode = $request->get('zipcode');
            $user->auto_accept_friends = $request->get('auto_accept_friends') ? '1' : '0';

            $user->domain_1 = $request->get('domain_1');
            $user->domain_2 = $request->get('domain_2');

            if (!empty($request->get('domain_1')) && $user->domain_1_confirm_code != '1') {
                $user->domain_1_confirm_code = str_random(50);
            }

            if (!empty($request->get('domain_2')) && $user->domain_2_confirm_code != '1') {
                $user->domain_2_confirm_code = str_random(50);
            }
            //$user->show_address  = $request->get('show_address');

            $location = $this->getLocation($request->get('zipcode'));

            if ($location != 'error') {
                $user->lat = $location['lat'];
                $user->lng = $location['lng'];
            }

            $address = $this->getAddress($request->input('zipcode'));

            if ($address != 'error') {
                $user->city = $address['city'];
                $user->state = $address['state'];
                $user->country = $address['country'];
            }

            $user->location = $request->get('address');
            $user->contact_number = $request->get('contact_num');

            $user->brif = $request->get('org_summary');

            if ($request->get('new_password') != '') {
                $user->password = bcrypt($request->get('new_password'));
            }

            if ($request->get('facebook_url') != '') {
                $user->facebook_url = $request->get('facebook_url');
            }

            if ($request->get('twitter_url') != '') {
                $user->twitter_url = $request->get('twitter_url');
            }

            if ($request->get('linkedin_url') != '') {
                $user->linkedin_url = $request->get('linkedin_url');
            }

            $save = $user->save();

            if ($save) {

                $returnArr = array(
                    'status' => '1',
                    'msg' => 'Profile updated successfully..!'
                );

                $allChatsUser = Chat::where('user_id', Auth::user()->id)->whereNotNull('to_user_id')->get();
                $userAuth = Auth::user();
                $chatService = new ChatService();
                $chatService->updateUserInfo($userAuth);

                if ($user->user_role === 'organization')
                    $logo = $user->logo_img === null ? asset('img/org/001.png') : $user->logo_img;
                else {
                    $logo = $user->logo_img === null ? asset('img/logo/member-default-logo.png') : $user->logo_img;
                }

                foreach ($allChatsUser as $chat) {
                    $chatService->updateChatInfo($chat->chat_id, $user->org_name, $logo);
                }

                $allChatsUser = Chat::where('to_user_id', $user->id)->get();

                foreach ($allChatsUser as $chat) {
                    $opponent = User::find($chat->user_id);
                    $chatService->updateChatInfo($chat->chat_id, $user->org_name, $logo, $opponent->user_name);
                }

            } else {

                Log::warning('profile update fail for ' . $user->id);
                $returnArr = array(
                    'status' => '0',
                    'msg' => 'Something went wrong during update.'
                );
            }
            echo json_encode($returnArr);
            die();
        }
    }

    public function update_volunteer_account(Request $request)
    {
        $user = User::find(Auth::user()->id);

        /*$validation = Validator::make(
            $request->all(), [
                'email' => "unique:users,email,$user->id,id"
            ]
        );

        if($validation->fails()){
            $returnArr = array('status' => '0', 'msg' => 'Email already exist please choose another.');
            echo json_encode($returnArr);
            die();
        }*/

        if ($user != null) {
            // $user->first_name = $request->get('first_name');
            // $user->last_name = $request->get('last_name');
            //$user->user_name = $request->get('user_id'); //Not changeable from now to onward

            $user->email2 = (!empty($request->get('email2')))? $request->get('email2') : null;
            $user->email3 = (!empty($request->get('email3')))? $request->get('email3') : null;

            if(!empty($request->get('email2')) && $user->confirm_code2 != '1'){
                $user->confirm_code2 = str_random(50);
            }

            if(!empty($request->get('email3')) && $user->confirm_code3 != '1' ){
                $user->confirm_code3 = str_random(50);
            }

            $user->gender = $request->get('gender');
            $user->birth_date = dateMdySlashFormat($request->input('birth_day'));
            $user->zipcode = $request->get('zipcode');
            $user->show_address = $request->get('show_address');
            $user->show_age = $request->get('show_age');
            $user->middle_initial = $request->get('middle_initial');

            $location = $this->getLocation($request->get('zipcode'));

            if ($location != 'error') {
                $user->lat = $location['lat'];
                $user->lng = $location['lng'];
            }

            $address = $this->getAddress($request->input('zipcode'));

            if ($address != 'error') {
                $user->city = $address['city'];
                $user->state = $address['state'];
                $user->country = $address['country'];
            }

            $user->contact_number = $request->get('contact_num');
            $user->brif = $request->get('my_summary');

            if ($request->get('new_password') != '') {
                $user->password = bcrypt($request->get('new_password'));
            }

            $save = $user->save();

            if ($save) {


                if (!empty($request->opportunity_type)) {

                    SelectedCategoryUserpref::where('user_id', Auth::user()->id)->delete();
                    $opportunity_type_array = $request->opportunity_type;

                    foreach ($opportunity_type_array as $key => $opportunity_type_id) {
                        $selected_cat = new SelectedCategoryUserpref();
                        $selected_cat->user_id = Auth::user()->id;
                        $selected_cat->category_id = $opportunity_type_id;
                        $selected_cat->status = 1;
                        $selected_cat->save();
                    }
                }

                $this->alterEmailConfirmation($user->id);
                $returnArr = array('status' => '1', 'msg' => 'Your account is successfully updated!');

	            $allChatsUser = Chat::where('user_id', Auth::user()->id)->whereNotNull('to_user_id')->get();
	            $userAuth = Auth::user();
	            $chatService = new ChatService();
	            $chatService->updateUserInfo($userAuth);
	            if($user->user_role === 'organization')
		            $logo = $user->logo_img === null ? asset('img/org/001.png') : $user->logo_img;
	            else{
		            $logo = $user->logo_img === null ? asset('img/logo/member-default-logo.png') :  $user->logo_img;
	            }
	            foreach ($allChatsUser as $chat){
		            $chatService->updateChatInfo($chat->chat_id, $user->getFullNameVolunteer(), $logo);
	            }
	            $allChatsUser = Chat::where('to_user_id', $user->id)->get();
	            foreach ($allChatsUser as $chat){
		            $opponent = User::find($chat->user_id);
		            $chatService->updateChatInfo($chat->chat_id, $user->org_name, $logo, $opponent->user_name);
	            }
            } else {
                $returnArr = array('status' => '0', 'msg' => 'update failed');
            }

            echo json_encode($returnArr);
            die();
        }

        /* return Redirect::back()->with('Success', 'Your account is successfully updated!'); */
    }

    public function confirmAccount($user_name, $token)
    {
        $user = User::where('user_name', $user_name)->where('confirm_code', $token)->get()->first();
        $flag = 0;
        if ($user) {
            $user->confirm_code = '1';
            $url = $this->confirmEmail($user);
            return Redirect()->to($url);
            $flag++;
        }else{
            $userEmail2 = User::where('user_name', $user_name)->where('confirm_code2', $token)->get()->first();
            if($userEmail2){
                $userEmail2->confirm_code2 = '1';
                $url = $this->confirmEmail($userEmail2);
                return Redirect()->to($url);
                $flag++;
            }else{
                $userEmail3 = User::where('user_name', $user_name)->where('confirm_code3', $token)->get()->first();
                if($userEmail3){
                    $userEmail3->confirm_code3 = '1';
                    $url = $this->confirmEmail($userEmail3);
                    return Redirect()->to($url);
                    $flag++;
                }
            }
        }
        if($flag == 0){
            session()->flash('app_error', 'User already confirmed.');
        }

        return Redirect()->to('/');
    }
    private function confirmEmail($user){

        if ($user->user_role == 'volunteer') {
            
            $VolOrgMapp =  VoluntierOrganizationMapping::where('voluntier_id', $user->id)->get()->first();
            
            $user->status = '1';
            $user->save();
            Auth::login($user);
            
            if($VolOrgMapp){
               return '/volunteer/show_organization/'.$VolOrgMapp->organization_id;
            }else{
               return '/volunteer/home/verified';  
            }
            
        } else {
            $user->save();
            session()->flash('app_message', 'You have successfully confirmed your email address. Your account is currently pending verification by a MyVoluntier account specialist.');
            return '/';
        }
    }
    public function forgotPassword(Request $request)
    {   
        $parentOrg=  $request->input('parentOrg');
        $subOrg= $request->input('subOrg');
        $email = $request->input('email');
        $loginAs = $request->input('loginas');
        $orgName = $request->input('name');
    // $role = $request->input('loginas') == 'volunteer' ? 'volunteer' : 'organization';
        if($loginAs== 'organization'){
            #MYVOL1-312 Modify the Password Reset screen for Organizations to include a drop-down for the user to select the Organization name prior to entering the email address.
            $user = User::where('org_name', $orgName)->where('email', $email)->where('user_role', 'organization')->where('parent_id',null)->first();
            $message = 'Invalid '.ucfirst($loginAs). ' name or email.';
            $response = ['result' => 'name_not_exist' ,'message' => $message];
        }
       else if($loginAs=='suborganization'){
            #MYVOL1-312 Modify the Password Reset screen for Organizations to include a drop-down for the user to select the Organization name prior to entering the email address.
            $user = User::where('org_name', $parentOrg)->where('email', $email)->where('user_role', 'organization')->where('parent_id','<>',null)->first();
            $message = 'Invalid '.ucfirst($loginAs). ' name or email.';
            $response = ['result' => 'subname_not_exist' ,'message' => $message];
        }
        else {
            $user = User::where('email', $email)->where('user_role', 'volunteer')->first();
            $message = 'Invalid email address.';
            $response = ['result' => 'email_not_exist' ,'message' => $message];
        }

        if ($user) {
            $user->forgot_status = str_random(50);
            $user->save();
        
            $this->sendForgotPasswordUserNameMail($user , 'forgotPassword');

            return Response::json(['result' => 'success']);

        } else {
            return Response::json($response);
        }


    }

    public function sendForgotPasswordUserNameMail($user , $type){

        $email = EmailsTemplate::where('type' , $type)->first();
        $body = replaceBodyContents($email->body ,$email->type, $user);

        \Mail::to($user->email)->send(new UsersDefaultMail($body ,$email->title));

    }


    public function forgotUsername(Request $request)
    {
        $email = $request->input('email');
        $user = User::where('email', $email)->get()->first();

        if ($user) {
            $this->sendForgotPasswordUserNameMail($user , 'forgotUsername');

            return Response::json(['result' => 'success']);
        } else {
            return Response::json(['result' => 'email_not_exist']);
        }
    }

    public function changeForgotPassword($user_name, $token)
    {
        $user = User::where('user_name', $user_name)->where('forgot_status', $token)->get()->first();

        if ($user) {
            return view('changepassword', ['id' => $user->id]);
        } else {
            return Redirect()->to('/');
        }
    }

    public function createNewPassword(Request $request) {

        $user_id = $request->get('user_id');
        $password = $request->get('new_password');
        $user = User::find($user_id);
        $user->password = bcrypt($password);
        $user->forgot_status = '';
        $user->save();

        $message = null;

        $role = ucfirst($user->user_role);

        if ($user->confirm_code == '1') {

            if ($user->status == '1') {
                Auth::login($user);

            } else {
                $message = "Your {$role} account is currently pending verification by a MyVoluntier account specialist. You will be notified once complete";
            }

        } else {

            $message = "Email has not been verified for your {$role} account please check inbox.";
            $this->sendUserConfirmationEmail($user, 'confirmAccount');
        }

        if($message) {
            session()->flash('app_error', $message);
            return \redirect()->route('home');
        }


        if($user->user_role == "volunteer"){
            return Redirect()->to('/volunteer/');
        }else{
            return Redirect()->to('/organization/');
        }
    }

    public function updateName(Request $request)
    {
        $user = User::find(Auth::user()->id);

        if(Input::hasFile('image')) {
            $file=Input::file('image');
            $name = str_replace(" ","",$file->getClientOriginalName());
            $file->move(public_path().'/',$name);
            $user->temp_first_name = $request->get('first_name1');
            $user->temp_last_name = $request->get('last_name1');
            $user->proof_of_identity = $name;
            $user->approval = 'PENDING';
            $url = 'volunteer/profile';
        }elseif(Input::hasFile('poi_org')) {
            $file=Input::file('poi_org');
            $name = str_replace(" ","",$file->getClientOriginalName());
            $name = str_replace(".","_".$user->id.".",$name);
            if (!file_exists(public_path().'/org_proof/')) {
                    mkdir(public_path().'/org_proof/', 0777, true);
                }
            $file->move(public_path().'/org_proof/',$name);
            $user->temp_org_name = $request->get('temp_org_name');
            $user->proof_of_identity_org = $name;
            $user->approval_org = 'PENDING';
            $url = 'organization/profile';
        }
        session()->flash('app_message', 'Request has been sent successfully.');
        $user->save();
        return redirect($url);
    }

    public function statusPost(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->post_status = $request->get('comment');
        $user->selection = $request->get('select');
        $user->save();
        return redirect('volunteer/profile');
    }

    public function imageDelete(Request $request)
    {
        // $user = User::find(Auth::user()->id);
        // $user->post_status =NULL;
        // $user->selection =NULL;
        // $comments=DB::table('comments')->where('status_user_id',$user->id)->delete();
        // $user->save();


        $comment=$request->input('id');
        //DB::table('tracked_hours')->where('id',$comment)->update(['comment'=>NULL]);
        DB::table('tracked_hours')->where('id',$comment)->update(['image'=>NULL]);
        return redirect('volunteer/profile');
    }

    public function commentDelete(Request $request)
    {
        // $user = User::find(Auth::user()->id);
        // $user->post_status =NULL;
        // $user->selection =NULL;
        // $comments=DB::table('comments')->where('status_user_id',$user->id)->delete();
        // $user->save();

        $comment=$request->input('id');
        DB::table('tracked_hours')->where('id',$comment)->update(['comment'=>NULL]);
        //DB::table('tracked_hours')->where('id',$comment)->update(['image'=>NULL]);
        return redirect('volunteer/profile');
    }

    public function activityImageUpload(request $request)
    {
        $trackedObject = Tracking::find($request->input('id'));

        if (Input::hasFile('image')) {
            $trackedObject->image = fileUploadOnS3($trackedObject->image, Input::file('image') ,User::S3PathActivity,'act');
        }

        $trackedObject->save();

        return redirect('volunteer/profile');
    }

    public function about_us()
    {
        return view('about_us', [
            'parentOrg' => $this->parentOrg,
            'allOrganizations' => $this->allOrganizations,
            'org_type_names' => Organization_type::all(),
            'school_type' => School_type::all(),
        ]);
    }

    public function termsAndConditions()
    {
        $content = Sitecontent::where('type' ,'termsAndConditions')->first();

        return view('privacy-tos', [
                'parentOrg' => $this->parentOrg,
                'allOrganizations' => $this->allOrganizations,
                'org_type_names' => Organization_type::all(),
                'school_type' => School_type::all(),
                'allOrganizations' => $this->allOrganizations,
                'content' => $content,
            ]
        );
    }

    public function privacyPolicy()
    {
        $content = Sitecontent::where('type' ,'privacyPolicy')->first();

        return view('privacy-tos', [
                'parentOrg' => $this->parentOrg,
                'allOrganizations' => $this->allOrganizations,
                'org_type_names' => Organization_type::all(),
                'school_type' => School_type::all(),
                'content' => $content,
            ]
        );
    }

    public function quickSightDashboard()
    {
        $reqAmazon = new AmazonRequests();
        $url = $reqAmazon::amazonRegisterUserGetEmbedURL();

        if($url) {
            return Redirect::to($url);
        }

        session()->flash('app_error', 'Unable to load dashboard please try again later.');
        return back();

//        return view('quick_sight_dashboard', [
//            'parentOrg' => $this->parentOrg,
//            'org_type_names' => Organization_type::all(),
//            'school_type' => School_type::all(),
//            'url' => $url
//        ]);
    }

    public function getSubOrganization($parentOrg = null)
    {
        return User::where('parent_id', $parentOrg)
            ->where('is_deleted', '<>', 1)
            ->where('status', '<>', 0)
            ->orderBy('org_name', 'asc')
            ->pluck('org_name', 'email')->all();
    }

    public function delete_alteremail(request $request){
        $columnName= $request->input('columnName');
        if($columnName == 'email2'){
            DB::table('users')
                ->where('id',Auth::user()->id)
                ->update(['email2'=>NULL, 'confirm_code2'=>NULL]);
        }else{
            DB::table('users')
                ->where('id',Auth::user()->id)
                ->update(['email3'=>NULL, 'confirm_code3'=>NULL]);
        }
        return Response::json(['result' => 'success']);
    }

    public function alterEmailConfirmation($userId){
        $type = 'confirmAlternateEmail';
        $user = User::where('id' , $userId)->first();
        $email = EmailsTemplate::where('type' , $type)->first();

        if(!empty($user->email2) && $user->confirm_code2 != '1'){
            $user->confirm_code = $user->confirm_code2;
            $body = replaceBodyContents($email->body ,$email->type, $user);
             \Mail::to($user->email2)->send(new UsersDefaultMail($body ,$email->title));
        }
        if(!empty($user->email3) && $user->confirm_code3 != '1'){
            $user->confirm_code = $user->confirm_code3;
            $body = replaceBodyContents($email->body ,$email->type, $user);
             \Mail::to($user->email3)->send(new UsersDefaultMail($body ,$email->title));
        }
    }

    public function delete_domain(request $request){
        $columnName= $request->input('columnName');
        if($columnName == 'domain_1'){
            DB::table('users')
                ->where('id',Auth::user()->id)
                ->update(['domain_1'=>NULL, 'domain_1_confirm_code'=>NULL]);
        }else{
            DB::table('users')
                ->where('id',Auth::user()->id)
                ->update(['domain_2'=>NULL, 'domain_2_confirm_code'=>NULL]);
        }
        return Response::json(['result' => 'success']);
    }

    public function signUp($user_type=null){
        if (!Auth::check()) {
            return view('sign_up', [
                'parentOrg' => $this->parentOrg,
                'allOrganizations' => $this->allOrganizations,
                'org_type_names' => Organization_type::all(),
                'school_type' => School_type::all(),
                'user_type'=>$user_type
            ]);
        } else {
            if (Auth::user()->user_role == 'volunteer') {
                return redirect()->to('/volunteer/');
            } else {
                return redirect()->to('/organization/');
            }
        }
    }

    public function signIn(){
       if (!Auth::check()) {
            return view('sign_in', [
                'parentOrg' => $this->parentOrg,
                'allOrganizations' => $this->allOrganizations,
                'org_type_names' => Organization_type::all(),
                'school_type' => School_type::all()
            ]);
        } else {
            if (Auth::user()->user_role == 'volunteer') {
                return redirect()->to('/volunteer/');

            } else {
                return redirect()->to('/organization/');
            }
        }
    }

    public function viewForgetPassword(){

        if (!Auth::check()) {
            return view('forgot_password', [
                'subOrg' => $this->subOrg,
                'parentOrg' => $this->parentOrg,
                'allOrganizations' => $this->allOrganizations,
            ]);
        } else {
            if (Auth::user()->user_role == 'volunteer') {
                return redirect()->to('/volunteer/');

            } else {
                return redirect()->to('/organization/');
            }
        }
    }
    public function userDetails(){
        $ids=DB::table('users')->select('id', 'first_name', 'last_name' , 'middle_initial','gender','birth_date','zipcode','email','contact_number as phone_number','email2 as alternate_email','org_name as organization')->where('user_role','=','volunteer')->get();
        // $ids=DB::table('users')->select('id', 'first_name', 'last_name' , 'middle_initial','gender','birth_date','zipcode','email','contact_number','email2','org_name')->limit(50)->get();
        foreach($ids as $key1=>$id){
            $response= DB::table('group_members')->select('group_id')->where('user_id','=',$id->id)->get();
            $sdata =[];
            $opp_mem =[];
            // $new = true;
            foreach($response as $key2=>$res){
                $data=DB::table('groups')->select('name')->where('id','=',$res->group_id)->get();
                // $id->group[]=(object)$data;
                if(count($data)>0){
                 $sdata[]= $data[0]->name;
                }

             }
             $opp_org = Opportunity_member::select('users.org_name' ,'opportunity_members.org_id')
                 ->join('users', 'users.id', '=', 'opportunity_members.org_id')
                ->where('opportunity_members.user_id',$id->id)
                ->where('users.user_role','=', "organization")
                ->groupBy('org_id')->get();

            foreach($opp_org as $key3=>$res3){
                $opp_mem[]=$res3->org_name;
            }
            $sdata = implode(",",$sdata);
             $opp_mem = implode(",",$opp_mem);
             $id->group=$sdata;
             $id->organization=$opp_mem;

        }
        return response()->json($ids);
    }

    public function signUpAffiliate($id){

        if (Auth::check()) {
            return redirect('/');
            }

        $last_word_start = strrpos($id, '-')+1; 
        $last_word = substr($id, $last_word_start);
        $user = User::find($last_word);   
        if($user){
            return view('affiliateSignUp',[
                'id'=>$last_word,
                'img'=>$user->logo_img,
                'parentOrg' => $user->parentOrg,
                'allOrganizations' => $user->allOrganizations,
                'org_type_names' => Organization_type::all(),
                'school_type' => School_type::all()]);
        }else{
            return redirect('/');
             }
    }

    public function affiliateLandingPage($id){
        $data=explode("@",base64_decode($id));
        if (Auth::check()) {
            return redirect('/');
            }
        $groups=Group::where("creator_id",$data[0])->get();
        if($groups){
              $user = User::where('id',$data[0])->first();
            return view('affiliate_landing_page',["groups"=>$groups,"userId"=>$data[1],"userDetail"=>$user]);
        }else{
            return redirect('/');
        }
       


    }

    public function addGroupInvitation(Request $req)
    {
        $group_id = $req->get('id');
        $groupName = Group::select('name')->where('id', $group_id)->first();
        $last_id=0;
        $session = $req->get('userId');
        $userName= User::select('first_name','last_name')->where('id',$session)->first();
        $users=DB::table('group_members')->where('group_id',$group_id)->get();
        $temp=array();
        $a=0;
        if($users)
        {
            foreach($users as $u)
            {
                $temp[$a] = $u->user_id;
                $a = $a + 1;
            }

            $list_user_id=$temp;
        }
        else
        {
            $list_user_id=0;
            $last_id=0;
        }
        if ($list_user_id!=0 && $list_user_id!=NULL) {
      
            foreach ($list_user_id as $value) {
                $user_id = $value;
                if ($session != $value) {
                    $getUserData = User::where('id', $value)->get();
                    $groupData = Group::where('id', $group_id)->get();
                    $GroupMemberData = Group_member::where('user_id', $value)->where('group_id', $group_id)->where('is_deleted', 0)->first();
                    if ($GroupMemberData === null) {
                        $group_member = new Group_member;
                        $group_member->group_id = $group_id;
                        $group_member->user_id = $value;
                        $group_member->role_id = 2;
                        $group_member->status = 1;
                        $group_member->save();

                        $last_id = $group_member->id;

                        $alerts = new Alert;
                        $alerts->receiver_id = $value;
                        $alerts->sender_id = $session;
                        $alerts->sender_type = $getUserData[0]->user_role;
                        $alerts->alert_type = 10;
                        $alerts->contents = $userName->first_name.' '.$userName->last_name.' sent you an invitation to join ' . $groupData[0]->name . ' group';
                        $alerts->is_checked = 0;
                        $alerts->related_id = $last_id;
                        $alerts->save();
                    } else {
                        $checkAlert = Alert::where('receiver_id', $value)
                            ->where('sender_id', $session)
                            ->where('sender_type', $getUserData[0]->user_role)
                            ->where('alert_type', 10)
                            ->where('contents', $userName->first_name.' '.$userName->last_name.' sent you an invitation to join ' . $groupData[0]->name . ' group')
                            ->where('related_id', $GroupMemberData->id)
                            ->first();
                        if ($checkAlert !== null) {
                             return response()->json(['error'=>'You have already requested to join <b>'.$groupName->name.'</b> group','redirect'=>"/organization/group"]);
                        }
                        
                        $alerts = new Alert;
                        $alerts->receiver_id = $value;
                        $alerts->sender_id = $session;
                        $alerts->sender_type = $getUserData[0]->user_role;
                        $alerts->alert_type = 10;
                        $alerts->contents = $userName->first_name.' '.$userName->last_name.' sent you an invitation to join ' . $groupData[0]->name . ' group';
                        $alerts->is_checked = 0;
                        $alerts->related_id = $GroupMemberData->id;
                        $alerts->save();
                    }
                }
            }
        }
        if ($list_user_id==0 && $last_id == 0) {   
            return response()->json(['error'=>'Already added']);
       } else {    
        
           return response()->json(['success'=>'You have successfully requested to join '.$groupName->name.' group']);
        }
            return response()->json(['redirect'=>"/organization/group"]);
    }
    // public function orgDetails(){
    //     $sdata =[];
    //     $opp_mem=[];
    //     $opp_userId=[];
    //     $opp_opporId=[]; 
    //      $ids=DB::table('users')->select('id','org_name','user_role')->where('user_role','=','organization')->get();
    //         foreach($ids as $id){
    //         $response= DB::table('opportunity_members')->select('oppor_id')->where('org_id','=',$id->id)->get();
    //         foreach($response as $res){
    //          $sdata= DB::table('opportunity_members')->select('oppor_id','user_id')->where('oppor_id','=',$res->oppor_id)->get();
    //          foreach($sdata as $key=>$d){
    //            $opp_org = Opportunity_member::select('users.user_name' , 'opportunity_members.user_id','opportunity_members.oppor_id' )
    //            ->join('users', 'users.id', '=', 'opportunity_members.org_id')
    //            ->where('opportunity_members.user_id',$d->user_id)
    //            ->where('opportunity_members.oppor_id',$res->oppor_id)
    //            ->get();  
    //         }
    //         foreach($opp_org as $key3=>$res3){
    //             $opp_mem[]=$res3->user_name;
    //             $opp_userId[]=$res3->user_id;
    //             $opp_opporId[]=$res3->oppor_id;
    //             $id->volunteer=$opp_mem;
    //             $id->userId=$opp_userId;
    //             $id->opporId=$opp_opporId;
    //         }
    //      } 
        
    // //      echo "<pre>";
    // //         print_r($id);
    // //         echo "</pre>";
    // //    die;
    //     //  print_r($id->vol); 
    //    }

    //    // return response()->json($ids);
    // }


}
