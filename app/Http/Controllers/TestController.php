<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Chat;
use App\Customattributes;
use App\Events\DynamicGroupMembersUpdateEvent;
use App\Friend;
use App\Group;
use App\Group_member;
use App\Http\Controllers\Organization\GroupCtrl;
use App\Http\Controllers\Volunteer\CustomAttributeCtrl;
use App\Mail\UsersDefaultMail;
use App\Models\Delegate;
use App\Models\EmailsTemplate;
use App\Models\UnlistedOrgnizationOpprHour;
use App\Opportunity;
use App\Opportunity_member;
use App\Review;
use App\Selected_categoty;
use App\Services\AmazonRequests;
use App\Services\ChatService;
use App\Tracking;
use App\User;
use Aws\QuickSight\QuickSightClient;
use Aws\S3\S3Client;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use Services\Amazon\HttpApiRequest;

use Aws\Sts\StsClient;
use Aws\Exception\AwsException;


class TestController extends Controller
{
    public $where = [];
    public $whereOr = [];
    public $condition = null;

    public function operatorsUI(){

        return [
            'equal' => '=',
            'not_equal'=>  '!='  ,
            'in' => 'in',
            'not_in' => 'not_in',
            'less'=> '<' ,
            'less_or_equal' => '<='  ,
            'greater' => '>' ,
            'greater_or_equal' => '>=',
            'is_empty' => 'empty' ,
            'is_not_empty' => 'notEmpty',
            'is_null' => 'null',
            'is_not_null' => 'notNull'
        ];
    }

    public function sendUnlistedOrgEmail($unlistOrgHour){

        if($unlistOrgHour->org_email){
            $emailTemp = EmailsTemplate::where('type', 'unlistedOpportunityOrganization')->first();

            $dataArray['oppor_name'] = ucfirst($unlistOrgHour->oppor_name);
            $dataArray['org_mail'] = $unlistOrgHour->org_email;
            $dataArray['org_name'] = $unlistOrgHour->org_name;
            $path = route('home').'?auto-signup=org?o_email='.$unlistOrgHour->org_email;
            $dataArray['link'] = '<a href="'.$path.'">Start MyVoluntier</a>';
            $body = replaceBodyContents($emailTemp->body, $emailTemp->type,null,null,null,$dataArray);

            \Mail::to($unlistOrgHour->org_email)->send(new UsersDefaultMail($body, $emailTemp->title));
        }

    }

    public function index(){

        $currentUser = auth()->user();

        $state = $currentUser->state;
        $last_year = date('Y') - 1;

        $lastYear1 = $last_year.'-01-01';
        $lastYear12 = $last_year.'-12-31';

        $top_5_state_year = DB::table('users')
            ->join('opportunities', 'users.id', '=', 'opportunities.org_id')
            ->join('tracked_hours', 'opportunities.id', '=', 'tracked_hours.oppor_id')
            ->where('tracked_hours.approv_status', 1)
            ->where('tracked_hours.created_at', '>=', $lastYear1)
            ->where('tracked_hours.created_at', '<=', $lastYear12)
            ->where('users.is_deleted', '<>', 1)
            ->where('users.confirm_code', '=', 1)
            ->where('users.state', '=', "'" . $state . "'")
            ->groupBy('tracked_hours.org_id')
            ->orderBy(DB::raw('SUM(tracked_hours.logged_mins)', 'desc'))
            ->limit(5)
            ->select('tracked_hours.org_id')
            ->get();

        dd($top_5_state_year);

        $query= 'select `tracked_hours`.`org_id` from `users` inner join `opportunities` on `users`.`id` = `opportunities`.`org_id` inner join `tracked_hours` on `opportunities`.`id` = `tracked_hours`.`oppor_id` where `tracked_hours`.`approv_status` = 1 and `tracked_hours`.`created_at` >= \'2019-01-01\' and `tracked_hours`.`created_at` <= \'2019-12-31\' and `users`.`is_deleted` <> 1 and `users`.`confirm_code` = 1 and `users`.`state` = \'WB\' group by `tracked_hours`.`org_id` order by SUM(tracked_hours.logged_mins) asc limit 5';


        $results = DB::select( DB::raw($query) );



        dd($results);


        dd('ok testing');

        $opprs = Opportunity::all();

        foreach ($opprs as $oppr){
            $start =$oppr->start_at;
            $end = $oppr->end_at;

            if($start){
                if ($start == trim($start) && strpos($start, ' ') !== true) {
                    $oppr->start_at =  substr_replace($start, ' ' . substr($start, -2), -2);
                }
            }

            if($end){
                if ($end == trim($end) && strpos($end, ' ') !== true) {
                    $oppr->end_at =  substr_replace($end, ' ' . substr($end, -2), -2);
                }
            }

            //$oppr->save();
        }

        dd('done');

        $contr = new CustomAttributeCtrl();

        $contr->refreshDynamicGroupMembers(null ,'abc.com');

//        $clientSts = new StsClient([
//            'region' => 'us-east-1',
//            'version' => 'latest'
//        ]);
//
//        $roleToAssumeArn = 'arn:aws:iam::581081770241:role/myvoluntierdashboard';
//
//        $result = $clientSts->assumeRole([
//            'RoleArn' => $roleToAssumeArn,
//            'RoleSessionName' => 'amirDEV1@seersol.com'
//        ]);
//
//        $sessionIds = [
//            'sessionId'    => $result['Credentials']['AccessKeyId'],
//            'sessionKey' => $result['Credentials']['SecretAccessKey'],
//            'sessionToken'  => $result['Credentials']['SessionToken']
//        ];

//        $uri = 'https://signin.aws.amazon.com/federation?Action=getSigninToken&Session='.json_encode($sessionIds);


        // output AssumedRole credentials, you can use these credentials
        // to initiate a new AWS Service client with the IAM Role's permissions

//        $quickSiteClient = new  QuickSightClient([
//            'region'=>'us-east-1',
//            'version' => 'latest',
//            'credentials' =>  [
//                'key'    => $result['Credentials']['AccessKeyId'],
//                'secret' => $result['Credentials']['SecretAccessKey'],
//                'token'  => $result['Credentials']['SessionToken']
//            ]
//        ]);

//        $clientQuickSight = new QuickSightClient(['region'=>'us-east-1','version' => 'latest']);
//
//        $result = $clientQuickSight->registerUser([
//            'version' => 'latest', // REQUIRED
//            'AwsAccountId' => '581081770241', // REQUIRED
//            'Email' => 'amirseersol@seersol.com', // REQUIRED
//            'IamArn' => 'arn:aws:iam::581081770241:role/myvoluntierdashboard',
//            'IdentityType' => 'IAM', // REQUIRED IAM|QUICKSIGHT
//            'UserRole' => 'READER', // REQUIRED READER:AUTHOR:ADMIN
//            'SessionName' => 'amirDEV1@seersol.com', // REQUIRED
//            'Namespace' => 'default', // REQUIRED
//        ]);
//
//        dd($result);
//
//        $result2 = $clientQuickSight->getDashboardEmbedUrl([
//            'AwsAccountId' => '581081770241', // REQUIRED
//            'DashboardId' => '24dc6133-2cdc-4ce3-91ac-abc6a6858640', // REQUIRED
//            'IdentityType' => 'IAM', // REQUIRED or IAM
//            'SessionLifetimeInMinutes' => '30', // REQUIRED or IAM
//            'UndoRedoDisabled' => true, // REQUIRED or IAM
//            'ResetDisabled' => true, // REQUIRED or IAM
//        ]);
//
//        dd($result2);

//        https://signin.aws.amazon.com/federation?Action=getSigninToken&Session={%22sessionId%22:%22temporary-access-key-ID%22,%22sessionKey%22:%22temporary-secret-access-key%22,%22sessionToken%22:%22security-token%22}

        $req = new AmazonRequests();

        $request = $req::amazonRegisterUserGetEmbedURL();

        dd($request);


        $tracking_info = UnlistedOrgnizationOpprHour::first();

        $this->sendUnlistedOrgEmail($tracking_info);

        dd($tracking_info);

        $name = convertUserName('amir.seersol@gmail.com');

       dd($name);

        $data[]['enrolled_In'] = 'bscs';
        $data[]['enrolled_In'] = 'bscs123';

        Event::fire(new DynamicGroupMembersUpdateEvent($data ,'update'));

        $groupObject = new GroupCtrl();

        $data['class_one'] = 'amirShahzad';

        Event::fire(new DynamicGroupMembersUpdateEvent($data ,'add'));

        $deleted['class_one'] = 'its class 123';

        Event::fire(new DynamicGroupMembersUpdateEvent($deleted ,'delete'));

        $domains = $rules = [];
        $allVolunteersQuery = User::where('user_role' ,'volunteer');

        foreach ($domains as $index => $domain){

            if($index == 0)
                $allVolunteersQuery->where('email', 'like', "%{$domain}%");
            else
                $allVolunteersQuery->OrWhere('email', 'like', "%{$domain}%");
        }

        $allVolunteersIds = $allVolunteersQuery->pluck('id')->toArray();

        list($where, $whereOr) = self::recursiveFunction($rules);

        $query =  Customattributes::latest();

        foreach ($where as $condition) {

            $query->where('attributekey' ,$condition['key'])
                ->where('attributevalue' ,$condition['opr'],$condition['vale']);
        }

        foreach ($whereOr as $condition) {
            $query->orWhere('attributekey' ,$condition['key'])
                ->orWhere('attributevalue' ,$condition['opr'],$condition['vale']);
        }

        $customAttributeFilterIds = $query->pluck('userid')->toArray();

        $customAttributesQuery = Customattributes::whereIn('userid' ,$allVolunteersIds)->pluck('userid')->toArray();

        $attributesMatUserId = array_intersect($customAttributeFilterIds ,$customAttributesQuery);

        $dynamicMembers = User::whereIn('id' ,$attributesMatUserId)->get();

        if($dynamicMembers->count() == 0)
            $dynamicMembers = [];

        dd($dynamicMembers);

        $abc = User::whereIn('id' ,[1,2,3,4,5,6,7,8,9,0])->get();


        dd(json_decode( "",true));

        $group_name = 'Random Group ' .now();
        dd($group_name);
        $domains = ['gmail.com' ,'abc.com'];
        $allVolunteersQuery = User::where('user_role' ,'volunteer')->get();

        dd($allVolunteersQuery);
        foreach ($domains as $index => $domain){

            if($index == 0)
                $allVolunteersQuery->where('email', 'like', "%{$domain}%");
            else
                $allVolunteersQuery->OrWhere('email', 'like', "%{$domain}%");
        }

        $allVolunteersIds = $allVolunteersQuery->pluck('id')->toArray();

        $array = [

            "condition" => "AND",
            "rules" => [
                0 => [
                    "id" => "school_child",
                    "field" => "school_child",
                    "type" => "string",
                    "input" => "text",
                    "operator" => "not_equal",
                    "value" => "itc RBC canada 1",
                ],
                1 => [
                    "id" => "school_child",
                    "field" => "school_child",
                    "type" => "string",
                    "input" => "text",
                    "operator" => "not_equal",
                    "value" => "itc RBC canada 2",
                ],
                2 => [
                    "condition" => "OR",
                    "rules" => [
                        0 => [
                            "id" => "class_one",
                            "field" => "class_one",
                            "type" => "string",
                            "input" => "text",
                            "operator" => "greater",
                            "value" => "its class 3",
                        ]
                    ]
                ]
            ]
        ];

        list($where, $whereOr) = ($this->recursiveFunction($array));

        $andConditions = null;
        $orConditions = null;

        $query =  Customattributes::latest();

        foreach ($where as $condition) {

            $query->where('attributekey' ,$condition['key'])
                ->where('attributevalue' ,$condition['opr'],$condition['vale']);
        }

        foreach ($whereOr as $condition) {
            $query->orWhere('attributekey' ,$condition['key'])
                ->orWhere('attributevalue' ,$condition['opr'],$condition['vale']);
        }

       $customAttributeFilterIds = $query->pluck('userid')->toArray();

        $customAttributesQuery = Customattributes::whereIn('userid' ,$allVolunteersIds)->pluck('userid')->toArray();

        $attributesMatUserId = array_intersect($customAttributeFilterIds ,$customAttributesQuery);

        dd($attributesMatUserId);
    }

    public function recursiveFunction($array){


        $operators = $this->operatorsUI();

        foreach ($array as $key => $value) {

            if($key == 'condition'){
                $this->condition = $value;
            }

            elseif($key == 'rules') {

                $dt = current($value);

                $new = isset($dt['rules']) ? $dt['rules'] : $value;

                foreach ($new as $keyRule => $valueRule) {

                    if(isset($valueRule['condition']))
                        $this->recursiveFunction($valueRule);

                    else if(isset($valueRule['field'])) {

                        if ($this->condition == 'AND') {
                            $this->where[] = [
                              'key' => $valueRule['field'],
                              'opr' => $operators[$valueRule['operator']],
                              'vale' => $valueRule['value'],
                            ];
                        } else
                            $this->whereOr[] = [
                                'key' => $valueRule['field'],
                                'opr' => $operators[$valueRule['operator']],
                                'vale' => $valueRule['value'],
                            ];
                    }
                }
            }
        }

        return  [$this->where,$this->whereOr];

    }

    public function addTestOpprData($opprId){

        if(env('APP_ENV') == 'production'){
            session()->flash('app_error', 'You cannot add test data in production environment.');
            return back();
        }

        $opprtunity = Opportunity::find($opprId);

        $someVolonteers = User::activeUser()->where('user_role' ,'volunteer')->get()->random(3);

        foreach ($someVolonteers as $ind => $volonteer){

            $this->joinToOpportunity($opprtunity ,$volonteer ,$ind);
        }

        session()->flash('app_message', 'Some opportunity impact test data has been inserted.');
        return back();
    }

    public function addTestGroupsData(){

        if(env('APP_ENV') == 'production'){
            session()->flash('app_error', 'You cannot add test data in production environment.');
            return back();
        }

        $faker = Faker::create();

        $randomUser = DB::table('users')->inRandomOrder()->first();

        $currentUser = auth()->user();

        $data = [
            "creator_id" => $randomUser->id,
            "name" => $faker->name(),
            "logo_img" => asset('img/org/001.png'),
            "banner_image" => asset('img/org/001.png'),
            "back_img" => null,
            "description" => $faker->text(),
            "contact_name" => $currentUser->getFullNameVolunteer(),
            "contact_email" => $currentUser->email,
            "contact_phone" => $currentUser->contact_number,
            "is_deleted" => 0,
            "status" => 1,
            "is_public" => 1,
            "is_share_able" => 0,
            "auto_accept_join" => 1,
            "private_passcode" => null
        ];

        $group = Group::create($data);

        $opportunities = Opportunity::get()->pluck('id')->random(3)->toArray();

        $someVolonteers = User::activeUser()->where('user_role' ,$currentUser->user_role)->get()->random(2);

        $this->joinGroupByUser($group ,$currentUser ,1);

        foreach ($someVolonteers as $ind => $volonteer){
            $this->joinGroupByUser($group ,$volonteer ,0);
        }

        for ($i=0; $i < 5; $i++){

            $groupData = $group->getAttributes();

            unset($groupData['id']);


            $groupData['name'] =$faker->name() ;
            $groupData['logo_img'] = asset('img/org/001.png');
            $groupData['banner_image'] = asset('img/org/001.png');

            $groupData['description'] = $faker->text();

            $newGroup = Group::create($groupData);

            $this->joinGroupByUser($newGroup ,$currentUser ,1);

            $someVolonteers = User::activeUser()->where('user_role' ,$currentUser->user_role)->get()->random(2);

            foreach ($someVolonteers as $ind => $volonteer){
                $this->joinGroupByUser($newGroup ,$volonteer ,0);
            }

        }

        foreach ($group->users as $ind => $volonteer){

            $opprtunity = Opportunity::find($opportunities[array_rand($opportunities, 1)]);

            $this->joinToOpportunity($opprtunity ,$volonteer ,$ind);
        }

        session()->flash('app_message', 'Group impact testing data inserted.');
        return back();

    }


    public function addTestOrgData($user_id){

        if(env('APP_ENV') == 'production'){
            session()->flash('app_error', 'You cannot add test data in production environment.');
            return back();
        }

        $opportunities = Opportunity::active()->get()->pluck('id')->random(3)->toArray();
        $someVolonteers = User::activeUser()->where('user_role' ,'volunteer')->get()->random(3);


        for ($i = 1 ; $i < 5; $i++) {

            $opp = Opportunity::find($opportunities[array_rand($opportunities, 1)]);

            $cats = [1,2,3,4,5,6,7,8,12,15,16,17,19,20,21];

            $data = $opp->getAttributes();

            $faker = Faker::create();

            unset($data['id'] ,$data['category_id']);

            $data['title'] = $faker->company;
            $data['org_id'] = $user_id;
            $data['contact_name'] = $faker->name;
            $data['is_deleted'] = 0;

            // create new Order based on Post's data
            $newOppr = Opportunity::create($data);

            Selected_categoty::create(['opportunities_id'=> $newOppr->id ,'category_id' =>$cats[array_rand($cats)]]);

            $this->joinToOpportunity($newOppr ,$someVolonteers->first() ,$i ,$user_id);
        }


        foreach ($someVolonteers as $ind => $volonteer){

            $opprtunity = Opportunity::find($opportunities[array_rand($opportunities, 1)]);

            $this->joinToOpportunity($opprtunity ,$volonteer ,$ind ,$user_id);
        }


        session()->flash('app_message', 'Impact testing data inserted.');
        return back();
    }

    public function addTestVolImpactData($user_id){

        if(env('APP_ENV') == 'production'){
            session()->flash('app_error', 'You cannot add test data in production environment.');
            return back();
        }

        $opportunities = Opportunity::active()->get()->pluck('id')->random(3)->toArray();
        $someVolonteers = User::activeUser()->where('user_role' ,'volunteer')->get()->random(3);


        $faker = Faker::create();

        for ($i = 1 ; $i < 5; $i++) {

            $opp = Opportunity::find($opportunities[array_rand($opportunities, 1)]);

            $this->joinToOpportunity($opp , Auth::user() , $i ,$user_id);

            $org_id = User::where('user_role' ,'organization')->first()->id;

            $review = new Review;
            $review->review_from = $org_id;
            $review->review_to = $user_id;
            $review->mark = rand(1,5);
            $review->comment = $faker->text();

            $review->save();
        }


        foreach ($someVolonteers as $ind => $volonteer){

            $friend = new Friend();
            $friend->status = 2;
            $friend->user_id = $user_id;
            $friend->friend_id = $volonteer->id;
            $friend->save();
            $opp = Opportunity::find($opportunities[array_rand($opportunities, 1)]);

            $this->joinToOpportunity($opp , $volonteer , $i);
        }

        //Volunteer’s hours by Opportunity Type


        session()->flash('app_message', 'volunteer impact testing data inserted.');
        return back();
    }

    public function trackHoursTesting(){

        $tracks = new Tracking();

        $faker = Faker::create();

        $currentUser = auth()->user();

        $tracks->oppor_name = $faker->name();

        $tracks->is_designated = 0;

        $tracks->started_time = '10:00';
        $tracks->ended_time = '12:00';
        $tracks->logged_mins = $logged_mins = 120;
        $tracks->logged_date = now()->format('d-m-Y');
        $tracks->selectd_timeblocks = '';
        $tracks->description = 'testing data insertion';
        $tracks->is_deleted = 0;
        $tracks->approv_status = 0;
        $tracks->save();

        $add_activity = new Activity;
        $add_activity->user_id = $currentUser->id;
        $add_activity->oppor_id = 0;
        $add_activity->oppor_title = $faker->name();
        $add_activity->content = "You Updated Logged Hours to ".$logged_mins." mins on Opportunity ";
        $add_activity->type = Activity::ACTIVITY_ADD_HOURS;
        $add_activity->save();
    }

    public function joinToOpportunity($opprtunity ,$user ,$ind ,$org_id = null){

        $orgId =  $opprtunity->organization ? $opprtunity->organization->id : User::where('user_role' ,'organization')->first()->id;

        $org_id = $org_id ? $org_id : $orgId ;

        $opp_mem = new Opportunity_member;
        $opp_mem->oppor_id = $opprtunity->id;
        $opp_mem->user_id = $user->id;
        $opp_mem->org_id = $org_id;
        $opp_mem->save();


        $tracks = new Tracking;
        $tracks->volunteer_id = $user->id;
        $tracks->oppor_id = $opprtunity->id;
        $tracks->link = 1;

        $tracks->oppor_name = is_null($opprtunity->title) ? 'testing data test oppr name here' :  $opprtunity->title;
        $tracks->org_id = $org_id;
        $tracks->started_time = '08:00';
        $tracks->ended_time = '02:00';
        $tracks->logged_mins = 60*rand(10,15);
        $tracks->logged_date = $ind == 0 ? Carbon::now()->addDay(1) :Carbon::now()->subDays(rand(5,10));
        $tracks->description = 'testing data completely from auto addition';
        $tracks->selectd_timeblocks = '18:00,19:00,20:00';
        $tracks->approv_status = 1;
        $tracks->save();


        $add_activity = new Activity;
        $add_activity->user_id = $user->id;
        $add_activity->oppor_id = $opprtunity->id;
        $add_activity->oppor_title = $tracks->oppor_name;
        $add_activity->content = "You Updated Logged Hours to ".$tracks->logged_mins." mins on Opportunity ";
        $add_activity->type = Activity::ACTIVITY_ADD_HOURS;
        $add_activity->save();

    }


    public function joinGroupByUser($group ,$user ,$isAdmin){

        $isChatFound = Chat::where('group_id' ,$group->id)->where('user_id' ,$user->id)->first();

        if($isChatFound) {

            $chatService = new ChatService();
            $logo = $group->logo_img === null ? asset('img/org/001.png') : $group->logo_img;
            $chatId = $chatService->createChat($group->name, $logo);
            $chatService->addUserToChat(Auth::user(), $chatId, $group->name, 'groups');

            $chat = new Chat();
            $chat->chat_id = $chatId;
            $chat->user_id = $user->id;
            $chat->group_id = $group->id;
            $chat->type = 'groups';
            $chat->save();
        }


        $group_member = new Group_member;
        $group_member->group_id = $group->id;
        $group_member->user_id = $user->id;
        $group_member->role_id = $isAdmin ? Group::GROUP_ADMIN : Group::GROUP_MEMBER;
        $group_member->status = Group_member::APPROVED;
        $group_member->save();

    }


    public function bannersImagesNames(){

         return [
                 0 => "1518508220images1.jpg",
                 1 => "1518510750groupbanner.jpg",
                 2 => "1518512112groupbanner.jpg",
                 3 => "1518774997phpThumb_generated_thumbnail.jpg",
                 4 => "1518775288phpThumb_generated_thumbnail.jpg",
                 5 => "1518777671Physical health.jpg",
                 6 => "1518794937443fac75923828e886a79f13d3a0026e.jpg",
                 7 => "15198029831518775408443fac75923828e886a79f13d3a0026e.jpg",
                 8 => "15198770871518770057443fac75923828e886a79f13d3a0026e.jpg",
                 9 => "1520950593parakeet-yellow-parakeet-green-parakeet-bird-51161.jpeg",
                 10 => "15210341721280x720-data_out_142_69171268-oxford-wallpapers.jpg",
                 11 => "1521034359pexels-photo-485853 - Copy.jpeg",
                 12 => "1521036889oxford-1378641_1280.jpg",
                 13 => "1521113147IMG_6401.JPG"
             ];
    }

    public function logosImagesNames(){

        return [
            0 => "1518507152download.jpg",
            1 => "1518507206download.jpg",
            2 => "1518507267download.jpg",
            3 => "1518507322images1.jpg",
            4 => "1518507385images2.jpg",
            5 => "1518508220bluemarble.jpg",
            6 => "1518510749images.jpg",
            7 => "1518774996imagethisoup.jpg",
            8 => "1518775288imagethisoup.jpg",
            9 => "1518777670loader2.gif",
            10 => "1518794937geneva.jpg",
            11 => "1520950561pexels-photo (1).jpg",
            12 => "1520950593pexels-photo (1).jpg",
            13 => "1520950724pexels-photo-89873.jpeg",
            14 => "1520950737pexels-photo-89873.jpeg",
            15 => "1520952803pexels-photo.jpg",
            16 => "1520960093cat-pet-animal-domestic-104827.jpeg",
            17 => "1520960738pexels-photo-218925.jpeg",
            18 => "1521113147skyline-new-york-empire-state-building-skyscraper-39695.jpeg",
            19 => "1521127549pexels-photo-239581.jpeg",
            20 => "1521127885food-restaurant-menu-asia.jpg",
            21 => "1521128219pexels-photo-239581.jpeg",
            22 => "1521717728burger.jpg",
        ];
    }
}


