<?php
namespace App\Http\Controllers\Volunteer;

use App\Chat;
use App\Group;
use App\Group_member;
use App\Services\ChatService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Response;
use App\Customattributes;
use App\Models\Attributelookup;

class CustomAttributeCtrl extends Controller {
   
    public function saveCustomAttribute($id = null, Request $request) {
        $userId = (isset(Auth::user()->id)) ? Auth::user()->id : "";
        if($id){
            $id = base64_decode($id);
            $customAttribute = Customattributes::find($id)->toArray();
            $atrributelookups = Attributelookup::where('is_deleted', '<>', 1)->orderBy('attributename', 'ASC')->get();
            return view('components.auth.modal_body_cust_attr', [ 'atrributelookups'=>$atrributelookups,'customAttribute' => $customAttribute]);
            
        }
        $subOrgid = "";
        if (!empty($userId)) {
            $attributekey = $request->input('attributekey');
            $attributevalue = $request->input('attributevalue');
            $attributeid = (!empty($request->input('cust_attr_id')))? base64_decode($request->input('cust_attr_id')): '';
            if (empty($attributeid)) {
                $customAttribute = new Customattributes();
            } else {
                $customAttribute = Customattributes::find($attributeid);
                if(empty($customAttribute)){
                    return Response::json(['result' => 'invalid_id']);
                }
            }

            if($attributekey == 'domain'){

                $oldDomain = $customAttribute->attributevalue;
                $newDomain= $attributevalue;

                $this->refreshDynamicGroupMembers($oldDomain ,$newDomain);
            }

            $customAttribute->userid = $userId;
            $customAttribute->attributekey = $attributekey;
            $customAttribute->attributevalue = $attributevalue;
            $customAttribute->save();

            session()->flash('app_message', 'Data has been saved successfully.');
        }
        return Response::json(['result' => 'success']);
    }

    public function refreshDynamicGroupMembers($oldDomain ,$newDomain){

        $member = auth()->user();
        $chatService = new ChatService();

        $oldGroups = $oldDomain ? Group::where('is_public' ,2)->where('applicable_domains', 'LIKE', "%{$oldDomain}%")->get() : [];

        //Remove from groups
        foreach ($oldGroups as $group) {

            $memberOld = Group_member::where([
                'user_id' => $member->id,
                'group_id' => $group->id,
                'is_deleted' => 0,
            ])->first();

            if ($memberOld) {

                $chatsGroup = Chat::where('group_id', $group->id)->first();

                $chatService->removeUserFromChat($chatsGroup->chat_id, $member->user_name, 'groups');

                $memberOld->update(['is_deleted' => 1]);
            }

        }

        $newGroups = $newDomain ? Group::where('is_public' ,2)->where('applicable_domains', 'LIKE', "%{$newDomain}%")->get() : [];

        //Add into groups
        foreach ($newGroups as $group) {

            $chatsGroup = Chat::where('group_id', $group->id)->first();

            $isAlreadyMember = Group_member::where([
                'user_id' => $member->id,
                'group_id' => $group->id,
                'is_deleted' => 0,
            ])->first();

            if(!$isAlreadyMember){

                $group_member = new Group_member;
                $group_member->group_id = $group->id;
                $group_member->user_id = $member->id;

                $group_member->role_id = Group::GROUP_MEMBER;
                $group_member->status = Group_member::APPROVED;
                $group_member->save();

                $chatService->addUserToChat($member, $chatsGroup->chat_id, $group->name, 'groups');
            }
        }
    }
    
    public function deleteCustomAttribute($id = null) {

        $userId = (isset(Auth::user()->id)) ? Auth::user()->id : "";

        if($id){
            $id = base64_decode($id);
            $customAttribute = Customattributes::find($id);
            $customAttribute->userid = $userId;

            if($customAttribute->attributekey == 'domain'){
                $oldDomain = $customAttribute->attributevalue;
                $this->refreshDynamicGroupMembers($oldDomain ,null);
            }

            $customAttribute->is_deleted = '1';
            $customAttribute->save();
            session()->flash('app_message', 'Data has been deleted successfully.'); 
        }else {
            session()->flash('app_error', 'Something went wrong.');
        }

        return redirect()->to('/volunteer/profile#customAttributes');
        
    }

}
