<?php

namespace App\Http\Controllers\Volunteer;

use App\Tracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Support\Facades\Response;
use App\Service_project;
use App\Service_project_hour;
use App\Share;
use App\Customattributes;
use App\Models\Attributelookup;

class ServiceProjectCtrl extends Controller {
    
    public function checkOrgEmailExist(Request $request) {

        return User::where('email', $request->orgEmail)->where('is_deleted', '0')->first();
    }
    //Method to create/update Service Project
    public function createServiceProject($id = null, Request $request) {

        $trackHoursIds = [];
        if (!empty($id)) {
            $id = base64_decode($id);
            $serviceProject = Service_project::find($id);
            $serviceProjectHours = Service_project_hour::where('service_project_id', $id)->get();


            if (!empty($serviceProjectHours)) {

                foreach ($serviceProjectHours as $val) {
                    $trackHoursIds[] = $val->track_hours_id;
                }
            }
            $msg = 'You have successfully updated service project.';
        } else {
            $msg = 'You have successfully created service project.';
        }
        if (!empty($request->input('title')) && !empty($request->input('start_date'))) {
            $serviceProjectId = $this->saveServiceProject($request, $id, $trackHoursIds);
            session()->flash('app_message', $msg);
            return redirect()->to('/volunteer/profile#serviceProject');
            //return redirect()->to('/volunteer/edit-service-project/' . base64_encode($serviceProjectId));
        }

        if ($id == null) {
            return view('volunteer.createServiceProject', ['service_project_id' => $id, "tracks" => $this->getTrackHours()]);
        } else {

            return view('volunteer.createServiceProject', [ 'serviceProject' => $serviceProject,
                "tracks" => $this->getTrackHours(),
                'trackHoursIds' => $trackHoursIds,
                'service_project_id' => $id
            ]);
        }
    }

    //Method to get volunteer track hours 
    private function getTrackHours($volunteerId = "") {

        $volunteerId = (empty($volunteerId)) ? Auth::user()->id : $volunteerId;

        $my_orgs = Tracking::where('volunteer_id', $volunteerId)->where('is_deleted', '<>', 1)->where('approv_status', '=', 1)->groupBy('org_id')->pluck('org_id')->toArray();
        $org_info = User::whereIn('id', $my_orgs)->get();
        $type_script = array();
        $tracks = Tracking::where('volunteer_id', $volunteerId)->where('is_deleted', '<>', 1)->where('approv_status', '=', 1)->orderBy('logged_date', 'desc')->get()->groupBy('org_id')->toArray();

        foreach ($org_info as $info) {
            $type_script[$info->id]['org_name'] = $info->org_name;
            $type_script[$info->id]['org_logo'] = $info->logo_img;
            $type_script[$info->id]['contact_number'] = $info->contact_number;
            $type_script[$info->id]['zipcode'] = $info->zipcode;
            $type_script[$info->id]['address'] = $info->city . ', ' . $info->state . ' ' . $info->zipcode . ', ' . $info->country;
            $type_script[$info->id]['email'] = $info->email;
            $type_script[$info->id]['primary_contact'] = '';
            $type_script[$info->id]['track_info'] = $tracks[$info->id];
        }
        return $type_script;
    }

    //Method to save volunteer service project
    private function saveServiceProject($request, $id, $trackHoursIds = array()) {

        if ($id == null) {
            $serviceProject = new Service_project;
        } else {
            $serviceProject = Service_project::find($id);
        }

        $serviceProject->volunteer_id = Auth::user()->id;
        $serviceProject->title = trim($request->input('title'));
        $serviceProject->start_date = date('Y-m-d', strtotime(str_replace("-", "/", $request->input('start_date'))));
        $serviceProject->end_date = date('Y-m-d', strtotime(str_replace("-", "/", $request->input('end_date'))));
        $serviceProject->description = trim($request->input('description'));
        $serviceProject->outcome = trim($request->input('outcome'));
        $serviceProject->save();

        $serviceProjectId = $serviceProject->id;
        if (!empty($request->input('service_hours'))) {
            $serviceHours = $request->input('service_hours');
            $totalHours = 0;
            $totalOpp = [];
            foreach ($serviceHours as $val) {
                $data = explode('~', $val);
                if (!empty($trackHoursIds) && in_array($data[2], $trackHoursIds)) {
                    $spHours = Service_project_hour::where('track_hours_id', $data[2])->get()[0];
                } else {
                    $spHours = new Service_project_hour;
                }

                $spHours->service_project_id = $serviceProjectId;

                $spHours->orgnization_id = $data[0];
                $spHours->oppor_id = $data[1];
                $spHours->track_hours_id = $data[2];
                $spHours->hours = $data[3];

                $totalHours = $totalHours + $spHours->hours;
                $totalOpp[$data[1]] = 1;
                $spHours->save();
                $newTrackHours[] = $data[2];
            }

            $serviceProjectEdit = Service_project::find($serviceProjectId);
            $serviceProjectEdit->total_hours = $totalHours;
            $serviceProjectEdit->number_opp = count($totalOpp);
            $serviceProjectEdit->save();

            $mapTrackHours = array_diff($trackHoursIds, $newTrackHours);
            if (!empty($mapTrackHours)) {
                foreach ($mapTrackHours as $trId) {
                    $delete_existing = Service_project_hour::where('track_hours_id', $trId)->delete();
                }
            }
        }
        return $serviceProjectId;
    }

    //Method to view Service Project
    public function viewServiceProject($id = null) {

        if (!empty($id)) {
            $id = base64_decode($id);
            $serviceProject = Service_project::find($id);
            if (empty($serviceProject)) {
                session()->flash('app_error', 'Something went wrong. We could not find record.');
                return redirect()->to('/volunteer/profile#serviceProject');
            }
            $serviceProjectHours = Service_project_hour::where('service_project_id', $id)->get();

            $trackHoursIds = [];
            $trackOrgoIds = [];
            if (!empty($serviceProjectHours)) {

                foreach ($serviceProjectHours as $val) {
                    $trackHoursIds[] = $val->track_hours_id;
                    $trackOrgoIds[$val->orgnization_id] = $val->orgnization_id;
                }
            }
            return view('volunteer.viewServiceProject', [ 'serviceProject' => $serviceProject,
                "tracks" => $this->getTrackHours(),
                'serviceProject' => $serviceProject,
                'trackHoursIds' => $trackHoursIds,
                'service_project_id' => $id,
                'trackOrgoIds' => $trackOrgoIds,
                'viewpage' => true
            ]);
        } else {
            session()->flash('app_error', 'Something went wrong.');
            return redirect()->to('/volunteer/profile#serviceProject');
        }
    }

    //Method to delete Service Project
    public function deleteServiceProject($id = null) {

        if (!empty($id)) {
            $id = base64_decode($id);
            $serviceProject = Service_project::find($id);
            $serviceProject->is_deleted = 1;
            $serviceProject->save();
            //Service_project::where('id', $id)->where('volunteer_id', Auth::user()->id)->delete();
            //Service_project_hour::where('service_project_id', $id)->delete();
            session()->flash('app_message', 'You have successfully deleted service project.');
        } else {
            session()->flash('app_error', 'Something went wrong.');
        }

        return redirect()->to('/volunteer/profile#serviceProject');
    }

    //Method to view Service Project
    public function sharedServiceProjectPage($shared_id, $code) {

        $shared_info = Share::where('id', '=', $shared_id)->first();

        if ($shared_info === null) {
            return redirect()->to('/');
        } else {
            $id = $shared_info->shared_user_id;

            $today = date("Y-m-d");

            if ($shared_info->is_deleted == 0 && $shared_info->expired_at > $today && strcmp($shared_info->codes, $code) == 0) {
                $id = explode('~', base64_decode($shared_info->codes))[1];

                $serviceProject = Service_project::find($id);

                if (empty($serviceProject)) {
                    session()->flash('app_error', 'Something went wrong. We could not find record.');
                    return redirect()->to('/');
                }
                $serviceProjectHours = Service_project_hour::where('service_project_id', $id)->get();

                $trackOrgoIds = [];
                if (!empty($serviceProjectHours)) {

                    foreach ($serviceProjectHours as $val) {
                        $trackHoursIds[] = $val->track_hours_id;
                        $trackOrgoIds[$val->orgnization_id] = $val->orgnization_id;
                    }
                }
                return view('shareServiceProjectPage', [ 'serviceProject' => $serviceProject,
                    "tracks" => $this->getTrackHours($serviceProject->volunteer_id),
                    'serviceProject' => $serviceProject,
                    'trackHoursIds' => $trackHoursIds,
                    'service_project_id' => $id,
                    'trackOrgoIds' => $trackOrgoIds,
                    'viewpage' => true
                ]);
            } else {
                return redirect()->to('/');
                //return view('home', ['org_type_names' => Organization_type::all(), 'school_type' => School_type::all()]);
            }
        }
    }

    public function validatedServiceProject(Request $request) {
        $title = trim($request->input('title'));
        $id = trim($request->input('serviceid'));
        $serviceProject = Service_project::where('title', $title)
                ->where('is_deleted', '0')
                ->where('volunteer_id', Auth::user()->id)
                ->where('id', '!=', $id)
                ->get();

        if (count($serviceProject) == '0') {
            echo 'success';
        } else {
            echo "Title is already exist for other service project.";
        }
        exit;
    }

}
