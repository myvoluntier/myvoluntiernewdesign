<?php

namespace App\Http\Controllers\Volunteer;

use App\Activity;
use App\Alert;
use App\Mail\SharingProfileMail;
use App\Models\EmailsTemplate;
use App\Models\UnlistedOrgnizationOpprHour;
use App\Opportunity;
use App\Tracking;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnlistedOrganizationOpprHourController extends Controller
{
    public function storeUnlistedOrgHours($data){

        $unlistedOrgHour =  UnlistedOrgnizationOpprHour::create($data);

        return $unlistedOrgHour;
    }

    public function copyUnlistedOrgHours($unlistHour ,$user){

        $new_opp = new Opportunity;
        $new_opp->org_id = $user->id;
        $new_opp->org_type = $user->org_type;
        $new_opp->title = 'General Volunteering '. $unlistHour->org_name;
        $new_opp->description = $unlistHour->oppor_name;
        $new_opp->end_date = $unlistHour->end_date;
        $new_opp->type = 1;
        $new_opp->save();

        $tracking = new Tracking;
        $tracking->volunteer_id = $unlistHour->volunteer_id;
        $tracking->org_id = $user->id;
        $tracking->oppor_id = $new_opp->id;
        $is_link = $new_opp->type;

        if($is_link == 1){
            $tracking->link = 1;
        }
        $tracking->oppor_name = $unlistHour->oppor_name;
        $tracking->started_time = $unlistHour->started_time;
        $tracking->ended_time = $unlistHour->ended_time;
        $tracking->logged_mins = $unlistHour->logged_mins;
        $tracking->logged_date = $unlistHour->logged_date;
        $tracking->description = $unlistHour->description;
        $tracking->selectd_timeblocks = $unlistHour->selectd_timeblocks;
        $tracking->confirm_code = $unlistHour->confirm_code;
        $tracking->save();

        $add_activity = new Activity;
        $add_activity->user_id = $unlistHour->volunteer_id;
        $add_activity->oppor_id = $new_opp->id;
        $add_activity->oppor_title = $new_opp->title;
        $add_activity->content = "You Added ".$unlistHour->logged_mins." mins on Opportunity ";
        $add_activity->type = Activity::ACTIVITY_ADD_HOURS;
        $add_activity->save();

        $alert = new Alert;
        $alert->receiver_id = $user->id;
        $alert->sender_id = $unlistHour->volunteer_id;
        $alert->sender_type = 'volunteer';
        $alert->related_id = $tracking->id;
        $alert->alert_type = Alert::ALERT_TRACK_CONFIRM_REQUEST;
        $alert->contents = ' asking confirm logged hours on your opportunity.';
        $alert->save();

        $this->sendConfirmEmail($user->email, $tracking->id, $tracking->confirm_code ,$unlistHour->volunteer_id);
    }

    public function sendConfirmEmail($to, $track_id, $confirm_code ,$volunteer_id){

        $user = User::find($volunteer_id);

        $fromName = $user->first_name . ' ' . $user->last_name ;

        $data['track_id'] = $track_id;
        $data['confirm_code'] = $confirm_code;

        $email = EmailsTemplate::where('type' , 'trackConfirm')->first();

        $body = replaceBodyContents($email->body ,$email->type, $user, null, null,$data);
        \Mail::to($to)->send(new SharingProfileMail($body ,$fromName.' '.$email->title ,$user->email, $fromName));

        return 1;
    }

}
