<?php

namespace App\Http\Controllers\Volunteer;

use App\Activity;
use App\Alert;
use App\CommonModel;
use App\Group;
use App\Mail\UsersDefaultMail;
use App\Models\EmailsTemplate;
use App\Opportunity_member;
use App\Organization_type;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Opportunity;
use Auth;
use App\Tracking;
use App\Opportunity_category;
use App\Selected_categoty;
use Illuminate\Support\Facades\DB;
use App\Partner;

class OpportunityCtrl extends Controller
{

    public function getOpportunityTimeInfo($id){

        $oppr_info = Opportunity::find($id);

        return view('volunteer.layout._opporInfoHtml' ,compact('oppr_info'));
    }

	public function viewOpportunity(Request $request)
	{

		$user = Auth::user();
		$oppr_types = Opportunity_category::where('is_deleted',0)->orderBy('name', 'asc')->get();
		$org_types = Organization_type::all();
		$sar_op_type = array();
		$sar_og_type = array();
		$current_loc = $this->getAddress($request);
		if($current_loc != 'error'){
			$search_addr['city'] = $current_loc['city'];
			if($current_loc['country'] == "US"){
				$search_addr['country'] = "United States";

				$search_addr['state'] = $this->getAbbreviation($current_loc['
                ']);
			}
			else{
				$search_addr['country'] = $current_loc['country'];
				$search_addr['state'] = $current_loc['region'];
			}

			$latlng = explode(',',$current_loc['loc']);
			$search_addr['lat'] = $latlng[0];
			$search_addr['lng'] = $latlng[1];
			$today = date("Y-m-d");

			$active_oppr = Opportunity::where('state','like',$search_addr['state'])->where('city','like',$search_addr['city'])
				->where('type',1)->where('is_deleted','<>','1')
			    ->orderBy('created_at','desc')->get();
			$oppr_count_by_id = Opportunity::where('state','like',$search_addr['state'])->where('city','like',$search_addr['city'])
				->where('type',1)->where('is_deleted','<>','1')
			    ->groupBy('category_id')->selectRaw('category_id, count(*) as count')->get();

			$count_oppr = array();
			foreach ($oppr_count_by_id as $oppr_c){
				$count_oppr[$oppr_c->category_id] = $oppr_c->count;
			}
			foreach ($oppr_types as $ot){
				if(array_key_exists($ot->id,$count_oppr))
					$sar_op_type[$ot->id]['count'] = $count_oppr[$ot->id];
				else
					$sar_op_type[$ot->id]['count'] = 0;
				$sar_op_type[$ot->id]['name'] = $ot->name;
			}

			$org_count_by_id = Opportunity::where('state','like',$search_addr['state'])->where('city','like',$search_addr['state'])
				->where('type',1)->where('is_deleted','<>','1')
			    ->groupBy('org_type')->selectRaw('org_type, count(*) as count')->get();

			$count_org = array();
			foreach ($org_count_by_id as $org_c){
				$count_org[$org_c->org_type] = $org_c->count;
			}
			foreach ($org_types as $og){
				if(array_key_exists($og->id,$count_org))
					$sar_og_type[$og->id]['count'] = $count_org[$og->id];
				else
					$sar_og_type[$og->id]['count'] = 0;
				$sar_og_type[$og->id]['name'] = $og->organization_type;
			}
			$circle_radius = 3959;
			$max_distance = 20;
		}else{
			$search_addr['country'] = "United States";
			$search_addr['city'] = Auth::user()->city;
			$search_addr['state'] = Auth::user()->state;
			$search_addr['lat'] = Auth::user()->lat;
			$search_addr['lng'] = Auth::user()->lng;
			$today = date("Y-m-d");
			$active_oppr = DB::table('opportunities')->where('state','like',$search_addr['state'])->where('city','like',$search_addr['city'])
				->where('type',1)->where('is_deleted','<>','1')->orderBy('created_at','desc')->get();
                $oppr_count_by_id = Opportunity::where('state','like',$search_addr['state'])->where('city','like',$search_addr['city'])
				->where('type',1)->where('is_deleted','<>','1')
			    ->groupBy('category_id')->selectRaw('category_id, count(*) as count')->get();

			$count_oppr = array();
			foreach ($oppr_count_by_id as $oppr_c){
				$count_oppr[$oppr_c->category_id] = $oppr_c->count;
			}
			foreach ($oppr_types as $ot){
				if(array_key_exists($ot->id,$count_oppr))
					$sar_op_type[$ot->id]['count'] = $count_oppr[$ot->id];
				else
					$sar_op_type[$ot->id]['count'] = 0;
				$sar_op_type[$ot->id]['name'] = $ot->name;
			}

			$org_count_by_id = Opportunity::where('state','like',$search_addr['state'])->where('city','like',$search_addr['city'])
				->where('type',1)->where('is_deleted','<>','1')
			    ->groupBy('org_type')->selectRaw('org_type, count(*) as count')->get();

			$count_org = array();
			foreach ($org_count_by_id as $org_c){
				$count_org[$org_c->org_type] = $org_c->count;
			}
			foreach ($org_types as $og){
				if(array_key_exists($og->id,$count_org))
					$sar_og_type[$og->id]['count'] = $count_org[$og->id];
				else
					$sar_og_type[$og->id]['count'] = 0;
				$sar_og_type[$og->id]['name'] = $og->organization_type;
			}
		}

		$comments = Tracking::with('user')->where('volunteer_id','=',$user->id)->get();

        $user = DB::table('users')->where('id', '=', $user->id)->first();

        $orgList = User::select('id', 'org_name')
            ->where('user_role', 'organization')
            ->where('is_deleted', '<>', 1)
            ->orderBy('id', 'AES')
            ->get()
            ->toArray();

		return view('volunteer.opportunities',
			['orgList'=>$orgList,'search_addr'=>$search_addr,'op_type'=>$sar_op_type,'og_type'=>$sar_og_type,'opprs'=>$active_oppr,'page_name'=>'vol_oppor']);
	}

	public function getSearchResult(Request $request){

        $active_oppr = null;
        $include_expired = $request->input('include_expired') ;

        $start = $request->input('start_date');
        $end = $request->input('end_date');

        $location = $request->input('location');

        $oppor_types = $request->input('opp_types');
        $org_types = $request->input('org_types');

        $partner_ids = $request->input('partner_ids');
        $keyword = $request->input('keyword');
        $today = now()->format('Y-m-d');

        $partnerOrg = $partners = [];
        if (!empty($partner_ids)) {
            $partners = Partner::select('partners.partner_org_id')
                ->where('is_deleted', '<>', '1')
                ->whereIn('org_id', $partner_ids)
                ->get()->toArray();
        }

        if(!empty($partners)){
            foreach ($partners as $partnerVal) {
                $partnerOrg[$partnerVal['partner_org_id']] = $partnerVal['partner_org_id'];
            }
        }

        $start_date = empty($start)  ? $today : dateYmdFormat($start ,'m-d-Y');
        $end_date = empty($end) ? now()->addDay(30)->format('Y-m-d') : dateYmdFormat($end ,'m-d-Y');


        if ($location != null) {

            $pos = explode(',', $location);
            $city = $pos[0];

            $state = '';
            if (array_key_exists(1, $pos)) {
                $state = str_replace(' ', '', $pos[1]);
            }

        } else {
            $city = "";
            $state = "";
        }

        $query = Opportunity::where('state', 'like', '%' . $state . '%')
            ->where('type', 1)
            ->where('is_deleted', '<>', '1');

        if (!empty($partnerOrg)) {
            $query->whereIn('org_id', $partnerOrg);
        }

        $allOpprSql = null;

        if(!empty($oppor_types)){
            if(!empty($org_types)){
                $allOpprSql = $query
                    ->whereIn('org_type',$org_types)
                    ->whereHas('opprSelectedCategories', function ($query) use ($oppor_types) {
                        $query->whereIn('category_id', $oppor_types);
                    });
            }
            else{
                $allOpprSql = $query->whereHas('opprSelectedCategories', function ($query) use ($oppor_types) {
                    $query->whereIn('category_id', $oppor_types);
                });;
            }
        }else{
            if(!empty($org_types)){
                $allOpprSql = $query->whereIn('org_type',$org_types);
            }
            else
                $allOpprSql = $query;
        }

        if($include_expired == '0'){
            if(empty($end)){
                $allOpprSql->where(function ($q) use ($start_date, $end_date) {
                    //$q->where('start_date', '>=', $start_date);
                    $q->where('end_date', '>=', $start_date);
                });
            }else{
               $allOpprSql->where(function ($q) use ($start_date, $end_date) {
                $q->where('start_date', '>=', $start_date);
                $q->where('end_date', '<=', $end_date);
            }); 
            }
        }

        $active_oppr = $allOpprSql
            ->where(function ($query) use ($keyword) {
                $query->where("title", "LIKE", "%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%")
                    ->orWhere("activity", "LIKE", "%$keyword%");
            })->orderBy('zipcode', 'desc')->get();

        if (empty($partnerOrg) && !empty($partner_ids)) {
            $active_oppr = array();
            $oppCount = 0;
        } else {
            $oppCount = $active_oppr->count();
        }

		return Response::json([
		    'oppr' => $active_oppr,
            "oppr_count" =>$oppCount
        ]);
	}

    public function getLocation($address)
    {
        /*get location from address*/
        $address = str_replace(' ', '+', $address);
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $address . "&key=AIzaSyA3n1_WGs2PVEv2JqsmxeEsgvrorUiI5Es";
        $result = json_decode(file_get_contents($url), true);
        if ($result['results'] == []) {
            return 'error';
        } else
            return $result['results'][0]['geometry']['location'];
    }

	public function viewOpportunityPage($id)
	{
        ini_set('memory_limit', '2056M');

		$user = Auth::user();
        $oppr = Opportunity::find($id);
        $impactsData = [];
        $timeBlock = [];

		if($user->user_role == 'volunteer') {

            $member = Opportunity_member::where('oppor_id',$id)->where('user_id',Auth::user()->id)->first();

            if($member != null){
                if($member->status == 1){
                    $is_member = 1;
                }elseif ($member->status == 0){
                    $is_member = 2;
                }
            }else{
                $is_member = 0;
            }

            $friends = DB::table('friends')->where('user_id',$user->id)->get();

            $friend=array();

            $a = 0;

            foreach($friends as $f) {
                $friend[$a]=$f->friend_id;
                $a=$a+1;
            }
        }

        $all_frnds = DB::table('users')
            ->join('friends', 'users.id', '=', 'friends.friend_id')
            ->where('friends.user_id', Auth::user()->id)
            ->where('friends.is_deleted', '<>', 1)
            ->where('friends.status', 2)
            ->where('users.is_deleted', '<>', 1)
            ->where('users.confirm_code', 1)
            ->select('users.*', 'friends.*')
            ->get();

		$opportunityCategory = Opportunity_category::where('is_deleted',0)->orderBy('name', 'asc')->get();

		$cat_array = array();
		$all_categories = Selected_categoty::where('opportunities_id',$id)->get()->toArray();

		if(!empty($all_categories)) {
			foreach ($all_categories as $key => $cat) {
				array_push($cat_array, $cat['category_id']);
			}
		}

        $tracked_hours_obj = Tracking::confirmedNotDel()
            ->where('oppor_id' ,$oppr->id)
            ->groupBy('oppor_id')
            ->selectRaw('oppor_id, sum(logged_mins)/60 as logged_hours')
            ->orderBy('logged_hours' ,'asc')
            ->first();

        $impactsData['tracked_hours'] = $tracked_hours_obj ? $tracked_hours_obj->logged_hours : '0';

        $impactsData['ranking_all'] = $this->getOpprRankingHours($oppr);

        $impactsData['ranking_categories'] = $this->getOpprRankingCategory($oppr);

        list($impactsData['volunteers'],
            $impactsData['volunteers_avg'],
            $impactsData['volunteers_pie_zip'],
            $impactsData['volunteers_pie_age'],
            $impactsData['top5Chart']) = $this->getOpprVolonteers($oppr);
        list(
            $impactsData['this_month_chart'],
            $impactsData['last_month_chart'],
            $impactsData['last_6month_chart'],
            $impactsData['year_date_chart']) = $this->getOpprChartsData($oppr);

        $impactsData['top_5_vol_chart']['heading'] = array_keys($impactsData['top5Chart']);
//        $impactsData['top_5_vol_chart'] = $impactsData['top5Chart'];

//        dd($impactsData);
//        unset($impactsData['top5Chart'] ,$impactsData['volunteers_pie']);

        $oppr->selected_cat = $cat_array;

        $return_data = [
            'opportunity_category' => $opportunityCategory,
            'all_friends' => $all_frnds,
            'oppr_info' => $oppr,
            'page_name' => '',
            'user' => $user,
            'impactsData' => $impactsData,
            'groups_list' => Group::activeGroup()->get(),
        ];

        if ($user->user_role == 'volunteer') {
            $return_data['is_member'] = $is_member;
            $return_data['friend'] = $friend;
            $return_data['timeBlocks'] = $this->getTimeBlocks($oppr);
        }
		return view('organization.viewOpportunity', $return_data);
	}

	public function getTimeBlocks($opportunity){

        $start_time = "00:00";
        $end_time = "24:00";

	    if($opportunity->start_at && $opportunity->end_at) {

            $start_time = date("H:i", strtotime($opportunity->start_at));
            $end_time = date("H:i", strtotime($opportunity->end_at));
        }

        $timeBlock = [];

        $tNow = $tStart = strtotime($start_time);
        $tEnd = strtotime($end_time);

        while ($tNow <= strtotime('-30 minutes', $tEnd)) {
            $value= date("h:i A", $tNow) . '-' . date("h:i A", strtotime('+30 minutes', $tNow));
            $key  = date("H:i", $tNow);

            $timeBlock[$key] = $value;

            $tNow = strtotime('+30 minutes', $tNow);
        }

        return $timeBlock;
    }

	public function getOpprChartsData($oppr){

        $data['this_month'] = [];
        $data['last_month'] = [];
        $data['last_6month'] = [];
        $data['year_date'] = [];

        $thisMonthFirstDay = date('Y-m-d', strtotime("first day of this month"));
        $currentDay = date('Y-m-d');


        $lastMonthFirstDay = date('Y-m-d', strtotime("first day of -1 month"));
        $lastMonthLastDay = date('Y-m-d', strtotime("last day of -1 month"));


        $last6MonthFirstDay = date('Y-m-d', strtotime("first day of -6 month"));

        $lastYearFirstDay = date('Y-m-d', strtotime("first day of -12 month"));


        $query = Tracking::confirmedNotDel()
            ->groupBy('logged_date')
            ->where('oppor_id' ,$oppr->id)
            ->selectRaw('oppor_id,logged_date, sum(logged_mins)/60 as logged_hours')
            ->orderBy('logged_date' ,'asc');


        $tracked_hours_this = (clone $query)->whereBetween('logged_date', [$thisMonthFirstDay ,$currentDay])->get();


        foreach ($tracked_hours_this as $chart){
            
            if(isset($data['this_month'][dateDDMMYY($chart->logged_date)]))
                $data['this_month'][dateDDMMYY($chart->logged_date)] = $data['this_month'][dateDDMMYY($chart->logged_date)]+ $chart->logged_hours;
            else
                $data['this_month'][dateDDMMYY($chart->logged_date)] = $chart->logged_hours;
        }

        $tracked_hours_last = (clone $query)->whereBetween('logged_date', [$lastMonthFirstDay ,$lastMonthLastDay])->get();

        foreach ($tracked_hours_last as $chart){

            if(isset($data['last_month'][dateDDMMYY($chart->logged_date)]))
                $data['last_month'][dateDDMMYY($chart->logged_date)] = $data['last_month'][dateDDMMYY($chart->logged_date)]+ $chart->logged_hours;
            else
                $data['last_month'][dateDDMMYY($chart->logged_date)] = $chart->logged_hours;
        }

        $tracked_hours_last6 = (clone $query)->whereBetween('logged_date', [$last6MonthFirstDay ,$currentDay])->get();

        foreach ($tracked_hours_last6 as $chart){
            if(isset($data['last_6month'][dateMMYY($chart->logged_date)]))
                $data['last_6month'][dateMMYY($chart->logged_date)] = $data['last_6month'][dateMMYY($chart->logged_date)]+ $chart->logged_hours;
            else
                $data['last_6month'][dateMMYY($chart->logged_date)] = $chart->logged_hours;
        }

        $tracked_hours_yearD = (clone $query)->whereBetween('logged_date', [$lastYearFirstDay ,$currentDay])->get();

        foreach ($tracked_hours_yearD as $chart){
            if(isset($data['year_date'][dateMMYY($chart->logged_date)]))
                $data['year_date'][dateMMYY($chart->logged_date)] = $data['year_date'][dateMMYY($chart->logged_date)]+ $chart->logged_hours;
            else
                $data['year_date'][dateMMYY($chart->logged_date)] = $chart->logged_hours;
        }


        return [$data['this_month'] , $data['last_month'] ,$data['last_6month'] ,$data['year_date']];

    }

    public function getOpprVolonteers($oppr){

        $data = [];
        $pieChartZip = [];
        $pieChartAge = [];
        $top5Chart = [];
        $pChartAge['13 – 17'] = $pChartAge['18 – 22'] =$pChartAge['23 – 30'] = $pChartAge['30 and older'] = 0;
        $totalHours = 0;
        $all_volunteersIds = [];

        foreach ($oppr->users as $index => $user) {

            $object = [];
            $object['name'] = $user->getFullNameVolunteer();
            $object['hours'] = ($user->tracksOpportunityHours($oppr->id)->sum('logged_mins')) / 60;
            $object['location'] = $user->fullLocation();
            $object['zip_code'] = $user->zipcode ;

            if($object['hours'] > 0 && !in_array( $user->id,$all_volunteersIds)){

                $top5Chart[$user->id.$object['name']] = $object['hours'];

                $data[] = $object;

                $totalHours = $totalHours + $object['hours'];

                $all_volunteersIds[] = $user->id;
            }
        }


        $all_volunteers_zip = User::whereIn('id' ,$all_volunteersIds)
            ->groupBy('zipcode')
            ->selectRaw('zipcode, count(*) as total')
            ->orderBy('total' ,'asc')
            ->get();


        $all_volunteers_age = User::whereIn('id' ,$all_volunteersIds)
            ->select( [
                \DB::raw("floor((DATEDIFF(CURRENT_DATE, STR_TO_DATE(birth_date, '%m/%d/%Y'))/365)) as age"),
                \DB::raw("count(*) as total")
            ])
            ->groupBy('age')
            ->orderBy('total' ,'asc')
            ->get();

        foreach ($all_volunteers_zip as $user){

            $pChartZip['name'] = "$user->zipcode";
            $pChartZip['y'] = (int)$user->total;
            $pChartZip['color'] = rand_color();

            $pieChartZip[] = $pChartZip;

        }

        foreach ($all_volunteers_age as $user){

            if($user->age >= 13 && $user->age <= 17 )
                $pChartAge['13 – 17'] +=  (int)$user->total;

            if($user->age >= 18 && $user->age <= 22 )
                $pChartAge['18 – 22'] +=  (int)$user->total;

            if($user->age >= 23 && $user->age <= 30 )
                $pChartAge['23 – 30'] +=  (int)$user->total;

            if($user->age >= 30  )
                $pChartAge['30 and older'] +=  (int)$user->total;
        }

        ksort($pChartAge);

        foreach ($pChartAge as $index => $value){

            $array['name'] = "$index";
            $array['y'] = (int)$value;
            $array['color'] = rand_color();

            $pieChartAge[] = $array; //age group pie
        }

        array_multisort(array_column($data, 'hours'), SORT_DESC, $data);
        arsort($top5Chart);

        $top5Chart = array_slice($top5Chart, 0, 5, true);
//        $data = array_slice($data, 0, 10, true); or pagination os datatable

        $avg = count($data) > 0 ?  number_format($totalHours / count($data),1) : 0;
       
        return [$data , $avg ,$pieChartZip ,$pieChartAge ,$top5Chart];
    }

    public function getOpprRankingCategory($oppr){

        $selectedCats = Selected_categoty::where('opportunities_id' ,$oppr->id)->get();

        $response = [];

        foreach ($selectedCats as $category){

            $final['ranking_category'] = ($category->category->name);
            $final['rank'] = 0;

            $allOpportunities = Opportunity::whereHas("opprSelectedCategories", function($q) use ($category){
                $q->where("category_id","=",$category->category_id);
            })->pluck('id');


            $tracked_hours = Tracking::confirmedNotDel()
                ->whereIn('oppor_id' ,$allOpportunities)
                ->groupBy('oppor_id')
                ->selectRaw('oppor_id, sum(logged_mins)/60 as logged_hours')
                ->orderBy('logged_hours' ,'desc')
                ->get();


            foreach ($tracked_hours as $index => $track) {
                if($track->oppor_id == $oppr->id) {
                    $final['rank'] = $index + 1;
                    break;
                }
            }

            $response[] = $final;
        }

        if(count($response) == 0) {

            $final['ranking_category'] = 'Opportunity';
            $final['rank'] = 0;
            
            $response [] = $final;
        }


        return $response;
    }

    public function getOpprRankingHours($oppr){

        $tracked_hours = Tracking::confirmedNotDel()
            ->groupBy('oppor_id')
            ->selectRaw('oppor_id, sum(logged_mins)/60 as logged_hours')
            ->orderBy('logged_hours' ,'desc')
            ->get();

        $rank = 0;

        foreach ($tracked_hours as $index => $track) {
            if($track->oppor_id == $oppr->id) {
                $rank = $index + 1;
                break;
            }
        }

        return $rank;
    }

	public function calculateOpprImpactsData($oppr){}

    public function joinOpportunity(Request $request)
    {
        $passCode = $request->input('passcode');

        $user_id = $request->input('user_id');
        $oppor_id = $request->input('oppor_id');
        $oppor_title = $request->input('oppor_title');
        $my_id = Auth::user()->id;

        $opportunity = Opportunity::find($oppor_id);
        $receiver_id = $opportunity->org_id;

        $dob = date('Y-m-d', strtotime(Auth::user()->birth_date));
        $birthdate = new \DateTime($dob);
        $today = new \DateTime('today');
        $age = $birthdate->diff($today)->y;

        if ($opportunity->min_age > $age) {
            session()->flash('app_error', 'Minimum age require to join this opportunity is (' . $opportunity->min_age . ' years) .');
            return back();
        }

        $oppor_member = new Opportunity_member;
        $oppor_member->oppor_id = $oppor_id;
        $oppor_member->user_id = $user_id;

        if ($opportunity->auto_accept == 1) {
            if ($opportunity->private_passcode != $passCode) {
                session()->flash('app_error', 'Pass Code is invalid to join Opportunity.');
                return back();
            }
            $oppor_member->status = 1;
        } else {
            $oppor_member->status = 0;
        }
        $oppor_member->org_id = Opportunity::find($oppor_id)->org_id;
        $oppor_member->save();

        $activity_data = new Activity;
        $activity_data->user_id = $user_id;
        $activity_data->oppor_id = $oppor_id;
        $activity_data->oppor_title = $oppor_title;
        $activity_data->link = 0;
//		$activity_data->content = "You Joined on Opportunity";
//		$activity_data->type = 1;
        if ($opportunity->auto_accept == 1) {
            $activity_data->content = "You Joined on Opportunity";
            $activity_data->type = 1;
        } else {
            $activity_data->content = "You Sent Join Request to Opportunity";
            $activity_data->type = 8;
        }
        $activity_data->created_at = date("Y-m-d H:i:s");
        $activity_data->updated_at = date("Y-m-d H:i:s");
        $activity_data->is_deleted = 0;
        $activity_data->save();

        if ($opportunity->auto_accept == 0) {
            $is_auto_accept = 0;
            $alert = new Alert;
            $alert->receiver_id = $receiver_id;
            $alert->sender_id = $my_id;
            $alert->sender_type = 'volunteer';
            $alert->alert_type = Alert::ALERT_JOIN_OPPORTUNITY_CONFIRM_REQUEST;
            $alert->related_id = $oppor_member->id;
            $alert->contents = ' requested to join opportunity | ' . $opportunity->title;
            $alert->save();
        } else {
            $is_auto_accept = 1;
            $alert = new Alert;
            $alert->receiver_id = $receiver_id;
            $alert->sender_id = $my_id;
            $alert->sender_type = 'volunteer';
            $alert->alert_type = Alert::ALERT_JOIN_OPPORTUNITY;
            $alert->related_id = $oppor_member->id;
            $alert->contents = 'Joined Opportunity | ' . $opportunity->title;
            $alert->save();
        }
      // $this->sendJoinOpportunityEmail($oppor_id, $is_auto_accept);
      // $this->responseJoinOpportunityEmail($oppor_id, $is_auto_accept);

        //return Response::json(['result'=>'success']);
        return redirect('volunteer/view_opportunity/' . $oppor_id);
    }
	
	public function sendJoinOpportunityEmail($oppr_id, $is_auto_accept){

	    try{

            $type = $is_auto_accept==1 ? 'JoinedOpportunity' : 'joinRequestOpportunity';
            $user = Auth::user();
            $opportunity = Opportunity::find($oppr_id);

            $email = EmailsTemplate::where('type' , $type)->first();
            $body = replaceBodyContents($email->body ,$email->type, $user, null, $opportunity);

            \Mail::to($opportunity->contact_email)->send(new UsersDefaultMail($body ,$email->title));

        } catch (\Exception $ex) {

        }

	}

	public function responseJoinOpportunityEmail($oppr_id, $is_auto_accept){

	    $user = Auth::user();
		$opportunity = Opportunity::find($oppr_id);

        $type = $is_auto_accept==1 ? 'responseJoinOpportunityAccept' : 'responseJoinOpportunityReceive';
        $email = EmailsTemplate::where('type' , $type)->first();

        if($user->join_oppr_email=='0') //if emails enabled
        {
            $body = replaceBodyContents($email->body ,$email->type, $user, null, $opportunity);
            \Mail::to($user->email)->send(new UsersDefaultMail($body ,$email->title));
        }

        if($user->join_oppr_text=='0' && $user->contact_number) //if texts enabled
        {
            $messageBody = replaceTextBodyContents($email->text_body ,$email->type ,$user , null, $opportunity);
            sendSMSByTwilio($opportunity->contact_number , $messageBody);
        }
	}

	public function findOnMap(Request $request){
		$id = $request->input('opportunity_id');
		$result = Opportunity::find($id);
		return Response::json(['result'=>$result]);
	}

	public function getAddress(Request $request){
        $ip = $request->ip();
//		$ip = '207.244.125.230'; //rodney dc fixed iP

        if(isset($ip) && $ip!='::1'){
			$details = json_decode(file_get_contents("http://ipinfo.io/".$ip."/json"),true);

			if(isset($details['loc'])){
				return $details;
			}else{
				return 'error';
			}
		}else{
			return 'error';
		}

	}

	public function getAbbreviation($name)
	{
		$states = array(
			'Alabama'=>'AL',
			'Alaska'=>'AK',
			'Arizona'=>'AZ',
			'Arkansas'=>'AR',
			'California'=>'CA',
			'Colorado'=>'CO',
			'Connecticut'=>'CT',
			'Delaware'=>'DE',
			'Florida'=>'FL',
			'Georgia'=>'GA',
			'Hawaii'=>'HI',
			'Idaho'=>'ID',
			'Illinois'=>'IL',
			'Indiana'=>'IN',
			'Iowa'=>'IA',
			'Kansas'=>'KS',
			'Kentucky'=>'KY',
			'Louisiana'=>'LA',
			'Maine'=>'ME',
			'Maryland'=>'MD',
			'Massachusetts'=>'MA',
			'Michigan'=>'MI',
			'Minnesota'=>'MN',
			'Mississippi'=>'MS',
			'Missouri'=>'MO',
			'Montana'=>'MT',
			'Nebraska'=>'NE',
			'Nevada'=>'NV',
			'New Hampshire'=>'NH',
			'New Jersey'=>'NJ',
			'New Mexico'=>'NM',
			'New York'=>'NY',
			'North Carolina'=>'NC',
			'North Dakota'=>'ND',
			'Ohio'=>'OH',
			'Oklahoma'=>'OK',
			'Oregon'=>'OR',
			'Pennsylvania'=>'PA',
			'Rhode Island'=>'RI',
			'South Carolina'=>'SC',
			'South Dakota'=>'SD',
			'Tennessee'=>'TN',
			'Texas'=>'TX',
			'Utah'=>'UT',
			'Vermont'=>'VT',
			'Virginia'=>'VA',
			'Washington'=>'WA',
			'District of Columbia'=>'MD',
			'West Virginia'=>'WV',
			'Wisconsin'=>'WI',
			'Wyoming'=>'WY'
		);

        if(isset($states[$name])){
            return $states[$name];
        }else{
            return 'MD';
        }

	}

	public function search(Request $request)
	{
		if($request->ajax() && $request->search!='')
		{
			$output="";
			$products=DB::table('opportunities')->where('title','LIKE','%'.$request->search."%")->get();
			if($products)
			{
				foreach ($products as $key => $product) 
				{
					$output.= '<li><a href="javascript:void(0)" onclick="swap(\''.$product->title.'\')" value="'.$product->title.'">'.$product->title.'</a></li>';
				}
			return Response($output);
			}
		}
	}

}


