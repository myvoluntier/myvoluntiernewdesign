<?php

namespace App\Http\Controllers\Volunteer;

use App\Chat;
use App\Events\UserChatNotifyEvent;
use App\Services\ChatService;
use App\Alert;
use App\Follow;
use App\Friend;
use App\Group;
use App\Message;
use App\NewsfeedModel;
use App\Group_member;
use App\Group_member_role;
use App\Opportunity;
use App\SelectedCategoryUserpref;
use App\Opportunity_member;
use App\Review;
use App\Tracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use App\Opportunity_category;
use App\Organization_type;
use Image;
use App\Services\NewsFeedService;
use App\Services\OrganizationViewProfileService as viewService;
use Illuminate\Database\Eloquent\Builder;
use App\Services\ChatManager;
use App\Services\ImageExif;
use App\Selected_categoty;
use App\Service_project;
use App\Customattributes;
use App\Models\Attributelookup;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class HomeCtrl extends Controller {
    
    public function checkOrgEmailExist(Request $request) {

        return User::where('email', $request->orgEmail)->where('is_deleted', '0')->first();
    }

    public function viewHome($action = null, NewsFeedService $newsFeedService) {

        $user = Auth::user();

        //---------opportunities news feed-----------
        $getOpporMembers = Opportunity_member::where('user_id', $user->id)->where('status', 1)->where('is_deleted', '<>', 1)->groupBy('oppor_id')->pluck('oppor_id')->toArray();
        $opportunities = Opportunity::whereIn('id', $getOpporMembers)->where('is_deleted', '<>', 1)->get();
        $newsFeedOppData = NewsfeedModel::whereIn('who_joined', $opportunities)
        ->orWhere('related_id', $user->id) //updated logic by seersol
        ->groupBy('who_joined','related_id','reason')
         ->orderBy('created_at', 'desc')->get();                

        //->groupBy('related_id','who_joined','reason')

        $feedNewsArrOpp = array();
        if (!empty($newsFeedOppData[0])) {
            $feedNewsArrOpp = $newsFeedService->transformNewsFeedToArrayInfo($newsFeedOppData, 'volunteer');
            $feedNewsArrOpp = $this->paginate($feedNewsArrOpp ,15);
            $feedNewsArrOpp->withPath(url()->current());
        }
    
        //--------news feed----------
        $getAllFrndId = Friend::where('user_id', $user->id)->where('status', 2)->pluck('friend_id')->toArray();
        $newsFeedData = NewsfeedModel::whereIn('who_joined', $getAllFrndId)
                        ->orWhere('related_id', $user->id) //updated logic by seersol
                        ->groupBy('who_joined','related_id','reason')
                        ->orderBy('created_at', 'desc')->get();  
        
        $feedNewsArr = array();
        if (!empty($newsFeedData[0])) {
            $feedNewsArr = $newsFeedService->transformNewsFeedToArrayInfo($newsFeedData, 'volunteer');
            $feedNewsArr = $this->paginate($feedNewsArr ,15);
            $feedNewsArr->withPath(url()->current());
        }

                        //->groupBy('related_id','who_joined','reason')
                        //->orderBy('created_at', 'desc')->get();                  
           


        // $feedNewsArr = $newsFeedService->transformNewsFeedToArrayInfo($newsFeedData, 'volunteer');
        $current_date = date("Y-m-d");
        $lastOrganitationData = User::where('user_role', 'organization')
                                  ->where('is_deleted','0')
                                  ->where('status','1')
                                  ->where('confirm_code','1')
                                  ->orderBy('id', 'desc')->limit(3)->offset(0)->get()->toArray();

        // $user_city = $user->city;
        //$nearestOpportunityData = Opportunity::where('city', $user_city)->where('end_date', '>', $current_date)->orderBy('id', 'desc')->limit(5)->offset(0)->get()->toArray();

        return view('volunteer.home', ['newsFeedData'=>$newsFeedData , 'feedNewsArr' => $feedNewsArr, 'lastOrgData' => $lastOrganitationData, 'opportunityFeeds'=>$feedNewsArrOpp, 'page_name' => 'vol_home', 'newsFeedPaginate' => $newsFeedData, 'action' => $action]);
    }


    public function viewEditAccount() {
        return view('volunteer.accountsetting', ['page_name' => '']);
    }

    public function viewProfile($id = null) {
        $user = Auth::user();
        if ($id == null) {
            $id = $user->id;
        }

        if (is_null(User::find($id))) {
            session()->flash('app_error', 'User not found in the system.');
            return back();
        }

        $keywords = Opportunity::with('users')->whereHas('users', function($query) {
                    $query->where('user_id', Auth::user()->id);
                })->get();

        if ($user->user_role == 'organization' and User::find($id)->user_role == 'volunteer') {
            $viewService = new viewService;
            return $viewService->viewProfile($id);
        }

//        $activeGroups = Group::activeGroup()->where('creator_id' ,$id)->get();

        if (User::find($id)->user_role == 'volunteer') {

            $user = User::find($id);
            $logged_hours = Tracking::where('volunteer_id', $id)->where('is_deleted', '<>', 1)->where('approv_status', 1)->sum('logged_mins');
            $logged_hours = $logged_hours / 60;

            $today = date('Y-m-d');
            $my_opportunities = Opportunity_member::where('user_id', $id)->where('status', 1)->where('is_deleted', '<>', 1)->pluck('oppor_id')->toArray();
            $opportunities = Opportunity::whereIn('id', $my_opportunities)->where('type', 1)->where('is_deleted', '<>', 1)->
                            where('end_date', '>', $today)->get();

            $groupsQuery = DB::table('groups')
                ->join('group_members', 'groups.id', '=', 'group_members.group_id')
                ->join('group_categories', 'groups.group_category_id', '=', 'group_categories.id')
                ->where('group_members.user_id', $id)
                ->where('group_members.is_deleted', '<>', 1)->where('group_members.status', Group_member::APPROVED)
                ->where('groups.is_deleted', '<>', 1)
                ->where('groups.affiliated_org_id', null)
                ->select('groups.*', 'group_members.role_id', 'group_categories.name as categoryName');

            $groups = (clone $groupsQuery)->where('is_public' ,'<>',2)->get()->toArray();
            $dynamicGroups = (clone $groupsQuery)->where('is_public' ,2)->get()->toArray();

            $affiliatedGroups = DB::table('groups')
                ->join('group_members', 'groups.id', '=', 'group_members.group_id')
                ->join('users', 'groups.affiliated_org_id', '=', 'users.id')
                ->join('group_categories', 'groups.group_category_id', '=', 'group_categories.id')
                ->where('group_members.user_id', $id)
                ->where('group_members.is_deleted', '<>', 1)
                ->where('group_members.status', Group_member::APPROVED)
                ->where('groups.is_deleted', '<>', 1)
                ->where('groups.affiliated_status', 1)
                ->select('groups.*', 'group_members.role_id', 'group_categories.name as categoryName', 'users.org_name as org_name')
                ->get()->toArray();

            $groups = (object) array_merge($groups, $affiliatedGroups);

            $friends = DB::table('users')->join('friends', 'users.id', '=', 'friends.friend_id')->where('friends.user_id', $id)->
                            where('friends.is_deleted', '<>', 1)->where('friends.status', 2)->
                            where('users.is_deleted', '<>', 1)->where('users.confirm_code', 1)->select('users.*')->get();

            $profile_info = array();
            $profile_info['is_my_profile'] = 0;
            if ($id == Auth::user()->id) {
                $profile_info['is_my_profile'] = 1;
            }
            $is_friend = Friend::where('user_id', Auth::user()->id)->where('friend_id', $id)->where('is_deleted', '<>', 1)->get()->first();
            if ($is_friend == null) {
                $profile_info['is_friend'] = 0;
            } else {
                $profile_info['is_friend'] = $is_friend->status;
            }
            $profile_info['is_volunteer'] = 1;
            $profile_info['logged_hours'] = $logged_hours;

            $my_orgs = Tracking::where('volunteer_id', $user->id)->where('is_deleted', '<>', 1)->where('approv_status', '=', 1)->groupBy('org_id')->pluck('org_id')->toArray();
            $org_info = User::whereIn('id', $my_orgs)->get();
            $type_script = array();
            $tracks = Tracking::where('volunteer_id', $user->id)->where('is_deleted', '<>', 1)->where('approv_status', '=', 1)->orderBy('logged_date', 'desc')->get()->groupBy('org_id')->toArray();

            foreach ($org_info as $info) {
                $type_script[$info->id]['org_name'] = $info->org_name;
                $type_script[$info->id]['org_logo'] = $info->logo_img;
                $type_script[$info->id]['contact_number'] = $info->contact_number;
                $type_script[$info->id]['zipcode'] = $info->zipcode;
                $type_script[$info->id]['address'] = $info->city . ', ' . $info->state . ' ' . $info->zipcode . ', ' . $info->country;
                $type_script[$info->id]['email'] = $info->email;
                $type_script[$info->id]['primary_contact'] = '';
                $type_script[$info->id]['track_info'] = $tracks[$info->id];
            }
            $type_script = $this->paginate($type_script);
            $type_script->withPath(url()->current());
            $oppr_types = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();

            $org_types = Organization_type::all();
            $sa_oppr_type = array();
            $sa_org_type = array();
            foreach ($oppr_types as $ot) {
                $sa_oppr_type[$ot->id]['name'] = $ot->name;
            }
            foreach ($org_types as $or) {
                $sa_org_type[$or->id]['name'] = $or->organization_type;
            }
            $deleted = true;
            $chat_id = DB::table('user_chat')->where('user_id', Auth::user()->id)->where('to_user_id', $id)->select('*')->first();
            if ($chat_id == null)
                $chat_id = DB::table('user_chat')->where('user_id', $id)->where('to_user_id', Auth::user()->id)->select('*')->first();
            if ($chat_id != null) {
                $deleted = false;
                if ($chat_id->status == 1 && $chat_id->user_id == Auth::user()->id)
                    $deleted = true;
                if ($chat_id->status == 2 && $chat_id->to_user_id == Auth::user()->id)
                    $deleted = true;
                $chat_id = $chat_id->chat_id;
            }
            //$comments= DB::table('tracked_hours')->where('volunteer_id',$id)->get();
            //$comments= DB::table('tracked_hours')->join('users','tracked_hours.org_id','=','users.id')->where('volunteer_id',$id)->get();
            //$comments= Tracking::with('user')->where('volunteer_id',$id)->orderBy('tracked_hours.created_at','DESC')->get();
            $comments = Tracking::with('user')->where([['volunteer_id', '=', $id], ['approv_status', '=', '1']])->orderBy('tracked_hours.created_at', 'DESC')->paginate(10);
            //dd($comments->comment);
            //dd($comments);
            //Get Service Projects
            $serviceProjects = Service_project::where('volunteer_id', $user->id)->where('is_deleted', '<>', 1)->get()->toArray();
            $customAttributes = Customattributes::where('userid', $user->id)->where('is_deleted', '<>', 1)->get()->toArray();
            $atrributelookups = Attributelookup::where('is_deleted', '<>', 1)->orderBy('attributename', 'ASC')->get();
            
            $opportunityCategory = Opportunity_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();
            $selectedCat = array();
            $all_categories = SelectedCategoryUserpref::where('user_id', $user->id)->get()->toArray();

            if (!empty($all_categories)) {
                foreach ($all_categories as $key => $cat) {
                    array_push($selectedCat, $cat['category_id']);
                }
            }
            
            return view('organization.profile', [
                'user' => $user,
                'profile_info' => $profile_info,
                'active_oppr' => $opportunities,
                'tracks' => $type_script,
                'group' => $groups,
                'dynamic_group' => $dynamicGroups,
                'friend' => $friends,
                'page_name' => 'vol_profile',
                'chat_id' => $chat_id,
                'chat_deleted' => $deleted,
                'keywords' => $keywords,
                'comments' => $comments,
                'id' => $id,
                'oppr_types' => $sa_oppr_type,
                'org_types' => $sa_org_type,
                'typeView'=>'profile',
                'serviceProjects' => $serviceProjects,
                'customAttributes' => $customAttributes,
                'atrributelookups' => $atrributelookups,
                'opportunityCategory' => $opportunityCategory,
                'selectedCat' => $selectedCat,
            ]);
        }

        else {

            $profile_info = array();
            $profile_info['is_volunteer'] = 0;
            $is_followed = Follow::where('follower_id', Auth::user()->id)->where('followed_id', $id)->where('is_deleted', '<>', 1)->get()->first();
            if ($is_followed == null) {
                $profile_info['is_followed'] = 0;
            } else {
                $profile_info['is_followed'] = 1;
            }
            $is_friend = Friend::where('user_id', Auth::user()->id)->where('friend_id', $id)->where('is_deleted', '<>', 1)->get()->first();
            if ($is_friend == null) {
                $profile_info['is_friend'] = 0;
            } else {
                $profile_info['is_friend'] = $is_friend->status;
            }
            $user = User::find($id);
            $tracks_hours = Tracking::where('org_id', $id)->where('approv_status', 1)->where('is_deleted', '<>', 1)->sum('logged_mins');
            $profile_info['tracks_hours'] = $tracks_hours / 60;

            $today = date("Y-m-d");
            $active_oppr = Opportunity::where('org_id', Auth::user()->id)->where('type', 1)->where('is_deleted', '<>', '1')->where('end_date', '>=', $today)->orderBy('created_at', 'desc')->get();

            $groups = DB::table('groups')->join('group_members', 'groups.id', '=', 'group_members.group_id')->where('group_members.user_id', $id)->
                            where('group_members.is_deleted', '<>', 1)->where('groups.is_deleted', '<>', 1)->select('groups.*', 'group_members.role_id')->get();

            $my_members = DB::table('opportunity_members')->where('opportunity_members.org_id', $id)->where('opportunity_members.is_deleted', '<>', 1)->
                            join('users', 'opportunity_members.user_id', '=', 'users.id')->select('users.*')->get();

            $members = array();

            foreach ($my_members as $m) {
                $members[$m->id] = $m;
            }

            $friends = DB::table('users')->join('friends', 'users.id', '=', 'friends.friend_id')->where('friends.user_id', Auth::user()->id)->
                            where('friends.is_deleted', '<>', 1)->where('friends.status', 2)->
                            where('users.is_deleted', '<>', 1)->where('users.confirm_code', 1)->select('users.*')->get();

            $deleted = true;
            $chat_id = DB::table('user_chat')->where('user_id', Auth::user()->id)->where('to_user_id', $id)->select('*')->first();
            if ($chat_id == null)
                $chat_id = DB::table('user_chat')->where('user_id', $id)->where('to_user_id', Auth::user()->id)->select('*')->first();
            if ($chat_id != null) {
                $deleted = false;
                if ($chat_id->status == 1 && $chat_id->user_id == Auth::user()->id)
                    $deleted = true;
                if ($chat_id->status == 2 && $chat_id->to_user_id == Auth::user()->id)
                    $deleted = true;
                $chat_id = $chat_id->chat_id;
            }

            return view('volunteer.profile', [
                'user' => $user,
                'profile_info' => $profile_info,
                'active_oppr' => $active_oppr,
                'group' => $groups,
                'members' => $members,
                'friend' => $friends,
                'page_name' => 'org_profile',
                'chat_id' => $chat_id,
                'chat_deleted' => $deleted,
                'id' => $id]);
        }
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function getTranscript(Request $request) {

        $id = $request->input('id');
        if ($request->input('start_date') == '') {
            $start_date = '1970-01-01';
        } else {
            $start_date = date("Y-m-d", strtotime($request->input('start_date')));
        }
        if ($request->input('end_date') == '') {
            $end_date = '2999-12-31';
        } else {
            $end_date = date("Y-m-d", strtotime($request->input('end_date')));
        }

        $oppor_types = $request->input('opp_types');
        $org_types = $request->input('org_types');

        if ($oppor_types != '') {
            if ($org_types != '') {
                $my_orgs = DB::table('tracked_hours')
                                ->join('opportunities', 'tracked_hours.oppor_id', '=', 'opportunities.id')
                                ->where('tracked_hours.volunteer_id', $id)
                                ->where('tracked_hours.is_deleted', '<>', 1)
                                ->where('tracked_hours.approv_status', '=', 1)
                                //->where('opportunities.type','=',$oppor_types)
                                ->where('opportunities.org_type', '=', $org_types)
                                ->whereBetween('logged_date', array($start_date, $end_date))
                                ->groupBy('tracked_hours.org_id')->pluck('tracked_hours.org_id')->toArray();
                $org_info = User::whereIn('id', $my_orgs)->get();
                $type_script = array();
                $active_opportunities = DB::table('tracked_hours')
                                ->join('opportunities', 'tracked_hours.oppor_id', '=', 'opportunities.id')
                                ->select('tracked_hours.*')
                                ->where('tracked_hours.volunteer_id', $id)
                                ->where('tracked_hours.is_deleted', '<>', 1)
                                ->where('tracked_hours.approv_status', '=', 1)
                                ->where('opportunities.org_type', '=', $org_types)
                                ->whereBetween('tracked_hours.logged_date', array($start_date, $end_date))
                                ->orderBy('tracked_hours.logged_date', 'desc')->get()->groupBy('org_id')->toArray();
                foreach ($org_info as $info) {

                    $getFilter_opportunities = $this->getSelectedCategory($active_opportunities[$info->id], $oppor_types);

                    if (!empty($getFilter_opportunities)) {
                        $type_script[$info->id]['org_name'] = $info->org_name;
                        $type_script[$info->id]['org_logo'] = $info->logo_img;
                        $type_script[$info->id]['contact_number'] = $info->contact_number;
                        $type_script[$info->id]['zipcode'] = $info->zipcode;
                        $type_script[$info->id]['address'] = $info->city . ', ' . $info->state . ' ' . $info->zipcode . ', ' . $info->country;
                        $type_script[$info->id]['email'] = $info->email;
                        $type_script[$info->id]['primary_contact'] = '';
                        $type_script[$info->id]['track_info'] = $getFilter_opportunities;
                    }
                }
            } else {
                $my_orgs = DB::table('tracked_hours')
                                ->join('opportunities', 'tracked_hours.oppor_id', '=', 'opportunities.id')
                                ->where('tracked_hours.volunteer_id', $id)
                                ->where('tracked_hours.is_deleted', '<>', 1)
                                ->where('tracked_hours.approv_status', '=', 1)
                                // ->where('opportunities.type','=',$oppor_types)
                                ->whereBetween('logged_date', array($start_date, $end_date))
                                ->groupBy('tracked_hours.org_id')->pluck('tracked_hours.org_id')->toArray();
                $org_info = User::whereIn('id', $my_orgs)->get();
                $type_script = array();
                $active_opportunities = DB::table('tracked_hours')
                                ->join('opportunities', 'tracked_hours.oppor_id', '=', 'opportunities.id')
                                ->select('tracked_hours.*')
                                ->where('tracked_hours.volunteer_id', $id)
                                ->where('tracked_hours.is_deleted', '<>', 1)
                                ->where('tracked_hours.approv_status', '=', 1)
                                ->whereBetween('tracked_hours.logged_date', array($start_date, $end_date))
                                ->orderBy('tracked_hours.logged_date', 'desc')->get()->groupBy('org_id')->toArray();

                $i = 0;
                foreach ($org_info as $info) {

                    $getFilter_opportunities = $this->getSelectedCategory($active_opportunities[$info->id], $oppor_types);
                    if (!empty($getFilter_opportunities)) {
                        $type_script[$info->id]['org_name'] = $info->org_name;
                        $type_script[$info->id]['org_logo'] = $info->logo_img;
                        $type_script[$info->id]['contact_number'] = $info->contact_number;
                        $type_script[$info->id]['zipcode'] = $info->zipcode;
                        $type_script[$info->id]['address'] = $info->city . ', ' . $info->state . ' ' . $info->zipcode . ', ' . $info->country;
                        $type_script[$info->id]['email'] = $info->email;
                        $type_script[$info->id]['primary_contact'] = '';
                        $type_script[$info->id]['track_info'] = $getFilter_opportunities;
                    }
                }
            }
        } else {
            if ($org_types != '') {
                $my_orgs = DB::table('tracked_hours')
                                ->join('opportunities', 'tracked_hours.oppor_id', '=', 'opportunities.id')
                                ->where('tracked_hours.volunteer_id', $id)
                                ->where('tracked_hours.is_deleted', '<>', 1)
                                ->where('tracked_hours.approv_status', '=', 1)
                                ->where('opportunities.org_type', '=', $org_types)
                                ->whereBetween('logged_date', array($start_date, $end_date))
                                ->groupBy('tracked_hours.org_id')->pluck('tracked_hours.org_id')->toArray();
                $org_info = User::whereIn('id', $my_orgs)->get();
                $type_script = array();
                $active_opportunities = DB::table('tracked_hours')
                                ->join('opportunities', 'tracked_hours.oppor_id', '=', 'opportunities.id')
                                ->select('tracked_hours.*')
                                ->where('tracked_hours.volunteer_id', $id)
                                ->where('tracked_hours.is_deleted', '<>', 1)
                                ->where('tracked_hours.approv_status', '=', 1)
                                ->where('opportunities.org_type', '=', $org_types)
                                ->whereBetween('tracked_hours.logged_date', array($start_date, $end_date))
                                ->orderBy('tracked_hours.logged_date', 'desc')->get()->groupBy('org_id')->toArray();
                foreach ($org_info as $info) {
                    $getFilter_opportunities = $this->getSelectedCategory($active_opportunities[$info->id], $oppor_types);
                    if (!empty($getFilter_opportunities)) {
                        $type_script[$info->id]['org_name'] = $info->org_name;
                        $type_script[$info->id]['org_logo'] = $info->logo_img;
                        $type_script[$info->id]['contact_number'] = $info->contact_number;
                        $type_script[$info->id]['zipcode'] = $info->zipcode;
                        $type_script[$info->id]['address'] = $info->city . ', ' . $info->state . ' ' . $info->zipcode . ', ' . $info->country;
                        $type_script[$info->id]['email'] = $info->email;
                        $type_script[$info->id]['primary_contact'] = '';
                        $type_script[$info->id]['track_info'] = $getFilter_opportunities;
                    }
                }
            } else {
                $my_orgs = DB::table('tracked_hours')
                                ->join('opportunities', 'tracked_hours.oppor_id', '=', 'opportunities.id')
                                ->where('tracked_hours.volunteer_id', $id)
                                ->where('tracked_hours.is_deleted', '<>', 1)
                                ->where('tracked_hours.approv_status', '=', 1)
                                ->whereBetween('logged_date', array($start_date, $end_date))
                                ->groupBy('tracked_hours.org_id')->pluck('tracked_hours.org_id')->toArray();
                $org_info = User::whereIn('id', $my_orgs)->get();
                $type_script = array();
                $active_opportunities = DB::table('tracked_hours')
                                ->join('opportunities', 'tracked_hours.oppor_id', '=', 'opportunities.id')
                                ->select('tracked_hours.*')
                                ->where('tracked_hours.volunteer_id', $id)
                                ->where('tracked_hours.is_deleted', '<>', 1)
                                ->where('tracked_hours.approv_status', '=', 1)
                                ->whereBetween('tracked_hours.logged_date', array($start_date, $end_date))
                                ->orderBy('tracked_hours.logged_date', 'desc')->get()->groupBy('org_id')->toArray();

                foreach ($org_info as $info) {
                    $getFilter_opportunities = $this->getSelectedCategory($active_opportunities[$info->id], $oppor_types);
                    if (!empty($getFilter_opportunities)) {
                        $type_script[$info->id]['org_name'] = $info->org_name;
                        $type_script[$info->id]['org_logo'] = $info->logo_img;
                        $type_script[$info->id]['contact_number'] = $info->contact_number;
                        $type_script[$info->id]['zipcode'] = $info->zipcode;
                        $type_script[$info->id]['address'] = $info->city . ', ' . $info->state . ' ' . $info->zipcode . ', ' . $info->country;
                        $type_script[$info->id]['email'] = $info->email;
                        $type_script[$info->id]['primary_contact'] = '';
                        $type_script[$info->id]['track_info'] = $getFilter_opportunities;
                    }
                }
            }
        }

        return Response::json(['oppr' => $type_script]);
    }

    public function viewImpact(Request $request) {

        $user_id = Auth::user()->id;
        $logged_hours = Tracking::where('volunteer_id', $user_id)
            ->where('is_deleted', '<>', 1)
            ->where('approv_status', 1)
            ->sum('logged_mins');

        $logged_hours = $logged_hours / 60;

        $track_by_org = Tracking::where('volunteer_id', $user_id)
            ->where('is_deleted', '<>', 1)
            ->where('approv_status', 1)
            ->groupBy('org_id')
            ->selectRaw('org_id, sum(logged_mins)/60 as logged_hours')
            ->get();

        $org_rank = array();
        $k = 0;
   
        foreach ($track_by_org as $tbo) {
            $org_rank[$k]['org_name'] = $tbo->org_name;
            $org_rank[$k]['my_hours'] = floatval($tbo->logged_hours);
            $org_rank[$k]['my_ranking'] = $this->getRanking($tbo->org_id, floatval($tbo->logged_hours));
            $k++;
        }

        foreach ($track_by_org as $tbo) {
            $org_rank[$k]['org_name'] = $tbo->org_name;
            $org_rank[$k]['my_hours'] = floatval($tbo->logged_hours);
            $org_rank[$k]['my_ranking'] = $this->getRanking($tbo->org_id, floatval($tbo->logged_hours));
            $k++;
        }

        return view('volunteer.impact', [
            'logged_mins' => $logged_hours,
            'org_ranking' => $org_rank,
            'page_name' => 'vol_impact'
        ]);
    }

    public function getRanking($org_id, $my_tracked_hours) {

        $get_volunteer_hours = Tracking::where('org_id', $org_id)->where('is_deleted', '<>', 1)
            ->where('approv_status', 1)
            ->groupBy('volunteer_id')
            ->selectRaw('volunteer_id, sum(logged_mins)/60 as logged_hours')
            ->get();

        $my_ranking = 1;
        foreach ($get_volunteer_hours as $gh) {
            if (floatval($gh->logged_hours) > $my_tracked_hours) {
                $my_ranking++;
            }
        }
        return $my_ranking;
    }

    public function getFriendInfo() {

        $user_id = Auth::user()->id;

        $my_logged_hours = Tracking::where('volunteer_id', $user_id)
                ->where('is_deleted', '<>', 1)
                ->where('approv_status', 1)
                ->sum('logged_mins');

        $my_logged_hours = $my_logged_hours / 60;

        $friends_id = Friend::where('user_id',$user_id)
                ->whereHas('friendUser', function (Builder $query) {
                    $query->where('user_role', '=', 'volunteer');
                })
                ->where('is_deleted', '<>', 1)
                ->where('status', 2)
                ->pluck('friend_id')
                ->toArray();

        $friends = User::whereIn('id', $friends_id)->get();

        $friends_name = [];

        foreach ($friends as $name) {
            $friends_name[] = $name->first_name . $name->org_name;
        }

        $tracked_hours = Tracking::where('is_deleted', '<>', 1)
                ->where('approv_status', 1)
                ->whereIn('volunteer_id', $friends_id)
                ->groupBy('volunteer_id')
                ->selectRaw('sum(logged_mins)/60 as logged_mins')
                ->pluck('logged_mins')
                ->toArray();

        $numeric_val = array();
        $i = 0;
        $j = 1;

        foreach ($tracked_hours as $tr) {
            $numeric_val[$i] = floatval($tr);
            $i++;

            if (floatval($tr) > $my_logged_hours) {
                $j++;
            }
        }

        $track_by_org = Tracking::where('volunteer_id', $user_id)
                ->where('is_deleted', '<>', 1)
//            ->whereIn('org_id', $friends_id_org)
                ->where('approv_status', 1)
                ->groupBy('org_id')
                ->selectRaw('org_id, sum(logged_mins)/60 as logged_hours')
                ->get();

        $org_hours = array();
        $k = 0;


        foreach ($track_by_org as $tbo) {
            $org_hours[$k][] = $tbo->org_name;
            $org_hours[$k][] = floatval($tbo->logged_hours);
            $k++;
        }

        $typesHours = $typesHoursGraph = [];

        $track_by_opp = Tracking::where('volunteer_id', $user_id)
            ->where('is_deleted', '<>', 1)
            ->where('approv_status', 1)
            ->groupBy('oppor_id')
            ->selectRaw('oppor_id, sum(logged_mins)/60 as logged_hours')
            ->get();


        foreach ($track_by_opp as $track) {
            $types = $track->category_names;
            $index = $types ? $types : 'untitled';
            $typesHours[$index] = isset($typesHours[$index]) ?
                $typesHours[$index]+$track->logged_hours : $track->logged_hours;
        }


        foreach ($typesHours as $name => $hour) {
            $typesHoursGraph[] = [
                $name ,(int)$hour
            ];
        }

        return Response::json([
            'friend_name' => $friends_name,
            'logged_hours' => $numeric_val,
            'opprTypesHoursGraph' => $typesHoursGraph,
            'rank' => $j,
            'org_hours' => $org_hours
        ]);
    }

    public function upload_logo(Request $request) {

        if ($request->get('image_logo')) {

            $get_user = Auth::user();
            $get_user->logo_img = base64fileUploadOnS3($get_user->logo_img, $request->image_logo ,User::S3PathLogo,$get_user->id.'logo');

            $save = $get_user->save();

            if ($save) {

                $allChatsUser = Chat::where('user_id', Auth::user()->id)->whereNotNull('to_user_id')->get();
                $chatService = new ChatService();
                $chatService->updateUserInfo($get_user);

                if ($get_user->user_role === 'organization')
                    $logo = $get_user->logo_img === null ? asset('img/org/001.png') : $get_user->logo_img;
                else {
                    $logo = $get_user->logo_img === null ? asset('img/logo/member-default-logo.png') : $get_user->logo_img;
                }

                foreach ($allChatsUser as $chat) {
                    $chatService->updateChatInfo($chat->chat_id, $get_user->user_name, $logo);
                }

                $allChatsUser = Chat::where('to_user_id', $get_user->id)->get();

                foreach ($allChatsUser as $chat) {
                    $opponent = User::find($chat->user_id);
                    $chatService->updateChatInfo($chat->chat_id, $get_user->org_name, $logo, $opponent->user_name);
                }
            }
        }

        session()->flash('app_message', 'Logo image uploaded successfully.');

        return back();
    }

    public function upload_back_img(Request $request) {

        if ($request->get('image_banner')) {
            $get_user = Auth::user();
            $get_user->back_img = base64fileUploadOnS3($get_user->back_img, $request->image_banner ,User::S3PathBan,$get_user->id.'ban');
            $get_user->save();
        }

        session()->flash('app_message', 'Back image uploaded successfully.');

        return back();
    }

    public function getAddress(Request $request) {

        $ip = $request->ip();
        $details = json_decode(file_get_contents("http://ipinfo.io/" . $ip . "/json"), true);
        if (isset($details['loc'])) {
            return $details;
        } else {
            return 'error';
        }
    }

    public function Search(Request $request) {
        $keyword = $request->input('keyword');
        $my_id = Auth::user()->id;
        $search_filter = is_null($request->input('filter')) ? '' : $request->input('filter');
        $filerts = ['v' => 'Volunteer', 'r' => 'Organization', 'g' => 'Group', 'p' => 'Opportunities'];

        if ($search_filter == '') {
            $org_result = User::where('is_deleted', '<>', 1)
                        ->where('status', 1)->where(function ($query) use ($keyword) {
                        $keyword_terms = explode(' ', $keyword);
                        foreach ($keyword_terms as $terms) {
                            $query->orWhere("org_name", "LIKE", "%$terms%")
                                    ->orWhere("first_name", "LIKE", "%$terms%")
                                    ->orWhere("last_name", "LIKE", "%$terms%")
                                    ->orWhere("brif", "LIKE", "%$terms%")
                                    ->orWhere("city", "LIKE", "%$terms%")
                                    ->orWhere("state", "LIKE", "%$terms%")
                                    ->orWhere("ein", "LIKE", "%$terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();
            $result = array();
            $i = 0;
            foreach ($org_result as $o) {
                if ($o->id != $my_id) {
                    $result[$i]['id'] = $o->id;
                    $result[$i]['group_id'] = 0;
                    if ($o->user_role == 'organization') {
                        $result[$i]['name'] = $o->org_name;
                    } else {
                        $result[$i]['name'] = $o->first_name . ' ' . $o->last_name;
                    }
                    $result[$i]['user_role'] = $o->user_role;
                    $result[$i]['logo_img'] = $o->logo_img;
                    $result[$i]['city'] = $o->city;
                    $result[$i]['state'] = $o->state;
                    $result[$i]['country'] = $o->country;
                    $result[$i]['show_address'] = $o->show_address;
                    $friend = Friend::where('user_id', $my_id)
                            ->where('friend_id', $o->id)
                            ->where('is_deleted', '<>', 1)
                            ->get()
                            ->first();

                    if ($friend == null) {
                        $result[$i]['is_friend'] = 0;
                    } else {
                        $result[$i]['is_friend'] = $friend->status;
                    }

                    $result[$i]['is_followed'] = Follow::where('follower_id', $my_id)
                            ->where('type', 'organization')
                            ->where('followed_id', $o->id)
                            ->where('is_deleted', '<>', 1)
                            ->count();
                    $i++;
                }
            }
            $grp_result = Group::where('is_deleted', '<>', 1)->where('is_public', '<>', 0)->where(function ($grp_query) use ($keyword) {
                        $grp_keyword_terms = explode(' ', $keyword);
                        foreach ($grp_keyword_terms as $grp_terms) {
                            $grp_query->orWhere("name", "LIKE", "%$grp_terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();
            $j = $i;
            foreach ($grp_result as $g) {
                if ($g->id != $my_id) {
                    $result[$j]['id'] = $g->id;
                    $result[$j]['group_id'] = $g->id;
                    $result[$j]['name'] = $g->name;
                    $result[$j]['user_role'] = 'group';
                    $result[$j]['logo_img'] = $g->logo_img;
                    $result[$j]['city'] = "";
                    $result[$j]['state'] = "";
                    $result[$j]['country'] = "";
                    //$result[$i]['show_address'] = $g->show_address ;
                    //echo "User_id".$my_id;
                    //echo "Group id".$g->id;
                    $friend = Group_member::where('user_id', $my_id)->where('group_id', $g->id)->where('status', 2)->where('is_deleted', '<>', 1)->get()->first();
                    //echo '<pre>';print_r($friend);die;
                    if ($friend == null) {
                        $result[$j]['is_friend'] = 0;
                    } else {
                        $result[$j]['is_friend'] = 1;
                    }
                    $result[$j]['is_followed'] = Follow::where('follower_id', $my_id)->where('type', 'organization')->where('followed_id', $g->id)->where('is_deleted', '<>', 1)->count();
                    $j++;
                }
            }
            $opportunity_result = Opportunity::where('is_deleted', '<>', 1)->where(function ($opportunity_query) use ($keyword) {
                        $opportunity_keyword_terms = explode(' ', $keyword);
                        foreach ($opportunity_keyword_terms as $opportunity_terms) {
                            $opportunity_query->orWhere("title", "LIKE", "%$opportunity_terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();
            $k = $j;
            foreach ($opportunity_result as $o) {
                if ($o->id != $my_id) {
                    $result[$k]['id'] = $o->id;
                    $result[$k]['group_id'] = $o->id;
                    $result[$k]['name'] = $o->title;
                    $result[$k]['user_role'] = 'opportunity';
                    $result[$k]['logo_img'] = $o->logo_img;
                    $result[$k]['city'] = $o->city;
                    $result[$k]['state'] = $o->state;
                    $result[$k]['country'] = "";
                    $friend = Group_member::where('user_id', $my_id)->where('group_id', $o->id)->where('status', 2)->where('is_deleted', '<>', 1)->get()->first();
                    if ($friend == null) {
                        $result[$k]['is_friend'] = 0;
                    } else {
                        $result[$k]['is_friend'] = 1;
                    }
                    $result[$k]['is_followed'] = Follow::where('follower_id', $my_id)->where('type', 'organization')->where('followed_id', $o->id)->where('is_deleted', '<>', 1)->count();
                    $k++;
                }
            }
        } else {
            $search_filter = explode(' ', (string) $search_filter);
            $vol_result = User::where('is_deleted', '<>', 1)
                            ->where('status', 1)
                            ->where('user_role', '=', 'volunteer')
                            ->where(function ($query) use ($keyword) {
                                $keyword_terms = explode(' ', $keyword);
                                foreach ($keyword_terms as $terms) {
                                    $query->orWhere("org_name", "LIKE", "%$terms%")
                                    ->orWhere("first_name", "LIKE", "%$terms%")
                                    ->orWhere("last_name", "LIKE", "%$terms%")
                                    ->orWhere("brif", "LIKE", "%$terms%")
                                    ->orWhere("city", "LIKE", "%$terms%")
                                    ->orWhere("state", "LIKE", "%$terms%")
                                    ->orWhere("ein", "LIKE", "%$terms%");
                                }
                            })->orderBy('created_at', 'desc')->get();

            $result = array();
            $h = 0;
            if (in_array('v', $search_filter)) {
                foreach ($vol_result as $o) {
                    if ($o->id != $my_id) {
                        $result[$h]['id'] = $o->id;
                        $result[$h]['group_id'] = 0;
                        if ($o->user_role == 'organization') {
                            $result[$h]['name'] = $o->org_name;
                        } else {
                            $result[$h]['name'] = $o->first_name . ' ' . $o->last_name;
                        }
                        $result[$h]['user_role'] = $o->user_role;
                        $result[$h]['logo_img'] = $o->logo_img;
                        $result[$h]['city'] = $o->city;
                        $result[$h]['state'] = $o->state;
                        $result[$h]['country'] = $o->country;
                        $result[$h]['show_address'] = $o->show_address;
                        $friend = Friend::where('user_id', $my_id)->where('friend_id', $o->id)->where('is_deleted', '<>', 1)->get()->first();
                        if ($friend == null) {
                            $result[$h]['is_friend'] = 0;
                        } else {
                            $result[$h]['is_friend'] = $friend->status;
                        }
                        $result[$h]['is_followed'] = Follow::where('follower_id', $my_id)->where('type', 'organization')->where('followed_id', $o->id)->where('is_deleted', '<>', 1)->count();
                        $h++;
                    }
                }
            }
            $org_result = User::where('is_deleted', '<>', 1)
                            ->where('user_role', '=', 'organization')
                            ->where('status', 1)
                            ->where(function ($query) use ($keyword) {
                                $keyword_terms = explode(' ', $keyword);
                                foreach ($keyword_terms as $terms) {
                                    $query->orWhere("org_name", "LIKE", "%$terms%")
                                    ->orWhere("first_name", "LIKE", "%$terms%")
                                    ->orWhere("last_name", "LIKE", "%$terms%")
                                    ->orWhere("brif", "LIKE", "%$terms%")
                                    ->orWhere("city", "LIKE", "%$terms%")
                                    ->orWhere("state", "LIKE", "%$terms%")
                                    ->orWhere("ein", "LIKE", "%$terms%");
                                }
                            })->orderBy('created_at', 'desc')->get();
            $i = $h;
            if (in_array('r', $search_filter)) {
                foreach ($org_result as $o) {
                    if ($o->id != $my_id) {
                        $result[$i]['id'] = $o->id;
                        $result[$i]['group_id'] = 0;
                        if ($o->user_role == 'organization') {
                            $result[$i]['name'] = $o->org_name;
                        } else {
                            $result[$i]['name'] = $o->first_name . ' ' . $o->last_name;
                        }
                        $result[$i]['user_role'] = $o->user_role;
                        $result[$i]['logo_img'] = $o->logo_img;
                        $result[$i]['city'] = $o->city;
                        $result[$i]['state'] = $o->state;
                        $result[$i]['country'] = $o->country;
                        $result[$i]['show_address'] = $o->show_address;
                        $friend = Friend::where('user_id', $my_id)->where('friend_id', $o->id)->where('is_deleted', '<>', 1)->get()->first();
                        if ($friend == null) {
                            $result[$i]['is_friend'] = 0;
                        } else {
                            $result[$i]['is_friend'] = $friend->status;
                        }
                        $result[$i]['is_followed'] = Follow::where('follower_id', $my_id)->where('type', 'organization')->where('followed_id', $o->id)->where('is_deleted', '<>', 1)->count();
                        $i++;
                    }
                }
            }
            $grp_result = Group::where('is_deleted', '<>', 1)->where('is_public', '<>', 0)->where(function ($grp_query) use ($keyword) {
                        $grp_keyword_terms = explode(' ', $keyword);
                        foreach ($grp_keyword_terms as $grp_terms) {
                            $grp_query->orWhere("name", "LIKE", "%$grp_terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();

            $j = $i;
            if (in_array('g', $search_filter)) {
                foreach ($grp_result as $g) {
                    if ($g->id != $my_id) {
                        $result[$j]['id'] = $g->id;
                        $result[$j]['group_id'] = $g->id;
                        $result[$j]['name'] = $g->name;
                        $result[$j]['user_role'] = 'group';
                        $result[$j]['logo_img'] = $g->logo_img;
                        $result[$j]['city'] = "";
                        $result[$j]['state'] = "";
                        $result[$j]['country'] = "";
                        $friend = Group_member::where('user_id', $my_id)->where('group_id', $g->id)->where('status', 2)->where('is_deleted', '<>', 1)->get()->first();
                        //todo:need to get exact status and then show the results.

                        if ($friend == null) {
                            $result[$j]['is_friend'] = 0;
                        } else {
                            $result[$j]['is_friend'] = 1;
                        }
                        $result[$j]['is_followed'] = Follow::where('follower_id', $my_id)->where('type', 'organization')->where('followed_id', $g->id)->where('is_deleted', '<>', 1)->count();
                        $j++;
                    }
                }
            }
            $opportunity_result = Opportunity::where('is_deleted', '<>', 1)->where(function ($opportunity_query) use ($keyword) {
                        $opportunity_keyword_terms = explode(' ', $keyword);
                        foreach ($opportunity_keyword_terms as $opportunity_terms) {
                            $opportunity_query->orWhere("title", "LIKE", "%$opportunity_terms%");
                        }
                    })->orderBy('created_at', 'desc')->get();
            $k = $j;

            if (in_array('p', $search_filter)) {
                foreach ($opportunity_result as $o) {
                    if ($o->id != $my_id) {
                        $result[$k]['id'] = $o->id;
                        $result[$k]['group_id'] = $o->id;
                        $result[$k]['name'] = $o->title;
                        $result[$k]['user_role'] = 'opportunity';
                        $result[$k]['logo_img'] = $o->logo_img;
                        $result[$k]['city'] = $o->city;
                        $result[$k]['state'] = $o->state;
                        $result[$k]['country'] = "";
                        $friend = Group_member::where('user_id', $my_id)->where('group_id', $o->id)->where('status', 2)->get()->first();
                        if ($friend == null) {
                            $result[$k]['is_friend'] = 0;
                        } else {
                            $result[$k]['is_friend'] = 1;
                        }
                        $result[$k]['is_followed'] = Follow::where('follower_id', $my_id)->where('type', 'organization')->where('followed_id', $o->id)->where('is_deleted', '<>', 1)->count();
                        $k++;
                    }
                }
            }
        }
        if ($search_filter == '') {
            $search_filter = array();
        }
        $result = $this->paginate($result);
        $result->withPath(url()->current());
        return view('organization.search', ['keyword' => $keyword, 'result' => $result, 'filter' => $search_filter, 'page_name' => '']);
    }

    public function followOrganization(Request $request) {
        $id = $request->input('id');
        $my_id = Auth::user()->id;
        $is_exist = Follow::where('follower_id', $my_id)->where('type', 'organization')->where('followed_id', $id)->get()->first();
        if ($is_exist == null) {
            $follower = new Follow;
            $follower->follower_id = $my_id;
            $follower->type = 'organization';
            $follower->followed_id = $id;
            $follower->save();
        } else {
            $is_exist->is_deleted = 0;
            $is_exist->save();
        }

        $alert = new Alert;
        $alert->receiver_id = $id;
        $alert->sender_id = $my_id;
        $alert->sender_type = 'volunteer';
        $alert->alert_type = Alert::ALERT_FOLLOW;
        $alert->contents = ' followed you!';
        $alert->save();
        return Response::json(['result' => 'success']);
    }

    public function unfollowOrganization(Request $request) {
        $id = $request->input('id');
        $my_id = Auth::user()->id;
        $is_exist = Follow::where('follower_id', $my_id)->where('type', 'organization')->where('followed_id', $id)->get()->first();
        $is_exist->is_deleted = 1;
        $is_exist->save();

        $alert = new Alert;
        $alert->receiver_id = $id;
        $alert->sender_id = $my_id;
        $alert->sender_type = 'volunteer';
        $alert->alert_type = Alert::ALERT_FOLLOW;
        $alert->contents = ' unfollowed you!';
        $alert->save();
        return Response::json(['result' => 'success']);
    }

    public function connectOrganization(Request $request) {
        $receiver_id = $request->input('id');
        $currentUser = Auth::user();
        $receiverUser = User::find($receiver_id);
        $sender_id = $currentUser->id;
        $autoAccept = false;

        $is_exist = Friend::where('user_id', $sender_id)->where('friend_id', $receiver_id)->get()->first();

        if ($is_exist == null) {
            $mine = new Friend;
            $mine->user_id = $sender_id;
            $mine->friend_id = $receiver_id;
            $mine->status = Friend::FRIEND_PENDING;
            $mine->save();

            $friend = new Friend;
            $friend->user_id = $receiver_id;
            $friend->friend_id = $sender_id;
            $friend->status = Friend::FRIEND_GET_REQUEST;
            $friend->save();
        } else {
            $is_exist->status = Friend::FRIEND_PENDING;
            $is_exist->is_deleted = 0;
            $is_exist->save();

            $friend = Friend::where('user_id', $receiver_id)->where('friend_id', $sender_id)->get()->first();
            $friend->is_deleted = 0;
            $friend->status = Friend::FRIEND_GET_REQUEST;
            $friend->save();
        }

        if ($receiverUser->auto_accept_friends) {
            $autoAccept = true;
            $orgHomeCtrl = new \App\Http\Controllers\Organization\HomeCtrl();
            $orgHomeCtrl->acceptFriendRequestOrg($sender_id, $receiver_id);

            $alert = new Alert;
            $alert->receiver_id = $receiver_id;
            $alert->sender_id = $sender_id;
            $alert->sender_type = 'volunteer';
            $alert->alert_type = Alert::ALERT_CONNECT_CONFIRM_REQUEST;
            $alert->contents = ' want keep connection with you!';

            $alert->save();
        } else {
            $alert = new Alert;
            $alert->receiver_id = $receiver_id;
            $alert->sender_id = $sender_id;
            $alert->sender_type = 'volunteer';
            $alert->alert_type = Alert::ALERT_CONNECT_CONFIRM_REQUEST;
            $alert->contents = ' want keep connection with you!';
            $alert->save();
        }

        return Response::json(['result' => 'success', 'autoAccept' => $autoAccept]);
    }

    public function disconnectFriend($id = null) {
        $my_id = Auth::user()->id;
        $is_exist_mine = Friend::where('user_id', $my_id)->where('friend_id', $id)->get()->first();
        if ($is_exist_mine != null) {
            $is_exist_mine->delete();
        }
        $is_exist_opp = Friend::where('user_id', $id)->where('friend_id', $my_id)->get()->first();
        if ($is_exist_opp != null) {
            $is_exist_opp->delete();
        }
        $alert = new Alert;
        $alert->receiver_id = $id;
        $alert->sender_id = $my_id;
        $alert->sender_type = 'volunteer';
        $alert->alert_type = Alert::ALERT_DISCONNECT;
        $alert->contents = ' disconnect with you!';
        $alert->save();

        $new_request = DB::table('users')
                ->join('friends', 'users.id', '=', 'friends.user_id')
                ->where('friends.friend_id', $my_id)
                ->where('friends.is_deleted', '<>', 1)
                ->where('friends.status', 1)
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', 1)
                ->select('users.*', 'friends.*')
                ->get();

        $friends = DB::table('users')
                ->join('friends', 'users.id', '=', 'friends.friend_id')
                ->where('friends.user_id', $my_id)
                ->where('friends.is_deleted', '<>', 1)
                ->where('friends.status', 2)
                ->where('users.is_deleted', '<>', 1)
                ->where('users.confirm_code', 1)
                ->select('users.*', 'friends.*')
                ->paginate(12);

        return view('organization.friend', ['page_name' => 'vol_friend',
            'nameClassUl' => 'friends-list-main',
            'friend_requests' => $new_request,
            'friends' => $friends,
            'user_id' => $my_id]);
    }

    public function acceptFriend(Request $request) {
        $id = $request->input('id');
        $my_id = Auth::user()->id;
        $mine = Friend::where('user_id', $my_id)->where('friend_id', $id)->get()->first();
        $mine->status = Friend::FRIEND_APPROVED;
        $mine->save();

        $friend = Friend::where('user_id', $id)->where('friend_id', $my_id)->get()->first();
        $friend->status = Friend::FRIEND_APPROVED;
        $friend->save();

        $alert = new Alert;
        $alert->receiver_id = $id;
        $alert->sender_id = $my_id;
        $alert->sender_type = 'volunteer';
        $alert->alert_type = Alert::ALERT_ACCEPT;
        $alert->contents = ' accept connection.';
        $alert->save();

        $chatManager = new ChatManager();
        $chatManager->join($id);

        return Response::json(['result' => 'success']);
    }

    public function joinGroup(Request $request) {
        //SAME CODE REPEATING IN ORGANIZATION HOMECTRL LINE 717

        $group_id = $request->input('group_id');
        $group_list = Group::where('id', $group_id)->first();
        $creator = User::find($group_list->creator_id);
        $sender = Auth::user();
        $related_id = 0;

        $autoAccept = false;

        $is_exist = Group_member::where('group_id', $group_id)->where('user_id', $sender->id)->get()->first();

        if ($is_exist == null) {
            $member = new Group_member;
            $member->group_id = $group_id;
            $member->user_id = $sender->id;
            $member->role_id = Group_member_role::MEMBER;
            $member->status = Group_member::PENDING;

            if ($group_list->auto_accept_join) {
                $autoAccept = true;
                $member->status = Group_member::APPROVED;
            }

            $member->save();
            $related_id = $member->id;
        } else {
            $is_exist->is_deleted = 0;
            $is_exist->status = Group_member::PENDING;

            if ($group_list->auto_accept_join) {
                $autoAccept = true;
                $is_exist->status = Group_member::APPROVED;
            }

            $is_exist->save();
            $related_id = $is_exist->id;
        }

        if ($autoAccept) {
            $chatManager = new ChatManager();
            $chatManager->joinToGroup($group_id);

            $newsfeed = new NewsfeedModel;
            $newsfeed->who_joined = $sender->id;
            $newsfeed->related_id = $group_list->creator_id;
            $newsfeed->table_name = 'group_members';
            $newsfeed->table_id = $related_id;
            $newsfeed->reason = 'joined in a new group';
            $newsfeed->created_at = date('Y-m-d H:i:s');
            $newsfeed->updated_at = date('Y-m-d H:i:s');
            $newsfeed->save();

            //return $newsfeed;
        } else
            $this->sendGroupJoinAlert($creator, $sender, $group_list, $related_id);

        return Response::json(['result' => 'success', 'autoAccept' => $autoAccept]);
    }

    public function sendGroupJoinAlert($receiver, $sender, $group, $related_id) {
        $alerts = new Alert;
        $alerts->receiver_id = $receiver->id;
        $alerts->sender_id = $sender->id;
        $alerts->sender_type = $sender->user_role;
        $alerts->alert_type = 10;
        $alerts->related_id = $related_id;
        $alerts->contents = 'MyVoluntier want to join ' . $group->name . ' group.';
        $alerts->save();
    }

    public function sendMessage(Request $request) {
        //not using this code

        $sender_id = Auth::user()->id;
        $msg = new Message;
        if ($sender_id != null) {
            $msg->receiver_id = $request->input('receiver_id');
            $msg->content = $request->input('content');
            $msg->sender_id = $sender_id;
            $msg->created_at = date('Y-m-d H:i:s');
            $msg->updated_at = date('Y-m-d H:i:s');

            $save = $msg->save();
            if ($save) {
                $returnArr = array('status' => '1', 'msg' => 'Message send successfully!');

                if ($msg)
                    event(new UserChatNotifyEvent($msg));
            } else {
                $returnArr = array('status' => '0', 'msg' => 'Faliure');
            }
            echo json_encode($returnArr);
            die();
        }
    }

    public function statusComment(Request $request) {

        // $sender_id = Auth::user()->id;
        // $sender_name = Auth::user()->user_name;

        $comment = $request->input('comment');
        $id = $request->input('id');
        DB::table('tracked_hours')->where('id', $id)->update(['comment' => $comment]);

        // $comment->commenter_id= $sender_id;
        // $comment->commenter_name= $sender_name;
        // $comment->body= $request->input('comment');
        // $comment->post_status= $request->input('status');
        // $comment->status_user_id=$request->input('friend_id');
        // $comment->save();

        return redirect('/volunteer/profile');
    }

    public function getAbbreviation($name) {
        $states = array(
            'Alabama' => 'AL',
            'Alaska' => 'AK',
            'Arizona' => 'AZ',
            'Arkansas' => 'AR',
            'California' => 'CA',
            'Colorado' => 'CO',
            'Connecticut' => 'CT',
            'Delaware' => 'DE',
            'Florida' => 'FL',
            'Georgia' => 'GA',
            'Hawaii' => 'HI',
            'Idaho' => 'ID',
            'Illinois' => 'IL',
            'Indiana' => 'IN',
            'Iowa' => 'IA',
            'Kansas' => 'KS',
            'Kentucky' => 'KY',
            'Louisiana' => 'LA',
            'Maine' => 'ME',
            'Maryland' => 'MD',
            'Massachusetts' => 'MA',
            'Michigan' => 'MI',
            'Minnesota' => 'MN',
            'Mississippi' => 'MS',
            'Missouri' => 'MO',
            'Montana' => 'MT',
            'Nebraska' => 'NE',
            'Nevada' => 'NV',
            'New Hampshire' => 'NH',
            'New Jersey' => 'NJ',
            'New Mexico' => 'NM',
            'New York' => 'NY',
            'North Carolina' => 'NC',
            'North Dakota' => 'ND',
            'Ohio' => 'OH',
            'Oklahoma' => 'OK',
            'Oregon' => 'OR',
            'Pennsylvania' => 'PA',
            'Rhode Island' => 'RI',
            'South Carolina' => 'SC',
            'South Dakota' => 'SD',
            'Tennessee' => 'TN',
            'Texas' => 'TX',
            'Utah' => 'UT',
            'Vermont' => 'VT',
            'Virginia' => 'VA',
            'Washington' => 'WA',
            'West Virginia' => 'WV',
            'Wisconsin' => 'WI',
            'Wyoming' => 'WY'
        );
        return $states[$name];
    }

    //This method to filter multiple category for opportunity and return array of opportunities
    private function getSelectedCategory($active_opportunities, $oppor_types) {
        $active_opportunities_selected = array();
        foreach ($active_opportunities as $key => $oppVal) {
            $get_categories = Selected_categoty::where('opportunities_id', $oppVal->oppor_id);
            if (!empty($oppor_types)) {
                $get_categories = $get_categories->where('category_id', '=', $oppor_types);
            }
            $get_categories = $get_categories->get()->toArray();
            if (empty($get_categories)) {
                continue;
            }
            $all_categories = DB::table('selected_category')
                            ->join('opportunity_categories', 'selected_category.category_id', '=', 'opportunity_categories.id')
                            ->select('selected_category.*', 'opportunity_categories.name')
                            ->where('selected_category.opportunities_id', $oppVal->oppor_id)
                            ->get()->toArray();

            $cat_array = array();
            if (!empty($all_categories)) {
                foreach ($all_categories as $key => $cat) {
                    array_push($cat_array, $cat->name);
                }
            }

            $oppVal->name = implode(", ", $cat_array);
            $active_opportunities_selected[] = $oppVal;
        }
        return $active_opportunities_selected;
    }

}
