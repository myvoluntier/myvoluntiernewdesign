<?php

namespace App\Http\Controllers\Volunteer;

use App\Group;
use App\Mail\SharingProfileMail;
use App\Mail\UsersDefaultMail;
use App\Models\EmailsTemplate;
use App\Models\UnlistedOrgnizationOpprHour;
use App\Services\ChatManager;
use App\Activity;
use App\Alert;
use App\Opportunity;
use App\Opportunity_member;
use App\Tracking;
use App\User;
use App\Group_member;
use App\NewsfeedModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Mail;
// add for ICS - Calendar
use App\ICS;

class TrackingCtrl extends Controller
{
	public function viewTrackingPage(Request $request){

	    $user_id = Auth::user()->id;

		$oppr_mem = Opportunity_member::where('user_id',$user_id)->where('status', 1)->where('is_deleted','<>',1)->pluck('oppor_id')->toArray();
		$today = date("Y-m-d");
		$oppr = Opportunity::where('allow_tracking',1)->where('is_deleted','<>',1)->whereIn('id',$oppr_mem)->paginate(10);
		$org_name = User::active()->where('user_role','organization')->get(['id','org_name']);
        $alltracks = Tracking::where('volunteer_id',$user_id)->where('is_deleted','<>',1)->orderBy('logged_date','desc')->get();

		$today = date("Y-m-d");
		$week_date = date('Y-m-d',strtotime($today."-7 days"));
		$week_date = $week_date.' 00:00:00';
		$activity = Activity::where('user_id',$user_id)->where('is_deleted','<>',1)->where('updated_at','>=',$week_date)->orderBy('updated_at','desc')->get();

        $unlistTrackOrgs = UnlistedOrgnizationOpprHour::where('volunteer_id', $user_id)->get();

        // $allGroups = Group::activeGroup()->get();
        $allGroups = Group::select('groups.id', 'groups.name')->leftJoin('group_members', function ($join) {
            $join->on('groups.id', '=', 'group_members.group_id');
        })->where('group_members.user_id', Auth::user()->id)
            ->where('groups.status', 1)
            ->where('groups.is_deleted', '<>', 1)
            ->where('group_members.status', 2)
            ->where('group_members.is_deleted', '<>', 1)
            ->get();
          
        return view('volunteer.track', [
            'unlistTrackOrgs' => $unlistTrackOrgs,
            'oppr' => $oppr,
            'tracks' => $alltracks,
            'activity' => $activity,
            'org_name' => $org_name,
            'page_name' => 'vol_track',
            'all_groups_list' => $allGroups,
        ]);
	}

	public function viewSingleTrackingPage(Request $request){

	    $user_id = Auth::user()->id;
		$oppr_mem = Opportunity_member::where('user_id',$user_id)->where('status', 1)->where('is_deleted','<>',1)->pluck('oppor_id')->toArray();
		$today = date("Y-m-d");
		$oppr = Opportunity::where('is_deleted','<>',1)->whereIn('id',$oppr_mem)->get();
		$org_name = User::where('user_role','organization')->where('is_deleted','<>',1)->where('status','<>',1)->get(['id','org_name']);
		$tracks = Tracking::where('volunteer_id',$user_id)->where('is_deleted','<>',1)->orderBy('updated_at','desc')->get();
		$today = date("Y-m-d");
		$week_date = date('Y-m-d',strtotime($today."-7 days"));
		$week_date = $week_date.' 00:00:00';
		$activity = Activity::where('user_id',$user_id)->where('is_deleted','<>',1)->where('updated_at','>=',$week_date)->orderBy('updated_at','desc')->get();

		foreach ($tracks as $tr){

			if($tr->oppor_id == 0){
				$tr['opp_logo'] = '';
				$tr['opp_type'] = '';
				$ud = explode(" ",$tr->updated_at);
				$tr['updated_date'] = $ud[0];
			}else{
				$opp_info = Opportunity::find($tr->oppor_id);
				$tr['opp_logo'] = $opp_info->logo_img;
				$tr['opp_type'] = $opp_info->type;
				$ud = explode(" ",$tr->updated_at);
				$tr['updated_date'] = $ud[0];
			}
		}

        $allGroups = Group::activeGroup()->get();

		return view('volunteer.track',['oppr'=>$oppr,'all_groups_list' => $allGroups, 'tracks'=>$tracks,'activity'=>$activity,'org_name'=>$org_name,'page_name'=>'vol_track']);
	}

	public function getTrackingDetail(){

	    try{

            $track = Tracking::find(\request()->track_id);

            $user = $track->user;

            $organization = $track->organization;

            $data = [
                "id" => $track->id,
                "name" => $user->first_name .' '.$user->first_nam,
                "image" => $user->logo_img , #'https://via.placeholder.com/100'
                "org_name" => $organization->org_name,
                "opportunity_name" => $track->oppor_name,
                "date" => Carbon::parse($track->logged_date)->format('F d, Y'),
                "time_range" =>  date("g:iA", strtotime($track->started_time)) .' - '.date("g:iA", strtotime($track->ended_time)),
                "hours" => $track->logged_mins/60 .' Hours',
            ];

            return $data;
        }

        catch (\Exception $ex){
	        Log::error('getTrackingDetail error '.request()->track_id.' user Id Logged In' .auth()->id());
            return null;
        }
    }

	public function getOpportunityNames(Request $request){

		$org_id = $request->input('org_id');
		$oppors = Opportunity::where('org_id',$org_id)->where('is_deleted','<>','1')->where('type',1)->get();
		return Response::json(['oppor'=>$oppors]);
	}

	public function getOpportunityInfo(Request $request){
		$opp_id = $request->input('opp_id');
		$oppors = Opportunity::where('id',$opp_id)->where('is_deleted','<>','1')->where('type',1)->get();
		return Response::json(['oppr'=>$oppors]);
	}

	public function joinToOpportunity(Request $request){

		$is_opp_exist = $request->input('is_opp_exist');
		$org_id = $request->input('org_id');
		$opp_id = $request->input('opp_id');

		$end_date =  Carbon::now()->addYear(10)->toDateString();
		$start_date =  Carbon::now()->toDateString();

		$private_opp_title = $request->input('private_opp');
		$user_id = Auth::user()->id;

		if($is_opp_exist == 1){
			$is_mem_exist = Opportunity_member::where('oppor_id',$opp_id)->where('user_id',$user_id)->where('status', 1)->count();
			if($is_mem_exist > 0){
				return Response::json(['result'=>'already exist']);
			}else{
				$opp_mem = new Opportunity_member;
				$opp_mem->oppor_id = $opp_id;
				$opp_mem->user_id = $user_id;
				$opp_mem->org_id = $org_id;
				$opp_mem->save();

				$insert_id = $opp_mem->id;
	            if($insert_id){
	                $newsfeed  = new NewsfeedModel;
	                $newsfeed->who_joined  = $user_id;
	                $newsfeed->related_id   = $org_id;
	                $newsfeed->table_name  = 'opportunity_members';
	                $newsfeed->table_id   = $insert_id;
	                $newsfeed->reason   = 'joined in a new opportunity';
	                $newsfeed->created_at    = date('Y-m-d H:i:s');
	                $newsfeed->updated_at    = date('Y-m-d H:i:s');
	                $newsfeed->save();      
	            }

				$add_activity = new Activity;
				$add_activity->user_id = $user_id;
				$add_activity->oppor_id = $opp_id;
				$add_activity->oppor_title = Opportunity::find($opp_id)->title;
				$add_activity->content = "You Joined on Opportunity ";
				$add_activity->type = Activity::ACTIVITY_JOIN_OPPORTUNITY;
				$add_activity->save();

				$opportunity = Opportunity::find($opp_id);
				$opp_logo = $opportunity->logo_img;

				//add message//
				$activeoppr = $opportunity->title;
				//$joine_dtl = User::select('first_name','last_name')->where('id',$org_id)->first();
				
				$user = User::find($user_id);

				$alert = new Alert;
				$alert->receiver_id  = $org_id;
				$alert->sender_id = $user_id;
				$alert->sender_type = 'volunteer';
				$alert->alert_type = Alert::ALERT_JOIN_OPPORTUNITY;
				$alert->contents = $user->first_name.' '.$user->last_name." sent a request to join Opportunity ".$activeoppr;
				$alert->save();

				//send mail
				$opo_admin_email = $opportunity->contact_email ;
				//echo $comments;die;

                if ($opo_admin_email) {
                    $this->sendJoinRequestOpportunityEmail($opportunity, $opo_admin_email);
                }

				$manager = new ChatManager();
				$manager->joinOrganization(Auth::user(), $opportunity);

				return Response::json(['result'=>'public opportunity added','opp_logo'=>$opp_logo]);
			}
		}else{
			$private_opp = new Opportunity;
			$private_opp->org_id = $org_id;
			$private_opp->org_type = User::find($org_id)->org_type;
			$private_opp->title = $private_opp_title;
			$private_opp->type = 2;
			$private_opp->start_date = $start_date;
			$private_opp->end_date = $end_date;

            //Rodney_Upwork If it's an unlisted opportunity then "Allow Days" should be everyday and all time.
			$private_opp->allow_tracking = 1;
			$private_opp->start_at = "0:00 AM";
			$private_opp->end_at ="12:00 AM";
			$private_opp->weekdays = "Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday";

			$private_opp->save();

			$add_activity = new Activity;
			$add_activity->user_id = $user_id;
			$add_activity->oppor_id = $private_opp->id;
			$add_activity->oppor_title = $private_opp_title;
			$add_activity->content = "You created Private Opportunity ";
			$add_activity->type = Activity::ACTIVITY_CREATE_PRIVATE_OPPORTUNITY;
			$add_activity->save();

			$opp_mem = new Opportunity_member;
			$opp_mem->oppor_id = $private_opp->id;
			$opp_mem->user_id = $user_id;
			$opp_mem->org_id = $org_id;
			$opp_mem->save();

			$add_activity = new Activity;
			$add_activity->user_id = $user_id;
			$add_activity->oppor_id = $private_opp->id;
			$add_activity->oppor_title = $private_opp_title;
			$add_activity->content = "You Joined on Private Opportunity ";
			$add_activity->type = Activity::ACTIVITY_JOIN_OPPORTUNITY;
			$add_activity->save();


			//$opportunity = Opportunity::find($private_opp->id);

            $joine_dtl = User::select('first_name','last_name')->where('id',$user_id)->first();
			$alert = new Alert;
			$alert->receiver_id  = $org_id;
			$alert->sender_id = $user_id;
			$alert->sender_type = 'volunteer';
			$alert->alert_type = Alert::ALERT_CREATE_PRIVATE_OPPORTUNITY;
			$alert->contents = $joine_dtl->first_name.' '.$joine_dtl->last_name." sent a request to join Opportunity ".$private_opp_title;
			$alert->save();

			$opo_admin_email = Opportunity::find($private_opp->id)->contact_email ;

			if ($opo_admin_email) {
			    $this->sendJoinRequestOpportunityEmail($private_opp, $opo_admin_email);
			}

			return Response::json([
			    'result'=>'private opportunity added',
                'opp_id'=>$private_opp->id,
                'href'=> route('volunteer-view-opportunity-profile' ,$private_opp->id),
            ]);
		}
	}

    public function sendJoinRequestOpportunityEmail($opportunity, $contact_email, $is_auto_accept=0){

        $type = $is_auto_accept==1 ? 'JoinedOpportunity' : 'joinRequestOpportunity';
        $email = EmailsTemplate::where('type', $type)->first();

        $receiverUser = $opportunity->organization;

        if($receiverUser && $receiverUser->join_oppr_email=='0') //if enabled emails enabled
        {
            $body = replaceBodyContents($email->body, $email->type, $receiverUser, null, $opportunity);
            \Mail::to($contact_email)->send(new UsersDefaultMail($body, $email->title));
        }

        if($receiverUser && $receiverUser->join_oppr_text=='0' && $opportunity->contact_number) //if texts emails enabled
        {
            $messageBody = replaceTextBodyContents($email->text_body ,$email->type ,$receiverUser , null, $opportunity);
            sendSMSByTwilio($opportunity->contact_number , $messageBody);
        }
    }

	public function addHours(Request $request){

        $opp_id = $request->input('opp_id');
        $opportunity = Opportunity::find($opp_id);


        $is_designated = $request->is_designated == '1' ? 1 : 0;
        $designated_group_id = $is_designated ? $request->designated_group_id : null;

        $opp_name = $request->input('opp_name');
		$volunteer_id = Auth::user()->id;
		$start_time = $request->input('start_time');
		$end_time = $request->input('end_time');
		$logged_mins = $request->input('logged_mins');

		$logged_date = dateYmdFormat($request->input('selected_date'));


        $comments = $request->input('comments');
		$is_edit = $request->input('is_edit');
		$tracking_id = $request->input('tracking_id');
		$is_no_org = $request->input('is_no_org');
		$org_email = $request->input('org_email');

		if(is_null($org_email)) {
            $org_email = $opportunity->organization->email;
        }

		$private_opp_name = $request->input('private_opp_name');
        $unlist_org_name = $request->input('unlist_org_name');
        $unlist_org_email = $request->input('unlist_org_email');
        $end_date =  date("Y-m-d", strtotime($request->input('selected_date')));

        $time_block = $request->input('time_block');

		$logged_date = strtotime ($logged_date);
		$logged_date = date('Y-m-d',$logged_date);


        if($private_opp_name != '') {

			if($is_edit == 0 && $unlist_org_name && $unlist_org_email) {

				$data['volunteer_id'] = $volunteer_id;
				$data['oppor_name'] = $private_opp_name;
				$data['designated_group_id'] = $designated_group_id;
				$data['is_designated'] = $is_designated;
				$data['org_name'] = $unlist_org_name;
				$data['org_email'] = $unlist_org_email;
                $data['logged_date'] = $logged_date;
                $data['started_time'] = $start_time;
                $data['ended_time'] = $end_time;
                $data['logged_mins'] = $logged_mins;
				$data['description'] = $comments;
				$data['selectd_timeblocks'] = $time_block;
				$data['end_date'] = $end_date;

				$data['confirm_code'] = str_random(30);

				$unlistOrg = new UnlistedOrganizationOpprHourController();
				$tracking_info = $unlistOrg->storeUnlistedOrgHours($data);

				$this->sendUnlistedOrgEmail($tracking_info);

                return Response::json(['result'=>'unlist sent']);
            }

            else {

				$tracks = Tracking::find($tracking_id);

				if($tracks->approv_status == 1){
					return Response::json(['result'=>'approved track']);
				}
				if($tracks->approv_status == 2){
					return Response::json(['result'=>'declined track']);
				}

				$tracks->oppor_name = $private_opp_name;
				$tracks->designated_group_id = $designated_group_id;
				$tracks->is_designated = $is_designated;

				$tracks->started_time = $start_time;
				$tracks->ended_time = $end_time;
				$tracks->logged_mins = $logged_mins;
				$tracks->logged_date = $logged_date;
				$tracks->selectd_timeblocks = $time_block;
				$tracks->description = $comments;
				$tracks->save();

				$add_activity = new Activity;
				$add_activity->user_id = $volunteer_id;
				$add_activity->oppor_id = 0;
				$add_activity->oppor_title =$private_opp_name;
				$add_activity->content = "You Updated Logged Hours to ".$logged_mins." mins on Opportunity ";
				$add_activity->type = Activity::ACTIVITY_ADD_HOURS;
				$add_activity->save();

                $org = User::where('email' ,$org_email)->first();

                $this->sendConfirmEmailForAddedHours($opportunity,$org, $is_edit, 0, $private_opp_name,$logged_mins/60,$logged_date);
			}

			$opp_logo = null;
			$is_link = 0;
		}
		else{

			if($is_edit == 0){
				$tracks = new Tracking;
				$tracks->volunteer_id = $volunteer_id;
				$tracks->oppor_id = $opp_id;
				$is_link = Opportunity::find($opp_id)->type;

				if($is_link == 1){
					$tracks->link = 1;
				}
				$tracks->oppor_name = $opp_name;
                $tracks->designated_group_id = $designated_group_id;
                $tracks->is_designated = $is_designated;
				$tracks->org_id = Opportunity::find($opp_id)->org_id;
				$tracks->started_time = $start_time;
				$tracks->ended_time = $end_time;
				$tracks->logged_mins = $logged_mins;
				$tracks->logged_date = $logged_date;
				$tracks->description = $comments;
				$tracks->selectd_timeblocks = $time_block;
				$tracks->save();

				$add_activity = new Activity;
				$add_activity->user_id = $volunteer_id;
				$add_activity->oppor_id = $opp_id;
				if($is_link == 1){
					$add_activity->link = 1;
				}
				$add_activity->oppor_title = $opp_name;
				$add_activity->content = "You Added ".$logged_mins."mins on Opportunity ";
				$add_activity->type = Activity::ACTIVITY_ADD_HOURS;
				$add_activity->save();

				$alert = new Alert;
				$alert->receiver_id = $tracks->org_id;
				$alert->sender_id = $volunteer_id;
				$alert->sender_type = 'volunteer';
				$alert->related_id = $tracks->id;
				$alert->alert_type = Alert::ALERT_TRACK_CONFIRM_REQUEST;
				$alert->contents = ' asking confirm logged hours on your opportunity.';
				$alert->save();

                $org = User::where('email' ,$org_email)->first();
                $this->sendConfirmEmailForAddedHours($opportunity, $org, $is_edit, 1, $opp_name,$logged_mins/60,$logged_date);
			}
			else{

				$tracks = Tracking::find($tracking_id);
				if($tracks->approv_status == 1){
					return Response::json(['result'=>'approved track']);
				}
				$tracks->oppor_id = $opp_id;
				$is_link = Opportunity::find($opp_id)->type;
				if($is_link == 1){
					$tracks->link = 1;
				}
				$tracks->oppor_name = $opp_name;
                $tracks->designated_group_id = $designated_group_id;
                $tracks->is_designated = $is_designated;
				$tracks->org_id = Opportunity::find($opp_id)->org_id;
				$tracks->started_time = $start_time;
				$tracks->ended_time = $end_time;
				$tracks->logged_mins = $logged_mins;
				$tracks->logged_date = $logged_date;
				$tracks->description = $comments;
				$tracks->selectd_timeblocks = $time_block;
				$tracks->save();

				$add_activity = new Activity;
				$add_activity->user_id = $volunteer_id;
				$add_activity->oppor_id = $opp_id;
				if($is_link == 1){
					$add_activity->link = 1;
				}
				$add_activity->oppor_title =$opp_name;
				$add_activity->content = "You Updated Logged Hours to ".$logged_mins." mins on Opportunity ";
				$add_activity->type = Activity::ACTIVITY_ADD_HOURS;
				$add_activity->save();

                $org = User::where('email' ,$org_email)->first();

                $this->sendConfirmEmailForAddedHours($opportunity,$org, $is_edit, 1, $opp_name,$logged_mins/60,$logged_date);
			}
			$opp_logo = Opportunity::find($opp_id)->logo_img;
		}

		return Response::json(['result'=>'success','track_id'=>$tracks->id,'opp_logo'=>$opp_logo,'is_link_exist'=>$is_link]);
	}

    public function sendUnlistedOrgEmail($unlistOrgHour){

	    if($unlistOrgHour->org_email){
            $emailTemp = EmailsTemplate::where('type', 'unlistedOpportunityOrganization')->first();

            $dataArray['oppor_name'] = ucfirst($unlistOrgHour->oppor_name);
            $dataArray['org_mail'] = $unlistOrgHour->org_email;
            $dataArray['org_name'] = $unlistOrgHour->org_name;
            $path = route('home').'?auto-signup=org?o_email='.$unlistOrgHour->org_email;
            $dataArray['link'] = '<a href="'.$path.'">Start MyVoluntier</a>';
            $body = replaceBodyContents($emailTemp->body, $emailTemp->type,null,null,null,$dataArray);

            \Mail::to($unlistOrgHour->org_email)->send(new UsersDefaultMail($body, $emailTemp->title));
        }

    }

    public function sendConfirmEmailForAddedHours($opportunity,$user, $is_edit, $opp_type, $opp_name, $added_hours, $logged_date){

       try{

           $type = $is_edit == 0 ? 'requestConfirmTrackedHoursAdd' : 'requestConfirmTrackedHoursChange';

           $data['opp_type'] = $opp_type =='1' ? 'Private Opportunity' : 'Public Opportunity';
           $data['logged_hours'] = $added_hours;
           $data['opp_name'] = $opp_name;
           $data['logged_date'] = $logged_date;

           $emailTemp = EmailsTemplate::where('type', $type)->first();

           if($user->track_hour_email=='0') //if enabled emails
           {
               $body = replaceBodyContents($emailTemp->body, $emailTemp->type, $user, null, $opportunity, $data);
               \Mail::to($user->email)->send(new UsersDefaultMail($body, $emailTemp->title));
           }

           if($user->track_hour_text=='0' && $user->contact_number && $emailTemp->text_body) //if texts enabled
           {
               $messageBody = replaceTextBodyContents($emailTemp->text_body ,$emailTemp->type ,$user , null, $opportunity ,$data);
               sendSMSByTwilio($user->contact_number , $messageBody);
           }
       }
       Catch(\Exception $ex){

           Log::error($ex->getMessage().' error in sendConfirmEmailForAddedHours in line'.$ex->getLine());
       }
	}

	public function viewTracks(){

        $user_id = Auth::user()->id;
		$tracks = Tracking::where('volunteer_id',$user_id)->where('is_deleted','<>',1)->get();
		$tracked_history = array();

		$i = 0;

		foreach ($tracks as $t){

		    if($t->oppor_id == 0){
				$tracked_history[$i]['id'] = $t->id;
				$tracked_history[$i]['opp_id'] = 0;
				$tracked_history[$i]['title'] = $t->oppor_name;
				$tracked_history[$i]['start'] = $t->logged_date.' '.$t->started_time;
				$tracked_history[$i]['end'] = $t->logged_date.' '.$t->ended_time;
				$tracked_history[$i]['comments'] = $t->description;
				$tracked_history[$i]['approved'] = $t->approv_status;
				$tracked_history[$i]['selected_blocks'] = $t->selectd_timeblocks;
				$tracked_history[$i]['logged_hours'] = $t->logged_mins/60;
				$tracked_history[$i]['logged_date_day'] = dateMDY($t->logged_date).' ('.date('l',strtotime($t->logged_date)).')';
				$tracked_history[$i]['logged_date'] = date('m-d-Y',strtotime($t->logged_date));
                $tracked_history[$i]['is_designated'] = $t->is_designated;
                $tracked_history[$i]['designated_group_id'] = $t->designated_group_id;
                $tracked_history[$i]['link'] = 0;
                $tracked_history[$i]['approv_status'] = $t->approv_status;

                $i++;

			}else{
                $tracked_history[$i]['id'] = $t->id;
                $tracked_history[$i]['opp_id'] = $t->oppor_id;
                $tracked_history[$i]['title'] = Opportunity::find($t->oppor_id)->title;
                $tracked_history[$i]['start'] = $t->logged_date . ' ' . $t->started_time;
                $tracked_history[$i]['end'] = $t->logged_date . ' ' . $t->ended_time;
                $tracked_history[$i]['comments'] = $t->description;
                $tracked_history[$i]['approved'] = $t->approv_status;
                $tracked_history[$i]['selected_blocks'] = $t->selectd_timeblocks;
                $tracked_history[$i]['logged_date_day'] = dateMDY($t->logged_date) . ' (' . date('l', strtotime($t->logged_date)) . ')';
                $tracked_history[$i]['logged_hours'] = $t->logged_mins / 60;
                $tracked_history[$i]['is_designated'] = $t->is_designated;
                $tracked_history[$i]['designated_group_id'] = $t->designated_group_id;
                $tracked_history[$i]['logged_date'] = date('m-d-Y',strtotime($t->logged_date));
                $tracked_history[$i]['approv_status'] = $t->approv_status;


                $is_link = Opportunity::find($t->oppor_id)->type;

				if($is_link == 1){
					$tracked_history[$i]['link'] = 1;
				}else{
					$tracked_history[$i]['link'] = 0;
				}
				$i++;
			}
		}

		return Response::json($tracked_history);
	}

	public function removeHours(Request $request){

	    $user_id = Auth::user()->id;
		$track_id = $request->input('tracking_id');
		$target = Tracking::find($track_id);
		$opp_id = $target->oppor_id;
		$opp_title = $target->oppor_name;
		$is_link = 0;

		if($opp_id == 0){
			$opp_logo = null;
		}else{
			$opp_logo = Opportunity::find($opp_id)->logo_img;
			$is_link = Opportunity::find($opp_id)->type;
		}

		$target->is_deleted = 1;
		$target->save();

		$add_activity = new Activity;
		$add_activity->user_id = $user_id;
		$add_activity->oppor_id = $opp_id;
		if($is_link == 1){
			$add_activity->link = 1;
		}
		$add_activity->oppor_title = $opp_title;
		$add_activity->content = "You Removed Logged Hours on Opportunity ";
		$add_activity->type = Activity::ACTIVITY_REMOVE_HOURS;
		$add_activity->save();

		Alert::where('related_id', $track_id)->where('sender_id', $user_id)->where('alert_type',4)->delete();

		return Response::json([
		    'result'=>$track_id,
            'opp_logo'=>$opp_logo,
            'is_link_exist'=>$is_link
        ]);
	}

	public function activityView(Request $request){
		$range = $request->input('range');
		$date_range = '-'.$range.' days';
		$user_id = Auth::user()->id;
		$today = date("Y-m-d");
		$current_date = date('Y-m-d',strtotime($today.$date_range));
		if($range == 0){
			$current_date = date('Y-m-d',strtotime($today));
		}
		$current_date = $current_date.' 00:00:00';
		$activity = Activity::where('user_id',$user_id)->where('is_deleted','<>',1)->where('updated_at','>=',$current_date)->orderBy('updated_at','desc')->paginate(10);
		return view('volunteer.trackActivitiesList', ['activity'=> $activity]);
	}

	public function sendConfirmEmail($to, $track_id, $confirm_code){

	    $user = Auth::user();

        $fromName = $user->first_name . ' ' . $user->last_name ;

        $data['track_id'] = $track_id;
        $data['confirm_code'] = $confirm_code;

        $email = EmailsTemplate::where('type' , 'trackConfirm')->first();

        if($user->track_hour_email=='0') //if enabled emails
        {
            $body = replaceBodyContents($email->body ,$email->type, $user, null, null, $data);
            \Mail::to($to)->send(new SharingProfileMail($body ,$fromName.' '.$email->title ,$user->email, $fromName));
        }

        if($user->friend_req_text=='0' && $user->contact_number) //if texts enabled
        {
            $messageBody = replaceTextBodyContents($email->text_body ,$email->type ,$user ,null, null, $data);
            sendSMSByTwilio($user->contact_number , $messageBody);
        }

        return 1;
	}

	public function approveGroupRequest($id, $group_id, $status){

		$Group_member = Group_member::where('id',$group_id)->first();
		if($Group_member != null)
		{
			Group_member::where('id',$group_id)->update(['status'=>$status]);
			$is_approved = 2;
			if($status==2)
			{
                $chatManager = new ChatManager();
                $chatManager->joinToGroup($Group_member->group_id);

				$is_approved = 1;
			}
			Alert::where('id',$id)->update(['related_id'=>0,'is_apporved'=>$is_approved]);
		}
		return redirect()->to('/viewAlert');
	}

    public function downloadICSFile($id)
    {
        $tracks = Tracking::find($id);
        $opp_info = Opportunity::find($tracks->oppor_id);
        $location = $opp_info->street_addr1 . " " . $opp_info->city . " " . $opp_info->state . " " . $opp_info->zipcode;
        $data_array = array(
            'title' => $tracks->oppor_name,
            'location' => $location,
            'volunteer_id' => $tracks->volunteer_id,
            'dtstart' => $tracks->started_time,
            'dtend' => $tracks->ended_time,
            'logged_mins' => $tracks->logged_mins,
            'logged_date' => $tracks->logged_date,
            'description' => $tracks->oppor_name . " " . $tracks->description,
            'tracking_id' => $tracks->id,
            'summary' => $tracks->oppor_name,
            'time_block' => $tracks->selectd_timeblocks,
            'url' => url('/')
        );

        $ics = new ICS($data_array);

        $fileName = time() . 'event.ics';
        \Storage::disk('ics')->put($fileName, $ics->to_string());
        $ics_path = url('uploads/ics/' . $fileName);
        return $ics_path;
    }

    /*
     * Method to validated days allow by opportunity
     */
    public function validatedTrackDate(Request $request)
    {

        $addDate = dateYmdFormat(trim($request->input('addDate')));# Carbon::parse()->format('Y-d-m');
        $oppId = trim($request->input('oppId'));

        $oppObj = Opportunity::find($oppId);

        if (!empty($oppObj)) {
           
            $addDateTime = new \DateTime($addDate);
            $startDateTime = new \DateTime($oppObj->start_date);
            $endDateTime = new \DateTime($oppObj->end_date);
            if ($addDateTime >= $startDateTime && $addDateTime <= $endDateTime) {
                $weekDays = explode(',', $oppObj->weekdays);
                $dayName = date('l', strtotime($addDate));

                if (in_array($dayName, $weekDays)) {
                    echo "success";
                } else {
                    echo "Selected opportunity does not allow for selected day. Allow Day(s) " . implode(', ', $weekDays);
                }
            } else {
                echo "Selected opportunity does't allow for selected date.<br>Start Date: " . dateMDY($oppObj->start_date) . " End Date: " . dateMDY($oppObj->end_date);
            }
        } else {
            echo "Invalid selected opportunity.";
        }
        exit;
    }

    /*
     * Method to get validate track hours
     */
    public function validatedTrackHours(Request $request)
    {

        $addDate = dateYmdFormat(trim($request->input('addDate')));# Carbon::parse()->format('Y-d-m');

        $oppId = trim($request->input('oppId'));
        $trackId = trim($request->input('track_id'));
        $isEdit = trim($request->input('is_edit'));
        $timeBlock = explode(',', $request->input('time_block'));
        $loggeDate = date('Y-m-d', strtotime($addDate));
        $oppObj = Opportunity::find($oppId);

        if (!empty($oppObj)) {
            if (strtotime(current($timeBlock)) >= strtotime($oppObj->start_at) && strtotime(end($timeBlock)) < strtotime($oppObj->end_at)) {
                $trackHoursObj = Tracking::select('id', 'selectd_timeblocks')->where('oppor_id', $oppId)
                    ->where('logged_date', $loggeDate)->where('is_deleted', '0')->where('volunteer_id', Auth::user()->id);
                if ($isEdit) {
                    $trackHoursObj = $trackHoursObj->where('id', '<>', $trackId);
                }
                $trackHoursObj = $trackHoursObj->orderBy('id', 'DESC')->get();

                if (!empty($trackHoursObj)) {
                    foreach ($trackHoursObj as $key => $val) {
                        $existTrackHours = explode(',', $val->selectd_timeblocks);
                        if (count(array_intersect($timeBlock, $existTrackHours)) > 0) {
                            echo "Selected time is overlapped.";
                            exit;
                        }
                    }

                    echo "success";

                } else {
                    echo "Invalid selected time.";
                }
            } else {
                echo "Invalid time for selected opportunity. It is start " . $oppObj->start_at . ' and end ' . $oppObj->end_at;
            }
        } else {
            echo "Invalid selected time.";
        }
        exit;
    }

    /*
     * Method to get validate max concurrent volunteer
     */
    public function validatedMaxVolunteer(Request $request)
    {

        $addDate = dateYmdFormat(trim($request->input('addDate')));# Carbon::parse()->format('Y-d-m');

        $oppId = trim($request->input('oppId'));
        $trackId = trim($request->input('track_id'));
        $isEdit = trim($request->input('is_edit'));
        $timeBlock = explode(',', $request->input('time_block'));
        $loggeDate = date('Y-m-d', strtotime($addDate));
        $oppObj = Opportunity::find($oppId);

        if (!empty($oppObj)) {
            if (!isset($oppObj->max_concurrent_vol) || empty($oppObj->max_concurrent_vol)) {
                echo "success";
                exit;
            }
            if (strtotime(current($timeBlock)) >= strtotime($oppObj->start_at) && strtotime(end($timeBlock)) < strtotime($oppObj->end_at)) {
                $trackHoursObj = Tracking::select('id', 'selectd_timeblocks')->where('oppor_id', $oppId)
                    ->where('logged_date', $loggeDate)->where('is_deleted', '0')->where('volunteer_id', '<>', Auth::user()->id);

                if ($isEdit) {
                    $trackHoursObj = $trackHoursObj->where('id', '<>', $trackId);
                }
                $trackHoursObj = $trackHoursObj->orderBy('id', 'DESC')->get();
                if (!empty($trackHoursObj)) {
                    $cntVolunteer = 0;
                    foreach ($trackHoursObj as $key => $val) {
                        $existTrackHours = explode(',', $val->selectd_timeblocks);

                        if (count(array_intersect($timeBlock, $existTrackHours)) > 0) {
                            $cntVolunteer++;
                        }

                        if ($cntVolunteer >= $oppObj->max_concurrent_vol) {
                            echo "Selected duration is over max concurrent volunteer limit (" . $oppObj->max_concurrent_vol . ")";
                            exit;
                        }
                    }

                    echo "success";

                } else {
                    echo "Invalid selected time.";
                }
            } else {
                echo "Invalid time for selected opportunity. It is start " . $oppObj->start_at . ' and end ' . $oppObj->end_at;
            }
        } else {
            echo "Invalid selected time.";
        }
        exit;
    }
        
}
