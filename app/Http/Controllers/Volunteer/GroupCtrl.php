<?php

namespace App\Http\Controllers\Volunteer;

use App\Opportunity;
use App\Tracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Friend;
use App\Group;
use App\Group_member;
use App\Group_category;
use App\Services\ChatService;
use App\Chat;
use App\NewsfeedModel;
use App\CommonModel;
use App\User;
use App\Alert;
use Validator;
use Image;
use DB;
use App\Services\ImageExif;

class GroupCtrl extends Controller
{
    public function viewGroupPage()
    {
        $session = Auth::guard('web')->user();        
        $groupLists = CommonModel::getAll('group_members', array(array('user_id', '=', $session->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');
        /**********************FOR ALL GROUP***********************/

        $allGroupList = array();
        $allGroupList = $this->msort($allGroupList, array('tracked_hours'));
        //echo "<pre>"; print_r($allGroupList); echo "</pre>"; die;

        /**********************FOR ALL GROUP***********************/
        $groupList = array();
        $count=0;

        if (count($groupLists) > 0) {

            foreach ($groupLists as $key => $value) {

                $count++;

                $groupList[] = $value;
                $groupList[$key]->members = array();
                $groupList[$key]->tracked_hours = 0;
                $groupList[$key]->datewise = array();
                $groupList[$key]->sixmonthwise = array();
                $groupList[$key]->lastYearwise = array();
                $groupList[$key]->Yearwise = array();
                $groupList[$key]->CountryWiseCountlastMonth = array();
                $groupList[$key]->groupStateWiseCountlastMonth = array();

                $groupList[$key]->creatorDetails = CommonModel::getAllRow('users', array(array('id', '=', $value->creator_id)));

                $grpMembers = CommonModel::getAll('group_members', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id'), array('reviews', 'reviews.review_to', 'users.id')), '', '', array('group_members.id'));

                list(
                    $groupList[$key]->tracked_hours,
                    $groupList[$key]->des_tracked_hours,
                    $groupList[$key]->members,
                    $groupList[$key]->ranking_cat,
                    $groupList[$key]->ranking_cat_name) = $this->getGrpRankingHoursNameInfo($allGroupList, $grpMembers ,$value);

                $allMembersIds = collect($groupList[$key]->members)->pluck('user_id')->toArray();

            }
            // echo "<pre>"; print_r($groupList); echo "</pre>"; die;
        }

        return view('organization.groups', ['page_name' => 'vol_group'], compact('groupList'));
    }


    public function getGrpMemberOpprsData($grpMembersIds){

        $top5OpprChart = [];
        $opprTotalHours = 0;

        $trackings = Tracking::confirmedNotDel()
            ->where('oppor_id', '<>' ,0)
            ->groupBy('oppor_id')
            ->groupBy('logged_date')
            ->whereIn('volunteer_id' ,$grpMembersIds)
            ->selectRaw('oppor_id ,logged_date, sum(logged_mins)/60 as opp_logged_hours')
            ->get();


        $Oppr_ids = $trackings->pluck('oppor_id')->toArray();


        $Opportunities = Opportunity::whereIn('id' ,$Oppr_ids)->get();


        $allOpportunities = [];

        foreach ($Opportunities as $Oppor){

            $hours = $Oppor->getLoggedHoursSumByMembers($grpMembersIds);
            $top5OpprChart[$Oppor->title] = number_format($hours,1);

            $opprTotalHours = $opprTotalHours + $hours;

            $oppr['name'] = $Oppor->title;
            $oppr['hours'] = $hours;


            $oppr['volonteers'] = $Oppor->opportunity_member ? $Oppor->opportunity_member->whereIn('user_id' ,$grpMembersIds)->groupby('user_id')->count() : 0;

            $allOpportunities[] = $oppr;
        }

        $opprTotalHoursAvg = count($Opportunities) > 0 ? $opprTotalHours / count($Opportunities) : 0;

        $yAxis = getLast12MonthsY();

        $headings = [];

        foreach ($trackings as $tracking){

            if($tracking->category_names){

                foreach (explode(',' ,$tracking->category_names) as $cat){

                    if (array_key_exists($cat ,$headings)){
                        if (isset($headings[$cat][dateMMYY($tracking->logged_date)]))
                            $headings[$cat][dateMMYY($tracking->logged_date)] = $headings[$cat][dateMMYY($tracking->logged_date)] + $tracking->opp_logged_hours;

                        else
                            $headings[$cat][dateMMYY($tracking->logged_date)] = $tracking->opp_logged_hours;
                    }

                    else
                        $headings[$cat][dateMMYY($tracking->logged_date)] = $tracking->opp_logged_hours;

                }
            }
        }

        $hours_oppr_type_chart = [];

        foreach ($headings as $ind => $heading){

            $hours_oppr_type_chart[$ind] = [0,0,0,0,0,0,0,0,0,0,0,0];

            foreach ($heading as $index =>  $hour){
                $ok = array_search($index, $yAxis);
                $hours_oppr_type_chart[$ind][$ok] = $hour;
            }
        }

        arsort($top5OpprChart);

        $top5OpprChart = array_slice($top5OpprChart, 0, 5, true);

        array_multisort(array_column($allOpportunities, 'hours'), SORT_DESC, $allOpportunities);

        return [$hours_oppr_type_chart ,count($allOpportunities) ,$opprTotalHoursAvg ,$top5OpprChart ,$allOpportunities];
    }

    public function getGrpMemberOpprVolonteers($grpMembersIds ,$group){

        $data = [];
        $pieChartZip = [];
        $pieChartAge = [];
        $top5Chart = [];
        $pChartAge['13 – 17'] = $pChartAge['18 – 22'] =$pChartAge['23 – 30'] = $pChartAge['30 and older'] = 0;


        $totalHours = 0;
        $all_volunteersIds = [];

        $query = User::whereIn('id' ,$grpMembersIds);

        $grpMembers = (clone $query)->get();

        foreach ($grpMembers as $index => $user) {

            $object = [];

            $object['name'] = $user->getFullNameVolunteer();
            $object['hours'] = $user->getLoggedHoursSum();
            $object['total_oppr'] = $user->opportunity->count();

            $top5Chart[$user->id.$object['name']] = number_format($object['hours'],1);

            $data[] = $object;

            $totalHours = $totalHours + $object['hours'];
        }

        $all_volunteers_zip = (clone $query)
            ->groupBy('zipcode')
            ->selectRaw('zipcode, count(*) as total')
            ->orderBy('total' ,'asc')
            ->get();

        $all_volunteers_age = (clone $query)
            ->select( [
                \DB::raw("floor((DATEDIFF(CURRENT_DATE, STR_TO_DATE(birth_date, '%m/%d/%Y'))/365)) as age"),
                \DB::raw("count(*) as total")
            ])
            ->groupBy('age')
            ->orderBy('total' ,'asc')
            ->get();

        foreach ($all_volunteers_zip as $user){

            $pChartZip['name'] = "$user->zipcode";
            $pChartZip['y'] = (int)$user->total;
            $pChartZip['color'] = rand_color();

            $pieChartZip[] = $pChartZip;

        }

        foreach ($all_volunteers_age as $user){

            if($user->age >= 13 && $user->age <= 17 )
                $pChartAge['13 – 17'] +=  (int)$user->total;

            if($user->age >= 18 && $user->age <= 22 )
                $pChartAge['18 – 22'] +=  (int)$user->total;

            if($user->age >= 23 && $user->age <= 30 )
                $pChartAge['23 – 30'] +=  (int)$user->total;

            if($user->age >= 30  )
                $pChartAge['30 and older'] +=  (int)$user->total;
        }

        ksort($pChartAge);

        foreach ($pChartAge as $index => $value){

            $array['name'] = "$index";
            $array['y'] = (int)$value;
            $array['color'] = rand_color();

            $pieChartAge[] = $array; //age group pie
        }

        array_multisort(array_column($data, 'hours'), SORT_DESC, $data);
        arsort($top5Chart);

        $top5Chart = array_slice($top5Chart, 0, 5, true);
//        $data = array_slice($data, 0, 10, true); datatable

        $avg = count($data) > 0 ?  number_format($totalHours / count($data),1) : 0;

        return [$data , $avg ,$pieChartZip ,$pieChartAge ,$top5Chart];
    }

    public function getGrpRankingHoursNameInfo($allGroupList ,$grpMembers , $group){

        $total_hours = 0;
        $catRank = 0;
        $members = [];

        $desg_tracked_hours = Tracking::confirmedNotDel()
            ->where('is_designated' ,'1')
            ->where('designated_group_id' ,$group->id)
            ->groupBy('designated_group_id')
            ->selectRaw('sum(logged_mins)/60 as logged_hours')
            ->first();

        $desg_tracked_hours = $desg_tracked_hours ? $desg_tracked_hours->logged_hours : '0';

        if (count($grpMembers) > 0) {
            foreach ($grpMembers as $memberObj) {
                $hours = CommonModel::getAll('tracked_hours', array(array('volunteer_id', '=', $memberObj->user_id), array('approv_status', '=', 1)), '', 'logged_mins', '', '');

                if (count($hours) > 0) {
                    $memberObj->impact = 0;
                    foreach ($hours as $track) {

                        $memberObj->impact += $track->logged_mins;
                        $total_hours = $total_hours + $track->logged_mins;
                    }
                }
                else{
                    $memberObj->impact = 0;
                }

                if($group->creator_id != $memberObj->user_id){
                    $members[] = $memberObj;
                }

            }
        }

        $myCategory = Group::getGroupCategory(count($members));
        array_multisort(array_column($allGroupList, 'total_members'), SORT_DESC, $allGroupList);

        $groupCollect = collect($allGroupList);

        $allGroupListCatWise = $groupCollect->where('cat_name' ,$myCategory);

        foreach ($allGroupListCatWise as $index => $grp) {
            if($grp->group_id == $group->id) {
                $catRank = $index + 1;
                break;
            }
        }

        return [
            $total_hours, $desg_tracked_hours, $members, $catRank, $myCategory
        ];
    }


    function msort($array, $key, $sort_flags = SORT_REGULAR)
    {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';

                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        // @TODO This should be fixed, now it will be sorted as string
                        foreach ($key as $key_key) {
                            $sort_key .= $v->$key_key;
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }

                arsort($mapping, $sort_flags);
                $sorted = array();

                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }

                return $sorted;
            }
        }
        return $array;
    }

    function myfunction($products, $field, $value){

        foreach ($products as $key => $product) {

            if ($product->$field === $value)
                return $key + 1;
        }

        return false;
    }

    public function viewGroupAddPage($id = null)
    {
        $affiliatedOrgs = array();
        $friendDatas = Friend::where('user_id', Auth::user()->id)->where('status', Friend::FRIEND_APPROVED)->get();
        if(!empty($friendDatas)) {
            foreach ($friendDatas as $val){
                if(strtolower($val->friendUser->user_role) == 'organization'){
                    $affiliatedOrgs[$val->friendUser->id] = $val->friendUser->first_name.' '.$val->friendUser->last_name;
                }
            }
        }
        $groupCategory = Group_category::where('is_deleted', 0)->orderBy('name', 'asc')->get();
        if ($id == null)
            return view('organization.create_group', ['affiliatedOrgs'=>$affiliatedOrgs,'group_id' => $id, 'groupCategory'=>$groupCategory, 'page_name' => 'org_group']);
        else {
            $group = Group::find($id);
            return view('organization.create_group', ['affiliatedOrgs'=>$affiliatedOrgs,'group_id' => $id, 'groupCategory'=>$groupCategory, 'group_info' => $group, 'page_name' => 'org_group']);
        }
    }

    public function createGroup(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file_logo' => 'max:5000',
            'file_banner' => 'max:5000',
        ]);

        if ($validator->fails()) {
            \Session::flash('error', 'image bigger than 5 mb');
            return redirect()->back();
        }

        $user_id = Auth::user()->id;
        $group_name = $request->get('group_name');
        $description = $request->get('description');
        $contact_name = $request->get('contact_name');
        $contact_email = $request->get('contact_email');
        $contact_phone = $request->get('contact_phone');
        $group_category_id = $request->get('group_category_id');
        $affiliated_org_id = $request->get('affiliated_org_id');
        $group_type = $request->get('group_type');

        $group = new Group;
        $group->creator_id = $user_id;
        $group->name = $group_name;
        $group->description = $description;
        $group->contact_name = $contact_name;
        $group->contact_email = $contact_email;
        $group->contact_phone = $contact_phone;
        $group->group_category_id = $group_category_id;
        $group->affiliated_org_id = $affiliated_org_id;
        $group->status = 1;
        $group->is_public = $group_type;
        $group->auto_accept_join = $request->get('auto_accept_join') ? 1 : 0;
        $group->private_passcode = $request->get('is_share_able') ? $request->get('private_passcode') : null;
        $group->is_share_able = $request->get('is_share_able') ? 1 : 0;
        
        if ($request->get('image_logo')) {
            $group->logo_img = base64fileUploadOnS3(null,$request->image_logo ,Group::S3PathLogo,$user_id.'logo');
        }

        if ($request->get('image_banner')) {
            $group->banner_image = base64fileUploadOnS3(null,$request->image_banner ,Group::S3PathBan,$user_id.'ban');
        }

        $group->save();

        if(!empty($affiliated_org_id)){
            $alert = new Alert;
            $alert->related_id = $group->id;
            $alert->receiver_id = $affiliated_org_id;
            $alert->sender_id = Auth::user()->id;
            $alert->sender_type = Auth::user()->user_role;
            $alert->alert_type = Alert::ALERT_AFFILIATED_GROUP;
            $alert->contents = " has added ".$group_name." affiliated group.";
            $alert->save();
        }

        $chatService = new ChatService();
        $logo = $group->logo_img === null ? asset('img/org/001.png') : $group->logo_img;

        $chatId = $chatService->createChat($group->name, $logo);
        $chatService->addUserToChat(Auth::user(), $chatId, $group->name, 'groups');

        $chat = new Chat();
        $chat->chat_id = $chatId;
        $chat->user_id = $user_id;
        $chat->group_id = $group->id;
        $chat->type = 'groups';
        $chat->save();

        $group_member = new Group_member;
        $group_member->group_id = $group->id;
        $group_member->user_id = $user_id;
        $group_member->role_id = Group::GROUP_ADMIN;
        $group_member->status = Group_member::APPROVED;
        $group_member->save();

        return redirect()->to('/volunteer/group');
    }

    public function changeGroup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file_logo' => 'max:5000',
            'file_banner' => 'max:5000',
        ]);

        if ($validator->fails()) {
            \Session::flash('error', 'image bigger than 5 mb');
            return redirect()->back();
        }

        $user_id = auth()->id();

        $group_id = $request->get('group_id');
        $group_name = $request->get('group_name');
        $description = $request->get('description');
        $contact_name = $request->get('contact_name');
        $contact_email = $request->get('contact_email');
        $contact_phone = $request->get('contact_phone');
        $group_category_id = $request->get('group_category_id');
        $is_public = $request->get('group_type');

        $group = Group::find($group_id);
        $group->name = $group_name;
        $group->description = $description;
        $group->contact_name = $contact_name;
        $group->contact_email = $contact_email;
        $group->contact_phone = $contact_phone;
        $group->group_category_id = $group_category_id;
        $group->is_public = $is_public;
        $group->auto_accept_join = $request->get('auto_accept_join') ? 1 : 0;;
        $group->private_passcode = $request->get('is_share_able') ? $request->get('private_passcode') : null;
        $group->is_share_able = $request->get('is_share_able') ? 1 : 0;

        if ($request->get('image_logo')) {
            $group->logo_img = base64fileUploadOnS3($group->logo_img,$request->image_logo ,Group::S3PathLogo,$user_id.'logo');
        }

        if ($request->get('image_banner')) {
            $group->banner_image = base64fileUploadOnS3($group->banner_image,$request->image_banner ,Group::S3PathBan,$user_id.'ban');
        }

        if(!empty($group->affiliated_org_id)){
            $group->affiliated_status = 0;
            $alert = new Alert;
            $alertGroup = Alert::where('receiver_id', $group->affiliated_org_id)
                    ->where('sender_id', Auth::user()->id)
                    ->where('related_id', $group_id)
                    ->where('is_apporved', 0)
                    ->first();
            if(empty($alertGroup)){
                $alert->related_id = $group_id;
                $alert->receiver_id = $group->affiliated_org_id;
                $alert->sender_id = Auth::user()->id;
                $alert->sender_type = Auth::user()->user_role;
                $alert->alert_type = Alert::ALERT_AFFILIATED_GROUP;
                $alert->contents = " has updated ".$group_name." affiliated group.";
                $alert->save();
            }else{
               $alertGroup->is_checked = 0; 
               $alertGroup->save();
            }
        }

        $group->save();

	    $chatsGroup = Chat::where('group_id', $group->id)->first();
	    $chatService = new ChatService();
	    $logo = $group->logo_img === null ? asset('img/org/001.png') : $group->logo_img;
//	    $chatService->updateChatInfo($chatsGroup->id, $group->name,  $logo);

        return redirect()->to('/volunteer/group');
    }

    public function getUserList(Request $req)
    {
        $keyword = $req->get('keyword');
        $groupId = $req->get('groupId');
        $getUserList = User::where('first_name', 'like', '%' . $keyword . '%')->get();

        //dd($getUserList);

        $html = '';

        if ($getUserList->count()) {
            $html .= '<div class="searchResult" data-id='.$groupId.'>
                  <div id="volError" style="display:none;color:red">Please check volunteer</div>
					<form action="' . url('api/volunteer/group/add_user_invitation') . '" method="post"  id="sendFrm">

						<ul>';

            foreach ($getUserList as $key => $value) {

                $html .= '		<li style="cursor:pointer" class="listUser"><input type="checkbox" name="list_user_id" value="' . $value->id . '">' . $value->first_name . ' ' . $value->last_name . '</li>';

            }

            $html .= csrf_field();

            $html .= '	</ul>

						<input type="hidden" name="group_id" value="' . $groupId . '">

						

						

					</form>

				</div>';

        } else {

            $html .= '<p>No records found.</p>';

        }

        echo $html;
        die;

    }

    public function addUserInvitation(Request $req)
    {
        $list_user_id= array();
        foreach ($req->get('data') as $value) {
                 
                if($value['name']=="list_user_id"){
                    $list_user_id[]= $value['value'];
                }
                else if($value['name']=="group_id"){
                    $group_id= $value['value'];
                }
          }     

        $session = Auth::guard('web')->user();
        $last_id = 0;

        if ($list_user_id) {
         
            foreach ($list_user_id as $value) {
                $user_id = $value;
                if ($session->id != $value) {
                    $getUserData = User::where('id', $value)->first();
                    $groupData = Group::where('id', $group_id)->first();
                    $GroupMemberCount = Group_member::where('user_id', $value)->where('group_id', $group_id)->count();
                    if ($GroupMemberCount == 0) {
                        $group_member = new Group_member;
                        $group_member->group_id = $group_id;
                        $group_member->user_id = $value;
                        $group_member->role_id = 2;
                        $group_member->status = 1;
                        $group_member->save();
                        $last_id = $group_member->id;

                        $this->sendGroupInviteJoinAlert($getUserData, $session, $groupData, $last_id);
                    }
                }
            }
        }

        if ($last_id == 0) {
            return response()->json(['error'=>'Already added',"group_id"=>$group_id]);
            // $req->session()->flash('error', 'Already added');
        } else {
            return response()->json(['success'=>'Successfully send',"group_id"=>$group_id]);
           // $req->session()->flash('success', 'Successfully send');
        }
        return response()->json(['redirect'=>"/volunteer/group","group_id"=>$group_id]);
        //return redirect()->to('/volunteer/group');
    }

    public function getUserByGroup(Request $request)
    {
        $keyword = trim($request->get('keyword'));
        $geroupId = trim($request->get('groupId'));

        $lists = DB::table('users')
            ->where(function ($query) use ($keyword) {
                $query->where("users.first_name", "LIKE", "%$keyword%")
                    ->orWhere("users.last_name", "LIKE", "%$keyword%")
                    ->orWhere("users.org_name", "LIKE", "%$keyword%");
            })
            ->join('group_members', 'users.id', '=', 'group_members.user_id')
            ->where('group_members.group_id', $geroupId)->where('group_members.status', 2)
            ->select('users.*')
            ->get();

        return view('organization.groupsMembersList', ['lists' => $lists]);
    }

    public function leaveGroup($groupId)
    {
        $group_list = Group::where('id', $groupId)->get();
        $creator_id = $group_list[0]->creator_id;

        $session = Auth::guard('web')->user();

        $groupmember = Group_member::where('user_id', $session->id)->where('group_id', $groupId)->get();
        $table_id = $groupmember[0]->id;

        $chat = Chat::where('group_id', $groupId)->first();

        if($chat) {
            $chatService = new ChatService();
            $chatService->removeUserFromChat($chat->chat_id, Auth::user()->user_name, 'groups');
        }

        $GroupMemberData = Group_member::where('user_id', $session->id)->where('group_id', $groupId)->update(['is_deleted' => 1]);;

        if ($GroupMemberData) {
            $newsfeed = new NewsfeedModel;
            $newsfeed->who_joined = $session->id;
            $newsfeed->related_id = $creator_id;
            $newsfeed->table_name = 'group_members';
            $newsfeed->table_id = $table_id;
            $newsfeed->reason = 'left from a group';
            $newsfeed->created_at = date('Y-m-d H:i:s');
            $newsfeed->updated_at = date('Y-m-d H:i:s');
            $newsfeed->save();
        }

        return redirect()->to('/volunteer/group');
    }

    public function sendGroupInviteJoinAlert($receiver, $sender, $group, $related_id)
    {
        $alerts = new Alert;
        $alerts->receiver_id = $receiver->id;
        $alerts->sender_id = $sender->id;
        $alerts->sender_type = $receiver->user_role;
        $alerts->alert_type = 10;
        $alerts->contents = 'sent you an invitation to join ' . $group->name . ' group';
        $alerts->is_checked = 0;
        $alerts->related_id = $related_id;
        $alerts->save();
    }


    public function viewGroupMember($groupId)
    {
        if (Auth::check()) {
        $session = Auth::guard('web')->user();
        $allGroupLists = CommonModel::getAll('group_members', array(array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');
        $groupLists = CommonModel::getAll('group_members', array(array('group_id', '=', $groupId), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');
        /**********************FOR ALL GROUP***********************/
         
        $allGroupList = array();
    
        if (count($allGroupLists) > 0) {
    
            foreach ($allGroupLists as $keys => $values) {
    
                $total_hours = 0;
                $allGroupList[] = $values;
    
                $allGroupList[$keys]->tracked_hours = 0;
    
                $grpMembers = CommonModel::getAll('group_members', array(array('group_id', '=', $values->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id')));
    
                if (count($grpMembers) > 0) {
                    foreach ($grpMembers as $memberObj) {
                        $hours = CommonModel::getAll('tracked_hours', array(array('volunteer_id', '=', $memberObj->user_id), array('approv_status', '=', 1)), '', 'logged_mins', '', '');
                        if (count($hours) > 0) {
                            foreach ($hours as $track) {
                                $total_hours = $total_hours + $track->logged_mins;
                            }
                        }
                    }
                }
    
                $allGroupList[$keys]->tracked_hours = $total_hours;
                $allGroupList[$keys]->total_members = count($grpMembers);
                $allGroupList[$keys]->cat_name = Group::getGroupCategory(count($grpMembers));
            }
        }
    
    
        $allGroupList = $this->msort($allGroupList, array('tracked_hours'));
        //echo "<pre>"; print_r($allGroupList); echo "</pre>"; die;
    
        /**********************FOR ALL GROUP***********************/
        $groupList = array();
        $count=0;
    
        if (count($groupLists) > 0) {
            foreach ($groupLists as $key => $value) {
                $count++;
                $groupList[] = $value;
                $groupList[$key]->members = array();
                $groupList[$key]->tracked_hours = 0;
                $groupList[$key]->datewise = array();
                $groupList[$key]->sixmonthwise = array();
                $groupList[$key]->lastYearwise = array();
                $groupList[$key]->Yearwise = array();
                $groupList[$key]->CountryWiseCountlastMonth = array();
                $groupList[$key]->groupStateWiseCountlastMonth = array();
    
                $groupList[$key]->creatorDetails = CommonModel::getAllRow('users', array(array('id', '=', $value->creator_id)));
    
                $grpMembers = CommonModel::getAll('group_members', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id'), array('reviews', 'reviews.review_to', 'users.id')), '', '', array('group_members.id'));
    
                list(
                    $groupList[$key]->tracked_hours,
                    $groupList[$key]->des_tracked_hours,
                    $groupList[$key]->members,
                    $groupList[$key]->ranking_cat,
                    $groupList[$key]->ranking_cat_name) = $this->getGrpRankingHoursNameInfo($allGroupList, $grpMembers ,$value);
    
                $allMembersIds = collect($groupList[$key]->members)->pluck('user_id')->toArray();
    
                list(
                    $groupList[$key]->hours_oppr_type_chart,
                    $groupList[$key]->total_opportunities,
                    $groupList[$key]->avg_hrs_per_oppr,
                    $groupList[$key]->top_5_opprtunities,
                    $groupList[$key]->opprtunities_list) = $this->getGrpMemberOpprsData($allMembersIds);
    
                list(
                    $groupList[$key]->volunteers,
                    $groupList[$key]->volunteers_avg,
                    $groupList[$key]->volunteers_pie_zip,
                    $groupList[$key]->volunteers_pie_age,
                    $groupList[$key]->volunteersTop5Chart) = $this->getGrpMemberOpprVolonteers($allMembersIds ,$value);
    
    
                $rank = $this->myfunction($allGroupList, 'group_id', $groupList[$key]->group_id);
                $groupList[$key]->rank = $rank;
    
    
                $thisMonthFirstDay = date('Y-m-d', strtotime("first day of this month"));
                $currentDay = date('Y-m-d');
                $groupList[$key]->thisMonth = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=',$thisMonthFirstDay), array('logged_date', '<=',  $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'logged_date', array('logged_date', 'SUM' => 'logged_mins'));
    
    
                $lastmonthFirstDay = date('Y-m-d', strtotime("first day of -1 month"));
                $lastmonthlastDay = date('Y-m-d', strtotime("last day of -1 month"));
                $groupList[$key]->lastMonth = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthFirstDay), array('logged_date', '<=', $lastmonthlastDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'logged_date', array('logged_date', 'SUM' => 'logged_mins'));
    
    
                $sixmonthFirstDay = date('Y-m-d', strtotime("first day of -6 month"));
                $currentDay = date('Y-m-d');
                $groupList[$key]->last6Month = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $sixmonthFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));
    
    
                $lastYearFirstDay = date('Y-m-d', strtotime("first day of -12 month"));
    
                $currentDay = date('Y-m-d');
    
                $groupList[$key]->last12Month = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));
                $currentDay = date('Y-m-d');
    
    //                $groupList[$key]->Yearwise = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'YEAR', array('logged_date', 'SUM' => 'logged_mins', 'YEAR' => 'logged_date'));
                
                if($groupList[$key]->creatorDetails!=null)
                {
                    $groupList[$key]->CountryWiseCountlastMonth = $CountryWiseCountlastMonth = CommonModel::getAll('groups', array(array('country', '=', $groupList[$key]->creatorDetails->country)), array(array('users', 'users.id', 'groups.creator_id')), 'groups.id', '', '', '', 0, 10);
                    $groupList[$key]->CountryWiseCountlastMonth5 = $CountryWiseCountlastMonth5 = CommonModel::getAll('groups', array(array('state', '=', $groupList[$key]->creatorDetails->state)), array(array('users', 'users.id', 'groups.creator_id')), 'groups.id', '', '', '', 0, 5);
                }
    
                $groupList[$key]->arr = array();
                $groupList[$key]->month = array();
                $groupList[$key]->arr5 = array();
                $groupList[$key]->month5 = array();
    
                if(isset($CountryWiseCountlastMonth))
                {
                    if (count($CountryWiseCountlastMonth) > 0) {
                        foreach ($CountryWiseCountlastMonth as $k => $groups) {
                            $groupList[$key]->yearly[$k]['country'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));
    
                            if (count($groupList[$key]->yearly[$k]['country']) > 0) {
                                array_push($groupList[$key]->arr, $groups->id);
                            }
                            //print_r($groupList[$key]->yearly[$k]);
                            $groupList[$key]->monthly[$k]['month'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthlastDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));
    
                            if (count($groupList[$key]->monthly[$k]['month']) > 0) {
                                array_push($groupList[$key]->month, $groups->id);
                            }
                        }
                    }
                }
    
                if(isset($CountryWiseCountlastMonth5))
                {
                    if (count($CountryWiseCountlastMonth5) > 0) {
                        foreach ($CountryWiseCountlastMonth5 as $k => $groups) {
                            $groupList[$key]->yearly[$k]['country'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));
    
                            if (count($groupList[$key]->yearly[$k]['country']) > 0) {
                                array_push($groupList[$key]->arr5, $groups->id);
                            }
                            //print_r($groupList[$key]->yearly[$k]);
    
                            $groupList[$key]->monthly[$k]['month'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthlastDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));
    
                            if (count($groupList[$key]->monthly[$k]['month']) > 0) {
                                array_push($groupList[$key]->month5, $groups->id);
                            }
                        }
                    }
                }
            }
            //echo "<pre>"; print_r($groupList); echo "</pre>"; die;
        }
        return view('organization.groupMembers', ['groupId'=>$groupId,'page_name' => 'vol_group'],compact('groupList'));  
      }else{
            return redirect()->to('/sign-in');
      }
    }

    public function viewGroupImpact($groupId)
    {
        if (Auth::check()) {
        $session = Auth::guard('web')->user();
        //$allGroupLists = CommonModel::getAll('group_members', array(array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');
        $groupLists = CommonModel::getAll('group_members', array(array('group_id', '=', $groupId), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id')), '', '', 'groups.id');
        /**********************FOR ALL GROUP***********************/

        $allGroupList = array();

        // if (count($allGroupLists) > 0) {

        //     foreach ($allGroupLists as $keys => $values) {

        //         $total_hours = 0;
        //         $allGroupList[] = $values;

        //         $allGroupList[$keys]->tracked_hours = 0;

        //         $grpMembers = CommonModel::getAll('group_members', array(array('group_id', '=', $values->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id')));

        //         if (count($grpMembers) > 0) {
        //             foreach ($grpMembers as $memberObj) {
        //                 $hours = CommonModel::getAll('tracked_hours', array(array('volunteer_id', '=', $memberObj->user_id), array('approv_status', '=', 1)), '', 'logged_mins', '', '');
        //                 if (count($hours) > 0) {
        //                     foreach ($hours as $track) {
        //                         $total_hours = $total_hours + $track->logged_mins;
        //                     }
        //                 }
        //             }
        //         }
        //         $allGroupList[$keys]->tracked_hours = $total_hours;
        //         $allGroupList[$keys]->total_members = count($grpMembers);
        //         $allGroupList[$keys]->cat_name = Group::getGroupCategory(count($grpMembers));
        //     }
        // }


        $allGroupList = $this->msort($allGroupList, array('tracked_hours'));
        //echo "<pre>"; print_r($allGroupList); echo "</pre>"; die;

        /**********************FOR ALL GROUP***********************/
        $groupList = array();
        $count=0;

        if (count($groupLists) > 0) {

            foreach ($groupLists as $key => $value) {

                $count++;

                $groupList[] = $value;
                $groupList[$key]->members = array();
                $groupList[$key]->tracked_hours = 0;
                $groupList[$key]->datewise = array();
                $groupList[$key]->sixmonthwise = array();
                $groupList[$key]->lastYearwise = array();
                $groupList[$key]->Yearwise = array();
                $groupList[$key]->CountryWiseCountlastMonth = array();
                $groupList[$key]->groupStateWiseCountlastMonth = array();
                $groupList[$key]->creatorDetails = CommonModel::getAllRow('users', array(array('id', '=', $value->creator_id)));
                $grpMembers = CommonModel::getAll('group_members', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id'), array('reviews', 'reviews.review_to', 'users.id')), '', '', array('group_members.id'));

                list(
                    $groupList[$key]->tracked_hours,
                    $groupList[$key]->des_tracked_hours,
                    $groupList[$key]->members,
                    $groupList[$key]->ranking_cat,
                    $groupList[$key]->ranking_cat_name) = $this->getGrpRankingHoursNameInfo($allGroupList, $grpMembers ,$value);

                $allMembersIds = collect($groupList[$key]->members)->pluck('user_id')->toArray();

                list(
                    $groupList[$key]->hours_oppr_type_chart,
                    $groupList[$key]->total_opportunities,
                    $groupList[$key]->avg_hrs_per_oppr,
                    $groupList[$key]->top_5_opprtunities,
                    $groupList[$key]->opprtunities_list) = $this->getGrpMemberOpprsData($allMembersIds);

                list(
                    $groupList[$key]->volunteers,
                    $groupList[$key]->volunteers_avg,
                    $groupList[$key]->volunteers_pie_zip,
                    $groupList[$key]->volunteers_pie_age,
                    $groupList[$key]->volunteersTop5Chart) = $this->getGrpMemberOpprVolonteers($allMembersIds ,$value);


                $rank = $this->myfunction($allGroupList, 'group_id', $groupList[$key]->group_id);
                $groupList[$key]->rank = $rank;


                $thisMonthFirstDay = date('Y-m-d', strtotime("first day of this month"));
                $currentDay = date('Y-m-d');
                $groupList[$key]->thisMonth = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=',$thisMonthFirstDay), array('logged_date', '<=',  $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'logged_date', array('logged_date', 'SUM' => 'logged_mins'));


                $lastmonthFirstDay = date('Y-m-d', strtotime("first day of -1 month"));
                $lastmonthlastDay = date('Y-m-d', strtotime("last day of -1 month"));
                $groupList[$key]->lastMonth = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthFirstDay), array('logged_date', '<=', $lastmonthlastDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'logged_date', array('logged_date', 'SUM' => 'logged_mins'));


                $sixmonthFirstDay = date('Y-m-d', strtotime("first day of -6 month"));
                $currentDay = date('Y-m-d');
                $groupList[$key]->last6Month = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $sixmonthFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));


                $lastYearFirstDay = date('Y-m-d', strtotime("first day of -12 month"));

                $currentDay = date('Y-m-d');

                $groupList[$key]->last12Month = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'MONTH', array('logged_date', 'SUM' => 'logged_mins', 'MONTH' => 'logged_date'));
                $currentDay = date('Y-m-d');

                 // $groupList[$key]->Yearwise = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $value->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', '', 'YEAR', array('logged_date', 'SUM' => 'logged_mins', 'YEAR' => 'logged_date'));               
                // if($groupList[$key]->creatorDetails!=null)
                // {
                //     $groupList[$key]->CountryWiseCountlastMonth = $CountryWiseCountlastMonth = CommonModel::getAll('groups', array(array('country', '=', $groupList[$key]->creatorDetails->country)), array(array('users', 'users.id', 'groups.creator_id')), 'groups.id', '', '', '', 0, 10);
                //     $groupList[$key]->CountryWiseCountlastMonth5 = $CountryWiseCountlastMonth5 = CommonModel::getAll('groups', array(array('state', '=', $groupList[$key]->creatorDetails->state)), array(array('users', 'users.id', 'groups.creator_id')), 'groups.id', '', '', '', 0, 5);
                // }

                $groupList[$key]->arr = array();
                $groupList[$key]->month = array();
                $groupList[$key]->arr5 = array();
                $groupList[$key]->month5 = array();

                if(isset($CountryWiseCountlastMonth))
                {
                    if (count($CountryWiseCountlastMonth) > 0) {
                        foreach ($CountryWiseCountlastMonth as $k => $groups) {
                            $groupList[$key]->yearly[$k]['country'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));

                            if (count($groupList[$key]->yearly[$k]['country']) > 0) {
                                array_push($groupList[$key]->arr, $groups->id);
                            }
                            //print_r($groupList[$key]->yearly[$k]);
                            $groupList[$key]->monthly[$k]['month'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthlastDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));

                            if (count($groupList[$key]->monthly[$k]['month']) > 0) {
                                array_push($groupList[$key]->month, $groups->id);
                            }
                        }
                    }
                }

                if(isset($CountryWiseCountlastMonth5))
                {
                    if (count($CountryWiseCountlastMonth5) > 0) {
                        foreach ($CountryWiseCountlastMonth5 as $k => $groups) {
                            $groupList[$key]->yearly[$k]['country'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastYearFirstDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));

                            if (count($groupList[$key]->yearly[$k]['country']) > 0) {
                                array_push($groupList[$key]->arr5, $groups->id);
                            }
                            //print_r($groupList[$key]->yearly[$k]);

                            $groupList[$key]->monthly[$k]['month'] = CommonModel::getAll('tracked_hours', array(array('group_id', '=', $groups->id), array('group_members.status', '=', 2), array('approv_status', '=', 1), array('logged_date', '>=', $lastmonthlastDay), array('logged_date', '<=', $currentDay)), array(array('group_members', 'tracked_hours.volunteer_id', 'group_members.user_id')), '', 'SUM', '', array('group_id', 'SUM' => 'logged_mins'));

                            if (count($groupList[$key]->monthly[$k]['month']) > 0) {
                                array_push($groupList[$key]->month5, $groups->id);
                            }
                        }
                    }
                }

                $groupList[$key]->volun = array();
                $groupList[$key]->volun5 = array();

                if($groupList[$key]->creatorDetails)
                {
                    $volunteer = CommonModel::getAll('group_members', array(array('country', '=', $groupList[$key]->creatorDetails->country), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id'), array('users', 'users.id', 'group_members.user_id')), '', 'SUM', 'group_id', array('group_id', 'SUM' => 'group_id'), 0, 10);
                    $volunteer5 = CommonModel::getAll('group_members', array(array('state', '=', $groupList[$key]->creatorDetails->state), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('groups', 'groups.id', 'group_members.group_id'), array('users', 'users.id', 'group_members.user_id')), '', 'SUM', 'group_id', array('group_id', 'SUM' => 'group_id'), 0, 5);

                    if (count($volunteer) > 0) {
                        foreach ($volunteer as $ks => $volun) {
                            array_push($groupList[$key]->volun, $volun->group_id);
                            //print_r($groupList[$key]->yearly[$k]);
                        }
                    }

                    if (count($volunteer5) > 0) {
                        foreach ($volunteer as $ks => $volun) {
                            array_push($groupList[$key]->volun5, $volun->group_id);
                            //print_r($groupList[$key]->yearly[$k]);
                        }
                    }
                }
            }
            //echo "<pre>"; print_r($groupList); echo "</pre>"; die;
        }

        return view('organization.groupImpact', ['groupId'=>$groupId,'page_name' => 'vol_group'], compact('groupList'));
     }else{
        return redirect()->to('/sign-in');
        }
   }
    
}

