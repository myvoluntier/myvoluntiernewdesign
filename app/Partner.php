<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    
    protected $table = 'partners';

    public $timestamps = true;

    public function organizationPartner()
    {
        return $this->belongsTo(User::class, 'partner_org_id', 'id');
    }

    
}
