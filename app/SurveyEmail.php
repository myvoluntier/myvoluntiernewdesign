<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyEmail extends Model
{
    protected $table = 'survey_emails';

    protected $fillable = [
        'survey_id',
        'opportunity_id',
        'voluntier_id',
        'email_body',
    ];
    public $timestamps = true;
}
