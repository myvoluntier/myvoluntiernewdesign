<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    protected $table = 'shares';

    protected $fillable = [
        'code',
        'shared_user_id',
        'email',
        'expired_at',
        'is_deleted',
    ];

    public $timestamps = true;
    public function user()
    {
        return $this->belongsTo(User::class, 'shared_user_id', 'id');
    }
}
