<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service_project_hour extends Model
{
	
	protected $table = 'service_project_hours';

	public $timestamps = true;
}
