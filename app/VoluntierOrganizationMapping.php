<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoluntierOrganizationMapping extends Model
{
    public $table = 'voluntier_organization_mapping';
    protected $fillable = [
        'voluntier_id',
        'organization_id'
    ];
    public $timestamps = false;
}
