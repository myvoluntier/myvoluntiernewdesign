<?php
/**
 * Created by PhpStorm.
 * User: Trembach.V
 * Date: 07.06.2018
 * Time: 18:16
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

class Chat extends Model{

    protected $table='user_chat';
    public $guarded = [];

    const TYPE_GROUPS = 'groups';
    const TYPE_ORGANIZATIONS = 'organizations';
    const TYPE_FRIENDS = 'friends';
    
    static function getChatInfo($id){
        $deleted = true;
        $chat_id = DB::table('user_chat')->where('user_id', Auth::user()->id)->where('to_user_id', $id)->select('*')->first();
        if ($chat_id == null)
            $chat_id = DB::table('user_chat')->where('user_id', $id)->where('to_user_id', Auth::user()->id)->select('*')->first();
        if ($chat_id != null) {
            $deleted = false;
            if ($chat_id->status == 1 && $chat_id->user_id == Auth::user()->id)
                $deleted = true;
            if ($chat_id->status == 2 && $chat_id->to_user_id == Auth::user()->id)
                $deleted = true;
            $chat_id = $chat_id->chat_id;
        }
        $result['chat_id'] = $chat_id;
        $result['chat_deleted'] = $deleted;
        
        return $result;
    }

}