<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group_category extends Model
{
	protected $table = 'group_categories';

	protected $fillable = [
		'name'
	];

	public $timestamps = false;

    public function group()
    {
        return $this->hasOne(Group::class, 'group_category_id', 'id');
    }
}
