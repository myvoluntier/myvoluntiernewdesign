<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestions extends Model
{
    protected $table = 'survey_questions';

    protected $fillable = [
        'survey_id',
        'question_type',
        'questions',
    ];
    public $timestamps = true;

    public function quesOption(){
        return $this->hasMany('App\SurveyOptions','question_id','id');

    }
    
}
