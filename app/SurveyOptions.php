<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyOptions extends Model
{
    protected $table = 'survey_options';

    protected $fillable = [
        'survey_id',
        'question_id',
        'options',
    ];
    public $timestamps = true;

    public function options(){
        return $this->hasOne('App\SurveyResult','survey_id','survey_id');
    }
}
