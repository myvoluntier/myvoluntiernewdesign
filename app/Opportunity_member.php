<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opportunity_member extends Model
{
	protected $table = 'opportunity_members';

    //	Status = 0 => Not Joined
    //	Status = 1 => Joined
    //	Status = 2 => Pending

	protected $fillable = [
		'oppor_id',
		'user_id',
		'org_id',
		'status',
		'is_deleted',
	];

	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function opportunity()
    {
        return $this->belongsTo(Opportunity::class, 'oppor_id', 'id');

    }
}
