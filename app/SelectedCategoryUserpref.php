<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelectedCategoryUserpref extends Model
{
	protected $table = 'selected_category_userprefs';

	protected $fillable = [
		'user_id',
		'category_id',
	];

	public $timestamps = true;

   
}
