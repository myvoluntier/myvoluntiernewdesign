<?php

namespace App;

use App\Models\ApiAccess;
use App\Models\Sitecontent;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'org_name',
        'first_name',
        'last_name',
        'user_name',
        'logo_img',
        'back_img',
        'user_role',
        'email',
        'password',
        'birth_date',
        'gender',
        'zipcode',
        'location',
        'lat',
        'lng',
        'contact_number',
        'brif',
        'website',
        'org_type',
        'remember_token',
        'forgot_status',
        'auto_accept_friends',
        'status',
        'is_deleted'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'type_name',
        'oppr_count',
        'all_emails_send',
        'all_texts_send'
    ];

    const S3PathLogo = 'users/logos';
    const S3PathBan = 'users/banners';
    const S3PathActivity = 'users/activity';


    public static function sortingColumns() {

        return [
            'created_at' => 'Created At',
            'first_name' => 'First Name',
            'org_name'=>'Organization name',
            'email' => 'Email',
            'user_name' => 'User Name',
            'zipcode' => 'Zip Code',
            'contact_number' => 'Contact',
        ];
    }

    public function scopeSearchingUser($query ,$search){

        return $query->where(function ($q) use ($search) {
            $q->orWhere('first_name', 'like', "%{$search}%");
            $q->orWhere('last_name', 'like', "%{$search}%");
            $q->orWhere('org_name', 'like', "%{$search}%");
            $q->orWhere('email', 'like', "%{$search}%");
            $q->orWhere('user_name', 'like', "%{$search}%");
            $q->orWhere('zipcode', 'like', "%{$search}%");
            $q->orWhere('contact_number', 'like', "%{$search}%");
        });
}

    public function scopeActiveUser($query){
        return $query->where('status', 1)->where('is_deleted', '<>', 1);
    }

    public function scopeActive($query){
        return $query->where('is_deleted', '<>', 1)->where('status', 1);
    }

    public function allNotificationsSendColumns($type){

        if($type == 'all_emails_send')
            $array = [
                'join_oppr_email', 'friend_req_email', 'track_hour_email', 'chat_msg_email'
            ];
        else
            $array = [
                'join_oppr_text', 'friend_req_text', 'track_hour_text', 'chat_msg_text',
            ];

        return $array;
    }

    public function getAllEmailsSendAttribute()
    {
        $response = '1';
        foreach ($this->allNotificationsSendColumns('all_emails_send') as $value){

            if(isset($this->attributes[$value]) && $this->attributes[$value] == '0'){
                $response = 0;
                break;
            }
        }

        return $response;
    }

    public function getAllTextsSendAttribute()
    {
        $response = '1';
        foreach ($this->allNotificationsSendColumns('all_texts_send') as $value){

            if(isset($this->attributes[$value]) && $this->attributes[$value] == '0'){
                $response = 0;
                break;
            }
        }

        return $response;
    }

    public function getTypeNameAttribute()
    {
        if ($this->user_role == 'organization') {
            $org_type_name = Organization_type::where('id', $this->org_type)->first();
            return @$org_type_name->organization_type;
        } else
            return '';
    }
    public function getSchoolType()
    {
        if ($this->user_role == 'organization') {
            if(is_null($this->school_type)){
                return '';
            }else{
                $school_type_name = School_type::where('id', $this->school_type)->first();
                return is_null($school_type_name->school_type) ? '' : $school_type_name->school_type;
            }

        } else
            return '';
    }
    public function getFirstNameAttribute($value)
    {
        if ($this->user_role == 'organization') {
            $value = $this->org_name;
        }
        return ucfirst($value);
    }

    public function getOpprCountAttribute()
    {
        $today = date("Y-m-d");
        $count = Opportunity::where('org_id', $this->id)->where('type', 1)->where('is_deleted', '<>', '1')->where('end_date', '>=', $today)->count();
        return $count;
    }

    public function getLoggedHoursSum()
    {
        return $this->tracksVoluntier()
                ->where('approv_status', 1)
                ->where('is_deleted', '<>', 1)
                ->sum('logged_mins') / 60;
    }

    public function tracksOpportunityHours($oppr_id)
    {
        return $this->tracksVoluntier()
                ->where('approv_status', 1)
                ->where('is_deleted', '<>', 1)
                ->where('logged_mins', '>', 0)
                ->where('oppor_id' , $oppr_id)->get();
    }

    public function tracksHoursForOrg($org_id, $orgIds=array())
    {
        if(empty($orgIds) && $org_id != ""){
            $orgIds[] = $org_id; 
        }
        return $this->tracksVoluntier()
            ->where('approv_status', 1)
            ->where('is_deleted', '<>', 1)
            ->where('logged_mins', '>', 0)
            ->whereIn('org_id' , $orgIds)->sum('logged_mins')/60;
    }

    public function totalOpprForOrgVolonteer($org_id, $orgIds=array())
    {
        if(empty($orgIds) && $org_id != ""){
            $orgIds[] = $org_id; 
        }
        return $this->opportunity()
                ->whereIn('opportunity_members.org_id' , $orgIds)->distinct()->get();
    }

    public function getFullNameVolunteer()
    {
        return implode(' ', array_filter([$this->first_name, $this->last_name]));
    }

    public function fullLocation()
    {
        return implode(', ', array_filter([$this->city, $this->state, $this->country]));
    }

    public function friends()
    {
        return $this->hasMany(Friend::class, 'user_id', 'id');
    }

    public function whereUserFriends()
    {
        return $this->hasMany(Friend::class, 'friend_id', 'id');
    }

    public function activities()
    {
        return $this->hasMany(Activity::class, 'user_id', 'id');
    }

    public function alertReceivers()
    {
        return $this->hasMany(Alert::class, 'receiver_id', 'id');
    }

    public function alertSenders()
    {
        return $this->hasMany(Alert::class, 'sender_id', 'id');
    }

    public function follower()
    {
        return $this->hasMany(Follow::class, 'follower_id', 'id');
    }

    public function followed()
    {
        return $this->hasMany(Follow::class, 'followed_id', 'id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_members', 'user_id', 'group_id');
    }

    public function MessagesWhereYouReceiver()
    {
        return $this->hasMany(Message::class, 'receiver_id', 'id');
    }

    public function MessagesWhereYouSender()
    {
        return $this->hasMany(Message::class, 'sender_id', 'id');
    }

    public function newsFeeds()
    {
        return $this->hasMany(NewsfeedModel::class, 'who_joined', 'id');
    }
    
    public function serviceProjects()
    {
        return $this->hasMany(Service_project::class, 'volunteer_id', 'id');
    }
    public function opportunity()
    {
        return $this->belongsToMany(Opportunity::class, 'opportunity_members', 'user_id', 'oppor_id');
    }

    public function opportunityOrg()
    {
        return $this->belongsToMany(Opportunity::class, 'opportunity_members', 'org_id', 'oppor_id');
    }

    public function reviewFrom()
    {
        return $this->hasMany(Review::class, 'review_from', 'id');
    }

    public function reviewTo()
    {
        return $this->hasMany(Review::class, 'review_to', 'id');
    }

    public function tracksVoluntier()
    {
        return $this->hasMany(Tracking::class, 'volunteer_id', 'id');
    }

    public function tracksOrganization()
    {
        return $this->hasMany(Tracking::class, 'org_id', 'id');
    }

	public function groupOrg()
	{
		return $this->hasMany(Group::class, 'creator_id','id');
	}

    public function apiAccess(){
        return $this->hasOne(ApiAccess::class, 'org_id', 'id');
    }
	
	public function getOppoCreatePopupContent($id = 1)
    {
        $sitecontent = Sitecontent::find($id);
		if(!empty($sitecontent->html_body)){
			$retContent = $sitecontent->html_body;
		}else{
			$retContent =  '<h6 style="text-align: center; color: rgb(0, 0, 0);"><font size="3">It looks like you don\'t have any active Opportunities set up.&nbsp;<br></font><font size="3"><span style="color: rgb(51, 51, 51);">Would you like to create an Volunteer Opportunity now?</span></font></h6>';
		}
        return $retContent;
    }

    public function getAge(){
        $date = Carbon::parse($this->birth_date);
        return $date->diff(Carbon::now())->format('%y year');
    }
    
    public function parentOrgName($parentId = null)
    {
    if(empty($parentId)){
            $parentId = $this->parent_id;
        }
        $parentOrg = User::Select('org_name')->where('id', $parentId)->first();
        return $parentOrg->org_name;
    }

    public function first_name(){

      return $this->belongsTo(Group_member::class,'user_id','id');
    }
   

   
}
