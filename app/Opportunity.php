<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Opportunity extends Model
{
	const REGULAR_OPPORTUNITY = 1;
	const IRREGULAR_OPPORTUNITY = 2;

    const S3PathLogo = 'opportunity';
	
	protected $table = 'opportunities';

	protected $guarded = [];

	public $timestamps = true;

	protected $appends = [
		'parent_id',
		'org_name',
		'org_logo',
		'opportunity_type',
		'opportunity_member',
		'is_expired',
	];

	public static function getStates() {

        return [
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District Of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming'
        ];
    }

    public function scopeOwner($query){
        return $query->where('org_id', auth()->id());
    }

    public function scopeActive($query){
        return $query->where('is_deleted','<>',1);
//        return $query->where('is_deleted','<>',1)->where('status',1);
    }


	public function getOrgNameAttribute(){
		$poster = User::find($this->org_id);
		return (!is_null($poster)) ? $poster->org_name : '';
	}
	public function getParentIdAttribute(){
		$poster = User::find($this->org_id);
		return (!is_null($poster)) ? $poster->parent_id : '';
	}
	public function getIsExpiredAttribute(){

        if (Carbon::now()->gt(Carbon::parse($this->end_date)))
            return true;

        return false;
	}

	public function getOppNameAttribute(){
		$poster = Opportunity::find($this->id);
		return (!is_null($poster)) ? $poster->title : '';
	}

	public function getOrgLogoAttribute(){
		$poster = User::find($this->org_id);
		return (!is_null($poster)) ? $poster->logo_img : '';
	}

	public function getOpportunityTypeAttribute(){
		if($this->category_id != 0){
			$categories = Opportunity_category::find($this->category_id);
			return $categories['name'];
		}else{
			return '';
		}
	}

	public function getOpportunityMemberAttribute(){
        return Opportunity_member::where('oppor_id',$this->id)->where('status', 1)->where('is_deleted','<>',1)->get();
	}


//	public function getOrgMembersAttribute(){
//		$members = Opportunity_members::where('oppor_id',$this->id)->orderby('created_at')->get();
//		return $members;
//	}

    public function activities()
    {
        return $this->hasMany(Activity::class, 'oppor_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'opportunity_members', 'oppor_id', 'user_id')
            ->where('opportunity_members.is_deleted', '<>', 1);
    }

    public function opprSelectedCategories()
    {
        return $this->hasMany(Selected_categoty::class, 'opportunities_id', 'id');
    }

    public function tracks()
    {
        return $this->hasMany(Tracking::class, 'oppor_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(User::class, 'org_id', 'id');
    }

    public function oppCategory()
    {
        return $this->belongsTo(Opportunity_category::class, 'category_id', 'id');
    }

    public function getLoggedHoursSum()
    {
        return $this->tracks()
                ->where('approv_status', 1)
                ->where('is_deleted', '<>', 1)
                ->sum('logged_mins') / 60;
    }

    public function getLoggedHoursSumByMembers($grpMembersIds)
    {
        return $this->tracks()
                ->whereIn('volunteer_id' ,$grpMembersIds)
                ->where('approv_status', 1)
                ->where('is_deleted', '<>', 1)
                ->sum('logged_mins') / 60;
    }

}


