<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'survey';

    protected $fillable = [
        'organization_id',
        'opportunity_id',
        'survey_name',
        'survey_type',
        'status',
        'survey_start',
        'survey_end'
    ];
    public $timestamps = true;
}
