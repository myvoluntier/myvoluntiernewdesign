<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiAccess extends Model
{
    protected $table = 'api_access';
    protected $guarded = [];

}
