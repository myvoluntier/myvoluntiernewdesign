<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sitecontent extends Model
{

    protected $table = 'sitecontents';
    protected $guarded = [];

}
