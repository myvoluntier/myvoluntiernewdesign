<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnlistedOrgnizationOpprHour extends Model
{
    protected $table = 'unlist_track_org_hours';
    protected $guarded = [];
}
