<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSync extends Model
{
    protected $table = 'user_syncs';
    protected $guarded = [];
}
