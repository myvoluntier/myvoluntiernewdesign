<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailsTemplate extends Model
{
    protected $table = 'email_templates';
    protected $guarded = [];

}
