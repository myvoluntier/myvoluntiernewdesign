<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSyncAttribute extends Model
{
    protected $table = 'user_sync_attributes';
    protected $guarded = [];
}
