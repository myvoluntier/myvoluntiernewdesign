<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommonModel extends Model
{
    public static function getAll($table, $where = '', $join = '', $select = '*', $orderBy = '', $groupBy = '', $rawselect = '', $offset = '', $limit = '', $whereIn=array())
    {
        $query = DB::table($table);

        if ($join) {
            foreach ($join as $row) // $join=array(array('users','users.id','group_members.user_id'));
            {
                $query = $query->leftJoin($row[0], $row[1], '=', $row[2]);
            }
        }
        if ($where) {
            foreach ($where as $wh) // $where=array(array('group_id','=',$value->id),array('group_members.status',2));
            {
                $query = $query->where($wh[0], $wh[1], $wh[2]);
            }
        }
        if (!empty($whereIn)) {
            $query = $query->whereIn($whereIn[0], $whereIn[1]);
            
        }
        if ($select) {
            $query = $query->select($select);
        }
        if ($rawselect) {
            $i = 0;
            $sel[$i] = $rawselect[0];
            foreach ($rawselect as $key => $value) {
                if ($i != '0')
                    $sel[$i] = DB::raw('' . $key . '(' . $value . ') as ' . $key . '');
                $i++;
            }
            $query = $query->select($sel);
        }
        if ($groupBy) {
            $query = $query->groupBy($groupBy);
        }
        if ($orderBy) {
            $query = $query->orderBy($orderBy, 'DESC');
        }
        if ($offset) {
            $query = $query->offset($offset);
        }
        if ($limit) {
            $query = $query->limit($limit);
        }

       // dd($query->toSql());exit;
        $result = $query->get();

        return $result;
    }


    public static function getAllRow($table, $where = '', $join = '', $select = '*', $orderBy = '', $groupBy = '')
    {
        $occation = DB::table($table);

        if ($join) {
            foreach ($join as $row) // $join=array(array('users','users.id','group_members.user_id'));
            {
                $occation = $occation->leftJoin($row[0], $row[1], '=', $row[2]);
            }
        }
        if ($where) {
            foreach ($where as $wh) // $where=array(array('group_id','=',$value->id),array('group_members.status',2));
            {
                $occation = $occation->where($wh[0], $wh[1], $wh[2]);
            }
        }
        if ($select) {
            $occation = $occation->select($select);
        }
        if ($groupBy) {
            $occation = $occation->groupBy($groupBy);
        }
        if ($orderBy) {
            $occation = $occation->orderBy($orderBy);
        }
        $occations = $occation->get()->first();

        return $occations;
    }

    public function insertData($table, $arr)
    {
        $occation = DB::table($table);
        $occations = $occation->insertGetId($arr); //array('email' => 'john@example.com', 'votes' => 0)
        return $occations;
    }

    public function updateData($table, $arr, $where = '')
    {

        $occation = DB::table($table);
        if ($where) {
            foreach ($where as $wh) // $where=array(array('group_id','=',$value->id),array('group_members.status',2));
            {
                $occation = $occation->where($wh[0], $wh[1], $wh[2]);
            }
        }
        $occations = $occation->update($arr); //array('email' => 'john@example.com', 'votes' => 0)
        return $occations;
    }

    public function deleteData($table, $where = '')
    {
        $occation = DB::table($table);
        if ($where) {
            foreach ($where as $wh) // $where=array(array('group_id','=',$value->id),array('group_members.status',2));
            {
                $occation = $occation->where($wh[0], $wh[1], $wh[2]);
            }
        }
        $occations = $occation->delete();
        return $occations;
    }
}
