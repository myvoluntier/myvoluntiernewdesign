<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SharingProfileMail extends Mailable
{
    use Queueable, SerializesModels;
    public $body;
    public $title;
    public $reply_to;
    public $reply_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($body ,$title ,$reply_to=null, $reply_name=null)
    {
        $this->body = $body;
        $this->title = $title;
        $this->reply_to = $reply_to;
        $this->reply_name = $reply_name;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.default')->subject($this->title)
            ->replyTo($this->reply_to, $this->reply_name);
    }
}
