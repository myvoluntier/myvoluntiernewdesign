<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class SyncUsersFreshdesk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncusers:freshdesk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Volunteer/Organization Data with FreshDesk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //organizations list
		$organizationlist = User::where('user_role','organization')->where('sync_freshdesk','0')->orderBy('is_deleted', 'ASC')->get();
		$this->makeUserData($organizationlist, "Organization");
		//
		
		//volunteers list
		$volunteerlist = User::where('user_role','volunteer')->where('sync_freshdesk','0')->orderBy('is_deleted', 'ASC')->get();
		$this->makeUserData($volunteerlist, "Volunteer");
		
    }
	
	//make data for save into the FreshDesk
	private function makeUserData($userList, $type="Organization"){
		if(count($userList) > 0){
		    foreach($userList as $val){
				$address = $val->city.', '.$val->state.', '.$val->country.', '.$val->zipcode;
				$name = (strtolower($type) == 'organization') ? $val->org_name : $val->first_name.' '.$val->last_name;
				$contact_data = json_encode(array(
					"name" => $name,
					"email" => $val->email,
					"job_title" => $type,
					"phone" => $val->contact_number,
					"address" => $address
				));
				$result = $this->saveDataInFreshdesk($contact_data);
				if($result['msg'] == 'success'){
					$val->sync_freshdesk  = 1; 
					$val->save();
				}else{
					$result['userid'] = $val->id;
					$result['email'] = $val->email;
					$errorInfo[] = $result;
				}
			}
			if(!empty($errorInfo)){
				echo "Some of the user data has been not import into FreshDesk.";
				print_r($errorInfo);
			}else{
				echo $type." has been imported successfully on FreshDesk.";
			}
		}else{
			echo "No record found for : ".$type." into sync with FreshDesk.";
		}
	}
	
	//save user data into the FreshDesk
	private function saveDataInFreshdesk($contact_data = array()){
		
		//FreshDesk API Set Credentials below 
		$api_key = env('FRESHDESK_API_KEY');
		$password = env('FRESHDESK_PASSWORD');
		$yourdomain = env('FRESHDESK_DOMAINNAME');
		$url = "https://$yourdomain.freshdesk.com/api/v2/contacts";
		
		//Call FreshDesk API and post user data
		$ch = curl_init($url);
		$header[] = "Content-type: application/json";
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_USERPWD, "$api_key:$password");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $contact_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec($ch);
		$info = curl_getinfo($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$headers = substr($server_output, 0, $header_size);
		$response = substr($server_output, $header_size);
		
		if($info['http_code'] == 201) {
			$result['msg'] = 'success';
			$result['response'] = $response;
		} else {
			$result['msg'] = 'error';
			$result['response'] = $response;
			
				
		}
		curl_close($ch);
		return $result;
		
	}
}
