<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		$htType = 'http';
		if(\Request::isSecure()){
			$htType = 'https';
        }
        if(env('FORCE_HTTPS')) {
            $htType = 'https';
        }        
		\URL::forceScheme($htType);
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
