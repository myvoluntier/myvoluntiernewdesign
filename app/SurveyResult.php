<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyResult extends Model
{
    protected $table = 'survey_result';

    protected $fillable = [
        'survey_id',
        'user_id',
        'question_id',
        'option_id'
    ];
    public $timestamps = true;
}
