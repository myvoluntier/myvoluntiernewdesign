<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Tracking extends Model
{
	const STATUS_PENDING = 0;
	const STATUS_APPROVE = 1;
	const STATUS_DECLINE = 2;
	
	protected $table = 'tracked_hours';

	protected $fillable = [
		'volunteer_id',
		'oppor_id',
        'is_designated',
        'designated_group_id',
		'org_id',
		'logged_date',
		'started_time',
		'ended_time',
		'logged_mins',
		'uploaded_file',
		'description',
		'confirmer_name',
		'approv_status',
		'selectd_timeblocks',
		'is_disabled',
		'is_deleted',
		'confirmed_at'
	];

	protected $appends = [
		'org_name',
		'opp_name',
		'category_type',
		'org_type',
		'submitted_date',
		'category_names'
    ];

	public function scopeConfirmedNotDel($query){
        return $query->where('is_deleted','<>', 1)->where('approv_status', 1);
    }

	public function getOrgNameAttribute(){
		if($this->org_id != null){
			$poster = User::find($this->org_id);
			return (!is_null($poster)) ? $poster->org_name: '';
		}else{
			return '';
		}
	}

	public function getOppNameAttribute(){
		if($this->oppor_id != null){
			$poster = Opportunity::find($this->oppor_id);
			return (!is_null($poster)) ? $poster->title : '';
		}else{
			return '';
		}
	}

	public function getCategoryTypeAttribute(){
		if($this->oppor_id != null){
			$poster = Opportunity::find($this->oppor_id);
			return (!is_null($poster)) ? $poster->type : '';
		}else{
			return '';
		}
	}

	public function getOrgTypeAttribute(){
		if($this->oppor_id != null){
			$poster = Opportunity::find($this->oppor_id);
			return (!is_null($poster)) ? $poster->org_type : '';
		}else{
			return '';
		}
	}

	public function getLoggedHours(){
		if($this->oppor_id != null){
			$poster = Tracking::find($this->oppor_id);
			return (!is_null($poster)) ? ($poster->logged_mins)/60: '';
		}else{
			return '';
		}
	}

	public function getVolunteerNameAttribute(){
		if($this->volunteer_id != null){
			$poster = Opportunity::find($this->volunteer_id);
			return $poster->first_name.$poster->last_name;
		}else{
			return '';
		}
	}

	public function getSubmittedDateAttribute(){

		try{
            if($this->oppor_id != null){
                $poster = Opportunity::find($this->oppor_id);
                $created_at = $poster->created_at;
                $date = explode(' ',$created_at);
                $submitted_date = date('F j, Y',strtotime($date[0]));
                return $submitted_date;
            }else{
                return '';
            }
        }
        catch (\Exception $e) {
            return '';
        }
	}
	
	public function getCategoryNamesAttribute(){

	    if($this->oppor_id != null){
			$cat_array = array();
            $all_categories = Selected_categoty::where('opportunities_id',$this->oppor_id)->get()->toArray();
            
            if(!empty($all_categories))
            {   
                foreach ($all_categories as $key => $cat) {
					$opportunityCategory = Opportunity_category::select('name')->where('id',$cat['category_id'])->get()->toArray();
                    array_push($cat_array, $opportunityCategory[0]['name']);
                }
            }
			return (!is_null($cat_array)) ? implode(', ',$cat_array) : '';
		}else{
			return '';
		}
	}

	public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class, 'volunteer_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(User::class, 'org_id', 'id');
    }

    public function opportunity()
    {
        return $this->belongsTo(Opportunity::class, 'oppor_id', 'id');
	}
	
}
