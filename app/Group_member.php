<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group_member extends Model
{
    protected $table = 'group_members';
    protected $guarded = [];
    protected $fillable = [
        'user_id',
        'group_id',

    ];
    const DECLINED = 0;
    const PENDING = 1;
    const APPROVED = 2;

    //Status 2 is active status and other is ?

    public function scopeActiveMember($query){
        return $query->where('status', 2)->where('is_deleted','<>', 1);
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    
    public function group(){
        return $this->hasOne(Group::class,'id','group_id');
    }
    
}
