<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Group extends Model
{
    protected $table = 'groups';
    protected $guarded = [];

    const GROUP_ADMIN = 1;
    const GROUP_MEMBER = 2;

    const S3PathLogo = 'groups/logos';
    const S3PathBan = 'groups/banners';
    protected $fillable = [
      'name'

    ];
    protected $appends = [
		'group_member','domains_array'
	];


	public function scopeActiveGroup($query){
	    return $query->where('status', 1)->where('is_deleted','<>', 1);
    }

    public function scopeDynamicAct($query){
        return $query->where('is_public', 2)->where('status', 1)->where('is_deleted','<>', 1);
    }

    public function getGroupMemberAttribute(){
        return Group_member::where('group_id',$this->id)->where('user_id', '<>',Auth::user()->id)->where('status', 2)->where('is_deleted','<>',1)->get();
    }

    public function getGroupMembersData(){

        $count = 0;
        $groupMembers = CommonModel::getAll('group_members', array(array('group_id', '=', $this->id), array('group_members.status', '=', 2), array('group_members.is_deleted', '=', 0)), array(array('users', 'users.id', 'group_members.user_id'), array('reviews', 'reviews.review_to', 'users.id')), '', '', array('group_members.id'));
        $members = [];
        if (count($groupMembers) > 0)
        foreach ($groupMembers as $member) {
            $hours = CommonModel::getAll('tracked_hours', array(
                array('volunteer_id', '=', $member->user_id),
                array('approv_status', '=', 1)), '', 'logged_mins', '', '');

            if (count($hours) > 0) {
                $member->impact = 0;
                foreach ($hours as $track) {
                    $member->impact += $track->logged_mins;
                }
            }
            else{
                $member->impact = 0;
            }
            if($count++ > 5) break; //limit to show some members due to less space

            $members[] = $member;
        }

        return $members;
    }

    public function getDomainsArray(){
        return explode(',', $this->applicable_domains);
    }

    public function users(){
        return $this->belongsToMany(User::class, 'group_members', 'group_id', 'user_id');
    }

    public function members(){
        return $this->hasMany(Group_member::class,'group_id', 'id'); //not used
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function category()
    {
        return $this->hasOne(Group_category::class, 'id', 'group_category_id');
    }

    public function getcategoryNameAttribute(){
        return '';
    }

    public static function getGroupCategory($members){

	    if($members < 21)
	        $cat = 'Micro';
	    elseif($members > 20 && $members < 51)
            $cat = 'Small';
        elseif($members > 50 && $members < 151)
            $cat = 'Medium';
        else
            $cat = 'Large';

        return $cat;
    }
    public function Group_member(){
        return $this->belongsTo(Group_member::class,'group_id','id');
      }

}
