function ValidateEmail(email) {
    //return true;
    var expr = /^([\w-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

function ValidatePassword(password) {
    if(password.length > 5){
        return true;
    }else {
        return false;
    }
};

function ValidateZipcode(number) {
    var expr = /([0-9]{5})/;
    return expr.test(number);
};

function ValidatePhoneNumber(number) {
    var expr = /^[0-9]{3}-[0-9]{3}-[0-9]{4}$/;
    // but make sure user doesn't actually submit 111-111-1111
    return expr.test(number) && number!='111-111-1111';
};

function ValidateDomain(domain) {
    //return true;
    var expr = /^((http|https):\/\/)?([a-zA-Z0-9_][-_a-zA-Z0-9]{0,62}\.)+([a-zA-Z0-9]{1,10})$/;
    return expr.test(domain);
};