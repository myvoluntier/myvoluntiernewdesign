/** 
 * Its help to search location within given radius 
 */
function showCloseLocations(data, icon, volunteerUrl) {
    var radius_circle;
    var markers_on_map = [];
    var i;
    var radius_miles = parseFloat($('#distance_radius').val()) * 1609.34;
    var address = $('#input_search_loc').val();

    var latlngbounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();
    var contentMenuString = '';
    contentMenuString = '<div class="col-12 col-md-3" id="one"><div class="container user_place">';

    //remove all radii and markers from map before displaying new ones
    if (radius_circle) {
        radius_circle.setMap(null);
        radius_circle = null;
    }
    for (i = 0; i < markers_on_map.length; i++) {
        if (markers_on_map[i]) {
            markers_on_map[i].setMap(null);
            markers_on_map[i] = null;
        }
    }

    if (geocoder) {
        geocoder.geocode({ 'address': address }, function(results, status) {
            //console.log('results lat', results[0].geometry.location.lat());
            //console.log('results lng', results[0].geometry.location.lng());
            if (status == google.maps.GeocoderStatus.OK) {
                if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                    var address_lat_lng = results[0].geometry.location;
                    // var address_lat_lng = {
                    //     lat: results[0].geometry.location.lat(),
                    //     lng: results[0].geometry.location.lng()
                    // }
                    radius_circle = new google.maps.Circle({
                        center: address_lat_lng,
                        radius: radius_miles,
                        clickable: false,
                        map: map
                    });
                    if (radius_circle) map.fitBounds(radius_circle.getBounds());

                    $.each(data.oppr, function(index, value) {
                        // var OpLatLng = { lat: parseFloat(value.lat), lng: parseFloat(value.lng) };
                        var OpLatLng = new google.maps.LatLng(
                          parseFloat(value.lat),
                          parseFloat(value.lng)
                        );
                        // console.log('address_lat_lng', address_lat_lng);
                        // console.log('OpLatLng', OpLatLng);
                        
                        var distance_from_location = google.maps.geometry.spherical.computeDistanceBetween(address_lat_lng, OpLatLng); //distance in meters between your location and the marker
                        //console.log('distance_from_location seersol', distance_from_location);
                        if (distance_from_location <= radius_miles) {

                            var marker = new google.maps.Marker({
                                position: OpLatLng,
                                map: map,
                                icon: icon
                            });

                            contentMenuString += '<a href="' + volunteerUrl + value.id + '">' + value.title + '</a><br><br>';
                            var contentMapString = '';
                            latlngbounds.extend(OpLatLng);
                            google.maps.event.addListener(marker, 'click', function() {
                                infowindow.close();
                                contentMapString = '<div class="map-modal-box"><div><div class="main-text">';
                                contentMapString = contentMapString +
                                    '<p class="h3">' + value.title + '</p>' +
                                    '<p><strong>Opportunity</strong></p> ' +
                                    '<p class="light"> ' + value.start_date + ' ' + value.street_addr1 + ',' + value.city + ',' + value.state + '</p> ' +
                                    '<p><a href="' + volunteerUrl + value.id + '">View opportunity info</a></p>';
                                contentMapString = contentMapString + '</div></div></div>';
                                infowindow.setContent(contentMapString);
                                infowindow.open(map, this);
                            });
                            markers_on_map.push(marker);
                        }

                    });
                    contentMenuString += '</div></div>';
                    $('#one').replaceWith(contentMenuString);
                    $(".green").html($(".user_place a").length);

                    map.fitBounds(latlngbounds);
                    map.setZoom(10);
                } else {
                    toastr.success("No results found while geocoding!", "Message");
                }
            } else {
                toastr.error("Geocode was not successful: " + status, "Error");
            }
        });
    } else {
        //console.log("not found");
    }
}

function setTypes() {
    var organization_type = $('#organization_type_select').val();
    var opportunity_type = $('#opportunity_type_select').val();
    $('.organization_type').val(organization_type);
    $('.opportunity_type').val(opportunity_type);
}

function isMarkerInArea(circle, marker){
   return (circle.getBounds().contains(marker.getPosition()));
}