jQuery(document).ready(function () {
    $('#btn_agree').on('click', function () {
        $('#o_accept_terms').prop('checked', true);
        $('#v_accept_terms').prop('checked', true);
    });

    $(document).ready(function () {
        $('.forgot_user_success').hide();
        $('.forgot_password_success').hide();

    });

    $('#btn_next').on('click', function () {
        var level = $('#level').val();
        level = parseInt(level) + 1;
        $('#level').val(level);

        $('.reg-first').hide();
        $('#btn_prev').show();
        if ($('select[name=account]').val() == 'Volunteer') {
            $('.reg-second').show();
            $('.reg-third').hide();
            $(this).hide();
            $('#btn_regs').show();
        } else {
            if (parseInt($('#level').val()) == 1) {
                $('.reg-select-org').show();
                $('.reg-second').hide();
                $('.reg-third').hide();
            } else if (parseInt($('#level').val()) == 2) {
                if ($('select[name=org_type]').val() == 1) {
                    $('.school-type').show();
                    $('.non-org-type').hide();
                    $('.ein-field').hide();
                    $('#p_org_name').text('School Name');
                }
                if ($('select[name=org_type]').val() == 2) {
                    $('.school-type').hide();
                    $('.non-org-type').hide();
                    $('.ein-field').show();
                    $('#p_org_name').text('Organization Name');
                }
                if ($('select[name=org_type]').val() == 3) {
                    $('.school-type').hide();
                    $('.non-org-type').show();
                    $('.ein-field').hide();
                    $('#p_org_name').text('Organization Name');
                }
                if ($('select[name=org_type]').val() == 4) {
                    $('.school-type').hide();
                    $('.non-org-type').hide();
                    $('.ein-field').hide();
                    $('#p_org_name').text('Organization Name');
                }
                $('.reg-select-org').hide();
                $('.reg-second').hide();
                $('.reg-third').show();
                $(this).hide();
                $('#btn_regs').show();
            }
        }
    });

    $("#o_accept_terms").on('click', function () {
        $("#ov_terms_alert").hide();
    });

    $("#op_accept_terms").on('click', function () {
        $("#ov_policy_alert").hide();
    });

    $('#btn_prev').on('click', function () {
        var level = $('#level').val();
        level = parseInt(level) - 1;
        $('#level').val(level);

        if ($('select[name=account]').val() == 'Volunteer') {
            $('.reg-first').show();
            $('.reg-second').hide();
            $('.reg-select-org').hide();
            $('.reg-third').hide();
            $(this).hide();
            $('#btn_regs').hide();
            $('#btn_next').show();
        } else {
            if (parseInt($('#level').val()) == 1) {
                $('.reg-select-org').show();
                $('.reg-second').hide();
                $('.reg-third').hide();
                $(this).show();
                $('#btn_regs').hide();
                $('#btn_next').show();
            } else {
                $('.reg-first').show();
                $('.reg-second').hide();
                $('.reg-select-org').hide();
                $('.reg-third').hide();
                $(this).hide();
                $('#btn_regs').hide();
                $('#btn_next').show();
            }
        }
    });

    $('#v_confirm').on('change', function () {
        if ($('#v_password').val() != $(this).val()) {
            $(this).addClass("error-has");
        }
    });

    $('#o_confirm').on('change', function () {
        if ($('#o_password').val() != $(this).val()) {
            $(this).addClass("error-has");
        }
    });

    $('.btn_regs').on('click', function () {
        var vol_flags = 0;
        var org_flags = 0;
        if ($('input[name="loginAS"]:checked').val() == 'volunteer') {
            if ($('#first_name').val() == '' || $.isNumeric($('#first_name').val())) {
                $('#first_name').addClass("error-has");
                vol_flags++;
            }
            else {
                $('#first_name').removeClass("error-has");
            }
            if ($('#last_name').val() == '' || $.isNumeric($('#last_name').val())) {
                $('#last_name').addClass("error-has");
                vol_flags++;
            } else {
                $('#last_name').removeClass("error-has");
            }

            if ($('#birth_day').val() == '') {
                $('#birth_day').addClass("error-has");
                vol_flags++;
            } else {
                $('#birth_day').removeClass("error-has");
            }

            if ($('#birth_day').val() != '') {
                var birthday = $('#birth_day').val();
                var now = new Date();
                var past = new Date(birthday);
                var nowYear = now.getFullYear();
                var pastYear = past.getFullYear();
                var age = nowYear - pastYear;
                if (age < 13) {
                    vol_flags++;
                    $('#v_invalid_age').show();
                }
                else {
                    $('#v_invalid_age').hide();
                }
            }

            if ($('#v_zipcode').val() == '') {
                $('#v_zipcode').addClass("error-has");
                vol_flags++;

            } else {
                $('#v_zipcode').removeClass("error-has");
            }

            if (!ValidateZipcode($('#v_zipcode').val())) {
                vol_flags++;
                $('#v_invalid_zipcode_alert').show();
            } else {
                $('#v_invalid_zipcode_alert').hide();
                $('#v_invalid_zipcode_alert').removeClass("error-has");
            }

            if ($('#v_email').val() == '') {
                $('#v_email').addClass("error-has");
                vol_flags++;
            } else {
                $('#v_email').removeClass("error-has");
            }

            if (!ValidateEmail($('#v_email').val())) {
                vol_flags++;
                $('#v_invalid_email_alert').show();
                $('#v_existing_email_alert').hide();
            } else {
                $('#v_invalid_email_alert').hide();
                $('#v_existing_email_alert').hide();
            }

            $('#v_invalid_email2_alert, #v_invalid_email3_alert').hide()
            $('#v_email2, #v_email3').css('border', '');
            if ($('#v_email2').val() != "" && !ValidateEmail($('#v_email2').val())) {
                vol_flags++;
                $('#v_invalid_email2_alert').show();
                $('#v_email2').addClass("error-has");
            }
            if ($('#v_email3').val() != "" && !ValidateEmail($('#v_email3').val())) {
                vol_flags++;
                $('#v_invalid_email3_alert').show();
                $('#v_email3').addClass("error-has");
            }

            if ($('#v_contact_num').val() == '') {
                $('#v_contact_num').addClass("error-has");
                vol_flags++;
            } else {
                $('#v_contact_num').removeClass("error-has");
            }

            if (!ValidatePhoneNumber($('#v_contact_num').val())) {
                vol_flags++;
                $('#v_invalid_contact_number').show();
            }
            else {
                $('#v_invalid_contact_number').hide();
                $('#v_invalid_contact_number').removeClass("error-has");
            }

            if ($('#v_password').val() == '') {
                $('#v_password').addClass("error-has");
                vol_flags++;
            } else {
                $('#v_password').removeClass("error-has");
            }


            if (!ValidatePassword($('#v_password').val())) {
                vol_flags++;
                $('#v_invalid_password').show();
            } else {
                $('#v_invalid_password').hide();
                $('#v_invalid_password').removeClass("error-has");
            }

            if ($('#v_confirm').val() != $('#v_password').val()) {
                $('#v_confirm').addClass("error-has");
                vol_flags++;
            } else {
                $('#v_confirm').removeClass("error-has");
                $('#v_confirm').css("border", '');
            }


            if ($("#verify_aga").is(":not(:checked)")) {
                $("#verify_age_alert").show();
                $("#verify_age_alert").css("color", "red");
                vol_flags++;
            } else {
                $("#verify_age_alert").hide();
                $('#verify_age_alert').removeClass("error-has");
            }

            if ($('.years-old-check-value').val() == 0) {

            } else {
                $('.years-old-check-value').removeClass("error-has");
            }

            if ($('.terms-conditions-vol').val() == 0) {

            } else {
                $('.years-old-check-value').removeClass("error-has");
            }
            if ($("#v_accept_terms").is(":not(:checked)")) {
                $("#v_terms_alert").show();
                $("#v_terms_alert").css("color", "red");
                vol_flags++;
            } else {
                $('#v_terms_alert').removeClass("error-has");
            }
            if ($("#p_accept_policy").is(":not(:checked)")) {
                $("#v_policy_alert").show();
                $("#v_policy_alert").css("color", "red");
                vol_flags++;
            } else {
                $('#v_policy_alert').removeClass("error-has");
            }

            // alert(vol_flags);
            if (vol_flags == 0) {

                var url = API_URL + 'reg_volunteer';
                $(".p_invalid").hide();
                $(".error-has").removeClass("error-has");
                $(".btn_regs").hide();
                $(".btn_regs_loading").show();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var type = "POST";
                var formData = {
                    //user_name: $("#v_user_name").val(),
                    first_name: $('#first_name').val(),
                    last_name: $('#last_name').val(),
                    birth_day: $('#birth_day').val(),
                    zipcode: $('#v_zipcode').val(),
                    email: $('#v_email').val(),
                    email2: $('#v_email2').val(),
                    email3: $('#v_email3').val(),
                    contact_number: $('#v_contact_num').val(),
                    password: $('#v_password').val(),
                    gender: $("input[name='gender']:checked").val(),
                    middle_initial: $('#middle_initial').val(),
                    auto_accept_friends: '0'
                };

                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    success: function (data) {
                        $(".btn_regs").show();
                        $(".btn_regs_loading").hide();
                        // if(data.result == 'username exist'){
                        //     $('#v_invalid_username_alert').show();
                        //     //$("#v_user_name").val('');
                        // }
                        if (data.result == 'email exist') {
                            $('#v_existing_email_alert').show();
                            //$("#v_email").val('');
                        }
                        if (data.result == 'invalid zipcode') {
                            $("#v_zipcode").addClass("error-has");
                            $('#v_location_zipcode_alert').show();
                            $("#v_zipcode").val('');
                        }
                        if (data.result == 'success') {
                            $("#register_success_div").text("Volunteer successfully inserted");
                            $(".register_div").show();
                            setTimeout(function () {
                                window.location.replace(SITE_URL + "thank-you");
                            }, 2000);
                            // $('#myModalSuccessfulReg').modal('show');

                            // $('.close').click();
                            // $('#myModalSingVolRegistration').hide();
                            // $('#btn_prev').hide();
                            // $('#btn_regs').hide();
                            // $('.reg-forth').show();
                            // $('#btn_ok').show();
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }
        else {

            $('#o_invalid_username_alert').hide();
            // if ($("#o_user_name").val() == ''){
            //     $("#o_user_name").css("border","1px solid #ff0000");
            //     org_flags++;
            // }

            if ($("select[name=org_type]").val() == '') {
                $(".organizationType").addClass("error");
                org_flags++;
            } else {
                $('.organizationType').removeClass("error");
            }

            if ($("#org_name").val() == '') {
                $("#org_name").addClass("error-has");
                org_flags++;
            } else {
                $('#org_name').removeClass("error-has");
            }

            if ($("#found_day").val() == '') {
                $("#found_day").addClass("error-has");
                org_flags++;
            } else {
                $('#found_day').removeClass("error-has");
            }
            if ($("#o_zipcode").val() == '') {
                $("#o_zipcode").addClass("error-has");
                org_flags++;
            }

            if (!ValidateZipcode($('#o_zipcode').val())) {
                org_flags++;
                $('#o_invalid_zipcode_alert').show();
            } else {
                $('#o_invalid_zipcode_alert').hide();
                $('#o_zipcode').removeClass("error-has");
            }
            if ($("#o_email").val() == '') {
                $("#o_email").addClass("error-has");
                org_flags++;
            }
            if (!ValidateEmail($('#o_email').val())) {
                org_flags++;
                $('#o_invalid_email_alert').show();
            } else {
                $('#o_invalid_email_alert').hide();
                $('#o_email').removeClass("error-has");
            }


            if ($("#o_contact_num").val() == '') {
                $("#o_contact_num").addClass("error-has");
                org_flags++;
            }

            if (!ValidatePhoneNumber($('#o_contact_num').val())) {
                org_flags++;
                $('#o_invalid_contact_number').show();
            } else {
                $('#o_invalid_contact_number').hide();
                $('#o_contact_num').removeClass("error-has");
            }

            if ($("#o_password").val() == '') {
                $("#o_password").addClass("error-has");
                org_flags++;
            }
            if (!ValidatePassword($('#o_password').val())) {
                org_flags++;
                $('#o_invalid_password_alert').show();
            } else {
                $('#o_password').removeClass("error-has");
            }
            if ($("#o_confirm").val() == '') {
                $("#o_confirm").addClass("error-has");
                org_flags++;
            }
            // if ($("#o_accept_terms").is(":not(:checked)")) {
            //     console.log("org terms not vchekced ");
            //     $("#o_terms_alert").show();
            //     $("#o_terms_alert").css("color", "red");
            //     org_flags++;
            // }
            if ($('#o_password').val() != $('#o_confirm').val()) {
                $("#o_confirm").addClass("error-has");
                org_flags++;
            } else {
                $('#o_confirm').removeClass("error-has");
            }

            if ($("#o_accept_terms").is(":not(:checked)")) {
                $("#ov_terms_alert").show();
                $("#ov_terms_alert").css("color", "red");
                org_flags++;
            } else {
                $('#ov_terms_alert').removeClass("error-has");
            }
            if ($("#op_accept_terms").is(":not(:checked)")) {
                $("#ov_policy_alert").show();
                $("#ov_policy_alert").css("color", "red");
                org_flags++;
            } else {
                $('#ov_policy_alert').removeClass("error-has");
            }
            $(".select2", ".parentOrgRego").css("border", "");
            if ($('#type-user-select-id').val() == 'SubOrganization' && $('#parentOrgSelRego').val() == "") {
                org_flags++;
                $(".select2", ".parentOrgRego").css("border", "1px solid #ff0000");
                toastr.error("Please select parent organization.", "Error");
            }
            if (org_flags == 0) {
                var url = API_URL + 'reg_organization';
                $(".p_invalid").hide();
                $(".error-has").removeClass("error-has");
                $(".btn_regs").hide();
                $(".btn_regs_loading").show();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var type = "POST";
                var formData = {
                    // user_name: $("#o_user_name").val(),
                    parent_org: $('#parentOrgSelRego').val(),
                    org_name: $('#org_name').val(),
                    school_type: $('#school_type option:selected').val(),
                    ein: $('#org_ein').val(),
                    nonprofit_org_type: $('#non_org_type').val(),
                    birth_day: $('#found_day').val(),
                    zipcode: $('#o_zipcode').val(),
                    org_type: $("#org_type option:selected").val(),
                    email: $('#o_email').val(),
                    contact_number: $('#o_contact_num').val(),
                    password: $('#o_password').val(),
                    auto_accept_friends: '0',
                };

                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    success: function (data) {
                        $(".btn_regs").show();
                        $(".btn_regs_loading").hide();
                        // if(data.result == 'username exist'){
                        //     $('#o_invalid_username_alert').show();
                        //     //$("#o_user_name").val('');
                        // }
                        if (data.result == 'email exist') {
                            $('#o_existing_email_alert').show();
                            //$("#o_email").val('');
                        }
                        if (data.result == 'invalid zipcode') {
                            $("#o_zipcode").addClass("error-has");
                            $('#o_location_zipcode_alert').show();
                            $("#o_zipcode").val('');
                        }
                        if (data.result == 'success') {
                            $("#register_success_div").text("Organization successfully inserted");
                            $(".register_div").show();
                            setTimeout(function () {
                                window.location.replace(SITE_URL + "thank-you");
                            }, 2000);
                            // $('#myModalSuccessfulReg').modal('show')
                            // $('.close').click();
                            // $('#myModalSingOrgRegistration').hide();
                            // $('.reg-third').hide();
                            // $('#btn_prev').hide();
                            // $('#btn_regs').hide();
                            // $('.reg-forth').show();
                            // $('#btn_ok').show();
                        }
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        }
    });

    $('#login_password').keyup(function (e) {
        if (e.which == 13) {
            $("#btn_login_l").click()
        }
    });

    $('#btn_login_l').on('click', function () {

        var count = 0;
        $(".select2-container--bootstrap,#login_password,#login_user").removeClass("error");
        if ($('input[name="loginAS"]:checked').val() == "") {
            $(".select2-container--bootstrap .select2-selection--single", "#myModalLogin").addClass("error");
            count++;
        } else {

            if ($('input[name="loginAS"]:checked').val() == 'organization') {
                if ($('#organizationSelect').val() == '') {
                    $(".select2-container--bootstrap", ".newOrgWrapper").addClass("error");
                    count++;
                }
            }

            if ($('input[name="loginAS"]:checked').val() == 'suborganization') {
                if ($('#parentOrgSel').val() == '') {
                    $(".select2-container--bootstrap", ".parentOrgWrapper").addClass("error");
                    count++;
                }
                if ($('#subOrgSel').val() == '') {
                    $(".select2-container--bootstrap", ".subOrgWrapper").addClass("error");
                    count++;
                }
            }

            $(".select2-container--bootstrap .select2-selection--single", "#myModalLogin").removeClass("error");
            if ($("#login_user").val() == '') {
                $("#login_user").addClass("error");
                count++;
            }
            else {
                $("#login_user").removeClass("error");
            }

            if ($("#login_password").val() == '') {
                $("#login_password").addClass("error");
                count++;
            }
            else {
                $("#login_password").removeClass("error");
            }
        }
        if (count > 0) {
            return false;
        }

        var url = API_URL + 'login_user';
        $("#btn_login_l").hide();
        $("#btn_login_loading").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var type = "POST";
        var formData = {
            user_id: $("#login_user").val(),
            password: $("#login_password").val(),
            login_as: $('input[name="loginAS"]:checked').val(),
            parent_user_id: $("#parent_user_id").val(),
            organization_id: $("#organizationSelect").val()
        };
        //console.log(formData);

        $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function (data) {
                $("#btn_login_l").show();
                $("#btn_login_loading").hide();
                if (data.errors) {
                    $("#login_errors_div").text(data.message);
                    $(".login_error_div").show();
                }
                else {
                    window.location.replace(SITE_URL + data.role);
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });



    $('#forgot_user').on('click', function () {

        if ($("#get_user_email").val() == '')
            $("#get_user_email").css("border", "1px solid #ff0000");
        var url = API_URL + 'forgot_username';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var type = "POST";
        var formData = {
            email: $("#get_user_email").val(),
        };
        $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function (data) {
                if (data.result == 'success') {
                    $('.forgot_user_form').hide();
                    $('.forgot_user_success').show();
                    $('#forgot_user').hide();
                    $('#forgot_user_close').show();
                }
                if (data.result == 'email_not_exist') {
                    $('#forgot_user_invalid_email').show();
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    $('#forgot_password_close').on('click', function () {
        $("#myModalForgotPassword").modal('hide');
    });

    // $('#loginASForg').on('change', function () {

    //     if (this.value == 'organization') {

    //         $(".forgot_pass_input_email").hide();
    //         $(".forgot_pass_input_name").show();
    //         $("#forgetHeading").text('Organization Name:');
    //     }
    //     else {
    //         $(".forgot_pass_input_email").show();
    //         $(".forgot_pass_input_name").hide();
    //         $("#forgetHeading").text('Email:');
    //     }
    // });

    $('input[type=radio][name=loginASForg]').on('change', function () {
        if ($("input[name='loginASForg']:checked").val() == "organization") {
            $(".forgot_pass_input_name").show();
            $(".organizationDivS").show();
            $(".parentOrganizationDiv").hide();
            $(".sub_OrganizationDiv").hide();
        }
        else if ($("input[name='loginASForg']:checked").val() == "suborganization") {
            $(".forgot_pass_input_name").show();
            $(".organizationDivS").hide();
            $(".sub_OrganizationDiv").show();
            $(".parentOrganizationDiv").show();
        }
        else {
            $(".forgot_pass_input_name").hide();
            $(".organizationDivS").hide();
            $(".parentOrganizationDiv").hide();
            $(".sub_OrganizationDiv").hide();
        }

    });


    $('#forgot_password').on('click', function () {
        var flag = 0;
        if ($('input[name="loginASForg"]:checked').val() == 'organization') {
            if ($("#choose_org").val() == '') {
                $(".organizationDivS").addClass("error");
                flag++;
            } else {
                $(".organizationDivS").removeClass("error");

            }
            if ($("#get_password_email").val() == '') {
                $("#get_password_email").addClass("error");
                flag++;
            } else {
                $("#get_password_email").removeClass("error");
            }

        }
        else if ($('input[name="loginASForg"]:checked').val() == 'volunteer') {
            if ($("#get_password_email").val() == '') {
                $("#get_password_email").addClass("error");
                flag++;
            } else {
                $("#get_password_email").removeClass("error");
            }

        }
        else if ($('input[name="loginASForg"]:checked').val() == 'suborganization') {
            if ($("#parentOrgSel").val() == '') {
                $(".parentOrganizationDiv").addClass("error");
                flag++;
            } else {
                $(".parentOrganizationDiv").removeClass("error");
            }
            if ($("#subOrgSel").val() == '') {
                $(".subOrgWrapper").addClass("error");
                flag++;
            } else {
                $(".subOrgWrapper").removeClass("error");
            }

        }
        if ($('input[name="loginASForg"]:checked').val() == '') {
            $(".select2-container--bootstrap .select2-selection--single", "#myModalForgotPassword").css("border", "1px solid #ff0000");
            flag++;
        } else {
            $(".select2-container--bootstrap .select2-selection--single", "#myModalForgotPassword").css("border", "");
        }

        if (flag == 0) {
            var url = API_URL + 'forgot_password';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#forgot_password").hide();
            $("#forgot_password_loading").show();

            var type = "POST";
            var formData = {
                parentOrg: $("#parentOrgSel").val(),
                subOrg: $("#subOrgSel").val(),
                email: $("#get_password_email").val(),
                name: $("#choose_org").val(),
                loginas: $('input[name="loginASForg"]:checked').val()
            };
            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function (data) {

                    $("#forgot_password").show();
                    $("#forgot_password_loading").hide();
                    if (data.result == 'success') {
                        $('.forgot_password_form').hide();
                        $('#forgot_password').hide();
                        $('#forgot_password_invalid_email').hide();
                        $('#forgot_password_close').show();
                        $('.input-email').hide();
                        $('.lab-email').hide();
                        $('.loginAsDiv').hide();
                        window.location.replace(SITE_URL + "thank-you/forget_pass");
                    }
                    if (data.result == 'email_not_exist') {
                        $('#forgot_password_invalid_email').text(data.message);
                        $('#forgot_password_invalid_email').show();
                    }
                    else if (data.result == 'name_not_exist') {
                        $('#forgot_password_invalid_name').text(data.message);
                        $('#forgot_password_invalid_name').show();
                    }
                    else {
                        $('#forgot_password_invalid_subOrg_name').text(data.message);
                        $('#forgot_password_invalid_subOrg_name').show();
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });

    $("#o_accept_terms").on('click', function () {
        $("#o_terms_alert").hide();
    });

    $("#verify_aga").on('click', function () {
        $("#verify_age_alert").hide();
    });

    $("#v_accept_terms").on('click', function () {
        $("#v_terms_alert").hide();
    });

    $("#p_accept_policy").on('click', function () {
        $("#v_policy_alert").hide();
    });

    $('.form-control').on('click', function () {
        $(this).css("border", "1px solid #e5e6e7");
        $(this).parent().find('.p_invalid').hide();
    });

    var cbpAnimatedHeader = (function () {
        var docElem = document.documentElement,
            header = document.querySelector('.navbar-default'),
            didScroll = false,
            changeHeaderOn = 200;
        function init() {
            window.addEventListener('scroll', function (event) {
                if (!didScroll) {
                    didScroll = true;
                    setTimeout(scrollPage, 250);
                }
            }, false);
        }
        function scrollPage() {
            var sy = scrollY();
            if (sy >= changeHeaderOn) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }
        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        init();

    })();
});
$('.vol_invite').on('click', function () {
    var vol_flags = 0;
    var org_flags = 0;

    if ($('#first_name').val() == '') {
        $('#first_name').addClass("error-has");
        vol_flags++;
    }
    else {
        $('#first_name').removeClass("error-has");
    }

    if ($('#last_name').val() == '') {
        $('#last_name').addClass("error-has");
        vol_flags++;
    } else {
        $('#last_name').removeClass("error-has");
    }

    if ($('#birth_day').val() == '') {
        $('#birth_day').addClass("error-has");
        vol_flags++;
    } else {
        $('#birth_day').removeClass("error-has");
    }

    if ($('#birth_day').val() != '') {
        var birthday = $('#birth_day').val();
        var now = new Date();
        var past = new Date(birthday);
        var nowYear = now.getFullYear();
        var pastYear = past.getFullYear();
        var age = nowYear - pastYear;
        if (age < 13) {
            vol_flags++;
            $('#v_invalid_age').show();
        }
        else {
            $('#v_invalid_age').hide();
        }
    }

    if ($('#v_zipcode').val() == '') {
        $('#v_zipcode').addClass("error-has");
        vol_flags++;

    } else {
        $('#v_zipcode').removeClass("error-has");
    }

    if (!ValidateZipcode($('#v_zipcode').val())) {
        vol_flags++;
        $('#v_invalid_zipcode_alert').show();
    } else {
        $('#v_invalid_zipcode_alert').hide();
        $('#v_invalid_zipcode_alert').removeClass("error-has");
    }

    if ($('#v_email').val() == '') {
        $('#v_email').addClass("error-has");
        vol_flags++;
    } else {
        $('#v_email').removeClass("error-has");
    }

    if (!ValidateEmail($('#v_email').val())) {
        vol_flags++;
        $('#v_invalid_email_alert').show();
        $('#v_existing_email_alert').hide();
    } else {
        $('#v_invalid_email_alert').hide();
        $('#v_existing_email_alert').hide();
    }

    $('#v_invalid_email2_alert, #v_invalid_email3_alert').hide()
    $('#v_email2, #v_email3').css('border', '');
    if ($('#v_email2').val() != "" && !ValidateEmail($('#v_email2').val())) {
        vol_flags++;
        $('#v_invalid_email2_alert').show();
        $('#v_email2').addClass("error-has");
    }
    if ($('#v_email3').val() != "" && !ValidateEmail($('#v_email3').val())) {
        vol_flags++;
        $('#v_invalid_email3_alert').show();
        $('#v_email3').addClass("error-has");
    }

    if ($('#v_contact_num').val() == '') {
        $('#v_contact_num').addClass("error-has");
        vol_flags++;
    } else {
        $('#v_contact_num').removeClass("error-has");
    }

    if (!ValidatePhoneNumber($('#v_contact_num').val())) {
        vol_flags++;
        $('#v_invalid_contact_number').show();
    }
    else {
        $('#v_invalid_contact_number').hide();
        $('#v_invalid_contact_number').removeClass("error-has");
    }

    if ($('#v_password').val() == '') {
        $('#v_password').addClass("error-has");
        vol_flags++;
    } else {
        $('#v_password').removeClass("error-has");
    }


    if (!ValidatePassword($('#v_password').val())) {
        vol_flags++;
        $('#v_invalid_password').show();
    } else {
        $('#v_invalid_password').hide();
        $('#v_invalid_password').removeClass("error-has");
    }

    if ($('#v_confirm').val() != $('#v_password').val()) {
        $('#v_confirm').addClass("error-has");
        vol_flags++;
    } else {
        $('#v_confirm').removeClass("error-has");
        $('#v_confirm').css("border", '');
    }


    if ($("#verify_aga").is(":not(:checked)")) {
        $("#verify_age_alert").show();
        $("#verify_age_alert").css("color", "red");
        vol_flags++;
    } else {
        $("#verify_age_alert").hide();
        $('#verify_age_alert').removeClass("error-has");
    }

    if ($('.years-old-check-value').val() == 0) {

    } else {
        $('.years-old-check-value').removeClass("error-has");
    }

    if ($('.terms-conditions-vol').val() == 0) {

    } else {
        $('.years-old-check-value').removeClass("error-has");
    }
    if ($("#v_accept_terms").is(":not(:checked)")) {
        $("#v_terms_alert").show();
        $("#v_terms_alert").css("color", "red");
        vol_flags++;
    } else {
        $('#v_terms_alert').removeClass("error-has");
    }
    if ($("#p_accept_policy").is(":not(:checked)")) {
        $("#v_policy_alert").show();
        $("#v_policy_alert").css("color", "red");
        vol_flags++;
    } else {
        $('#v_policy_alert').removeClass("error-has");
    }

    // alert(vol_flags);
    if (vol_flags == 0) {

        var url = API_URL + 'reg_volunteer';
        $(".p_invalid").hide();
        $(".error-has").removeClass("error-has");
        $(".vol_invite").hide();
        $(".vol_invite_loading").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var type = "POST";
        var formData = {
            //user_name: $("#v_user_name").val(),
            org_id: $('#org_id').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            birth_day: $('#birth_day').val(),
            zipcode: $('#v_zipcode').val(),
            email: $('#v_email').val(),
            email2: $('#v_email2').val(),
            email3: $('#v_email3').val(),
            contact_number: $('#v_contact_num').val(),
            password: $('#v_password').val(),
            gender: $("input[name='gender']:checked").val(),
            middle_initial: $('#middle_initial').val(),
            auto_accept_friends: '0'
        };

        $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function (data) {
                $(".vol_invite").show();
                $(".vol_invite_loading").hide();
                if (data.result == 'email exist') {
                    $('#v_existing_email_alert').show();
                }
                if (data.result == 'invalid zipcode') {
                    $("#v_zipcode").addClass("error-has");
                    $('#v_location_zipcode_alert').show();
                    $("#v_zipcode").val('');
                }
                if (data.result == 'success') {
                    $("#register_success_div").text("Volunteer successfully inserted");
                    $(".register_div").show();
                    setTimeout(function () {
                        var strMD5 = $('#org_id').val() + "@" + data.user_id;
                        window.location.replace(SITE_URL + "affiliate-landing-page/" + btoa(strMD5));
                    }, 2000);
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }


});
// Activate WOW.js plugin for animation on scrol
// new WOW().init();