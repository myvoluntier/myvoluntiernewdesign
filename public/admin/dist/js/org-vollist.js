
$('.adminMailSend').on('click', function(e) {
     $("#cke_message_txt").css("border", "");
   // CKEDITOR.instances['message_txt'].setData('');

    $("#orgEmail_loading").hide();
    $('.success-first').hide();
    $('.hide-email-comment').show();
    $('#orgEmailBtn').show();
    $('#orgEmail_hide').hide();

    $('#emaiIdToSend').val($(this).attr('id'))
    $('.top-50').css('margin-top', '0px');
    CKEDITOR.instances['message_txt'].setData('');
    $('#errorEmailBlast').remove();
    $('#adminEmailSendBox').modal('show');
});
$('#orgEmailBtn').on('click', function() {
    var flags = 0;
    
    $(".select2, #cke_message_txt","#orgEmail").css("border", "");
    if (CKEDITOR.instances['message_txt'].getData() == '') {
        $("#cke_message_txt").css("border", "1px solid #ff0000");
        flags++;
    }
   
    $('#errorEmailBlast').remove();
    if (flags == 0) {
        $("#orgEmailBtn").hide();
        $("#orgEmail_loading").show();
        
        var formData = {
            message: CKEDITOR.instances['message_txt'].getData(),
            userid: $('#emaiIdToSend').val()
        }
       
        var url = API_URL + 'admin_email_send';
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            success: function(data) {
                $("#orgEmail_loading").hide();
                if(data.result == 'error'){
                    $("#orgEmailBtn").show();
                    $("#orgEmail_loading").hide();
                    $('.hide-email-comment','#orgEmail').prepend('<span id="errorEmailBlast" style="color:red;">'+data.message+'</span>');
                    toastr.error(data.message, "Error");
                }else{
                    $('.success-first').show();
                    $('.hide-email-comment').hide();
                    $("#verifiedDomain").val("").trigger('change');
                    CKEDITOR.instances['message_txt'].setData('');
                    $('#orgEmailBtn').hide();
                    $('#orgEmail_hide').show();
                    $('.top-50').css('margin-top', '50px');
                }
            },
            error: function(data) {
                $("#orgEmailBtn").show();
                $("#orgEmail_loading").hide();
                //console.log('Error:', data);
                toastr.error("Something went wrong.", "Error");
            }
        });
    }
});
$('#orgEmail_hide').on('click', function() {
    $('#adminEmailSendBox').modal('hide')
});