$('document').ready(function() {
    if (window.location.href.indexOf("subOrganization") > -1) {
        $('#subOrganizationTab').trigger('click');
    }
});

$('.closeModal').on('click', function() {
    $('#myModalPartner').modal('hide');
});

$('.add_partner').on('click', function(e) {
    $('input', '#partnerBody').val('');
    $('.form-group', '#partnerBody').removeClass('hide');
    $('.wrapper_input .form-control').css("border", "");
    $(".select2-container", "#partnerBody").css("border", "");
    
    scroll(0, 0);
    setTimeout(function() {
        e.preventDefault();
       // var text = 'Organization';
       // $('.type-user').val(text);
        $('#myModalPartner').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function() {
            $('body').addClass('modal-open');
        }, 500);

    }, 200)
});

bindElement();

$('.btn_partner').on('click', function() {
    var flag = 0;

  //  console.log($("#attributekey").val().trim()+'---');
    $('.p_invalid').hide();
    $('.wrapper_input .form-control').css("border", "");
    $(".select2-container", "#partnerBody").css("border", "");
    if ($("#partnerkey").val().trim() == "") {
        $(".select2-container", "#partnerBody").css("border", "1px solid #ff0000");
        flag++;
    }
   
    if (flag == 0) {
        var url = API_URL + 'add-partner';

        $(".btn_partner, .closeModal").hide();
        $(".btn_regs_loading").show();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var type = "POST";
        var formData = {
            selPartnerId : $('#selPartnerId').val(),
            partnerid: $("#partnerkey").val(),
        };

        $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function(data) {
                $(".btn_partner, .closeModal").show();
                $(".btn_regs_loading").hide();
                
                if (data.result == 'success') {
                    window.location.href = $('#partnerURL').val();
                    location.reload();
                }
                if (data.result == 'exist') {
                    toastr.error("Selected partner already exist..!", "Error");
                  
                }else if (data.result == 'invalid_id') {
                    toastr.error("Record not found for sent id..!", "Error");
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    }

});

$('.editPartnerBtn').on('click', function() {
    $.ajax({
        type: "GET",
        url: API_URL + 'edit-partner/' + $(this).attr('data-partnerid'),
        success: function(data) {
            //$('#subOrgBody').html('');
            $('#partnerBody').html(data);
            $('#myModalPartner').modal('show');
            bindElement();
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
});

function bindElement() {
    $("select").select2({
        theme: "bootstrap",
        minimumResultsForSearch: -1
    });
}
/////////////////////////////////////////////////