$('document').ready(function () {
    if (window.location.href.indexOf("orgDelegates") > -1) {
        $('#orgDelegatesTab').trigger('click');
    }
});

$('.btn_delegate_close').on('click', function () {
    $('#delegateRegistrationModal').modal('hide');
});

$('.registerDelegate').on('click', function (e) {
    $('#delegateRegistrationModal').modal('show');
});


$('.btn_delegate_save').on('click', function () {
    var org_flags = 0;

    $('.p_invalid').hide();
    $('.wrapper_input .form-control').css("border", "");

    if ($("#del_first_name").val() == '') {
        $("#del_first_name").css("border", "1px solid #ff0000");
        org_flags++;
    }
    if ($("#del_last_name").val() == '') {
        $("#del_last_name").css("border", "1px solid #ff0000");
        org_flags++;
    }

    if ($("#del_email").val() == '' || !ValidateEmail($('#del_email').val())) {
        $("#del_email").css("border", "1px solid #ff0000");
        $('#o_invalid_del_email_alert').show();
        org_flags++;
    }

    if ($("#del_comments").val() == '') {
        $("#del_comments").css("border", "1px solid #ff0000");
        org_flags++;
    }

    if (org_flags == 0) {
        var url = API_URL + 'reg_delegate';

        $(".btn_delegate_save, .btn_delegate_close").hide();
        $(".btn_delegate_loading").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var type = "POST";
        var formData = {
            delegate_id: $("#delegate_id").val(),
            first_name: $("#del_first_name").val(),
            last_name: $("#del_last_name").val(),
            email: $("#del_email").val(),
            comments: $("#del_comments").val(),
        };

        $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function (data) {
                $(".btn_delegate_save, .btn_delegate_close").show();
                $(".btn_delegate_loading").hide();

                if (data.result == 'email exist') {
                    $('#o_existing_del_email_alert').show();
                    $('#del_email').css('border', '1px solid #ff0000');
                }

                if (data.result == 'success') {
                    window.location.href = $('#delegatesURL').val();
                    location.reload();
                }

                if (data.result == 'error') {
                    toastr.error("Something went wrong please contact admin.", "Message");
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

});

$('.editDelegateBtn').on('click', function () {
    $("#delegate_id").val($(this).attr('data-delId'));
    $("#del_first_name").val($(this).attr('data-delFirst'));
    $("#del_last_name").val($(this).attr('data-delLast'));
    $("#del_email").val($(this).attr('data-delEmail'));
    $("#del_comments").val($(this).attr('data-delComments'));

    $('#del_email').prop('readonly', true);

    $('#delegateRegistrationModal').modal('show');
    bindElement();
});

/////////////////////////////////////////////////