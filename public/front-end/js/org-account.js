$('#selectType').on('change',function(){
    $('#verifiedDomain, #groupEmailBlast').parent().parent().hide();
    if($(this).val() == 'verifiedDomain'){
        $('#verifiedDomain').parent().parent().show();
    }else if($(this).val() == 'groupEmailBlast'){
        $('#groupEmailBlast').parent().parent().show();
    }
});
$('.massDomainMail').on('click', function(e) {
     $("#selectType, #verifiedDomain, #groupEmailBlast").val("").trigger('change');
    $('#verifiedDomain, #groupEmailBlast').parent().parent().hide();
    $("#cke_message_vol_members").css("border", "");
   // CKEDITOR.instances['message_vol_members'].setData('');

    $("#orgEmailBlast_loading").hide();
    $('.success-first').hide();
    $('.hide-email-comment').show();
    $('#orgEmailBlastBtn').show();
    $('#orgEmailBlast_hide').hide();

    $('#opportunitiIdMail').val($(this).data('opportunitiIdMail'))
    $('.top-50').css('margin-top', '0px');
    $("#verifiedDomain").val("").trigger('change');
    CKEDITOR.instances['message_vol_members'].setData('');
    $('#errorEmailBlast').remove();
    $('#orgEmailBlast').modal('show');
});
$('#orgEmailBlastBtn').on('click', function() {
    var flags = 0;
    
    $(".select2, #cke_message_vol_members","#orgEmailBlast").css("border", "");
    if (CKEDITOR.instances['message_vol_members'].getData() == '') {
        $("#cke_message_vol_members").css("border", "1px solid #ff0000");
        flags++;
    }
    if($('#selectType').val() == '' ){
        $(".select2","#selectTypeDiv").css("border", "1px solid #ff0000");
        flags++;
    }
    if($('#verifiedDomain').val() == "" && $('#selectType').val() == 'verifiedDomain' ){
        $(".select2","#verifyDomainDiv").css("border", "1px solid #ff0000");
        flags++;
    }
    if($('#groupEmailBlast').val() == "" && $('#selectType').val() == 'groupEmailBlast' ){
        $(".select2","#groupEmailBlastDiv").css("border", "1px solid #ff0000");
        flags++;
    }
    $('#errorEmailBlast').remove();
    if (flags == 0) {
        $("#orgEmailBlastBtn").hide();
        $("#orgEmailBlast_loading").show();
        if($('#selectType').val() == 'verifiedDomain'){
            var formData = {
                verifiedDomain: $('#verifiedDomain').val(),
                message: CKEDITOR.instances['message_vol_members'].getData(),
            }
        }else{
            var formData = {
                groups: $('#groupEmailBlast').val(),
                message: CKEDITOR.instances['message_vol_members'].getData(),
            }
        }
        var url = API_URL + 'organization_email_blast';
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            success: function(data) {
                $("#orgEmailBlast_loading").hide();
                if(data.result == 'error'){
                    $("#orgEmailBlastBtn").show();
                    $("#orgEmailBlast_loading").hide();
                    $('.hide-email-comment','#orgEmailBlast').prepend('<span id="errorEmailBlast" style="color:red;">'+data.message+'</span>');
                    toastr.error(data.message, "Error");
                }else{
                    $('.success-first').show();
                    $('.hide-email-comment').hide();
                    $("#verifiedDomain").val("").trigger('change');
                    CKEDITOR.instances['message_vol_members'].setData('');
                    $('#orgEmailBlastBtn').hide();
                    $('#orgEmailBlast_hide').show();
                    $('.top-50').css('margin-top', '50px');
                }
            },
            error: function(data) {
                $("#orgEmailBlastBtn").show();
                $("#orgEmailBlast_loading").hide();
                //console.log('Error:', data);
                toastr.error("Something went wrong.", "Error");
            }
        });
    }
});
$('#orgEmailBlast_hide').on('click', function() {
    $('#orgEmailBlast').modal('hide')
});