$('document').ready(function () {
    if (window.location.href.indexOf("subOrganization") > -1) {
        $('#subOrganizationTab').trigger('click');
    }
});

$('.closeModal').on('click', function () {
    $('#myModalCustomAtribute').modal('hide');
});

$('.add_attribute').on('click', function (e) {
    $('input', '#custAttrBody').val('');
    $('.form-group', '#custAttrBody').removeClass('hide');
    $('.wrapper_input .form-control').css("border", "");
    $(".select2-container", "#custAttrBody").css("border", "");
    scroll(0, 0);
    setTimeout(function () {
        e.preventDefault();
        var text = 'Organization';
        $('.type-user').val(text);
        $('#myModalCustomAtribute').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);

    }, 200)
});

bindElement();

$('.btn_cust_attribs').on('click', function() {
    var flag = 0;

    $('.p_invalid').hide();
    $('.wrapper_input .form-control').css("border", "");
    $(".select2-container", "#custAttrBody").css("border", "");
    if ($("#attributekey").val().trim() == "") {
        $(".select2-container", "#custAttrBody").css("border", "1px solid #ff0000");
        flag++;
    }
    if ($("#attributevalue").val().trim() == "") {
        $("#attributevalue").css("border", "1px solid #ff0000");
        flag++;
    }

    if (flag == 0) {
        var url = API_URL + 'add-customattribute';

        $(".btn_cust_attribs, .closeModal").hide();
        $(".btn_regs_loading").show();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var type = "POST";
        var formData = {
            cust_attr_id: $("#custAttributId").val(),
            attributekey: $("#attributekey").val(),
            attributevalue: $("#attributevalue").val()
        };

        $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function(data) {
                $(".btn_cust_attribs, .closeModal").show();
                $(".btn_regs_loading").hide();

                if (data.result == 'success') {
                    window.location.href = $('#custAttrURL').val();
                    location.reload();
                }
                if (data.result == 'invalid_id') {
                    alert('Record not found for sent id');
                    /*  $('#o_location_zipcode_alert').show();
                      $('#o_zipcode').css('border','1px solid #ff0000');
                      $("#o_zipcode").val('');*/
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }
});

$('.editBtn').on('click', function () {
    $.ajax({
        type: "GET",
        url: API_URL + 'edit-customattribute/' + $(this).attr('data-cust-attrid'),
        success: function (data) {
            console.log(data);
            //$('#subOrgBody').html('');
            $('#custAttrBody').html(data);
            $('#myModalCustomAtribute').modal('show');
            bindElement();
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
});

function bindElement() {
    $("select").select2({
        theme: "bootstrap",
        minimumResultsForSearch: -1
    });
    $('#found_day').datepicker({
        autoclose: true,
        format: 'yyyy',
        minViewMode: 2
    });
}

/////////////////////////////////////////////////