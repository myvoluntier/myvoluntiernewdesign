$('#share_able').on('change', function () {

    if ($(this).prop('checked')) {
        $('.private_passcode_cls').show();
        $('.auto_accept_join_cls').show();

    } else {
        $('.private_passcode_cls').hide();
        $('.auto_accept_join_cls').hide();
        $('#private_passcode').val("");
    }
});

$('#group_type').on('change', function () {

    if ($('#group_type').val() == 0) {
        $('.private_share_able_cls').removeClass('d-none');
        $('.auto_accept_join_cls').hide();
        if ($('#share_able').prop('checked')) {
            $('.private_passcode_cls').show();
            $('.auto_accept_join_cls').show();
        }
    }
    else if ($('#group_type').val() == 2) {
        $('.private_share_able_cls').addClass('d-none');
        $('.auto_accept_join_cls').hide();
        $('.private_passcode_cls').hide();
    }
    else {
        $('.private_share_able_cls').addClass('d-none');
        $('.auto_accept_join_cls').show();
        $('.private_passcode_cls').hide();
    }

});

var $uploadCrop,
    tempFilename,
    rawLogoImg,
    rawBannerImg,
    imageId;
var $selected_input;

function readURL(input, name) {
    $selected_input = name;
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        if (name == "profile") {
            reader.onload = function (e) {
                $('.upload-logo').addClass('ready');
                $('#cropImagePop').modal('show');
                rawLogoImg = e.target.result;
            };
        } else {
            reader.onload = function (e) {
                $('.upload-banner').addClass('ready');
                $('#cropBannerImagePop').modal('show');
                rawBannerImg = e.target.result;
            };
        }

        reader.readAsDataURL(input.files[0]);
        // console.log()e.target.result;
    }
}

$uploadLogoCrop = $('#upload-logo').croppie({
    viewport: {
        width: 300,
        height: 300
    },
    enforceBoundary: false,
    enableExif: true
});

$uploadBannerCrop = $('#upload-banner').croppie({
    viewport: {
        width: 700,
        height: 300
    },
    enforceBoundary: false,
    enableExif: true
});

$('#cropImagePop').on('shown.bs.modal', function () {
    // alert('Shown pop');
    $uploadLogoCrop.croppie('bind', {
        url: rawLogoImg
    }).then(function () {
        console.log('jQuery bind complete');
    });
});

$('#cropBannerImagePop').on('shown.bs.modal', function () {
    // alert('Shown pop');
    $uploadBannerCrop.croppie('bind', {
        url: rawBannerImg
    }).then(function () {
        console.log('jQuery bind complete');
    });
});

$('.item-img').on('change', function () {
    imageId = $(this).data('id');
    tempFilename = $(this).val();
    $('#cancelCropBtn').data('id', imageId);
    readFile(this);
});

$('#cropImageBtn').on('click', function (ev) {
    $uploadLogoCrop.croppie('result', {
        type: 'base64',
        format: 'jpeg',
        backgroundColor: 'white',
        size: { width: 300, height: 300 }
    }).then(function (resp) {
        $('#image_logo').val(resp);
        $('#item-img-output').attr('src', resp);
        $('#cropImagePop').modal('hide');
    });
});

$('#cropBannerImageBtn').on('click', function (ev) {
    $uploadBannerCrop.croppie('result', {
        type: 'base64',
        format: 'jpeg',
        backgroundColor: 'white',
        size: { width: 1110, height: 250 }
    }).then(function (resp) {
        $('#image_banner').val(resp);
        $('#item-img-output-banner').attr('src', resp);
        $('#cropBannerImagePop').modal('hide');
    });
});

$('#btn_create_group').on('click', function (e) {

    e.preventDefault;

    var flags = 0;
    var domains = $(".domain").select2('val');

    $('#private_passcode,.select2-selection--multiple,#group_name,#group_category_div .select2,#cke_description').css("border-bottom", "1px solid rgb(229, 230, 231)");

    isPublic = $('#group_type').val();


    if ($("#group_name").val() == '') {
        $("#group_name").addClass("error");
        flags++;
    } else {
        $("#group_name").removeClass("error");
        flags = getValidatedName(flags);
    }

    if (isPublic == 0 && $('#share_able').prop('checked')) {
        if ($('#private_passcode').val() == '' || $('#private_passcode').val().length < 4) {
            $('#private_passcode').css("border-bottom", "1px solid #ff0000");
            flags++;
        }
    }


    if (isPublic == 2 && domains == '') {
        $(".select2-selection--multiple").css("border-bottom", "1px solid #ff0000");
        flags++;
    }

    if (isPublic == 2 && domains != '') {

        var rules = $('#builder').queryBuilder('getRules');

        if (!$.isEmptyObject(rules)) {
            $('#rules').val(JSON.stringify(rules));
        }
    }

    $("#group_category_div .select2").css("border", "");

    if ($("#group_category_id").val() == '') {
        $("#group_category_div .select2").addClass("error");
        flags++;
    }
    $("#cke_description, #cke_qualification").css("border", "");
    if (CKEDITOR.instances['description'].getData() == '') {
        $("#cke_description").addClass("error");
        flags++;
    } else {
        $("#cke_description").removeClass("error");
    }
    if ($('#contact_name').val() == '') {
        $('#contact_name').addClass("error");
        flags++;
    }
    if ($('#contact_email').val() == '') {
        $('#contact_email').addClass("error");
        flags++;
    }
    if (!ValidateEmail($('#contact_email').val())) {
        flags++;
        $('#invalid_email_alert').show();
    }
    if ($('#contact_phone').val() == '') {
        $('#contact_phone').addClass("error");
        $('#invalid_email_alert').show();
        flags++;
    }
    if (!ValidatePhoneNumber($('#contact_phone').val())) {
        flags++;
        $('#invalid_phone_alert').show();
        $('#contact_phone').css("border-bottom", "1px solid #ff0000");
    }

    if (flags == 0 && $('#banner_size').val() == 0) {

        // if (isPublic == 2 && $.isEmptyObject($('#builder').queryBuilder('getRules'))) {
        //
        //     toastr.error("Please fill the UI rule for dynamic group.", "Error");
        // }
        // else
        {
            $('#uiGroup').addClass("disableControl");

            $("#btn_create_group").hide();
            $("#btn_create_group_loading").show();

            $('#post_group_form').submit();
        }
    } else {
        toastr.error("Form data is invalid please select all fields.", "Error");
    }
});

var _URL = window.URL || window.webkitURL;

$("#inputBanner").change(function (e) {
    var file, img;
    readURL(this, 'banner');
    //            if ((file = this.files[0])) {
    //                img = new Image();
    //                img.onload = function () {
    //
    //                    if(this.width<1200 || this.height<280)
    //                    {
    //                        $('#banner_size').val(1);
    //                        alert('Recomended banner size 1200 X 280. Please try another');
    //                    }
    //                    else{
    //                        console.log()
    //                        $('#banner_size').val(0);
    //
    //                        //alert(this.width+'=='+this.height);
    //                    }
    //                };
    //                img.src = _URL.createObjectURL(file);
    //            }
});

$('.form-control').on('click', function () {
    $(this).css("border-bottom", "1px solid #e5e6e7");
    $(this).parent().find('.p_invalid').hide();
});

$(document).ready(function () {

    $('body').on('change', '#inputImage', function () {
        readURL(this, 'profile');
    });

    $("select").select2({
        theme: "bootstrap",
        minimumResultsForSearch: -1
    });

    $('#weekdays').change(function () {
        $('#weekday_vals').val($('#weekdays').val());
    });

    var $image = $(".profile-image-hover > img");

    var $inputImage = $("#inputImage");

    if (window.FileReader) {
        $inputImage.change(function () {
            var fileReader = new FileReader(),
                files = this.files,
                file;

            if (!files.length) {
                return;
            }

            file = files[0];
            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function (e) {
                    $image.attr('src', e.target.result);
                };
            } else {
                console.log("Please choose an image file.");
            }
        });
    } else {
        $inputImage.addClass("hide");
    }


});

function getValidatedName(flags) {

    $('#nameError').hide();
    var formData = {
        group_name: $('#group_name').val(),
        group_id: $('#group_id').val()
    };

    $.ajax({
        type: 'POST',
        url: API_URL + 'organization/validated-group',
        data: formData,
        async: false,
        success: function (data) {

            $('#nameError').html(data);
            if (data != "success") {

                $("#group_name").css("border-bottom", "1px solid #ff0000");
                $('#nameError').show();
                flags++;
            } else {
                flags = 0;
            }

        }
    });
    return flags;
}

CKEDITOR.replace('description', {
    height: 150,
    removeButtons: '',
    extraPlugins: 'colorbutton,colordialog'
});