$('#titleError').hide();

$('.select2').select2();

$('#title').attr('maxlength', '120');

if ($('#auto_accept').is(':checked')) {
    $('.private_passcode_cls').show();
}

if ($('#non_expiring').is(':checked')) {
    $('#endDateInput').hide();
    $('#end_date').datepicker('setDate', '<?php echo date("m/d/Y"); ?>');
}

$('#start_date_icon').css("cursor", "pointer");

$('#end_date_icon').css("cursor", "pointer");

$('#start_date_icon').on('click', function () {
    $('#start_date').datepicker('show');
})

$('#end_date_icon').on('click', function () {
    $('#end_date').datepicker('show');
});

var start = new Date();
// set end date to max one year period:
var end = new Date(new Date().setYear(start.getFullYear() + 1));

$('#start_date').datepicker({
    format: 'mm-dd-yyyy',
    autoclose: true
    // update "toDate" defaults whenever "fromDate" changes
}).on('changeDate', function () {
    // set the "toDate" start to not be later than "fromDate" ends:
    $('#end_date').datepicker('setStartDate', new Date($(this).val()));
});

$('#end_date').datepicker({
    format: 'mm-dd-yyyy',
    autoclose: true
    // update "fromDate" defaults whenever "toDate" changes
}).on('changeDate', function () {
    // set the "fromDate" end to not be later than "toDate" starts:
    $('#start_date').datepicker('setEndDate', new Date($(this).val()));
});

$('#btn_post').on('click', function (e) {
    e.preventDefault;
    $("#end_date").css("border", "0px");
    var flags = 0;
    if ($("#title").val() == '') {
        $("#title").addClass("error");
        flags++;
    } else {
        flags = getValidatedTitle(flags);
    }
    $("#cke_description, #cke_qualification").css("border", "");
    if (CKEDITOR.instances['description'].getData() == '') {
        $("#cke_description").addClass("error");
        flags++;
    }
    // if ($("#activity").val() == '') {
    //     $("#activity").css("border", "1px solid #ff0000");
    //     flags++;
    // }
    if (CKEDITOR.instances['qualification'].getData() == '') {
        $("#cke_qualification").addClass("error");
        flags++;
    }
    if ($("#street1").val() == '') {
        $("#street1").css("border-bottom", "1px solid #ff0000");
        flags++;
    }
    if ($("#city").val() == '') {
        $("#city").addClass("error");
        flags++;
    }
    if ($("#state").val() == '') {
        $("#state").css("border-bottom", "1px solid #ff0000");
        flags++;
    }
    if ($('#zipcode').val() == '') {
        $('#zipcode').addClass("error");
        flags++;
    }
    if (!ValidateZipcode($('#zipcode').val())) {
        flags++;
        $('#zipcode').addClass("error");
        $('#invalid_zipcode_alert').show();
    }
    if ($("#start_date").val() == '') {
        $("#start_date").addClass("error");
        flags++;
    }
    if (!$('#non_expiring').is(':checked') && $("#end_date").val() == '') {
        $("#end_date").addClass("error");
        flags++;
    }
    if ($("#weekday_vals").val() == '') {
        $("#weekdays_chk_border").addClass("error");
        flags++;
    }
    else {
        $("#weekdays_chk_border").css("border-bottom", "1px solid #fff");
    }

    if ($('#auto_accept').val() == '1' && $('#private_passcode').val().length > 0 && $('#private_passcode').val().length < 4) {
        $('#private_passcode').css("border-bottom", "1px solid #ff0000");
        flags++;
    }


    if ($('#contact_name').val() == '') {
        $('#contact_name').addClass("error");
        flags++;
    }
    if ($('#contact_email').val() == '') {
        $('#contact_email').addClass("error");
        flags++;
    }
    if (!ValidateEmail($('#contact_email').val())) {
        flags++;
        $('#invalid_email_alert').show();
    }
    if ($('#contact_phone').val() == '') {
        $('#contact_phone').addClass("error");
        flags++;
    }
    $('#time-range').parent().css("border", "");
    if ($('#start_at').val() == $('#end_at').val()) {

        $('#time-range').parent().css("border-bottom", "1px solid #ff0000");
        flags++;
    }
    if (!ValidatePhoneNumber($('#contact_phone').val())) {
        flags++;
        $('#invalid_phone_alert').show();
        $('#contact_phone').addClass("error");
        flags++;
    }

    if (flags == 0) {
        $('#btn_post_loading').show();
        $('#btn_post').hide();
        $('#post_opportunity').submit();
    } else {
        toastr.error("Form data is invalid.", "Error");
    }
});

function inProgress() {
    $('#opportunity_box').addClass("in-progress");
    $('#btn_post_loading').show();
    $('#btn_post').hide();
}

function outProgress() {
    $('#opportunity_box').removeClass("in-progress");
    $('#btn_post_loading').hide();
    $('#btn_post').show();
}

function getValidatedTitle(flags) {
    var formData = {
        title: $('#title').val(),
        opp_id: $('#opp_id').val()
    }
    inProgress();
    $.ajax({
        type: 'POST',
        url: API_URL + 'organization/validated-opportunity',
        data: formData,
        async: false,
        success: function (data) {
            outProgress();
            $('#titleError').hide();
            if (data != "success") {

                $("#title").css("border-bottom", "1px solid #ff0000");
                $('#titleError').show();
                $('#titleError').html('Title is already exist for other opportunity.');
                flags++;
            } else {
                flags = 0;
            }

        }
    });
    return flags;
}

CKEDITOR.replace('description', {
    height: 150,
    removeButtons: '',
    extraPlugins: 'colorbutton,colordialog'
});
CKEDITOR.replace('activity', {
    height: 150,
    removeButtons: '',
    extraPlugins: 'colorbutton,colordialog'
});
CKEDITOR.replace('qualification', {
    height: 150,
    removeButtons: '',
    extraPlugins: 'colorbutton,colordialog'
});
CKEDITOR.replace('add_info', {
    height: 150,
    removeButtons: '',
    extraPlugins: 'colorbutton,colordialog'
});