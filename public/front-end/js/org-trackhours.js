$('document').ready(function() {
    if(window.location.href.indexOf("trackedHours") > -1) {
        $('#trackedHoursTab').trigger('click');
    }
    $('.wrapper_input.fa-icons input').datepicker({
        'format': 'mm-dd-yyyy',
        'autoclose': true,
        'orientation': 'right',
        'todayHighlight': true,
        'disabled' : true
    });
    $('#time_block_view').multiSelect("refresh");
    $('.viewTrackHours').on('click', function() {
        var track_id = $(this).attr('data-trackid');
        var url = $('#trackurl').val() + "/" + track_id;
        $.ajax({
            url: url,
            type: 'GET',
            dataType: "json",
            success: function(result) {

                // refereshAddHours(result[track_id]);
                $('#time_block option:selected', '#view_track').removeAttr("selected");
                $('#time_block_view option:selected', '#view_track').removeAttr("selected");

                var dataarray = result[track_id].selected_blocks.split(",");
                i = 0, size = dataarray.length;
                var mins = size * 30;
                for (i; i < size; i++) {
                    $("#time_block option[value='" + dataarray[i] + "']", '#view_track').attr("selected", 1);
                    $("#time_block_view option[value='" + dataarray[i] + "']", '#view_track').attr("selected", 1);
                }

                $('#time_block', '#view_track').multiSelect("refresh");
                $('#time_block_view', '#view_track').multiSelect("refresh");

                $('#comment_display, #adding_hours_comments', '#view_track').html(result[track_id].comment);

                $('#hours_mins_view', '#view_track').text("(" + mins + "mins)");

                $(".oppertunity_display", '#view_track').html(result[track_id].opportunity_name);
                $('#date_day', '#view_track').html(result[track_id].logged_date_day);


                $('#view_track').modal('show');
            }
        });

    });

    $('.eidtTrackHours').on('click', function() {
        var track_id = $(this).attr('data-trackid');
        var url = $('#trackurl').val() + "/" + track_id;
        $.ajax({
            url: url,
            type: 'GET',
            dataType: "json",
            success: function(result) {

                refereshAddHours(result[track_id]);
                $('#time_block option:selected', '#edit_hours').removeAttr("selected");
                $('#time_block_view option:selected', '#edit_hours').removeAttr("selected");

                var dataarray = result[track_id].selected_blocks.split(",");
                i = 0, size = dataarray.length;
                var mins = size * 30;
                for (i; i < size; i++) {
                    $("#time_block option[value='" + dataarray[i] + "']", '#edit_hours').attr("selected", 1);
                    $("#time_block_view option[value='" + dataarray[i] + "']", '#edit_hours').attr("selected", 1);
                }

                $('#time_block', '#edit_hours').multiSelect("refresh");
                $('#time_block_view', '#edit_hours').multiSelect("refresh");

                $('#adding_hours_comments', '#edit_hours').val(result[track_id].comment);

                $('#hours_mins_view', '#edit_hours').text("(" + mins + "mins)");

                $(".oppertunity_display", '#edit_hours').html(result[track_id].opportunity_name);
                $("#opp_name", '#edit_hours').val(result[track_id].opportunity_name);
                $('#date_day', '#edit_hours').html(result[track_id].logged_date_day);
                $('#selected_date_for_unlist', '#edit_hours').val(result[track_id].logged_date);
                
                $('#hours_mins', '#edit_hours').text("(" + mins + "mins)");
                $('#opp_id', '#edit_hours').val(result[track_id].opportunity_id);
                $('#is_designated', '#edit_hours').val(result[track_id].is_designated);
                $('#designated_group_id_select', '#edit_hours').val(result[track_id].designated_group_id_select);
                $('#track_id', '#edit_hours').val(track_id);
                $('#volunteer_id', '#edit_hours').val(result[track_id].volunteer_id)

                $('#edit_hours').modal('show');
            }
        });

    });

    function setTrackData(calEvent) {
        refereshAddHours(calEvent);
        $('#opp_id').prop('disabled', false);
        $('.opp_select_div').show();
        $('.private_opp_div_add_hours').hide();
        $('#private_opp_name_hours').val('');
        $('#unlist_org_name_hours').val('');
        $('#unlist_org_mail_hours').val('');
        $('#unlist_opp_name').val('');
        // alert('Event: ' + calEvent.title);
        $('#is_edit').val(1);
        $('#track_id').val(calEvent.id);
        $('#btn_remove_hours').show();
        $('#is_no_org').val(0);
        $('#date_day').html(calEvent.logged_date_day);
        $('#adding_hours_comments').val(calEvent.comments);

        if (calEvent.approved == 1) {
            $('#btn_remove_hours').hide();
            $('#btn_add_hours').hide();
            $('#time_block').prop('disabled', true);
            $('#time_block_view').prop('disabled', true);
            $('#opp_id').prop('disabled', true);
            $('#adding_hours_comments').prop('disabled', true);

        } else {
            $('#btn_remove_hours').show();
            $('#btn_add_hours').show();
            $('#time_block').prop('disabled', false);
            $('#time_block_view').prop('disabled', true);
            $('#opp_id').prop('disabled', false);
            $('#adding_hours_comments').prop('disabled', false);
        }

        $('#time_block option:selected').removeAttr("selected");
        $('#time_block_view option:selected').removeAttr("selected");

        var dataarray = calEvent.selected_blocks.split(",");
        i = 0, size = dataarray.length;

        for (i; i < size; i++) {
            $("#time_block option[value='" + dataarray[i] + "']").attr("selected", 1);
            $("#time_block_view option[value='" + dataarray[i] + "']").attr("selected", 1);
//                                $("#time_block").multiselect("refresh");
        }

        $('#time_block').multiSelect("refresh");
        $('#time_block_view').multiSelect("refresh");
        $('.private_opp_div').hide();
        $('.opp_select_div').show();
        $('.confirmed_track').hide();
        $('.add_hours_modal_title').text("Do you want make Change?");

        if (calEvent.opp_id == 0) {
            $('.opp_select_div').hide();
            $('.private_opp_div').show();
            $('#private_opp_name_hours').val(calEvent.title);
            $('#is_no_org').val(1);
            $('.private_opp_div_add_hours').show();
        }
        if (calEvent.link == 1) {
            $('#is_link_exist').val(1);
        } else {
            $('#is_link_exist').val(0);
        }


        var mins = size * 30;
        // $('#hours').text("("+h+"hrs "+m+"mins)");
        $('#hours_mins').text("(" + mins + "mins)");
        $('#selected_date_for_unlist').datepicker('setDate', calEvent.logged_date);
        $('#comments').text('');
        $("select").select2({
            theme: "bootstrap",
            minimumResultsForSearch: 1
        });

        $('#btn_add_hours').prop('disabled', false);

        //set modal value for view
        var title = $('#opp_id option:selected').text();
        // alert(title);
        $(".oppertunity_display").html(title);
        $('#hours_mins_view').text("(" + mins + "mins)");
        $('#comment_display').html(calEvent.comments);
        // alert(calEvent.id);
        $('#ics_download_value').val(calEvent.id);
    }

    function refereshAddHours(calEvent) {
        $('#time_block', '#edit_hours').prop('disabled', false);
        $('option', $('#time_block', '#edit_hours')).each(function(element) {
            $(this).removeAttr('selected').prop('selected', false);
        });
        $("#time_block", '#edit_hours').multiSelect('refresh');
        $('#hours_mins', '#edit_hours').text('(0min)');
        $('#adding_hours_comments', '#edit_hours').val('');

        $('#is_designated', '#edit_hours').val('0');
        if ($('#is_designated', '#edit_hours').is(':checked')) {
            $('#is_designated', '#edit_hours').trigger('click');
        }
        $('#dayError,#hourError', '#edit_hours').hide();
        $("#designated_group_id", '#edit_hours').hide();
        $('#btn_add_hours').text('Save')
    }

    $('#time_block').multiSelect({
        afterSelect: function(values) {
            var mins = $('#time_block').val().length * 30;
            $('#hours_mins').text("(" + mins + "mins)");
//                alert("Select value: "+values);
        },
        afterDeselect: function(values) {
            var mins = $('#time_block').val().length * 30;
            $('#hours_mins').text("(" + mins + "mins)");
//                alert("Deselect value: "+values);
        }
    });

    $('#btn_add_hours').on('click', function(e) {
        $(".ms-selection").css("border", "");
        $('#dayError,#hourError').hide();
        e.preventDefault();

        var selected_date = $('#selected_date_for_unlist').val();
        selected_date = selected_date.slice(0, 10);

        var opp_id = $('#opp_id').val();
        var is_designated = $('#is_designated').val();
        var designated_group_id = $('#designated_group_id_select').val();

        var opp_name = $('#opp_name').val();
        var logged_mins = $('#hours_mins', '#edit_hours').text();
        logged_mins = (logged_mins.slice(1)).slice(0, -5);

        var adding_hours_comments = $('#adding_hours_comments', '#edit_hours').val();
        var tracking_id = $('#track_id', '#edit_hours').val();
        var is_edited = 1;
        var is_no_org = $('#is_no_org').val();
        var timeval = $('#time_block', '#edit_hours').val().toString().split(',');
        var start_time = timeval[0];
        var end_time = timeval[timeval.length - 1];


        end_time = addMinutesToTime(end_time, 30);
        var time_block = $('#time_block', '#edit_hours').val().toString();
        if (is_no_org == 1) {
            opp_id = 'private';
        }
        var private_opp_name = opp_name;
        var unlist_org_name = $('#unlist_org_name_hours').val();
        var unlist_org_email = $('#unlist_org_mail_hours').val();

        $('.ms-list', '#ms-time_block').css('border', '');
        $('#selected_date_for_unlist').css('border', '');

        var org_email = $('#org_emails').val();



        if ($('#time_block', '#edit_hours').val() == "") {
            $('.ms-list', '#ms-time_block').css('border', '1px solid red');
            toastr.error("Please select time blocks.", "Error");
            return;
        }
        if ($('#selected_date_for_unlist', '#edit_hours').val() == "") {
            $('#selected_date_for_unlist').css('border', '1px solid red');
            toastr.error("Please select date you want add hours on.", "Error");
            return;
        }
        var restDay = 0;
        restDay = getValidatedTrackDay('0', 'organization');
        console.log(restDay);
        
        //restDay = 2;
        
        if (restDay == 0) {

            $('#btn_add_hours', '#edit_hours').prop('disabled', true);
            var url = API_URL + 'organization/track/addHours';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var type = "post";
            var formData = {
                opp_id: opp_id,
                is_designated: is_designated,
                designated_group_id: designated_group_id,
                opp_name: opp_name,
                start_time: start_time,
                end_time: end_time,
                logged_mins: logged_mins,
                selected_date: selected_date,
                comments: adding_hours_comments,
                is_edit: is_edited,
                tracking_id: tracking_id,
                time_block: time_block,
                is_no_org: is_no_org,
                private_opp_name: private_opp_name,
                unlist_org_name: unlist_org_name,
                unlist_org_email: unlist_org_email,
                org_email: org_email,
                volunteer_id: $('#volunteer_id').val()
            };

            $('#btn_add_hours', '#edit_hours').text('Please Wait..!');

            $.ajax({
                type: type,
                url: url,
                data: formData,
                success: function(data) {
                    // location.reload();
                    // if (data.result == 'approved track') {
                     $('#btn_add_hours').text('Save');
                     toastr.success('Updated hours successfully.', "Message");
                      $('#edit_hours').modal('hide');
                   /* if (data.result == 'unlist sent') {
                        toastr.success('Unlisted Opportunity request sent to organization.', "Message");
                    }

                    else if (data.result == 'approved track') {
                        $('.confirmed_track').show();
                        toastr.success('Approved Tracking Hours.', "Message");
                    } else if (data.result == 'declined track') {
                        toastr.success('Declined Tracking Hours.', "Message");
                    } else {
                        if (data.opp_logo == null) {
                            var logo = SITE_URL + 'front-end/img/org/001.png';
                        } else {
                            var logo = SITE_URL + 'uploads/' + data.opp_logo;
                        }
                        var currentdate = new Date();
                        var current_time = currentdate.getFullYear() + '-' + (currentdate.getMonth() + 1) + '-' + currentdate.getDate() + ' ' + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
                        var current_date = currentdate.getFullYear() + '-' + (currentdate.getMonth() + 1) + '-' + currentdate.getDate();
                        var url = SITE_URL + 'volunteer/view_opportunity/' + opp_id;
                        if (data.is_link_exist == 1) {
                            var link = '<a href="' + url + '"><strong>' + opp_name + '</strong></a>';
                        } else {
                            if (is_no_org == 0) {
                                var link = '<strong>' + opp_name + '</strong>';
                            } else {
                                var link = '<strong>' + private_opp_name + '</strong>';
                            }
                        }
                        

                            $('.track-activity-panel').prepend($('<tr><td style="padding-left: 50px;"><img alt="image" class="img-circle" src="' + logo + '"> <i class="fa fa-reply"></i>You Updated Logged Hours to ' + logged_mins + 'mins on Opportunity ' + link + '</td><td>' + current_time + '</td></tr>'));
                            $('#' + 'pending' + tracking_id).remove();
                            $('.table_pending_view').prepend($('<tr class="pending-approval" id="pending' + tracking_id + '"><td style="text-align: left; padding-left: 20px"><img alt="image" class="img-circle" src="' + logo + '"> ' + link + '</td><td>' + selected_date + '</td><td>' + start_time + '</td><td>' + end_time + '</td><td>' + logged_mins + '</td><td>' + current_date + '</td><td class="label label-warning" style="display: table-cell; font-size:13px;">Pending</td></tr>'));
                        

                      //  doSubmit(data.track_id);
                    }*/

                    window.location.href = $('#trackedURL').val();
                    location.reload();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        } else {
            toastr.error("Something went wrong.", "Error");
            return;
            //$('#empty_opportunity_alert').show();
        }
    });
});
/////////////////////////////////////////////////