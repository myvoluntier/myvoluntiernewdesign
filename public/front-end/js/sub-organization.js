$('document').ready(function () {
    if (window.location.href.indexOf("subOrganization") > -1) {
        $('#subOrganizationTab').trigger('click');
    }
});

$('.closeModal').on('click', function () {
    $('#myModalSubOrgRegistration').modal('hide');
});

$('.suborg_registr').on('click', function (e) {
    $('input', '#subOrgBody').val('');
    $('.form-group', '#subOrgBody').removeClass('hide');
    scroll(0, 0);
    setTimeout(function () {
        e.preventDefault();
        var text = 'Organization';
        $('.type-user').val(text);
        $('#myModalSubOrgRegistration').modal('show');
        $('body').removeClass('offcanvas-menu-show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        }, 500);

    }, 200)
});

bindElement();

$('.btn_sub_regs').on('click', function () {
    var vol_flags = 0;
    var org_flags = 0;

    $('#o_invalid_username_alert').hide();
    $('.p_invalid').hide();
    $('.wrapper_input .form-control').css("border", "");

    // if ($("#o_user_name").val() == '') {
    //     $("#o_user_name").css("border", "1px solid #ff0000");
    //     org_flags++;
    // }

    if ($("#sub_org_name").val() == '') {
        $("#sub_org_name").css("border", "1px solid #ff0000");
        org_flags++;
    }
    if ($("#found_day").val() == '') {
        $("#found_day").css("border", "1px solid #ff0000");
        org_flags++;
    }
    if ($("#o_zipcode").val() == '') {
        $("#o_zipcode").css("border", "1px solid #ff0000");
        org_flags++;
    }
    if (!ValidateZipcode($('#o_zipcode').val())) {
        org_flags++;
        $('#o_invalid_zipcode_alert').show();
    }
    if ($("#o_email").val() == '') {
        $("#o_email").css("border", "1px solid #ff0000");
        org_flags++;
    }
    if (!ValidateEmail($('#o_email').val())) {
        org_flags++;
        $('#o_invalid_email_alert').show();
    }
    if ($("#o_contact_num").val() == '') {
        $("#o_contact_num").css("border", "1px solid #ff0000");
        org_flags++;
    }

    if (!ValidatePhoneNumber($('#o_contact_num').val())) {
        org_flags++;
        $('#o_invalid_contact_number').show();
    }

    if ($("#o_password").val() != '') {

        if (!ValidatePassword($('#o_password').val())) {
            org_flags++;
            $('#o_invalid_password_alert').show();
        }
        if ($("#o_confirm").val() == '') {
            $("#o_confirm").css("border", "1px solid #ff0000");
            org_flags++;
            $('#o_invalid_password_alert').show();
        }

        if($('#o_password').val() != $('#o_confirm').val()){
            $("#o_confirm").css("border", "1px solid #ff0000");
            org_flags++;
            $('#o_invalid_password').show();
        }
    }

    if ($('#subOrgId').val() == "") {

        if ($("#o_password").val() == '') {
            $("#o_password").css("border", "1px solid #ff0000");
            org_flags++;
        }
        if (!ValidatePassword($('#o_password').val())) {
            org_flags++;
            $('#o_invalid_password').show();
        }
        if ($("#o_confirm").val() == '') {
            $("#o_confirm").css("border", "1px solid #ff0000");
            org_flags++;
            $('#o_invalid_password').show();
        }

        if($('#o_password').val() != $('#o_confirm').val()){
            $("#o_confirm").css("border", "1px solid #ff0000");
            org_flags++;
            $('#o_invalid_password').show();
        }

        if ($("#o_accept_terms").is(":not(:checked)")) {
            $("#o_terms_alert").show();
            $("#o_terms_alert").css("color", "red");
            org_flags++;
        }
        if ($('#o_password').val() != $('#o_confirm').val()) {
            org_flags++;
        }

        if ($("#o_accept_terms").is(":not(:checked)")) {
            $("#ov_terms_alert").show();
            $("#ov_terms_alert").css("color", "red");
            org_flags++;
        } else {
            $('#ov_terms_alert').removeClass("error-has");
        }
        if ($("#op_accept_terms").is(":not(:checked)")) {
            $("#ov_policy_alert").show();
            $("#ov_policy_alert").css("color", "red");
            org_flags++;

        } else {
            $('#ov_policy_alert').removeClass("error-has");
        }
    }
    var autoAccept = 0;
    if ($("#o_auto_accept_friends").is(":not(:checked)")) {
        autoAccept = 0;
    } else {
        autoAccept = 1;
    }
    console.log("org_flags " + org_flags);

    if (org_flags == 0) {
        var url = API_URL + 'reg_organization';

        $(".btn_sub_regs, .closeModal").hide();
        $(".btn_regs_loading").show();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var type = "POST";
        var formData = {
            sub_org_id: $("#subOrgId").val(),
            // user_name: $("#o_user_name").val(),
            sub_org_name: $('#sub_org_name').val(),
            school_type: $('#school_type option:selected').val(),
            ein: $('#org_ein').val(),
            nonprofit_org_type: $('#non_org_type').val(),
            birth_day: $('#found_day').val(),
            zipcode: $('#o_zipcode').val(),
            org_type: $("#o_org_type option:selected").val(),
            email: $('#o_email').val(),
            contact_number: $('#o_contact_num').val(),
            password: $('#o_password').val(),
            facebook_url: $('#o_facebook_url').val(),
            twitter_url: $('#o_twitter_url').val(),
            linkedin_url: $('#o_linkedin_url').val(),
            auto_accept_friends: autoAccept,
            website: $('#o_website').val(),
        };

        $.ajax({
            type: type,
            url: url,
            data: formData,
            success: function (data) {
                $(".btn_sub_regs, .closeModal").show();
                $(".btn_regs_loading").hide();
                // if (data.result == 'username exist') {
                //     $('#o_invalid_username_alert').show();
                //     $('#o_user_name').css('border','1px solid #ff0000');
                //     //$("#o_user_name").val('');
                // }
                if (data.result == 'email exist') {
                    $('#o_existing_email_alert').show();
                    $('#o_email').css('border', '1px solid #ff0000');
                    //$("#o_email").val('');
                }
                if (data.result == 'invalid zipcode') {
                    $('#o_location_zipcode_alert').show();
                    $('#o_zipcode').css('border', '1px solid #ff0000');
                    $("#o_zipcode").val('');
                }
                if (data.result == 'success') {
                    window.location.href = $('#subOrgURL').val();
                    location.reload();
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

});
$('.editSubOrgBtn').on('click', function () {
    $.ajax({
        type: "GET",
        url: API_URL + 'organization/edit-sub-organization/' + $(this).attr('data-sub-orgid'),
        success: function (data) {
            console.log(data);
            //$('#subOrgBody').html('');
            $('#subOrgBody').html(data);
            $('#myModalSubOrgRegistration').modal('show');
            bindElement();
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
});

function bindElement() {
    $("select").select2({
        theme: "bootstrap",
        minimumResultsForSearch: -1
    });
    $('#found_day').datepicker({
        autoclose: true,
        format: 'yyyy',
        minViewMode: 2
    });
}

/////////////////////////////////////////////////