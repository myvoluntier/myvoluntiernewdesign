function addMinutesToTime(time, minsAdd) {
    function z(n) {
        return (n < 10 ? '0' : '') + n;
    }
    var bits = time.split(':');
    var mins = bits[0] * 60 + +bits[1] + +minsAdd;
    return z(mins % (24 * 60) / 60 | 0) + ':' + z(mins % 60);
}

    
    
     function getValidatedTrackDay(flags, cntName) {
                console.log(cntName);
                var formData = {
                    addDate: $('#selected_date_for_unlist').val(),
                    oppId: $('#opp_id').val()
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: API_URL + cntName + '/track/validatedTrackDate',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#dayError').hide();
                        $("#selected_date_for_unlist").css("border", "");
                        if (data != "success") {

                            $("#selected_date_for_unlist").css("border", "1px solid #ff0000");
                            $('#dayError').css('color', '#ff0000');
                            $('#dayError').show();
                            $('#dayError').html(data);
                            flags++;
                        } else {
                            flags = 0;
                        }
                    }
                });

                if ($('#time_block', '#edit_hours').val().length > 0 && flags == 0) {
                    var resultFlag = getValidatedMaxVolunteer('0', cntName);
                    if (resultFlag == '0') {
                        return getValidatedTrackHours('0', cntName);
                    } else {
                        return 1;
                    }
                }
                return flags;
            }

            function getValidatedTrackHours(flags, cntName) {

                var formData = {
                    addDate: $('#selected_date_for_unlist').val(),
                    oppId: $('#opp_id').val(),
                    time_block: $('#time_block', '#edit_hours').val().toString(),
                    track_id: $('#track_id').val(),
                    is_edit: $('#is_edit').val()
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: API_URL + cntName + '/track/validatedTrackHours',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#hourError').hide();
                        $(".ms-selection").css("border", "");
                        if (data != "success") {

                            $(".ms-selection").css("border", "1px solid #ff0000");
                            $('#hourError').css('color', '#ff0000');
                            $('#hourError').show();
                            $('#hourError').html(data);
                            toastr.error("Selected time is overlapped.", "Error");
                            flags++;
                        } else {
                            flags = 0;
                        }

                    }
                });
                return flags;
            }

            function getValidatedMaxVolunteer(flags, cntName) {

                var formData = {
                    addDate: $('#selected_date_for_unlist').val(),
                    oppId: $('#opp_id').val(),
                    time_block: $('#time_block', '#edit_hours').val().toString(),
                    track_id: $('#track_id').val(),
                    is_edit: $('#is_edit').val()
                };

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: API_URL + cntName + '/track/validatedMaxVolunteer',
                    data: formData,
                    async: false,
                    success: function (data) {
                        $('#hourError').hide();
                        $(".ms-selection").css("border", "");

                        if (data != "success") {

                            $(".ms-selection").css("border", "1px solid #ff0000");
                            $('#hourError').css('color', '#ff0000');
                            $('#hourError').show();
                            $('#hourError').html(data);
                            toastr.error(data, "Error");
                            flags++;
                        } else {
                            flags = 0;
                        }

                    }
                });
                return flags;
            }

/////////////////////////////////////////////////